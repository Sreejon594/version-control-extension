<?php
 
require_once __DIR__ . '/vendor/autoload.php';

use Flow\JSONPath\JSONPath;
use Flow\JSONPath\JSONPathLexer;
use \Peekmo\JsonPath\JsonPath as PeekmoJsonPath;
 
// use the factory to create a Faker\Generator instance
//$jp = Flow\JSONPath\JSONPath::create();

$jsonPath = new JSONPath(['a' => 'A', 'b', 'B']);
$firstKey = $jsonPath->firstKey();

echo $firstKey;

$json = '{
  "type": "Form",
  "currentStatus": "Draft",
  "id": "22",
  "createdAt": "1474476438",
  "createdBy": "14",
  "depth": "complete",
  "folderId": "7",
  "name": "Transporter Validate Test 1.0",
  "permissions": [
    "Retrieve",
    "SetSecurity",
    "Delete",
    "Update"
  ],
  "updatedAt": "1474633196",
  "updatedBy": "11",
  "elements": [
    {
      "type": "FormField",
      "id": "199",
      "name": "Last Name",
      "style": "{\"fieldSize\":\"large\",\"labelPosition\":\"top\"}",
      "createdFromContactFieldId": "100003",
      "dataType": "text",
      "displayType": "text",
      "fieldMergeId": "3",
      "htmlName": "lastName",
      "validations": []
    },
    {
      "type": "FormField",
      "id": "201",
      "name": "Big_Small_Haha",
      "style": "{\"fieldSize\":\"large\",\"labelPosition\":\"top\"}",
      "createdFromContactFieldId": "100215",
      "dataType": "text",
      "displayType": "text",
      "htmlName": "bigSmallHaha1",
      "validations": []
    },
    {
      "type": "FormField",
      "id": "200",
      "name": "Submit",
      "altText": "Submit",
      "dataType": "text",
      "displayType": "submit",
      "htmlName": "submit",
      "validations": []
    }
  ],
 "htmlName": "UntitledForm-1474476393278",
  "processingSteps": [
    {
      "type": "FormStepCreateUpdateContactFromFormField",
      "id": "210",
      "name": "Create / Update Contact, Prospect or Company",
      "execute": "always",
      "mappings": [
        {
          "type": "FormFieldUpdateMapping",
          "sourceFormFieldId": "199",
          "targetEntityFieldId": "100003",
          "updateType": "useFieldRule"
        },
        {
          "type": "FormFieldUpdateMapping",
          "sourceFormFieldId": "201",
          "targetEntityFieldId": "100217",
          "updateType": "useFieldRule"
        }
      ]
    },
    {
      "type": "FormStepCreateUpdateAccountFromFormField",
      "id": "213",
      "name": "Create / Update Contact, Prospect or Company",
      "execute": "always",
      "keyFieldId": "100209",
      "mappings": [
        {
          "type": "FormFieldUpdateMapping",
          "sourceFormFieldId": "201",
          "updateType": "always"
        },
        {
          "type": "FormFieldUpdateMapping",
          "sourceFormFieldId": "199",
          "targetEntityFieldId": "100209"
        }
      ]
    }
  ],
  "processingType": "externalWebsite",
  "size": {
    "type": "Size",
    "width": "0",
    "height": "0"
  },
  "style": "{\"fieldSize\":\"medium\",\"labelPosition\":\"top\"}"
}';

$results = (new JSONPath(json_decode($json)))->find('$.processingSteps[?(@.type=FormStepCreateUpdateContactFromFormField)]');

echo (json_encode($results));

?>