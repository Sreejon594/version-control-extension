<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Logging_model extends CI_Model{
	function __construct()
    {
        parent::__construct();
		$this -> load -> helper('url');
        $this -> load -> database();
		// $this -> load -> model('Logging_model','',TRUE);
		$this -> load -> model('Settings_model','',TRUE);
		$this -> load -> library('session');
		$this->load->helper('file');	
    }
	
	function logT($packageId=0,$packageItemId=0,$message='',$additional_message='',$message_data='')
	{
		$tdata['Message'] = $message;
		$tdata['Additional_Message'] = $additional_message;
		if(is_object($message_data) || is_array($message_data))
		{
			$tdata['Message_Data'] = json_encode($message_data);
		}
		else
		{
			$tdata['Message_Data'] = $message_data;
		}	
		
		$tdata['Time_Stamp'] = date('Y-m-d H:i:s');
		$tdata['Deployment_Package_Item_Id'] = $packageItemId;	
		
		if($packageId == 0 && $packageItemId > 0)
		{
			$packageId = $this->getPackageId($packageItemId);
		}
		
		if(isset($packageId) && $packageId != '')
		{
			$tdata['Deployment_Package_Id'] = $packageId;
		}
		else
		{
			$tdata['Deployment_Package_Id'] = 0;
		}
		$this->db->insert('Logs',$tdata);
	}
	
	function getPackageId($packageItemId)
	{
		$this->db->select('Deployment_Package_Id');
		$this->db->from('deployment_package_item');
		$this->db->where('Deployment_Package_Item_Id',$packageItemId);
		$query = $this->db->get();
		if($query->num_rows()>0) 
		{
			return $query->result()[0];
		}
	}
	
	function logT_Additional($packageId=0,$packageItemId=0,$message='',$additional_message='',$message_data='')
	{		
		$sourceSiteName=$this->getSourceSiteNameByPackageId($packageId);
		if(isset($sourceSiteName))
		{
			$Additional_LogFlag=$this->getAdditionalLogFlagBySourceSiteName($sourceSiteName);
			if(isset($Additional_LogFlag) && $Additional_LogFlag==1)
			{
				$tdata['Message'] = $message;
				$tdata['Additional_Message'] = $additional_message;
				if(is_object($message_data) || is_array($message_data))
				{
					$tdata['Message_Data'] = json_encode($message_data);
				}
				else
				{
					$tdata['Message_Data'] = $message_data;
				}				
				$tdata['Time_Stamp'] = date('Y-m-d H:i:s');
				$tdata['Deployment_Package_Item_Id'] = $packageItemId;	
				
				if($packageId == 0 && $packageItemId > 0)
				{
					$packageId = $this->getPackageId($packageItemId);
				}
				
				if(isset($packageId) && $packageId != '')
				{
					$tdata['Deployment_Package_Id'] = $packageId;
				}
				else
				{
					$tdata['Deployment_Package_Id'] = 0;
				}
				$this->db->insert('Logs',$tdata);
			}	
		}
	}
	
	function getSourceSiteNameByPackageId($packageId)
	{
		$sourceSiteName='';		
		$this->db->select('Source_Site_Name');
		$this->db->from('deployment_package');
		$this->db->where('Deployment_Package_Id',$packageId);
		$query = $this->db->get();
		if($query->num_rows()>0) 
		{
			if(isset($query->result()[0]->Source_Site_Name))
			{
				$sourceSiteName = $query->result()[0]->Source_Site_Name;
			}	
		}
		return $sourceSiteName;	
	}
	
	function getAdditionalLogFlagBySourceSiteName($sourceSiteName)
	{
		$additionalLog='';
		$this->db->select('Additional_Log');
		$this->db->from('instance_');
		$this->db->where('Site_Name',$sourceSiteName);
		$query = $this->db->get();
		if($query->num_rows()>0) 
		{
			if(isset($query->result()[0]->Additional_Log))
			{
				$additionalLog=$query->result()[0]->Additional_Log;
			}	
		}
		return $additionalLog;
	}	
}
?>	