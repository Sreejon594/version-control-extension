<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require(APPPATH.'/third_party/json_path/vendor/autoload.php');
use Flow\JSONPath\JSONPath;
use Flow\JSONPath\JSONPathLexer;
use \Peekmo\JsonPath\JsonPath as PeekmoJsonPath;
use \Peekmo\JsonPath\JsonStore;
class Deploy_Helper_model extends CI_Model
{
	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->load->model('eloqua','',TRUE);
		$this->load->model('deploy_model','',TRUE);
		$this->load->model('logging_model','',TRUE);		
    }
	
	//Method to get CDO JSON from Source & Target
	function CDO_from_DB($customObjectId,$deployment_package_id)
	{
		$this->db->select('JSON_Asset');
		$this -> db -> from('deployment_package_item');
		$this -> db -> where('Deployment_Package_Id',$deployment_package_id);
		$this -> db -> where('Asset_Type','Custom Object');
		$this -> db -> where('Asset_Id',$customObjectId);
		$query = $this -> db -> get();
		$CDO_result=$query->result()[0];
		$source_CDO=json_decode($CDO_result->JSON_Asset);
		$result_CDO['source_CDO']=$source_CDO;
		$result_CDO['target_CDO']=$this->deploy_model->getSourceCDOName($customObjectId,$deployment_package_id);
		return $result_CDO;
	}
	
	function CDO_FieldMerge($JSON_Submitted,$instance,$Package_Item)
	{
		$JSON_Submitted = json_decode($JSON_Submitted);
		if((!empty($JSON_Submitted->type)) && $JSON_Submitted->type=='FieldMerge')
		{	
			if(!empty($JSON_Submitted->fieldConditions))
			{
				$CDO_DB = $this->CDO_from_DB($JSON_Submitted->customObjectId,$Package_Item->Deployment_Package_Id);
				
				$source_CDO_Fields =$CDO_DB['source_CDO'];
				$target_CDO_Fields =$CDO_DB['target_CDO'];
				
				$target_CDO_id=$target_CDO_Fields->id;
				if(isset($JSON_Submitted->customObjectId))
				{
					$JSON_Submitted->customObjectId=$target_CDO_id;
					$result = $JSON_Submitted;
				}	
				if(isset($JSON_Submitted->customObjectFieldId))
				{
					foreach($source_CDO_Fields->fields as $CDOkey=>$CDOval)
					{
						if($CDOval->id==$JSON_Submitted->customObjectFieldId)
						{
							$find_in_targetCDO=$CDOval->name;
						}	
					}
					foreach($target_CDO_Fields->fields as $CDOkey1=>$CDOval1)
					{
						if($CDOval1->name==$find_in_targetCDO)
						{
							$target_CDOField_id=$CDOval1->id;
						}
					}
					$JSON_Submitted->customObjectFieldId=$target_CDOField_id;
					$result = $JSON_Submitted;
				}
				if(isset($JSON_Submitted->fieldConditions))
				{
					foreach($source_CDO_Fields->fields as $CDOkey3=>$CDOval3)
					{
						if($CDOval3->id==$JSON_Submitted->fieldConditions[0]->fieldId)
						{
							$find_in_targetCDO=$CDOval3->name;
						}	
					}
					foreach($target_CDO_Fields->fields as $CDOkey4=>$CDOval4)
					{
						if($CDOval4->name==$find_in_targetCDO)
						{
							$target_CDOField_id=$CDOval4->id;
						}
					}
					$JSON_Submitted->fieldConditions[0]->fieldId=$target_CDOField_id;
					$result = $JSON_Submitted;
				}
				
			}
			else
			{
				if(isset($JSON_Submitted->customObjectId))
				{
					$CDO_DB = $this->CDO_from_DB($JSON_Submitted->customObjectId,$Package_Item->Deployment_Package_Id);
					
					$source_CDO_Fields =$CDO_DB['source_CDO'];
					$target_CDO_Fields =$CDO_DB['target_CDO'];
		
					$target_CDO_id=$target_CDO_Fields->id;
					if(isset($JSON_Submitted->customObjectId))
					{
						$JSON_Submitted->customObjectId=$target_CDO_id;
						$result = $JSON_Submitted;
					}	
					if(isset($JSON_Submitted->customObjectFieldId))
					{
						foreach($source_CDO_Fields->fields as $CDOkey=>$CDOval)
						{
							if($CDOval->id==$JSON_Submitted->customObjectFieldId)
							{
								$find_in_targetCDO=$CDOval->name;
							}	
						}
						foreach($target_CDO_Fields->fields as $CDOkey1=>$CDOval1)
						{
							if($CDOval1->name==$find_in_targetCDO)
							{
								$target_CDOField_id=$CDOval1->id;
							}
						}
						$JSON_Submitted->customObjectFieldId=$target_CDOField_id;
						$result = $JSON_Submitted;
					}
				}
				else
				{
					$result = $JSON_Submitted;
				}		
			}
		}
		else
		{
			$result = $JSON_Submitted;
			
		}
		return json_encode($result);	
	}
	
	function signatureRule_CDO($JSON_Submitted,$instance,$Package_Item)
	{
		$JSON_Submitted = json_decode($JSON_Submitted);
		if((!empty($JSON_Submitted->type)) && $JSON_Submitted->type=='EmailSignatureRule' && isset($JSON_Submitted->customObjectId))
		{	
			$CDO_DB = $this->CDO_from_DB($JSON_Submitted->customObjectId,$Package_Item->Deployment_Package_Id);
			$source_CDO_Fields =$CDO_DB['source_CDO'];
			$target_CDO_Fields =$CDO_DB['target_CDO'];
			
			$target_CDO_id=$target_CDO_Fields->id;
			if(isset($JSON_Submitted->customObjectId))
			{
				$JSON_Submitted->customObjectId=$target_CDO_id;
				$result = $JSON_Submitted;
			}	
			if(isset($JSON_Submitted->customObjectFieldId))
			{
				foreach($source_CDO_Fields->fields as $CDOkey=>$CDOval)
				{
					if($CDOval->id==$JSON_Submitted->customObjectFieldId)
					{
						$find_in_targetCDO=$CDOval->name;
					}	
				}
				foreach($target_CDO_Fields->fields as $CDOkey1=>$CDOval1)
				{
					if($CDOval1->name==$find_in_targetCDO)
					{
						$target_CDOField_id=$CDOval1->id;
					}
				}
				$JSON_Submitted->customObjectFieldId=$target_CDOField_id;
				$result = $JSON_Submitted;
			}
		}
		else
		{
			$result = $JSON_Submitted;
			
		}
		return json_encode($result);	
	}
	
	function CDO_DynamicContent($JSON_Submitted,$instance,$Package_Item)
	{
		$JSON_Submitted = json_decode($JSON_Submitted);
		$dynamicCDO_Flag=0;
		$t=0;
		$source_CDO_replace_element=array();
		$source_CDO_replace_element_temp=array();
		if((!empty($JSON_Submitted)) && $JSON_Submitted->type=='DynamicContent')
		{
			if(isset($JSON_Submitted->rules) && count($JSON_Submitted->rules)>0)
			{
				$dynamicContent_elements_temp=$JSON_Submitted->rules;
				foreach($JSON_Submitted->rules as $dKey => $dval)
				{
					foreach($dval->criteria as $dKey1 => $dval1)
					{
						if($dval1->type=='LinkedCustomObjectCriterion')
						{	
							foreach($dval1 as $dKey2=>$dVal2)
							{
								if($dKey2=='customObjectId')
								{
									$source_CDO_replace_element_temp['customObjectId']=$dVal2;
								}
								if($dKey2=='fieldConditions')
								{
									foreach($dVal2 as $dKey3=>$dVal3)
									{
										if(isset($dVal3->fieldId))
										{
											$source_CDO_replace_element_temp['fieldConditions']=$dVal3->fieldId;
										}	
									}	
									
								}	
							}
							$source_CDO_replace_element[$t++]=$source_CDO_replace_element_temp;
							$dynamicCDO_Flag=1;
						}	
					} 
				}
			}
			if($dynamicCDO_Flag==1)
			{
				$k=0;
				foreach($source_CDO_replace_element as $psKey6=>$psVal6)
				{
					$target_fields_ids_dynamicContent[$k++]=$this->get_cdo_field_dynamicContent($psVal6['customObjectId'],$psVal6['fieldConditions'],$Package_Item->Deployment_Package_Id);
				}
				$replaced_fields_dynamicContent=$this->replace_dynamicContent_CDO_field($target_fields_ids_dynamicContent,$dynamicContent_elements_temp);
				$JSON_Submitted->rules = $replaced_fields_dynamicContent;
				$JSON_Submitted_temp=$this->statementNegative_dynamicContent($JSON_Submitted);
				$result = $JSON_Submitted_temp;
			}	
			else
			{	
				$JSON_Submitted_temp=$this->statementNegative_dynamicContent($JSON_Submitted);
				$result=$JSON_Submitted;
			}	
		}	
		else
		{
			$result = $JSON_Submitted;
		}
		return json_encode($result);
	}
	
	function statementNegative_dynamicContent($JSON_Submitted)
	{
		$JSON_Submitted_temp=$JSON_Submitted;
		foreach($JSON_Submitted_temp->rules as $sKey1=>$sVal1)
		{
			$statement_temp=explode(" ",$sVal1->statement);
			foreach($statement_temp as $sKey2=>$sVal2)
			{
				if (strpos($sVal2, '(') !== false) 
				{
					$sVal2=str_replace('(','(-',$sVal2);
					$statement_temp[$sKey2]=$sVal2;
				}	
				else if (strpos($sVal2, ')') !== false)
				{
					$sVal2=str_replace($sVal2,'-'.$sVal2,$sVal2);
					$statement_temp[$sKey2]=$sVal2;
				}	
				else
				{
					if(is_numeric($sVal2))
					{
						$sVal2=str_replace($sVal2,'-'.$sVal2,$sVal2);
						$statement_temp[$sKey2]=$sVal2;
					}
				}	
			}
			$statement_temp = implode(" ",$statement_temp);
			// if(strpos($statement_temp, '(-(-') !== false)
			// {
				// $statement_temp_modify=str_replace('(-(-','((-',$statement_temp);
				// $statement_temp=$statement_temp_modify;
			// }
			$sVal1->statement= $statement_temp;
		}
		return $JSON_Submitted_temp;
	}
	
	function statementNegative_contactFilter($JSON_Submitted)
	{
		$JSON_Submitted_temp = json_decode($JSON_Submitted);
		// print_r($JSON_Submitted_temp->type);
		
		if($JSON_Submitted_temp->type=='ContactFilter')
		{
			$statement_temp=explode(" ",$JSON_Submitted_temp->statement);
			foreach($statement_temp as $sKey2=>$sVal2)
			{
				if (strpos($sVal2, '(') !== false) 
				{
					$sVal2=str_replace('(','(-',$sVal2);
					$statement_temp[$sKey2]=$sVal2;
				}	
				else if (strpos($sVal2, ')') !== false)
				{
					$sVal2=str_replace($sVal2,'-'.$sVal2,$sVal2);
					$statement_temp[$sKey2]=$sVal2;
				}	
				else
				{
					if(is_numeric($sVal2))
					{
						$sVal2=str_replace($sVal2,'-'.$sVal2,$sVal2);
						$statement_temp[$sKey2]=$sVal2;
					}
				}	
			}
			$statement_temp = implode(" ",$statement_temp);
			if(strpos($statement_temp, '(-(-') !== false)
			{
				$statement_temp_modify=str_replace('(-(-','((-',$statement_temp);
				$statement_temp=$statement_temp_modify;
			}
			$JSON_Submitted_temp->statement= $statement_temp;
			$result=$JSON_Submitted_temp;
			//print_r($JSON_Submitted_temp);
		}
		else
		{
			$result=$JSON_Submitted_temp;	
		}	
		return json_encode($result);
	}
	
	function get_cdo_field_dynamicContent($customObjectId,$fieldConditions,$Package_Id)
	{
		$CDO_DB = $this->CDO_from_DB($customObjectId,$Package_Id);
		$source_CDO=$CDO_DB['source_CDO'];
		$target_CDO=$CDO_DB['target_CDO'];
		
		foreach($source_CDO->fields as $sKey=>$sVal)
		{
			if($sVal->id==$fieldConditions)
			{
				$source_fields_name=$sVal->name;
				$source_fields_id[$sVal->name]=$fieldConditions;
			}	
		}
		foreach($target_CDO->fields as $tKey=>$tVal)
		{
			if($tVal->name==$source_fields_name)
			{
				$target_fields_id[$tVal->name]=$tVal->id;
			}
			
		}	
		$new_target_fields_id = array(); 
		$t=0;
		foreach ($target_fields_id as $iKey => $iVal)
		{
			foreach ($source_fields_id as $iKey1 => $iVal1)
			{
				if($iKey==$iKey1)
				{
					$new_target_fields_id[$iVal1] = $iVal;
				}	
			}
		}
		$tCDO[$source_CDO->id] = $target_CDO->id;
		$tCDO['fieldConditions'] = $new_target_fields_id;
		return $tCDO;
	}
	
	function replace_dynamicContent_CDO_field($target_fields_ids_dynamicContent,$dynamicContent_elements_temp)
	{
		foreach($dynamicContent_elements_temp as $dKey=>$dVal)
		{
			foreach($dVal->criteria as $dKey1=>$dVal1)
			{
				if($dVal1->type=='LinkedCustomObjectCriterion')
				{
					foreach($target_fields_ids_dynamicContent as $dKey2=>$dVal2)
					{
						foreach($dVal2 as $dKey3=>$dVal3)
						{
							if($dVal1->customObjectId==$dKey3)
							{
								$dVal1->customObjectId=$dVal3;
							}
							if(isset($dVal1->fieldConditions) && $dKey3=='fieldConditions')
							{
								foreach($dVal1->fieldConditions as $dKey4=>$dVal4)
								{
									foreach($dVal3 as $dKey5=>$dVal5)
									{
										if($dVal4->fieldId==$dKey5)
										{
											$dVal4->fieldId=$dVal5;
										}
									}	
								}		
							}	
						}
					}
				}
			}
		}
		return $dynamicContent_elements_temp;
	}

	function CDO_Form_PS_Form_Data($JSON_Submitted,$instance,$Package_Item)
	{
		$JSON_Submitted = json_decode($JSON_Submitted);
		$source_CDO_replace_element = array();
		$source_CDO_replace_element_temp=array();
		$j=0;
		$CDO_Update_Flag=0;
		if($JSON_Submitted->type=='Form')
		{	
			$form_processingSteps = $JSON_Submitted->processingSteps;
			$form_processingSteps_temp = $form_processingSteps;
			foreach($form_processingSteps as $psKey1=>$psVal1)
			{
				if($psVal1->type=='FormStepCreateUpdateCustomObjectFromFormField' && isset($psVal1->customObjectId))
				{
					foreach($psVal1 as $psKey2=>$psVal2)
					{
						if($psKey2=='keyFieldId')
						{
							$source_CDO_replace_element_temp['keyFieldId']=$psVal2;
						}
						if($psKey2=='customObjectId' )
						{
							$source_CDO_replace_element_temp['customObjectId']=$psVal2;
						}
						if($psKey2=='uniqueFieldId')
						{
							$source_CDO_replace_element_temp['uniqueFieldId']=$psVal2;
						}
						if($psKey2=='mappings')
						{
							$i=0;
							foreach($psVal2 as $psKey3=>$psVal3)
							{
								if(isset($psVal3->targetEntityFieldId))
								{
									$source_CDO_replace_element_temp['mappings'][$i++]=$psVal3->targetEntityFieldId;
								}
							}
						}
					}
					$source_CDO_replace_element[$j++] = $source_CDO_replace_element_temp;
					$CDO_Update_Flag=1;
				}
			}
			if($CDO_Update_Flag==1)	
			{
				$k=0;
				$target_fields_ids=array();
				foreach($source_CDO_replace_element as $psKey4=>$psVal4)
				{
					$target_fields_ids[$k++]=$this->get_cdo_field_CDO_Form_PS_Form_Data($psVal4['customObjectId'],$psVal4['keyFieldId'],$psVal4['uniqueFieldId'],$psVal4['mappings'],$Package_Item->Deployment_Package_Id);
				} 
				$replaced_fields_FormData=$this->replace_Form_PS_CDO_field_Form_Data($target_fields_ids,$form_processingSteps_temp);
				$JSON_Submitted->processingSteps = $replaced_fields_FormData;
				$result = $JSON_Submitted;
			}
			else 
			{
				$result = $JSON_Submitted;
			}	
		} 
		else
		{
			$result = $JSON_Submitted;
		}
		return json_encode($result);	
	}
	
	function get_cdo_field_CDO_Form_PS_Form_Data($customObjectId,$keyFieldId,$uniqueFieldId,$source_CDO_mapping,$Package_Id)
	{
		$CDO_DB = $this->CDO_from_DB($customObjectId,$Package_Id);
		$source_CDO=$CDO_DB['source_CDO'];
		$target_CDO=$CDO_DB['target_CDO'];
	
		//Get CDO source Field Name
		foreach($source_CDO->fields as $sKey=>$sVal)
		{
			foreach($source_CDO_mapping as $sKey1=>$sVal1)
			{
				if($sVal->id==$sVal1)
				{
					$source_fields_name[]=$sVal->name;
					$source_fields_id[$sVal->name]=$sVal1;
				}	
			}
			if($sVal->id==$keyFieldId)
			{
				$keyFieldId_source_name=$sVal->name;
			}	
			
			if($sVal->id==$uniqueFieldId)
			{
				$uniqueFieldId_source_name=$sVal->name;
			}	
		}
		foreach($target_CDO->fields as $tKey=>$tVal)
		{
			foreach($source_fields_name as $tKey1=>$tVal1)
			{
				if($tVal->name==$tVal1)
				{
					$target_fields_id[$tVal->name]=$tVal->id;
				}
			}
			
			if($tVal->name==$keyFieldId_source_name)
			{
				$keyFieldId_target_id=$tVal->id;
			}
			
			if($tVal->name==$uniqueFieldId_source_name)
			{
				$uniqueFieldId_target_id=$tVal->id;
			}
		}
		$new_target_fields_id = array(); 
		$t=0;
		foreach ($target_fields_id as $iKey => $iVal)
		{
			foreach ($source_fields_id as $iKey1 => $iVal1)
			{
				if($iKey==$iKey1)
				{
					$new_target_fields_id[$iVal1] = $iVal;
				}	
			}
		}
		$tCDO[$keyFieldId] = $keyFieldId_target_id;
		$tCDO['mappings'] = $new_target_fields_id;
		$tCDO[$uniqueFieldId] = $uniqueFieldId_target_id;
		$tCDO[$source_CDO->id] = $target_CDO->id;
		return $tCDO;
	}
	
	function replace_Form_PS_CDO_field_Form_Data($target_fields_ids,$form_processingSteps_temp)
	{
		foreach($form_processingSteps_temp as $rKey=>$rVal)
		{
			if($rVal->type=='FormStepCreateUpdateCustomObjectFromFormField')
			{
				foreach($target_fields_ids as $vKey1=>$rVal1)
				{
					foreach($rVal1 as $rKey3=>$rVal3)
					{
						if($rKey3==$rVal->keyFieldId)
						{
							$rVal->keyFieldId=$rVal3;
						}
						if($rKey3==$rVal->customObjectId)
						{
							$rVal->customObjectId=$rVal3;
						}	
						if($rKey3==$rVal->uniqueFieldId)
						{
							$rVal->uniqueFieldId=$rVal3;
						}
						if(isset($rVal->mappings) && $rKey3=='mappings')
						{
							foreach($rVal->mappings as $rKey4=>$rVal4)
							{
								if(isset($rVal4->targetEntityFieldId))
								{
									foreach($rVal3 as $rKey6=>$rVal6)
									{
										if($rKey6==$rVal4->targetEntityFieldId)
										{
											$rVal4->targetEntityFieldId=$rVal6;
										}	
									}	
									
								}
							}
						}	
					}
				}
			}
		}
		return $form_processingSteps_temp;
	}
	
	function get_cdo_field_CDO_Form_PS_Custom_Value($customObjectId,$updateRules,$keyFieldMapping,$Package_Id)
	{
		$CDO_DB = $this->CDO_from_DB($customObjectId,$Package_Id);
		$source_CDO=$CDO_DB['source_CDO'];
		$target_CDO=$CDO_DB['target_CDO'];
		
		//Get CDO source Field Name
		foreach($source_CDO->fields as $sKey=>$sVal)
		{
			foreach($updateRules as $sKey1=>$sVal1)
			{
				if($sVal->id==$sVal1)
				{
					$source_fields_name[]=$sVal->name;
					$source_fields_id[$sVal->name]=$sVal1;
				}
				if($sVal->id==$keyFieldMapping)
				{
					$keyFieldMapping_source_name=$sVal->name;
				}
			}	
		}
		//Get CDO target Field Id
		foreach($target_CDO->fields as $tKey=>$tVal)
		{
			foreach($source_fields_name as $tKey1=>$tVal1)
			{
				if($tVal->name==$tVal1)
				{
					$target_fields_id[$tVal->name]=$tVal->id;
				}
				if($tVal->name==$keyFieldMapping_source_name)
				{
					$keyFieldMapping_target_id=$tVal->id;
				}
			}
		}
		$new_target_fields_id = array(); 
		$t=0;
		foreach ($target_fields_id as $iKey => $iVal)
		{
			foreach ($source_fields_id as $iKey1 => $iVal1)
			{
				if($iKey==$iKey1)
				{
					$new_target_fields_id[$iVal1] = $iVal;
				}	
			}
		}
		$tCDO[$source_CDO->id] = $target_CDO->id;
		$tCDO['updateRules'] = $new_target_fields_id;
		$tCDO[$keyFieldMapping] = $keyFieldMapping_target_id;
		return $tCDO;	
	}
	
	function CDO_Form_PS_CustomValue($JSON_Submitted,$instance,$Package_Item)
	{
		$JSON_Submitted = json_decode($JSON_Submitted);
		$source_CDO_replace_element_customValue = array();
		$source_CDO_replace_element_customValue_temp=array();
		$t=0;
		$CDO_Update_Flag1=0;
		 
		if($JSON_Submitted->type=='Form')
		{
			$form_processingSteps_customValue = $JSON_Submitted->processingSteps;
			$form_processingSteps_temp_customValue = $form_processingSteps_customValue;
			foreach($form_processingSteps_customValue as $psKey1=>$psVal1)
			{
				if($psVal1->type=='FormStepCreateUpdateCustomObject')
				{
					foreach($psVal1 as $psKey2=>$psVal2)
					{
						if($psKey2=='customObjectId')
						{
							$source_CDO_replace_element_customValue_temp['customObjectId']=$psVal2;
						}
						if($psKey2=='customObjectUpdateRuleSet')
						{
							foreach($psVal2 as $psKey3=>$psVal3)
							{
								if($psKey3=='updateRules')
								{
									$i=0;
									foreach($psVal3 as $psKey4=>$psVal4)
									{
										if(isset($psVal4->targetFieldId))
										{
											$source_CDO_replace_element_customValue_temp['updateRules'][$i++]=$psVal4->targetFieldId;
										}
									}
								}	
							}
						}
						if($psKey2=='keyFieldMapping')
						{
							foreach($psVal2 as $psKey5=>$psVal5)
							{
								if($psKey5=='targetEntityFieldId')
								{
									$source_CDO_replace_element_customValue_temp['targetEntityFieldId']=$psVal5;
								}
							}	
						}
					}
					$source_CDO_replace_element_customValue[$t++] = $source_CDO_replace_element_customValue_temp;	
					$CDO_Update_Flag1=1;
				}
			}
			if($CDO_Update_Flag1==1)
			{
				$k=0;
				foreach($source_CDO_replace_element_customValue as $psKey6=>$psVal6)
				{
					$target_fields_ids_customValue[$k++]=$this->get_cdo_field_CDO_Form_PS_Custom_Value($psVal6['customObjectId'],$psVal6['updateRules'],$psVal6['targetEntityFieldId'],$Package_Item->Deployment_Package_Id);
					
				}
				$replaced_fields_customValue=$this->replace_Form_PS_CDO_field_customValue($target_fields_ids_customValue,$form_processingSteps_temp_customValue);
				$JSON_Submitted->processingSteps = $replaced_fields_customValue;
				$result = $JSON_Submitted;
			}
			else 
			{
				$result = $JSON_Submitted;
			}	
		} 
		else
		{
			$result = $JSON_Submitted;
		}
		return json_encode($result);
	}

	function replace_Form_PS_CDO_field_customValue($target_fields_ids_customValue,$form_processingSteps_temp_customValue)
	{
		foreach($form_processingSteps_temp_customValue as $rKey=>$rVal)
		{
			if($rVal->type=='FormStepCreateUpdateCustomObject')
			{
				foreach($target_fields_ids_customValue as $rKey1=>$rVal1)
				{
					foreach($rVal1 as $rKey2=>$rVal2)
					{
						if($rKey2==$rVal->customObjectId)
						{
							$rVal->customObjectId=$rVal2;
						}
						if(isset($rVal->keyFieldMapping))
						{
							if(isset($rVal->keyFieldMapping->targetEntityFieldId))
							{
								if($rKey2==$rVal->keyFieldMapping->targetEntityFieldId)
								{
									$rVal->keyFieldMapping->targetEntityFieldId=$rVal2;
								}
							}		
						}
						if(isset($rVal->customObjectUpdateRuleSet->updateRules))
						{
							foreach($rVal->customObjectUpdateRuleSet->updateRules as $rKey3=>$rVal3)
							{
								if($rKey2=='updateRules')
								{
									foreach($rVal2 as $rKey4=>$rVal4)
									{
										if($rVal3->targetFieldId==$rKey4)
										{
											$rVal3->targetFieldId=$rVal4;
										}	
									}	
								}
							}
						}	
					}
				}
			}
		}
		return $form_processingSteps_temp_customValue;
	}
	
	function CDO_Segment($JSON_Submitted,$Package_Item)
	{
		$JSON_Submitted = json_decode($JSON_Submitted);
		$source_CDO_replace_element = array();
		$source_CDO_replace_element_temp=array();
		$t=0;
		$CDO_Update_Flag2=0;
		 
		if($JSON_Submitted->type=='ContactSegment')
		{
			$segment_elements = $JSON_Submitted->elements;
			$segment_elements_temp = $segment_elements;
			foreach($segment_elements_temp as $sKey=>$sVal)
			{
				if(isset($sVal->filter))
				{
					foreach($sVal->filter->criteria as $sKey1=>$sVal1 )
					{
						if($sVal1->type=='LinkedAccountCustomObjectCriterion' || $sVal1->type=='LinkedCustomObjectCriterion')
						{
							foreach($sVal1 as $sKey2=>$sVal2)
							{
								if($sKey2=='customObjectId')
								{
									$source_CDO_replace_element_temp['customObjectId']=$sVal2;
								}
								if($sKey2=='fieldConditions')
								{
									$i=0;
									foreach($sVal2 as $sKey3=>$sVal3)
									{
										if(isset($sVal3->fieldId))
										{
											$source_CDO_replace_element_temp['fieldConditions'][$i++]=$sVal3->fieldId;
										}
									}	
									
								}	
								
							}
							$source_CDO_replace_element[$t++] = $source_CDO_replace_element_temp;	
							$CDO_Update_Flag2=1;
						}
						
					}
				}	
			}
			if($CDO_Update_Flag2==1)
			{
				$k=0;
				foreach($source_CDO_replace_element as $psKey6=>$psVal6)
				{
					$target_fields_ids_segment[$k++]=$this->get_cdo_field_segment($psVal6['customObjectId'],$psVal6['fieldConditions'],$Package_Item->Deployment_Package_Id);
					
				}
				$replaced_fields_segment=$this->replace_segment_CDO_field($target_fields_ids_segment,$segment_elements_temp);
				$JSON_Submitted->elements = $replaced_fields_segment;
				$JSON_Submitted = $this->statementNegative($JSON_Submitted );
				$result = $JSON_Submitted;
			}
			else 
			{
				$JSON_Submitted = $this->statementNegative($JSON_Submitted );
				$result = $JSON_Submitted;
			}
		}
		else
		{
			$result=$JSON_Submitted;
		}
		return json_encode($result);	
	}
	
	function statementNegative($JSON_Submitted)
	{
		$JSON_Submitted_temp=$JSON_Submitted;
		foreach($JSON_Submitted_temp->elements as $sKey1=>$sVal1)
		{
			if(isset($sVal1->filter))
			{
				$statement_temp=explode(" ",$sVal1->filter->statement);
				foreach($statement_temp as $sKey2=>$sVal2)
				{
					if (strpos($sVal2, '(') !== false) 
					{
						$sVal2=str_replace('(','(-',$sVal2);
						$statement_temp[$sKey2]=$sVal2;
					}	
					else if (strpos($sVal2, ')') !== false)
					{
						$sVal2=str_replace($sVal2,'-'.$sVal2,$sVal2);
						$statement_temp[$sKey2]=$sVal2;
					}	
					else
					{
						if(is_numeric($sVal2))
						{
							$sVal2=str_replace($sVal2,'-'.$sVal2,$sVal2);
							$statement_temp[$sKey2]=$sVal2;
						}
					}	
				}
				$statement_temp = implode(" ",$statement_temp);
				if(strpos($statement_temp, '(-(-') !== false)
				{
					$statement_temp_modify=str_replace('(-(-','((-',$statement_temp);
					$statement_temp=$statement_temp_modify;
				}
				$sVal1->filter->statement= $statement_temp;
			}	
		}
		return $JSON_Submitted_temp;
	}
	
	function replace_segment_CDO_field($target_fields_ids_segment,$segment_elements_temp)
	{
		foreach($segment_elements_temp as $sKey=>$sVal)
		{
			foreach($sVal->filter->criteria as $sKey1=>$sVal1 )
			{
				if($sVal1->type=='LinkedAccountCustomObjectCriterion' || $sVal1->type=='LinkedCustomObjectCriterion')
				{
					foreach($target_fields_ids_segment as $sKey2=>$sVal2)
					{
						foreach($sVal2 as $sKey3=>$sVal3)
						{
							if($sVal1->customObjectId==$sKey3)
							{
								$sVal1->customObjectId=$sVal3;
							}
							if(isset($sVal1->fieldConditions))
							{
								if($sKey3=='fieldConditions')
								{
									foreach($sVal3 as $sKey4=>$sVal4)
									{
										foreach($sVal1->fieldConditions as $sKey5=>$sVal5)
										{
											if($sVal5->fieldId==$sKey4)
											{
												$sVal5->fieldId=$sVal4;
											}	
										}
									}
								}
							}	
						}
					}
				}
			}
		}
		return $segment_elements_temp;
	}
	
	function get_cdo_field_segment($customObjectId,$fieldConditions,$Package_Id)
	{
		$CDO_DB = $this->CDO_from_DB($customObjectId,$Package_Id);
		$source_CDO=$CDO_DB['source_CDO'];
		$target_CDO=$CDO_DB['target_CDO'];
		
		foreach($source_CDO->fields as $sKey=>$sVal)
		{
			foreach($fieldConditions as $sKey1=>$sVal1)
			{
				if($sVal->id==$sVal1)
				{
					$source_fields_name[]=$sVal->name;
					$source_fields_id[$sVal->name]=$sVal1;
				}
			}	
		}
		foreach($target_CDO->fields as $tKey=>$tVal)
		{
			foreach($source_fields_name as $tKey1=>$tVal1)
			{
				if($tVal->name==$tVal1)
				{
					$target_fields_id[$tVal->name]=$tVal->id;
				}
			}
		}
		$new_target_fields_id = array(); 
		$t=0;
		foreach ($target_fields_id as $iKey => $iVal)
		{
			foreach ($source_fields_id as $iKey1 => $iVal1)
			{
				if($iKey==$iKey1)
				{
					$new_target_fields_id[$iVal1] = $iVal;
				}	
			}
		}
		$tCDO[$source_CDO->id] = $target_CDO->id;
		$tCDO['fieldConditions'] = $new_target_fields_id;
		return $tCDO;
	}
	
	function CDO_Campaign($JSON_Submitted,$Package_Item)
	{
		$JSON_Submitted = json_decode($JSON_Submitted);
		$source_CDO_replace_element = array();
		$source_CDO_replace_element_temp=array();
		$t=0;
		$CDO_Campaign_Flag2=0;
		if($JSON_Submitted->type=='Campaign')
		{
			if(isset($JSON_Submitted->elements))
			{
				$campaign_elements = $JSON_Submitted->elements;
				$campaign_elements_temp = $campaign_elements;
				foreach($campaign_elements_temp as $cKey=>$cVal)
				{
					if($cVal->type=='CampaignCustomObjectFieldComparisonRule')
					{
						foreach($cVal as $cKey1=>$cVal1)
						{
							if($cKey1=='customObjectId')
							{
								$source_CDO_replace_element_temp['customObjectId']=$cVal1;
							}
							if($cKey1=='fieldCondition')
							{
								foreach($cVal1 as $cKey2=>$cVal2)
								{
									if($cKey2=='fieldId')
									{
										$source_CDO_replace_element_temp['fieldCondition']=$cVal2;
									}	
								}	
								
							}	
						}
						$source_CDO_replace_element[$t++]=$source_CDO_replace_element_temp;
						$CDO_Campaign_Flag2=1;
					}
					
				}
			}	
			if($CDO_Campaign_Flag2==1)
			{
				$k=0;
				foreach($source_CDO_replace_element as $psKey6=>$psVal6)
				{
					$target_fields_ids_campaign[$k++]=$this->get_cdo_field_camapign($psVal6['customObjectId'],$psVal6['fieldCondition'],$Package_Item->Deployment_Package_Id);
					
				}
				$replaced_fields_campaign=$this->replace_campaign_CDO_field($target_fields_ids_campaign,$campaign_elements_temp);
				$JSON_Submitted->elements = $replaced_fields_campaign;
				$result = $JSON_Submitted;
			}
			else
			{
				$result=$JSON_Submitted;	
			}	
		}
		else
		{
			$result=$JSON_Submitted;
		}
		return json_encode($result);
	}
	
	function get_cdo_field_camapign($customObjectId,$fieldCondition,$Package_Id)
	{
		$CDO_DB = $this->CDO_from_DB($customObjectId,$Package_Id);
		$source_CDO=$CDO_DB['source_CDO'];
		$target_CDO=$CDO_DB['target_CDO'];
		
		//Get CDO source Field Name
		foreach($source_CDO->fields as $sKey=>$sVal)
		{
			if($sVal->id==$fieldCondition)
			{
				$source_fields_name=$sVal->name;
				$source_fields_id[$sVal->name]=$fieldCondition;
			}	
		}
		foreach($target_CDO->fields as $tKey=>$tVal)
		{
			if($tVal->name==$source_fields_name)
			{
				$target_fields_id[$tVal->name]=$tVal->id;
			}
			
		}	
		$new_target_fields_id = array(); 
		$t=0;
		foreach ($target_fields_id as $iKey => $iVal)
		{
			foreach ($source_fields_id as $iKey1 => $iVal1)
			{
				if($iKey==$iKey1)
				{
					$new_target_fields_id[$iVal1] = $iVal;
				}	
			}
		}
		$tCDO[$source_CDO->id] = $target_CDO->id;
		$tCDO['fieldCondition'] = $new_target_fields_id;
		return $tCDO;
	}
	
	function replace_campaign_CDO_field($target_fields_ids_campaign,$campaign_elements_temp)
	{
		foreach($campaign_elements_temp as $cKey=>$cVal)
		{
			if($cVal->type=='CampaignCustomObjectFieldComparisonRule')
			{
				foreach($target_fields_ids_campaign as $cKey1=>$cVal1)
				{
					foreach($cVal1 as $cKey2=>$cVal2)
					{
						if($cVal->customObjectId==$cKey2)
						{
							$cVal->customObjectId=$cVal2;
						}
						if($cVal->fieldCondition)
						{
							if($cKey2=='fieldCondition')
							{
								foreach($cVal2 as $cKey3=>$cVal3)
								{
									if($cVal->fieldCondition->fieldId==$cKey3)
									{
										$cVal->fieldCondition->fieldId=$cVal3;
									}	
								}
							}	
						}	
					}
				}	
				
			}	
		}
		return $campaign_elements_temp;
	}
	
	function handleHTMLChanges($temp__Submitted,$package_validation_list)
	{
		$temp__Submitted_temp = json_decode($temp__Submitted);
		$temp__Submitted_temp1 = json_decode($temp__Submitted,true);
		$package_validation_list = json_encode($package_validation_list);
		echo '<pre>';
		if($temp__Submitted_temp->type=='Email'|| $temp__Submitted_temp->type=='LandingPage')
		{	
			$url = 'http://transporter.portqii.com:8080/Test1/SendBackWithElqTypeChanged';
			$data_json =$temp__Submitted.'|||'.$package_validation_list ;
			
			$ch=curl_init();
			$headers = array('Content-Type:text/plain');
			curl_setopt($ch, CURLOPT_URL, $url); 
			curl_setopt($ch, CURLOPT_POST, true); 
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
			curl_setopt($ch, CURLOPT_POSTFIELDS,$data_json);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			$result 		= curl_exec($ch);
			$result_rootId = $this->rootIdReplace($result,$package_validation_list);
			$result_temp = $result_rootId;
		}
		else
		{
			$result_temp=$temp__Submitted;
		}
		return $result_temp;		
	}
	
	function rootIdReplace($JSON_Submitted,$package_validation_list)
	{
		$JSON_Submitted_temp=json_decode($JSON_Submitted,true);
		$package_validation_list=json_decode($package_validation_list,true);
		$replaceArray=array();
		$i=0;
		// echo '<pre>';
		foreach($package_validation_list as $rKey=>$rVal)
		{
			if($rVal['Asset_Type']=='Signature Layout')
			{
				$replaceArray[$i++][$rVal['Asset_Id']]=$rVal['Target_Asset_Id'];
			}
			if($rVal['Asset_Type']=='Dynamic Content')
			{
				$replaceArray[$i++][$rVal['Asset_Id']]=$rVal['Target_Asset_Id'];
			}
			if($rVal['Asset_Type']=='Shared Content')
			{
				$replaceArray[$i++][$rVal['Asset_Id']]=$rVal['Target_Asset_Id'];
			}
			if($rVal['Asset_Type']=='Form')
			{
				$replaceArray[$i++][$rVal['Asset_Id']]=$rVal['Target_Asset_Id'];
			}
		}
		// echo '<pre>';
			// print_r($replaceArray);
		// echo '</pre>';
		if(isset($JSON_Submitted_temp['htmlContent']) && !empty($replaceArray))
		{
			if(isset($JSON_Submitted_temp['htmlContent']['root']))
			{
				$root=$JSON_Submitted_temp['htmlContent']['root'];
				$root_temp=explode(',',$root);

				if(sizeof($root_temp)>0 && strpos($root, 'contentId') !== false)
				{
					foreach($root_temp as $key=>$val)
					{
						if(strpos($val, 'contentRecordType') !== false) 
						{
							if(strpos($val, 'EmailSignatureLayout') !== false) 
							{
								$root_temp_key_1stepBack= explode(':',$root_temp[$key-1]);
								foreach($replaceArray as $key2=>$val2)
								{
									$root_temp_key_1stepBack[1]=str_replace('"','',$root_temp_key_1stepBack[1]);
									foreach($val2 as $key3=>$val3)
									{
										if($root_temp_key_1stepBack[1]==$key3) 
										{
											$root_temp_key_1stepBack[1]=' "'.$val3.'"';
										}
									}		
								}
								$root_temp_key_1stepBack_=implode(':',$root_temp_key_1stepBack);
								$root_temp[$key-1]=$root_temp_key_1stepBack_;
							}
							if(strpos($val, 'ContentSection') !== false) 
							{
								$root_temp_key_1stepBack= explode(':',$root_temp[$key-1]);
								foreach($replaceArray as $key2=>$val2)
								{
									$root_temp_key_1stepBack[1]=str_replace('"','',$root_temp_key_1stepBack[1]);
									foreach($val2 as $key3=>$val3)
									{
										if($root_temp_key_1stepBack[1]==$key3) 
										{
											$root_temp_key_1stepBack[1]=' "'.$val3.'"';
										}
									}		
								}
								$root_temp_key_1stepBack_=implode(':',$root_temp_key_1stepBack);
								$root_temp[$key-1]=$root_temp_key_1stepBack_;
							}
							if(strpos($val, 'DynamicContent') !== false) 
							{
								$root_temp_key_1stepBack= explode(':',$root_temp[$key-1]);
								foreach($replaceArray as $key2=>$val2)
								{
									$root_temp_key_1stepBack[1]=str_replace('"','',$root_temp_key_1stepBack[1]);
									foreach($val2 as $key3=>$val3)
									{
										if($root_temp_key_1stepBack[1]==$key3) 
										{
											$root_temp_key_1stepBack[1]=' "'.$val3.'"';
										}
									}		
								}
								$root_temp_key_1stepBack_=implode(':',$root_temp_key_1stepBack);
								$root_temp[$key-1]=$root_temp_key_1stepBack_;
							}	
						}
						else if(strpos($root, 'FormView') !== false)
						{
							if(strpos($val, 'contentId') !== false)
							{
								$root_temp_key_1stepBack= explode(':',$root_temp[$key]);
								foreach($replaceArray as $key2=>$val2)
								{
									$root_temp_key_1stepBack[1]=str_replace('"','',$root_temp_key_1stepBack[1]);
									foreach($val2 as $key3=>$val3)
									{
										if($root_temp_key_1stepBack[1]==$key3) 
										{
											$root_temp_key_1stepBack[1]=' "'.$val3.'"';
										}
									}		
								}
								$root_temp_key_1stepBack_=implode(':',$root_temp_key_1stepBack);
								$root_temp[$key]=$root_temp_key_1stepBack_;
							}	
						}	
					}
					$root_temp_=implode(',',$root_temp);
					$JSON_Submitted_temp['htmlContent']['root']=$root_temp_;
					// print_r($JSON_Submitted_temp['htmlContent']['root']=$root_temp_);	
					$result = $JSON_Submitted_temp;
				}
				else
				{
					$result = $JSON_Submitted_temp;
				}	
			}
			else
			{
				$result = $JSON_Submitted_temp;
			}	
		}
		else
		{
			$result = $JSON_Submitted_temp;	
		}
		// print_r($result);
		return json_encode($result);
	}
	
	function elqTrackId_handler($JSON_Submitted,$Package_Item)
	{
		$JSON_Submitted_temp=$JSON_Submitted;
		if (strpos($JSON_Submitted_temp, 'elqTrackId') !== false) 
		{
			$data_json=$JSON_Submitted_temp;
			$url = 'http://transporter.portqii.com:8080/Test1/SendBackWithRemoveElqTrackId';
			$ch=curl_init();
			$headers = array('Content-Type:text/plain');
			curl_setopt($ch, CURLOPT_URL, $url); 
			curl_setopt($ch, CURLOPT_POST, true); 
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
			curl_setopt($ch, CURLOPT_POSTFIELDS,$data_json);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			$result_		= curl_exec($ch);
			//print_r($result_);
			$result=$result_;	
		}
		else
		{
			$result=$JSON_Submitted;
		}	
		return $result;
	}
	
	function JSON_Submitted_unset_campaignInput($JSON_Submitted,$instance,$Package_Item)
	{
		$JSON_Submitted_temp=json_decode($JSON_Submitted);
		
		if($JSON_Submitted_temp->type=='Campaign')
		{
			if(isset($JSON_Submitted_temp->elements))
			{
				foreach($JSON_Submitted_temp->elements as $cKey=>$cVal)
				{
					if($cVal->type=='CampaignInput')
					{
						unset($JSON_Submitted_temp->elements[$cKey]);
						$result=$JSON_Submitted_temp;
					}
					else
					{
						$result=$JSON_Submitted_temp;
					}	
				}
			}
			else
			{
				$result=$JSON_Submitted_temp;
			}	
		}
		else
		{
			$result=$JSON_Submitted_temp;
		}
		return json_encode($result);
	}
	
	function JSON_Submitted_Replace_Target_Element_Id($JSON_Submitted,$instance,$Package_Item)
	{
		$JSON_Submitted_temp=json_decode($JSON_Submitted);
		$campaignInputFlag=false;
		if(strpos($JSON_Submitted, 'CampaignInput') !==false)
		{
			// print_r($JSON_Submitted_temp); echo '<br>';
			if($JSON_Submitted_temp->type=='Campaign' && isset($JSON_Submitted_temp->elements))
			{
				//Enchangement 28-11-2017 ,Array to store connectedId to replace with target connectedId
				$connectedIdArray_global=array();
				foreach($JSON_Submitted_temp->elements as $cKey=>$cVal)
				{
					$campaign_target = $this->getCampaign_targetInstance($Package_Item->Deployment_Package_Id,$JSON_Submitted_temp->type,$Package_Item->Target_Asset_Id);
					// echo 'target campaign';
					// echo '<pre><br>';
						// print_r($campaign_target);
					// echo '</pre>';
					if(isset($campaign_target->elements))
					{
						foreach($campaign_target->elements as $cKey1=>$cVal1)
						{  
							if($cVal1->type == 'CampaignInput')
							{
								$campaignInputFlag=true;
								//Email
								if($cVal1->outputTerminals[0]->connectedType == 'CampaignEmail')
								{	
									foreach($campaign_target->elements as $cKey2=>$cVal2)
									{	
										if($cVal2->type == 'CampaignEmail' && $cVal2->id == $cVal1->outputTerminals[0]->connectedId)
										{
											$emailId = $cVal2->emailId;
											
											foreach($JSON_Submitted_temp->elements as $cKey3=>$cVal3)
											{
												if($cVal3->type == 'CampaignEmail')
												{
													if($cVal3->emailId == $emailId)
													{
														if(isset($cVal1->outputTerminals[0]->connectedId))
														{
															if(abs($cVal3->id) != $cVal1->outputTerminals[0]->connectedId)
															{
																$connectedIdArray_local=array($cVal3->id=>$cVal1->outputTerminals[0]->connectedId);
																array_push($connectedIdArray_global,$connectedIdArray_local);
															}	
															$cVal3->id=$cVal1->outputTerminals[0]->connectedId;
															break;
														}	
													}
												}
												
												// echo '<pre>';
													// print_r($JSON_Submitted_temp);
												// echo '</pre>';
												
											}
										}
									}
								} 
								//Segment
								if($cVal1->outputTerminals[0]->connectedType == 'CampaignSegment')
								{
									foreach($campaign_target->elements as $cKey2=>$cVal2)
									{
										if($cVal2->type == 'CampaignSegment' && $cVal2->id == $cVal1->outputTerminals[0]->connectedId)
										{
											$segmentId = $cVal2->segmentId;
											
											foreach($JSON_Submitted_temp->elements as $cKey3=>$cVal3)
											{
												if($cVal3->type == 'CampaignSegment')
												{
													if($cVal3->segmentId == $segmentId)
													{
														if(isset($cVal1->outputTerminals[0]->connectedId))
														{
															if(abs($cVal3->id) != $cVal1->outputTerminals[0]->connectedId)
															{
																$connectedIdArray_local=array($cVal3->id=>$cVal1->outputTerminals[0]->connectedId);
																array_push($connectedIdArray_global,$connectedIdArray_local);
															}
															$cVal3->id=$cVal1->outputTerminals[0]->connectedId;
															break;
														}	
													}
												}
											}
										}
									}
								}
								//Submitted Form
								if($cVal1->outputTerminals[0]->connectedType == 'CampaignSubmitFormRule')
								{
									foreach($campaign_target->elements as $cKey2=>$cVal2)
									{
										if($cVal2->type == 'CampaignSubmitFormRule' && $cVal2->id == $cVal1->outputTerminals[0]->connectedId)
										{
											$formId = $cVal2->formId;
											foreach($JSON_Submitted_temp->elements as $cKey3=>$cVal3)
											{
												if($cVal3->type == 'CampaignSubmitFormRule')
												{
													if($cVal3->formId == $formId)
													{
														if(isset($cVal1->outputTerminals[0]->connectedId))
														{
															if(abs($cVal3->id) != $cVal1->outputTerminals[0]->connectedId)
															{
																$connectedIdArray_local=array($cVal3->id=>$cVal1->outputTerminals[0]->connectedId);
																array_push($connectedIdArray_global,$connectedIdArray_local);
															}
															$cVal3->id=$cVal1->outputTerminals[0]->connectedId;
															break;
														}	
													}
												}
											}
										}
									}
								}
								//Contact Field
								if($cVal1->outputTerminals[0]->connectedType == 'CampaignContactFieldComparisonRule')
								{
									foreach($campaign_target->elements as $cKey2=>$cVal2)
									{
										if($cVal2->type == 'CampaignContactFieldComparisonRule' && $cVal2->id == $cVal1->outputTerminals[0]->connectedId)
										{
											$fieldId = $cVal2->fieldId;
											foreach($JSON_Submitted_temp->elements as $cKey3=>$cVal3)
											{
												if($cVal3->type == 'CampaignContactFieldComparisonRule')
												{
													if($cVal3->fieldId == $fieldId)
													{
														if(isset($cVal1->outputTerminals[0]->connectedId))
														{
															if(abs($cVal3->id) != $cVal1->outputTerminals[0]->connectedId)
															{
																$connectedIdArray_local=array($cVal3->id=>$cVal1->outputTerminals[0]->connectedId);
																array_push($connectedIdArray_global,$connectedIdArray_local);
															}
															$cVal3->id=$cVal1->outputTerminals[0]->connectedId;
															break;
														}	
													}
												}
											}
										}
									}
								}
								//Shared Filter Member or Contact Filter
								if($cVal1->outputTerminals[0]->connectedType == 'CampaignContactFilterMembershipRule')
								{
									foreach($campaign_target->elements as $cKey2=>$cVal2)
									{
										if($cVal2->type == 'CampaignContactFilterMembershipRule' && $cVal2->id == $cVal1->outputTerminals[0]->connectedId)
										{
											$filterId = $cVal2->filterId;
											foreach($JSON_Submitted_temp->elements as $cKey3=>$cVal3)
											{
												if($cVal3->type == 'CampaignContactFilterMembershipRule')
												{
													if($cVal3->filterId == $filterId)
													{
														if(isset($cVal1->outputTerminals[0]->connectedId))
														{
															if(abs($cVal3->id) != $cVal1->outputTerminals[0]->connectedId)
															{
																$connectedIdArray_local=array($cVal3->id=>$cVal1->outputTerminals[0]->connectedId);
																array_push($connectedIdArray_global,$connectedIdArray_local);
															}
															$cVal3->id=$cVal1->outputTerminals[0]->connectedId;
															break;
														}	
													}
												}
											}
										}
									}
								}
								//CampaignAddToContactListAction
								if($cVal1->outputTerminals[0]->connectedType == 'CampaignAddToContactListAction')
								{
									foreach($campaign_target->elements as $cKey2=>$cVal2)
									{
										if($cVal2->type == 'CampaignAddToContactListAction' && $cVal2->id == $cVal1->outputTerminals[0]->connectedId)
										{
											$listId = $cVal2->listId;
											foreach($JSON_Submitted_temp->elements as $cKey3=>$cVal3)
											{
												if($cVal3->type == 'CampaignAddToContactListAction')
												{
													if($cVal3->listId == $listId)
													{
														if(isset($cVal1->outputTerminals[0]->connectedId))
														{
															if(abs($cVal3->id) != $cVal1->outputTerminals[0]->connectedId)
															{
																$connectedIdArray_local=array($cVal3->id=>$cVal1->outputTerminals[0]->connectedId);
																array_push($connectedIdArray_global,$connectedIdArray_local);
															}
															$cVal3->id=$cVal1->outputTerminals[0]->connectedId;
															break;
														}	
													}
												}
											}
										}
									}
								}
								//CampaignMoveToContactListAction
								if($cVal1->outputTerminals[0]->connectedType == 'CampaignMoveToContactListAction')
								{
									foreach($campaign_target->elements as $cKey2=>$cVal2)
									{
										if($cVal2->type == 'CampaignMoveToContactListAction' && $cVal2->id == $cVal1->outputTerminals[0]->connectedId)
										{
											$listId = $cVal2->listId;
											foreach($JSON_Submitted_temp->elements as $cKey3=>$cVal3)
											{
												if($cVal3->type == 'CampaignMoveToContactListAction')
												{
													if($cVal3->listId == $listId)
													{
														if(isset($cVal1->outputTerminals[0]->connectedId))
														{
															if(abs($cVal3->id) != $cVal1->outputTerminals[0]->connectedId)
															{
																$connectedIdArray_local=array($cVal3->id=>$cVal1->outputTerminals[0]->connectedId);
																array_push($connectedIdArray_global,$connectedIdArray_local);
															}
															$cVal3->id=$cVal1->outputTerminals[0]->connectedId;
															break;
														}	
													}
												}
											}
										}
									}
								}
								//CampaignContactListMembershipRule
								if($cVal1->outputTerminals[0]->connectedType == 'CampaignContactListMembershipRule')
								{
									foreach($campaign_target->elements as $cKey2=>$cVal2)
									{
										if($cVal2->type == 'CampaignContactListMembershipRule' && $cVal2->id == $cVal1->outputTerminals[0]->connectedId)
										{
											$listId = $cVal2->listId;
											foreach($JSON_Submitted_temp->elements as $cKey3=>$cVal3)
											{
												if($cVal3->type == 'CampaignContactListMembershipRule')
												{
													if($cVal3->listId == $listId)
													{
														if(isset($cVal1->outputTerminals[0]->connectedId))
														{
															if(abs($cVal3->id) != $cVal1->outputTerminals[0]->connectedId)
															{
																$connectedIdArray_local=array($cVal3->id=>$cVal1->outputTerminals[0]->connectedId);
																array_push($connectedIdArray_global,$connectedIdArray_local);
															}
															$cVal3->id=$cVal1->outputTerminals[0]->connectedId;
															break;
														}	
													}
												}
											}
										}
									}
								}
								//CampaignCustomObjectFieldComparisonRule
								if($cVal1->outputTerminals[0]->connectedType == 'CampaignCustomObjectFieldComparisonRule')
								{
									foreach($campaign_target->elements as $cKey2=>$cVal2)
									{
										if($cVal2->type == 'CampaignCustomObjectFieldComparisonRule' && $cVal2->id == $cVal1->outputTerminals[0]->connectedId)
										{
											$customObjectId = $cVal2->customObjectId;
											foreach($JSON_Submitted_temp->elements as $cKey3=>$cVal3)
											{
												if($cVal3->type == 'CampaignCustomObjectFieldComparisonRule')
												{
													if($cVal3->customObjectId == $customObjectId)
													{
														if(isset($cVal1->outputTerminals[0]->connectedId))
														{
															if(abs($cVal3->id) != $cVal1->outputTerminals[0]->connectedId)
															{
																$connectedIdArray_local=array($cVal3->id=>$cVal1->outputTerminals[0]->connectedId);
																array_push($connectedIdArray_global,$connectedIdArray_local);
															}
															$cVal3->id=$cVal1->outputTerminals[0]->connectedId;
															break;
														}	
													}
												}
											}
										}
									}
								}
								//CampaignEmailClickthroughRule
								if($cVal1->outputTerminals[0]->connectedType == 'CampaignEmailClickthroughRule')
								{
									foreach($campaign_target->elements as $cKey2=>$cVal2)
									{
										if($cVal2->type == 'CampaignEmailClickthroughRule' && $cVal2->id == $cVal1->outputTerminals[0]->connectedId)
										{
											$emailId = $cVal2->emailId;
											foreach($JSON_Submitted_temp->elements as $cKey3=>$cVal3)
											{
												if($cVal3->type == 'CampaignEmailClickthroughRule')
												{
													if($cVal3->emailId == $emailId)
													{
														if(isset($cVal1->outputTerminals[0]->connectedId))
														{
															if(abs($cVal3->id) != $cVal1->outputTerminals[0]->connectedId)
															{
																$connectedIdArray_local=array($cVal3->id=>$cVal1->outputTerminals[0]->connectedId);
																array_push($connectedIdArray_global,$connectedIdArray_local);
															}
															$cVal3->id=$cVal1->outputTerminals[0]->connectedId;
															break;
														}	
													}
												}
											}
										}
									}
								}
								//CampaignEmailSentRule
								if($cVal1->outputTerminals[0]->connectedType == 'CampaignEmailSentRule')
								{
									foreach($campaign_target->elements as $cKey2=>$cVal2)
									{
										if($cVal2->type == 'CampaignEmailSentRule' && $cVal2->id == $cVal1->outputTerminals[0]->connectedId)
										{
											$emailId = $cVal2->emailId;
											foreach($JSON_Submitted_temp->elements as $cKey3=>$cVal3)
											{
												if($cVal3->type == 'CampaignEmailSentRule')
												{
													if($cVal3->emailId == $emailId)
													{
														if(isset($cVal1->outputTerminals[0]->connectedId))
														{
															if(abs($cVal3->id) != $cVal1->outputTerminals[0]->connectedId)
															{
																$connectedIdArray_local=array($cVal3->id=>$cVal1->outputTerminals[0]->connectedId);
																array_push($connectedIdArray_global,$connectedIdArray_local);
															}
															$cVal3->id=$cVal1->outputTerminals[0]->connectedId;
															break;
														}	
													}
												}
											}
										}
									}
								}
								//CampaignEmailOpenedRule
								if($cVal1->outputTerminals[0]->connectedType == 'CampaignEmailOpenedRule')
								{
									foreach($campaign_target->elements as $cKey2=>$cVal2)
									{
										if($cVal2->type == 'CampaignEmailOpenedRule' && $cVal2->id == $cVal1->outputTerminals[0]->connectedId)
										{
											$emailId = $cVal2->emailId;
											foreach($JSON_Submitted_temp->elements as $cKey3=>$cVal3)
											{
												if($cVal3->type == 'CampaignEmailOpenedRule')
												{
													if($cVal3->emailId == $emailId)
													{
														if(isset($cVal1->outputTerminals[0]->connectedId))
														{
															if(abs($cVal3->id) != $cVal1->outputTerminals[0]->connectedId)
															{
																$connectedIdArray_local=array($cVal3->id=>$cVal1->outputTerminals[0]->connectedId);
																array_push($connectedIdArray_global,$connectedIdArray_local);
															}
															$cVal3->id=$cVal1->outputTerminals[0]->connectedId;
															break;
														}	
													}
												}
											}
										}
									}
								}
							}
							else
							{
								$result=$JSON_Submitted_temp;
							}	
						}
					}
					if(strpos($cVal->id, '-') !== false)
					{
						$cVal->id=$cVal->id;
					}
					else
					{
						$cVal->id='-'.$cVal->id;
					}	
					$result=$JSON_Submitted_temp;
				}
				if($campaignInputFlag==true)
				{
					foreach($JSON_Submitted_temp->elements as $cKey1=>$cVal1)
					{
						if($cVal1->type !='CampaignInput' && isset($cVal1->outputTerminals))
						{
							foreach($cVal1->outputTerminals as $cKey2=>$cVal2)
							{
								foreach($connectedIdArray_global as $cKey3=>$cVal3)
								{
									foreach($cVal3 as $cKey4=>$cVal4)
									{
										if($cVal2->connectedId==$cKey4)
										{
											$cVal2->connectedId=$cVal4;
										}	
									}
								}
							}
						}
						else
						{
							$result=$JSON_Submitted_temp;
						}	
					}
				}
				else
				{
					$result=$JSON_Submitted_temp;
				}		
			}
			else
			{
				$result=$JSON_Submitted_temp;
			}
		}
		else
		{
			$result=$JSON_Submitted_temp;
		}
		return json_encode($result);
	}
	
	function JSON_Submitted_add2move2Campaign_handler($JSON_Submitted,$Package_Item)
	{
		$JSON_Submitted_temp=json_decode($JSON_Submitted);
		$campaignElementId='';
		$c_flag=0;
		$f_flag=0;
		$cElement=0;
		if(($JSON_Submitted_temp->type=='Campaign') && (strpos($JSON_Submitted, 'CampaignAddToCampaignAction') !== false || strpos($JSON_Submitted, 'CampaignMoveToCampaignAction') !== false))
		{
			foreach($JSON_Submitted_temp->elements as $cKey=>$cVal)
			{
				if($cVal->type=='CampaignAddToCampaignAction' || $cVal->type=='CampaignMoveToCampaignAction')
				{
					$campaignElementId[$cElement]['campaignElementId']=$cVal->campaignElementId;
					$campaignElementId[$cElement++]['campaignId']=$cVal->campaignId;
					$c_flag=1;
				}
			}
			if(($c_flag==1) && ((strpos($JSON_Submitted, 'CampaignAddToCampaignAction') !== false) ||strpos($JSON_Submitted, 'CampaignMoveToCampaignAction') !== false))
			{
				foreach($campaignElementId as $elementKey=>$elementVal)
				{
					$result=$this->find_replace_campaign_element_id($JSON_Submitted_temp,$elementVal,$Package_Item->Deployment_Package_Id);
				}
			}
			else
			{
				$result=$JSON_Submitted_temp;
			}	
		}
		else if(($JSON_Submitted_temp->type=='Form') && strpos($JSON_Submitted, 'FormStepAddToCampaign') !== false )
		{
			foreach($JSON_Submitted_temp->processingSteps as $cKey=>$cVal)
			{
				if($cVal->type=='FormStepAddToCampaign' && isset($cVal->campaignElementId->constantValue))
				{
					$campaignElementId['campaignElementId']=$cVal->campaignElementId->constantValue;
					$campaignElementId['campaignId']=$cVal->campaignId;
					$f_flag=1;
				}
			}
			if(($f_flag==1) && (strpos($JSON_Submitted, 'FormStepAddToCampaign') !== false))
			{
				$result=$this->find_replace_campaign_element_id_form($JSON_Submitted_temp,$campaignElementId,$Package_Item->Deployment_Package_Id);	
			}
			else
			{
				$result=$JSON_Submitted_temp;
			}	
		}
		else
		{
			$result=$JSON_Submitted_temp;
		}
		return json_encode($result);	
	}
	
	function find_replace_campaign_element_id($JSON_Submitted_temp,$campaignElementId,$packageId)
	{
		if(isset($campaignElementId['campaignElementId']))
		{
			$campaign_source_element_id=$campaignElementId['campaignElementId'];
			$campaign_source_id=$campaignElementId['campaignId'];
		}	
		$this->db->select('JSON_Asset,New_JSON_Asset,Target_Asset_Id');
		$this -> db -> from('deployment_package_item');
		$this -> db -> where('Deployment_Package_Id',$packageId);
		$this -> db -> where('Asset_Type','Campaign');
		$this -> db -> where('Asset_Id',$campaign_source_id);
		$query = $this -> db -> get();
		$campaign_result=$query->result()[0];
		if(isset($campaign_result->JSON_Asset) && isset($campaign_result->New_JSON_Asset))
		{
			$source_campaign=json_decode($campaign_result->JSON_Asset);
			$target_campaign = $this->searchCampaign_targetInstance($packageId,'Campaign',$source_campaign->name);
			
			foreach($source_campaign->elements as $cKey2=>$cVal2)
			{
				if($cVal2->id==$campaign_source_element_id)
				{
					$element_type=$cVal2->type;
					$element_name=$cVal2->name;
					
					//Enchangement for Correct Linking-It will get SOURCE ASSET ID based on JSON Key as CampaignEmail,CampaignEmail then it will get SOURCE ASSET NAME and then it search TARGET for same Asset Name and it will return TARGET ASSET ID 
					//Email
					if($cVal2->type=='CampaignEmail' || $cVal2->type=='CampaignEmailClickthroughRule' || $cVal2->type=='CampaignEmailSentRule' || $cVal2->type=='CampaignEmailOpenedRule')
					{
						$sourceAssetName=$this->getSourceAssetNameById($cVal2->emailId,'Email',$packageId);
						$element_assetId=$this->getTargetAssetIdBySourceAssetName($sourceAssetName,'Email',$packageId);
					}
					//Segment
					else if($cVal2->type=='CampaignSegment')
					{
						$sourceAssetName=$this->getSourceAssetNameById($cVal2->segmentId,'Segment',$packageId);
						$element_assetId=$this->getTargetAssetIdBySourceAssetName($sourceAssetName,'Segment',$packageId);
					}
					//Form
					else if($cVal2->type=='CampaignSubmitFormRule')
					{
						$sourceAssetName=$this->getSourceAssetNameById($cVal2->formId,'Form',$packageId);
						$element_assetId=$this->getTargetAssetIdBySourceAssetName($sourceAssetName,'Form',$packageId);
					}
					//Shared Filter/Contact Filter
					else if($cVal2->type=='CampaignContactFilterMembershipRule')
					{
						$sourceAssetName=$this->getSourceAssetNameById($cVal2->filterId,'Shared Filter',$packageId);
						$element_assetId=$this->getTargetAssetIdBySourceAssetName($sourceAssetName,'Shared Filter',$packageId);
					}
					//Contact List
					else if($cVal2->type=='CampaignAddToContactListAction' || $cVal2->type=='CampaignMoveToContactListAction')
					{
						$sourceAssetName=$this->getSourceAssetNameById($cVal2->listId,'Contact List',$packageId);
						$element_assetId=$this->getTargetAssetIdBySourceAssetName($sourceAssetName,'Contact List',$packageId);
					}
					//Contact Field
					else if($cVal2->type=='CampaignContactFieldComparisonRule')
					{
						$sourceAssetName=$this->getSourceAssetNameById($cVal2->fieldId,'Contact Field',$packageId);
						$element_assetId=$this->getTargetAssetIdBySourceAssetName($sourceAssetName,'Contact Field',$packageId);
					}
					//Custom Object Id
					else if($cVal2->type=='CampaignCustomObjectFieldComparisonRule')
					{
						$sourceAssetName=$this->getSourceAssetNameById($cVal2->customObjectId,'Custom Object',$packageId);
						$element_assetId=$this->getTargetAssetIdBySourceAssetName($sourceAssetName,'Custom Object',$packageId);
					}	
				}
			}
			// print_r('element_type-'.$element_type);echo '<br>';
			// print_r('element_name-'.$element_name);echo '<br>';
			// print_r('element_assetId-'.$element_assetId);
			if(isset($target_campaign->elements))
			{	
				foreach($target_campaign->elements as $cKey3=>$cVal3)
				{
					if($cVal3->type==$element_type && $cVal3->name==$element_name)
					{
						if($cVal3->type=='CampaignEmail' || $cVal3->type=='CampaignEmailClickthroughRule' || $cVal3->type=='CampaignEmailSentRule' || $cVal3->type=='CampaignEmailOpenedRule')
						{
							if($cVal3->emailId==$element_assetId)
							{
								$target_element_id=$cVal3->id;	
							}	
						}
						//Segment
						else if($cVal3->type=='CampaignSegment')
						{
							if($cVal3->segmentId==$element_assetId)
							{
								$target_element_id=$cVal3->id;	
							}
						}
						//Form
						else if($cVal3->type=='CampaignSubmitFormRule')
						{
							if($cVal3->formId==$element_assetId)
							{
								$target_element_id=$cVal3->id;	
							}
						}
						//Shared Filter/Contact Filter
						else if($cVal3->type=='CampaignContactFilterMembershipRule')
						{
							if($cVal3->filterId==$element_assetId)
							{
								$target_element_id=$cVal3->id;	
							}
						}
						//Contact List
						else if($cVal3->type=='CampaignAddToContactListAction' || $cVal2->type=='CampaignMoveToContactListAction')
						{
							if($cVal3->listId==$element_assetId)
							{
								$target_element_id=$cVal3->id;
								//break;	
							}
						}
						//Contact Field
						else if($cVal3->type=='CampaignContactFieldComparisonRule')
						{
							if($cVal3->fieldId==$element_assetId)
							{
								$target_element_id=$cVal3->id;
								//break;	
							}							
						}
						//Custom Object Id
						else if($cVal3->type=='CampaignCustomObjectFieldComparisonRule')
						{
							if($cVal3->customObjectId==$element_assetId)
							{
								$target_element_id=$cVal3->id;
								//break;	
							}
						}	
						else
						{
							$target_element_id=$cVal3->id;
						}		
					}		
				}
			}	
		}
		// print_r($target_element_id);
		if(isset($target_element_id))
		{
			foreach($JSON_Submitted_temp->elements as $cKey4=>$cVal4)
			{
				if(isset($cVal4->campaignElementId) && ($cVal4->campaignElementId==$campaign_source_element_id))
				{
					$cVal4->campaignElementId=$target_element_id;
				}
				if(isset($cVal4->campaignId) && ($cVal4->campaignId==$campaign_source_id))
				{
					$cVal4->campaignId=$target_campaign->id;
				}
				if(strpos($cVal4->id, '-') !== false)
				{
					$cVal4->id=$cVal4->id;
				}
				else
				{
					$cVal4->id='-'.$cVal4->id;
				}		
			}
		}
		// echo '<pre>';
			// print_r($JSON_Submitted_temp);
		// echo '</pre>';	
		return $JSON_Submitted_temp;
	}
	
	function find_replace_campaign_element_id_form($JSON_Submitted_temp,$campaignElementId,$packageId)
	{
		if(isset($campaignElementId['campaignElementId']))
		{
			$campaign_source_element_id=$campaignElementId['campaignElementId'];
			$campaign_source_id=$campaignElementId['campaignId'];
		}	
		$this->db->select('JSON_Asset,New_JSON_Asset,Target_Asset_Id');
		$this -> db -> from('deployment_package_item');
		$this -> db -> where('Deployment_Package_Id',$packageId);
		$this -> db -> where('Asset_Type','Campaign');
		$this -> db -> where('Asset_Id',$campaign_source_id);
		$query = $this -> db -> get();
		$campaign_result=$query->result()[0];
		if(isset($campaign_result->JSON_Asset) && isset($campaign_result->New_JSON_Asset))
		{
			$source_campaign=json_decode($campaign_result->JSON_Asset);
			$target_campaign = $this->searchCampaign_targetInstance($packageId,'Campaign',$source_campaign->name);
			foreach($source_campaign->elements as $cKey2=>$cVal2)
			{
				if($cVal2->id==$campaign_source_element_id)
				{
					$element_type=$cVal2->type;
					$element_name=$cVal2->name;
					
					//Enchangement for Correct Linking-It will get SOURCE ASSET ID based on JSON Key as CampaignEmail,CampaignEmail then it will get SOURCE ASSET NAME and then it search TARGET for same Asset Name and it will return TARGET ASSET ID 
					//Email
					if($cVal2->type=='CampaignEmail' || $cVal2->type=='CampaignEmailClickthroughRule' || $cVal2->type=='CampaignEmailSentRule' || $cVal2->type=='CampaignEmailOpenedRule')
					{
						$sourceAssetName=$this->getSourceAssetNameById($cVal2->emailId,'Email',$packageId);
						$element_assetId=$this->getTargetAssetIdBySourceAssetName($sourceAssetName,'Email',$packageId);
					}
					//Segment
					else if($cVal2->type=='CampaignSegment')
					{
						$sourceAssetName=$this->getSourceAssetNameById($cVal2->segmentId,'Segment',$packageId);
						$element_assetId=$this->getTargetAssetIdBySourceAssetName($sourceAssetName,'Segment',$packageId);
					}
					//Form
					else if($cVal2->type=='CampaignSubmitFormRule')
					{
						$sourceAssetName=$this->getSourceAssetNameById($cVal2->formId,'Form',$packageId);
						$element_assetId=$this->getTargetAssetIdBySourceAssetName($sourceAssetName,'Form',$packageId);
					}
					//Shared Filter/Contact Filter
					else if($cVal2->type=='CampaignContactFilterMembershipRule')
					{
						$sourceAssetName=$this->getSourceAssetNameById($cVal2->filterId,'Shared Filter',$packageId);
						$element_assetId=$this->getTargetAssetIdBySourceAssetName($sourceAssetName,'Shared Filter',$packageId);
					}
					//Contact List
					else if($cVal2->type=='CampaignAddToContactListAction' || $cVal2->type=='CampaignMoveToContactListAction')
					{
						$sourceAssetName=$this->getSourceAssetNameById($cVal2->listId,'Contact List',$packageId);
						$element_assetId=$this->getTargetAssetIdBySourceAssetName($sourceAssetName,'Contact List',$packageId);
					}
					//Contact Field
					else if($cVal2->type=='CampaignContactFieldComparisonRule')
					{
						$sourceAssetName=$this->getSourceAssetNameById($cVal2->fieldId,'Contact Field',$packageId);
						$element_assetId=$this->getTargetAssetIdBySourceAssetName($sourceAssetName,'Contact Field',$packageId);
					}
					//Custom Object Id
					else if($cVal2->type=='CampaignCustomObjectFieldComparisonRule')
					{
						$sourceAssetName=$this->getSourceAssetNameById($cVal2->customObjectId,'Custom Object',$packageId);
						$element_assetId=$this->getTargetAssetIdBySourceAssetName($sourceAssetName,'Custom Object',$packageId);
					}
				}	
			}
			if(isset($target_campaign->elements))
			{	
				foreach($target_campaign->elements as $cKey3=>$cVal3)
				{
					if($cVal3->type==$element_type && $cVal3->name==$element_name)
					{
						if($cVal3->type=='CampaignEmail' || $cVal3->type=='CampaignEmailClickthroughRule' || $cVal3->type=='CampaignEmailSentRule' || $cVal3->type=='CampaignEmailOpenedRule')
						{
							if($cVal3->emailId==$element_assetId)
							{
								$target_element_id=$cVal3->id;	
							}	
						}
						//Segment
						else if($cVal3->type=='CampaignSegment')
						{
							if($cVal3->segmentId==$element_assetId)
							{
								$target_element_id=$cVal3->id;	
							}
						}
						//Form
						else if($cVal3->type=='CampaignSubmitFormRule')
						{
							if($cVal3->formId==$element_assetId)
							{
								$target_element_id=$cVal3->id;	
							}
						}
						//Shared Filter/Contact Filter
						else if($cVal3->type=='CampaignContactFilterMembershipRule')
						{
							if($cVal3->filterId==$element_assetId)
							{
								$target_element_id=$cVal3->id;	
							}
						}
						//Contact List
						else if($cVal3->type=='CampaignAddToContactListAction' || $cVal2->type=='CampaignMoveToContactListAction')
						{
							if($cVal3->listId==$element_assetId)
							{
								$target_element_id=$cVal3->id;
								//break;	
							}
						}
						//Contact Field
						else if($cVal3->type=='CampaignContactFieldComparisonRule')
						{
							if($cVal3->fieldId==$element_assetId)
							{
								$target_element_id=$cVal3->id;
								//break;	
							}							
						}
						//Custom Object Id
						else if($cVal3->type=='CampaignCustomObjectFieldComparisonRule')
						{
							if($cVal3->customObjectId==$element_assetId)
							{
								$target_element_id=$cVal3->id;
								//break;	
							}
						}	
						else
						{
							$target_element_id=$cVal3->id;
						}
					}	
				}
			}	
		}
		if(isset($target_element_id))
		{
			foreach($JSON_Submitted_temp->processingSteps as $cKey4=>$cVal4)
			{
				if(isset($cVal4->campaignElementId->constantValue) && ($cVal4->campaignElementId->constantValue==$campaign_source_element_id))
				{
					$JSON_Submitted_temp->processingSteps[$cKey4]->campaignElementId->constantValue=$target_element_id;
				}
				if(isset($cVal4->campaignId) && ($cVal4->campaignId==$campaign_source_id))
				{
					$JSON_Submitted_temp->processingSteps[$cKey4]->campaignId=$target_campaign->id;
				}
				if(strpos($cVal4->id, '-') !== false)
				{
					$JSON_Submitted_temp->processingSteps[$cKey4]->id=$cVal4->id;
				}
				else
				{
					$JSON_Submitted_temp->processingSteps[$cKey4]->id='-'.$cVal4->id;
				}		
			}
		}	
		return $JSON_Submitted_temp;
	}
	
	public function getCampaign_targetInstance($packageId,$Asset_Type,$Campaign_id)
	{
		$this -> db -> select('Target_Site_Name');
		$this -> db -> from('deployment_package dp');
		$this -> db -> where('dp.Deployment_Package_Id', $packageId); 	
		$query1 = $this -> db -> get();
		$targerSiteName = $query1 -> result()[0] -> Target_Site_Name;
		
		$this -> db -> select('Base_Url, Token, Site_Name, Token_Expiry_Time, Refresh_Token');
		$this -> db -> from('instance_ inst');	
		$this -> db -> where('inst.Site_Name',$targerSiteName);
		$this -> db -> limit(1);
		$query23   = $this -> db -> get();
		
		$token 	   = $query23 -> result()[0] -> Token;
		$Base_Url  = $query23 -> result()[0] -> Base_Url;	
		$targetOrg = $query23 -> result()[0];
		
		if(time()+1800 > $query23 -> result()[0] -> Token_Expiry_Time)
		{
		   $result2 = $this -> eloqua -> refreshToken($targetOrg);
		   $query -> result()[0] -> Token_Expiry_Time = $result2['Token']['access_token'];
		}
		
		$this -> db -> select('*');
		$this -> db -> from('rsys_asset_endpoint rae');
		$this -> db -> where('rae.Asset_Type', $Asset_Type);
		$this -> db -> where('rae.Endpoint_Type', 'Read Single');
		$query22 = $this -> db -> get();
		$endpoint = $query22->result();
		
		$url = $Base_Url .$endpoint[0]->Endpoint_URL .'/'.$Campaign_id;
		//print_r($url);
		$result_asset = $this->eloqua->get_request($token, $url);
		
		$result_asset = json_decode($result_asset['data']);
		
		return $result_asset;
	}
	
	public function searchCampaign_targetInstance($packageId,$Asset_Type,$Campaign_name)
	{
		$this -> db -> select('Target_Site_Name');
		$this -> db -> from('deployment_package dp');
		$this -> db -> where('dp.Deployment_Package_Id', $packageId); 	
		$query1 = $this -> db -> get();
		$targerSiteName = $query1 -> result()[0] -> Target_Site_Name;
		
		$this -> db -> select('Base_Url, Token, Site_Name, Token_Expiry_Time, Refresh_Token');
		$this -> db -> from('instance_ inst');	
		$this -> db -> where('inst.Site_Name',$targerSiteName);
		$this -> db -> limit(1);
		$query23   = $this -> db -> get();
		
		$token 	   = $query23 -> result()[0] -> Token;
		$Base_Url  = $query23 -> result()[0] -> Base_Url;	
		$targetOrg = $query23 -> result()[0];
		
		if(time()+1800 > $query23 -> result()[0] -> Token_Expiry_Time)
		{
		   $result2 = $this -> eloqua -> refreshToken($targetOrg);
		   $query -> result()[0] -> Token_Expiry_Time = $result2['Token']['access_token'];
		}
		
		$this -> db -> select('*');
		$this -> db -> from('rsys_asset_endpoint rae');
		$this -> db -> where('rae.Asset_Type', $Asset_Type);
		$this -> db -> where('rae.Endpoint_Type', 'Find Asset');
		$query22 = $this -> db -> get();
		$endpoint = $query22->result();
		
		$url = $Base_Url .$endpoint[0]->Endpoint_URL .'?search="'.str_replace(' ', '%20', $Campaign_name) .'"&depth=complete';
		$result_asset = $this->eloqua->get_request($token, $url);
		$result_asset = json_decode($result_asset['data']);
		if(isset($result_asset->elements[0]))
		{
			$result=$result_asset->elements[0];
		}
		else
		{
			$result = null;
		}
		return $result;
	}
	
	function JSON_Submitted_quickList($JSON_Submitted)
	{

		$JSON_Submitted_temp = json_decode($JSON_Submitted);	
		if($JSON_Submitted_temp->type=='OptionList')
		{
			$OptionListName= $JSON_Submitted_temp->name;
			
			if(strpos($OptionListName, '/') !== false) 
			{	
				$OptionListName = preg_replace("/[\/\&%#\$:]/", " ", $OptionListName);
				
				$JSON_Submitted_temp->name=$OptionListName.strtotime("now");
				$result=$JSON_Submitted_temp;
			}
			else
			{
				$result=$JSON_Submitted_temp;
			}	
		}
		else
		{
			$result=$JSON_Submitted_temp;
		}	
		return json_encode($result);	
	}
	
	function FormHiddenEmailCampaignId($JSON_Submitted,$Package_Item)
	{
		$form_JSON_Submitted= json_decode($JSON_Submitted);
		$hiddenFlag=0;
		$emailFormFieldId='';
		$campaignFormFieldId='';
		if($form_JSON_Submitted->type=='Form')
		{
			if(isset($form_JSON_Submitted->processingSteps) && (strpos(json_encode($form_JSON_Submitted->processingSteps), 'FormStepSendEmail') !== false) || strpos(json_encode($form_JSON_Submitted->processingSteps), 'FormStepAddToCampaign') !== false)
			{
				foreach($form_JSON_Submitted->processingSteps as $fKey=>$fVal)
				{
					if(isset($fVal->emailId->formFieldId))
					{
						$emailFormFieldId=$fVal->emailId->formFieldId;
						$hiddenFlag=1;
					}
					else if(isset($fVal->campaignElementId->formFieldId) && !isset($fVal->campaignElementId->optionListId))
					{
						$campaignFormFieldId=$fVal->campaignElementId->formFieldId;
						$hiddenFlag=1;
						//print_r($campaignFormFieldId);
					}
				}
				if($hiddenFlag==1)
				{	
					if(isset($emailFormFieldId) || isset($campaignFormFieldId))
					{
						foreach($form_JSON_Submitted->elements as $fKey=>$fVal)
						{
							if($fVal->id==$emailFormFieldId)
							{
								$emailId=$fVal->defaultValue;
							}
							else if($fVal->id==$campaignFormFieldId)
							{
								$campaignId=$fVal->defaultValue;
							} 
						}	
					}
					if(isset($emailId) || isset($campaignId))
					{
						$result_form=$this->findReplaceHiddenId($form_JSON_Submitted,$Package_Item->Deployment_Package_Id);
						$result= $result_form;
					}	
				}
				else
				{
					$result= $form_JSON_Submitted;
				}	
			}
			else
			{
				$result= $form_JSON_Submitted;
			}	
		}	
		else
		{
			$result= $form_JSON_Submitted;
		}	
		return json_encode($result);
	}
	
	function findReplaceHiddenId($form_JSON_Submitted,$Package_Id)
	{
		$this->db->select('Asset_Id,Target_Asset_Id');
		$this -> db -> from('deployment_package_validation_list');
		$this -> db -> where('Deployment_Package_Id',$Package_Id);
		$this -> db -> where("(Asset_Type='Campaign' OR Asset_Type='Email')");
		// $this -> db -> or_where('Asset_Type','Email');
		$query = $this -> db -> get();
		$form_result=$query->result();
		// print_r($form_result);
		foreach($form_result as $fKey1=>$fVal1)
		{
			$new_formResult[$fVal1->Asset_Id]=$fVal1->Target_Asset_Id;
		}
		//print_r($new_formResult);
		foreach($form_JSON_Submitted->elements  as $fKey2=>$fVal2)
		{
			//print_r($fVal2);
			if(isset($fVal2->defaultValue) && is_numeric($fVal2->defaultValue))
			{	
				foreach($new_formResult as $fKey3=>$fVal3)
				{	
					// print_r($fVal2->defaultValue .'--'.$fKey3);
					if($fVal2->defaultValue==$fKey3)
					{	//echo 'inside replace';
						$fVal2->defaultValue=$fVal3;
						// print_r($fVal3);
					}	
				}
			}
		}
		return $form_JSON_Submitted;
	}
	
	function updateUnsupportedAssets($packageId)
	{
		$this->logging_model->logT($packageId,0,'updateUnsupportedAssets','','');
		$this->db->select('Asset_Type,JSON_Asset,Deployment_Package_Item_Id');
		$this -> db -> from('deployment_package_item');
		$this -> db -> where('Deployment_Package_Id',$packageId);
		$this -> db -> where('verified', 1);
		// $this -> db -> or_where('Asset_Type','Email');
		$query = $this -> db -> get();
		$DPIResult=$query->result();
		
		if($query->num_rows()>0)
		{
			foreach($DPIResult as $DPIkey=>$DPIval)
			{
				$this->db->select('*');
				$this->db->from('rsys_asset_type');
				$this->db->where('Asset_Type_Name',$DPIval->Asset_Type);
				$ATQuery = $this->db->get();
				$ATResult = $ATQuery->result();
				// print_r($ATResult);
				if($ATQuery->num_rows()>0)
				{
					if(isset($ATResult[0]->Unsupported_JSON_Path) && !empty($ATResult[0]->Unsupported_JSON_Path))
					{
						$Unsupported_JSON_Paths = array();
						$Unsupported_JSON_Paths = explode(',',$ATResult[0]->Unsupported_JSON_Path);
						
						// print_r($Unsupported_JSON_Paths);
						if(sizeof($Unsupported_JSON_Paths)>0)
						{
							foreach($Unsupported_JSON_Paths as $pathKey => $pathValue)
							{
								$replaceKey =  trim(substr($pathValue, strrpos($pathValue, '.') + 1));
								$replaceKeys = array();
								$replaceKeys = explode('|',$pathValue);
								
								$replaceList = (new JSONPath(json_decode($DPIval->JSON_Asset)))->find($replaceKeys[0]);
								
								$replaceList = Json_decode(Json_encode($replaceList));
								// print_r($replaceList);
								if(sizeof($replaceList) > 0)
								{
									$this->updateDeploymentPackage($packageId);
									$this->updateDeploymentPackageItem($DPIval->Deployment_Package_Item_Id,$pathValue,$replaceKeys[1]);
								}
							}
						}
							
					}
				}
				
			}
		}
	}
	
	function updateDeploymentPackage($packageId)
	{
		//update 
		$tdata['Status'] = 'Unsupported';
		$this->db->where('Deployment_Package_Id',$packageId);
		$this->db->update('deployment_package',$tdata);
		
	}
	
	function updateDeploymentPackageItem($packageItemId,$pathValue,$message)
	{
		$this->db->select('*');
		$this->db->from('deployment_package_item');
		$this->db->where('Deployment_Package_Item_Id',$packageItemId);
		$DPIQuery = $this->db->get();
		$DPIResult = $DPIQuery->result();
		if(($this->containsTheMessage($message,$DPIResult[0]->Unsupported_Asset_Message))==0)
		{
			if(isset($DPIResult[0]->Unsupported_Asset_Message)&&(!empty($DPIResult[0]->Unsupported_Asset_Message)))
			{
				$tdata['Unsupported_Asset_Message'] = $DPIResult[0]->Unsupported_Asset_Message.','.$message;
			}
			else
			{	
				$tdata['Unsupported_Asset_Message'] = $message;
			}
		}
		$tdata['Status'] = 'Unsupported';
		$tdata['verified'] = 1;
		$this->db->where('Deployment_Package_Item_Id',$packageItemId);
		$this->db->update('deployment_package_item',$tdata);
	}
	
	function containsTheMessage($message,$Unsupported_Asset_Message)
	{
		if(isset($Unsupported_Asset_Message)&&(!empty($Unsupported_Asset_Message)))
		{
			if(strpos($Unsupported_Asset_Message, ',') !== false)
			{
				$messages = array();
				$messages = explode(',',$Unsupported_Asset_Message);
				
				foreach($messages as $key=>$val)
				{
					if($message == $val)
					{
						
						return 1;
					}
				}
				return 0;
			}
			else
			{
				if($Unsupported_Asset_Message == $message)
				{
					return 1;
				}
				else
				{
					return 0;
				}
			}
		}
		else
		{
			return 0;	
		}	
	}
	
	function checkDuplicatehtmlName($packageId)
	{
		$this->logging_model->logT($packageId,0,'checkDuplicatehtmlName','','');
		$package_forms_dpi=array();
		$pathValue='';
		$this->db->select('Asset_Type,Asset_Name,Deployment_Package_Item_Id,JSON_Asset');
		$this -> db -> from('deployment_package_item');
		$this -> db -> where('Deployment_Package_Id', $packageId);
		$this -> db -> where('Asset_Type','Form');
		$this -> db -> where('verified', 1);
		$query_dpi = $this -> db -> get();
		
		if($query_dpi -> num_rows() > 0)
		{
			$package_forms_dpi[] = $query_dpi->result();
		}
		
		// print_r($package_forms_dpi);
		
		$Package_details = $this->deploy_model->getPackage($packageId);
		$instance = $this->deploy_model->getInstance($Package_details[0]->Target_Site_Name);		
		$formList=array();
			
		if($query_dpi -> num_rows() > 0)
		{
			if(isset($package_forms_dpi))
			{
				foreach($package_forms_dpi[0] as $fKey=>$fVal)
				{
					// print_r($fVal['Asset_Type']);
					// print_r($fVal->Unsupported_Asset_Message);
					$formhtmlName=json_decode($fVal->JSON_Asset)->htmlName;
					$duplicateResult=$this->getDuplicateFormHTMLName($instance,$formhtmlName,$fVal->Asset_Name);
					if($duplicateResult>=1)
					{
						$this->updateDeploymentPackageItem($fVal->Deployment_Package_Item_Id,$pathValue,$message='htmlname already exists in the target');
						$this->updateDeploymentPackage($packageId);
					}	
				}
			}
		}
	}
	
	function getDuplicateFormHTMLName($instance,$tempHTMLName,$assetName)
	{		
		$token 	   = $instance [0] -> Token;
		$Base_Url  = $instance [0]-> Base_Url;	
		$targetOrg = $instance [0];
				
		$this -> db -> select('*');
		$this -> db -> from('rsys_asset_endpoint rae');
		$this -> db -> where('rae.Asset_Type', 'Form');
		$this -> db -> where('rae.Endpoint_Type', 'Read List');
		$queryFindEndpoint = $this -> db -> get();
		$endpoint = $queryFindEndpoint->result();
		
		$url = $Base_Url .$endpoint[0]->Endpoint_URL .'?depth=complete&search="'.$tempHTMLName.'"';
		$result_asset = $this->eloqua->get_request($token, $url);
		
		$result_asset_=json_decode($result_asset['data'])->elements;
		
		if(isset($result_asset_) && isset($result_asset_[0]))
		{
			if($result_asset_[0]->name==$assetName)
			{
				$returnValue=0;
			}
			else
			{
				$returnValue= json_decode($result_asset['data'])->total;
			}		
		}
		else
		{
			$returnValue=-1;
		}	
		return $returnValue;
	}
		
	function LPFileStorage($JSON_Submitted,$Package_Item)
	{
		$fileStorage=array();
		$JSON_Submitted_LP=json_decode($JSON_Submitted);
		if($JSON_Submitted_LP->type=='LandingPage')
		{	
			if(isset($JSON_Submitted_LP->files))
			{	
				// print_r($JSON_Submitted_LP->files);
				foreach($JSON_Submitted_LP->files as $FSKey=>$FSVal)
				{
					if($FSVal->id > 0)
					{
						$temp_file=$this->targetFileStorageJSON($Package_Item->Deployment_Package_Id,$FSVal->name);
						if(isset($temp_file))
						{
							// print_r($temp_file);
							$fileStorage[] = $temp_file;
						}	
					}
				}
				$fileStorage=array_unique($fileStorage, SORT_REGULAR);
				$result=$this->fileStorageLPHandaler($JSON_Submitted,$fileStorage);
				$result = json_decode($result);
				// echo '<pre>';
					// print_r($result);
				// echo '</pre>';	
			}
			else
			{
				$result=$JSON_Submitted_LP;
			}		
		}
		else
		{
			$result=$JSON_Submitted_LP;
		}
		return json_encode($result);	
		
	}
	
	function targetFileStorageJSON($packageId,$filStorageName)
	{		
		// print_r($fileStorageName);
		$Package_details = $this->deploy_model->getPackage($packageId);
		$instance_details = $this->deploy_model->getInstance($Package_details[0]->Target_Site_Name);
		
		$token 	   = $instance_details [0] -> Token;
		$Base_Url  = $instance_details [0]-> Base_Url;	
		$targetOrg = $instance_details [0];
				
		$this -> db -> select('*');
		$this -> db -> from('rsys_asset_endpoint rae');
		$this -> db -> where('rae.Asset_Type', 'File Storage');
		$this -> db -> where('rae.Endpoint_Type', 'Read List');
		$queryFindEndpoint = $this -> db -> get();
		$endpoint = $queryFindEndpoint->result();
		
		$url = $Base_Url .$endpoint[0]->Endpoint_URL .'?depth=complete&search="'.str_replace(' ','%20',$filStorageName).'"';
		$result_asset = $this->eloqua->get_request($token, $url);
		$result_asset_=json_decode($result_asset['data'])->elements;
		
		// print_r($url);
		
		if(isset($result_asset_[0]))
		{
			$returnResult=json_decode(json_encode($result_asset_[0]),true);
			//modified on 28-08 
			return $returnResult;
		}	
		// print_r($returnResult);
		// return json_encode($returnResult);
		// return $returnResult;
	}
	
	function fileStorageLPHandaler($JSON_Submitted,$fileStorage)
	{
		// print_r(json_encode($fileStorage));
		$data_json=array();
		$data_json['LP']=json_decode($JSON_Submitted,true); // encoded json
		$data_json['files_LP']=$fileStorage; //php array
		// print_r(array_values($data_json)); //final encoded
		// print_r(json_encode($data_json)); //final encoded
		if(isset($data_json))
		{
			$url = 'http://transporter.portqii.com:8080/Test1/fileStorageReplaceLink';
			$ch=curl_init();
			$headers = array('Content-Type:text/plain');
			curl_setopt($ch, CURLOPT_URL, $url); 
			curl_setopt($ch, CURLOPT_POST, true); 
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
			curl_setopt($ch, CURLOPT_POSTFIELDS,json_encode($data_json)); //error here is array to string cnversion
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			$result		= curl_exec($ch);
		}	
		return $result;
	}
	
	function LPHyperlinkLPID($JSON_Submitted,$package_validation_list)
	{
		$fileStorage=array();
		$JSON_Submitted_LP=json_decode($JSON_Submitted);
		$sourceLPID='';
		if($JSON_Submitted_LP->type=='LandingPage' && isset($JSON_Submitted_LP->hyperlinks) && !empty($JSON_Submitted_LP->hyperlinks))
		{	
			foreach($JSON_Submitted_LP->hyperlinks as $hKey=>$hVal)
			{
				if(isset($hVal->referencedEntityId))
				{
					$sourceLPID=explode('LP=',$hVal->href);
					$sourceLPName=$this->getSourceLPNameId($sourceLPID[1],$package_validation_list);
					$targetLPID=$this->targetHyperlinkLandingpageJSON($package_validation_list[0]->Deployment_Package_Id,$sourceLPName);
					$sourceLPID_temp = $sourceLPID[1];
					$sourceLPID[1]=$targetLPID;
					$hVal->href=implode('LP=',$sourceLPID);
					$hVal->referencedEntityId=$targetLPID;
					// $result=$JSON_Submitted_LP;
					$result=$this->replaceallLPId(json_encode($JSON_Submitted_LP),$sourceLPID_temp,$targetLPID);
					// echo '<pre>';
						// print_r(json_decode($result));
					// echo '</pre>';
				}
				else
				{
					$result=json_decode($JSON_Submitted);
				}	
			}
			
		}
		else
		{
			$result=json_decode($JSON_Submitted);
		}
		return json_encode($result);	
	}
	
	function getSourceLPNameId($sourceLPID,$package_validation_list)
	{
		foreach($package_validation_list as $lpKey=>$lpVal)
		{
			if($lpVal->Asset_Id==$sourceLPID && $lpVal->Asset_Type='Landing Page')
			{
				$sourceLPName=$lpVal->Asset_Name;
			}	
		}
		return $sourceLPName;
	}
	
	function targetHyperlinkLandingpageJSON($packageId,$sourceLPName)
	{		
		// print_r($sourceLPID);
		$Package_details = $this->deploy_model->getPackage($packageId);
		$instance_details = $this->deploy_model->getInstance($Package_details[0]->Target_Site_Name);
		
		$token 	   = $instance_details [0] -> Token;
		$Base_Url  = $instance_details [0]-> Base_Url;	
		$targetOrg = $instance_details [0];
				
		$this -> db -> select('*');
		$this -> db -> from('rsys_asset_endpoint rae');
		$this -> db -> where('rae.Asset_Type', 'Landing Page');
		$this -> db -> where('rae.Endpoint_Type', 'Read List');
		$queryFindEndpoint = $this -> db -> get();
		$endpoint = $queryFindEndpoint->result();
		
		$url = $Base_Url .$endpoint[0]->Endpoint_URL .'?depth=complete&search="'.$sourceLPName.'"';
		$result_asset = $this->eloqua->get_request($token, $url);
		$result_asset_=json_decode($result_asset['data'])->elements;
		
		if(isset($result_asset_[0]))
		{
			$returnResult=json_decode(json_encode($result_asset_[0]),true);
		}
		
		return $returnResult['id'];	
	}
	
	function replaceallLPId($JSON_Submitted,$sourceLPID,$targetLPID)
	{
		$foundLPID='LP='.$sourceLPID;
		$replaceLPID='LP='.$targetLPID;
		if(strpos($JSON_Submitted, $foundLPID) !== false)
		{	
			$JSON_Submitted_LP_Replace=str_replace($foundLPID,$replaceLPID,$JSON_Submitted);
			$result=$JSON_Submitted_LP_Replace;
			// print_r(json_decode($JSON_Submitted_LP_Replace));
		}
		else
		{	
			$result=$JSON_Submitted;
		}
		return 	$result;
	}

	// function HyperlinkLPID($JSON_Submitted,$Package_Item)
	// {
		// $JSON_Submitted_temp=json_decode($JSON_Submitted);
		// $assetsIds=array();
		// if($Package_Item->Asset_Type=='Landing Page')
		// {
			// $assetsIds[]=$Package_Item->Asset_Id;
		// }		
		// if($JSON_Submitted_temp->type='Hyperlink' && isset($JSON_Submitted_temp->referencedEntityId))
		// {	/////print_r($Package_Item->Asset_Id.'----'.$JSON_Submitted_temp->referencedEntityId);
			// if($Package_Item->Asset_Id==$JSON_Submitted_temp->referencedEntityId)
			// {	
				// $sourceLPName=$Package_Item->Asset_Name;
				
			// }
			// $result=$JSON_Submitted_temp;
		// }	
		// else
		// {
			// $result=$JSON_Submitted_temp;
		// }
		// return json_encode($result);	
	// }
	
	
	function getSourceLPNameId_inHyperlink($sourceLPID,$package_validation_list)
	{
		foreach($package_validation_list as $lpKey=>$lpVal)
		{
			if($lpVal->Asset_Id==$sourceLPID && $lpVal->Asset_Type='Landing Page')
			{
				$sourceLPName=$lpVal->Asset_Name;
				// print_r($sourceLPName); echo '<br>';
			}	
		}
		return $sourceLPName;
	}
	
	function hyperlinkLhref($JSON_Submitted,$packageId)
	{
		$JSON_Submitted_temp=json_decode($JSON_Submitted);
		$allHyperlink=$this->getTargetHyperlinks($packageId);
		if($JSON_Submitted_temp->type=='Hyperlink' && $JSON_Submitted_temp->hyperlinkType=='ExternalURL')
		{	
			$hyperlinkName=$JSON_Submitted_temp->name;
			$hyperlinkHref=$JSON_Submitted_temp->href;
			$newHyperlinkJSON=$this->compareHyperlinkNameHref($hyperlinkName,$hyperlinkHref,$allHyperlink,$JSON_Submitted_temp);
			$result=$newHyperlinkJSON;
		}	
		else
		{
			$result=$JSON_Submitted_temp;
		}
		return json_encode($result);	
	}
	
	function compareHyperlinkNameHref($hyperlinkName,$hyperlinkHref,$allHyperlink,$JSON_Submitted)
	{
		$newJSONSubmitted=$JSON_Submitted;
		foreach($allHyperlink as $hKey=>$hVal)
		{
			if($hVal->name==$hyperlinkName && $hVal->href==$hyperlinkHref)
			{
				$newJSONSubmitted=$JSON_Submitted;
			}	
			else if($hVal->name !=$hyperlinkName && $hVal->href==$hyperlinkHref)
			{
				$newJSONSubmitted->id=$hVal->id;
				//$newJSONSubmitted->name=$hVal->name;
				$newJSONSubmitted->name=$hyperlinkName;
				$newJSONSubmitted->folderId=$hVal->folderId;
				$newJSONSubmitted->href=$hVal->href;
			}	
		}
		return $newJSONSubmitted;
	}
	
	function getTargetHyperlinks($packageId)
	{
		$Package_details = $this->deploy_model->getPackage($packageId);
		$instance_details = $this->deploy_model->getInstance($Package_details[0]->Target_Site_Name);
		
		$token 	   = $instance_details [0] -> Token;
		$Base_Url  = $instance_details [0]-> Base_Url;	
		$targetOrg = $instance_details [0];
				
		$this -> db -> select('*');
		$this -> db -> from('rsys_asset_endpoint rae');
		$this -> db -> where('rae.Asset_Type', 'Hyperlink');
		$this -> db -> where('rae.Endpoint_Type', 'Read List');
		$queryFindEndpoint = $this -> db -> get();
		$endpoint = $queryFindEndpoint->result();
		
		$url = $Base_Url .$endpoint[0]->Endpoint_URL .'?depth=complete';
		$result_asset = $this->eloqua->get_request($token, $url);
		$result_asset_=json_decode($result_asset['data'])->elements;
		return $result_asset_;
	}
	
	function LPMicrositeIdReplace($JSON_Submitted,$Package_Item)
	{
		$JSON_Submitted_Microsite=json_decode($JSON_Submitted);	
		
			if($JSON_Submitted_Microsite->type=='LandingPage')
			{
				//echo 'LPMicrositeIdReplace<pre>';
				//print_r($JSON_Submitted_Microsite->name);
				//print_r('<br>---------------------------<br>');
				//echo '</pre>';					
			}	
			
		
		if($JSON_Submitted_Microsite->type=='LandingPage' && isset($JSON_Submitted_Microsite->micrositeId))
		{	
			if($Package_Item->Target_Microsite_JSON !=false)
			{	
				$replaceMSID = json_decode($Package_Item->Target_Microsite_JSON)->id;
				$JSON_Submitted_Microsite->micrositeId=$replaceMSID;
				$result=$JSON_Submitted_Microsite;
			}
			else
			{
				$JSON_Submitted_Microsite->micrositeId= "2";
				$result=$JSON_Submitted_Microsite;
			}		
		}	
		else
		{
			$result=$JSON_Submitted_Microsite;
		}
		return json_encode($result);
	}
	
	
	function LPDomainNameReplace($JSON_Submitted,$Package_Item)
	{
		$JSONSubmitted_DomainName=json_decode($JSON_Submitted);	
		if(($JSONSubmitted_DomainName->type=='LandingPage' || $JSONSubmitted_DomainName->type=='Email' || $JSONSubmitted_DomainName->type=='EmailHeader' || $JSONSubmitted_DomainName->type=='EmailFooter' || $JSONSubmitted_DomainName->type=='ContentSection' || $JSONSubmitted_DomainName->type=='DynamicContent' || $JSONSubmitted_DomainName->type=='EmailSignatureLayout') && isset($JSONSubmitted_DomainName->hyperlinks))
		{
			if(!empty($JSONSubmitted_DomainName->hyperlinks))
			{
				foreach($JSONSubmitted_DomainName->hyperlinks as $hKey=>$hVal)
				{
					if(isset($hVal->hyperlinkType) && $hVal->hyperlinkType=='LandingPageURL')
					{
						$LPName= $this->getLandingPageName($hVal->referencedEntityId,$Package_Item);	
						
						$targetMicrositeId=$this->getTargetLPMSID($LPName,$Package_Item->Deployment_Package_Id,$destination='target',$needed='microsite');
						
						if($targetMicrositeId>0)
						{
							$targetMicrositeDomain=$this->getMicrositeDeatils($targetMicrositeId,$Package_Item->Deployment_Package_Id,$destination='target');
							
							$sourceMicrositeId = $this->getTargetLPMSID($LPName,$Package_Item->Deployment_Package_Id,$destination='source',$needed='microsite');
							
							$sourceMicrositeDomain=$this->getMicrositeDeatils($sourceMicrositeId,$Package_Item->Deployment_Package_Id,$destination='source');
							
							$targetLandingpageId=$this->getTargetLPMSID($LPName,$Package_Item->Deployment_Package_Id,$destination='target',$needed='id');
							
							$foundURL = $this->formURL($sourceMicrositeDomain['href'],$hVal->referencedEntityId);
							
							$replaceURL = $this->formURL($targetMicrositeDomain['href'],$targetLandingpageId);
							
							$hrefReplace = $this->replaceWithtargetDomain($foundURL,$replaceURL,$JSONSubmitted_DomainName);
							$JSONSubmitted_DomainName = $hrefReplace;
							
							$domainReplace = $this->replaceWithtargetDomain($sourceMicrositeDomain['name'],$targetMicrositeDomain['name'],$JSONSubmitted_DomainName);					
							$JSONSubmitted_DomainName= $domainReplace;							
							$result = $JSONSubmitted_DomainName;
						}
						else
						{
							$result = $JSONSubmitted_DomainName;
						}	
					}
					else
					{
						$result = $JSONSubmitted_DomainName;
					}	
				}
			}
			else
			{
				$result = $JSONSubmitted_DomainName;
			}		
		}	
		else
		{
			$result = $JSONSubmitted_DomainName;
		}
		return json_encode($result);	
	}
	
	function getLandingPageName($LPId,$Package_Item)
	{
		$this -> db -> select('Asset_Name');
		$this -> db -> from('deployment_package_item');
		$this -> db -> where('Deployment_Package_Id', $Package_Item->Deployment_Package_Id);
		$this -> db -> where('Asset_Type', 'Landing Page');
		$this -> db -> where('Asset_Id', $LPId);
		$LPName = $this -> db -> get();
		$LPName = $LPName->result();
		// print_r($LPName);
		return $LPName[0]->Asset_Name;
	}
	
	function getTargetLPMSID($targetLPName,$packageId,$destination,$needed)
	{
		$Package_details = $this->deploy_model->getPackage($packageId);

		if($destination=='target')
		{
			$instance_details = $this->deploy_model->getInstance($Package_details[0]->Target_Site_Name);	
		}
		else if($destination=='source')
		{
			$instance_details = $this->deploy_model->getInstance($Package_details[0]->Source_Site_Name);
		}
		$token 	   = $instance_details [0] -> Token;
		$Base_Url  = $instance_details [0]-> Base_Url;	
		$targetOrg = $instance_details [0];
		
		$this -> db -> select('*');
		$this -> db -> from('rsys_asset_endpoint');
		$this -> db -> where('Asset_Type', 'Landing Page');
		$this -> db -> where('Endpoint_Type', 'Find Asset');
		$query22 = $this -> db -> get();
		$endpoint = $query22->result();
		
		$url = $Base_Url .$endpoint[0]->Endpoint_URL .'?depth=complete&search="'.str_replace(' ', '%20', $targetLPName).'"';
		// print_r($url);
		$result_asset = $this->eloqua->get_request($token, $url);
		$result_asset_=json_decode($result_asset['data'])->elements;
		
		if($needed=='id')
		{
			$returnResult=$result_asset_[0]->id;
		}
		else if($needed=='microsite')
		{
			if(isset($result_asset_[0]->micrositeId))
			{
				$returnResult= $result_asset_[0]->micrositeId;
			}
			else
			{
				$returnResult=-1;
			}	
		}	
		return $returnResult;
	}
	
	function getMicrositeDeatils($MSID,$packageId,$destination)
	{
		// print_r($MSID);
		$Package_details = $this->deploy_model->getPackage($packageId);
		if($destination=='target')
		{
			$instance_details = $this->deploy_model->getInstance($Package_details[0]->Target_Site_Name);
		}
		else if($destination=='source')
		{
			$instance_details = $this->deploy_model->getInstance($Package_details[0]->Source_Site_Name);
		}	
		$token 	   = $instance_details [0] -> Token;
		$Base_Url  = $instance_details [0]-> Base_Url;	
		$targetOrg = $instance_details [0];

		$url = $Base_Url .'/API/REST/2.0/assets/microsite/'.$MSID;
		
		$result_asset = $this->eloqua->get_request($token, $url);
		$result_asset_=json_decode($result_asset['data']);
		// print_r($result_asset_);
		$domainName='';
		if(isset($result_asset_->domains[0]))
		{
			$domain['name']=$result_asset_->domains[0];
			if($result_asset_->isSecure=='true')
			{
				$domain['href'] = 'https://'.$result_asset_->domains[0];
			}	
			else
			{
				$domain['href'] = 'http://'.$result_asset_->domains[0];
			}
		}	
		return $domain;
	}
	
	function formURL($href,$LandingpageId)
	{		
		return $href .'/LP='.$LandingpageId;
	}
	
	function replaceWithtargetDomain($sourceDomain,$targetDomain,$JSONSubmitted)
	{
		$result = $this->deploy_model->getImageUrlsReplaced($JSONSubmitted,$sourceDomain,$targetDomain);
		return $result;
	}
	
	function ReplaceMicrositeIdForExistingLP($JSON_Submitted,$Package_Item)
	{
		$JSON_Submitted_temp= json_decode($JSON_Submitted);
		if($JSON_Submitted_temp->type=='LandingPage' && (isset($JSON_Submitted_temp->micrositeId)))
		{
			$getLPMSIdForExistingLP=$this->getTargetMSIDIfExists($JSON_Submitted_temp->name,$Package_Item->Deployment_Package_Id);
			if($getLPMSIdForExistingLP>0)
			{
				$JSON_Submitted_temp->micrositeId=$getLPMSIdForExistingLP;
				$result=$JSON_Submitted_temp;
			}	
			else if($getLPMSIdForExistingLP==-1)
			{
				if($Package_Item->Target_Microsite_JSON !=false)
				{	
					$replaceMSID = json_decode($Package_Item->Target_Microsite_JSON)->id;
					$JSON_Submitted_temp->micrositeId=$replaceMSID;
					$result=$JSON_Submitted_temp;	
				}
				else
				{
					$JSON_Submitted_temp->micrositeId= "2";
					$result=$JSON_Submitted_temp;
				}				
			}
			else
			{
				$result=$JSON_Submitted_temp;
			}			
		}
		else
		{
			$result=$JSON_Submitted_temp;
		}
		
		
		return json_encode($result);	
	}
	
	// function checkForSelectedMicrosite($AssetId,$Package_Id)
	// {
		// $this -> db -> select('Target_Microsite_JSON');
		// $this -> db -> from('deployment_package_item');
		// $this -> db -> where('Asset_Type', 'Landing Page');
		// $this -> db -> where('Asset_Id', 'AssetId');
		// $this -> db -> where('deployment_package_id', $Package_Id);
		// $query = $this -> db -> get();
		
		// if($query->num_rows()>0)
		// {
			// print_r($query->result());
			// $result=
		// }
		// else
		// {
			// $result=-2;
		// }
		// return $result;
	// }
	
	function getTargetMSIDIfExists($targetLPName,$packageId)
	{
		$Package_details = $this->deploy_model->getPackage($packageId);
		$instance_details = $this->deploy_model->getInstance($Package_details[0]->Target_Site_Name);
		
		$token 	   = $instance_details [0] -> Token;
		$Base_Url  = $instance_details [0]-> Base_Url;	
		$targetOrg = $instance_details [0];
		
		$this -> db -> select('*');
		$this -> db -> from('rsys_asset_endpoint');
		$this -> db -> where('Asset_Type', 'Landing Page');
		$this -> db -> where('Endpoint_Type', 'Find Asset');
		$query22 = $this -> db -> get();
		$endpoint = $query22->result();
		
		$url = $Base_Url .$endpoint[0]->Endpoint_URL .'?depth=complete&search="'.str_replace(' ', '%20', $targetLPName).'"';
		
		$result_asset = $this->eloqua->get_request($token, $url);
		$result_asset_=json_decode($result_asset['data'])->elements;
		
		$returnResult='';
		
		// echo '<pre>';
				// print_r($result_asset_[0]); echo '<br>';
		// echo '</pre>';	
			
		if(isset($result_asset_[0]))
		{
			if(isset($result_asset_[0]->micrositeId))
			{
				$returnResult=$result_asset_[0]->micrositeId;
			}
			else
			{
				$returnResult=-1;
			}		
		}
		else
		{
			$returnResult=-2;
		}	
		// print_r($returnResult);
		return $returnResult;
	}
	
	function LPFoundInTarget($targetLPName,$packageId)
	{
		$Package_details = $this->deploy_model->getPackage($packageId);
		$instance_details = $this->deploy_model->getInstance($Package_details[0]->Target_Site_Name);
		
		$token 	   = $instance_details [0] -> Token;
		$Base_Url  = $instance_details [0]-> Base_Url;	
		$targetOrg = $instance_details [0];
		
		$this -> db -> select('*');
		$this -> db -> from('rsys_asset_endpoint');
		$this -> db -> where('Asset_Type', 'Landing Page');
		$this -> db -> where('Endpoint_Type', 'Find Asset');
		$query22 = $this -> db -> get();
		$endpoint = $query22->result();
		
		$url = $Base_Url .$endpoint[0]->Endpoint_URL .'?depth=complete&search="'.str_replace(' ', '%20', $targetLPName).'"';
		
		$result_asset = $this->eloqua->get_request($token, $url);
		$result_asset_=json_decode($result_asset['data'])->elements;
					
		if(isset($result_asset_[0]))
		{
			$returnResult='Found';	
		}
		else
		{
			$returnResult='Not Found';
		}	
		return $returnResult;
	}
	
	function CDO_ContactFilter($JSON_Submitted,$Package_Item)
	{
		$JSON_Submitted_temp = json_decode($JSON_Submitted);
		$source_CDO_replace_element = array();
		$source_CDO_replace_element_temp=array();
		$t=0;
		$CDO_Update_Flag2=0;
		 
		if($JSON_Submitted_temp->type=='ContactFilter' && isset($JSON_Submitted_temp->criteria))
		{
			$contactFilter_elements = $JSON_Submitted_temp->criteria;
			$contactFilter_elements_temp = $contactFilter_elements;
			foreach($contactFilter_elements_temp as $sKey=>$sVal)
			{
				if($sVal->type=='LinkedAccountCustomObjectCriterion' || $sVal->type=='LinkedCustomObjectCriterion')
				{					
					foreach($sVal as $sKey2=>$sVal2)
					{
						if($sKey2=='customObjectId')
						{
							$source_CDO_replace_element_temp['customObjectId']=$sVal2;
						}
						if($sKey2=='fieldConditions')
						{
							$i=0;
							foreach($sVal2 as $sKey3=>$sVal3)
							{
								if(isset($sVal3->fieldId))
								{
									$source_CDO_replace_element_temp['fieldConditions'][$i++]=$sVal3->fieldId;
								}
							}	
						}	
					}
					$source_CDO_replace_element[$t++] = $source_CDO_replace_element_temp;	
					$CDO_Update_Flag2=1;
				}
			}
			if($CDO_Update_Flag2==1)
			{
				$k=0;
				foreach($source_CDO_replace_element as $psKey6=>$psVal6)
				{
					$target_fields_ids_contactFilter[$k++]=$this->get_cdo_field_contactFilter($psVal6['customObjectId'],$psVal6['fieldConditions'],$Package_Item->Deployment_Package_Id);
					
				}
				$replaced_fields_contactFilter=$this->replace_contactFilter_CDO_field($target_fields_ids_contactFilter,$contactFilter_elements_temp);
				$JSON_Submitted_temp->criteria = $replaced_fields_contactFilter;
				$result = $JSON_Submitted_temp;
			}
			else 
			{
				$result = $JSON_Submitted_temp;
			}
		}
		else
		{
			$result=$JSON_Submitted_temp;
		}
		return json_encode($result);	
	}
	
	function get_cdo_field_contactFilter($customObjectId,$fieldConditions,$Package_Id)
	{
		$CDO_DB = $this->CDO_from_DB($customObjectId,$Package_Id);
		$source_CDO=$CDO_DB['source_CDO'];
		$target_CDO=$CDO_DB['target_CDO'];
		foreach($source_CDO->fields as $sKey=>$sVal)
		{
			foreach($fieldConditions as $sKey1=>$sVal1)
			{
				if($sVal->id==$sVal1)
				{
					$source_fields_name[]=$sVal->name;
					$source_fields_id[$sVal->name]=$sVal1;
				}
			}	
		}
		foreach($target_CDO->fields as $tKey=>$tVal)
		{
			foreach($source_fields_name as $tKey1=>$tVal1)
			{
				if($tVal->name==$tVal1)
				{
					$target_fields_id[$tVal->name]=$tVal->id;
				}
			}
		}
		$new_target_fields_id = array(); 
		$t=0;
		foreach ($target_fields_id as $iKey => $iVal)
		{
			foreach ($source_fields_id as $iKey1 => $iVal1)
			{
				if($iKey==$iKey1)
				{
					$new_target_fields_id[$iVal1] = $iVal;
				}	
			}
		}
		$tCDO[$source_CDO->id] = $target_CDO->id;
		$tCDO['fieldConditions'] = $new_target_fields_id;
		return $tCDO;
	}
	
	function replace_contactFilter_CDO_field($target_fields_ids_contactFilter,$contactFilter_elements_temp)
	{
		foreach($contactFilter_elements_temp as $sKey=>$sVal)
		{
			if($sVal->type=='LinkedAccountCustomObjectCriterion' || $sVal->type=='LinkedCustomObjectCriterion')
			{	
				foreach($target_fields_ids_contactFilter as $sKey2=>$sVal2)
				{
					foreach($sVal2 as $sKey3=>$sVal3)
					{
						if($sVal->customObjectId==$sKey3)
						{
							$sVal->customObjectId=$sVal3;
						}
						if(isset($sVal->fieldConditions))
						{
							if($sKey3=='fieldConditions')
							{
								foreach($sVal3 as $sKey4=>$sVal4)
								{
									foreach($sVal->fieldConditions as $sKey5=>$sVal5)
									{
										if($sVal5->fieldId==$sKey4)
										{
											$sVal5->fieldId=$sVal4;
										}	
									}
								}
							}
						}	
					}
				}
			}
		}
		return $contactFilter_elements_temp;
	}
	
	//Function to Check Email in target with different Email Group
	function EmailWithDifferentEG($packageId)
	{
		$this -> db -> select('Deployment_Package_Item_Id,JSON_Asset');
		$this -> db -> from('deployment_package_item');
		$this -> db -> where('Asset_Type', 'Email');
		$this -> db -> where('deployment_package_id', $packageId);
		$this -> db -> where('verified', 1);
		$query = $this -> db -> get();
		$Emails = $query->result();
		$i=0;
		$emailResults[]=array();
		$emailGroupFound=false;
		foreach($Emails as $EKey=>$EVal)
		{
			$Email = json_decode($EVal->JSON_Asset);
			if(isset($Email->emailGroupId))
			{
				$emailGroupFound=true;
				$emailResults[$i]['EmailName']=$Email->name;
				$emailResults[$i]['EmailGroupName']=$this->getEmailGroupNameById($Email->emailGroupId,$packageId,'source');
				$emailResults[$i++]['Deployment_Package_Item_Id']=$EVal->Deployment_Package_Item_Id;
			}
		}
		if($emailGroupFound)
		{
			$pathValue='';
			foreach($emailResults as $EKey1=>$EVal1)
			{
				$DiffEGStatus=$this->getTargetEmailByName($EVal1['EmailName'],$packageId,$EVal1['EmailGroupName']);
				// print_r($DiffEGStatus);
				if($DiffEGStatus>=1)
				{
					$this->updateDeploymentPackageItem($EVal1['Deployment_Package_Item_Id'],$pathValue,$message='Email with different email group exists in the target');
					$this->updateDeploymentPackage($packageId);
				}	
			}
		}	
		// echo '<pre>';
			// print_r($emailResults);
		// echo '</pre>';
	}
	
	function getEmailGroupNameById($EGID,$packageId,$destination)
	{
		$Package_details = $this->deploy_model->getPackage($packageId);
		if($destination=='target')
		{
			$instance_details = $this->deploy_model->getInstance($Package_details[0]->Target_Site_Name);
		}
		else if($destination=='source')
		{
			$instance_details = $this->deploy_model->getInstance($Package_details[0]->Source_Site_Name);
		}	
		
		$token 	   = $instance_details [0] -> Token;
		$Base_Url  = $instance_details [0]-> Base_Url;	
		$targetOrg = $instance_details [0];
		
		$this -> db -> select('*');
		$this -> db -> from('rsys_asset_endpoint');
		$this -> db -> where('Asset_Type', 'Email Group');
		$this -> db -> where('Endpoint_Type', 'Read Single');
		$query22 = $this -> db -> get();
		$endpoint = $query22->result();
		
		$url = $Base_Url .$endpoint[0]->Endpoint_URL .'/'.$EGID;
		$result_asset = $this->eloqua->get_request($token, $url);
		$result_asset_=json_decode($result_asset['data']);
		
		if(isset($result_asset_))
		{
			$returnResult=$result_asset_->name;
		}
		else
		{
			$returnResult='';
		}
		return 	$returnResult;
	}
	
	//Function to get target Email and check Email & Email group are same in source & target
	function getTargetEmailByName($emailName,$packageId,$EmailGroupName)
	{
		$Package_details = $this->deploy_model->getPackage($packageId);
		$instance_details = $this->deploy_model->getInstance($Package_details[0]->Target_Site_Name);	
		
		$token 	   = $instance_details [0] -> Token;
		$Base_Url  = $instance_details [0]-> Base_Url;	
		
		$this -> db -> select('*');
		$this -> db -> from('rsys_asset_endpoint');
		$this -> db -> where('Asset_Type', 'Email');
		$this -> db -> where('Endpoint_Type', 'Find Asset');
		$query22 = $this -> db -> get();
		$endpoint = $query22->result();
		
		$url = $Base_Url .$endpoint[0]->Endpoint_URL .'?depth=complete&search="'.urlencode($emailName).'"';
		$result_asset = $this->eloqua->get_request($token, $url);
		$result_asset_=json_decode($result_asset['data'])->elements;
	
		$i=0;
		$emailWithEGStatus=0;
		$emailTargetResults=array();
		if(isset($result_asset_))
		{
			foreach($result_asset_ as $Ekey1=>$Eval1)
			{
				// echo '<pre>';
					// print_r($result_asset);
				// echo '</pre>';
				
				if(!isset($Eval1->emailGroupId))
				{	
					$emailWithEGStatus=0;
					break;
				}				
				else
				{	
					if(json_decode($result_asset['data'])->total>1)
					{
						$emailWithEGStatus=0;
						break;

					}
					else
					{
						$emailWithEGStatus=1;
						$emailTargetResults[$i]['EmailName']=$Eval1->name;
						$emailTargetResults[$i++]['EmailGroupName']=$this->getEmailGroupNameById($Eval1->emailGroupId,$packageId,'target');
					}		
					
				}		
			}
			// echo '<pre>';
					// print_r('emailWithEGStatus :' .$emailWithEGStatus .'<br>');
					// print_r($emailTargetResults);
			// echo '</pre>';
			if($emailWithEGStatus==1 && isset($emailTargetResults))
			{	
				foreach($emailTargetResults as $Ekey2=>$Eval2)
				{
					if(($emailName==$Eval2['EmailName']) && ($EmailGroupName==$Eval2['EmailGroupName']))
					{
						$returnValue=-2;
					}
					else
					{
						$returnValue=1;
						break;
					}	
				}
			}
			else
			{
				$returnValue=-1;	
			}		
		}
		else
		{
			$returnValue=-1;
		}
		// print_r($returnValue);
		return $returnValue;
	}
	
	function getSourceJSONById($AssetId,$AssetType,$packageId)
	{
		$Package_details = $this->deploy_model->getPackage($packageId);
			
		$this -> db -> select('Base_Url, Token, Site_Name, Token_Expiry_Time, Refresh_Token');
		$this -> db -> from('instance_ inst');	
		$this -> db -> where('inst.Site_Name',$Package_details[0]->Source_Site_Name);
		$this -> db -> limit(1);
		$query23   = $this -> db -> get();		
		$token 	   = $query23 -> result()[0] -> Token;
		$Base_Url  = $query23 -> result()[0] -> Base_Url;	
		$targetOrg = $query23 -> result()[0];
		
		if(time()+1800 > $query23 -> result()[0] -> Token_Expiry_Time)
		{
		   $result2 = $this -> eloqua -> refreshToken($targetOrg);
		   $query -> result()[0] -> Token_Expiry_Time = $result2['Token']['access_token'];
		}
		
		$this -> db -> select('*');
		$this -> db -> from('rsys_asset_endpoint rae');
		$this -> db -> where('rae.Asset_Type', $AssetType);
		$this -> db -> where('rae.Endpoint_Type', 'Read Single');
		$query22 = $this -> db -> get();
		$endpoint = $query22->result();
		
		$url = $Base_Url .$endpoint[0]->Endpoint_URL .'/'.$AssetId ."?depth=complete";
		$result_asset = $this->eloqua->get_request($token, $url);		
		$result_asset = json_decode($result_asset['data']);	
		return $result_asset;
	}
	
	function handleFolderStructure($packageItem,$jsonSubmitted)
	{
		$jsonSubmittedDecoded = json_decode($jsonSubmitted);
		$assetType = $packageItem->Asset_Type;
		$currentAssetFolderId = $jsonSubmittedDecoded->folderId;
		$folderArray = array();
		$folderId = '';
		if($packageItem->verified == 1)
		{
			$folderId = $this->buildFolderArray($packageItem,$currentAssetFolderId,$folderArray);
		}
		return $folderId;
	}
	
	function buildFolderArray($packageItem,$currentAssetFolderId,&$folderArray)
	{
		$this->db->select('Endpoint_URL');
		$this->db->from('rsys_asset_endpoint');
		$this->db->where('Asset_Type',$packageItem->Asset_Type);
		$this->db->where('Endpoint_Type','Read Single');
		$endpointQuery = $this->db->get();
		$endpointResult = $endpointQuery->result_array();
		
		$this -> db -> select('Base_Url, Token, Site_Name, Token_Expiry_Time, Refresh_Token');
		$this -> db -> from('instance_ inst');				
		$this -> db -> join('deployment_package dp','dp.Target_Site_Name = inst.Site_Name', 'LEFT OUTER');
		$this -> db -> join('deployment_package_item dpi','dpi.Deployment_Package_Id= dp.Deployment_Package_Id', 'LEFT OUTER');
		$this -> db -> where('dpi.Deployment_Package_Id', $packageItem->Deployment_Package_Id);
		$this -> db -> limit(1);
		$queryTarget		= $this -> db -> get();
		$tokenTarget		= $queryTarget -> result()[0] -> Token;
		$Base_UrlTarget 	= $queryTarget -> result()[0] -> Base_Url;		
		$orgTarget	= $queryTarget -> result()[0];
		
		$this -> db -> select('Base_Url, Token, Site_Name, Token_Expiry_Time, Refresh_Token');
		$this -> db -> from('instance_ inst');				
		$this -> db -> join('deployment_package dp','dp.Source_Site_Name = inst.Site_Name', 'LEFT OUTER');
		$this -> db -> join('deployment_package_item dpi','dpi.Deployment_Package_Id= dp.Deployment_Package_Id', 'LEFT OUTER');
		$this -> db -> where('dpi.Deployment_Package_Id', $packageItem->Deployment_Package_Id);
		$this -> db -> limit(1);
		$querySource		= $this -> db -> get();
		$tokenSource		= $querySource -> result()[0] -> Token;
		$Base_UrlSource 	= $querySource -> result()[0] -> Base_Url;		
		$orgSource	= $querySource -> result()[0];
		
		$sourceFoldersArray = array();
		$targetFoldersArray = array();
		// print_r($orgSource);
		$folderId = '';
		if($endpointQuery->num_rows()>0)
		{
			$sourceUrl = $Base_UrlSource.$endpointResult[0]['Endpoint_URL'].'/folders?depth=complete';
			$pendingPagesSource = true;
			$i=1;
			while($pendingPagesSource)
			{
				$sourceUrl = $Base_UrlSource.$endpointResult[0]['Endpoint_URL'].'/folders?depth=complete&page='.$i;
				$resultSource = $this->eloqua->get_request($tokenSource,$sourceUrl);
				if($resultSource['httpCode'] == 200)
				{
					if(!empty($resultSource['data']))
					{
						$tempSourceFoldersArray = $sourceFoldersArray;
						$tempArrayList = json_decode($resultSource['data'],true)['elements'];
						if(!empty($tempArrayList))
						{
							$sourceFoldersArray = array_merge($tempSourceFoldersArray,$tempArrayList);
							$i++;
						}
						else
						{
							$pendingPagesSource = false;
						}
					}
					
				}
				
				
			}
			
			
			$targetUrl = $Base_UrlTarget.$endpointResult[0]['Endpoint_URL'].'/folders?depth=complete';
			$resultTarget = $this->eloqua->get_request($tokenTarget,$targetUrl);
			if($resultSource['httpCode'] == 200)
			{
				if(!empty($resultSource['data']))
				{
					$targetFoldersArray = json_decode($resultSource['data'],true)['elements'];
					//print_r(json_decode($resultSource['data'],true)['elements']);
				}
				
			}
			
			if((!empty($sourceFoldersArray)))// && (!empty($targetFoldersArray)))
			{
				$this->buildSourceFoldersArray($sourceFoldersArray,$currentAssetFolderId,$folderArray);
				// $parentFolder = $this->folderFromId($sourceFoldersArray,$currentAssetFolderId);
				// print_r(min(array_keys($folderArray)));
				// print_r('<pre>');
				// print_r($sourceFoldersArray);
				// print_r('<pre>');
				//print_r($folderArray);
				$folderId = $this->createFoldersInTarget($folderArray,$targetFoldersArray,$packageItem,$tokenTarget,$Base_UrlTarget.$endpointResult[0]['Endpoint_URL'].'/folder');
				
			}
		}
		if($folderId == '')
		{
			return -1;
		}
		return $folderId;
	}
	
	function buildSourceFoldersArray($sourceFoldersArray,$currentAssetFolderId,&$folderArray)
	{
		$parentFolder = $this->folderFromId($sourceFoldersArray,$currentAssetFolderId);
		$folderArray[$currentAssetFolderId] = $parentFolder;
		if(isset($parentFolder) && (!empty($parentFolder)) &&(isset($parentFolder['folderId'])))
		{
			$this->buildSourceFoldersArray($sourceFoldersArray,$parentFolder['folderId'],$folderArray);
		}
	}
	
	function folderFromId($folderArray,$folderId)
	{
		foreach($folderArray as $key=>$val)
		{
			if($val['id'] == $folderId)
			{
				return $val;
			}
		}
	}
	
	function createFoldersInTarget(&$folderArray,$targetFoldersArray,$packageItem,$tokenTarget,$targetUrl)
	{
		$parentFolderId = '';
		// print_r($folderArray);
		while(!empty($folderArray))
		{
			// print_r('<br>');
			$FoldersCreatedHere = array();
			//$tempFolder = $folderArray[min(array_keys($folderArray))];
			$tempFolder = end($folderArray);
				// print_r($tempFolder);
				// print_r('<br>');
			$folderId = $this->checkForTargetFolderByName($tempFolder['name'],$packageItem,$parentFolderId);
			// print_r($folderId);
			if($folderId != -1)
			{
				$parentFolderId = $folderId;
			}
			else
			{
				$newFolder['name'] = $tempFolder['name'];
				$newFolder['type'] = $tempFolder['type'];
				if(isset($parentFolderId))
				{
					$newFolder['folderId'] = $parentFolderId;
				}
				$jsonNewFolder = json_encode($newFolder);
				$result = $this->eloqua->postRequest($tokenTarget,$targetUrl,$jsonNewFolder);
				if($result['httpCode'] == 201)
				{
					$parentFolderId = json_decode($result['Output_result'])->id;
				}
				
			}
			array_pop($folderArray);
			// unset($folderArray[min(array_keys($folderArray))]);

			// if(sizeof($folderArray)>=1)
			// {
				// unset($folderArray[sizeof($folderArray)-1]);
			// }
			
		}
		if($parentFolderId == '')
		{
			return -1;
		}
		return $parentFolderId;
	}
	
	function checkForTargetFolderByName($name,$packageItem,$parentFolderId)
	{
		/*
		print_r('<br>');
		print_r('---------');
		print_r('<br>');
	
		print_r($name);
		print_r('<br>');
		print_r($parentFolderId);
		print_r('<br>');
		print_r('---------');
		*/
		$this->db->select('Endpoint_URL');
		$this->db->from('rsys_asset_endpoint');
		$this->db->where('Asset_Type',$packageItem->Asset_Type);
		$this->db->where('Endpoint_Type','Read Single');
		$endpointQuery = $this->db->get();
		$endpointResult = $endpointQuery->result_array();
		
		$this -> db -> select('Base_Url, Token, Site_Name, Token_Expiry_Time, Refresh_Token');
		$this -> db -> from('instance_ inst');				
		$this -> db -> join('deployment_package dp','dp.Target_Site_Name = inst.Site_Name', 'LEFT OUTER');
		$this -> db -> join('deployment_package_item dpi','dpi.Deployment_Package_Id= dp.Deployment_Package_Id', 'LEFT OUTER');
		$this -> db -> where('dpi.Deployment_Package_Id', $packageItem->Deployment_Package_Id);
		$this -> db -> limit(1);
		$queryTarget		= $this -> db -> get();
		$tokenTarget		= $queryTarget -> result()[0] -> Token;
		$Base_UrlTarget 	= $queryTarget -> result()[0] -> Base_Url;		
		$orgTarget	= $queryTarget -> result()[0];
		
		$foundFolder=0;
		if($endpointQuery->num_rows()>0)
		{
			$targetUrl = $Base_UrlTarget.$endpointResult[0]['Endpoint_URL'].'/folders?search="'.urlencode($name).'"&depth=complete';
			$resultTarget = $this->eloqua->get_request($tokenTarget,$targetUrl);
			// print_r($resultTarget);
			if($resultTarget['httpCode'] == 200)
			{
				if(!empty($resultTarget['data']))
				{
					$targetFoldersArray = json_decode($resultTarget['data'],true)['elements'];
					if(!empty($targetFoldersArray))
					{
						if(sizeof($targetFoldersArray)>1)
						{
							for($i = 0; $i < sizeof($targetFoldersArray); $i++)
							{
								if($parentFolderId != '')
								{
									if(isset($targetFoldersArray[$i]['folderId']))
									{
										if($targetFoldersArray[$i]['folderId'] == $parentFolderId)
										{
											$foundFolder = $targetFoldersArray[$i];
											break;
										}
									}
								}
								else
								{
									$foundFolder = $targetFoldersArray[$i];
									break;
								}
							}
						}
						else
						{
							if($parentFolderId != '')
							{
								if(isset($targetFoldersArray[0]['folderId']))
								{
									if($targetFoldersArray[0]['folderId'] == $parentFolderId)
									{
										$foundFolder = $targetFoldersArray[0];
									}
								}
							}
							else
							{
								$foundFolder = $targetFoldersArray[0];
							}
						}
					}
				}
				
			}
		}
		if(isset($foundFolder) && $foundFolder != 0)
		{
			return $foundFolder['id'];
		}
		return -1;
	}
	
	function hyperlinkAsLP($JSON_Submitted,$instance,$Package_Item)
	{
		$JSON_Submitted_temp=json_decode($JSON_Submitted);
		$allHyperlink=$this->getTargetHyperlinks($Package_Item->Deployment_Package_Id);
		if($JSON_Submitted_temp->type=='Hyperlink' && $JSON_Submitted_temp->hyperlinkType=='LandingPageURL')
		{	
			$hyperlinkName=$JSON_Submitted_temp->name;
			$hyperlinkHref=$JSON_Submitted_temp->href;
			$hyperlinkName=strstr($hyperlinkName, '(', true);	
			$newHyperlinkJSON=$this->checkExistingHyperlink($hyperlinkName,$hyperlinkHref,$allHyperlink,$JSON_Submitted_temp);
			$result=$newHyperlinkJSON;
		}	
		else
		{
			$result=$JSON_Submitted_temp;
		}
		
		return json_encode($result);
	}
	
	function checkExistingHyperlink($hyperlinkName,$hyperlinkHref,$allHyperlink,$JSON_Submitted)
	{
		foreach($allHyperlink as $hKey=>$hVal)
		{	
			if(strpos($hVal->name,$hyperlinkName) !== false && $hVal->hyperlinkType=='LandingPageURL' && $hVal->href==$hyperlinkHref) 
			{
				$newJSONSubmitted=$JSON_Submitted;
				//$newJSONSubmitted->folderId=$hVal->folderId;
				$newJSONSubmitted->name=$hVal->name;
				$newJSONSubmitted->href=$hVal->href;
				$newJSONSubmitted->referencedEntityId=$hVal->referencedEntityId;
				$newJSONSubmitted = json_decode(json_encode($newJSONSubmitted),true);
				$newJSONSubmitted['id']= $hVal->id;
				$newJSONSubmittedTemp = $newJSONSubmitted;
				break;
			}
			else if(strpos($hVal->name,$hyperlinkName) !== false && $hVal->hyperlinkType=='LandingPageURL' && $hVal->href != $hyperlinkHref) 
			{
				$newJSONSubmitted=$JSON_Submitted;
				//$newJSONSubmitted->folderId=$hVal->folderId;
				$newJSONSubmitted->name=$hVal->name;
				$newJSONSubmitted->href=$hVal->href;
				$newJSONSubmitted->referencedEntityId=$hVal->referencedEntityId;
				$newJSONSubmitted = json_decode(json_encode($newJSONSubmitted),true);
				$newJSONSubmitted['id']= $hVal->id;
				$newJSONSubmittedTemp = $newJSONSubmitted;
				break;
			}	
			else
			{
				$newJSONSubmittedTemp = $JSON_Submitted;
			}	
		}
		return $newJSONSubmittedTemp;
	}

	function checkForTokenAvailability($packageId)
	{
		$this->db->select('*');
		$this->db->from('deployment_package');
		$this->db->where('Deployment_Package_Id',$packageId);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			$result = $query->result();
			
			$sourceSiteName = $result[0]->Source_Site_Name;
			$targetSiteName = $result[0]->Target_Site_Name;
			
			$this->db->select('*');
			$this->db->from('instance_');
			$this->db->where('Site_Name',$sourceSiteName);
			$querySourceInstance = $this->db->get();
			$resultSourceInstance = $querySourceInstance->result();
			//print_r($resultSourceInstance);
			$this->db->select('*');
			$this->db->from('instance_');
			$this->db->where('Site_Name',$targetSiteName);
			$queryTargetInstance = $this->db->get();
			$resultTargetInstance = $queryTargetInstance->result();
			// print_r($resultTargetInstance);
			if($querySourceInstance->num_rows()>0 && $queryTargetInstance->num_rows()>0)
			{
				// print_r('hi');
				if(($resultSourceInstance[0]->Token == '' || $resultSourceInstance[0]->Refresh_Token == '') && 
					($resultTargetInstance[0]->Token == '' || $resultTargetInstance[0]->Refresh_Token == ''))
				{
					return "bothTokensMissing";
				}
				else if(($resultSourceInstance[0]->Token == '' || $resultSourceInstance[0]->Refresh_Token == ''))
				{
					return "sourceTokenMissing";
				}
				else if($resultTargetInstance[0]->Token == '' || $resultTargetInstance[0]->Refresh_Token == '')
				{
					return "targetTokenMissing";
				}
				else if(($resultSourceInstance[0]->Token != '' && $resultSourceInstance[0]->Refresh_Token != '') && 
					($resultTargetInstance[0]->Token != '' && $resultTargetInstance[0]->Refresh_Token != ''))
				{
					return "success";
				}	
			}
		}
	}
	
	function getSourceAssetNameById($AssetId,$AssetType,$packageId)
	{
		$Package_details = $this->deploy_model->getPackage($packageId);		
		$instance_details = $this->deploy_model->getInstance($Package_details[0]->Source_Site_Name);
		
		$token 	   = $instance_details [0] -> Token;
		$Base_Url  = $instance_details [0]-> Base_Url;	
		$targetOrg = $instance_details [0];
		
		$this -> db -> select('*');
		$this -> db -> from('rsys_asset_endpoint');
		$this -> db -> where('Asset_Type', $AssetType);
		$this -> db -> where('Endpoint_Type', 'Read Single');
		$query22 = $this -> db -> get();
		$endpoint = $query22->result();
		
		$url = $Base_Url .$endpoint[0]->Endpoint_URL .'/'.$AssetId;
		$result_asset = $this->eloqua->get_request($token, $url);
		$result_asset_=json_decode($result_asset['data']);
		
		if(isset($result_asset_))
		{
			$returnResult=$result_asset_->name;				
		}
		else
		{
			$returnResult='';
		}
		return 	$returnResult;
	}
	
	function getTargetAssetIdBySourceAssetName($AssetName,$AssetType,$packageId)
	{
		$Package_details = $this->deploy_model->getPackage($packageId);
		$instance_details = $this->deploy_model->getInstance($Package_details[0]->Target_Site_Name);
		
		$token 	   = $instance_details [0] -> Token;
		$Base_Url  = $instance_details [0]-> Base_Url;	
		$targetOrg = $instance_details [0];
		
		$this -> db -> select('*');
		$this -> db -> from('rsys_asset_endpoint');
		$this -> db -> where('Asset_Type', $AssetType);
		$this -> db -> where('Endpoint_Type', 'Find Asset');
		$query22 = $this -> db -> get();
		$endpoint = $query22->result();
		
		$url = $Base_Url .$endpoint[0]->Endpoint_URL .'?search="'.urlencode($AssetName) .'"';
		$result_asset = $this->eloqua->get_request($token, $url);
		$result_asset_=json_decode($result_asset['data']);
		
		if(isset($result_asset_->elements))
		{
			$returnResult=$result_asset_->elements[0]->id;
		}
		else
		{
			$returnResult='';
		}
		return 	$returnResult;
	}
	
	function checkForCampaignInputs($packageId)
	{
		$this->db->select('*');
		$this->db->from('deployment_package_item');
		$this->db->where('Deployment_Package_Id',$packageId);
		$this->db->where('Asset_Type','Campaign');
		$this->db->where('verified', 1);
		$query = $this->db->get();
		
		if($query->num_rows()>0)
		{			
			$result = $query->result();
			foreach($result as $key=>$val)
			{
				$assetJson = json_decode($val->JSON_Asset);
				if(isset($assetJson->elements) && (count($assetJson->elements)>0))
				{					
					foreach($assetJson->elements as $elementKey=>$elementVal)
					{
						if($elementVal->type == 'CampaignInput')
						{
							if(isset($elementVal->source))
							{
								$sourceType = $elementVal->source->type;
								print_r($sourceType);
								$sourceId = $elementVal->source->id;
								$childAssetFound = $this->checkForAssetInPackage($packageId,$sourceType,$sourceId);
								if($childAssetFound == 0)
								{
									$message = "Add the missing assetType assetName to the package";
									$message = str_replace("assetType",$sourceType,$message);
									$message = str_replace("assetName",$elementVal->source->name,$message);
									print_r($message);
									$this->updateDeploymentPackage($packageId);
									$this->updateDeploymentPackageItem($val->Deployment_Package_Item_Id,'',$message);
								}
							}
						}
					}
				}
			}
		}		
	}
	
	function checkForAssetInPackage($packageId,$assetType,$assetId)
	{
		$this->db->select('Asset_Type');
		$this->db->from('deployment_package_item');
		$this->db->where('Deployment_Package_Id',$packageId);
		$this->db->where('Asset_Type',$assetType);
		$this->db->where('Asset_Id',$assetId);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
	
	function checkForLandingPageWithoutMicrositeForChildOfHyperlink($packageId)
	{
		$this->db->select('*');
		$this->db->from('deployment_package_item');
		$this->db->where('Deployment_Package_Id',$packageId);
		$this->db->where('Asset_Type','Hyperlink');
		$this->db->where('verified', 1);
		$query = $this->db->get();
		
		if($query->num_rows()>0)
		{			
			$result = $query->result();
			foreach($result as $key=>$val)
			{
				$assetJson = json_decode($val->JSON_Asset);
				if(isset($assetJson->referencedEntityId))
				{
					$sourceLPName=$this->getSourceAssetNameById($assetJson->referencedEntityId,'Landing Page',$packageId);
					$targetMicrositeFoundFlag=$this->getTargetAssetJSONBySourceAssetName($sourceLPName,'Landing Page',$packageId);
					if($targetMicrositeFoundFlag=='Microsite Not Found')
					{
						$IncludedInPackageFlag=$this->checkForAssetsIncludedInPackage($packageId,'Landing Page',$assetJson->referencedEntityId);
						// print_r($IncludedInPackageFlag);
						if(!$IncludedInPackageFlag)
						{
							$message = "Add the missing Landing Page assetName to the package";
							$message = str_replace("assetName",strstr($assetJson->name,'(',true),$message);
							$this->updateDeploymentPackage($packageId);
							$this->updateDeploymentPackageItem($val->Deployment_Package_Item_Id,'',$message);
						}
					}	
				}	
			}
		}		
	}
	
	function getTargetAssetJSONBySourceAssetName($AssetName,$AssetType,$packageId)
	{
		$Package_details = $this->deploy_model->getPackage($packageId);
		$instance_details = $this->deploy_model->getInstance($Package_details[0]->Target_Site_Name);
		
		$token 	   = $instance_details [0] -> Token;
		$Base_Url  = $instance_details [0]-> Base_Url;	
		$targetOrg = $instance_details [0];
		
		$this -> db -> select('*');
		$this -> db -> from('rsys_asset_endpoint');
		$this -> db -> where('Asset_Type', $AssetType);
		$this -> db -> where('Endpoint_Type', 'Find Asset');
		$query22 = $this -> db -> get();
		$endpoint = $query22->result();
		
		$url = $Base_Url .$endpoint[0]->Endpoint_URL .'?search="*'.urlencode($AssetName) .'*"&depth=complete';
		
		$result_asset = $this->eloqua->get_request($token, $url);
		$result_asset_=json_decode($result_asset['data']);
		
		if(isset($result_asset_->elements))
		{
			if(isset($result_asset_->elements[0]->micrositeId))
			{
				$returnResult='Microsite Found';
			}
			else
			{
				$returnResult='Microsite Not Found';
			}	
		}
		else
		{
			$returnResult='LP Not Exists';
		}
		return 	$returnResult;
	}
	
	function checkForAssetsIncludedInPackage($packageId,$assetType,$assetId)
	{
		$this->db->select('Asset_Type');
		$this->db->from('deployment_package_item');
		$this->db->where('Deployment_Package_Id',$packageId);
		$this->db->where('Asset_Type',$assetType);
		$this->db->where('Asset_Id',$assetId);
		$this->db->where('verified',1);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
}	