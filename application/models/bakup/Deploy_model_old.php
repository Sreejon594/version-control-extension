<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require(APPPATH.'/third_party/json_path/vendor/autoload.php');
use Flow\JSONPath\JSONPath;
use Flow\JSONPath\JSONPathLexer;
use \Peekmo\JsonPath\JsonPath as PeekmoJsonPath;
use \Peekmo\JsonPath\JsonStore;
class Deploy_model_old extends CI_Model
{
	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->load->model('eloqua','',TRUE);
		$this->load->model('logging_model','',TRUE);		
    }
	
	protected $globalStack = array();
	
	//read the department list from db
    function get_OrgList($id)
    {
		$this->db->select('id,users_id,base_url,userid,password');
		$this -> db -> from('Eloqua_Instance');
		$this -> db -> where('users_id',$id);
		$this -> db -> where('enable',1);
		$result = $this->db->get()->result();
		return $result;
    }
	
	function get_OrgInfo($id,$orgid)
    {
		$this->db->select('id,users_id,base_url,userid,password');
		$this -> db -> from('Eloqua_Instance');
		$this -> db -> where('users_id',$id);
		$this -> db -> where('id',$orgid);
		$this -> db -> where('enable',1);
		$result = $this->db->get()->result();
		return $result;
    }
	
	function new_Org($id ,$data)
    {
		$this->db->select('id');
		$this -> db -> from('Eloqua_Instance');
		$this -> db -> where('userid',$data['userid']);
		$temp =$this->db->count_all_results();
		if($temp==0)
		{									
			$this->db->select_max('id');
			$this -> db -> from('Eloqua_Instance');
			$temprow = $this->db->get()->result();
			$data['id']=$temprow[0]->id+1;	
			$data['enable']=1;	
			$data['users_id']= $id;	
			$this->db->insert('Eloqua_Instance', $data); 		
			$result['id'] = $data['id'];
			$result['success'] = true;		
			return $result;
		}
		else
		{
			$result['success'] = false;		
			$result['msg'] = 'login Name Can\'t be Duplicate.';		
			return $result;
		}
	}
	
	function delete_Org($id , $data)
    {
		$this->db->where_in('id', $data['id']);
		$this->db->where('users_id', $id);
		$this->db->delete('Eloqua_Instance');
	}
	
	function update_Org($id , $data)
    {
		$this->db->select('id');
		$this -> db -> from('Eloqua_Instance');
		$this -> db -> where('userid',$data['userid']);
		$this -> db -> where_not_in('id',$data['id']);
		$temp =$this->db->count_all_results();
		if($temp==0)
		{								
			$this->db->where('id', $data['id']);
			$this->db->update('Eloqua_Instance' ,$data);
			$result['success'] = true;		
			$result['msg'] = 'Account is Updated..';		
			return $result;
		}
		else
		{
			$result['success'] = false;		
			$result['msg'] = 'Login Name Can\'t be Duplicate.';		
			return $result;
		}
	}
	
	function get_Preference($id)
    {
		$this->db->select('id,user_id,Contact,Account,CDO,Segments,Filters,Emails,Forms,Landing_Pages ,Campaigns,Programs,Shared_Lists,Email_Groups,Option_Lists,Events,Contact_Views,Account_Views');
		$this -> db -> from('Asset_Type_Preference');
		$this -> db -> where('user_id',$id);
		//$this -> db -> where('enable',1);
		$result = $this->db->get()->result();
		return $result;
    }
	
	function update_Preference($id , $data)
    {
		$this->db->where('user_id', $id);
		$this->db->update('Asset_Type_Preference' ,$data);
		//return $this->db->_error_message();
		$result['success'] = true;		
		$result['msg'] = 'Account is Updated..';		
		return $result;
	}
	
	function deploy_inqueue($package_id)
    {
		$this->db->select('*');
		$this -> db -> from('deployment_queue');
		$this -> db -> where('Deployment_Package_Id',$package_id);
		$this -> db -> where('Status','In Queue');
		$this->db->limit(1);
		$query = $this -> db -> get();
	    $deployment_queue = $query->result();
		
		$date = date('Y-m-d H:i:s');
		$data['Deployment_Package_Id'] = $package_id;
		$data['Status'] = 'In Queue';
		$data['Rn_Create_Date'] = $date; 
		
		$data1['Status'] = 'Deployment In Queue';
		$this->db->where('Deployment_Package_Id',$package_id);
		$this->db->update('deployment_package',$data1);
		
		$msg="";
		try 
		{
			if(sizeof($deployment_queue)==0)
			{
				$this->db->insert('deployment_queue',$data);
				$msg = "success";
			}
			else{
				$msg = "alreadyinqueue";
			}
		} 
		catch (Exception $e) 
		{
			$msg = "Caught exception:".$e->getMessage();
		}		
		return $msg;
	}
	
	function deploy_queue()
    {
		$date = date('Y-m-d H:i:s');		
		$this->db->select('*');
		$this -> db -> from('deployment_queue');
		$this -> db -> where('Status','In Queue');
		$this->db->order_by("Rn_Create_Date", "asc");
		$query = $this -> db -> get();
	    $deployment_queue = $query->result();
		$msg="";
		foreach($deployment_queue as $key=>$val)
		{
			$data1['Status'] = 'In Progress'; 
			$data1['Start_'] = date('Y-m-d H:i:s');
			try 
			{  
				$this->db->where('Deployment_Queue_Id',$val->Deployment_Queue_Id);
				$this->db->update('deployment_queue',$data1);
				
				echo '<br/>Validate Package Function<br/>';
				//$validateResult = $this->eloqua->validatepackage($val->Deployment_Package_Id);
				echo '<br> Validate Package Result'.$validateResult;
				
				echo '<br/>Set Deploy Package Status Function<br/>';
				$deployResult = $this->deploy_package($val->Deployment_Package_Id);
				echo '<br> Validate Package Result'.$deployResult;
				
				echo '<br/>Pre_Deploy_Package_Item Function<br/>';
				$predeployResult = $this->pre_deploy_package_item($val->Deployment_Package_Id,$val->Deployment_Queue_Id);			
				echo '<br> Validate Package Result'.$predeployResult;
				$msg = "success";
			} 
			catch (Exception $e) 
			{
				$msg = "Caught exception:".$e->getMessage();
			}		
		}	
		return $msg;		
	}
	
	
	// function deploy_queue_recursive()
    // {
		// $date = date('Y-m-d H:i:s');		
		// $this->db->select('*');
		// $this -> db -> from('deployment_queue');
		// $this -> db -> where('Status','In Queue');
		// $this->db->order_by("Rn_Create_Date", "asc");
		// $query = $this -> db -> get();
	    // $deployment_queue = $query->result();
		// $msg="";
		// foreach($deployment_queue as $key=>$val)
		// {
			// $data1['Status'] = 'In Progress'; 
			// $data1['Start_'] = date('Y-m-d H:i:s');
			// try 
			// {  
				// $this->db->where('Deployment_Queue_Id',$val->Deployment_Queue_Id);
				// $this->db->update('deployment_queue',$data1);
				
				// echo '<br/>Validate Package Function<br/>';
				// $validateResult = $this->eloqua->validatepackage($val->Deployment_Package_Id);
				// echo '<br> Validate Package Result'.$validateResult;
				
				// echo '<br/>Set Deploy Package Status Function<br/>';
				// $deployResult = $this->deploy_package($val->Deployment_Package_Id);
				// echo '<br> Validate Package Result'.$deployResult;
				
				// echo '<br/>Pre_Deploy_Package_Item Function<br/>';
				// $predeployResult = $this->pre_deploy_package_item_recursive($val->Deployment_Package_Id,$val->Deployment_Queue_Id);			
				// echo '<br> Validate Package Result'.$predeployResult;
				// $msg = "success";
			// } 
			// catch (Exception $e) 
			// {
				// $msg = "Caught exception:".$e->getMessage();
			// }		
		// }	
			
			
		// return $msg;		
	// }

	
	function validate_inqueue($package_id)
    {
		$this->db->select('*');
		$this -> db -> from('validate_queue');
		$this -> db -> where('Deployment_Package_Id',$package_id);
		$this -> db -> where('Status','In Queue');
		$this->db->limit(1);
		$query = $this -> db -> get();
	    $validate_queue = $query->result();
		
		$date = date('Y-m-d H:i:s');
		$data['Deployment_Package_Id'] = $package_id;
		$data['Status'] = 'Validate In Queue';
		$data['Rn_Create_Date'] = $date; 
		
		$data1['Status'] = 'Validate In Queue';
		$this->db->where('Deployment_Package_Id',$package_id);
		$this->db->update('deployment_package',$data1);
		
		$msg="";
		try 
		{
			if(sizeof($validate_queue)==0)
			{
				$this->db->insert('validate_queue',$data);
				$msg = "success";
				return true;
			}
			else
			{
				$msg = "Already Inqueue";
				return true;
			}
			
		} 
		catch (Exception $e) 
		{
			$msg = "Caught exception:".$e->getMessage();
		}		
		return $msg;
	}
	
	function validate_queue_recursive()
	{
		$date = date('Y-m-d H:i:s');		
		$this->db->select('*');
		$this -> db -> from('validate_queue');
		$this -> db -> where('Status','Validate In Queue');
		$this->db->order_by("Rn_Create_Date", "asc");
		$query = $this -> db -> get();
	    $validate_queue = $query->result();
		$msg="";
		foreach($validate_queue as $key=>$val)
		{
			$data1['Status'] = 'In Progress'; 
			$data1['Start_'] = date('Y-m-d H:i:s');
			try 
			{  
				$this->db->where('Validate_Queue_Id',$val->Validate_Queue_Id);
				$this->db->update('validate_queue',$data1);				
				$this->validatepackage_recursive($val->Deployment_Package_Id);
				
				$msg = "success";
				print_r("success");
				$data1['Status'] = 'Processed'; 
				$data1['End_'] = date('Y-m-d H:i:s');
				$this->db->where('Validate_Queue_Id',$val->Validate_Queue_Id);
				$this->db->update('validate_queue',$data1);	
				
				$data2['Status'] = $this->decideStatusValidate($val->Deployment_Package_Id);
				$this->db->where('Deployment_Package_Id',$val->Deployment_Package_Id);
				$this->db->update('deployment_package',$data2);
			} 
			catch (Exception $e) 
			{
				$msg = "Caught exception:".$e->getMessage();
			}		
		}		
		return $msg;
	}
	
	function validatepackage_recursive($packageId)
	{
		$resetdata['Status'] = 'Validating';
		$this -> db -> where('Deployment_Package_Id', $packageId);
		$this -> db -> update('deployment_package_item', $resetdata);
		//Step 2 start
		$this -> db -> select('*');
		$this -> db -> from('deployment_package_item dp');
		$this -> db -> where('dp.Deployment_Package_Id', $packageId);
		$query = $this -> db -> get();
	    $package_item = $query->result();
		
		
		foreach($package_item as $key=>$val)
		{
			$AssetChildData = $this->eloqua->getAssetChild($Package_Id,$val->Deployment_Package_Item_Id, $val->Asset_Id,$val->Asset_Type ,$Endpoint_Type = 'Read Single');
			
			if(is_array($AssetChildData) || is_object($AssetChildData))
			{
				foreach($AssetChildData as $ChildKey=>$ChildVal)
				{
					$data['Deployment_Package_Item_Id'] = $val->Deployment_Package_Item_Id;
					$data['Deployment_Package_Id'] = $val->Deployment_Package_Id;
					$data['Asset_Type'] = $ChildVal['Asset_Type'];
					$data['Asset_Id'] = $ChildVal['Asset_Id'];
					$data['Asset_Name'] = $ChildVal['Display_Name'];
					$data['JSON_Node_Value'] = $ChildVal['JSON_Node_Value'];
					$data['Status'] = 'New';
					$this->db->insert('deployment_package_validation_list',$data);
				}
			}
		}
		
		foreach( $package_item as $key=>$val)
		{
			$this->buildGlobalStack($val,$packageId);
		}
		print_r('<pre>');
		
		$this->updateMissingElements($packageId);
		$this->updateDeployOrdinal($packageId);
	}
	
	function updateDeployOrdinal($packageId)
	{
		foreach($this->globalStack as $key=>$val)
		{
			$data['Deploy_Ordinal'] = $key;
			$this->db->where('Deployment_Package_Item_Id',$val);
			$this->db->update('deployment_package_item',$data);
		}
	}
	
	function updateMissingElements($packageId)
	{
		$this -> db -> select('*');
		$this -> db -> from('deployment_package_item dp');
		$this -> db -> where('dp.Deployment_Package_Id', $packageId);
		$query = $this -> db -> get();
	    $package_item = $query->result();
		
		$this -> db -> select('*');
		$this -> db -> from('deployment_package dp');
		$this -> db -> join('instance_ inst','dp.Target_Site_Name= inst.Site_Name', 'LEFT OUTER');
		$this -> db -> where('dp.Deployment_Package_Id', $packageId);
		// $this -> db -> where('dp.Created_By_Contact_Id', $Contact_Id);
		$this -> db -> limit(1);
		$query = $this -> db -> get();
		$org = $query->result()[0];
		
		foreach( $package_item as $key=>$val)
		{
			//update missing elements
			$this -> db -> select('*');
			$this -> db -> from('rsys_asset_endpoint rae');
			$this -> db -> where('rae.Asset_Type', $val->Asset_Type);
			$this -> db -> where('rae.Endpoint_Type', 'Find Asset');
			$query = $this -> db -> get();
			$endpoint = $query->result();
			if($endpoint)
			{	
				$url = $org->Base_Url .$endpoint[0]->Endpoint_URL .'?search="'.str_replace(' ', '%20', $val->Asset_Name) .'"' ; 
				if(time()+1800 > $org->Token_Expiry_Time)
				{
				   $result2 = $this->eloqua->refreshToken($org);
				   $org->Token = $result2['Token']['access_token'];
				}
				$result = $this->eloqua->get_request($org->Token, $url);
				// if($result['httpCode']==401)
				// {
					 // $result = $this->requestToken($org->Refresh_Token,'refresh_token');
					  // $this->updateTokens($org->Site_Id,$result['access_token'],$result['refresh_token']);
					  // $result = $this->get_request($result['access_token'],$url);
				// }	
				$result = json_decode($result['data']);
				if(isset($result->total) && $result->total >0)
				{
					$pdata['missing_target'] = 0;
					$this->db->where('Deployment_Package_Item_Id',$val->Deployment_Package_Item_Id);
					$this->db->update('deployment_package_item',$pdata);
				}
				else
				{
					$pdata['missing_target'] = 1;
					$this->db->where('Deployment_Package_Item_Id',$val->Deployment_Package_Item_Id);
					$this->db->update('deployment_package_item',$pdata);
				}
			}
		}
	}
	
	
	function validate_queue()
    {		
		$date = date('Y-m-d H:i:s');		
		$this->db->select('*');
		$this -> db -> from('validate_queue');
		$this -> db -> where('Status','Validate In Queue');
		$this->db->order_by("Rn_Create_Date", "asc");
		$query = $this -> db -> get();
	    $validate_queue = $query->result();
		$msg="";
		foreach($validate_queue as $key=>$val)
		{
			$data1['Status'] = 'In Progress'; 
			$data1['Start_'] = date('Y-m-d H:i:s');
			try 
			{  
				$this->db->where('Validate_Queue_Id',$val->Validate_Queue_Id);
				$this->db->update('validate_queue',$data1);				
				$this->eloqua->validatepackage($val->Deployment_Package_Id);
				$msg = "success";
				$data1['Status'] = 'Processed'; 
				$data1['End_'] = date('Y-m-d H:i:s');
				$this->db->where('Validate_Queue_Id',$val->Validate_Queue_Id);
				$this->db->update('validate_queue',$data1);	
				
				$data2['Status'] = $this->decideStatusValidate($val->Deployment_Package_Id);
				$this->db->where('Deployment_Package_Id',$val->Deployment_Package_Id);
				$this->db->update('deployment_package',$data2);
			} 
			catch (Exception $e) 
			{
				$msg = "Caught exception:".$e->getMessage();
			}		
		}		
		return $msg;	
	}
	
	function decideStatusValidate($deploy_package_id)
	{
		$this->db->select('*');
		$this->db->from('deployment_package_item');
		$this->db->where('Deployment_Package_Id',$deploy_package_id);
		$query = $this->db->get();
		$totalRows = $query->num_rows();
		$errored = 0;
		$completed = 0;
		if($totalRows > 0)
		{
			$result = $query->result();
			foreach($result as $key=>$val)
			{
				if($val->Status == 'Invalid')
				{
					$errored++;
				}
				else if($val->Status == 'Valid')
				{
					$completed++;
				}
			}
		}
		if($errored>0)
		{
			return 'Invalid';
		}
		else
		{
			return 'Validate Completed';
		}
	}
	
	
	function deploy_package($packageId)
    {
		$data1['Status'] = 'Deployment In Queue';
		$this->db->where('Deployment_Package_Id',$packageId);
		$this->db->update('deployment_package',$data1);
	}
	
	function invalid_deploy_package_item($packageId)
    {
		$this->db->select('*');
		$this -> db -> from('deployment_package_item');
		$this -> db -> where('Deployment_Package_Id',$packageId);
		$this -> db -> where('Status','Invalid');
		$query123 = $this -> db -> get();
	    $invalid_deploy_package_item = $query123->result();
		
		if(sizeof($invalid_deploy_package_item)>0)
		{
			return false;
		}
		else
		{	
			return true;
		}	
	}
	
	// function pre_deploy_package_item_recursive($packageId)
	// {
		// $invalid_package_item = $this->invalid_deploy_package_item($packageId);
		// $status_details = '';
		// if($invalid_package_item)
		// {
			// $getsearch_endpoints = $this->getalleloqua_endpoints();
			// $package = $this->getPackage($packageId);
			// $instance = $this->getInstance($package[0]->Target_Site_Name);
			// $this->db->select('*');
			// $this -> db -> from('deployment_package_item');
			// $this -> db -> where('Deployment_Package_Id',$packageId);
			// $this->db->order_by('Deploy_Ordinal', 'asc');
			// $query = $this -> db -> get();
			// $deployment_package_items = $query->result();
			// foreach($deployment_package_items as $key=>$val)
			// {
				
				// if(sizeof($instance)>0)
				// {
					// print_r('<pre>');
					// print_r($val);
					// $this->buildGlobalStack($val,$packageId);
				// }
			// }
			
		// }
		
		// print_r($this->globalStack);
		// exit;

	// }
	
	function updateValidStatus($package_item_id)
	{
		$data['Status'] = 'Valid';
		$this->db->where('Deployment_Package_Item_Id',$package_item_id);
		$this->db->update('deployment_package_item',$data);
	}
	
	function updateDPValidationList($package_item,$ChildVal)
	{
		$this->db->select('*');
		$this->db->from('deployment_package_validation_list');
		$this->db->where('Deployment_Package_Item_Id',$package_item->Deployment_Package_Item_Id);
		$this->db->where('Deployment_Package_Id',$package_item->Deployment_Package_Id);
		$this->db->where('Asset_Type',$ChildVal['Asset_Type']);
		$this->db->where('Asset_Id',$ChildVal['Asset_Id']);
		$this->db->where('Asset_Name',$ChildVal['Display_Name']);
		$query = $this->db->get();
		if($query->num_rows()==0)
		{
			$data['Deployment_Package_Item_Id'] = $package_item->Deployment_Package_Item_Id;
			$data['Deployment_Package_Id'] = $package_item->Deployment_Package_Id;
			$data['Asset_Type'] = $ChildVal['Asset_Type'];
			$data['Asset_Id'] = $ChildVal['Asset_Id'];
			$data['Asset_Name'] = $ChildVal['Display_Name'];
			$data['JSON_Node_Value'] = $ChildVal['JSON_Node_Value'];
			$data['Status'] = 'New';
			$this->db->insert('deployment_package_validation_list',$data);
		}
		
	}
	
	function buildGlobalStack($package_item,$packageId)
	{
		$this->putOnGlobalStack($package_item->Deployment_Package_Item_Id);
		$this->updateValidStatus($package_item->Deployment_Package_Item_Id);
		$childAssets = $this->eloqua->getAssetChild($package_item->Deployment_Package_Id,$package_item->Deployment_Package_Item_Id,$package_item->Asset_Id,$package_item->Asset_Type);
		
		if(is_array($childAssets) || is_object($childAssets))
		{
			foreach($childAssets as $ChildKey=>$ChildVal)
			{
				$this->updateDPValidationList($package_item,$ChildVal);
			}
		}
		// print_r('<pre>');
		// print_r($package_item);
		// print_r($childAssets);
		// exit;
		if(sizeof($childAssets)>0)
		{
			$childAssetArray = $childAssets;
			if(sizeof($childAssetArray)>0)
			{
				for($i=0;$i<sizeof($childAssetArray);$i++)
				{
					$asset_type = $childAssetArray[$i]['Asset_Type'];
					$asset_id = $childAssetArray[$i]['Asset_Id'];
					$childPackageItem = $this->getPackageItem($packageId,$asset_type,$asset_id);
					// print_r($childPackageItem);
					if($childPackageItem!=false)
					{
						$this->buildGlobalStack($childPackageItem,$packageId);
					}
					else
					{
						$pdata['Asset_Id'] = $asset_id;
						$pdata['Asset_Name'] = $childAssetArray[$i]['Display_Name'];
						$pdata['Asset_Type'] = $asset_type;
						$pdata['Deployment_Package_Id'] = $packageId;
						$this->changeset->addtopackage(0,$pdata);
						$childPackageItem = $this->getPackageItem($packageId,$asset_type,$asset_id);
						$this->buildGlobalStack($childPackageItem,$packageId);
					}
				}
				// exit;
			}
		}
	}
	
	function getPackageItem($packageId,$AssetType,$AssetId)
	{
		$this->db->select('*');
		$this -> db -> from('deployment_package_item');
		$this -> db -> where('Deployment_Package_Id',$packageId);
		$this->db->where('Asset_Type',$AssetType);
		$this->db->where('Asset_Id',$AssetId);
		$query = $this -> db -> get();
		$deployment_package_items = $query->result();
		if($query->num_rows()>0)
		{
			return $deployment_package_items[0];
		}
		return false;
	}
	
	function putOnGlobalStack($currentAsset)
	{
		// print_r($currentAsset);
		$isPresent = array_search($currentAsset, $this->globalStack);
		// print_r($isPresent);
		
		if($isPresent  !== FALSE)
		{
			// print_r('passed');
			// print_r($this->globalStack[array_search($currentAsset, $this->globalStack)]);
			unset($this->globalStack[array_search($currentAsset, $this->globalStack)]);
		}
		// print_r('failed');
		//exit;
		array_push($this->globalStack,$currentAsset);
		// print_r($this->globalStack);
	}
	
	function pre_deploy_package_item($packageId,$queueId)
	{
		$invalid_package_item = $this->invalid_deploy_package_item($packageId);
		$status_details = '';
		print_r('<pre>');
		if($invalid_package_item)
		{
			$getsearch_endpoints = $this->getalleloqua_endpoints();
			
			$package = $this->getPackage($packageId);
			$instance = $this->getInstance($package[0]->Target_Site_Name);
					
			$this->db->select('*');
			$this -> db -> from('deployment_package_item');
			$this -> db -> where('Deployment_Package_Id',$packageId);
			$this->db->order_by('Deploy_Ordinal', 'desc');
			$query123 = $this -> db -> get();
			$deployment_package_item = $query123->result();
			$valid_err = false;
			print_r('<pre>');
			
			$package_item_arr = array();
			foreach($deployment_package_item as $key=>$val)
			{
				if(sizeof($instance)>0 )
				{
					if($val->JSON_Asset!=null)
					{
						$JSON_Asset = json_decode($val->JSON_Asset);
						if($val->Asset_Type="External Asset Type")
						{
							$all_assets = $this->eloqua->get_request($instance[0]->Token, $instance[0]->Base_Url.''.$getsearch_endpoints[str_replace(' ', '', $val->Asset_Type).'_FindAsset']->Endpoint_URL);
							print_r($all_assets);
						}
						else
						{		
							$result = $this->eloqua->get_request($instance[0]->Token, $instance[0]->Base_Url.''.$getsearch_endpoints[str_replace(' ', '', $val->Asset_Type).'_FindAsset']->Endpoint_URL.'?search="'.urlencode($JSON_Asset->name).'"');
							$result_decode = json_decode($result['data']);	
						}
						$xJsonIdReplace = null;
						
						if(isset($result_decode->total))
						{
							if($result_decode->total >0)
							{
								$JSON_Asset = $this->getExclusionResult(json_decode($getsearch_endpoints[str_replace(' ', '', $val->Asset_Type).'_Update']->JSON_Body_Exclusion),$JSON_Asset);
								// print_r('getExclusionResult');
								// print_r($JSON_Asset);
								$JSON_Asset = $this->replaceJSONKey(json_decode($getsearch_endpoints[str_replace(' ', '', $val->Asset_Type).'_Update']->JSON_Key_Replace),$JSON_Asset);
								
								// print_r('replaceJSONKey');
								// print_r($JSON_Asset);
								$JSON_Asset = $this->ChangeParaValue($JSON_Asset,$getsearch_endpoints[str_replace(' ', '', $val->Asset_Type).'_Update']->Change_Para_Config);
							
							
								// print_r('ChangeParaValue');
								// print_r($JSON_Asset);
								if(is_array($JSON_Asset))
								{
									foreach($JSON_Asset as $key1=>$val1)
									{
										if($key1=='id'){
											$xJsonIdReplace[$key1]=$result_decode->elements[0]->id;									
										}
										else
										{
											$xJsonIdReplace[$key1]=$val1;
										}
										
									}
								}
								$data1['JSON_Submitted'] = json_encode($xJsonIdReplace);
								$data1['Target_Asset_Id'] = $result_decode->elements[0]->id;
								// print_r($result_decode->elements[0]->id);
								
							}
							else
							{
								$JSON_Asset = $this->getExclusionResult(json_decode($getsearch_endpoints[str_replace(' ', '', $val->Asset_Type).'_Create']->JSON_Body_Exclusion),$JSON_Asset);
								
								// echo '<pre>pre replaceJSONKey';print_r($JSON_Asset);
								$JSON_Asset = $this->replaceJSONKey(json_decode($getsearch_endpoints[str_replace(' ', '', $val->Asset_Type).'_Create']->JSON_Key_Replace),$JSON_Asset);
								
								$JSON_Asset = $this->ChangeParaValue($JSON_Asset,$getsearch_endpoints[str_replace(' ', '', $val->Asset_Type).'_Create']->Change_Para_Config);
								$data1['JSON_Submitted'] = json_encode($JSON_Asset);
								$data1['Target_Asset_Id'] = 0;
							}
						}
						else
						{
							$JSON_Asset = $this->getExclusionResult(json_decode($getsearch_endpoints[str_replace(' ', '', $val->Asset_Type).'_Create']->JSON_Body_Exclusion),$JSON_Asset);
							
							$JSON_Asset = $this->replaceJSONKey(json_decode($getsearch_endpoints[str_replace(' ', '', $val->Asset_Type).'_Create']->JSON_Key_Replace),$JSON_Asset);
							
							$JSON_Asset = $this->ChangeParaValue($JSON_Asset,$getsearch_endpoints[str_replace(' ', '', $val->Asset_Type).'_Create']->Change_Para_Config);
							
							$data1['JSON_Submitted'] = json_encode($JSON_Asset);
							$data1['Target_Asset_Id'] = 0;
							
						}
						// echo 'pre_deploy </pre>';
							// print_r($data1);
						// echo '</pre>';	
						$this->db->where('Deployment_Package_Item_Id',$val->Deployment_Package_Item_Id);
						$this->db->update('deployment_package_item',$data1);
						
						// echo '<br> update deployment_package_item </br>'; 
						
						$val->Target_Asset_Id = $data1['Target_Asset_Id'];
						$val->JSON_Submitted = $data1['JSON_Submitted'];
						print_r($val);
						$this->getDeploymentPackageValidation($val,$data1['JSON_Submitted'],$instance,$getsearch_endpoints[str_replace(' ', '', $val->Asset_Type).'_Update']->Endpoint_URL);
					}
				}
			}
			// exit;
			
			$data['Status'] = $this->decideStatus($packageId);
			$this->db->where('Deployment_Package_Id',$packageId);
			$this->db->update('deployment_package',$data);
			
			$dqdata1['Status'] = 'Processed';
			$dqdata1['End_'] = date('Y-m-d H:i:s');
			$this->db->where('Deployment_Queue_Id',$queueId);
			$this->db->update('deployment_queue',$dqdata1);
		}
		else
		{
			$data['Status'] = 'Errored';
			$data['Status_Details'] =$status_details ;
			$this->db->where('Deployment_Package_Id',$packageId);
			$this->db->update('deployment_package',$data);
			
			$data1['Status'] = 'Processed � Error';
			$data1['End_'] = date('Y-m-d H:i:s');
			$this->db->where('Deployment_Queue_Id',$queueId);
			$this->db->update('deployment_queue',$data1);
			
		}
	}
	
	function decideStatus($deploy_package_id)
	{
		$this->db->select('*');
		$this->db->from('deployment_package_item');
		$this->db->where('Deployment_Package_Id',$deploy_package_id);
		$query = $this->db->get();
		$totalRows = $query->num_rows();
		$errored = 0;
		$completed = 0;
		if($totalRows > 0)
		{
			$result = $query->result();
			foreach($result as $key=>$val)
			{
				if($val->Status == 'Errored')
				{
					$errored++;
				}
				else if($val->Status == 'Completed')
				{
					$completed++;
				}
			}
		}
		if($errored>0)
		{
			return 'Errored';
		}
		else 
		{
			return 'Deploy Completed';
		}
	}
	
	
	function ChangeParaValue($JSON_Submitted,$ChangeParaConfig)
	{
		if($ChangeParaConfig =='')
		{
			return $JSON_Submitted;
		}
		
		//original
		$replaceList = (new JSONPath(json_decode($JSON_Submitted)))->find($ChangeParaConfig->JSON_Expressions);		
		
		//changes
		// $replaceList = (new JSONPath($JSON_Submitted))->find($ChangeParaConfig->JSON_Expressions);	
		
		$oldStr= '';
		$NewPara = '';
		// print_r($replaceList);
		foreach($replaceList as $key=>$val)
		{	
			$oldStr = $val;
			// $removeStr =substr($val,strpos($val, $ChangeParaConfig->Para)+strlen($ChangeParaConfig->Para),$ChangeParaConfig->removeLanght);
			$removeStr =substr($val,strpos($val, $ChangeParaConfig->Para)+strlen($ChangeParaConfig->Para),$ChangeParaConfig->removeLength);
			$NewPara =str_replace($removeStr,$ChangeParaConfig->ChangeToStr,$val);			
		}
		// print_r(json_decode($JSON_Submitted));
		$JSON_Submitted = str_replace($oldStr,$NewPara, $JSON_Submitted);
		// echo 'ChangeParaValue</br>';echo'<pre>';print_r($JSON_Submitted);echo'</pre>';
		return $JSON_Submitted;
	}	
	
	// function CDOUpdateHandler($jsonAsset)
	// {
		///// echo '<pre>';print_r($jsonAsset);echo'</pre>';
		// $this->db->select('New_JSON_Asset');
		// $this -> db -> from('deployment_package_item');
		// $this -> db -> where('Asset_Id',76);
		// $query = $this -> db -> get();
		// $targetJson = $query->result();
		// $CDOdifferent = $this->eloqua->IsCustomDataObjectDifferent($jsonAsset,$targetJson);
		////// print_r($targetJson);
		// $keys[$i=0]= array();
		// if($CDOdifferent)
		// {
			////// print_r($jsonAsset['fields']);
			// $targetJson = json_decode($targetJson[0]->New_JSON_Asset);
			////// $targetJson =($targetJson->fields[0]);
			// $targetJson =($targetJson->fields);
			// $targetJson = json_decode(json_encode($targetJson),True);
			///// print_r($targetJson);
			// foreach($targetJson as $key1=>$val1)
			// {
				// $keys[$i++]=$targetJson[$key1]['id'];
			// }	
			//// print_r($keys);
			
				// foreach($jsonAsset['fields'] as $key=>)
				// {
					// foreach($keys as $key1)
					// {	
						// print_r($key1);
						// $jsonAsset['fields'][$key]['id']=$key1;
						//// print_r($jsonAsset['fields'][$key]['id']);
					// }
					// break;	
				// }
			//////echo '<pre>'; print_r($jsonAsset['fields']); exit;echo '</pre>';
		// }	
	// }
	
	function replaceJSONKey($exclusion, $jsonAsset )
	{		
		// if($jsonAsset['type']=='CustomObject')
		// {
			// $this->CDOUpdateHandler($jsonAsset);
		// }	
		
		if(is_array($exclusion) || is_object($exclusion))
		{
			foreach($exclusion as $key=>$val)
			{	
				if($val->basekey!="")
				{
					foreach($val->basekey as $key1=>$val1)
					{						
						foreach($val1 as $key2=>$val2)
						{					
							if(is_array($jsonAsset))
							{
								$jsonAsset[$val2] = $jsonAsset[$key2];
								unset($jsonAsset[$key2]);
							}
							if(is_object($jsonAsset))
							{
								$jsonAsset->$val2 = $jsonAsset->$key2;
								unset($jsonAsset->$key2);
							}
						}
					}
				}
			}
		}
		// echo 'replaceJSONKey after</br>';echo'<pre>';print_r($jsonAsset);echo'</pre>';
		return $jsonAsset;
	}
	
	function getExclusionResult($exclusion, $jsonAsset )
	{
		if(is_array($exclusion) || is_object($exclusion))
		{
			foreach($exclusion as $key=>$val)
			{
				if($val->basekey!="")
				{
					$exSplit2 = explode(",",$val->basekey);
					foreach($exSplit2 as $key1=>$val1){
						if(is_array($jsonAsset) )
						{
							if(isset($jsonAsset[$val1]))
							unset($jsonAsset[$val1]);
						}
						if(is_object($jsonAsset))
						{
							if(isset($jsonAsset->$val1))
							unset($jsonAsset->$val1);
						}
					}
				}
			}
			$jsonAsset = json_encode($jsonAsset);
			$jsonAsset = json_decode($jsonAsset,true);
			$store = new JsonStore();
			foreach($exclusion as $key=>$val)
			{
				foreach($val->jsonpath as $k=>$v)
				{
					$store->remove($jsonAsset, $v->path);					
				}
			}	
		}
		// echo 'getExclusionResult</br>';echo'<pre>';print_r($jsonAsset);echo'</pre>';
		return $jsonAsset;
	}
	
	
	function injSON($jsonObj,$node,$key,$type)
	{
		if(is_array($jsonObj->$node))
		{
			foreach($jsonObj->$node  as $key1=>$val1)
			{
				unset($val1->$key);
			}
			
		}
		if(is_object($jsonObj->$node))
		{
			//$this->injSON($jsonObj,$node,$key,"object");
		}	
		return $jsonObj;
	}
	
	function getalleloqua_endpoints()
	{
		$Asset_Type = array('Find Asset', 'Update','Create');
		$this->db->select('*');
		$this -> db -> from('rsys_asset_endpoint');
		$this -> db -> where_in('Endpoint_Type',$Asset_Type);
		$this->db->order_by("Asset_Type", "asc");
		$query = $this -> db -> get();
	    $eloqua_endpoints = $query->result();
		$alleloqua_endpoints = array();
		foreach($eloqua_endpoints as $key=>$val)
		{
			$alleloqua_endpoints[str_replace(' ', '', $val->Asset_Type).'_'.str_replace(' ', '',$val->Endpoint_Type)] = $val;
		}
		return $alleloqua_endpoints;
	}
	
	function getPackage($packageId)
	{
		$this->db->select('*');
		$this -> db -> from('deployment_package');
		$this -> db -> where('Deployment_Package_Id',$packageId);
		$this->db->limit(1);
		$query = $this -> db -> get();
	    $result = $query->result();
		return $result;
	}
	
	function getInstance($Site_Name)
	{
		$this->db->select('*');
		$this -> db -> from('instance_');
		$this -> db -> where('Site_Name',$Site_Name);
		$this->db->limit(1);
		$query = $this -> db -> get();
		$instance = $query->result();
		
		return $instance;
	}

	function ReplaceSiteId($JSON_Submitted,$TargetSiteId,$Package_Item)
	{
		$temp__Submitted = $JSON_Submitted;
		
		$this->db->select("inst.Site_Id");
		$this->db->from("deployment_package dp");
		$this->db->join("instance_ inst","inst.Site_Name = dp.Source_Site_Name","LEFT OUTER");
		$this->db->where("Deployment_Package_Id",$Package_Item->Deployment_Package_Id);
		
		$result = $this->db->get();
		if($result->num_rows()>0)
		{
			// echo '<br> ReplaceSiteId </br>';
			$SourceSiteId = $result->result();
			$SourceSiteId = $SourceSiteId[0]->Site_Id;
			$temp__Submitted = str_replace($SourceSiteId,$TargetSiteId,$temp__Submitted);			
		}
		// echo 'After ReplaceSiteId</br>';echo'<pre>';print_r($temp__Submitted);echo'</pre>';

		return $temp__Submitted;
	}
	
	function ProcessChildAssetSwitch($temp__Submitted,$val)
	{
		echo 'ProcessChildAssetSwitch enter<br/>';
		if($val->Asset_Type =="Custom Object") 
		{ 
			$this->db->select('*');
			$this -> db -> from('rsys_asset_child_replacement');
			$this -> db -> where('Asset_Type ',$val->Asset_Type);
			$Child_Replace_list = $this -> db -> get()->result();
			// echo'<pre>';print_r($Child_Replace_list);echo'<pre>';
			// echo'<pre>';print_r($val);echo'<pre>';
			foreach($Child_Replace_list as $crkey=>$crval)
			{
				$replaceList = (new JSONPath(json_decode($temp__Submitted)))->find($crval->JSON_Expressions);
				
				foreach($replaceList as $repkey=>$repval)
				{
					$Source_Expression = str_replace('{X}',$repval,$crval->Source_Asset_Expression);
					
					$SourceIDList = (new JSONPath(json_decode($val->Source_Asset_JSON)))->find($Source_Expression);
					// echo'sourceIdList-<pre>';print_r($SourceIDList);echo'<pre>';

					if(isset($SourceIDList[0]))
					{
						$Target_Expression = str_replace('{X}',$SourceIDList[0],$crval->Target_Asset_Expression);
						$TargetIDList = (new JSONPath(json_decode($val->Target_Asset_JSON)))->find($Target_Expression);
						
						if(isset($TargetIDList[0]))
						{
							$temp__Submitted = str_replace($crval->JSON_Node_Value.'":"'.$repval,$crval->JSON_Node_Value.'":"'.$TargetIDList[0],$temp__Submitted);
						}
						else
						{
							echo '<br/> TargetID List is missing </br>';
						}
					}
				
				}
			}
		}
		return $temp__Submitted;
	}
	
	function ProcessNegativeIds($Package_Item,$JSON_Submitted,$instance)
	{
		$temp__Submitted = $JSON_Submitted;
		// print_r('Process Negative Ids<br/>'.$temp__Submitted);
		$store = new JsonStore();
		
		$this->db->select('JSON_Negative_Id_Component');
		$this -> db -> from('rsys_asset_endpoint');
		$this -> db -> where('Asset_Type',$Package_Item->Asset_Type);
		if($Package_Item->Target_Asset_Id > 0)
		{
			$this -> db -> where('Endpoint_Type',"Update");
		}
		else
		{	
			$this -> db -> where('Endpoint_Type',"Create");
		}
		$query = $this -> db -> get();
		$JSON_Negative_list = $query->result();
		// print_r($JSON_Negative_list);
		$replace_pairs = array();
		
		foreach($JSON_Negative_list as $jnkey=>$jnval)
		{
			if($jnval->JSON_Negative_Id_Component =='')
			{
				continue; 
			}
			$jnval = explode(",", $jnval->JSON_Negative_Id_Component);	
			
			foreach($jnval as $expjnkey => $expjnval)
			{ 
				$replaceKey =  trim(substr($expjnval, strrpos($expjnval, '.') + 1));
				$replaceList = (new JSONPath(json_decode($temp__Submitted)))->find($expjnval);
				foreach($replaceList as $repkey=>$repval)
				{
					if($repval >=0)
					{						
						$repvalList = explode(' ',$repval);
						foreach($repvalList as $key=>$val)
						{	
							if(is_numeric($val))
							{
								$repvalList[$key]= '-'. $val;
							}
						}
						$newrepval = implode(' ',$repvalList);
						$temp__Submitted = str_replace('"'.$replaceKey.'":"'. $repval.'"','"'.$replaceKey.'":"'. $newrepval.'"',$temp__Submitted);
					}
				}
			}
		} 
		// print_r('after Negative Ids</br>');
		// echo '<pre>';print_r($temp__Submitted);echo '</pre>';
		return $temp__Submitted;
 	}
	
	function getDeploymentPackageValidation($Package_Item,$JSON_Submitted,$instance,$endPoint)
	{
		//echo '<br/> Processing childs <br/>';
		
		$this->db->select('*');
		$this -> db -> from('deployment_package_validation_list');
		$this -> db -> where('Deployment_Package_Item_Id',$Package_Item->Deployment_Package_Item_Id);
		
		$query = $this -> db -> get();
		
		$JSON_Submitted = $this->ReplaceSiteId($JSON_Submitted,$instance[0]->Site_Id,$Package_Item); //JSON_Negative_Id_Component 
		
		// $JSON_Submitted = $this->linkCampaignChild($JSON_Submitted);
		
		// $Json_submitted = $this->CDOField($JSON_Submitted);
		
		if($query -> num_rows() == 0)
		{
			$JSON_Submitted = $this->ProcessNegativeIds($Package_Item,$JSON_Submitted,$instance); 
			if ($Package_Item->Target_Asset_Id == 0)
			{
				// $temp__Submitted = $JSON_Submitted;
				// print_r($temp__Submitted);exit;
				// $JSON_Submitted = str_replace($Package_Item->Asset_Id,$Package_Item->Target_Asset_Id,$temp__Submitted);
				// echo'Before post Request';print_r($JSON_Submitted);
				if(time()+1800 > $instance[0]->Token_Expiry_Time)
				{
				   $result2 = $this->refreshToken($instance[0]);
				   $instance[0]->Token = $result2['Token']['access_token'];
				}
				$result = $this->eloqua->postRequest($instance[0]->Token, $instance[0]->Base_Url.''.$endPoint,$JSON_Submitted);
				// if($result['httpCode']==401)
				// {
					// $result = $this->eloqua->requestToken($instance[0]->Refresh_Token,'refresh_token');
					// $this->eloqua->updateTokens($instance[0]->Site_Id,$result['access_token'],$result['refresh_token']);
					// $result = $this->eloqua->postRequest($result['access_token'], $instance[0]->Base_Url.''.$endPoint,$JSON_Submitted);
				// }
				echo '<br/> Post Request 1<br/><pre>';
					//print_r($result);
				echo '</pre>';
			}
			else
			{
				$temp__Submitted = $JSON_Submitted;
				$JSON_Submitted = str_replace($Package_Item->Asset_Id,$Package_Item->Target_Asset_Id,$temp__Submitted);
				// echo'Before post Request';print_r($JSON_Submitted);
				if(time()+1800 > $instance[0]->Token_Expiry_Time)
				{
				   $result2 = $this->refreshToken($instance[0]);
				   $instance[0]->Token = $result2['Token']['access_token'];
				}
				$result = $this->eloqua->putRequest($instance[0]->Token, $instance[0]->Base_Url.''.$endPoint.'/'.$Package_Item->Target_Asset_Id,$JSON_Submitted);
			    // if($result['httpCode']==401)
				// {
					// $result = $this->eloqua->requestToken($instance[0]->Refresh_Token,'refresh_token');
					// $this->eloqua->updateTokens($instance[0]->Site_Id,$result['access_token'],$result['refresh_token']);
					// $result = $this->eloqua->putRequest($result['access_token'], $instance[0]->Base_Url.''.$endPoint,$JSON_Submitted);
				// }
				echo '<br/> Put Request 1<br/><pre>';
					//print_r($result);
				echo '</pre>';
			    
			}
			$data123['JSON_Submitted'] = $JSON_Submitted;
			$this->db->where('Deployment_Package_Item_Id',$Package_Item->Deployment_Package_Item_Id);
			$this->db->update('deployment_package_item',$data123);
			$this->updateNew_JSON_Asset($result,$Package_Item);
		}
		else
		{ 
			$package_validation_list = $query->result();
			// echo 'Package Validation List<pre></br>';
				// print_r($package_validation_list);
			// echo '<pre>';	
			$temp__Submitted = $JSON_Submitted;
			$temp__Submitted = $this->ProcessNegativeIds($Package_Item,$temp__Submitted,$instance);
			foreach($package_validation_list as $key=>$val)
			{
				// echo 'val <pre></br>';print_r($val);echo '</pre>';
				// echo '<br/>ProcessChildAssetSwitch - ' .$val->Asset_Name .'  After id Replace</br>';
				
				// echo 'Asset ID - ' .$val->Asset_Id .' Target Id - '.$val->Target_Asset_Id .'</br>' ;
				// $temp__Submitted = str_replace($val->JSON_Node_Value.'":"'.$val->Asset_Id,$val->JSON_Node_Value.'":"'.$val->Target_Asset_Id,$temp__Submitted);
				$temp__Submitted = str_replace($val->Asset_Id,$val->Target_Asset_Id,$temp__Submitted);
				$temp__Submitted = $this->ProcessChildAssetSwitch($temp__Submitted,$val);
				// echo '<pre>'; print_r($temp__Submitted); echo '</pre>';
			}
			$data123['JSON_Submitted'] = $temp__Submitted;
			if ($Package_Item->Target_Asset_Id == 0)
			{
				if(time()+1800 > $instance[0]->Token_Expiry_Time)
				{
				   $result2 = $this->refreshToken($instance[0]);
				   $instance[0]->Token = $result2['Token']['access_token'];
				}
				$result = $this->eloqua->postRequest($instance[0]->Token, $instance[0]->Base_Url.''.$endPoint,$temp__Submitted);
				// if($result['httpCode']==401)
				// {
					// $result = $this->eloqua->requestToken($instance[0]->Refresh_Token,'refresh_token');
					// $this->eloqua->updateTokens($instance[0]->Site_Id,$result['access_token'],$result['refresh_token']);
					// $result = $this->eloqua->postRequest($result['access_token'], $instance[0]->Base_Url.''.$endPoint,$JSON_Submitted);
				// }
				// echo '<br/> Post Request 2<br/><pre>';
					// print_r($result);
				// echo '</pre>';
			}
			else
			{ 	
				if(time()+1800 > $instance[0]->Token_Expiry_Time)
				{
				   $result2 = $this->refreshToken($instance[0]);
				   $instance[0]->Token = $result2['Token']['access_token'];
				}		
				$result = $this->eloqua->putRequest($instance[0]->Token, $instance[0]->Base_Url.''.$endPoint.'/'.$Package_Item->Target_Asset_Id,$temp__Submitted);
				// if($result['httpCode']==401)
				// {
					// $result = $this->eloqua->requestToken($instance[0]->Refresh_Token,'refresh_token');
					// $this->eloqua->updateTokens($instance[0]->Site_Id,$result['access_token'],$result['refresh_token']);
					// $result = $this->eloqua->putRequest($result['access_token'], $instance[0]->Base_Url.''.$endPoint,$JSON_Submitted);
				// }
				// echo '<br/> Put Request 1<br/><pre>';
					// print_r($result);
				// echo '</pre>';				
			}
			// echo '<pre>'; print_r($data123); echo '</pre>';
			$this->updateNew_JSON_Asset($result,$Package_Item);
			$this->db->where('Deployment_Package_Item_Id',$Package_Item->Deployment_Package_Item_Id);
			$this->db->update('deployment_package_item',$data123);
		}
	}
	
	function linkCampaignChild($JSON_Submitted)
	{
		$filteredJson =json_decode($JSON_Submitted,true);
		if($filteredJson['type']=='Campaign')
		{
			if(isset($filteredJson['elements']))
			{
				foreach($filteredJson['elements'] as $key=>$val)
				{
					foreach($filteredJson['elements'][$key] as $key2=>$val2)
					{
						if($key2 == 'outputTerminals')
						{
							// echo '<pre>';
								// print_r($filteredJson['elements'][$key][$key2]);
							// echo '</pre>';	
							foreach($filteredJson['elements'][$key][$key2] as $key3=>$val3)
							{
								foreach($filteredJson['elements'][$key][$key2][$key3] as $key4=>$val4)
								{
									// echo '<pre>';
										// print_r($filteredJson['elements'][$key][$key2][$key3]);
									// echo '</pre>';	
								
									if( $key4 == 'id')
									{	
										// unset($filteredJson['elements'][$key][$key2][$key3][$key4]);
										$filteredJson['elements'][$key][$key2][$key3][$key4] = "-".$filteredJson['elements'][$key][$key2][$key3][$key4];
									}
									if( $key4 == 'connectedId')	
									{
										$filteredJson['elements'][$key][$key2][$key3][$key4] = "-" . $val4;
									}
								}
							}
						}
					}
				}
			}
		}
		return json_encode($filteredJson);
	}
	
	function updateNew_JSON_Asset($New_JSON_data,$Package_Item)
	{
		
		// echo '<pre> -------------  updateNew_JSON_Asset start------------   <br/> ';
		if($New_JSON_data['httpCode'] =='200' || $New_JSON_data['httpCode'] =='201')
		{
			$data['New_JSON_Asset'] = $New_JSON_data['Output_result'];
			$data['Error_Description'] = '';
			$data['Status'] = 'Completed';
			$Target_Asset_Id =  json_decode($New_JSON_data['body'])->id ;
			$valid_data['Target_Asset_Id'] = $Target_Asset_Id;
			$valid_data['Target_Asset_JSON'] = $New_JSON_data['body'];
			$this->db->where('Deployment_Package_Id',$Package_Item->Deployment_Package_Id);
			$this->db->where('Asset_Id',$Package_Item->Asset_Id);
			$this->db->update('deployment_package_validation_list',$valid_data);
		}
		else
		{
			$data['Error_Description'] = $New_JSON_data['Output_result'];
			$data['New_JSON_Asset'] = '';
			$data['Status'] = 'Errored';
		}
		
		$this->db->where('Deployment_Package_Item_Id',$Package_Item->Deployment_Package_Item_Id);
		$this->db->update('deployment_package_item',$data);
		
		// echo '<pre> -------------  updateNew_JSON_Asset  end------------   <br/> ';
	}
}
?>