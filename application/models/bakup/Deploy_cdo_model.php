<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require(APPPATH.'/third_party/json_path/vendor/autoload.php');
use Flow\JSONPath\JSONPath;
use Flow\JSONPath\JSONPathLexer;
use \Peekmo\JsonPath\JsonPath as PeekmoJsonPath;
use \Peekmo\JsonPath\JsonStore;
class Deploy_CDO_model extends CI_Model
{
	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->load->model('eloqua','',TRUE);
		$this->load->model('deploy_model','',TRUE);
		$this->load->model('logging_model','',TRUE);		
    }
	
	function CDO_from_DB($customObjectId,$deployment_package_id)
	{
		$this->db->select('JSON_Asset,New_JSON_Asset');
		$this -> db -> from('deployment_package_item');
		$this -> db -> where('Deployment_Package_Id',$deployment_package_id);
		$this -> db -> where('Asset_Type','Custom Object');
		$this -> db -> where('Asset_Id',$customObjectId);
		$query = $this -> db -> get();
		$CDO_result=$query->result()[0];
		$source_CDO=json_decode($CDO_result->JSON_Asset);
		$target_CDO =json_decode(preg_replace('/HTTP(.*)GMT/s',"",$CDO_result->New_JSON_Asset));
		$result_CDO['source_CDO']=$source_CDO;
		$result_CDO['target_CDO']=$target_CDO;
		return $result_CDO;
	}
	
	function get_cdo_field_CDO_Form_PS_Custom_Value($customObjectId,$updateRules,$keyFieldMapping,$Package_Id)
	{
		$CDO_DB = $this->CDO_from_DB($customObjectId,$Package_Id);
		$source_CDO=$CDO_DB['source_CDO'];
		$target_CDO=$CDO_DB['target_CDO'];
		
		//Get CDO source Field Name
		foreach($source_CDO->fields as $sKey=>$sVal)
		{
			foreach($updateRules as $sKey1=>$sVal1)
			{
				if($sVal->id==$sVal1)
				{
					$source_fields_name[]=$sVal->name;
					$source_fields_id[$sVal->name]=$sVal1;
				}
				if($sVal->id==$keyFieldMapping)
				{
					$keyFieldMapping_source_name=$sVal->name;
				}
			}	
		}
		// print_r('Source Id <br>');
		// print_r($source_fields_id);
		foreach($target_CDO->fields as $tKey=>$tVal)
		{
			foreach($source_fields_name as $tKey1=>$tVal1)
			{
				if($tVal->name==$tVal1)
				{
					$target_fields_id[$tVal->name]=$tVal->id;
				}
				if($tVal->name==$keyFieldMapping_source_name)
				{
					$keyFieldMapping_target_id=$tVal->id;
				}
			}
		}
		// print_r('Target Id <br>');print_r($target_fields_id);
		// exit;	
		$new_target_fields_id = array(); 
		$t=0;
		foreach ($target_fields_id as $iKey => $iVal)
		{
			foreach ($source_fields_id as $iKey1 => $iVal1)
			{
				if($iKey==$iKey1)
				{
					$new_target_fields_id[$iVal1] = $iVal;
				}	
			}
			//$new_target_fields_id[$source_fields_id[$t++]] = $iVal;
		}
		// print_r($new_target_fields_id);
		// exit;
		$tCDO[$source_CDO->id] = $target_CDO->id;
		$tCDO['updateRules'] = $new_target_fields_id;
		$tCDO[$keyFieldMapping] = $keyFieldMapping_target_id;
		return $tCDO;	
	}
	
	function CDO_Form_PS_CustomValue($JSON_Submitted,$instance,$Package_Item)
	{
		$JSON_Submitted = json_decode($JSON_Submitted);
		$source_CDO_replace_element_customValue = array();
		$source_CDO_replace_element_customValue_temp=array();
		$t=0;
		$CDO_Update_Flag1=0;
		 
		if($JSON_Submitted->type=='Form')
		{
			$form_processingSteps_customValue = $JSON_Submitted->processingSteps;
			$form_processingSteps_temp_customValue = $form_processingSteps_customValue;
			foreach($form_processingSteps_customValue as $psKey1=>$psVal1)
			{
				if($psVal1->type=='FormStepCreateUpdateCustomObject')
				{
					foreach($psVal1 as $psKey2=>$psVal2)
					{
						if($psKey2=='customObjectId')
						{
							$source_CDO_replace_element_customValue_temp['customObjectId']=$psVal2;
						}
						if($psKey2=='customObjectUpdateRuleSet')
						{
							foreach($psVal2 as $psKey3=>$psVal3)
							{
								if($psKey3=='updateRules')
								{
									$i=0;
									foreach($psVal3 as $psKey4=>$psVal4)
									{
										if(isset($psVal4->targetFieldId))
										{
											$source_CDO_replace_element_customValue_temp['updateRules'][$i++]=$psVal4->targetFieldId;
										}
									}
									
								}	
							}
						}
						if($psKey2=='keyFieldMapping')
						{
							foreach($psVal2 as $psKey5=>$psVal5)
							{
								if($psKey5=='targetEntityFieldId')
								{
									$source_CDO_replace_element_customValue_temp['targetEntityFieldId']=$psVal5;
								}
							}	
						}
					}
					$source_CDO_replace_element_customValue[$t++] = $source_CDO_replace_element_customValue_temp;	
					$CDO_Update_Flag1=1;
				}
			}
			if($CDO_Update_Flag1==1)
			{
				$k=0;
				foreach($source_CDO_replace_element_customValue as $psKey6=>$psVal6)
				{
					$target_fields_ids_customValue[$k++]=$this->get_cdo_field_CDO_Form_PS_Custom_Value($psVal6['customObjectId'],$psVal6['updateRules'],$psVal6['targetEntityFieldId'],$Package_Item->Deployment_Package_Id);
					
				}
				$replaced_fields_customValue=$this->replace_Form_PS_CDO_field_customValue($target_fields_ids_customValue,$form_processingSteps_temp_customValue);
				// print_r($replaced_fields);
				$JSON_Submitted->processingSteps = $replaced_fields_customValue;
				$result = $JSON_Submitted;
				// echo '<pre>';print_r($result);echo'</pre>';exit;
			}
			else 
			{
				$result = $JSON_Submitted;
			}	
		} 
		else
		{
			$result = $JSON_Submitted;
		}
		return json_encode($result);
	}

	function replace_Form_PS_CDO_field_customValue($target_fields_ids_customValue,$form_processingSteps_temp_customValue)
	{
		// echo '<pre>';print_r($form_processingSteps_temp_customValue);echo '</pre>';
		// echo '<pre>';print_r($target_fields_ids_customValue);echo '</pre>';
		// exit;
		// echo '<br/>';
		foreach($form_processingSteps_temp_customValue as $rKey=>$rVal)
		{
			if($rVal->type=='FormStepCreateUpdateCustomObject')
			{
				// print_r($rVal);
				foreach($target_fields_ids_customValue as $rKey1=>$rVal1)
				{
					// print_r($rKey1);
					foreach($rVal1 as $rKey2=>$rVal2)
					{
						if($rKey2==$rVal->customObjectId)
						{
							$rVal->customObjectId=$rVal2;
						}
						if(isset($rVal->keyFieldMapping))
						{
							if(isset($rVal->keyFieldMapping->targetEntityFieldId))
							{
								if($rKey2==$rVal->keyFieldMapping->targetEntityFieldId)
								{
									$rVal->keyFieldMapping->targetEntityFieldId=$rVal2;
								}
							}		
						}
						if(isset($rVal->customObjectUpdateRuleSet->updateRules))
						{
							// print_r($rVal->customObjectUpdateRuleSet->updateRules['targetFieldId']);
							foreach($rVal->customObjectUpdateRuleSet->updateRules as $rKey3=>$rVal3)
							{
								if($rKey2=='updateRules')
								{
									foreach($rVal2 as $rKey4=>$rVal4)
									{
										if($rVal3->targetFieldId==$rKey4)
										{
											$rVal3->targetFieldId=$rVal4;
										}	
									}	
								}
							}
						}	
					}
				}
			}
		}
		// print_r($form_processingSteps_temp_customValue);
		// exit;
		return $form_processingSteps_temp_customValue;
	}
	
	function CDO_Segment($JSON_Submitted,$Package_Item)
	{
		$JSON_Submitted = json_decode($JSON_Submitted);
		$source_CDO_replace_element = array();
		$source_CDO_replace_element_temp=array();
		$t=0;
		$CDO_Update_Flag2=0;
		 
		if($JSON_Submitted->type=='ContactSegment')
		{
			$segment_elements = $JSON_Submitted->elements;
			$segment_elements_temp = $segment_elements;
			foreach($segment_elements_temp as $sKey=>$sVal)
			{
				foreach($sVal->filter->criteria as $sKey1=>$sVal1 )
				{
					if($sVal1->type=='LinkedAccountCustomObjectCriterion' || $sVal1->type=='LinkedCustomObjectCriterion')
					{
						foreach($sVal1 as $sKey2=>$sVal2)
						{
							if($sKey2=='customObjectId')
							{
								$source_CDO_replace_element_temp['customObjectId']=$sVal2;
							}
							if($sKey2=='fieldConditions')
							{
								$i=0;
								foreach($sVal2 as $sKey3=>$sVal3)
								{
									if(isset($sVal3->fieldId))
									{
										$source_CDO_replace_element_temp['fieldConditions'][$i++]=$sVal3->fieldId;
									}
								}	
								
							}	
							
						}
						$source_CDO_replace_element[$t++] = $source_CDO_replace_element_temp;	
						$CDO_Update_Flag2=1;
					}
					
				}
			}
			if($CDO_Update_Flag2==1)
			{
				// print_r($source_CDO_replace_element);
				$k=0;
				foreach($source_CDO_replace_element as $psKey6=>$psVal6)
				{
					$target_fields_ids_segment[$k++]=$this->get_cdo_field_segment($psVal6['customObjectId'],$psVal6['fieldConditions'],$Package_Item->Deployment_Package_Id);
					
				}
				// print_r($target_fields_ids_segment); exit;
				$replaced_fields_segment=$this->replace_segment_CDO_field($target_fields_ids_segment,$segment_elements_temp);
				//////////////print_r($replaced_fields);
				$JSON_Submitted->elements = $replaced_fields_segment;
				$JSON_Submitted = $this->statementNegative($JSON_Submitted );
				$result = $JSON_Submitted;
				
				// echo '<pre>';print_r($result);echo'</pre>';exit;
			}
			else 
			{
				$JSON_Submitted = $this->statementNegative($JSON_Submitted );
				$result = $JSON_Submitted;
			}
		}
		else
		{
			$result=$JSON_Submitted;
		}
		return json_encode($result);	
	}
	
	function statementNegative($JSON_Submitted)
	{
		$JSON_Submitted_temp=$JSON_Submitted;
		foreach($JSON_Submitted_temp->elements as $sKey1=>$sVal1)
		{
			$statement_temp=explode(" ",$sVal1->filter->statement);
			foreach($statement_temp as $sKey2=>$sVal2)
			{
				// print_r($sVal1);
				if (strpos($sVal2, '(') !== false) 
				{
					$sVal2=str_replace('(','(-',$sVal2);
					$statement_temp[$sKey2]=$sVal2;
				}	
				else if (strpos($sVal2, ')') !== false)
				{
					$sVal2=str_replace($sVal2,'-'.$sVal2,$sVal2);
					$statement_temp[$sKey2]=$sVal2;
				}	
				else
				{
					if(is_numeric($sVal2))
					{
						$sVal2=str_replace($sVal2,'-'.$sVal2,$sVal2);
						$statement_temp[$sKey2]=$sVal2;
					}
				}	
			}
			$statement_temp = implode(" ",$statement_temp);
			$sVal1->filter->statement= $statement_temp;
		}
		return $JSON_Submitted_temp;
	}
	
	function replace_segment_CDO_field($target_fields_ids_segment,$segment_elements_temp)
	{
		// echo '<pre>';print_r($segment_elements_temp);echo '</pre>';
		// echo '<pre>';print_r($target_fields_ids_segment);echo '</pre>';
		// exit;
		// echo '<br/>';
		foreach($segment_elements_temp as $sKey=>$sVal)
		{
			foreach($sVal->filter->criteria as $sKey1=>$sVal1 )
			{
				//print_r($sVal1);
				if($sVal1->type=='LinkedAccountCustomObjectCriterion' || $sVal1->type=='LinkedCustomObjectCriterion')
				{
					foreach($target_fields_ids_segment as $sKey2=>$sVal2)
					{
						foreach($sVal2 as $sKey3=>$sVal3)
						{
							if($sVal1->customObjectId==$sKey3)
							{
								$sVal1->customObjectId=$sVal3;
							}
							if(isset($sVal1->fieldConditions))
							{
								if($sKey3=='fieldConditions')
								{
									foreach($sVal3 as $sKey4=>$sVal4)
									{
										foreach($sVal1->fieldConditions as $sKey5=>$sVal5)
										{
											if($sVal5->fieldId==$sKey4)
											{
												$sVal5->fieldId=$sVal4;
											}	
										}
									}
								}
										
								
							}	
						}
					}
				}
			}
		}
		// print_r($segment_elements_temp);
		// exit;
		return $segment_elements_temp;
	}
	
	function get_cdo_field_segment($customObjectId,$fieldConditions,$Package_Id)
	{
		$CDO_DB = $this->CDO_from_DB($customObjectId,$Package_Id);
		$source_CDO=$CDO_DB['source_CDO'];
		$target_CDO=$CDO_DB['target_CDO'];
		
		//Get CDO source Field Name
		foreach($source_CDO->fields as $sKey=>$sVal)
		{
			foreach($fieldConditions as $sKey1=>$sVal1)
			{
				if($sVal->id==$sVal1)
				{
					$source_fields_name[]=$sVal->name;
					$source_fields_id[$sVal->name]=$sVal1;
				}
			}	
		}
		// print_r('Source Id <br>');
		// print_r($source_fields_id);
		foreach($target_CDO->fields as $tKey=>$tVal)
		{
			foreach($source_fields_name as $tKey1=>$tVal1)
			{
				if($tVal->name==$tVal1)
				{
					$target_fields_id[$tVal->name]=$tVal->id;
				}
			}
		}
		// print_r('Target Id <br>');print_r($target_fields_id);
		// exit;	
		$new_target_fields_id = array(); 
		$t=0;
		foreach ($target_fields_id as $iKey => $iVal)
		{
			foreach ($source_fields_id as $iKey1 => $iVal1)
			{
				if($iKey==$iKey1)
				{
					$new_target_fields_id[$iVal1] = $iVal;
				}	
			}
			//$new_target_fields_id[$source_fields_id[$t++]] = $iVal;
		}
		// print_r($new_target_fields_id);
		// exit;
		$tCDO[$source_CDO->id] = $target_CDO->id;
		$tCDO['fieldConditions'] = $new_target_fields_id;
		return $tCDO;
	}
	
	
	
	// function handleHTMLChanges($temp__Submitted,$package_validation_list)
	// {
		// $temp__Submitted_temp = json_decode($temp__Submitted);
		// if($temp__Submitted_temp['type']=='Email')
		// {	
			// $url = 'http://52.36.180.91:8080/Test1/SendBackWithElqTypeChanged';
			// $data_json =$temp__Submitted.'|'.$package_validation_list ;
			/////////// print_r($data_json);
			// $ch=curl_init();
			// $headers = array('Content-Type:text/plain');
			// curl_setopt($ch, CURLOPT_URL, $url); 
			// curl_setopt($ch, CURLOPT_POST, true); 
			// curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			// curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
			// curl_setopt($ch, CURLOPT_POSTFIELDS,$data_json);
			// curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			// $result 		= curl_exec($ch);
			
			// print_r($result);
			//////////////$result_temp=json_decode($result,true);
		// }
		// else
		// {
			// $result_temp=$temp__Submitted;
		// }
		// return $result_temp;		
	// }
}	