<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require(APPPATH.'/third_party/json_path/vendor/autoload.php');
use Flow\JSONPath\JSONPath;
use Flow\JSONPath\JSONPathLexer;
use \Peekmo\JsonPath\JsonPath as PeekmoJsonPath;

class Eloqua extends CI_Model
{
	function __construct()
    {
        parent::__construct();
	}
		
	function merge()
	{
		$this->db->select('*');
		$this->db->from('Vikas');
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			$result = $query->result();
			foreach($result as $key=>$val)
			{
				$this->db->select('*');
				$this->db->from('Entry');
				$this->db->where('Entry_email',$val->Vikas_email);
				$query1 = $this->db->get();
				if($query1->num_rows()>0)
				{
					$result1 = $query1->result();
					$tdata['Email'] = $result1[0]->Entry_email;
					$tdata['Entry'] = $result1[0]->Entry_time;
					// $tdata['Entry_Date'] = new DateTime($result1[0]->Entry_time);
					$tdata['Vikas'] = $val->Vikas_time;
					// $tdata['Vikas_Date'] = new DateTime($val->Vikas_time);
					$tdata['Difference'] = ($val->Vikas_time - $result1[0]->Entry_time) / 60;
					$this->db->insert('Entry_Vikas',$tdata);
				}
			}
		}
		
	}
	function getAsset($org , $meta, $page =1 , $Endpoint_Type = 'Read List')
	{
		$this -> db -> select('*');
		$this -> db -> from('rsys_asset_type at');
		$this -> db -> join('rsys_asset_endpoint ep','at.Asset_Type_Name= ep.Asset_Type', 'LEFT OUTER');
		$this -> db -> where('Asset_Type_Name', $meta);
		$this -> db -> where('Endpoint_Type', $Endpoint_Type);
		$this -> db -> limit(1);
	    $query = $this -> db -> get();
	    $resultend = $query->result()[0];
		$url = $org->Base_Url .$resultend->Endpoint_URL .'?depth='.$resultend->Depth .'&count='.$resultend->Page_Size ."&page=".$page;
		if(time()+1800 > $org->Token_Expiry_Time)
		{
		   $result2 = $this->refreshToken($org);
		   //print_r($org);
		   $org->Token = $result2['Token']['access_token'];
		}
		$result = $this->get_request($org->Token, $url);
		
		return $result['data'];
	}
	
	public function updateTokens($siteId,$access_token,$refresh_token)
	{
		$this->db->where('Site_Id',$siteId);
		$udata['Token'] = $access_token;
		$udata['Refresh_Token'] = $refresh_token;
		$this->db->update('instance_',$udata);
	}
	
	function getPackageItems($packageId)
	{
		$this -> db -> select('Asset_Type,Asset_Name,Asset_Id,missing_target');
		$this -> db -> from(' deployment_package_item');
		$this -> db -> where('Deployment_Package_Id', $packageId);
		$query = $this -> db -> get();
		$query1 = $query->result();
		return $query1;
	}
		
	function getAssetChild($Package_Id,$Package_Item_Id, $Asset_Id,$Asset_Type ,$Endpoint_Type = 'Read Single')
	{
		$ChildList = array();
		$this -> db -> select('*');
		$this -> db -> from('deployment_package dp');
		$this -> db -> join('instance_ inst','dp.Source_Site_Name= inst.Site_Name', 'LEFT OUTER');
		$this -> db -> where('dp.Deployment_Package_Id', $Package_Id);
		$this -> db -> limit(1);
		$query = $this -> db -> get();
	    $org = $query->result()[0];
		
		$this -> db -> select('*');
		$this -> db -> from('rsys_asset_child rsc');
		$this -> db -> where('rsc.Parent_Asset_Type', $Asset_Type);
		$query = $this -> db -> get();
	    $Asset_Childs = $query->result();
		
		$this -> db -> select('*');
		$this -> db -> from('rsys_asset_endpoint rae');
		$this -> db -> where('rae.Asset_Type', $Asset_Type);
		$this -> db -> where('rae.Endpoint_Type', 'Read Single');
		$query = $this -> db -> get();
	    $endpoint = $query->result();
		 
		// print_r($endpoint); 
		if($endpoint)
		{	
			$url = $org->Base_Url .$endpoint[0]->Endpoint_URL .'/'.$Asset_Id.'?depth='.$endpoint[0]->Depth ;
			
			if(time()+1800 > $org->Token_Expiry_Time)
			{
			   $result2 = $this->refreshToken($org);
			   $org->Token = $result2['Token']['access_token'];
			}
			$result = $this->get_request($org->Token, $url);
			
			$result_temp=$result;
			
			$data['JSON_Asset'] = $result['data'];
			
			$this->db->where('Deployment_Package_Item_Id', $Package_Item_Id);
			$this->db->update('deployment_package_item' ,$data);	
			
			foreach($Asset_Childs as $key=>$val)
			{
				if($val->Asset_Node >0)
				{
					$findresult = array();
					
					if( ($val->JSON_Node_Type_In_Parent ==0))
					{	
						if($val->JSON_Node_Value != '')
						{ 
							$findresult = array();
							$findresult = $this->FindValueinJSON((array)json_decode($data['JSON_Asset']),$val->JSON_Node_Value);
						}						
					}
					else
					{ 
						$findresult = array();
						
						$FindExpression = (new JSONPath(json_decode($data['JSON_Asset'])))->find($val->JSONPath_Expression);
				       
						$FindExpression_temp = json_decode(json_encode($FindExpression),true);
					
						if(!isset($val->JSON_Node_Value) || $val->JSON_Node_Value == '')
						{
							for($j=0;$j<count($FindExpression_temp);$j++)
							{
								if(isset($FindExpression_temp[$j]) && (is_array($FindExpression_temp[$j]) || is_object($FindExpression_temp[$j])))
								{
									
									
									foreach($FindExpression_temp[$j] as $tkey=>$tval)
									{
										$findresult[] = $tval;
									}
								
								}
							}
						}
						else
						{	
							
							if($val->JSON_Node_Type=='Email_dynamicContents' || $val->JSON_Node_Type=='LandingPage_dynamicCotents')
							{
								if(is_object($FindExpression) || is_array($FindExpression))
								{
									$findresult = $this->getDynamicContentChilds($FindExpression);
									
								}	
							}
							else if($val->JSON_Node_Type=='Email_contentSections')
							{
								if(is_object($FindExpression) || is_array($FindExpression))
								{
									$findresult = $this->getLandingPageFormChilds($FindExpression);
									
								}	
							}
							else if($val->Parent_Asset_Type == 'Landing Page' && $val->JSON_Node_Type =='formId')
							{
								$findresult = $this->getLandingPageFormChilds($FindExpression);
							}
							else if($val->Parent_Asset_Type == 'Landing Page' && $val->JSON_Node_Type =='LandingPage_contentSections')
							{
								$findresult = $this->getLandingPageFormChilds($FindExpression);
							}
							else
							{
								$FindExpression = json_decode(json_encode($FindExpression));
								if(!empty($FindExpression))
								{
									$findresult = $this->FindValueinJSON($FindExpression,$val->JSON_Node_Value);
								}
							}
						}
					}
					if(sizeof($findresult)>0 )
					{
						$this -> db -> select('*');
						$this -> db -> from('rsys_node_endpoint rne');
						$this -> db -> where('rne.JSON_Node_Value', $val->JSON_Node_Value);
						$this -> db -> where('rne.JSON_Node_Type', $val->JSON_Node_Type);
						$query = $this -> db -> get();
						$NodeEndpoint = $query->result();
						
						if($NodeEndpoint )
						{	
							if(isset($findresult) && sizeof($findresult)>0)
							{
								foreach($findresult as $findkey=>$findval)
								{
									if($findval>0)
									{	
										$url = $org->Base_Url .$NodeEndpoint[0]->Endpoint_URL .'/'.$findval.'?depth='.$NodeEndpoint[0]->Depth ;
										// if(time()+1800 > $org->Token_Expiry_Time)
										// {
										   // $result2 = $this->refreshToken($org);
										   // $org->Token = $result2['Token']['access_token'];
										// }
										$childresult = $this->get_request($org->Token, $url);
										$childresult = (array)json_decode($childresult['data']);
										
										//Commented because of issue now condition is added to line no 198 for only First and Last Name(contact/Account Field)
										// $flag = 1;
										// if(isset($childresult['type']))
										// {
											// if($childresult['type'] == 'ContactField' || $childresult['type'] == 'AccountField')
											// if($childresult['type'] == 'ContactField'&& $childresult['name']='First and Last Name')
											// {
												// if($childresult['isStandard']=='true')
												// {
													// $flag = 0;
													// break;
												// }	
											// }
										// }
										if(isset($childresult['name']))
										{
											if($childresult['type'] == 'OptionList')
											{
												if(strpos($childresult['name'], '/') !== false) 
												{	
													$childresult['name'] = preg_replace("/[\/\&%#\$:]/", " ",$childresult['name']);
												}
											}
										}
										
										if(isset($childresult['name']))
										{
											if($Asset_Type == 'Segment')
											{
												if($childresult['type'] == 'ContactFilter')
												{
													if(isset($childresult['scope']) && $childresult['scope'] == 'local')
													{
														continue;
													}
												}
											}
										}
										if(isset($childresult[$NodeEndpoint[0]->Display_Value]) && $childresult['name'] !='First and Last Name')
										{
											$TempChildList['Asset_Id'] = $findval;
											$TempChildList['Asset_Type'] = $NodeEndpoint[0]->Asset_Type;
											$TempChildList['Display_Name'] = $childresult[$NodeEndpoint[0]->Display_Value];
											$TempChildList['JSON_Node_Value'] = $NodeEndpoint[0]->JSON_Node_Value ;
											$flag = true;
											if($flag)
											{
												$ChildList[] = $TempChildList;
											}
										}
									}	
								}
							
							}
						}
					}
				}
				
			}
			
			$tempChildEmail='';
			if($Asset_Type=='Email')
			{
				$tempChildEmail=$this->emailSignatureLayout($Package_Id,$Package_Item_Id,$org,$result_temp,$ChildList);
				$ChildList=$tempChildEmail;
			}
			$tempChildForm='';
			if($Asset_Type=='Form')
			{
				$tempChildForm=$this->formHiddenEmailCampaignId($org,$result_temp,$ChildList);
				$ChildList=$tempChildForm;
			}
			// $tempChildLPFM='';
			// print_r($Asset_Type);
			// if($Asset_Type=='Landing Page')
			// {
				// $tempChildForm=$this->LPFieldMerge($org,$result_temp,$ChildList);
				// $ChildList=$tempChildForm;
			// }
			//print_r($Asset_Type);
			if($Asset_Type == 'Segment')
			{
				//$this->removeFilterCriteria($ChildList);
			}
			return $ChildList;
		}
		else
		{
			return false;
		}
	}
	
	
	function getLandingPageFormChilds($FindExpression)
	{
		$FindExpression = json_decode(json_encode($FindExpression),true);
		$objects = array();
		if(is_object($FindExpression) || is_array($FindExpression))
		{
			for($i=0;$i<count($FindExpression);$i++)
				{
					foreach($FindExpression[$i] as $key=>$val)
					{
						if($key == 'id')
						{
							$objects[]=$val;
						}
					}
				}
		}
		return $objects;
	}
	
	function getDynamicContentChilds($FindExpression)
	{
		$FindExpression = json_decode(json_encode($FindExpression),true);
		$objects = array();
		if(is_object($FindExpression) || is_array($FindExpression))
		{
			for($i=0;$i<count($FindExpression);$i++)
				{
					foreach($FindExpression[$i] as $key=>$val)
					{
						if($key == 'id')
						{
							$objects[]=$val;
						}
					}
				}
		}
		return $objects;
	}	
	
	function formHiddenEmailCampaignId($org,$result_temp,$ChildList)
	{
		// print_r($result_temp);
		$result_json=json_decode($result_temp['data'],true); 
		if(isset($result_json['processingSteps']) && (strpos(($result_temp['data']), 'FormStepSendEmail') !== false) || strpos($result_temp['data'], 'FormStepAddToCampaign') !== false)
		{
			// $emailFormFieldId=array();
			foreach($result_json['processingSteps'] as $fKey=>$fVal)
			{
				// print_r($fVal);
				// print_r();
				// print_r();
				if(isset($fVal['emailId']['formFieldId']))
				{
					$emailFormFieldId=$fVal['emailId']['formFieldId'];
					
				}
				else if(isset($fVal['campaignElementId']['formFieldId']))
				{
					if($fVal['campaignElementId']['valueType']!='optionList')
					{
						$campaignFormFieldId=$fVal['campaignElementId']['formFieldId'];
					}
				}
			}
			//print_r($campaignFormFieldId);
			if(isset($emailFormFieldId) || isset($campaignFormFieldId))
			{
				foreach($result_json['elements'] as $fKey=>$fVal)
				{
					if(isset($emailFormFieldId))
					{	
						if($fVal['id']==$emailFormFieldId)
						{
							if(is_numeric($fVal['defaultValue']))
							{
								$emailId=$fVal['defaultValue'];
							}
						}
					}
					else if(isset($campaignFormFieldId))
					{					
						// print_r($fVal['id']);
						if($fVal['id']==$campaignFormFieldId)
						{
							if(is_numeric($fVal['defaultValue']))
							// if($fVal['defaultValue'])
							{
								$campaignId=$fVal['defaultValue'];
								//print_r($campaignId);
								// echo '<br>';
							}	
						} 
					}	
				}	
			}
			if(isset($emailId) || isset($campaignId))
			{
				if(isset($emailId))
				{
					$this -> db -> select('*');
					$this -> db -> from('rsys_asset_endpoint rae');
					$this -> db -> where('rae.Asset_Type','Email' );
					$this -> db -> where('rae.Endpoint_Type', 'Read Single');
					$query = $this -> db -> get();
					$endpoint = $query->result();
						
					$result='';
					
					if($endpoint)
					{	
						$url = $org->Base_Url .$endpoint[0]->Endpoint_URL .'/'.$emailId.'?depth='.$endpoint[0]->Depth ;
						
						if(time()+1800 > $org->Token_Expiry_Time)
						{
						   $result2 = $this->refreshToken($org);
						   $org->Token = $result2['Token']['access_token'];
						}
						$result = $this->get_request($org->Token, $url);
						
					}
					if($result['httpCode']!=200)
					{
						$ChildList;	
					}
					else
					{	
						$result1=json_decode($result['data'],true);	
						
						$TempChildList_email['Asset_Id'] = $result1['id'];
						$TempChildList_email['Asset_Type'] = 'Email';
						$TempChildList_email['Display_Name'] = $result1['name'];
						$TempChildList_email['JSON_Node_Value'] = 'defaultValue';
						$ChildList[] = $TempChildList_email;
					}	
				}
				if(isset($campaignId))
				{
					////echo($campaignId);
					$this -> db -> select('*');
					$this -> db -> from('rsys_asset_endpoint rae');
					$this -> db -> where('rae.Asset_Type','Campaign' );
					$this -> db -> where('rae.Endpoint_Type', 'Read Single');
					$query = $this -> db -> get();
					$endpoint = $query->result();
						
					$result='';
					
					if($endpoint)
					{	
						$url = $org->Base_Url .$endpoint[0]->Endpoint_URL .'/'.$campaignId.'?depth='.$endpoint[0]->Depth ;
						if(time()+1800 > $org->Token_Expiry_Time)
						{
						   $result2 = $this->refreshToken($org);
						   $org->Token = $result2['Token']['access_token'];
						}
						$result_c = $this->get_request($org->Token, $url);
					}
					if($result_c['httpCode']!=200)
					{
						$ChildList;	
					}
					else
					{	
						$result_campaign=json_decode($result_c['data'],true);	
						$TempChildList_campaign['Asset_Id'] = $result_campaign['id'];
						$TempChildList_campaign['Asset_Type'] = 'Campaign';
						$TempChildList_campaign['Display_Name'] = $result_campaign['name'];
						$TempChildList_campaign['JSON_Node_Value'] = 'defaultValue';
						$ChildList[] = $TempChildList_campaign;
					}	
				}	
			}	
		}
		else
		{
			$ChildList;
		}	
		
		return $ChildList;
	}	
	
	function emailSignatureLayout($Package_Id,$Package_Item_Id,$org,$result_temp,$ChildList)
	{
		$result_json=json_decode($result_temp['data'],true); 
		if(isset($result_json['plainText']) && strpos($result_json['plainText'], 'signaturelayout') !== false)
		{
			$plainText = $result_json['plainText'];	
			$palinText_temp=explode("layoutid=",$plainText);
			$palinText_temp=explode(" ",$palinText_temp[1]);
			$signaturelayout_asset_id=str_replace('"','',$palinText_temp[0]);
			if(isset($signaturelayout_asset_id))
			{
				$this -> db -> select('*');
				$this -> db -> from('rsys_asset_endpoint rae');
				$this -> db -> where('rae.Asset_Type', 'Signature Layout');
				$this -> db -> where('rae.Endpoint_Type', 'Read Single');
				$query = $this -> db -> get();
				$endpoint = $query->result();
					
				$result='';
				
				if($endpoint)
				{	
					$url = $org->Base_Url .$endpoint[0]->Endpoint_URL .'/'.$signaturelayout_asset_id.'?depth='.$endpoint[0]->Depth ;
					
					if(time()+1800 > $org->Token_Expiry_Time)
					{
					   $result2 = $this->refreshToken($org);
					   $org->Token = $result2['Token']['access_token'];
					}
					$result = $this->get_request($org->Token, $url);
					
				}
				$result1=json_decode($result['data'],true);	
				// //echo($result1);
				$TempChildList['Asset_Id'] = $result1['id'];
				if($result1['type']='EmailSignatureLayout')
				{
					$TempChildList['Asset_Type'] = 'Signature Layout';
				}	
				$TempChildList['Display_Name'] = $result1['name'];
				$TempChildList['JSON_Node_Value'] = 'email_signatureLayout';
				$ChildList[] = $TempChildList;
			}	
			else
			{
				$ChildList=$ChildList;
			}	
		}
		return $ChildList;
	}
	
	function FindValueinJSON($jsonObj,$JSON_Node)
	{
		$objects = array();
		
		if(is_object($jsonObj) || is_array($jsonObj))
		{
			foreach ($jsonObj as $key=>$val) 
			{				
				if (is_object($val) || is_array($val)) 
				{
					$objects = array_merge($objects , $this->FindValueinJSON((array)$val, $JSON_Node));
				} 
				else if ($key === $JSON_Node) 
				{
					$objects[] = $val;
				}
			}
		}
		return $objects;		
	}
	
	function FindObjectWithJSONExpression($jsonObj,$JSON_Node)
	{
		$objects = array();
		if(is_object($jsonObj) || is_array($jsonObj))
		{
			foreach ($jsonObj as $key=>$val) 
			{	
				if (is_object($val) || is_array($val)) 
				{
					$objects = array_merge($objects , $this->FindObjectWithJSONExpression((array)$val, $JSON_Node));
				} 
				else if ($val == $JSON_Node) 
				{
					$objects[] = $jsonObj;
				} 
			}
		}
		return $objects;	
	}
	
	
	function IsCustomDataObjectDifferent($sourceData,$targetData)
	{	
		$flag='';
		$this -> db -> select('*');
		$this -> db -> from('rsys_asset_endpoint rae');
		$this -> db -> where('rae.Asset_Type', "Custom Object");
		$this -> db -> where('rae.Endpoint_Type', 'Read Single');
		$queryFields = $this -> db -> get();
		$FieldList = $queryFields->result();
		$list1=array();
		$list2=array();
		foreach($FieldList as $key=>$val)
		{
			$list1[] = (new JSONPath($sourceData))->find($val->JSONPath_Expression_Fields);
		}
		
		foreach($FieldList as $key=>$val)
		{
			$list2[] = (new JSONPath($targetData))->find($val->JSONPath_Expression_Fields);
		}
		
		$list1 =json_decode(json_encode($list1[0]), true); 
		$list2 =json_decode(json_encode($list2[0]), true);
		asort( $list1 ); 
		asort( $list2 );
		foreach($list1 as $key1=>$val1)
		{
			$flag = false;
			foreach($list2 as $key2=>$val2)
			{
				if($val1 == $val2)
				{
					$flag = true;
					break;
				}
			}
			if(!$flag)
			{				
				break;
			}		
		}
		
		if($flag)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	
	function validatepackage($Package_Id)
	{
		$this -> db -> select('*');
		$this -> db -> from('deployment_package dp');
		$this -> db -> join('instance_ inst','dp.Target_Site_Name= inst.Site_Name', 'LEFT OUTER');
		$this -> db -> where('dp.Deployment_Package_Id', $Package_Id);
		// $this -> db -> where('dp.Created_By_Contact_Id', $Contact_Id);
		$this -> db -> limit(1);
		$query = $this -> db -> get();
		$org = $query->result()[0];
		
		$this -> db -> select('*');
		$this -> db -> from('deployment_package dp');
		$this -> db -> join('instance_ inst','dp.Source_Site_Name= inst.Site_Name', 'LEFT OUTER');
		$this -> db -> where('dp.Deployment_Package_Id', $Package_Id);
		$this -> db -> limit(1);
		$querysource = $this -> db -> get();
	    $sourceorg = $querysource->result()[0];
		
		$this->db->where('Deployment_Package_Id', $Package_Id);
		$this->db->delete('deployment_package_validation_list');
		
		
		// additional code to fix revalidate big - Start		
		$resetdata['Status'] = 'Validating';
		$this -> db -> where('Deployment_Package_Id', $Package_Id);
		$this -> db -> update('deployment_package_item', $resetdata);
		// additional code to fix revalidate big - End
		
		//Step 2 start
		$this -> db -> select('*');
		$this -> db -> from('deployment_package_item dp');
		$this -> db -> where('dp.Deployment_Package_Id', $Package_Id);
		$query = $this -> db -> get();
	    $package_item = $query->result();
		
		//step 3 start
		$this -> db -> select('*')->from('rsys_asset_type dp')->order_by("Deploy_Ordinal", "asc");;
		$query = $this -> db -> get();
	    $Asset_Type = $query->result();
		
		//step 4 start
		foreach($package_item as $key=>$val)
		{
			foreach($Asset_Type as $key2=>$val2)
			{
				if($val->Asset_Type == $val2->Asset_Type_Name)
				{
					$package_item[$key]->Deploy_Ordinal = $Asset_Type[$key2]->Deploy_Ordinal;
					$vdata = array();
					$vdata['Deploy_Ordinal'] = $Asset_Type[$key2]->Deploy_Ordinal;
					$this->db->where('Deployment_Package_Item_Id',$val->Deployment_Package_Item_Id);
					$this->db->update('deployment_package_item',$vdata);
					break;
					// Do we have to update this record in DB ?
				}				
			}
		}
		
		//step 5 start
		foreach($package_item as $key=>$val)
		{
			$AssetChildData = $this->getAssetChild($Package_Id,$val->Deployment_Package_Item_Id, $val->Asset_Id,$val->Asset_Type ,$Endpoint_Type = 'Read Single');
			
			if(is_array($AssetChildData) || is_object($AssetChildData))
			{
				foreach($AssetChildData as $ChildKey=>$ChildVal)
				{
					$data['Deployment_Package_Item_Id'] = $val->Deployment_Package_Item_Id;
					$data['Deployment_Package_Id'] = $val->Deployment_Package_Id;
					$data['Asset_Type'] = $ChildVal['Asset_Type'];
					$data['Asset_Id'] = $ChildVal['Asset_Id'];
					$data['Asset_Name'] = $ChildVal['Display_Name'];
					$data['JSON_Node_Value'] = $ChildVal['JSON_Node_Value'];
					$data['Status'] = 'New';
					$this->db->insert('deployment_package_validation_list',$data);
				}
			}
		}
		
		//step 6 start
		$this -> db -> select('*');
		$this -> db -> from('deployment_package_validation_list dp');
		$this -> db -> where('dp.Deployment_Package_Id', $Package_Id);
		$query = $this -> db -> get();
	    $Validation_List = $query->result();
		//step 7 start
		foreach($Validation_List as $key=>$val)
		{
			if($val->Status!= 'Valid')
			{
				$this -> db -> select('*');
				$this -> db -> from('rsys_asset_endpoint rae');
				$this -> db -> where('rae.Asset_Type', $val->Asset_Type);
				$this -> db -> where('rae.Endpoint_Type', 'Find Asset');
				$query = $this -> db -> get();
				$endpoint = $query->result();
				if($endpoint)
				{	
					$url = $org->Base_Url .$endpoint[0]->Endpoint_URL .'?search="'.str_replace(' ', '%20', $val->Asset_Name) .'"' ; 
					if(time()+1800 > $org->Token_Expiry_Time)
					{
					   $result2 = $this->refreshToken($org);
					   $org->Token = $result2['Token']['access_token'];
					}
					$result = $this->get_request($org->Token, $url);
	
					$result = json_decode($result['data']);
					$jsondata1 = array();
					$jsondata1['Source_Asset_JSON'] = json_encode($result);
					$this->db->where('Deployment_Package_Validation_List_Id',$val->Deployment_Package_Validation_List_Id);
					$this->db->update('deployment_package_validation_list',$jsondata1);
					
					if($val->Asset_Type=='Custom Object')
					{
						$this -> db -> select('*');
						$this -> db -> from('rsys_asset_endpoint rae');
						$this -> db -> where('rae.Asset_Type', "Custom Object");
						$this -> db -> where('rae.Endpoint_Type', 'Find Asset');
						$queryCO = $this -> db -> get();
						$endpointCO = $queryCO->result();
						$urlCO = $sourceorg->Base_Url .$endpointCO[0]->Endpoint_URL .'?search="'.str_replace(' ', '%20',$val->Asset_Name) .'"&depth='.$endpointCO[0]->Depth ; 
						if(time()+1800 > $sourceorg->Token_Expiry_Time)
						{
						   $result2 = $this->refreshToken($sourceorg);
						   $sourceorg->Token = $result2['Token']['access_token'];
						}
						$resultCOsource = $this->get_request($sourceorg->Token, $urlCO);	
						$resultCOsource = json_decode($resultCOsource['data']);
						$jsondata = array();
						$jsondata['Source_Asset_JSON'] = json_encode($resultCOsource);
						$this->db->where('Deployment_Package_Validation_List_Id',$val->Deployment_Package_Validation_List_Id);
						$this->db->update('deployment_package_validation_list',$jsondata);
					}	
					if(isset($result->total) && $result->total >0)
					{						
						if($result->elements[0]->type != "CustomObject")
						{
							$vdata = array();
							$vdata['Status'] = 'Valid';
							$vdata['Target_Asset_Id'] = $result->elements[0]->id;
							$this->db->where('Deployment_Package_Validation_List_Id',$val->Deployment_Package_Validation_List_Id);
							$this->db->update('deployment_package_validation_list',$vdata);
						}
						else
						{
							$urlCO = $org->Base_Url .$endpointCO[0]->Endpoint_URL .'?search="'.str_replace(' ', '%20', $result->elements[0]->name) .'"&depth='.$endpointCO[0]->Depth ; 
							if(time()+1800 > $org->Token_Expiry_Time)
							{
							   $result2 = $this->refreshToken($org);
							   $org->Token = $result2['Token']['access_token'];
							}
							$resultCO = $this->get_request($org->Token, $urlCO);	
							$resultCO = json_decode($resultCO['data']);
							$jsondata = array();
							$jsondata['Target_Asset_JSON'] = json_encode($resultCO->elements[0]);
							$this->db->where('Deployment_Package_Validation_List_Id',$val->Deployment_Package_Validation_List_Id);
							$this->db->update('deployment_package_validation_list',$jsondata);
							
							if(!$this->IsCustomDataObjectDifferent($resultCOsource,$resultCO))
							{	
								$vdata = array();
								$vdata['Status'] = 'Valid';
								$vdata['Target_Asset_Id'] = $result->elements[0]->id;
								$this->db->where('Deployment_Package_Validation_List_Id',$val->Deployment_Package_Validation_List_Id);
								$this->db->update('deployment_package_validation_list',$vdata);
							}
							else
							{
								$vdata = array();
								$vdata['Status'] = 'Invalid';
								$this->db->where('Deployment_Package_Validation_List_Id',$val->Deployment_Package_Validation_List_Id);
								$this->db->update('deployment_package_validation_list',$vdata);
						
								$vdata['Status'] = 'Invalid';
								$this->db->where('Deployment_Package_Item_Id',$val->Deployment_Package_Item_Id);
								$this->db->update('deployment_package_item',$vdata);
							}
						}
					}
					else
					{
						$flag = true;
						foreach($package_item as $rckey=>$rcval)
						{
							if($rcval->Asset_Id == $val->Asset_Id)
							{
								$flag = false;
								break;
							}
						}
						if($flag)
						{
							$vdata = array();
							$vdata['Status'] = 'Invalid';
							$this->db->where('Deployment_Package_Validation_List_Id',$val->Deployment_Package_Validation_List_Id);
							$this->db->update('deployment_package_validation_list',$vdata);
						
							$vdata['Status'] = 'Invalid';
							$this->db->where('Deployment_Package_Item_Id',$val->Deployment_Package_Item_Id);
							$this->db->update('deployment_package_item',$vdata);
						}
					}	
				}
			}
		}
		$this -> db -> select('*');
		$this -> db -> from('deployment_package_validation_list dp');
		$this -> db -> where('dp.Deployment_Package_Id', $Package_Id);
		$query = $this -> db -> get();
	    $Validation_List2 = $query->result();
		foreach($Validation_List2 as $key=>$val)
		{
			foreach($package_item as $key2=>$val2)
			{
				if(($val2->Asset_Type == $val->Asset_Type) && ($val2->Asset_Id == $val->Asset_Id))
				{
					$vdata = array();
					$vdata['Status'] = 'Valid';
					$this->db->where('Deployment_Package_Validation_List_Id',$val->Deployment_Package_Validation_List_Id);
					$this->db->update('deployment_package_validation_list',$vdata);
				}
			}
		}
		// Step 9 start
		$uptadedata['Status'] = 'Valid';
		$this -> db -> where('Deployment_Package_Id', $Package_Id);
		$this -> db -> where_not_in('Status', 'Invalid');
		$this -> db -> update('deployment_package_item', $uptadedata);
	}
	
	function get_request($Token, $url) 
	{ 
		// create the cURL resource 
		// $url = urlencode($url);
		$ch = curl_init(); 
		$Authorization = 'Authorization: Bearer' .$Token;
		$headers = array('Authorization: Bearer '.$Token);
		
		// set cURL options 		
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HTTPGET, true);
			
		//curl_setopt($ch, CURLOPT_HEADER, false);		
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);			
		$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		$header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
		// execute request and retrieve the response 
		$data = curl_exec($ch); 	
			
		$responseInfo = curl_getinfo($ch);
		// ////echo($responseInfo['http_code']);		exit;
		curl_close($ch);
		$result['data']=$data;
		// $result['data']= iconv("UTF-8", "ISO-8859-7//TRANSLIT", $data);
		$result['httpCode']=$responseInfo['http_code'];
		return $result; 
	}
	function putRequest($Token, $url , $data) 
	{ 
		$ch = curl_init();
		// set cURL and credential options		
		curl_setopt($ch, CURLOPT_HEADER, true);			
		curl_setopt($ch, CURLOPT_URL, $url);
		$headers = array('Content-Type:application/json','Authorization: Bearer '.$Token);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);		
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');		
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		$response = curl_exec($ch);
		sleep(5);
        // store the response info including the HTTP status
        // 400 and 500 status codes indicate an error
        $responseInfo = curl_getinfo($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		
		$header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
		$body = substr($response, $header_size); 		
		curl_close($ch); 
		$New_JSON_data['Output_result'] = $response;
		$New_JSON_data['body']=$body;
		$New_JSON_data['httpCode'] = $httpCode;
		return $New_JSON_data;
	}
	
	function postRequest($Token, $url, $data_string){
		
		$Authorization = $this->config->item('App_Id').':'.$this->config->item('Client_Secret');
		$ch=curl_init();
		curl_setopt($ch, CURLOPT_URL, $url); 
		curl_setopt($ch, CURLOPT_USERPWD, $Authorization);  
		curl_setopt($ch, CURLOPT_POST, true); 
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
		curl_setopt($ch, CURLOPT_HEADER, false);		
		$headers = array('Content-type: application/json');
		$headers = array('Content-type: application/json','Authorization: Bearer '.$Token);		
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		$result = curl_exec($ch);
		sleep(5);		
		$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		$responseInfo = curl_getinfo($ch);
		$header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
		$body = substr($result, $header_size); 
		curl_close($ch);
		$New_JSON_data['responseInfo'] = $responseInfo;
		$New_JSON_data['Output_result'] = $result;
		$New_JSON_data['body']=$result;
		$New_JSON_data['httpCode'] = $httpCode;
		return $New_JSON_data;
	}
	
	//Code added to account for expiry of tokens -- 14072017
	function validateTokens($Package_Id)
	{
		$this -> db -> select('*');
		$this -> db -> from('deployment_package dp');
		$this -> db -> join('instance_ inst','dp.Target_Site_Name= inst.Site_Name', 'LEFT OUTER');
		$this -> db -> where('dp.Deployment_Package_Id', $Package_Id);
		// $this -> db -> where('dp.Created_By_Contact_Id', $Contact_Id);
		$this -> db -> limit(1);
		$query = $this -> db -> get();
		if(isset($query->result()[0]))
		{
			$org = $query->result()[0];
			
			$this -> db -> select('*');
			$this -> db -> from('deployment_package dp');
			$this -> db -> join('instance_ inst','dp.Source_Site_Name= inst.Site_Name', 'LEFT OUTER');
			$this -> db -> where('dp.Deployment_Package_Id', $Package_Id);
			$this -> db -> limit(1);
			$querysource = $this -> db -> get();
			$sourceorg = $querysource->result()[0];
			
			if(time()+1800 > $sourceorg->Token_Expiry_Time)
			{
			   $result2 = $this->eloqua->refreshToken($sourceorg);
			}
			
			$this -> db -> select('*');
			$this -> db -> from('deployment_package dp');
			$this -> db -> join('instance_ inst','dp.Target_Site_Name= inst.Site_Name', 'LEFT OUTER');
			$this -> db -> where('dp.Deployment_Package_Id', $Package_Id);
			$this -> db -> limit(1);
			$querytarget= $this -> db -> get();
			$targetorg = $querytarget->result()[0];
			
			if(time()+1800 > $targetorg->Token_Expiry_Time)
			{
			   $result2 = $this->eloqua->refreshToken($targetorg);
			}
		}		
	}
	
	function refreshToken($data)
    {		
		// //echo('Mising 2');
		$returntoken = $this->requestToken($data->Refresh_Token,'refresh_token');
		// //echo('Mising 4');
		if(isset($returntoken['access_token']))
		{
			$tdata['Token'] = $returntoken['access_token'];
			$tdata['Refresh_Token'] = $returntoken['refresh_token'];
			$tdata['Token_Expiry_Time'] = time() + $returntoken['expires_in'];
			$this->db->where('Site_Name', $data->Site_Name);
			$this->db->update('instance_' ,$tdata);
			$result['success'] = true;		
			$result['Token'] = $returntoken;
		}
		else
		{
			$result['success'] = false;		
			$result['Token'] = $returntoken;
		}		
		return $result;		
	}
	
	function requestToken($code,$grant_type)
	{		
		// //echo('missing 3.1');
		$curl_post_data["grant_type"]= $grant_type;
		if($grant_type == "refresh_token")
		{
			$curl_post_data["refresh_token"]= $code;
			$curl_post_data["scope"]= "full";
		}
		else
		{
			$curl_post_data["code"]= $code;
		}		
		$curl_post_data["redirect_uri"]= $this->config->item('redirect_uri');			
		$curl_post_data = json_encode($curl_post_data );
		$Authorization = $this->config->item('App_Id').':'.$this->config->item('Client_Secret');
		$url = $this->config->item('TokenUrl');
		$ch=curl_init();
		curl_setopt($ch, CURLOPT_URL, $url); 
		curl_setopt($ch, CURLOPT_USERPWD, $Authorization);  
		curl_setopt($ch, CURLOPT_POST, true); 
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $curl_post_data);
		curl_setopt($ch, CURLOPT_HEADER, false);		
		$headers = array('Content-type: application/json');
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		$result = curl_exec($ch);
		$result1 = json_decode($result,true); 
		$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		// ////echo($result1);exit;
		$responseInfo = curl_getinfo($ch);
		curl_close($ch);
		// //echo('missing 3.2');
		////echo($result1);
		return($result1);
	}

	function postDeployLog($id,$data)
	{
		$tempdata['user_id'] = $id;
		$tempdata['changeset_id'] = $data['changeset_id'];
		$tempdata['destination'] = $data['org2'][0]->id;
		$tempdata['status'] = 1;
		$this->db->insert('changeset_log', $tempdata); 		
		//$result['success'][] = true;					
		$result['changesetLogId'] = $this->db->insert_id();
		foreach($data['meta'] as $key=>$val)
		{
			$tempdata2['changeset_log_id'] = $result['changesetLogId'];
			$tempdata2['source_id'] = $data['org1'][0]->id;
			$tempdata2['destination_id'] = $data['org2'][0]->id;
			$tempdata2['meta_type'] = $val->type;
			$tempdata2['meta'] = json_encode($val);
			$postrespons = $this->postAsset($data['org2'][0],$val);
			
			$tempdata2['msg'] = json_encode($postrespons['result']);
			$tempdata2['status'] = $postrespons['httpCode'];
			$this->db->insert('Changeset_Components_Log', $tempdata2); 	
		}
		$data = array('status' => 1,);
		$this->db->where('id', $tempdata['changeset_id']);
		$this->db->update('Changeset', $data); 
		
		return true;
	}
	
	
	function getSearchAsset($org , $meta , $page =1 , $searchtext ,$Endpoint_Type = 'Find Asset')
	{
		$this -> db -> select('');
		$this -> db -> from('rsys_asset_type at');
		$this -> db -> join('rsys_asset_endpoint ep','at.Asset_Type_Name= ep.Asset_Type', 'LEFT OUTER');
		$this -> db -> where('Asset_Type_Name', $meta);
		$this -> db -> where('Endpoint_Type', $Endpoint_Type);
		$this -> db -> limit(1);
	    $query = $this -> db -> get();
	    $resultend = $query->result()[0];
		
		$url = $org->Base_Url.$resultend->Endpoint_URL .'?depth='.$resultend->Depth .'&count='.$resultend->Page_Size ."&page=".$page;
		
		
		if($Endpoint_Type == "Find Asset")
		{
			// $url .= '&search="*'.str_replace(' ', '+',$searchtext).'*"'; 
			$url .= '&search="*'.urlencode(trim($searchtext)).'*"'; 
		}
		if(time()+1800 > $org->Token_Expiry_Time)
		{
		   $result2 = $this->refreshToken($org);
		   $org->Token = $result2['Token']['access_token'];
		}
		$result = $this->get_request($org->Token, $url);
		
		if($result['httpCode']==401)
		{
			$result = $this->requestToken($org->Refresh_Token,'refresh_token');
			$this->updateTokens($org->Site_Id,$result['access_token'],$result['refresh_token']);
			$result = $this->get_request($result['access_token'],$url);
		}	
		return $result['data'];
	}
}			
?>