<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Instance_info_model extends CI_model 
{
	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->load->model('License_model','',TRUE);
		// $this->legacy_db = $this->load->database('parent', true);   
    }
		
	function checkSiteId($data)
	{
		$this -> db -> select('Site_Id');
		$this -> db -> from('instance_');
		$this -> db -> where('Site_Id ', $data['siteId']);
		$session_data = $this->session->userdata('logged_in');
		$query = $this -> db -> get();
		$result = $query->result();
		if($query -> num_rows() > 0)
		{
			$this -> db -> select('*');
			$this -> db -> from('user_info');
			$this -> db -> where('user_Id ', $data['UserId']);
			
			$query = $this -> db -> get();
			$result = $query->result();
			if($query -> num_rows() > 0)
			{
			}
			else
			{
				$tdata['user_id'] = $data['UserId'];
				$tdata['user_name'] = $data['userName'];
				$tdata['site_id'] = $data['siteId'];
				$tdata['site_name'] = $data['siteName'];
				$this->db->insert('user_info', $tdata);
			}		
		}
		else //when there is no instance in DB
		{
			$tdata['Site_Id'] = $data['siteId'];
			$tdata['Site_Name'] = $data['siteName'];
			$tdata['Base_Url'] = '';
			$this->db->insert('instance_', $tdata);
			
			
			
			$mdata['user_id'] = $data['UserId'];
			$mdata['user_name'] = $data['userName'];
			$mdata['site_id'] = $data['siteId'];
			$mdata['site_name'] = $data['siteName'];
			$this->db->insert('user_info', $mdata);
			
			
		}
		
		return $data['siteName'];
	}

	//This method updates the instance_ record for the sitename. Sitename is entered manually when the client buys the license for that site.
	function updateOnInstall($data)
	{
		$this -> db -> select('*');
		$this -> db -> from('instance_');
		$this -> db -> where('Site_Name ', $data['siteName']); 
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			$licensed = $this->License_model->isSiteLicensed($data['siteName']);
			if($licensed)
			{
				$tdata['Site_Id'] = $data['siteId'];
				$tdata['User_Name'] = $data['userName'];
				$tdata['User_Id'] = $data['UserId'];
				$this->db->where('Site_Name',$data['siteName']);
				$this->db->update('instance_',$tdata);
				return 'success';
			}
			else
			{
				return 'NoLicense';
			}
		}
		else
		{
			//if no site found with that name, then no license found
			return 'NoSite'; 
		}
		
	}

	function updateOnUninstalled($data){
		$this -> db -> select('Site_Id');
		$this -> db -> from('instance_');
		$this -> db -> where('Site_Name ', $data['siteName']);
		$query = $this -> db -> get();
		if($query -> num_rows() > 0){
		

			$tdata['Token_Expiry_Time'] = "";
			$tdata['Token'] 		= "";
			// $tdata['Refresh_Token'] = "";
			$this -> db -> where('Site_Name ', $data['siteName']);
			$this -> db -> update('instance_', $tdata);
			
			$pidata['Status'] 		=  "Inactive";
			$this -> legacy_db -> where('Product_Name', 'Oracle Oracle Eloqua Transporter');
			$this -> legacy_db -> where('Site_Name', $data['siteName']);
			$this->legacy_db->update('product_install', $pidata);
			
		}
		
	}
	
	function saveToken($Token)
    {
		$returntoken = $this->requestToken('',$Token['code']);
		$utils = Array();
		$utils = explode('|',$Token['state']);
		if(isset($returntoken['access_token']))
		{	
			$data['Token'] = $returntoken['access_token'];
			$data['Refresh_Token'] = $returntoken['refresh_token'];
			
			$eloquadata = $this->get_request($returntoken['access_token'] , "https://login.eloqua.com/id");
			$eloquadata = json_decode($eloquadata);
					
			$data['Base_Url'] = $eloquadata->urls->base .'/';
			
			$this -> db -> select('Site_Id');
			$this -> db -> from('instance_');
			$this -> db -> where('Site_Id ', $eloquadata->site->id);
			
			$query = $this -> db -> get();
			$result = $query->result();
			
			if($query -> num_rows() > 0)
			{
				$tdata['Refresh_Token'] = $returntoken['refresh_token'];
				$tdata['Token'] = $returntoken['access_token'];
				$tdata['Base_Url'] = $eloquadata->urls->base;
				$tdata['Token_Expiry_Time'] = $returntoken['expires_in'] + time();
				$tdata['Install_Id'] = $utils[3];
				$this->db->where('Site_Id',$eloquadata->site->id);
				$this->db->update('instance_', $tdata);
				
				$this -> db -> select('*');
				$this -> db -> from('user_info');
				$this -> db -> where('user_Id ', $eloquadata->user->id);
				$this->db->where('Site_Id',$eloquadata->site->id);
				$query = $this -> db -> get();
				$result = $query->result();
				if($query -> num_rows() > 0)
				{
					$qdata['user_id'] = $eloquadata->user->id;
					$qdata['user_name'] = $eloquadata->user->username;
					$qdata['site_name'] = $eloquadata->site->name;
					$qdata['Refresh_Token'] = $returntoken['refresh_token'];
					$qdata['Token'] = $returntoken['access_token'];
					$this->db->where('user_Id ', $eloquadata->user->id);
					$this->db->where('Site_Id',$eloquadata->site->id);
					$this->db->update('user_info', $qdata);
				}
				else
				{
					$qdata['user_id'] = $eloquadata->user->id;
					$qdata['user_name'] = $eloquadata->user->username;
					$qdata['site_id'] = $eloquadata->site->id;
					$qdata['site_name'] = $eloquadata->site->name;
					$qdata['Refresh_Token'] = $returntoken['refresh_token'];
					$qdata['Token'] = $returntoken['access_token'];
					$this->db->insert('user_info', $qdata);
				}		
			}
			// else //when there is no instance in DB
			// {
				// $tdata['Site_Id'] = $eloquadata->site->id;
				// $tdata['Site_Name'] = $eloquadata->site->name;
				// $tdata['Base_Url'] = $eloquadata->urls->base;
				// $tdata['User_Name'] = $eloquadata->user->username;
				// $tdata['User_Id'] = $eloquadata->user->id;
				// $tdata['Refresh_Token'] = $returntoken['refresh_token'];
				// $tdata['Token'] = $returntoken['access_token'];
				// $tdata['Token_Expiry_Time'] = $returntoken['expires_in'] + time();
				// $this->db->insert('instance_', $tdata);
				
				// if(isset($utils[1]) && $utils[1]!=$eloquadata->site->name)
				// {
					// $jdata['parent_siteId'] = $utils[2];
					// $jdata['parent_siteName'] = $utils[1];
					// $jdata['child_siteId'] = $eloquadata->site->id;
					// $jdata['child_siteName'] = $eloquadata->site->name;
					// $this->db->insert('instance_mapping', $jdata);
				// }
				
				// $pdata['user_id'] = $eloquadata->user->id;
				// $pdata['user_name'] = $eloquadata->user->username;
				// $pdata['site_id'] = $eloquadata->site->id;
				// $pdata['site_name'] = $eloquadata->site->name;
				// $pdata['Refresh_Token'] = $returntoken['refresh_token'];
				// $pdata['Token'] = $returntoken['access_token'];
				// $this->db->insert('user_info', $pdata);
			// }
		
			$result['success'] = true;		
			$result['token'] = $returntoken;  
			$result['callback'] = $utils[0];
			
			
		}
		else
		{
			$result['success'] = false;		
			$result['token'] = $returntoken;
		}
				
		return $result;		
	}
	
	function getInstance($siteName){
		
		$this -> db -> select('License_Id, Site_Id');
		$this -> db -> from('instance_');
		$this -> db -> where('Site_Name ', $siteName);
		$this -> db -> limit(1);
		$query = $this -> db -> get();
		return $query->result();
	}
	
	function updateInstance($OldLicense_Id,$NewLicense_Id)
	{
		$this -> db -> select('Instance__Id,License_Id, Site_Id');
		$this -> db -> from('instance_');
		$this -> db -> where('License_Id ',$OldLicense_Id);
		$query = $this -> db -> get();
		$Instance = array();
		foreach($query->result() as $key=>$val)
		{
			$contact_Instance[] = array
									(
										'Instance__Id'=>$val->Instance__Id,
										'License_Id' => $NewLicense_Id
									);
		}
		$this->db->update_batch('instance_', $contact_Instance, 'Instance__Id');
	}
	
	function getContact_License($Contact_Id,$License_Id)
	{
		$this -> db -> select('*');
		$this -> db -> from('contact__license');
		$this -> db ->where('Contact_Id', $Contact_Id);
		$this -> db ->where('License_Id',$License_Id);
		$this -> db -> limit(1);
		$query = $this -> db -> get();
		return $query->result();
	}
	
	function updateContact_License($OldLicense_Id,$NewLicense_Id)
	{
		$this -> db -> select('*');
		$this -> db -> from('contact__license');
		$this -> db ->where('License_Id',$OldLicense_Id);
		$query = $this -> db -> get();
		$contact_license= array();
		foreach($query->result() as $key=>$val)
		{
			$contact_license[] = array
									(
										'Contact__License__Id'=>$val->Contact__License__Id,
										'License_Id' => $NewLicense_Id
				
									);
			
		}
		$this->db->update_batch('contact__license', $contact_license, 'Contact__License__Id');
	}

	
	function getLicense($License_Id)
	{
		$this -> db -> select('*');
		$this -> db -> from('license');
		$this -> db -> where('License_Id',$License_Id);
		$this -> db -> limit(1);
		$query_license = $this -> db -> get();
		
		return $query_license -> result();
	}
	
	
	
	 private function get_request($access_token, $url) 
	{ 
		// create the cURL resource 
		$ch = curl_init(); 
		$Authorization = 'Authorization: Bearer' .$access_token;
		$headers = array('Authorization: Bearer '.$access_token);
		
		// set cURL options 		
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HTTPGET, true);
		
		//curl_setopt($ch, CURLOPT_HEADER, false);		
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);			
			
		// execute request and retrieve the response 
		$data = curl_exec($ch); 		
		$responseInfo = curl_getinfo($ch);
		$httpCode = curl_getinfo($ch);
		// print_r($httpCode); 	
		// print_r($data);  
		
		// close resources and return the response 
		curl_close($ch); 		
		return $data; 
	}
				
				
				
	function requestToken($url, $code)
	{
		$curl_post_data = array
							(
								"grant_type" => 'authorization_code',
								"code" => $code,
								"redirect_uri" => $this->config->item('redirect_uri'),
							);
		$curl_post_data = json_encode($curl_post_data );
		$Authorization = $this->config->item('App_Id').':'.$this->config->item('Client_Secret');
		$url = 'https://login.eloqua.com/auth/oauth2/token/';
		$ch=curl_init();
		curl_setopt($ch, CURLOPT_URL, $url); 
		curl_setopt($ch, CURLOPT_USERPWD, $Authorization);  
		curl_setopt($ch, CURLOPT_POST, true); 
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $curl_post_data);
		curl_setopt($ch, CURLOPT_HEADER, false);
		
		$headers = array('Content-type: application/json');
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		$result = curl_exec($ch);
		$result1 = json_decode($result,true); 
		$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);
		//print_r($result1);
		return($result1);
	}
}