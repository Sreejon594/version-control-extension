<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Eloqua extends CI_Model{
	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		 
    }	
	function get_AccountFields($org1,$org2)
    {	
		
		$meta = 'AccountField';
		$dataOrg1 = $this->getAsset($org1[0] , $meta);		
		$dataOrg2 = '';
		if(isset($org2[0])){
			$dataOrg2 = $this->getAsset($org2[0] , $meta);
		}
		$data['difflist'] = array();
		if(isset($dataOrg1) && $dataOrg1 !=NULL)
		$data['difflist'] =  $this->objdiff($dataOrg1,$dataOrg2, array('name'),'internalName');		
		return $data;
    }
	function get_AccountFieldsById($org1,$id)
    {	
		$meta = 'AccountField';
		$dataOrg1 = $this->getAsset($org1[0] , $meta);	
		return json_decode($dataOrg1);
    }	
	
	function get_ContactFields($org1,$org2)
    {	
		$meta = 'ContactField';
		$dataOrg1 = $this->getAsset($org1[0] , $meta);		
		$dataOrg2 = '';
		if(isset($org2[0])){
			$dataOrg2 = $this->getAsset($org2[0] , $meta);
		}
		$data['difflist'] = array();
		if(isset($dataOrg1) && $dataOrg1 !=NULL)
		$data['difflist'] =  $this->objdiff($dataOrg1,$dataOrg2,array('name'),'internalName');
		return $data;
    }
	function get_ContactFieldsById($org1,$id)
    {	
		$meta = 'ContactField';
		$dataOrg1 = $this->getAsset($org1[0] , $meta);		
		return json_decode($dataOrg1);
    }
	function get_CustomObjectsFields($org1,$org2)
    {			
		$meta = 'CustomObject';
		$dataOrg1 = $this->getAsset($org1[0] , $meta);		
		$dataOrg2 = '';
		if(isset($org2[0])){
			$dataOrg2 = $this->getAsset($org2[0] , $meta);
		}
		$data['difflist'] = array();
		if(isset($dataOrg1) && $dataOrg1 !=NULL)
		$data['difflist'] =  $this->objdiff($dataOrg1,$dataOrg2,array('name'),'name');
		return $data;
    }	
	function get_CustomObjectsFieldsById($org1,$id)
    {	
		$meta = 'CustomObject';
		$dataOrg1 = $this->getAsset($org1[0] , $meta);
		return json_decode($dataOrg1);
    }	
	function get_SegmentList($org1,$org2)
    {			
		$meta = 'ContactSegment';
		$dataOrg1 = $this->getAsset($org1[0] , $meta);		
		$dataOrg2 = '';
		if(isset($org2[0])){
			$dataOrg2 = $this->getAsset($org2[0] , $meta);
		}
		$data['difflist'] = array();
		// echo '<pre>';print_r($dataOrg1); echo '</pre>';
		if(isset($dataOrg1) && $dataOrg1 !=NULL)
		$data['difflist'] =  $this->objdiff($dataOrg1,$dataOrg2,array('currentStatus','permissions','elements'),'name');
		return $data;
    }
	function get_SegmentListById($org1,$id)
    {	
		$meta = 'ContactSegment';
		$dataOrg1 = $this->getAsset($org1[0] , $meta);		
		return json_decode($dataOrg1);
    }
	function get_FilterList($org1,$org2)
    {	
		$meta = 'ContactFilter';
		$dataOrg1 = $this->getAsset($org1[0] , $meta);		
		$dataOrg2 = '';
		if(isset($org2[0])){
			$dataOrg2 = $this->getAsset($org2[0] , $meta);
		}
		$data['difflist'] = array();
		if(isset($dataOrg1) && $dataOrg1 !=NULL)
		$data['difflist'] =  $this->objdiff($dataOrg1,$dataOrg2, array('depth'),'name');		
		return $data;
    }
	function get_FilterListById($org1,$id)
    {	
		$meta = 'ContactFilter';
		$dataOrg1 = $this->getAsset($org1[0] , $meta);		
		$dataOrg2 = '';
		if(isset($org2[0])){
			$dataOrg2 = $this->getAsset($org2[0] , $meta);
		}
		return json_decode($dataOrg1);
    }	
	function get_EmailList($org1,$org2)
    {	
		$meta = 'Email';
		$dataOrg1 = $this->getAsset($org1[0] , $meta);		
		$dataOrg2 = '';
		if(isset($org2[0])){
			$dataOrg2 = $this->getAsset($org2[0] , $meta);
		}
		$data['difflist'] = array();
		if(isset($dataOrg1) && $dataOrg1 !=NULL)
		$data['difflist'] =  $this->objdiff($dataOrg1,$dataOrg2, array('depth'),'name');		
		return $data;
    }
	function get_EmailListById($org1,$id)
    {	
		$meta = 'Email';
		$dataOrg1 = $this->getAsset($org1[0] , $meta);		
		return json_decode($dataOrg1);
    }
	function get_FormsList($org1,$org2)
    {	
		$meta = 'Form';
		$dataOrg1 = $this->getAsset($org1[0] , $meta);		
		$dataOrg2 = '';
		if(isset($org2[0])){
			$dataOrg2 = $this->getAsset($org2[0] , $meta);
		}
		$data['difflist'] = array();
		if(isset($dataOrg1) && $dataOrg1 !=NULL)
		$data['difflist'] =  $this->objdiff($dataOrg1,$dataOrg2, array('depth'));		
		return $data;
    }
	function get_FormsListId($org1,$id)
    {	
		$meta = 'Form';
		$dataOrg1 = $this->getAsset($org1[0] , $meta);		
		return json_decode($dataOrg1);
    }
	function get_LandingPagesList($org1,$org2)
    {	
		$meta = 'LandingPage';
		$dataOrg1 = $this->getAsset($org1[0] , $meta);		
		$dataOrg2 = '';
		if(isset($org2[0])){
			$dataOrg2 = $this->getAsset($org2[0] , $meta);
		}
		$data['difflist'] = array();
		if(isset($dataOrg1) && $dataOrg1 !=NULL)
		$data['difflist'] =  $this->objdiff($dataOrg1,$dataOrg2, array('depth'));		
		return $data;
    }
	function get_LandingPagesListById($org1,$id)
    {	
		$meta = 'LandingPage';
		$dataOrg1 = $this->getAsset($org1[0] , $meta);		
		return json_decode($dataOrg1);
    }
	function get_CampaignsList($org1,$org2)
    {	
		$meta = 'Campaign';
		$dataOrg1 = $this->getAsset($org1[0] , $meta);		
		$dataOrg2 = '';
		if(isset($org2[0])){
			$dataOrg2 = $this->getAsset($org2[0] , $meta);
		}
		$data['difflist'] = array();
		if(isset($dataOrg1) && $dataOrg1 !=NULL)
		$data['difflist'] =  $this->objdiff($dataOrg1,$dataOrg2, array('depth'));
		return $data;		
    }
	function get_CampaignsListById($org1,$id)
    {	
		$meta = 'Campaign';
		$dataOrg1 = $this->getAsset($org1[0] , $meta);		
		return json_decode($dataOrg1);
    }
	
	function get_SharedList($org1,$org2)
    {	
    }
	function get_SharedListById($org1,$id)
    {			
    }	
	function get_EmailGroups($org1,$org2)
    {	
		$meta = 'Folder';
		$dataOrg1 = $this->getAsset($org1[0] , $meta);		
		$dataOrg2 = '';
		if(isset($org2[0])){
			$dataOrg2 = $this->getAsset($org2[0] , $meta);
		}
		$data['difflist'] = array();
		if(isset($dataOrg1) && $dataOrg1 !=NULL)
		$data['difflist'] =  $this->objdiff($dataOrg1,$dataOrg2, array('depth'));	
		return $data;
    }
	function get_EmailGroupsById($org1,$id)
    {	
		$meta = 'Folder';
		$dataOrg1 = $this->getAsset($org1[0] , $meta);		
		return json_decode($dataOrg1);
    }
	function get_OptionLists($org1,$org2)
    {	
		$meta = 'OptionList';
		$dataOrg1 = $this->getAsset($org1[0] , $meta);		
		$dataOrg2 = '';
		if(isset($org2[0])){
			$dataOrg2 = $this->getAsset($org2[0] , $meta);
		}
		$data['difflist'] = array();
		if(isset($dataOrg1) && $dataOrg1 !=NULL)
		$data['difflist'] =  $this->objdiff($dataOrg1,$dataOrg2, array('depth'));	
		return $data;
    }
	function get_OptionListsById($org1,$id)
    {		
		$meta = 'OptionList';
		$dataOrg1 = $this->getAsset($org1[0] , $meta);		
		return json_decode($dataOrg1);
    }	
	function get_EventList($org1,$org2)
    {	
		$meta = 'EventRegistration';
		$dataOrg1 = $this->getAsset($org1[0] , $meta);		
		$dataOrg2 = '';
		if(isset($org2[0])){
			$dataOrg2 = $this->getAsset($org2[0] , $meta);
		}
		$data['difflist'] = array();
		if(isset($dataOrg1) && $dataOrg1 !=NULL)
		$data['difflist'] =  $this->objdiff($dataOrg1,$dataOrg2, array('depth'));
		return $data;
    }
	function get_EventListById($org1,$id)
    {		
		$meta = 'EventRegistration';
		$dataOrg1 = $this->getAsset($org1[0] , $meta);		
		return json_decode($dataOrg1);
    }	
	function get_ContactViews($org1,$org2)
    {	
		$meta = 'ContactView';
		$dataOrg1 = $this->getAsset($org1[0] , $meta);		
		$dataOrg2 = '';
		if(isset($org2[0])){
			$dataOrg2 = $this->getAsset($org2[0] , $meta);
		}
		$data['difflist'] = array();
		if(isset($dataOrg1) && $dataOrg1 !=NULL)
		$data['difflist'] =  $this->objdiff($dataOrg1,$dataOrg2, array('depth'));		
		return $data;
    }
	function get_ContactViewsById($org1,$id)
    {	
		$meta = 'ContactView';
		$dataOrg1 = $this->getAsset($org1[0] , $meta);		
		return json_decode($dataOrg1);
    }
	function get_Account_Views($org1,$org2)
    {	
		$meta = 'AccountView';
		$dataOrg1 = $this->getAsset($org1[0] , $meta);		
		$dataOrg2 = '';
		if(isset($org2[0])){
			$dataOrg2 = $this->getAsset($org2[0] , $meta);
		}
		$data['difflist'] = array();
		if(isset($dataOrg1) && $dataOrg1 !=NULL)
		$data['difflist'] =  $this->objdiff($dataOrg1,$dataOrg2, array('depth'));		
		return $data;
    }
	function get_Account_ViewsById($org1,$id)
    {	
		$meta = 'AccountView';
		$dataOrg1 = $this->getAsset($org1[0] , $meta);		
		return json_decode($dataOrg1);
    }
	
	
	
	private function get_request($token, $url) 
	{ 
		// create the cURL resource 
		$ch = curl_init(); 
		$Authorization = 'Authorization: Bearer' .$token;
		$headers = array('Authorization: Bearer '.$token);
		
		// set cURL options 		
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HTTPGET, true);
			
		//curl_setopt($ch, CURLOPT_HEADER, false);		
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);			
			
		// execute request and retrieve the response 
		$data = curl_exec($ch); 		
		$responseInfo = curl_getinfo($ch);
		$httpCode = curl_getinfo($ch);
		//print_r($httpCode);
		//print_r($data);
		// close resources and return the response 
		curl_close($ch); 		
		return $data; 
	}
	private function putRequest($org, $url , $data) 
	{ 
		$data_string['id'] =$data['AssetId'];
		$data_string['name'] =$data['assetrename'];
		$ch = curl_init();
		// set cURL and credential options		
		curl_setopt($ch, CURLOPT_HEADER, true);			
		curl_setopt($ch, CURLOPT_URL, $url);
		$headers = array('Content-Type:application/json','Authorization: Bearer '.$org->token);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);		
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');		
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data_string));
		$response = curl_exec($ch);
        // store the response info including the HTTP status
        // 400 and 500 status codes indicate an error
        $responseInfo = curl_getinfo($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);        
        if ($httpCode > 400) 
        {            
           // print_r($this->responseInfo);            
        }
		curl_close($ch); 	
		return $httpCode;	 	
	}
	
	function postRequest($org, $url, $data_string){
		
		
		$curl_post_data = json_encode($data_string );
			// print_r($url);
			// print_r($curl_post_data);
			// print_r($data_string);
		$Authorization = $this->config->item('App_Id').':'.$this->config->item('Client_Secret');
		$ch=curl_init();
		curl_setopt($ch, CURLOPT_URL, $url); 
		curl_setopt($ch, CURLOPT_USERPWD, $Authorization);  
		curl_setopt($ch, CURLOPT_POST, true); 
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $curl_post_data);
		curl_setopt($ch, CURLOPT_HEADER, false);		
		$headers = array('Content-type: application/json');
		// curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		$headers = array('Content-type: application/json','Authorization: Bearer '.$org->token);		
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		$result = curl_exec($ch);		
		$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		$result1['result'] = $result; 
		$result1['httpCode'] = $httpCode; 
		curl_close($ch);
		// print_r($result1);
		// print_r($httpCode);
		return($result1);
	}
	
	private function objdiff($dataOrg1,$dataOrg2,$diffcol,$index = 'internalName')
	{
		$obj1 = json_decode($dataOrg1);
		// echo '<pre>';print_r($obj1); echo '</pre>';
		$obj2 = json_decode(isset($dataOrg2)?$dataOrg2:'');
		$difflist = array();
		foreach($obj1->elements as $key =>$vall)
		{	$tempobj=array();
			$tempobj['difference'] = 'added';
			$tempobj['obj'] = $vall;
			if(isset($obj2)){
			foreach($obj2->elements as $key2 =>$vall2)
			{
				// print_r($vall);print_r($vall2);
				if($vall->$index == $vall2->$index)
				{	$tempobj['difference'] = '';
					foreach($diffcol as $key3 =>$vall3)
					{
						if(isset($vall->$vall3) && isset($vall2->$vall3))
						{
							if(!is_array($vall->$vall3)) //&& !is_object($vall->$vall3)
							{
								if($vall->$vall3 != $vall2->$vall3)
								{
									$tempobj['difference'] = 'changed';
								}
							}
							elseif(is_array($vall->$vall3))
							{
								$diff = array_diff_assoc($vall->$vall3,$vall2->$vall3);
								if(sizeof($diff) >0 )
								{
									$tempobj['difference'] = 'changed';
								}
							}							
						}
					}
					$tempobj['obj2'] = $vall2;
					unset($obj2->elements[$key2]);
				}
			}
			}
			$difflist[]= $tempobj;
		}
		$tempobj= array();
		if(isset($obj2)){
		foreach($obj2->elements as $key2 =>$vall2)
		{
			$tempobj['difference'] = 'removed';
			$tempobj['obj'] = $vall2;
			$difflist[]= $tempobj;
		}
		}
		return $difflist;
	}	
	
	function refreshToken($id,$data)
    {		
		$returntoken = $this->requestToken($data->refresh_token,'refresh_token');
		if(isset($returntoken['access_token']))
		{
			$tdata['token'] = $returntoken['access_token'];
			$tdata['token_type'] = $returntoken['token_type'];
			$tdata['refresh_token'] = $returntoken['refresh_token'];
			$tdata['ExpiresAt'] = now() + $returntoken['expires_in'];
			$this->db->where('users_id', $id);
			$this->db->where('id', $data->id);
			$this->db->update('eloqua_instance' ,$tdata);
			$result['success'] = true;		
			$result['token'] = $returntoken;
		}
		else{
			$result['success'] = false;		
			$result['token'] = $returntoken;
		}		
		return $result;		
	}
	
	function requestToken($code,$grant_type){		
		$curl_post_data["grant_type"]= $grant_type;
		if($grant_type == "refresh_token")
		{
			$curl_post_data["refresh_token"]= $code;
			$curl_post_data["scope"]= "full";
		}else{
			$curl_post_data["code"]= $code;
		}		
		$curl_post_data["redirect_uri"]= $this->config->item('redirect_uri');			
		$curl_post_data = json_encode($curl_post_data );
		$Authorization = $this->config->item('App_Id').':'.$this->config->item('Client_Secret');
		$url = $this->config->item('TokenUrl');
		$ch=curl_init();
		curl_setopt($ch, CURLOPT_URL, $url); 
		curl_setopt($ch, CURLOPT_USERPWD, $Authorization);  
		curl_setopt($ch, CURLOPT_POST, true); 
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $curl_post_data);
		curl_setopt($ch, CURLOPT_HEADER, false);		
		$headers = array('Content-type: application/json');
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		$result = curl_exec($ch);
		$result1 = json_decode($result,true); 
		$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);
		return($result1);
	}

	function postDeployLog($id,$data)
	{
		$tempdata['user_id'] = $id;
		$tempdata['changeset_id'] = $data['changeset_id'];
		$tempdata['destination'] = $data['org2'][0]->id;
		$tempdata['status'] = 1;
		$this->db->insert('changeset_log', $tempdata); 		
		//$result['success'][] = true;					
		$result['changesetLogId'] = $this->db->insert_id();
		foreach($data['meta'] as $key=>$val)
		{
			//$tempdata2['user_id'] = $id;
			$tempdata2['changeset_log_id'] = $result['changesetLogId'];
			$tempdata2['source_id'] = $data['org1'][0]->id;
			$tempdata2['destination_id'] = $data['org2'][0]->id;
			$tempdata2['meta_type'] = $val->type;
			$tempdata2['meta'] = json_encode($val);
			$postrespons = $this->postAsset($data['org2'][0],$val);
			
			$tempdata2['msg'] = json_encode($postrespons['result']);
			$tempdata2['status'] = $postrespons['httpCode'];
			$this->db->insert('changeset_components_log', $tempdata2); 	
		}
		$data = array('status' => 1,);
		$this->db->where('id', $tempdata['changeset_id']);
		$this->db->update('changeset', $data); 
		//exit;
		return true;
	}
	
	function postAsset($org , $meta)
	{
		$result = $meta;
		$data = array();
		$data = $meta;		
		if(now() > $org->ExpiresAt)
		{
			$result = $this->refreshToken($org->users_id,$org);
			$org->token = $result['token']['access_token'];
		}
		$this -> db -> select('at.Asset_Type_Id,Asset_Type_Name,Endpoint_URL,Depth');
		$this -> db -> from('Rsys_Asset_Type at');
		$this -> db -> join('rsys_asset_endpoint ep','at.Asset_Type_Id= ep.Asset_Type_Id', 'LEFT OUTER');
		$this -> db -> where('Asset_Type_Name', $meta);
		$this -> db -> limit(1);
	    $query = $this -> db -> get();
	    $resultend = $query->result()[0];
		$url = $org->base_url.$resultend->Endpoint_URL;
		$result = $this->postRequest($org, $url, $data);
		return $result;
	}

	function getAsset($org , $meta , $page =1 , $Endpoint_Type = 'get_lists')
	{
		if(now() > $org->ExpiresAt)
		{
			$result2 = $this->refreshToken($org->users_id,$org);
			$org->token = $result2['token']['access_token'];
		}	
		$this -> db -> select('at.Asset_Type_Id,Asset_Type_Name,Endpoint_URL,Depth,Page_Size');
		$this -> db -> from('Rsys_Asset_Type at');
		$this -> db -> join('rsys_asset_endpoint ep','at.Asset_Type_Id= ep.Asset_Type_Id', 'LEFT OUTER');
		$this -> db -> where('Asset_Type_Name', $meta);
		$this -> db -> where('Endpoint_Type', $Endpoint_Type);
		$this -> db -> limit(1);
	    $query = $this -> db -> get();
	    $resultend = $query->result()[0];
		$url = $org->base_url.$resultend->Endpoint_URL .'?depth='.$resultend->Depth .'&count='.$resultend->Page_Size ."&page=".$page;
		$result = $this->get_request($org->token, $url);
		return $result;
	}	
	
	function getSearchAsset($org , $meta , $page =1 , $searchtext ,$Endpoint_Type = 'search',)
	{
		if(now() > $org->ExpiresAt)
		{
			$result2 = $this->refreshToken($org->users_id,$org);
			$org->token = $result2['token']['access_token'];
		}	
		$this -> db -> select('at.Asset_Type_Id,Asset_Type_Name,Endpoint_URL,Depth,Page_Size');
		$this -> db -> from('Rsys_Asset_Type at');
		$this -> db -> join('rsys_asset_endpoint ep','at.Asset_Type_Id= ep.Asset_Type_Id', 'LEFT OUTER');
		$this -> db -> where('Asset_Type_Name', $meta);
		$this -> db -> where('Endpoint_Type', $Endpoint_Type);
		$this -> db -> limit(1);
	    $query = $this -> db -> get();
	    $resultend = $query->result()[0];
		$url = $org->base_url.$resultend->Endpoint_URL .'?depth='.$resultend->Depth .'&count='.$resultend->Page_Size ."&page=".$page;
		if($Endpoint_Type == "search")
		{
			$url .= "search=*". $searchtext + "*";
		}
		$result = $this->get_request($org->token, $url);
		return $result;
	}
}			
?>