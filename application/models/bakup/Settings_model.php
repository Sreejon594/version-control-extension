<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Settings_model extends CI_Model{
	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		
		  
    }
	
	function get_child_site_assets($child_site_Name)
	{	
		$this -> db -> select('*');
		$this -> db -> from('instance_');
		$this -> db -> where('Site_Name' ,$child_site_Name);
		$result = $this -> db -> get() -> result();
		// print_r($result);
		return $result;
	}
	
	function saveUsers($postedData)
	{
		if(isset($postedData['siteId']))
		{
			if(isset($postedData['addedUsers']))
			{
				$tdata['site_Id'] = $postedData['siteId'];
				$this->db->delete('user_permissions',$tdata);
				foreach($postedData['addedUsers'] as $key=>$val)
				{
					$columns = Array();
					$columns = explode('|',$val);
					
					$ndata['Site_Id'] = $postedData['siteId'];
					$ndata['User_Id'] = $columns[1];
					$ndata['full_name'] = $columns[0];
					$this->db->insert('user_permissions',$ndata);
				}				
			}
			else{
				$tdata['site_Id'] = $postedData['siteId'];
				$this->db->delete('user_permissions',$tdata);
			}
		}
	}
	
	function get_externalInstances($parentSiteId)
	{	
		// print_r($parentSiteId);exit;
		$this -> db -> select('*');
		$this -> db -> from('instance_mapping');
		$this -> db -> where('parent_siteId' ,$parentSiteId);
		$result = $this -> db -> get() -> result();
		// print_r($result);exit;
		return $result;
	}
	
	function get_User_Permissions($siteId)
	{
		$this -> db -> select('*');
		$this -> db -> from('user_permissions');
		// $this -> db -> where('Site_Id !=', $id);
		$this -> db -> where('Site_Id !=' , $siteId);
		$result = $this -> db -> get() -> result();
		return $result;
	}
	
	//
	function userPermission($userId,$siteId)
	{
		$this -> db -> select('*');
		$this -> db -> from('user_permissions');
		$this -> db -> where('Site_Id' , $siteId);
		$this -> db -> where('User_Id' , $userId);
		$query = $this -> db -> get();
		// print_r($query->result());
		if($query->num_rows() >0)
		{
			return true;
		}	
		else
		{	
			return false;
		}	
	}
	
	//read the department list from db
    function get_OrgList($associatedSites,$siteId)
    {
		// print_r($id);exit;
		$this -> db -> select('*');
		$this -> db -> from('instance_');
		// $this -> db -> where('Site_Id !=', $id);
		$this -> db -> where_in('Site_Name' , $associatedSites);
		$this -> db -> where('Site_Id !=' , $siteId);
		$result = $this -> db -> get() -> result();
		return $result;
    }
	
	//This function is to map the installId vs site id. This will ensure us the validity of the user.
	//Along with the right data going to the right user.
	function checkInstallId($siteId,$installId)
	{
		$this -> db -> select('*');
		$this -> db -> from('instance_');
		// $this -> db -> where('Site_Id !=', $id);
		$this -> db -> where('Site_Id' , $siteId);
		$this -> db -> where('Install_Id',$installId);
		$query = $this -> db -> get();
		
		if($query->num_rows() > 0)
		{
			return 1;
		}
		return 0;
	}
	
	function get_OrgInfo($id, $orgid)
    {
		$this->db->select('*');
		$this -> db -> from('instance_');
		$this -> db -> join('deployment_package','deployment_package.Source_Site_Name = instance_.Site_Name','LEFT OUTER');
		$this -> db -> where('User_Id',$id);
		$this -> db -> where('instance_.Site_Name',$orgid);
		// $this -> db -> where('enable',1);
		$result = $this->db->get()->result();
		return $result;
    }
	
	function new_Org($id ,$data)
    {
		$this->db->select('id');
		$this -> db -> from('instance_');
		$this -> db -> where('OrgName',$data['OrgName']);
		$temp =$this->db->count_all_results();
		if($temp==0){									
			$this->db->select_max('id');
			$this -> db -> from('instance_');
			$temprow = $this->db->get()->result();
			$data['id']=$temprow[0]->id+1;	
			$data['enable']=1;	
			$data['users_id']= $id;	
			$this->db->insert('instance_', $data); 		
			$result['id'] = $data['id'];
			$result['success'] = true;		
			return $result;
		}else{
			$result['success'] = false;		
			$result['msg'] = 'Org Name Can\'t be Duplicate.';		
			return $result;
		}
	}
	
	function delete_Org($id , $data)
    {
		$this -> db -> where_in('Instance__Id', $data['id']);
		$this -> db -> delete('instance_');
		
		$this -> db -> where_in('child_siteId', $data['site_id']);
		$this -> db -> where('child_siteName', $data['site_name']);
		$this -> db -> delete('instance_mapping');
		
		$this -> db -> where_in('Site_Id', $data['site_id']);
		$this -> db -> delete('user_info');
	}
	
	function saveToken( $Token)
    {		
		$returntoken = $this->requestToken('',$Token['code']);
		// echo '<pre>';
		// print_r($returntoken);
		// echo '</pre>';
		if(isset($returntoken['access_token']))
		{	
			$data['Token'] = $returntoken['access_token'];
			// $data['token_type'] = $returntoken['token_type'];
			// $data['refresh_token'] = $returntoken['refresh_token'];
			// if(time()+1800 > $instance[0]->Token_Expiry_Time)
				// {
				   // $result2 = $this->refreshToken($instance[0]);
				   // $instance[0]->Token = $result2['Token']['access_token'];
				// }
			$eloquadata1 = $this->get_request($returntoken['access_token'] , "https://login.eloqua.com/id");
			// if( $eloquadata1['http_code'] == 401)
			  // {
				  // $result = $this->requestToken_new($result_db[0]->Refresh_Token,'refresh_token');
				  
				  // $this->updateTokens($siteId,$result['access_token'],$result['refresh_token']);
				  // $result = $this->get_request($result['access_token'],$url);
				 
			  // }
			$eloquadata = json_decode($eloquadata1['data']);
		
		// echo '<pre>';
		// print_r($eloquadata);
		// echo '</pre>';
			
			$data['Base_Url'] = $eloquadata->urls->base .'/';
			$data['First_Name'] = $eloquadata->user->firstName;
			$data['Last_Name'] = $eloquadata->user->lastName;
			$data['Email_Address'] = $eloquadata->user->emailAddress;
			$data['Token_Expiry_Time'] = now() + $returntoken['expires_in'];	
			$this->db->where('Site_Name', $Token['state']);
			$this->db->update('instance_' ,$data);
			$result['success'] = true;		
			$result['token'] = $returntoken;
			
			// updating parent database	
			// $pidata = array();				
			// $pidata['Product_Install_Id'] = $data['installId'];					
			// $pidata['Site_Name'] = $data['siteName'];
			// $pidata['Install_Username'] = $data['userName'];
			// $pidata['Install_UserId'] = $data['UserId'];
			// $pidata['Product_Name'] = "Oracle Eloqua Transporter";
			// $pidata['Status'] =  1;
			// $pidata['Valid_From'] = date("Y/m/d");
			// $pidata['Valid_Till'] = date("Y-m-d",strtotime("+30 day"));			
			// $this->legacy_db->insert('product_install', $pidata);
		}
		else{
			$result['success'] = false;		
			$result['token'] = $returntoken;
		}
				
		//$result['msg'] = 'Account is Updated..';		
		return $result;		
	}
	
	public function get_all_users($siteId)
	{
	  // print_r($id);exit;
	  $this -> db -> select('*');
	  $this -> db -> from('instance_');
	  // $this -> db -> where('Site_Id !=', $id);
	  $this -> db -> where('Site_Id' , $siteId);
	  $query = $this -> db -> get();
	  $result_db = $query -> result();
	  if($query->num_rows()>0)
	  {
		  $url = $result_db[0]->Base_Url.'/API/REST/2.0/system/users';
		  if(time()+1800 > $result_db[0]->Token_Expiry_Time)
			{
			   $result2 = $this->eloqua->refreshToken($result_db[0]);
			   $result_db[0]->Token = $result2['Token']['access_token'];
			}
		  $result = $this->get_request($result_db[0]->Token,$url);
		  // if( $result['http_code'] == 401)
		  // {
			  // $result = $this->requestToken_new($result_db[0]->Refresh_Token,'refresh_token');
			  
			  // $this->updateTokens($siteId,$result['access_token'],$result['refresh_token']);
			  // $result = $this->get_request($result['access_token'],$url);
			 
		  // }
		  $result = json_decode($result['data'],true);
		  $result = $result['elements'];
	  }
	  return $result;
	}
	public function updateTokens($siteId,$access_token,$refresh_token)
	{
		$this->db->where('Site_Id',$siteId);
		$udata['Token'] = $access_token;
		$udata['Refresh_Token'] = $refresh_token;
		$this->db->update('instance_',$udata);
		
	}
	public function get_User_Permissions_for_this_site($site_Id)
	{
		$this->db->select('*');
		$this -> db -> from('user_permissions');
		$this -> db -> where('Site_Id' , $site_Id);
		$query = $this -> db -> get();
		$result = $query -> result();
		return $result;
	}
	
		
	private function get_request($access_token, $url) 
	{ 
		// create the cURL resource 
		$ch = curl_init(); 
		$Authorization = 'Authorization: Bearer' .$access_token;
		$headers = array('Authorization: Bearer '.$access_token);
		// set cURL options 		
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HTTPGET, true);
		//curl_setopt($ch, CURLOPT_HEADER, false);		
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);			
			
		// execute request and retrieve the response 
		$data = curl_exec($ch); 		
		$responseInfo = curl_getinfo($ch);
		$info = curl_getinfo($ch);
		
		// print_r($data);  // close resources and return the response 
		curl_close($ch);
		$result['http_code'] = $info['http_code'];
		$result['data'] = $data;
		return $result; 
	}
				
				
				
	function requestToken($url, $code){
		$curl_post_data = array(
            "grant_type" => 'authorization_code',
            "code" => $code,
            "redirect_uri" => $this->config->item('redirect_uri'),
            );
			$curl_post_data = json_encode($curl_post_data );
		$Authorization = $this->config->item('App_Id').':'.$this->config->item('Client_Secret');
		$url = 'https://login.eloqua.com/auth/oauth2/token/';
		$ch=curl_init();
		curl_setopt($ch, CURLOPT_URL, $url); 
		curl_setopt($ch, CURLOPT_USERPWD, $Authorization);  
		curl_setopt($ch, CURLOPT_POST, true); 
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $curl_post_data);
		curl_setopt($ch, CURLOPT_HEADER, false);
		
		$headers = array('Content-type: application/json');
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		$result = curl_exec($ch);
		$result1 = json_decode($result,true); 
		$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);
		//print_r($result1);
		return($result1);
	}
	
	function update_Org($id , $data)
    {
		$this->db->select('id');
		$this -> db -> from('instance_');
		$this -> db -> where('userid',$data['userid']);
		$this -> db -> where_not_in('id',$data['id']);
		$temp =$this->db->count_all_results();
		if($temp==0){								
			$this->db->where('id', $data['id']);
			$this->db->update('instance_' ,$data);
			$result['success'] = true;		
			$result['msg'] = 'Account is Updated..';		
			return $result;
		}else{
			$result['success'] = false;		
			$result['msg'] = 'Login Name Can\'t be Duplicate.';		
			return $result;
		}
	}
	
	function get_Preference($id)
    {
		$this->db->select('*');
		$this -> db -> from('Asset_Type_Preference');
		$this -> db -> where('Company_Id',$id);
		//$this -> db -> where('enable',1);
		$result = $this->db->get()->result();
		return $result;
    }
	function update_Preference($Company_Id , $data)
    {
		$this->db->where('Company_Id', $Company_Id);
		$this->db->update('Asset_Type_Preference' ,array('Active_'=>0));

		foreach($data as $key =>$val)
		{
			$this->db->select('*');
			$this->db->from('Asset_Type_Preference');
			$this->db->where('Company_Id',$Company_Id);
			$this->db->where_in('Asset_Type',$val);
			$query = $this -> db -> get();
			if($query->num_rows() > 0)
			{
				$this->db->where('Company_Id', $Company_Id);
				$this->db->where('Asset_Type', $val);
				$this->db->update('Asset_Type_Preference' ,array('Active_'=>1));
			}
			else{
				$tdata['Company_Id'] = $Company_Id;
				// $tdata['Org_Id'] = $data['Contact'];
				$tdata['Asset_Type'] = $val;
				$tdata['Active_'] = 1;
				$this->db->insert('Asset_Type_Preference', $tdata);				
			}			
		}
		  	
		$result['success'] = true;		
		$result['msg'] = 'Account is Updated..';		
		return $result;		
	}
	
	function requestToken_new($code,$grant_type)
	{		
		// echo'<br/>';print_r($code);exit;
		$curl_post_data["grant_type"]= $grant_type;
		if($grant_type == "refresh_token")
		{
			$curl_post_data["refresh_token"]= $code;
			$curl_post_data["scope"]= "full";
			
		}
		else
		{
			$curl_post_data["code"]= $code;
			
		}		
		$curl_post_data["redirect_uri"]= $this->config->item('redirect_uri');			
		$curl_post_data = json_encode($curl_post_data );
		$Authorization = $this->config->item('App_Id').':'.$this->config->item('Client_Secret');
		$url = $this->config->item('TokenUrl');
		$ch=curl_init();
		curl_setopt($ch, CURLOPT_URL, $url); 
		curl_setopt($ch, CURLOPT_USERPWD, $Authorization);  
		curl_setopt($ch, CURLOPT_POST, true); 
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $curl_post_data);
		curl_setopt($ch, CURLOPT_HEADER, false);		
		$headers = array('Content-type: application/json');
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		$result = curl_exec($ch);
		$result1 = json_decode($result,true); 
		$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		// print_r($result1);exit;
		$responseInfo = curl_getinfo($ch);
		curl_close($ch);
		return($result1);
	}
	
}			
?>