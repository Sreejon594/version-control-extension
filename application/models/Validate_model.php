<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Validate_model extends CI_Model{
	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->load->model('eloqua','',TRUE);		  
    }
	//read the department list from db
   
	
	function validate_inqueue($package_id)
    {
		$status = array('In Progress','In Queue');
		$this->db->select('*');
		$this -> db -> from('validate_queue');
		$this -> db -> where('Deployment_Package_Id',$package_id);
		$this -> db -> where_in('Status',$status);
		$this->db->limit(1);
		$query = $this -> db -> get();
	    $deployment_queue = $query->result();
		
		$date = date('Y-m-d H:i:s');
		$data['Deployment_Package_Id'] = $package_id;
		$data['Status'] = 'In Queue';
		$data['Rn_Create_Date'] = $date; 
		$msg="";
		try {
			if(sizeof($deployment_queue)==0){
				$this->db->insert('validate_queue',$data);
				$msg = "success";
			}else{
				$msg = "alreadyinqueue";
			}
			
			
		} catch (Exception $e) {
			$msg = "Caught exception:".$e->getMessage();
		}		
		return $msg;
		
	}
	
	function validate_queue()
    {
		
		$date = date('Y-m-d H:i:s');		
		$this->db->select('*');
		$this -> db -> from('validate_queue');
		$this -> db -> where('Status','In Queue');
		$this->db->order_by("Rn_Create_Date", "asc");
		$this->db->limit(1);
		$query = $this -> db -> get();
	    $deployment_queue = $query->result();
		$msg="";
		foreach($deployment_queue as $key=>$val){
			$data1['Status'] = 'In Progress';
			$data1['Start_'] = date('Y-m-d H:i:s');
			try {
				$this->db->where('Validate_Queue_Id',$val->Validate_Queue_Id);
				$this->db->update('validate_queue',$data1);
				$this->validate_package($val->Deployment_Package_Id);
				//$this->deploy_package_item($val->Deployment_Package_Id,$val->Deployment_Queue_Id);
				$msg = "success";
			} catch (Exception $e) {
				$msg = "Caught exception:".$e->getMessage();
			}		
		}
		
		
		return $msg;
		
	}
	
	function validate_package($packageId)
    {
		//$resultout=$this->eloqua->validatepackage('',$Package_Id);
		if($resultout){
			$date = date('Y-m-d H:i:s');		
			$this->db->select('*');
			$this -> db -> from('deployment_queue');
			$this -> db -> where('Deployment_Package_Id',$Package_Id);
			$this -> db -> where('Status','In Validate Queue');
			//$this->db->order_by("Rn_Create_Date", "asc");
			//$this->db->limit(1);
			$query = $this -> db -> get();
			$deployment_queue = $query->result();
			$msg="";
			foreach($deployment_queue as $key=>$val){
				$data1['Status'] = 'In Queue';
				$data1['Start_'] = date('Y-m-d H:i:s');
				$this->db->where('Deployment_Queue_Id',$val->Deployment_Queue_Id);
				$this->db->update('deployment_queue',$data1);
			}
			
		}		
		
	}
	
	
	
	
}
?>