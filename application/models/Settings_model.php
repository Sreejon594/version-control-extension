<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Settings_model extends CI_Model{
	function __construct()
    {
        // Call the Model constructor
        parent::__construct();		  
    }
	
	function get_child_site_assets($child_site_Name)
	{	
		$this -> db -> select('*');
		$this -> db -> from('instance_');
		$this -> db -> where('Site_Name' ,$child_site_Name);
		$result = $this -> db -> get() -> result();
		// print_r($result);
		return $result;
	}
	
	function saveUsers($postedData)
	{
		if(isset($postedData['siteId']))
		{
			if(isset($postedData['addedUsers']))
			{
				$tdata['site_Id'] = $postedData['siteId'];
				$this->db->delete('user_permissions',$tdata);
				foreach($postedData['addedUsers'] as $key=>$val)
				{
					$columns = Array();
					$columns = explode('|',$val);
					
					$ndata['Site_Id'] = $postedData['siteId'];
					$ndata['User_Id'] = $columns[1];
					$ndata['full_name'] = $columns[0];
					$this->db->insert('user_permissions',$ndata);
				}				
			}
			else{
				$tdata['site_Id'] = $postedData['siteId'];
				$this->db->delete('user_permissions',$tdata);
			}
		}
	}
	
	function get_externalInstances($parentSiteId)
	{	
		// print_r($parentSiteId);exit;
		$this -> db -> select('*');
		$this -> db -> from('instance_mapping');
		$this -> db -> where('parent_siteId' ,$parentSiteId);
		$result = $this -> db -> get() -> result();
		// print_r($result);exit;
		return $result;
	}
	
	function get_User_Permissions($siteId)
	{
		$this -> db -> select('*');
		$this -> db -> from('user_permissions');
		// $this -> db -> where('Site_Id !=', $id);
		$this -> db -> where('Site_Id !=' , $siteId);
		$result = $this -> db -> get() -> result();
		return $result;
	}
	
	//
	function userPermission($userId,$siteId)
	{
		$this -> db -> select('*');
		$this -> db -> from('user_permissions');
		$this -> db -> where('Site_Id' , $siteId);
		$this -> db -> where('User_Id' , $userId);
		$query = $this -> db -> get();
		// print_r($query->result());
		if($query->num_rows() >0)
		{
			return true;
		}	
		else
		{	
			return false;
		}	
	}
	
	//read the department list from db
    function get_OrgList($associatedSites,$siteId)
    {		
		$this -> db -> select('*');
		$this -> db -> from('instance_');	
		$this -> db -> where_in('Site_Name' , $associatedSites);
		$this -> db -> where('Site_Id !=' , $siteId);
		$result = $this -> db -> get() -> result();
		return $result;
    }
	
	//This function is to map the installId vs site id. This will ensure us the validity of the user.
	//Along with the right data going to the right user.
	function checkInstallId($siteId,$installId)
	{
		$this -> db -> select('*');
		$this -> db -> from('instance_');
		// $this -> db -> where('Site_Id !=', $id);
		$this -> db -> where('Site_Id' , $siteId);
		$this -> db -> where('Install_Id',$installId);
		$query = $this -> db -> get();
		
		if($query->num_rows() > 0)
		{
			return 1;
		}
		return 0;
	}
	
	function get_OrgInfo($id, $orgid)
    {
		$this->db->select('*');
		$this -> db -> from('instance_');
		$this -> db -> join('deployment_package','deployment_package.Source_Site_Name = instance_.Site_Name','LEFT OUTER');
		$this -> db -> where('User_Id',$id);
		$this -> db -> where('instance_.Site_Name',$orgid);
		// $this -> db -> where('enable',1);
		$result = $this->db->get()->result();
		return $result;
    }
	
	function new_Org($id ,$data)
    {
		$this->db->select('id');
		$this -> db -> from('instance_');
		$this -> db -> where('OrgName',$data['OrgName']);
		$temp =$this->db->count_all_results();
		if($temp==0){									
			$this->db->select_max('id');
			$this -> db -> from('instance_');
			$temprow = $this->db->get()->result();
			$data['id']=$temprow[0]->id+1;	
			$data['enable']=1;	
			$data['users_id']= $id;	
			$this->db->insert('instance_', $data); 		
			$result['id'] = $data['id'];
			$result['success'] = true;		
			return $result;
		}else{
			$result['success'] = false;		
			$result['msg'] = 'Org Name Can\'t be Duplicate.';		
			return $result;
		}
	}
	
	function delete_Org($id , $data)
    {
		$this -> db -> where_in('Instance__Id', $data['id']);
		$this -> db -> delete('instance_');
		
		$this -> db -> where_in('child_siteId', $data['site_id']);
		$this -> db -> where('child_siteName', $data['site_name']);
		$this -> db -> delete('instance_mapping');
		
		$this -> db -> where_in('Site_Id', $data['site_id']);
		$this -> db -> delete('user_info');
	}
	
	function saveToken( $Token)
    {		
		$returntoken = $this->requestToken('',$Token['code']);
		// echo '<pre>';
		// print_r($returntoken);
		// echo '</pre>';
		if(isset($returntoken['access_token']))
		{	
			$data['Token'] = $returntoken['access_token'];
			// $data['token_type'] = $returntoken['token_type'];
			// $data['refresh_token'] = $returntoken['refresh_token'];
			// if(time()+1800 > $instance[0]->Token_Expiry_Time)
				// {
				   // $result2 = $this->refreshToken($instance[0]);
				   // $instance[0]->Token = $result2['Token']['access_token'];
				// }
			$eloquadata1 = $this->get_request($returntoken['access_token'] , "https://login.eloqua.com/id");
			// if( $eloquadata1['http_code'] == 401)
			  // {
				  // $result = $this->requestToken_new($result_db[0]->Refresh_Token,'refresh_token');
				  
				  // $this->updateTokens($siteId,$result['access_token'],$result['refresh_token']);
				  // $result = $this->get_request($result['access_token'],$url);
				 
			  // }
			$eloquadata = json_decode($eloquadata1['data']);
		
		// echo '<pre>';
		// print_r($eloquadata);
		// echo '</pre>';
			
			$data['Base_Url'] = $eloquadata->urls->base .'/';
			$data['First_Name'] = $eloquadata->user->firstName;
			$data['Last_Name'] = $eloquadata->user->lastName;
			$data['Email_Address'] = $eloquadata->user->emailAddress;
			$data['Token_Expiry_Time'] = now() + $returntoken['expires_in'];	
			$this->db->where('Site_Name', $Token['state']);
			$this->db->update('instance_' ,$data);
			$result['success'] = true;		
			$result['token'] = $returntoken;
			
			// updating parent database	
			// $pidata = array();				
			// $pidata['Product_Install_Id'] = $data['installId'];					
			// $pidata['Site_Name'] = $data['siteName'];
			// $pidata['Install_Username'] = $data['userName'];
			// $pidata['Install_UserId'] = $data['UserId'];
			// $pidata['Product_Name'] = "Oracle Eloqua Transporter";
			// $pidata['Status'] =  1;
			// $pidata['Valid_From'] = date("Y/m/d");
			// $pidata['Valid_Till'] = date("Y-m-d",strtotime("+30 day"));			
			// $this->legacy_db->insert('product_install', $pidata);
		}
		else{
			$result['success'] = false;		
			$result['token'] = $returntoken;
		}
				
		//$result['msg'] = 'Account is Updated..';		
		return $result;		
	}
	
	public function get_all_users($siteId)
	{
	  // print_r($id);exit;
	  $this -> db -> select('*');
	  $this -> db -> from('instance_');
	  // $this -> db -> where('Site_Id !=', $id);
	  $this -> db -> where('Site_Id' , $siteId);
	  $query = $this -> db -> get();
	  $result_db = $query -> result();
	  if($query->num_rows()>0)
	  {
		  $url = $result_db[0]->Base_Url.'/API/REST/2.0/system/users';
		  if(time()+1800 > $result_db[0]->Token_Expiry_Time)
			{
			   $result2 = $this->eloqua->refreshToken($result_db[0]);
			   $result_db[0]->Token = $result2['Token']['access_token'];
			}
		  $result = $this->get_request($result_db[0]->Token,$url);
		  // if( $result['http_code'] == 401)
		  // {
			  // $result = $this->requestToken_new($result_db[0]->Refresh_Token,'refresh_token');
			  
			  // $this->updateTokens($siteId,$result['access_token'],$result['refresh_token']);
			  // $result = $this->get_request($result['access_token'],$url);
			 
		  // }
		  $result = json_decode($result['data'],true);
		  $result = $result['elements'];
	  }
	  return $result;
	}
	public function updateTokens($siteId,$access_token,$refresh_token)
	{
		$this->db->where('Site_Id',$siteId);
		$udata['Token'] = $access_token;
		$udata['Refresh_Token'] = $refresh_token;
		$this->db->update('instance_',$udata);
		
	}
	public function get_User_Permissions_for_this_site($site_Id)
	{
		$this->db->select('*');
		$this -> db -> from('user_permissions');
		$this -> db -> where('Site_Id' , $site_Id);
		$query = $this -> db -> get();
		$result = $query -> result();
		return $result;
	}
	
		
	private function get_request($access_token, $url) 
	{ 
		// create the cURL resource 
		$ch = curl_init(); 
		$Authorization = 'Authorization: Bearer' .$access_token;
		$headers = array('Authorization: Bearer '.$access_token);
		// set cURL options 		
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HTTPGET, true);
		//curl_setopt($ch, CURLOPT_HEADER, false);		
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);			
			
		// execute request and retrieve the response 
		$data = curl_exec($ch); 		
		$responseInfo = curl_getinfo($ch);
		$info = curl_getinfo($ch);
		
		// print_r($data);  // close resources and return the response 
		curl_close($ch);
		$result['http_code'] = $info['http_code'];
		$result['data'] = $data;
		return $result; 
	}
				
				
				
	function requestToken($url, $code){
		$curl_post_data = array(
            "grant_type" => 'authorization_code',
            "code" => $code,
            "redirect_uri" => $this->config->item('redirect_uri'),
            );
			$curl_post_data = json_encode($curl_post_data );
		$Authorization = $this->config->item('App_Id').':'.$this->config->item('Client_Secret');
		$url = 'https://login.eloqua.com/auth/oauth2/token/';
		$ch=curl_init();
		curl_setopt($ch, CURLOPT_URL, $url); 
		curl_setopt($ch, CURLOPT_USERPWD, $Authorization);  
		curl_setopt($ch, CURLOPT_POST, true); 
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $curl_post_data);
		curl_setopt($ch, CURLOPT_HEADER, false);
		
		$headers = array('Content-type: application/json');
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		$result = curl_exec($ch);
		$result1 = json_decode($result,true); 
		$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);
		//print_r($result1);
		return($result1);
	}
	
	function update_Org($id , $data)
    {
		$this->db->select('id');
		$this -> db -> from('instance_');
		$this -> db -> where('userid',$data['userid']);
		$this -> db -> where_not_in('id',$data['id']);
		$temp =$this->db->count_all_results();
		if($temp==0){								
			$this->db->where('id', $data['id']);
			$this->db->update('instance_' ,$data);
			$result['success'] = true;		
			$result['msg'] = 'Account is Updated..';		
			return $result;
		}else{
			$result['success'] = false;		
			$result['msg'] = 'Login Name Can\'t be Duplicate.';		
			return $result;
		}
	}
	
	function get_Preference($id)
    {
		$this->db->select('*');
		$this -> db -> from('Asset_Type_Preference');
		$this -> db -> where('Company_Id',$id);
		//$this -> db -> where('enable',1);
		$result = $this->db->get()->result();
		return $result;
    }
	function update_Preference($Company_Id , $data)
    {
		$this->db->where('Company_Id', $Company_Id);
		$this->db->update('Asset_Type_Preference' ,array('Active_'=>0));

		foreach($data as $key =>$val)
		{
			$this->db->select('*');
			$this->db->from('Asset_Type_Preference');
			$this->db->where('Company_Id',$Company_Id);
			$this->db->where_in('Asset_Type',$val);
			$query = $this -> db -> get();
			if($query->num_rows() > 0)
			{
				$this->db->where('Company_Id', $Company_Id);
				$this->db->where('Asset_Type', $val);
				$this->db->update('Asset_Type_Preference' ,array('Active_'=>1));
			}
			else{
				$tdata['Company_Id'] = $Company_Id;
				// $tdata['Org_Id'] = $data['Contact'];
				$tdata['Asset_Type'] = $val;
				$tdata['Active_'] = 1;
				$this->db->insert('Asset_Type_Preference', $tdata);				
			}			
		}
		  	
		$result['success'] = true;		
		$result['msg'] = 'Account is Updated..';		
		return $result;		
	}
	
	function requestToken_new($code,$grant_type)
	{		
		// echo'<br/>';print_r($code);exit;
		$curl_post_data["grant_type"]= $grant_type;
		if($grant_type == "refresh_token")
		{
			$curl_post_data["refresh_token"]= $code;
			$curl_post_data["scope"]= "full";			
		}
		else
		{
			$curl_post_data["code"]= $code;			
		}		
		$curl_post_data["redirect_uri"]= $this->config->item('redirect_uri');			
		$curl_post_data = json_encode($curl_post_data );
		$Authorization = $this->config->item('App_Id').':'.$this->config->item('Client_Secret');
		$url = $this->config->item('TokenUrl');
		$ch=curl_init();
		curl_setopt($ch, CURLOPT_URL, $url); 
		curl_setopt($ch, CURLOPT_USERPWD, $Authorization);  
		curl_setopt($ch, CURLOPT_POST, true); 
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $curl_post_data);
		curl_setopt($ch, CURLOPT_HEADER, false);		
		$headers = array('Content-type: application/json');
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		$result = curl_exec($ch);
		$result1 = json_decode($result,true); 
		$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		// print_r($result1);exit;
		$responseInfo = curl_getinfo($ch);
		curl_close($ch);
		return($result1);
	}
	
	//This function will get mappings
	function get_Mapping($associatedSites)
	{
		$this->db->select('source_target_mapping_id,Source_Site_Name,Source_Asset_Id,Child_Asset_Type,Source_Asset_Name,Target_Site_Name,Target_Asset_Id,Target_Asset_Name');
		$this -> db -> from('source_target_mapping cs');
		$this->db->where_in('cs.Source_Site_Name', $associatedSites);
		$this->db->where_in('cs.Target_Site_Name', $associatedSites);
		$result = $this->db->get()->result();
		return $result;
	}
	
	function getAssetListByAssetTypeAndInstanceName($assetType,$instanceName,$instanceType)
	{
		$instance = $this->deploy_model->getInstance($instanceName);
		//print_r($instance[0]);	
		$token 	   = $instance [0] -> Token;
		$Base_Url  = $instance [0]-> Base_Url;	
		$tokenExpiryTme = $instance [0]-> Token_Expiry_Time;
		
		$this -> db -> select('*');
		$this -> db -> from('rsys_asset_endpoint rae');
		$this -> db -> where('rae.Asset_Type', $assetType);
		$this -> db -> where('rae.Endpoint_Type', 'Read List');
		$query = $this -> db -> get();
	    $endpoint = $query->result();
		
		if($assetType=='Picklist')
		{
			$url = $Base_Url .$endpoint[0]->Endpoint_URL .'?search="name!=QuickList*"&orderby=name';	
		}
		else if($assetType=='Conatct Field' || $assetType=='Campaign Field')
		{			
			$url = $Base_Url .$endpoint[0]->Endpoint_URL .'?orderby=name&depth=complete';		
		}		
		else{
			$url = $Base_Url .$endpoint[0]->Endpoint_URL .'?orderby=name';
		}
		if(time()+1800 > $tokenExpiryTme)
		{
		   $result2 = $this->eloqua->refreshToken($instance[0]);
		   $token = $result2['Token']['access_token'];
		}
		$result_asset = $this->eloqua->get_request($token, $url);		
		$result_asset_=json_decode($result_asset['data'])->elements;
		
		$modifiedResult = array();
		
		if(isset($result_asset_))
		{
			foreach($result_asset_ as $key => $val)
			{
				$resultTemp['id'] = $val->id;
				$resultTemp['name'] = $val->name;
				if(isset($val->dataType))
				{
					$resultTemp['dataType'] = $val->dataType;
				}
				array_push($modifiedResult,$resultTemp);
				// if(isset($val->dataType))
				// {
					// $resultTemp['dataType'] = $val->dataType;
				// }		
				// if($assetType=="Contact Field" && $instanceType=="source")
				// {					
					// if($val->isStandard=='true' || $val->createdAt<0)
					// {
						// continue;
					// }
					// else
					// {						
						// array_push($modifiedResult,$resultTemp);
					// }
				// }
				// else
				// {					
					// array_push($modifiedResult,$resultTemp);
				// }
				
			}			
		}
		return $modifiedResult;
	}
	
	function saveAssetMapping($assetMappingData)
	{
		$assetMappingData['sourceSiteId'] = $this->getSiteIdBySiteName($assetMappingData['sourceSiteName']);
		$assetMappingData['targetSiteId'] = $this->getSiteIdBySiteName($assetMappingData['targetSiteName']);
		
		$this->db->select('Source_Asset_Id,Target_Asset_Id');
		$this -> db -> from('source_target_mapping cs');
		$this->db->where('cs.Source_Site_Name', $assetMappingData['sourceSiteName']);
		$this->db->where('cs.Target_Site_Name', $assetMappingData['targetSiteName']);
		$this->db->where('cs.Child_Asset_Type', $assetMappingData['assetType']);
		$this->db->where('cs.Source_Asset_Id', $assetMappingData['sourceAssetId']);
		$this->db->where('cs.Target_Asset_Id', $assetMappingData['targetAssetId']);
		// $this->db->or_where('cs.Target_Asset_Id !=', $assetMappingData['targetAssetId']);
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			$returnResult = "Asset mapping exists.";
		}
		else
		{
			$this->db->select('Source_Asset_Id,Target_Asset_Id');
			$this -> db -> from('source_target_mapping cs');
			$this->db->where('cs.Source_Site_Name', $assetMappingData['sourceSiteName']);
			$this->db->where('cs.Target_Site_Name', $assetMappingData['targetSiteName']);
			$this->db->where('cs.Child_Asset_Type', $assetMappingData['assetType']);
			$this->db->where('cs.Source_Asset_Id', $assetMappingData['sourceAssetId']);
			$this->db->where('cs.Target_Asset_Id !=', $assetMappingData['targetAssetId']);
			$query1 = $this->db->get();
			if($query1->num_rows() > 0)
			{
				$returnResult = "Source asset is already mapped.";
			}
			else
			{
				//print_r($assetMappingData);
				$this->db->select('Source_Asset_Id,Target_Asset_Id');
				$this -> db -> from('source_target_mapping cs');
				$this->db->where('cs.Source_Site_Name', $assetMappingData['sourceSiteName']);
				$this->db->where('cs.Target_Site_Name', $assetMappingData['targetSiteName']);
				$this->db->where('cs.Child_Asset_Type', $assetMappingData['assetType']);
				$this->db->where('cs.Target_Asset_Name', $assetMappingData['sourceAssetName']);
				//$this->db->where('cs.Target_Asset_Id !=', $assetMappingData['targetAssetId']);
				$query2 = $this->db->get();
				if($query2->num_rows() > 0)
				{
					$returnResult = "Invalid.";
				}
				else
				{
					$tdata['Source_Site_Id'] = $assetMappingData['sourceSiteId'];
					$tdata['Target_Site_Id'] = $assetMappingData['targetSiteId'];
					
					$tdata['Child_Asset_Type'] = $assetMappingData['assetType'];
					
					$tdata['Source_Site_Name'] = $assetMappingData['sourceSiteName'];
					$tdata['Target_Site_Name'] = $assetMappingData['targetSiteName'];
					
					$tdata['Source_Asset_Name'] = $assetMappingData['sourceAssetName'];
					$tdata['Target_Asset_Name'] = $assetMappingData['targetAssetName'];
					
					$tdata['Source_Asset_Id'] = $assetMappingData['sourceAssetId'];
					$tdata['Target_Asset_Id'] = $assetMappingData['targetAssetId'];
					
					$this->db->insert('source_target_mapping', $tdata);	
					$returnResult = "Asset mapped successfully.";
				}	
			}	
		}
		return 	$returnResult;
	}
	
	function deleteMapping($assetMappingData)
	{		
		$this -> db -> where('source_target_mapping_id', $assetMappingData['source_target_mapping_id']);		
		
		if ($this->db->delete('source_target_mapping')) 
		{
			$returnResult= 'Asset mapping deleted successfully.';
		}
		return 	$returnResult;
	}
	
	function getSiteIdBySiteName($siteName)
	{	
		$this -> db -> select('Site_Id');
		$this -> db -> from('instance_');
		$this -> db -> where('Site_Name' ,$siteName);
		$result = $this -> db -> get() -> result();
		return $result[0]->Site_Id;
	}

	function getUserOnSearch($searchText,$sourceSiteName)
	{
		$instance = $this->deploy_model->getInstance($sourceSiteName);	
		$token 	   = $instance [0] -> Token;
		$Base_Url  = $instance [0]-> Base_Url;	
		$tokenExpiryTme = $instance [0]-> Token_Expiry_Time;
		
		$page = 1;
		$notOver = true;
		while($notOver)
		{			
			$getUsersUrl = $Base_Url."/API/REST/2.0/system/users?search=*".urlencode($searchText)."*&depth=complete&orderby=name&page=".$page;			
			
			if(time()+1800 > $tokenExpiryTme)
			{
			   $result2 = $this->eloqua->refreshToken($instance[0]);
			   $token = $result2['Token']['access_token'];
			}
			
			$result_asset = $this->eloqua->get_request($token, $getUsersUrl);		
			$getUsers_temp=json_decode($result_asset['data'])->elements;
			$temp_array = $getUsers_temp;			
			
			$notOver=true;
			if(isset($temp_array) && !empty($temp_array))
			{
				if(isset($merge_users_array) && !empty($merge_users_array) )
				{
					$merge_users_array=array_merge($merge_users_array,$temp_array);
				}
				else
				{
					$merge_users_array = $temp_array;
				}
				$page++;
			}
			else
			{
				$notOver=false;
			}		
		}
		$merge_users_array_final=null;
		if(isset($merge_users_array) && sizeof($merge_users_array)>0)
		{
			$merge_users_array_final= $this->getUserDetails($merge_users_array);
		}
		else
		{
			$merge_users_array_final=0;
		}		
		return $merge_users_array_final;
	}

	function getUserDetails($userSearchResult)
	{
		$userDetails=null;
		$userDetailsArray=array();
		$i=0;		
		foreach($userSearchResult as $uKey=>$uVal)
		{
			$userDetails['userId']=$uVal->id;
			$userDetails['UserName']=$uVal->name;
			$userDetails['UserEmail']=$uVal->emailAddress;
			$userDetailsArray[$i++]=$userDetails;
		}
		return 	$userDetailsArray;
	}

	function saveUserDetails($userDetails,$siteId)
	{		
		$this->db->select('User_Id,full_name,email_address');
		$this -> db -> from('user_permissions pc');
		$this->db->where('pc.User_Id', $userDetails['userId']);
		$this->db->where('pc.full_name', $userDetails['userName']);
		$this->db->where('pc.email_address', $userDetails['userEmail']);		
		$this->db->where('pc.Site_Id', $siteId);		
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			$returnResult = "User already exists.";
		}
		else
		{			
			$tdata['User_Id'] = $userDetails['userId'];
			$tdata['full_name'] = $userDetails['userName'];			
			$tdata['email_address'] = $userDetails['userEmail'];			
			$tdata['Site_Id'] = $siteId;		
			
			$this->db->insert('user_permissions', $tdata);	
			$returnResult = "User added successfully.";					
		}
		return 	$returnResult;
	}

	function deleteUser($postData)
	{		
		$this -> db -> where('user_permissions_id', $postData['user_permissions_id']);		
		if ($this->db->delete('user_permissions')) 
		{
			$returnResult= 'User removed successfully.';
		}
		return 	$returnResult;
	}	
	
	function getPackageListBySiteName($siteName)
	{
		$this->db->select('*');
		$this -> db -> from('deployment_package');
		$this->db->where('Source_Site_Name', $siteName);			
		$this->db->or_where('Target_Site_Name', $siteName);			
		$packageList = $this -> db -> get() -> result();
		return $packageList;
	}
	
	function saveImageBaseURL($siteId,$imgBaseURL)	{
		
		$data['Image_BaseURL']=$imgBaseURL;
		$affectedRow=-1;
		$this->db->select('Image_BaseURL');
		$this -> db -> from('instance_');
		$this->db->where('Image_BaseURL', $imgBaseURL);			
		$this->db->where('Site_Id', $siteId);			
		$result = $this -> db -> get();
		$returnResult=null;
		if($result->num_rows()>0){
			$this->db->where('Site_Id', $siteId);
			$this->db->update('instance_' ,$data);
			$affectedRow=$this->db->affected_rows();
		}
		else
		{			
			$this->db->where('Site_Id', $siteId);
			$this->db->update('instance_' ,$data);
			$affectedRow=$this->db->affected_rows();
		}
		if($affectedRow>0){
			$returnResult = $this->getImageURL($siteId);
		}
		return $returnResult;
	}
	
	function getImageURL($siteId)
	{
		$this->db->select('Image_BaseURL');
		$this -> db -> from('instance_');	
		$this->db->where('Site_Id', $siteId);			
		$result = $this -> db -> get();
		$returnResult=null;
		if($result->num_rows()>0)
		{
			$returnResult = $result->result()[0]->Image_BaseURL;
		}
		return $returnResult;
	}
	
	

	function readXlsxFile()	
    {
		$filePath = "https://appcloud-dev.portqii.com/transporter2.0/assets/excelFile/new.csv";
		if ($file = fopen($filePath, "r")) 
		{
	    $assetName = array();
        while(!feof($file)) 
		{
          $line = fgets($file);
		  $assetName[] =  $line ;
        }
		//print_r($assetName);
		$this->uploadeAssetToDB($assetName);
		fclose($file);
		}
					
	}
	
	function uploadeAssetToDB($assetNameList)
	{  
	     
		//print_r($assetNameList);
		
		$sourceSiteName="ThomsonReutersScience";
		$targetSiteName="ClarivateAnalytics";
		$sourceSiteId=1556;
		$targetSiteId=786780033;
		$assetType='Picklist';
		$instance_details = $this->deploy_model->getInstance($sourceSiteName);
		// echo '<br>';
		// echo 'instance_details';
	   // print_r($instance_details);
		$token     = $instance_details [0] -> Token;
		$Base_Url  = $instance_details [0]-> Base_Url; 
		$targetOrg = $instance_details [0];
		$tokenExpiryTime = $instance_details [0]-> Token_Expiry_Time;
	   
		//print_r($assetNameList);
		
	  foreach($assetNameList as $key=>$val)
	  {
		$url = '';
		
		if($assetType=='Picklist')
		{
			//$picklistName = $value;
			$url = $Base_Url ."/API/REST/2.0/assets/optionlists?search=name=".'"'.str_replace(" ","+",trim($val)) .'"' ;
			
		}
		// echo '<br>';
		// echo 'url';
		//print_r($url);
		if(time()+1800 > $tokenExpiryTime)
		{
			$result2 = $this->eloqua->refreshToken($instance_details[0]);
			$token = $result2['Token']['access_token'];
		} 
		// echo '<br>';
		// echo 'token';
		//print_r($token);
		$result_asset = $this->eloqua->get_request($token,$url);
		$result_asset_=json_decode($result_asset['data']);
		//print_r($result_asset);
		if($result_asset_->elements)
		{
        $assetId = $result_asset_->elements[0]->id ;
		if($assetId)
		{
			$this->db->select('Target_Asset_Id');
			$this -> db -> from('source_target_mapping');
			$this->db->where('Source_Site_Name', $sourceSiteName);
			$this->db->where('Target_Site_Name', $targetSiteName);
			$this->db->where('Child_Asset_Type', $assetType);
			$this->db->where('Source_Asset_Id', $assetId);
			$query2 = $this->db->get();
			//print_r($query2->num_rows());
	    if($query2->num_rows() == 0)
		 {			
			$tdata['Source_Site_Id'] = $sourceSiteId;
			$tdata['Target_Site_Id'] = $targetSiteId;
			
			$tdata['Child_Asset_Type'] = $assetType;
			
			$tdata['Source_Site_Name'] = $sourceSiteName;
			$tdata['Target_Site_Name'] = $targetSiteName;
			
			$tdata['Source_Asset_Name'] = trim($val);
			$tdata['Target_Asset_Name'] = "none";
			
			$tdata['Source_Asset_Id'] = $assetId;
			$tdata['Target_Asset_Id'] = -1;
			
			$this->db->insert('source_target_mapping', $tdata);
			$returnVal=$this->db->affected_rows();	
		//	print_r($returnVal);
		}
		else
		{
			continue;
		}
	    }
		}
	   else{
		   echo '<br>';
		   print_r($val);
			continue;
	    }
	  }
	 // return  $result_asset_;
	}
	
}			
?>