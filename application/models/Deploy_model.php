<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require(APPPATH.'/third_party/json_path/vendor/autoload.php');
use Flow\JSONPath\JSONPath;
use Flow\JSONPath\JSONPathLexer;
use \Peekmo\JsonPath\JsonPath as PeekmoJsonPath;
use \Peekmo\JsonPath\JsonStore;
class Deploy_model extends CI_Model
{
	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->load->model('eloqua','',TRUE);
		$this->load->model('deploy_helper_model','',TRUE);
		$this->load->model('logging_model','',TRUE);		
    }
	
	protected $globalStack = array();
	protected $childAssetCalled = array();
	protected $childAssetCalls = array();
	public $finalSubmitted =null;
	protected static $source_url;
	protected static $target_url;
	protected $visited = array();
	protected $recursive = array();
	protected $pattern = '/[\':;`!^�$%&*()}{@#~?><>,|=+�-]/';
	public $executionCounter=0;
	public $setSkeletonFlag=0;
	//protected $emailGroupRecursiveCalled = 0;   //in case of circular dependency involving emailgroup
	//read the department list from db
    function get_OrgList($id)
    {
		$this->db->select('id,users_id,base_url,userid,password');
		$this -> db -> from('Eloqua_Instance');
		$this -> db -> where('users_id',$id);
		$this -> db -> where('enable',1);
		$result = $this->db->get()->result();
		return $result;
    }
	
	function get_OrgInfo($id,$orgid)
    {
		$this->db->select('id,users_id,base_url,userid,password');
		$this -> db -> from('Eloqua_Instance');
		$this -> db -> where('users_id',$id);
		$this -> db -> where('id',$orgid);
		$this -> db -> where('enable',1);
		$result = $this->db->get()->result();
		return $result;
    }
	
	function new_Org($id ,$data)
    {
		$this->db->select('id');
		$this -> db -> from('Eloqua_Instance');
		$this -> db -> where('userid',$data['userid']);
		$temp =$this->db->count_all_results();
		if($temp==0)
		{									
			$this->db->select_max('id');
			$this -> db -> from('Eloqua_Instance');
			$temprow = $this->db->get()->result();
			$data['id']=$temprow[0]->id+1;	
			$data['enable']=1;	
			$data['users_id']= $id;	
			$this->db->insert('Eloqua_Instance', $data); 		
			$result['id'] = $data['id'];
			$result['success'] = true;		
			return $result;
		}
		else
		{
			$result['success'] = false;		
			$result['msg'] = 'login Name Can\'t be Duplicate.';		
			return $result;
		}
	}
	
	function delete_Org($id , $data)
    {
		$this->db->where_in('id', $data['id']);
		$this->db->where('users_id', $id);
		$this->db->delete('Eloqua_Instance');
	}
	
	function update_Org($id , $data)
    {
		$this->db->select('id');
		$this -> db -> from('Eloqua_Instance');
		$this -> db -> where('userid',$data['userid']);
		$this -> db -> where_not_in('id',$data['id']);
		$temp =$this->db->count_all_results();
		if($temp==0)
		{								
			$this->db->where('id', $data['id']);
			$this->db->update('Eloqua_Instance' ,$data);
			$result['success'] = true;		
			$result['msg'] = 'Account is Updated..';		
			return $result;
		}
		else
		{
			$result['success'] = false;		
			$result['msg'] = 'Login Name Can\'t be Duplicate.';		
			return $result;
		}
	}
	
	function get_Preference($id)
    {
		$this->db->select('id,user_id,Contact,Account,CDO,Segments,Filters,Emails,Forms,Landing_Pages ,Campaigns,Programs,Shared_Lists,Email_Groups,Option_Lists,Events,Contact_Views,Account_Views');
		$this -> db -> from('Asset_Type_Preference');
		$this -> db -> where('user_id',$id);
		//$this -> db -> where('enable',1);
		$result = $this->db->get()->result();
		return $result;
    }
	
	function update_Preference($id , $data)
    {
		$this->db->where('user_id', $id);
		$this->db->update('Asset_Type_Preference' ,$data);
		//return $this->db->_error_message();
		$result['success'] = true;		
		$result['msg'] = 'Account is Updated..';		
		return $result;
	}
	
	function deploy_inqueue($package_id,$skipValidationFlag)
    {
		$this -> db -> select('*');
		$this -> db -> from('deployment_queue');
		$this -> db -> where('Deployment_Package_Id',$package_id);
		$this -> db -> where('Status','In Queue');
		$this->db->limit(1);
		$query = $this -> db -> get();
	    $deployment_queue = $query->result();
		
		$date = date('Y-m-d H:i:s');
		$data['Deployment_Package_Id'] = $package_id;
		$data['Status'] = 'In Queue';
		$data['Rn_Create_Date'] = $date; 
		
		$data1['Status'] = 'Deployment In Queue';
		$this->db->where('Deployment_Package_Id',$package_id);
		$this->db->update('deployment_package',$data1);
		
		$data2['skip_validation']=$skipValidationFlag;
		$this->db->where('Deployment_Package_Id', $package_id);
		$this->db->update('deployment_package', $data2);
		
		$msg="";
		try 
		{
			if(sizeof($deployment_queue)==0)
			{
				$this->db->insert('deployment_queue',$data);
				$msg = "success";
			}
			else{
				$msg = "alreadyinqueue";
			}
		} 
		catch (Exception $e) 
		{
			$msg = "Caught exception:".$e->getMessage();
		}		
		return $msg;
	}
	
	function deploy_queue()
    {		
		$date = date('Y-m-d H:i:s');		
		$this->db->select('*');
		$this -> db -> from('deployment_queue');
		$this -> db -> where('Status','In Queue');
		$this->db->order_by("Rn_Create_Date", "asc");
		$query = $this -> db -> get();
	    $deployment_queue = $query->result();
		$msg="";
		foreach($deployment_queue as $key=>$val)
		{
			$data1['Status'] = 'In Progress'; 
			$data1['Start_'] = date('Y-m-d H:i:s');
			try 
			{  
				
				$this->db->where('Deployment_Queue_Id',$val->Deployment_Queue_Id);
				$this->db->update('deployment_queue',$data1);
				
				//////echo '<br/>Validate Package Function<br/>';
				//$validateResult = $this->eloqua->validatepackage($val->Deployment_Package_Id);
				//////echo '<br> Validate Package Result'.$validateResult;
				
				////echo '<br/>Validate Package Function<br/>';
				// $validateResult = $this->validatepackage_recursive($val->Deployment_Package_Id,1);
				$skipValidation=0;
				$skipValidation = $this->changeset->getSkipValidation($val->Deployment_Package_Id);
				if($skipValidation==1)
				{
					$validateResult = $this->validatepackage_recursive($val->Deployment_Package_Id);
				}	
				//echo '<br> Validate Package Result'.$validateResult;
				
				//Handles the circular dependency which is identified during validate
				//$this->handleCircularDependencybeforeDeploy($val->Deployment_Package_Id);
				//
				////echo '<br/>Set Deploy Package Status Function<br/>';
				$deployResult = $this->deploy_package($val->Deployment_Package_Id);
				////echo '<br> Validate Package Result'.$deployResult;
				
				////echo '<br/>Pre_Deploy_Package_Item Function<br/>';
				$predeployResult = $this->pre_deploy_package_item($val->Deployment_Package_Id,$val->Deployment_Queue_Id);			
				////echo '<br> Validate Package Result'.$predeployResult;
				
				// $this->handleCircularDependencyafterDeploy($val->Deployment_Package_Id);
				$msg = "success";
			} 
			catch (Exception $e) 
			{
				$msg = "Caught exception:".$e->getMessage();
			}		
		}	
		return $msg;		
	}
	
	function handleCircularDependencyafterDeploy($packageId)
	{
		$this -> db -> select('Base_Url, Token, Site_Name, Token_Expiry_Time, Refresh_Token');
		$this -> db -> from('instance_ inst');				
		$this -> db -> join('deployment_package dp','dp.Target_Site_Name = inst.Site_Name', 'LEFT OUTER');
		$this -> db -> join('deployment_package_item dpi','dpi.Deployment_Package_Id= dp.Deployment_Package_Id', 'LEFT OUTER');
		$this -> db -> where('dpi.Deployment_Package_Id', $packageId);
		$this -> db -> limit(1);
		$query1 		= $this -> db -> get();
		$token 		= $query1 -> result()[0] -> Token;
		$Base_Url 	= $query1 -> result()[0] -> Base_Url;		
		$org1	= $query1 -> result()[0];
		
		// print_r($org1);
		
		$this->db->select('*');
		$this->db->from('deployment_package_item dp');
		$this->db->where('Deployment_Package_Id',$packageId);
		$this->db->where('isCircular',-1);
		$this->db->order_by("Deploy_Ordinal", "desc");
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			$result = $query->result();
			// print_r('<pre>');
			// print_r($query->num_rows());
			
			// exit;
			
			foreach($result as $key=>$val)
			{
				$this->db->select('Minimal_JSON');
				$this->db->from('rsys_asset_type');
				$this->db->where('Asset_Type_Name',$val->Asset_Type);
				$query_at = $this->db->get();
				if($query_at->num_rows()>0)
				{
					$result_at = $query_at->result();
					$minimalJSON = $result_at[0]->Minimal_JSON;
					// print_r('<pre>');
					// print_r('handleCircularDependency');
					// print_r('rsys_asset_type loop');
					$this -> db -> select('*');
					$this -> db -> from('rsys_asset_endpoint rae');
					$this -> db -> where('rae.Asset_Type', $val->Asset_Type);
					$this -> db -> where('rae.Endpoint_Type', 'Create');
					$query_dpvl = $this -> db -> get();
					$endpoint_dpvl = $query_dpvl->result();
					if($query_dpvl->num_rows()>0)
					{
						// print_r('rsys_asset_endpoint loop');
						$minimalJSON_array = json_decode($minimalJSON,true);
						$minimalJSON_array['name'] = $val->Asset_Name;
						$minimalJSON = json_encode($minimalJSON_array);
						
						$url = $Base_Url .$endpoint_dpvl[0]->Endpoint_URL; 
						//print_r($url.'/'.$val->Target_Asset_Id );
						// print_r('<br>');
						// print_r($val->JSON_Submitted);
						if(time()+1800 > $org1->Token_Expiry_Time)
						{
						   $result2 = $this->eloqua->refreshToken($org1);
						   $org1->Token = $result2['Token']['access_token'];
						}
						// $result = $this->eloqua->postRequest($org1->Token, $url,$minimalJSON);
						$result = $this->eloqua->putRequest($token, $url.'/'.$val->Target_Asset_Id,$val->JSON_Submitted);
						//print_r($result );
						if($result['httpCode'] =='200' || $result['httpCode'] =='201')
						{
							
							// $Target_Asset_Id =  json_decode($result['body'])->id ;
							// $valid_data['Target_Asset_Id'] = $Target_Asset_Id;
							// $this->db->where('Deployment_Package_Id',$packageId);
							// $this->db->where('Asset_Id',$val->Asset_Id);
							// $this->db->where('Asset_Type',$val->Asset_Type);
							// $this->db->update('deployment_package_validation_list',$valid_data);							
							// $data['missing_target'] = 0;
							// $data['Target_Asset_Id'] = $Target_Asset_Id;
							// $data['isCircular'] = -1;
							// $this->db->where('Deployment_Package_Item_Id',$val->Deployment_Package_Item_Id);
							// $this->db->update('deployment_package_item',$data);
						}
					}
				}
			}
		}
	}
	
	//Handles the circular dependency which is identified during validate
	function handleCircularDependencybeforeDeploy($packageId)
	{
		$this->logging_model->logT($packageId,0,'handleCircularDependencybeforeDeploy','start','');
		
		$this -> db -> select('Base_Url, Token, Site_Name, Token_Expiry_Time, Refresh_Token');
		$this -> db -> from('instance_ inst');				
		$this -> db -> join('deployment_package dp','dp.Target_Site_Name = inst.Site_Name', 'LEFT OUTER');
		$this -> db -> join('deployment_package_item dpi','dpi.Deployment_Package_Id= dp.Deployment_Package_Id', 'LEFT OUTER');
		$this -> db -> where('dpi.Deployment_Package_Id', $packageId);
		$this -> db -> limit(1);
		$query1 		= $this -> db -> get();
		$token 		= $query1 -> result()[0] -> Token;
		$Base_Url 	= $query1 -> result()[0] -> Base_Url;		
		$org1	= $query1 -> result()[0];
		
		// print_r($org1);
		
		$this->db->select('*');
		$this->db->from('deployment_package_item dp');
		$this->db->where('Deployment_Package_Id',$packageId);
		$this->db->where('isCircular >',0);
		$this->db->where('missing_target',1);
		$this->db->order_by("Deploy_Ordinal", "desc");
		$query = $this->db->get();
		
		
		if($query->num_rows()>0)
		{
			$result = $query->result();
			// print_r('<pre>');
			// print_r($query->num_rows());
			// exit;
			$i=0;
			foreach($result as $key=>$val)
			{
				if($i==1)
				{
					break;
				}
				$i++;
				$this->db->select('Minimal_JSON');
				$this->db->from('rsys_asset_type');
				$this->db->where('Asset_Type_Name',$val->Asset_Type);
				$query_at = $this->db->get();
				if($query_at->num_rows()>0)
				{
					$result_at = $query_at->result();
					$minimalJSON = $result_at[0]->Minimal_JSON;
					//print_r('<pre>');
					// print_r('handleCircularDependency');
					////print_r('rsys_asset_type loop');
					$this -> db -> select('*');
					$this -> db -> from('rsys_asset_endpoint rae');
					$this -> db -> where('rae.Asset_Type', $val->Asset_Type);
					$this -> db -> where('rae.Endpoint_Type', 'Create');
					$query_dpvl = $this -> db -> get();
					$endpoint_dpvl = $query_dpvl->result();
					if($query_dpvl->num_rows()>0)
					{
						//print_r('rsys_asset_endpoint loop');
						$minimalJSON_array = json_decode($minimalJSON,true);
						$minimalJSON_array['name'] = $val->Asset_Name;
						$minimalJSON = json_encode($minimalJSON_array);
						
						$url = $org1->Base_Url .$endpoint_dpvl[0]->Endpoint_URL; 
				
						if(time()+1800 > $org1->Token_Expiry_Time)
						{
						   $result2 = $this->eloqua->refreshToken($org1);
						   $org1->Token = $result2['Token']['access_token'];

						}
						$result = $this->eloqua->postRequest($org1->Token, $url,$minimalJSON);
						//print_r($result );
						if($result['httpCode'] =='200' || $result['httpCode'] =='201')
						{
							
							$Target_Asset_Id =  json_decode($result['body'])->id ;
							$valid_data['Target_Asset_Id'] = $Target_Asset_Id;
							$this->db->where('Deployment_Package_Id',$packageId);
							$this->db->where('Asset_Id',$val->Asset_Id);
							$this->db->where('Asset_Type',$val->Asset_Type);
							$this->db->update('deployment_package_validation_list',$valid_data);
							
							$data['missing_target'] = 0;
							$data['Target_Asset_Id'] = $Target_Asset_Id;
							// $data['isCircular'] = -1;
							$data['Deploy_Ordinal'] = -1;
							$data['verified']  = 1;
							$this->db->where('Deployment_Package_Item_Id',$val->Deployment_Package_Item_Id);
							$this->db->update('deployment_package_item',$data);
						}
					}
				}
			}
		}
	}
	
	function validate_inqueue($package_id,$auto)
    {
		$this->db->select('*');
		$this -> db -> from('validate_queue');
		$this -> db -> where('Deployment_Package_Id',$package_id);
		$this -> db -> where('Status','In Queue');
		$this->db->limit(1);
		$query = $this -> db -> get();
	    $validate_queue = $query->result();
		
		$date = date('Y-m-d H:i:s');
		$data['Deployment_Package_Id'] = $package_id;
		$data['Status'] = 'Validate In Queue';
		$data['Rn_Create_Date'] = $date; 
		
		$data1['Status'] = 'Validate In Queue';
		$data1['auto_include_missing_assets'] = $auto;
		$this->db->where('Deployment_Package_Id',$package_id);
		$this->db->update('deployment_package',$data1);
		
		$msg="";
		try 
		{
			if(sizeof($validate_queue)==0)
			{
				$this->db->insert('validate_queue',$data);
				$msg = "success";
				return true;
			}
			else
			{
				$msg = "Already Inqueue";
				return true;
			}
			
		} 
		catch (Exception $e) 
		{
			$msg = "Caught exception:".$e->getMessage();
		}		
		return $msg;
	}
	
	//This function loops through the queue that are set for validation 
	function validate_queue_recursive()
	{
		$date = date('Y-m-d H:i:s');		
		$this->db->select('*');
		$this -> db -> from('validate_queue');
		$this -> db -> where('Status','Validate In Queue');
		$this->db->order_by("Rn_Create_Date", "asc");
		$query = $this -> db -> get();
	    $validate_queue = $query->result();
		$msg="";
		foreach($validate_queue as $key=>$val)
		{
			$data1['Status'] = 'In Progress'; 
			$data1['Start_'] = date('Y-m-d H:i:s');
			try 
			{  
				$this->db->where('Validate_Queue_Id',$val->Validate_Queue_Id);
				$this->db->update('validate_queue',$data1);				
				// $this->validatepackage_recursive($val->Deployment_Package_Id,0);
				$this->validatepackage_recursive($val->Deployment_Package_Id);
				$msg = "success";
				$data1['Status'] = 'Processed'; 
				$data1['End_'] = date('Y-m-d H:i:s');
				$this->db->where('Validate_Queue_Id',$val->Validate_Queue_Id);
				$this->db->update('validate_queue',$data1);	
			} 
			catch (Exception $e) 
			{
				$msg = "Caught exception:".$e->getMessage();
			}		
		}		
		return $msg;
	}
	
	function validatepackage_recursive($packageId)
	{
		$tokenAvailabilityCheck = $this->deploy_helper_model->checkForTokenAvailability($packageId);
		//print_r($tokenAvailabilityCheck);
		
		//Enhancement : 18 july 2018 , Purpose : If package includes images or files without extension after first validation if user correct asset in Eloqua get the changes in database.
		//$this->deploy_helper_model->updateImageAndFileNameForInvalid($packageId);	
		//exit;
		
		$this->db->set('Invalid_Asset_Message', null);
		$this->db->where('Deployment_Package_Id', $packageId);
		$this->db->update('deployment_package_item');
		
		$this->db->set('Unsupported_Asset_Message', null);
		$this->db->where('Deployment_Package_Id', $packageId);
		$this->db->update('deployment_package_item');
		
		
		$this->db->set('ActionRequired_Asset_Message', null);
		$this->db->where('Deployment_Package_Id', $packageId);
		$this->db->update('deployment_package_item');
		
		if($tokenAvailabilityCheck == 'success')
		{
			$this->logging_model->logT($packageId,0,'validatepackage_recursive','start','');
			$resetdata['Status'] = 'Validating';
			$this -> db -> where('Deployment_Package_Id', $packageId);
			$this -> db -> update('deployment_package_item', $resetdata);
			
			$this -> db -> select('*');
			$this -> db -> from('deployment_package_item dp');
			$this -> db -> where('dp.Deployment_Package_Id', $packageId);
			// if($fromDeploy == 1)
			// {
				// $this -> db -> where('dp.verified', 1);
			// }
			$query = $this -> db -> get();
			$package_item = $query->result();
			
			// echo '<pre>package_item<br>';
			 // print_r($package_item);
			 // echo '<pre><br>';
			 
			 // $this->deploy_helper_model->addExtension($package_item);	
			 
			 // $this -> db -> select('*');
			// $this -> db -> from('deployment_package_item dp');
			// $this -> db -> where('dp.Deployment_Package_Id', $packageId);
			 // $query2 = $this -> db -> get();
			// $package_item = $query2->result();
			
			// echo '<pre>package_item<br>';
			 // print_r($package_item);
			 // echo '<pre><br>';
			// exit;
					
			// $this->db->where('Deployment_Package_Id', $packageId);
			// $this->db->delete('deployment_package_validation_list');
			foreach($package_item as $key=>$val)
			{
				$AssetChildData = $this->eloqua->getAssetChild($packageId,$val->Deployment_Package_Item_Id, $val->Asset_Id,$val->Asset_Type ,$Endpoint_Type = 'Read Single');
				
				 // echo '<pre>AssetChildData 1<br>';
				 // print_r($AssetChildData);
				 // echo '<pre><br>';
			
				// if($val->Asset_Type=='Field Merge')
				// {
					// echo '<pre>';
						// print_r($AssetChildData);
					// echo '<pre>';
				// }	
				if(is_array($AssetChildData) || is_object($AssetChildData))
				{
					foreach($AssetChildData as $ChildKey=>$ChildVal)
					{
						
						$targetAsset = $this->checkForMapping($packageId,$val->Asset_Type,$ChildVal['Asset_Type'], $ChildVal['Asset_Id']);
				
						// echo '<pre>targetAsset23<br>';
							// print_r($targetAsset);
						// echo '<pre><br>';
						
						$this->logging_model->logT($packageId,$val->Deployment_Package_Item_Id,'validate_queue_recursive','inserting child into validation list','');
						if($targetAsset != false )
						{
							// echo 'target assetnot false';
							$data['Target_Asset_Id'] = $targetAsset['Asset_Id'];
							$data['Target_Asset_Name'] = $targetAsset['Asset_Name'];
						}
						
						//For VC 070818 Sreejon
						$this -> db -> select('*');
						$this -> db -> from('deployment_package_validation_list');
						$this -> db -> where('Deployment_Package_Item_Id', $val->Deployment_Package_Item_Id);
						$this -> db -> where('Asset_Id', $ChildVal['Asset_Id']);
						$this -> db -> where('Asset_Type', $ChildVal['Asset_Type']);
						$queryResult = $this->db->get();
						if($queryResult->num_rows()>0)
						{
						}
						else
						{
							$data['Deployment_Package_Item_Id'] = $val->Deployment_Package_Item_Id;
							$data['Deployment_Package_Id'] = $val->Deployment_Package_Id;
							$data['Asset_Type'] = $ChildVal['Asset_Type'];
							$data['Asset_Id'] = $ChildVal['Asset_Id'];
							$data['Asset_Name'] = $ChildVal['Display_Name'];
							$data['JSON_Node_Value'] = $ChildVal['JSON_Node_Value'];
							$data['Status'] = 'New';
							$this->db->insert('deployment_package_validation_list',$data);
						}
						//
						
						
						unset($data);
					}
				}
			}
			
			foreach( $package_item as $key=>$val)
			{
				$this->buildGlobalStack($val,$packageId);
			}
			//exit;
			$this->updateDeployOrdinal($packageId);
			
			// $this->updateMissingAssetJsonAndStatus($packageId);
			
			/////////print_r('Inside validatepackage_recursive');
			/////////$this->callNodeEnd($packageId);
			$this->updateMissingElements($packageId);
			
			// $this->identifyCircular_initiate_updated($packageId);
			$this->deploy_helper_model->updateUnsupportedAssets($packageId);			
			$this->deploy_helper_model->checkDuplicatehtmlName($packageId);		
			$this->deploy_helper_model->EmailWithDifferentEG($packageId);
			$this->deploy_helper_model->checkForCampaignInputs($packageId);			
			$this->deploy_helper_model->checkForLandingPageWithoutMicrositeForChildOfHyperlink($packageId);
			$this->deploy_helper_model->makeAssignMicrositeMandatory($packageId);
			
			$this->deploy_helper_model->checkForUserDefinedMappingForAsset($packageId);
			$this->deploy_helper_model->checkAssetsForActionRequired($packageId);	
			
			$this->deploy_helper_model->inValidKeyFieldMapping($packageId);
			$this->deploy_helper_model->invalidateDuplicateCampaignField($packageId);
			$this->deploy_helper_model->inValidPickListForAssets($packageId);
			// $this->deploy_helper_model->checkForInvalidImageAndFiles($packageId);
			
			$data2['Status'] = $this->decideStatusValidate($val->Deployment_Package_Id);
			$this->db->where('Deployment_Package_Id',$val->Deployment_Package_Id);
			$this->db->update('deployment_package',$data2);
			
			// $this->handleCircularDependency($packageId);
			
		}
		else
		{
			$data2['Status'] = "Missing Tokens";
			$this->db->where('Deployment_Package_Id',$packageId);
			$this->db->update('deployment_package',$data2);
		}
		$this->logging_model->logT($packageId,0,'validatepackage_recursive','end','');
	}	
	
	function updatePackageStatus($packageId)
	{
		$this -> db -> select('*');
		$this -> db -> from('deployment_package_item dp');
		$this -> db -> where('dp.Deployment_Package_Id', $packageId);
		$query = $this -> db -> get();
		
		$missing_assets = false;
	    $package_item = $query->result();
		
		if($query->num_rows()>0)
		{
			foreach($package_item  as $key=>$val)
			{
				if($val->missing_target == 1 &&  $val->verified == 0)
				{
					$missing_assets = true;
					break;
				}
			}
		}
		if($missing_assets)
		{
			$tdata['Status'] = 'Missing Assets';
			$this->db->where('Deployment_Package_Id',$packageId);
			$this->db->update('deployment_package',$tdata);
		}
	}
	
	function updateDeployOrdinal($packageId)
	{
		$this->logging_model->logT($packageId,'',0,'updating deploy ordinal','');
		foreach($this->globalStack as $key=>$val)
		{
			$data['Deploy_Ordinal'] = $key;
			$this->db->where('Deployment_Package_Item_Id',$val);
			$this->db->update('deployment_package_item',$data);
		}
	}
	
	function checkAssetInTarget($packageId,$asset_type,$asset_name)
	{
		$result_final = 0;
		$this -> db -> select('*');
		$this -> db -> from('deployment_package_item dp');
		$this -> db -> where('dp.Deployment_Package_Id', $packageId);
		$query = $this -> db -> get();
	    $package_item = $query->result();
		
		$this -> db -> select('*');
		$this -> db -> from('deployment_package dp');
		$this -> db -> join('instance_ inst','dp.Target_Site_Name= inst.Site_Name', 'LEFT OUTER');
		$this -> db -> where('dp.Deployment_Package_Id', $packageId);
		// $this -> db -> where('dp.Created_By_Contact_Id', $Contact_Id);
		$this -> db -> limit(1);
		$query = $this -> db -> get();
		
		$org = $query->result()[0];
		
		$this -> db -> select('*');
		$this -> db -> from('rsys_asset_endpoint rae');
		$this -> db -> where('rae.Asset_Type', $asset_type);
		$this -> db -> where('rae.Endpoint_Type', 'Find Asset');
		$query = $this -> db -> get();
		$endpoint = $query->result();
		if($endpoint)
		{	
			// $url = $org->Base_Url .$endpoint[0]->Endpoint_URL .'?search="'.str_replace(' ', '%20', $val->Asset_Name) .'"' ; 
			
			//changes for email with different email group and htmlname in form - START
			if($asset_type=='Email' || $asset_type=='Form' || $asset_type=='Campaign')
			{
				$url = $org->Base_Url .$endpoint[0]->Endpoint_URL .'?search="'.urlencode($asset_type) .'"&depth=complete' ; 
			}
			else
			{
				$url = $org->Base_Url .$endpoint[0]->Endpoint_URL .'?search="'.urlencode($asset_type) .'"' ;

			}
			
			if(time()+1800 > $org->Token_Expiry_Time)
			{
			   $result2 = $this->eloqua->refreshToken($org);
			   $org->Token = $result2['Token']['access_token'];
			}
			$result = $this->eloqua->get_request($org->Token, $url);
			$result = json_decode($result['data']);
			
			// if($val->Asset_Type=='Contact Field')
			// {
				// echo '<pre>';
					// print_r($result);
				// echo '</pre>';
			// }	
			
			if(isset($result->total) && $result->total >0)
			{	
				$result_final = 1;
			}
		}
		return $result_final;
	}
	
	function updateMissingElements($packageId)
	{
		$this -> db -> select('*');
		$this -> db -> from('deployment_package_item dp');
		$this -> db -> where('dp.Deployment_Package_Id', $packageId);
		$query = $this -> db -> get();
	    $package_item = $query->result();
		
		//Enhancement for extension on 26/07/18
		$this->deploy_helper_model->addExtension($package_item);	
			 
			 $this -> db -> select('*');
			$this -> db -> from('deployment_package_item dp');
			$this -> db -> where('dp.Deployment_Package_Id', $packageId);
			 $query2 = $this -> db -> get();
			$package_item = $query2->result();
		
		$this -> db -> select('*');
		$this -> db -> from('deployment_package dp');
		$this -> db -> join('instance_ inst','dp.Target_Site_Name= inst.Site_Name', 'LEFT OUTER');
		$this -> db -> where('dp.Deployment_Package_Id', $packageId);
		// $this -> db -> where('dp.Created_By_Contact_Id', $Contact_Id);
		$this -> db -> limit(1);
		$query = $this -> db -> get();
		
		$org = $query->result()[0];
		
		foreach($package_item as $key=>$val)
		{
			//update missing elements
			$this->logging_model->logT($packageId,$val->Deployment_Package_Item_Id,'updateMissingElements','','');
			$this -> db -> select('*');
			$this -> db -> from('rsys_asset_endpoint rae');
			$this -> db -> where('rae.Asset_Type', $val->Asset_Type);
			$this -> db -> where('rae.Endpoint_Type', 'Find Asset');
			$query = $this -> db -> get();
			$endpoint = $query->result();
			if($endpoint)
			{	
				// $url = $org->Base_Url .$endpoint[0]->Endpoint_URL .'?search="'.str_replace(' ', '%20', $val->Asset_Name) .'"' ; 
				$Asset_Name = $val->Asset_Name;
				// $posFlag = 0;
				// if(strpos($Asset_Name,':') !== false)
				// {
					// $pos = strpos($Asset_Name,':');
					
					// if($Asset_Name{$pos-1}!=' ' && $Asset_Name{$pos+1}!=' ')
					// {
						// $posFlag = 1;
						// $plainText_temp=explode(":",$Asset_Name);
						// $tempAssetName = trim($plainText_temp[0]);
						// $Asset_Name = $tempAssetName;
					// }
					// else
						// $Asset_Name = $Asset_Name;
				// }
				
				//changes to asset name for special characters
				if(preg_match($this->pattern,  $Asset_Name))
				{
					$tempAssetName = str_replace('-', '_', $Asset_Name);
					$tempAssetName = preg_replace('/[^A-Za-z0-9\-]/','*',$tempAssetName);
				}
				else
				{
					$tempAssetName=urlencode($Asset_Name);
				}	
				// if($val->Asset_Type=='Image')
				// {
					// echo 'here';
					// print_r($tempAssetName);
					// $tempAssetName = str_replace('amp','*',$tempAssetName);
				// }
				//changes for email with different email group and htmlname in form - START
				if($val->Asset_Type=='Email' || $val->Asset_Type=='Form' || $val->Asset_Type=='Campaign')
				{
					// if($posFlag == 0)
					// {
						//$url = $org->Base_Url .$endpoint[0]->Endpoint_URL .'?search="'.urlencode($val->Asset_Name) .'"&depth=complete' ; 
						// $url = $org->Base_Url .$endpoint[0]->Endpoint_URL .'?search="'.urlencode($Asset_Name) .'"&depth=complete' ; 
						$url = $org->Base_Url .$endpoint[0]->Endpoint_URL .'?search="'.$tempAssetName .'"&depth=complete' ; 
					// }
					// else
						// $url = $org->Base_Url .$endpoint[0]->Endpoint_URL .'?search="*'.urlencode($Asset_Name) .'*"&depth=complete' ; 
						// $url = $org->Base_Url .$endpoint[0]->Endpoint_URL .'?search="*'.$tempAssetName .'*"&depth=complete' ; 
				}
				else
				{
				// if($posFlag == 0)
					// {
						//$url = $org->Base_Url .$endpoint[0]->Endpoint_URL .'?search="'.urlencode($val->Asset_Name) .'"' ;
						// $url = $org->Base_Url .$endpoint[0]->Endpoint_URL .'?search="'.urlencode($Asset_Name) .'"' ;
						$url = $org->Base_Url .$endpoint[0]->Endpoint_URL .'?search="'.$tempAssetName .'"' ;
					// }
					// else
						// $url = $org->Base_Url .$endpoint[0]->Endpoint_URL .'?search="*'.urlencode($Asset_Name) .'*"' ;
						// $url = $org->Base_Url .$endpoint[0]->Endpoint_URL .'?search="*'.$tempAssetName .'*"' ;

				}
				
				// echo 'url<pre>';
					// print_r($url);
				// echo '</pre><br>';
				
				//changes for email with different email group and htmlname in form - END
				if(time()+1800 > $org->Token_Expiry_Time)
				{
				    $result2 = $this->eloqua->refreshToken($org);
				    $org->Token = $result2['Token']['access_token'];
				}
				
				$result = $this->eloqua->get_request($org->Token, $url);
				
				// print_r($url);
				// echo '<br>Result32<pre>';
					// print_r($result);
				// echo '</pre><br>';
				
				$result = json_decode($result['data']);	

				// echo 'Result33<pre>';
					// print_r($result);
				// echo '</pre><br>';
				
				$assetArray = array();
				$assetCount = 0;
				// print_r($Asset_Name);
				// echo '<br>';
				if($val->Asset_Type=='Image')
				{
					$imageNamePattern='/[&*#!$%+^",]/';
					if(preg_match($imageNamePattern,  $Asset_Name))
					{
						$Asset_Name = str_replace('amp;','&',$Asset_Name);
						$Asset_Name = preg_replace('/[&*#!$%+^",]/','_',$Asset_Name);
						//echo '<br>'; print_r($Asset_Name);
						
						//echo '<br>'; print_r($Asset_Name);
					}	
				}
				// print_r($url);
				// print_r($Asset_Name);
				// echo '<br>';
				foreach($result->elements as $rtKey=>$rtVal)
				{
					if($rtVal->name==$Asset_Name)
					{
						  array_push($assetArray,$rtVal);
						  $assetCount++;
					}
				}
				$result->elements=$assetArray;
				$result->total=$assetCount;				
				
				
				// echo '<pre>Result34<br>';
					// print_r($result);
				// echo '</pre><br>';
				
				// echo 'Result34total<pre>';
					// print_r($result->total);
				// echo '</pre><br>';
				
				if(isset($result->total) && $result->total >0)
				{
					$assetArray = array();
					$assetCount = 0;
					foreach($result->elements as $rKey=>$rVal)
					{
						if($rVal->name==$Asset_Name)
						{
							array_push($assetArray,$rVal);
							$assetCount++;
						}
							
					}
					$result->elements = $assetArray;
					$result->total=$assetCount;
				
					// echo 'Result35<pre>';
						// print_r($result);
					// echo '</pre><br>';
				
					if($result->total >1)
					{
						if($val->verified == 1)
						{
							//START 11 sep 
							$duplicates=array();
							$duplicate;
							$duplicate_E;
							$duplicate_F;
							if($val->Asset_Type=='Email')
							{
								$sourceJSON=json_decode($val->JSON_Asset);
								$sourceEmailGroupId=$sourceJSON->emailGroupId;
								$sourceEmailName=$sourceJSON->name;
								$sourceEmailGroupName=$this->deploy_helper_model->getEmailGroupNameById($sourceEmailGroupId,$packageId,'source');
								$i=0;
								$duplicatesEmail=array();
								$duplicateEmail;
								foreach($result->elements as $DKey=>$DVal)
								{
									if($DVal->type=='Email')
									{
										$duplicate_E['Asset_Name']=$DVal->name;
										$duplicate_E['Asset_Id']=$DVal->id;
										$duplicate_E['Asset_Type']=$DVal->type;
										if(isset($DVal->emailGroupId))
										{
											$targetEmailGroupName=$this->deploy_helper_model->getEmailGroupNameById($DVal->emailGroupId,$packageId,'target');
											if($sourceEmailName==$DVal->name && $sourceEmailGroupName==$targetEmailGroupName)
											{
												$duplicate_E['Status']='Same';
											}
											else
											{
												$duplicate_E['Status']='Different';
											}	
										}
										else
										{
											$duplicate_E['Status']='No EG';
										}
										array_push($duplicates,$duplicate_E);	
									}	
								}
							}
							else if($val->Asset_Type=='Form')
							{
								foreach($result->elements as $DKey=>$DVal)
								{
									$sourceJSONForm=json_decode($val->JSON_Asset);
									$sourceFormName=$sourceJSONForm->name;
									$sourceFormhtmlName=$sourceJSONForm->htmlName;
									if($DVal->type=='Form')
									{
										$duplicate_F['Asset_Name']=$DVal->name;
										$duplicate_F['Asset_Id']=$DVal->id;
										$duplicate_F['Asset_Type']=$DVal->type;
										if(isset($DVal->htmlName))
										{
											if($sourceFormName==$DVal->name && $sourceFormhtmlName==$DVal->htmlName)
											{
												$duplicate_F['Status']='Same';
											}
											else
											{
												$duplicate_F['Status']='Different';
											}	
										}
										else
										{
											$duplicate_E['Status']='No htmlName';
										}
										array_push($duplicates,$duplicate_F);	
									}	
								}
							}
							else
							{
								foreach($result->elements as $DKey=>$DVal)
								{
									$duplicate['Asset_Name']=$DVal->name;
									$duplicate['Asset_Id']=$DVal->id;
									array_push($duplicates,$duplicate);
								}
							}
							// echo '<pre>';
								// print_r($duplicates);
							// echo '</pre>';
	
							$targetAssetDetails['duplicates_from_target'] = $duplicates;
							$targetAssetDetails['parent_asset_type'] = '';
							$targetAssetDetails['parent_asset_name'] = '';
							$pdata['missing_target'] = 0;
							if($val->Target_Asset_Id==0 || $val->Target_Asset_Id=='')
							{
								$pdata['Status'] = 'Duplicate';
							}
							$pdata['verified'] = 1;
							$pdata['Target_Duplicate_Assets'] = json_encode($targetAssetDetails);
							$this->db->where('Deployment_Package_Item_Id',$val->Deployment_Package_Item_Id);
							$this->db->update('deployment_package_item',$pdata);
							unset($pdata);
							//changes for email with different email group and htmlname in form - END 11 sep
						} 
						
						//too critical for deploy
						if($val->Target_Asset_Id > 0)
						{
							$tdata['Target_Asset_Id'] = $val->Target_Asset_Id;
							$this->db->where('Deployment_Package_Id',$val->Deployment_Package_Id);
							$this->db->where('Asset_Type',$val->Asset_Type);
							$this->db->where('Asset_Id',$val->Asset_Id);
							$this->db->where('Target_Asset_Id',0);
							$this->db->update('deployment_package_validation_list',$tdata);
						}
						else
						{		
							$tdata['Target_Asset_Id'] = $result->elements[0]->id;
							$this->db->where('Deployment_Package_Id',$val->Deployment_Package_Id);
							$this->db->where('Asset_Type',$val->Asset_Type);
							$this->db->where('Asset_Id',$val->Asset_Id);
							$this->db->where('Target_Asset_Id',0);
							$this->db->update('deployment_package_validation_list',$tdata);
						}						
						unset($tdata);
					}
					else
					{
						// if($val->Asset_Type=='Email Group')
						// {
							// print_r($result->elements[0]->id);
						// }
						$pdata['missing_target'] = 0;
						$this->db->where('Deployment_Package_Item_Id',$val->Deployment_Package_Item_Id);
						$this->db->update('deployment_package_item',$pdata);
						unset($pdata);
						
						//too critical for deploy // 06042018
						$tdata['Target_Asset_Id'] = $result->elements[0]->id;
						$this->db->where('Deployment_Package_Id',$val->Deployment_Package_Id);
						$this->db->where('Asset_Type',$val->Asset_Type);
						$this->db->where('Asset_Id',$val->Asset_Id);
						$this->db->where('Target_Asset_Id',0);
						$this->db->update('deployment_package_validation_list',$tdata);
						
					}
				}
				else
				{
				
					$pdata['missing_target'] = 1;
					
					// echo 'pdata<pre>';
					// print_r($pdata);
					// echo '</pre><br>';
				
					//$pdata['verified'] = $org->auto_include_missing_assets;
					$this->db->where('Deployment_Package_Item_Id',$val->Deployment_Package_Item_Id);
					$this->db->update('deployment_package_item',$pdata);
					//$pdata['missing_target'] = 1
					//$pdata['verified'] = $org->auto_include_missing_assets;
				}
			}			
		}
		
		$this -> db -> select('*');
		$this -> db -> from('deployment_package_item dp');
		$this -> db -> where('dp.Deployment_Package_Id', $packageId);
	//	$this -> db -> where('verified',1 OR 'missing_target',1);
		$this->db->where("(verified=1 OR missing_target=1)");
	//	$this -> db -> or_where('missing_target',1);
		$query = $this -> db -> get();
	    $package_item1 = $query->result();
		
		$this -> db -> select('*');
		$this -> db -> from('deployment_package dp');
		$this -> db -> join('instance_ inst','dp.Target_Site_Name= inst.Site_Name', 'LEFT OUTER');
		$this -> db -> where('dp.Deployment_Package_Id', $packageId);
		// $this -> db -> where('dp.Created_By_Contact_Id', $Contact_Id);
		$this -> db -> limit(1);
		$query1 = $this -> db -> get();
		
		$org1 = $query1->result()[0];
		foreach($package_item1 as $key1=>$val1)
		{ 
			//This is to identify duplicates from one level child
			$this->db->select();
			$this->db->from('deployment_package_validation_list');
			$this->db->where('Deployment_Package_Item_Id',$val1->Deployment_Package_Item_Id);
			$query = $this->db->get();
			
			if($query->num_rows()>0)
			{
				$package_validation_list = $query->result();
				
				// echo 'package_validation_list<pre>';
						// print_r($package_validation_list);
					// echo '</pre><br>';
				
				foreach($package_validation_list as $pvlkey=>$pvlval)
				{     
					$this -> db -> select('*');
					$this -> db -> from('rsys_asset_endpoint rae');
					$this -> db -> where('rae.Asset_Type', $pvlval->Asset_Type);
					$this -> db -> where('rae.Endpoint_Type', 'Find Asset');
					$query = $this -> db -> get();
					$endpoint_dpvl = $query->result();
					// print_r('<pre>');
					// print_r('-------------<br>');
					// print_r($pvlval);
					// print_r('<br>-------------');
					//$url = $org1->Base_Url .$endpoint_dpvl[0]->Endpoint_URL .'?search="'.str_replace(' ', '%20', $pvlval->Asset_Name) .'"' ; 
					// echo 'endpoint_dpvl<pre>';
						// print_r($endpoint_dpvl);
					// echo '</pre><br>';
					
					$Asset_Name = $pvlval->Asset_Name;
					
					// echo 'pvlval<pre>';
						// print_r($pvlval);
					// echo '</pre><br>';
					
					// $posFlag = 0;
					// if(strpos($Asset_Name,':') !== false)
					// {
						// $pos = strpos($Asset_Name,':');
						
						// if($Asset_Name{$pos-1}!=' ' && $Asset_Name{$pos+1}!=' ')
						// {
							// $posFlag = 1;
							// $plainText_temp=explode(":",$Asset_Name);
							// $tempAssetName = trim($plainText_temp[0]);
							// $Asset_Name = $tempAssetName;
						// }
						// else
							// $Asset_Name = $Asset_Name;
					// }
					
					//changes to asset name for special characters
					if(preg_match($this->pattern,  $Asset_Name))
					{
						$tempAssetName = str_replace('-', '_', $Asset_Name);
						$tempAssetName = preg_replace('/[^A-Za-z0-9\-]/','*',$tempAssetName);
					}
					else
						$tempAssetName=urlencode($Asset_Name);
					
					//changes for email with different email group and htmlname in form - START
					if($pvlval->Asset_Type=='Email' || $pvlval->Asset_Type=='Form')
					{
						// if($posFlag == 0)
						// {
							// $url = $org->Base_Url .$endpoint_dpvl[0]->Endpoint_URL .'?search="'.urlencode($Asset_Name) .'"&depth=complete' ; 
							$url = $org->Base_Url .$endpoint_dpvl[0]->Endpoint_URL .'?search="'.$tempAssetName .'"&depth=complete' ; 
						// }
						// else
							// $url = $org->Base_Url .$endpoint_dpvl[0]->Endpoint_URL .'?search="*'.urlencode($Asset_Name) .'*"&depth=complete' ;
							// $url = $org->Base_Url .$endpoint_dpvl[0]->Endpoint_URL .'?search="*'.$tempAssetName .'*"&depth=complete' ;
					}
					else
					{
						// if($posFlag == 0)
						// {
							// $url = $org->Base_Url .$endpoint_dpvl[0]->Endpoint_URL .'?search="'.urlencode($Asset_Name) .'"' ; 
							$url = $org->Base_Url .$endpoint_dpvl[0]->Endpoint_URL .'?search="'.$tempAssetName .'"' ; 
						// }
						// else
							// $url = $org->Base_Url .$endpoint_dpvl[0]->Endpoint_URL .'?search="*'.urlencode($Asset_Name) .'*"' ; 
							// $url = $org->Base_Url .$endpoint_dpvl[0]->Endpoint_URL .'?search="*'.$tempAssetName .'*"' ; 
					}
				
					if(time()+1800 > $org1->Token_Expiry_Time)
					{
					   $result2 = $this->eloqua->refreshToken($org1);
					   $org1->Token = $result2['Token']['access_token'];
					}
					$result_dpvl = $this->eloqua->get_request($org1->Token, $url);
					
					// if($pvlval->Asset_Type == 'Contact Field')
					// {
						// print_r($result_dpvl);
					// }
					$result_dpvl = json_decode($result_dpvl['data']);
					
					// echo 'result_dpvl before<br><pre>';
						// print_r($result_dpvl);
					// echo '</pre><br>';
					
					$assetArray = array();
					$assetCount = 0;
					foreach($result_dpvl->elements as $rKey=>$rVal)
					{
						if($rVal->name==$Asset_Name)
						{
							array_push($assetArray,$rVal);
							$assetCount++;
						}
							
					}
					$result_dpvl->elements = $assetArray;
					$result_dpvl->total=$assetCount;
					
					// echo 'result_dpvl after<br><pre>';
						// print_r($result_dpvl);
					// echo '</pre><br>';
				
					if(isset($result_dpvl->total) && $result_dpvl->total >1)
					{
						$duplicates_dpvl = array();
						$duplicate_dpvl;
						/*foreach($result_dpvl->elements as $DpvlKey=>$DpvlVal)
						{
							$duplicate_dpvl['Asset_Name']=$DpvlVal->name;
							$duplicate_dpvl['Asset_Id']=$DpvlVal->id;
							array_push($duplicates_dpvl,$duplicate_dpvl);
						}
						*/
						/*START Update for email with different Email Group and Form with Different htmlName*/
						if($pvlval->Asset_Type=='Email')
						{
							$sourceJSON=$this->deploy_helper_model->getSourceJSONById($pvlval->Asset_Id,$pvlval->Asset_Type,$packageId);
							$sourceEmailGroupId=$sourceJSON->emailGroupId;
							$sourceEmailName=$sourceJSON->name;
							$sourceEmailGroupName=$this->deploy_helper_model->getEmailGroupNameById($sourceEmailGroupId,$packageId,'source');
							$i=0;
							$duplicatesEmail=array();
							$duplicateEmail;
							foreach($result_dpvl->elements as $DKey=>$DVal)
							{	
								if($DVal->type=='Email')
								{
									$duplicate_E['Asset_Name']=$DVal->name;
									$duplicate_E['Asset_Id']=$DVal->id;
									$duplicate_E['Asset_Type']=$DVal->type;

									//print_r($DVal);
									if(isset($DVal->emailGroupId))
									{
										$targetEmailGroupName=$this->deploy_helper_model->getEmailGroupNameById($DVal->emailGroupId,$packageId,'target');
										if($sourceEmailName==$DVal->name && $sourceEmailGroupName==$targetEmailGroupName)
										{
											$duplicate_E['Status']='Same';
										}
										else
										{
											$duplicate_E['Status']='Different';
										}	
									}
									else
									{
										$duplicate_E['Status']='No EG';
									}
									array_push($duplicates_dpvl,$duplicate_E);	
								}	
							}
						}
						else if($pvlval->Asset_Type=='Form')
						{	
							foreach($result_dpvl->elements as $DKey=>$DVal)
							{
								$sourceJSONForm=$this->deploy_helper_model->getSourceJSONById($pvlval->Asset_Id,$pvlval->Asset_Type,$packageId);
								$sourceFormName=$sourceJSONForm->name;
								$sourceFormhtmlName=$sourceJSONForm->htmlName;
								if($DVal->type=='Form')
								{
									
									$duplicate_F['Asset_Name']=$DVal->name;
									$duplicate_F['Asset_Id']=$DVal->id;
									$duplicate_F['Asset_Type']=$DVal->type;
									if(isset($DVal->htmlName))
									{
										if($sourceFormName==$DVal->name && $sourceFormhtmlName==$DVal->htmlName)
										{
											$duplicate_F['Status']='Same';
										}
										else
										{
											$duplicate_F['Status']='Different';
										}	
									}
									else
									{
										$duplicate_F['Status']='No htmlName';
									}
									array_push($duplicates_dpvl,$duplicate_F);	
								}		
							}
						}
						else
						{
							foreach($result_dpvl->elements as $DpvlKey=>$DpvlVal)
							{
								$duplicate_dpvl['Asset_Name']=$DpvlVal->name;
								$duplicate_dpvl['Asset_Id']=$DpvlVal->id;
								array_push($duplicates_dpvl,$duplicate_dpvl);
							}	
						}
						/*END Update for email with different Email Group and Form with Different htmlName*/
						$targetAssetDetails_dpvl['duplicates_from_target'] = $duplicates_dpvl;
						$targetAssetDetails_dpvl['parent_asset_type'] = $val1->Asset_Type;
						$targetAssetDetails_dpvl['parent_asset_name'] = $val1->Asset_Name;
						$pdata['missing_target'] = 0;
						// if($val1->Target_Asset_Id==0 || $val1->Target_Asset_Id=='')
						// {
							// $pdata['Status'] = 'Duplicate';
						// }
						$pdata['verified'] = 1;
						$pdata['Target_Duplicate_Assets'] = json_encode($targetAssetDetails_dpvl);
						$this->db->where('Deployment_Package_Id',$val1->Deployment_Package_Id);
						$this->db->where('Asset_Type',$pvlval->Asset_Type);
						$this->db->where('Asset_Id',$pvlval->Asset_Id);
						$this->db->update('deployment_package_item',$pdata);
						unset($pdata);
						
						$pdata['Status'] = 'Duplicate';
						$this->db->where('Deployment_Package_Id',$val1->Deployment_Package_Id);
						$this->db->where('Asset_Type',$pvlval->Asset_Type);
						$this->db->where('Asset_Id',$pvlval->Asset_Id);
						$this->db->where('Target_Asset_Id',0);
						$this->db->update('deployment_package_item',$pdata);
						
						$this->db->select('*');
						$this->db->from('deployment_package_item');
						$this->db->where('Deployment_Package_Id',$val1->Deployment_Package_Id);
						$this->db->where('Asset_Type',$pvlval->Asset_Type);
						$this->db->where('Asset_Id',$pvlval->Asset_Id);
						$result_final = $this->db->get()->result();
						
						// echo 'result_final<br><pre>';
							// print_r($result_final);
						// echo '</pre><br>';
					
						if($result_final[0]->Target_Asset_Id > 0)
						{
							$tempdata['Target_Asset_Id'] = $result_final[0]->Target_Asset_Id;
							$this->db->where('Deployment_Package_Id',$val1->Deployment_Package_Id);
							$this->db->where('Asset_Type',$pvlval->Asset_Type);
							$this->db->where('Asset_Id',$pvlval->Asset_Id);
							$this->db->where('Target_Asset_Id',0);
							$this->db->update('deployment_package_validation_list',$tempdata);
							
						}
						unset($pdata);
					}
				}
			}
		}
		$this->updateVerifiedforMissingElements($packageId);
	}
	
	function updateVerifiedforMissingElements($packageId)
	{
		$this -> db -> select('*');
		$this -> db -> from('deployment_package_item dp');
		$this -> db -> where('dp.Deployment_Package_Id', $packageId);
		$query = $this -> db -> get();
	    $package_item = $query->result();
		
		$this -> db -> select('*');
		$this -> db -> from('deployment_package dp');
		$this -> db -> join('instance_ inst','dp.Source_Site_Name = inst.Site_Name', 'LEFT OUTER');
		$this -> db -> where('dp.Deployment_Package_Id', $packageId);
		// $this -> db -> where('dp.Created_By_Contact_Id', $Contact_Id);
		$this -> db -> limit(1);
		$query_ = $this -> db -> get();
		$org = $query_->result()[0];
		
		foreach( $package_item as $key=>$val)
		{
			if($val->verified == 0 && $org->auto_include_missing_assets == 1)
			{
				// $parentsVerified = $this->checkIfParentsExistInPackage($val->Asset_Type,$val->Asset_Id,$packageId);
				// if($parentsVerified)
				// {
					$this->logging_model->logT($packageId,$val->Deployment_Package_Item_Id,'updateVerifiedforMissingElements','','');
					$pdata['verified'] = $org->auto_include_missing_assets;
					$this->db->where('Deployment_Package_Item_Id',$val->Deployment_Package_Item_Id);
					$this->db->where('missing_target',1);
					$this->db->where('verified',0);
					$this->db->update('deployment_package_item',$pdata);
				// }
			}
		}		
	}
	
	
	function checkIfParentsExistInPackage($assetType,$assetId,$packageId)
	{
		$verified = 0;
		$this->db->select('dpi.verified');
		$this->db->from('deployment_package_item dpi');
		$this->db->join('deployment_package_validation_list dpvl','dpi.Deployment_Package_Item_Id=dpvl.Deployment_Package_Item_Id','LEFT OUTER');
		$this->db->where('dpi.Deployment_Package_Id',$packageId);
		$this->db->where('dpvl.Asset_Type',$assetType);
		$this->db->where('dpvl.Asset_Id',$assetId);
		$query = $this->db->get();
		$result = $query->result();
		foreach($result as $key=>$val)
		{
			if($val->verified == 1)
			{
				$verified = 1;
				break;
			}
		}
		return $verified;
	}
	
	function updateVerifiedforMissingElements_old($packageId)
	{
		$this -> db -> select('*');
		$this -> db -> from('deployment_package_item dp');
		$this -> db -> where('dp.Deployment_Package_Id', $packageId);
		$query = $this -> db -> get();
	    $package_item = $query->result();
		
		$this -> db -> select('*');
		$this -> db -> from('deployment_package dp');
		$this -> db -> join('instance_ inst','dp.Source_Site_Name = inst.Site_Name', 'LEFT OUTER');
		$this -> db -> where('dp.Deployment_Package_Id', $packageId);
		// $this -> db -> where('dp.Created_By_Contact_Id', $Contact_Id);
		$this -> db -> limit(1);
		$query = $this -> db -> get();
		$org = $query->result()[0];
		
		foreach( $package_item as $key=>$val)
		{
			$this->logging_model->logT($packageId,$val->Deployment_Package_Item_Id,'updateVerifiedforMissingElements','','');
			$pdata['verified'] = $org->auto_include_missing_assets;
			$this->db->where('Deployment_Package_Item_Id',$val->Deployment_Package_Item_Id);
			$this->db->where('missing_target',1);
			$this->db->where('verified',0);
			$this->db->update('deployment_package_item',$pdata);
		}
	}
	
	
	
	function validate_queue()
    {		
		$date = date('Y-m-d H:i:s');		
		$this->db->select('*');
		$this -> db -> from('validate_queue');
		$this -> db -> where('Status','Validate In Queue');
		$this->db->order_by("Rn_Create_Date", "asc");
		$query = $this -> db -> get();
	    $validate_queue = $query->result();
		$msg="";
		foreach($validate_queue as $key=>$val)
		{
			$data1['Status'] = 'In Progress'; 
			$data1['Start_'] = date('Y-m-d H:i:s');
			try 
			{  
				$this->db->where('Validate_Queue_Id',$val->Validate_Queue_Id);
				$this->db->update('validate_queue',$data1);				
				$this->eloqua->validatepackage($val->Deployment_Package_Id);
				$msg = "success";
				$data1['Status'] = 'Processed'; 
				$data1['End_'] = date('Y-m-d H:i:s');
				$this->db->where('Validate_Queue_Id',$val->Validate_Queue_Id);
				$this->db->update('validate_queue',$data1);	
				
				$data2['Status'] = $this->decideStatusValidate($val->Deployment_Package_Id);
				$this->db->where('Deployment_Package_Id',$val->Deployment_Package_Id);
				$this->db->update('deployment_package',$data2);
			} 
			catch (Exception $e) 
			{
				$msg = "Caught exception:".$e->getMessage();
			}		
		}		
		return $msg;	
	}
	
	function decideStatusValidate($deploy_package_id)
	{
		$this->db->select('*');
		$this->db->from('deployment_package_item');
		$this->db->where('Deployment_Package_Id',$deploy_package_id);
		$query = $this->db->get();
		$totalRows = $query->num_rows();
		$errored = 0;
		$completed = 0;
		$unsupported = 0;
		$missing_assets = 0;
		$duplicates = 0;
		$action_required=0;
		if($totalRows > 0)
		{
			$result = $query->result();
			foreach($result as $key=>$val)
			{
				if($val->Status == 'Invalid')
				{
					$errored++;
				}
				else if($val->Status == 'Unsupported')
				{
					$unsupported++;
				}
				else if($val->Status == 'Valid')
				{
					$completed++;
				}
				else if($val->Status == 'Duplicate')
				{
					$duplicates++;
				}
				else if($val->Status == 'Action Required')
				{
					$action_required++;
				}
				if($val->missing_target == 1 && ( $val->verified == 0 && $this->checkIfParentsExistInPackage($val->Asset_Type,$val->Asset_Id,$val->Deployment_Package_Id)))
				{
					// print_r($this->checkIfParentsExistInPackage($val->Asset_Type,$val->Asset_Id,$val->Deployment_Package_Id));
					// print_r('<br>');
					// print_r($val->Asset_Name);
					// print_r('<br>');
					$missing_assets++;
				}
			}
		}
		if($errored>0)
		{
			return 'Invalid';
		}
		else if($unsupported>0)
		{
			return 'Unsupported';
		}
		else if($missing_assets>0)
		{
			return 'Missing Assets';
		}
		else if($duplicates>0)
		{
			return 'Duplicates Found';
		}
		else if($action_required>0)
		{
			return 'Action Required';
		}
		else
		{
			return 'Validate Completed';
		}
	}
		
	function deploy_package($packageId)
    {
		$this->db->select('Status');
		$this->db->from('deployment_package');
		$this->db->where('Status','Validate Completed');
		$this->db->where('Deployment_Package_Id',$packageId);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			$data1['Status'] = 'Deployment In Queue';
			$this->db->where('Deployment_Package_Id',$packageId);
			$this->db->update('deployment_package',$data1);
		}
		
	}
	
	function invalid_deploy_package_item($packageId)
    {
		$this->db->select('*');
		$this -> db -> from('deployment_package');
		// $this -> db -> where('Deployment_Package_Id',$packageId);
		$where = "Deployment_Package_Id = ".$packageId." AND (Status='Missing Assets' OR Status='Unsupported' OR Status='Duplicates Found')";
		$this->db->where($where);
		//$this -> db -> where('Status','Missing Assets');
		$query123 = $this -> db -> get();
	    $invalid_deploy_package_item = $query123->result();
		// //print_r('<pre>');
		// //print_r($invalid_deploy_package_item);
		if(sizeof($invalid_deploy_package_item)>0)
		{
			return false;
		}
		else
		{	
			return true;
		}	
	}
	
	function updateValidStatus($package_item_id)
	{
		$data['Status'] = 'Valid';
		$this->db->where('Deployment_Package_Item_Id',$package_item_id);
		$this->db->update('deployment_package_item',$data);
	}
	
	function updateDPValidationList($package_item,$ChildVal,$mapped)
	{
		
		$this->db->select('*');
		$this->db->from('deployment_package_validation_list');
		$this->db->where('Deployment_Package_Item_Id',$package_item->Deployment_Package_Item_Id);
		$this->db->where('Deployment_Package_Id',$package_item->Deployment_Package_Id);
		$this->db->where('Asset_Type',$ChildVal['Asset_Type']);
		$this->db->where('Asset_Id',$ChildVal['Asset_Id']);
		$this->db->where('Asset_Name',$ChildVal['Display_Name']);
		$query = $this->db->get();
		if($query->num_rows()==0)
		{
			if($mapped)
			{
				$data['Target_Asset_Id'] =  $ChildVal['Target_Asset_Id'];
				$data['Target_Asset_Name'] = $ChildVal['Target_Asset_Name'];
			}
			$data['Deployment_Package_Item_Id'] = $package_item->Deployment_Package_Item_Id;
			$data['Deployment_Package_Id'] = $package_item->Deployment_Package_Id;
			$data['Asset_Type'] = $ChildVal['Asset_Type'];
			$data['Asset_Id'] = $ChildVal['Asset_Id'];
			$data['Asset_Name'] = $ChildVal['Display_Name'];
			$data['JSON_Node_Value'] = $ChildVal['JSON_Node_Value'];
			$data['Status'] = 'New';
			if($mapped)
			{
				// print_r('<br>');
				// print_r('-------');
				// print_r($data);
				// print_r('-------');
			}
			$this->db->insert('deployment_package_validation_list',$data);
			unset($data);
		}		
	}
	
	// function identifyCircular_initiate($packageId)
	// {
		// $data['isCircular'] = 0;
		// $this->db->where('Deployment_Package_Id',$packageId);
		// $this->db->update('deployment_package_item',$data);
		
		// $this->db->select('*');
		// $this->db->from('deployment_package_item');
		// $this->db->where('Deployment_Package_Id',$packageId);
		// $query = $this->db->get();
		// if($query->num_rows()>0)
		// {
			// $result_rows = $query->result();
			// foreach($result_rows as $key=>$val)
			// {
				// $this->identifyCircular($val,$packageId);
			// }
		// }
		// //print_r('<pre>');
		// //print_r($this->childAssetCalls);
	// }
	
	function identifyCircular_initiate_updated($packageId)
	{
		//print_r('<pre>');
		// //print_r('identifyCircular_initiate_updated');
		$data['isCircular'] = 0;
		$this->db->where('Deployment_Package_Id',$packageId);
		$this->db->update('deployment_package_item',$data);
		
		$this->db->select('*');
		$this->db->from('deployment_package_item');
		$this->db->where('Deployment_Package_Id',$packageId);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			$result_rows = $query->result();
			foreach($result_rows as $key1=>$val1)
			{
				$this->visited[$val1->Deployment_Package_Item_Id] = false;
				$this->recursive[$val1->Deployment_Package_Item_Id] = false;
			}
			foreach($result_rows as $key=>$val)
			{
				$temp = $this->identifyCircular_new($val,$packageId);
				//print_r($temp);
			}
		}
	}
	
	function identifyCircular_new($val,$packageId)
	{	//print_r('hi');
		if($this->visited[$val->Deployment_Package_Item_Id] != true)
		{
			$this->visited[$val->Deployment_Package_Item_Id] = true;
			$this->recursive[$val->Deployment_Package_Item_Id] = true;
			$childAssetArray = $this->eloqua->getAssetChild($val->Deployment_Package_Id,$val->Deployment_Package_Item_Id,$val->Asset_Id,$val->Asset_Type);
			//print_r($childAssetArray);
			if((is_array($childAssetArray) ||  is_object($childAssetArray)) && sizeof($childAssetArray)>0)
			{
				if(sizeof($childAssetArray)>0)
				{
					for($i=0;$i<sizeof($childAssetArray);$i++)
					{
						$asset_type = $childAssetArray[$i]['Asset_Type'];
						$asset_id = $childAssetArray[$i]['Asset_Id'];
						$childPackageItem = $this->getPackageItem($packageId,$asset_type,$asset_id);
						if($childPackageItem!=false)
						{
							if ( !$this->visited[$childPackageItem->Deployment_Package_Item_Id] && $this->identifyCircular_new($childPackageItem,$packageId) )
							{
								
								//print_r('<br> child '.$childPackageItem->Asset_Type.' -> '.$childPackageItem->Asset_Name);
								//print_r('<br> parent '.$val->Asset_Type.' -> '.$val->Asset_Name);
								//print_r('<br>');
								//print_r('<br>');
								$data['isCircular'] = $val->Deployment_Package_Item_Id;
								$this->db->where('Deployment_Package_Item_Id',$childPackageItem->Deployment_Package_Item_Id);
								$this->db->update('deployment_package_item',$data);
									return true;
							}	
							else if ($this->recursive[$childPackageItem->Deployment_Package_Item_Id])
							{
								//print_r('<br> child '.$childPackageItem->Asset_Type.' -> '.$childPackageItem->Asset_Name);
								//print_r('<br> parent '.$val->Asset_Type.' -> '.$val->Asset_Name);
								//print_r('<br>');
								//print_r('<br>');
								
								$data['isCircular'] = $val->Deployment_Package_Item_Id;
								$this->db->where('Deployment_Package_Item_Id',$childPackageItem->Deployment_Package_Item_Id);
								$this->db->update('deployment_package_item',$data);
								return true;
							}
									
						}
					}
				}
			}
			
			
		}
		$this->recursive[$val->Deployment_Package_Item_Id] = false;  // remove the vertex from recursion stack
		return false;
	}
	
	//Identifying the circular dependency
	function identifyCircular($package_item_fixed,$packageId_fixed)
	{	
		$packageItems = array();
		$package_item = $package_item_fixed;
		while($package_item!=null )
		{
			//print_r('<pre><br>');
			//print_r('inside while '.$package_item->Asset_Type);
			$childAssetArray = $this->eloqua->getAssetChild($package_item->Deployment_Package_Id,$package_item->Deployment_Package_Item_Id,$package_item->Asset_Id,$package_item->Asset_Type);
			if((is_array($childAssetArray) ||  is_object($childAssetArray)) && sizeof($childAssetArray)>0)
			{
				if(sizeof($childAssetArray)>0)
				{
					for($i=0;$i<sizeof($childAssetArray);$i++)
					{
						$asset_type = $childAssetArray[$i]['Asset_Type'];
						$asset_id = $childAssetArray[$i]['Asset_Id'];
						$childPackageItem = $this->getPackageItem($packageId_fixed,$asset_type,$asset_id);
						if($childPackageItem!=false)
						{
							if(($childPackageItem->Asset_Type == $package_item_fixed->Asset_Type) &&($childPackageItem->Asset_Id  == $package_item_fixed->Asset_Id))
							{
								//print_r('<pre><br>');
								//print_r('matchfound');
								//print_r($package_item->Asset_Type);
								$data['isCircular'] = $package_item->Deployment_Package_Item_Id;
								$this->db->where('Deployment_Package_Item_Id',$package_item_fixed->Deployment_Package_Item_Id);
								$this->db->update('deployment_package_item',$data);
								$package_item = null;
							}
							else
							{
								if(!in_array($childPackageItem->Deployment_Package_Item_Id, $packageItems))
								{
									array_push($packageItems,$childPackageItem->Deployment_Package_Item_Id);
									$package_item = $childPackageItem;
								}
								else
								{
									$package_item = null;
								}
								
							}
							
						}
						else
						{
							$package_item = null;
						}
					}
				}
				else
				{
					$package_item = null;
				}
			}
			else
			{
				$package_item = null;
			}
		}
	
	}
	
	function checkForMapping($packageId, $parentAssetType, $childAssetType, $childAssetId)
	{
		$this->db->select('Source_Site_Name,Target_Site_Name');
		$this->db->from('deployment_package');
		$this->db->where('Deployment_Package_Id',$packageId);
		$packageQuery = $this->db->get();
		$packageResult = $packageQuery->result();
		
		$this->db->select('Target_Asset_Id,Target_Asset_Name');
		$this->db->from('source_target_mapping');
		$this->db->where('Child_Asset_Type',$childAssetType);
		$this->db->where('Source_Asset_Id',$childAssetId);
		$this->db->where('Source_Site_Name',$packageResult[0]->Source_Site_Name);
		$this->db->where('Target_Site_Name',$packageResult[0]->Target_Site_Name);
		$query = $this->db->get();
		
		// echo '<br>targetAsset<br>';
			// print_r($query);
		// echo '<br>';
		//$targetAsset=false;
		if($query->num_rows()>0)
		{
			$result = $query->result_array();
			// print_r($result);
			// echo '<br>';
			if($result[0]['Target_Asset_Id']==-1)
			{
				if(($parentAssetType=='Form' ||  $parentAssetType=='Segment' ||  $parentAssetType=='Campaign' ||  $parentAssetType=='Program' ||  $parentAssetType=='Shared Filter' ||  $parentAssetType=='Contact View' ||  $parentAssetType=='Dynamic Content')&& $childAssetType=='Contact Field' && $result[0]['Target_Asset_Id']==-1)
				{
					$targetAsset['Asset_Id'] = $result[0]['Target_Asset_Id'];
					$targetAsset['Asset_Name'] = $result[0]['Target_Asset_Name'];
				}
				else if(($parentAssetType=='Shared Filter' ||  $parentAssetType=='Segment' || $parentAssetType=='Form')&& $childAssetType=='Picklist' && $result[0]['Target_Asset_Id']==-1)
				{
					$targetAsset['Asset_Id'] = $result[0]['Target_Asset_Id'];
					$targetAsset['Asset_Name'] = $result[0]['Target_Asset_Name'];
				}
				else if(($parentAssetType=='Shared Filter' ||  $parentAssetType=='Segment' || $parentAssetType=='Campaign')&& $childAssetType=='Campaign Field' && $result[0]['Target_Asset_Id']==-1)
				{
					$targetAsset['Asset_Id'] = $result[0]['Target_Asset_Id'];
					$targetAsset['Asset_Name'] = $result[0]['Target_Asset_Name'];
				}
				else
				{
					return false;
				}
			}
			else
			{
				$targetAsset['Asset_Id'] = $result[0]['Target_Asset_Id'];
				$targetAsset['Asset_Name'] = $result[0]['Target_Asset_Name'];
			}			
			// $targetAsset['Asset_Id'] = $result[0]['Target_Asset_Id'];
			// $targetAsset['Asset_Name'] = $result[0]['Target_Asset_Name'];
		}	
		else
		{
			return false;
		}	
		// print_r($targetAsset);
		return $targetAsset;
	}
	
	function buildGlobalStack($package_item,$packageId)
	{
		// echo '<pre>Deployment_Package_Item_Id<br>';
			// print_r($package_item->Deployment_Package_Item_Id);
		// echo '<pre><br>';

		
		$this->logging_model->logT($packageId,$package_item->Deployment_Package_Item_Id,'buildGlobalStack','building a recursive stack','');
		$this->putOnGlobalStack($package_item->Deployment_Package_Item_Id);
		$this->updateValidStatus($package_item->Deployment_Package_Item_Id);
		
		//$ShellFlag=$this->deploy_helper_model->getShellJSONFlag($package_item->Deployment_Package_Item_Id);
		
		$childAssets = $this->eloqua->getAssetChild($package_item->Deployment_Package_Id,$package_item->Deployment_Package_Item_Id,$package_item->Asset_Id,$package_item->Asset_Type);
		
		 // echo '<pre>childAssets<br>';
		 // print_r($childAssets);
		 // echo '<pre><br>';
		
		array_push($this->childAssetCalled,$package_item->Deployment_Package_Item_Id);
		if(is_array($childAssets) || is_object($childAssets))
		{
			foreach($childAssets as $ChildKey=>$ChildVal)
			{
				//print_r('Hello');
				$targetAsset = $this->checkForMapping($packageId,$package_item->Asset_Type,$ChildVal['Asset_Type'], $ChildVal['Asset_Id']);
				
				// echo '<pre>targetAsset24<br>';
					// print_r($targetAsset);
				// echo '<pre><br>';
				
				if($targetAsset == false)
				{
					// echo '<pre>targetAsset24 false<br>';
					$this->updateDPValidationList($package_item,$ChildVal,false);
				}
				else
				{
					// print_r('<br>else<br>');
					// print_r();
					$ChildVal['Target_Asset_Id'] = $targetAsset['Asset_Id'];
					$ChildVal['Target_Asset_Name'] = $targetAsset['Asset_Name'];
					// print_r($ChildVal);
					$this->updateDPValidationList($package_item,$ChildVal,true);
				}
			}
		}
		if((is_array($childAssets) ||  is_object($childAssets)) && sizeof($childAssets)>0)
		{
			$childAssetArray = $childAssets;
			if(sizeof($childAssetArray)>0)
			{
				for($i=0;$i<sizeof($childAssetArray);$i++)
				{
					/*This is for taking backup*/
					$asset_type = $childAssetArray[$i]['Asset_Type'];
					$asset_id = $childAssetArray[$i]['Asset_Id'];
					
					$targetAsset = $this->checkForMapping($packageId,$package_item->Asset_Type,$asset_type, $asset_id);
					
					
					$targetAssetExists = $this->deploy_helper_model->CheckAssetInTarget($packageId,$asset_type,$childAssetArray[$i]['Display_Name']);
					
					$minimalJSONExitForParentChild = $this->deploy_helper_model->getMinimalJSONForParenetChild($package_item->Asset_Type,$asset_type);
					
					$enableJSONShellFlag= $this->deploy_helper_model->getEnableShellForJSONStatus($packageId);				
					
					$checkAssetCreatedUsingShell = $this->deploy_helper_model->checkAssetCreatedUsingShell($asset_id,$asset_type,$childAssetArray[$i]['Display_Name'],$packageId);
					
					
					
					if($targetAsset == false)
					{
						if($enableJSONShellFlag==1 && $targetAssetExists==0 && isset($minimalJSONExitForParentChild))
						{	
							$originalJSON=$this->deploy_helper_model->getSourceJSONById($asset_id,$asset_type,$packageId);
							$modifiedShellJSON = $this->deploy_helper_model->replaceIdNameFolderInEmptyShell($minimalJSONExitForParentChild,json_encode($originalJSON));
							$pdata['Asset_Id'] = $asset_id;
							$pdata['Asset_Name'] = $childAssetArray[$i]['Display_Name'];
							$pdata['Asset_Type'] = $asset_type;
							$pdata['User_Added'] = 0;
							$pdata['From_Shell'] = 1;
							$pdata['Status'] = 'Valid';
							$pdata['JSON_Asset'] = $modifiedShellJSON;
							$pdata['Deployment_Package_Id'] = $packageId;
							if($asset_type=='Landing Page')
							{
								$default_targetMicrosite=$this->changeset->getDefaultTargetMicrosite($packageId);
								$pdata['Target_Microsite_JSON'] = json_encode($default_targetMicrosite);
							}
							//print_r($pdata);
							$this->changeset->addtopackage(0,$pdata);
							$childPackageItem = $this->getPackageItem($packageId,$asset_type,$asset_id);
							$this->putOnGlobalStack($childPackageItem->Deployment_Package_Item_Id);
							$this->updateValidStatus($childPackageItem->Deployment_Package_Item_Id);	
						}					
						else
						{					
							if(($checkAssetCreatedUsingShell==1 || $targetAssetExists==1)&& $enableJSONShellFlag==1 && isset($minimalJSONExitForParentChild))
							{
								$originalJSON=$this->deploy_helper_model->getSourceJSONById($asset_id,$asset_type,$packageId);
								$modifiedShellJSON = $this->deploy_helper_model->replaceIdNameFolderInEmptyShell($minimalJSONExitForParentChild,json_encode($originalJSON));
								$pdata['Asset_Id'] = $asset_id;
								$pdata['Asset_Name'] = $childAssetArray[$i]['Display_Name'];
								$pdata['Asset_Type'] = $asset_type;
								$pdata['User_Added'] = 0;
								$pdata['From_Shell'] = 1;
								$pdata['Status'] = 'Valid';
								$pdata['JSON_Asset'] = $modifiedShellJSON;
								$pdata['Deployment_Package_Id'] = $packageId;
								if($asset_type=='Landing Page')
								{
									$default_targetMicrosite=$this->changeset->getDefaultTargetMicrosite($packageId);
									$pdata['Target_Microsite_JSON'] = json_encode($default_targetMicrosite);
								}
								//print_r($pdata);
								$this->changeset->addtopackage(0,$pdata);
								$childPackageItem = $this->getPackageItem($packageId,$asset_type,$asset_id);
								$this->putOnGlobalStack($childPackageItem->Deployment_Package_Item_Id);
								$this->updateValidStatus($childPackageItem->Deployment_Package_Item_Id);
							}
							else
							{
								$childPackageItem = $this->getPackageItem($packageId,$asset_type,$asset_id);
																
								if($childPackageItem!=false)
								{
									// echo '<pre>childAssetCalled<br>';
										// print_r($this->childAssetCalled);
									// echo '<pre><br>';
									
									$isPresent = array_search($childPackageItem->Deployment_Package_Item_Id, $this->childAssetCalled);
									
									// echo '<pre>isPresent<br>';
										// print_r($isPresent);
									// echo '<pre><br>';
									
									if($isPresent  !== FALSE)
									{
										$this->PushAssetChildAssetsOnGlobalStack($childPackageItem->Deployment_Package_Item_Id,true);
									}
									else
									{
										// if(!$this->checkAssetInTarget($packageId,$asset_type,$childAssetArray[$i]['Display_Name']))
										// {
											$this->buildGlobalStack($childPackageItem,$packageId);
										// }
										
									}
								}
								else
								{
									//echo '<br>new code<br>';
									$pdata['Asset_Id'] = $asset_id;
									$pdata['Asset_Name'] = $childAssetArray[$i]['Display_Name'];
									$pdata['Asset_Type'] = $asset_type;
									$pdata['User_Added'] = 0;
									$pdata['From_Shell'] = 0;
									$pdata['Deployment_Package_Id'] = $packageId;
					
									$this->changeset->addtopackage(0,$pdata);
									$childPackageItem = $this->getPackageItem($packageId,$asset_type,$asset_id);
									// if(!$this->checkAssetInTarget($packageId,$asset_type,$childAssetArray[$i]['Display_Name']))
									// {
										$this->buildGlobalStack($childPackageItem,$packageId);
									// }
								}
							}
						}
					}
					// else
					// {
						// echo '<pre><br>';						
						// echo 'Empty Shell : ';
						// print_r($asset_type);
						// echo '<br>';
						// $originalJSON=$this->deploy_helper_model->getSourceJSONById($asset_id,$asset_type,$packageId);
						// $modifiedShellJSON = $this->deploy_helper_model->replaceIdNameFolderInEmptyShell($minimalJSONExitForParentChild,json_encode($originalJSON));
						// $pdata['Asset_Id'] = $asset_id;
						// $pdata['Asset_Name'] = $childAssetArray[$i]['Display_Name'];
						// $pdata['Asset_Type'] = $asset_type;
						// $pdata['User_Added'] = 0;
						// $pdata['From_Shell'] = 1;
						// $pdata['Status'] = 'Valid';
						// $pdata['JSON_Asset'] = $modifiedShellJSON;
						// $pdata['Deployment_Package_Id'] = $packageId;
						// if($asset_type=='Landing Page')
						// {
							// $default_targetMicrosite=$this->changeset->getDefaultTargetMicrosite($packageId);
							// $pdata['Target_Microsite_JSON'] = json_encode($default_targetMicrosite);
						// }
						// print_r($pdata);
						// $this->changeset->addtopackage(0,$pdata);
						// $childPackageItem = $this->getPackageItem($packageId,$asset_type,$asset_id);
						// $this->putOnGlobalStack($childPackageItem->Deployment_Package_Item_Id);
						// $this->updateValidStatus($childPackageItem->Deployment_Package_Item_Id);				
					// }
					/*Please dont remove these Commented code files */
					// $asset_type = $childAssetArray[$i]['Asset_Type'];
					// $asset_id = $childAssetArray[$i]['Asset_Id'];
					// $childPackageItem = $this->getPackageItem($packageId,$asset_type,$asset_id);
					// if($childPackageItem!=false)
					// {
						// $this->buildGlobalStack($childPackageItem,$packageId);
					// }
					// else
					// {
						// $pdata['Asset_Id'] = $asset_id;
						// $pdata['Asset_Name'] = $childAssetArray[$i]['Display_Name'];
						// $pdata['Asset_Type'] = $asset_type;
						// $pdata['Deployment_Package_Id'] = $packageId;
						// $this->changeset->addtopackage(0,$pdata);
						// $childPackageItem = $this->getPackageItem($packageId,$asset_type,$asset_id);
						// $this->buildGlobalStack($childPackageItem,$packageId);
					// }
				}
			}
		}
		//exit;
	}
	
	function PushAssetChildAssetsOnGlobalStack($Deployment_Package_Item_Id,$mainCall)
	{
		// //print_r('<pre>');
		// //print_r($Deployment_Package_Item_Id);
		if($mainCall)
		{
		$this->putOnGlobalStack($Deployment_Package_Item_Id);
		}

		$this->db->select('*');
		$this->db->from('deployment_package_validation_list');
		$this->db->where('Deployment_Package_Item_Id',$Deployment_Package_Item_Id);
		$query = $this->db->get();
		// //print_r('---');
		// //print_r($query->num_rows());
		if($query->num_rows()>0)
		{
			$packages = array();
			$result = $query->result();
			////print_r($result);
			foreach($result as $key=>$value)
			{
				////print_r($value);
				$package = $this->getPackageItem($value->Deployment_Package_Id,$value->Asset_Type,$value->Asset_Id);
				// //print_r($package);
				if($package != false)
				{
					//array_push($packages, $package)
					////print_r($package->Deployment_Package_Item_Id);
					////print_r('<br>');
					$this->putOnGlobalStack($package->Deployment_Package_Item_Id);
					$this->PushAssetChildAssetsOnGlobalStack($package->Deployment_Package_Item_Id,false);
				} 
			}
		}
	}
 
	function PushAssetChildAssetsOnGlobalStack_old($Deployment_Package_Item_Id)
	{
		//print_r('<pre>');
		//print_r($Deployment_Package_Item_Id);
		$this->putOnGlobalStack($Deployment_Package_Item_Id);
		$this->db->select('*');
		$this->db->from('deployment_package_validation_list');
		$this->db->where('Deployment_Package_Item_Id',$Deployment_Package_Item_Id);
		$query = $this->db->get();
		//print_r('---');
		//print_r($query->num_rows());
		if($query->num_rows()>0)
		{
			$packages = array();
			$result = $query->result();
			//print_r($result);
			foreach($result as $key=>$value)
			{
				//print_r($value);
				$package = $this->getPackageItem($value->Deployment_Package_Id,$value->Asset_Type,$value->Asset_Id);
				//print_r($package);
				if($package != false)
				{
					//array_push($packages, $package)
					//print_r($package->Deployment_Package_Item_Id);
					//print_r('<br>');
					$this->putOnGlobalStack($package->Deployment_Package_Item_Id);
				}	
			}
		}
		
		
		
	}
	
	// function getMinElementFromGlobalStack($packages)
	// {
		// $min = 99999;
		// foreach($packages as $key=>$value)
		// {
			// $globalkey = array_search($value->Deployment_Package_Item_Id,$this->globalStack)
			// if($min>$globalkey)
			// {
				// $min = $globalkey;
			// }
		// }
		// return $this->globalStack[$min];
	// }
	
	function getPackageItem($packageId,$AssetType,$AssetId)
	{
		$this->db->select('*');
		$this -> db -> from('deployment_package_item');
		$this -> db -> where('Deployment_Package_Id',$packageId);
		$this->db->where('Asset_Type',$AssetType);
		$this->db->where('Asset_Id',$AssetId);
		$query = $this -> db -> get();
		$deployment_package_items = $query->result();
		if($query->num_rows()>0)
		{
			return $deployment_package_items[0];
		}
		return false;
	}
	
	function putOnGlobalStack($currentAsset)
	{
		$isPresent = array_search($currentAsset, $this->globalStack);
		if($isPresent  !== FALSE)
		{
			unset($this->globalStack[array_search($currentAsset, $this->globalStack)]);
		}
		array_push($this->globalStack,$currentAsset);
	}
	
	function isAssetChildCalled($currentAsset)
	{
		$isPresent = array_search($currentAsset,$this->childAssetCalls);
		if($isPresent !== FALSE)
		{
			// //print_r('<pre><br>');
			// //print_r('already there'.$currentAsset);
			// //print_r('<br>');
			array_push($this->childAssetCalls,$currentAsset);
			return 1;
		}
		else
		{
			array_push($this->childAssetCalls,$currentAsset);
			return 0;
		}
	}
	
	function pre_deploy_package_item($packageId,$queueId)
	{
		$this->logging_model->logT($packageId,0,'pre_deploy_package_item','start','');
		$invalid_package_item = $this->invalid_deploy_package_item($packageId);
		$status_details = '';
		
		if($invalid_package_item)
		{
			$getsearch_endpoints = $this->getalleloqua_endpoints();	
			$package = $this->getPackage($packageId);
			$instance = $this->getInstance($package[0]->Target_Site_Name);
					
			$this->db->select('*');
			$this -> db -> from('deployment_package_item');
			$this -> db -> where('Deployment_Package_Id',$packageId);
			$this -> db -> where('verified',1);
			// $this -> db -> where('New_JSON_Asset','');
			$this->db->order_by('Deploy_Ordinal', 'desc');
			$query123 = $this -> db -> get();
			$deployment_package_item = $query123->result();
			
			// echo '<br>deployment_package_item<br><pre>';
				// print_r($deployment_package_item);
			// echo '</pre><br>';
			
			$valid_err = false;
			////print_r('<pre>');
			$package_item_arr = array();
			foreach($deployment_package_item as $key=>$val)
			{	
				// echo '<br>val parent<br><pre>';
					// print_r($val);
				// echo '</pre><br>';
				if(sizeof($instance)>0 )
				{
					$this->executionCounter=1;
					$this->pre_deploy_package_item_branchOut($val,$instance,$getsearch_endpoints,$packageId);
				}
			}			
			$data['Status'] = $this->decideStatus($packageId);
			$this->db->where('Deployment_Package_Id',$packageId);
			$this->db->update('deployment_package',$data);
			
			$dqdata1['Status'] = 'Processed';
			$dqdata1['End_'] = date('Y-m-d H:i:s');
			$this->db->where('Deployment_Queue_Id',$queueId);
			$this->db->update('deployment_queue',$dqdata1);
		}
		else
		{
			$data1['Status'] = 'Processed � Error';
			$data1['End_'] = date('Y-m-d H:i:s');
			$this->db->where('Deployment_Queue_Id',$queueId);
			$this->db->update('deployment_queue',$data1);			
		}
	}
	
	// STRAT - Function to perform pre-deploy conditions -Anamika Singh
	public function pre_deploy_package_item_branchOut($val,$instance,$getsearch_endpoints,$packageId)
	{		
		if($val->Asset_Type != 'Image' && $val->Asset_Type != 'File Storage')
		{
			if($val->JSON_Asset!=null)
			{
				$JSON_Asset = json_decode($val->JSON_Asset);
				
				$this->logging_model->logT($packageId,$val->Deployment_Package_Item_Id,'pre_deploy_package_item','json modificaton start',$JSON_Asset);
								
				if($val->Asset_Type=='Hyperlink' && $JSON_Asset->hyperlinkType=='LandingPageURL')
				{					
					//$result = $this->eloqua->get_request($instance[0]->Token, $instance[0]->Base_Url.''.$getsearch_endpoints[str_replace(' ', '', $val->Asset_Type).'_FindAsset']->Endpoint_URL.'?search="*'.urlencode(strstr($JSON_Asset->name,'(',true)).'*"&depth=complete');
					
					$assetNameHyperlink = $JSON_Asset->name;
					$assetNameHyperlink_temp=substr($assetNameHyperlink, 0, strrpos($assetNameHyperlink, '('));
					
					//changes to asset name for special characters
					if(preg_match($this->pattern,  $assetNameHyperlink_temp))
					{
						$tempAssetName = str_replace('-', '_', $assetNameHyperlink_temp);
						$tempAssetName = preg_replace('/[^A-Za-z0-9\-]/','*',$tempAssetName);
					}
					else
						$tempAssetName=urlencode($assetNameHyperlink_temp);
					
					$result = $this->eloqua->get_request($instance[0]->Token, $instance[0]->Base_Url.''.$getsearch_endpoints[str_replace(' ', '', $val->Asset_Type).'_FindAsset']->Endpoint_URL.'?search="*'.$tempAssetName.'*"&depth=complete');
					
					// print_r($assetNameHyperlink_temp);
					
					
					
					$result_temp=json_decode($result['data']);					
					$assetArray = array();
					$assetCount = 0;
					foreach($result_temp->elements as $rtKey=>$rtVal)
					{
						if($rtVal->name==$assetNameHyperlink_temp)
						{
							  array_push($assetArray,$rtVal);
							  $assetCount++;
						}
					}					
					$result_temp->elements=$assetArray;
					$result_temp->total=$assetCount; 
					
					// echo '<pre>';				
						// print_r($result_temp);
					// echo '<pre>';
					
					if($result_temp->total >=1)
					{
						if(isset($JSON_Asset->referencedEntityId) && $JSON_Asset->hyperlinkType=='LandingPageURL')
						{
							$sourceLPName= $this->deploy_helper_model->getSourceAssetNameById($JSON_Asset->referencedEntityId,'Landing Page',$val->Deployment_Package_Id);
						
							$targetLPId=$this->deploy_helper_model->getTargetAssetIdBySourceAssetName($sourceLPName,'Landing Page',$val->Deployment_Package_Id);
							$matchingLPFound = false;
							
							foreach($result_temp->elements as $LPIDKey=>$LPIDVal)
							{
								if(isset($LPIDVal->referencedEntityId) && $JSON_Asset->hyperlinkType=='LandingPageURL')
								{
									if($LPIDVal->referencedEntityId==$targetLPId)
									{
										unset ($result_temp->elements);
										$result_temp->elements[0]=$LPIDVal;
										$result_temp->total=1;
										$matchingLPFound = true;
										break;
									}
								}		
							}
							if($matchingLPFound == true)
							{
								$result['data']=json_encode($result_temp);
							}
							else
							{								
								unset ($result_temp->elements);
								$result_temp->total=0;
								$result['data']=json_encode($result_temp);
							}															
						}
						else
						{
							$result=json_encode($result_temp);
						}
					}
					else
					{	
						$result=json_encode($result_temp);
					}	
				}
				else
				{
					//changes to asset name for special characters
					if(preg_match($this->pattern,  $JSON_Asset->name))
					{
						$tempAssetName = str_replace('-', '_', $JSON_Asset->name);
						$tempAssetName = preg_replace('/[^A-Za-z0-9\-]/','*',$tempAssetName);
					}
					else
						$tempAssetName=urlencode($JSON_Asset->name);
						
					$result = $this->eloqua->get_request($instance[0]->Token, $instance[0]->Base_Url.''.$getsearch_endpoints[str_replace(' ', '', $val->Asset_Type).'_FindAsset']->Endpoint_URL.'?search="'.$tempAssetName.'"');
				}
				//print_r($result);	
				if(isset($result['data']))
				{
					$result_decode = json_decode($result['data']);
				
				}
				else{
					$result_decode = json_decode($result);
				}
				// echo '<br>result_decode before check <br><pre>';
					// print_r($result_decode);
				// echo '</pre><br>';
				
				$assetArray = array();
				$assetCount = 0;
				foreach($result_decode->elements as $rdKey=>$rdVal)
				{
					if($rdVal->name==$JSON_Asset->name)
					{
						  array_push($assetArray,$rdVal);
						  $assetCount++;
					}
				}
					
			  $result_decode->elements=$assetArray;
			  $result_decode->total=$assetCount; 
			  
				// echo '<br>result_decode after check<br><pre>';
					// print_r($result_decode);
				// echo '</pre><br>';
				// echo '<br>result_decode total<br><pre>';
					// print_r($result_decode->total);
				// echo '</pre><br>';
				
				$xJsonIdReplace = null;
				
				if(isset($result_decode->total))
				{
					if($result_decode->total >0)
					{	
						$JSON_Asset = $this->getExclusionResult(json_decode($getsearch_endpoints[str_replace(' ', '', $val->Asset_Type).'_Update']->JSON_Body_Exclusion),$JSON_Asset);
						$this->logging_model->logT($packageId,$val->Deployment_Package_Item_Id,'pre_deploy_package_item','After getExclusionResult Update',$JSON_Asset);
						
						$JSON_Asset = $this->replaceJSONKey(json_decode($getsearch_endpoints[str_replace(' ', '', $val->Asset_Type).'_Update']->JSON_Key_Replace),$JSON_Asset);
						$this->logging_model->logT($packageId,$val->Deployment_Package_Item_Id,'pre_deploy_package_item','After replaceJSONKey Update',$JSON_Asset);
						
						$JSON_Asset = $this->ChangeParaValue($JSON_Asset,$getsearch_endpoints[str_replace(' ', '', $val->Asset_Type).'_Update']->Change_Para_Config);
						$this->logging_model->logT($packageId,$val->Deployment_Package_Item_Id,'pre_deploy_package_item','After ChangeParaValue Update',$JSON_Asset);
						
						// echo '<pre>';
							// print_r($JSON_Asset);
						// echo '</pre>';
						if(is_array($JSON_Asset))
						{	
							foreach($JSON_Asset as $key1=>$val1)
							{
								if($key1=='id')
								{
									if($result_decode->total > 1)
									{
										$xJsonIdReplace[$key1]=$val->Target_Asset_Id;		
									}
									else
									{								
										$xJsonIdReplace[$key1]=$result_decode->elements[0]->id;
										
										// foreach($result_decode->elements as $rKey=>$rVal)
										// {
											// if($rVal->name==$JSON_Asset->name)
											// {
												// $xJsonIdReplace[$key1]=$rVal->id;
												// break;
											// }
										// }
									}															
								}
								else
								{
									$xJsonIdReplace[$key1]=$val1;
								}								
							}  
						}
						$data1['JSON_Submitted'] = json_encode($xJsonIdReplace);
						if($result_decode->total > 1)
						{
							$data1['Target_Asset_Id'] = $val->Target_Asset_Id;
						}
						else
						{
							$data1['Target_Asset_Id'] = $result_decode->elements[0]->id;
						}
					}
					else
					{
						$JSON_Asset = $this->getExclusionResult(json_decode($getsearch_endpoints[str_replace(' ', '', $val->Asset_Type).'_Create']->JSON_Body_Exclusion),$JSON_Asset);
						$this->logging_model->logT($packageId,$val->Deployment_Package_Item_Id,'pre_deploy_package_item','After getExclusionResult Create',$JSON_Asset);
					
						$JSON_Asset = $this->replaceJSONKey(json_decode($getsearch_endpoints[str_replace(' ', '', $val->Asset_Type).'_Create']->JSON_Key_Replace),$JSON_Asset);
						$this->logging_model->logT($packageId,$val->Deployment_Package_Item_Id,'pre_deploy_package_item','After replaceJSONKey Create',$JSON_Asset);
						
						$JSON_Asset = $this->ChangeParaValue($JSON_Asset,$getsearch_endpoints[str_replace(' ', '', $val->Asset_Type).'_Create']->Change_Para_Config);
						$this->logging_model->logT($packageId,$val->Deployment_Package_Item_Id,'pre_deploy_package_item','After ChangeParaValue Create',$JSON_Asset);		
						
						
						$data1['JSON_Submitted'] = json_encode($JSON_Asset);
						$data1['Target_Asset_Id'] = 0;
					}
				}
				else
				{
					$JSON_Asset = $this->getExclusionResult(json_decode($getsearch_endpoints[str_replace(' ', '', $val->Asset_Type).'_Create']->JSON_Body_Exclusion),$JSON_Asset);
					$this->logging_model->logT($packageId,$val->Deployment_Package_Item_Id,'pre_deploy_package_item','After getExclusionResult Create_Else',$JSON_Asset);
					
					$JSON_Asset = $this->replaceJSONKey(json_decode($getsearch_endpoints[str_replace(' ', '', $val->Asset_Type).'_Create']->JSON_Key_Replace),$JSON_Asset);
					$this->logging_model->logT($packageId,$val->Deployment_Package_Item_Id,'pre_deploy_package_item','After replaceJSONKey Create_Else',$JSON_Asset);
					
					$JSON_Asset = $this->ChangeParaValue($JSON_Asset,$getsearch_endpoints[str_replace(' ', '', $val->Asset_Type).'_Create']->Change_Para_Config);
					$this->logging_model->logT($packageId,$val->Deployment_Package_Item_Id,'pre_deploy_package_item','After ChangeParaValue Create_Else',$JSON_Asset);
					
					
					
					$data1['JSON_Submitted'] = json_encode($JSON_Asset);
					$data1['Target_Asset_Id'] = 0;					
				}

				$data1['JSON_Submitted'] = $this->replaceSiteName_image($data1['JSON_Submitted'],$packageId);
				$data1['JSON_Submitted'] = $this->replaceSiteName_files($data1['JSON_Submitted'],$packageId);

				if(is_array($data1['JSON_Submitted']) || is_object($data1['JSON_Submitted']))
				{
					$data1['JSON_Submitted'] = json_encode($data1['JSON_Submitted']);
				}
				
				$this->db->where('Deployment_Package_Item_Id',$val->Deployment_Package_Item_Id);
				$this->db->update('deployment_package_item',$data1);
				
				$val->Target_Asset_Id = $data1['Target_Asset_Id'];
				$val->JSON_Submitted = $data1['JSON_Submitted'];
				
				if($val->Asset_Name=='Event Email Header')
				{
					// echo 'here<pre>';
					// print_r($data1['JSON_Submitted']);
				}		
				$this->getDeploymentPackageValidation($val,$data1['JSON_Submitted'],$instance,$getsearch_endpoints[str_replace(' ', '', $val->Asset_Type).'_Update']->Endpoint_URL);
			}
		}
		else
		{
			//add only verified elements
			if($val->verified == 1 && $val->isCircular!=-1)
			{
				if($val->Asset_Type == 'File Storage')
				{					
					$this->post_files($val);
				}
				else 
				{
					$this->post_image($val);
				}
			}						
		}
	}
	// END - Function to perform pre-deploy conditions - Anamika Singh
	
	public function replaceSiteName_image_copy($submitted_JSON,$packageId)
	{
		$submitted_JSON1='';
		$submitted_JSON=json_decode($submitted_JSON);
	
		if(isset($submitted_JSON->images) && (!empty($submitted_JSON->images)))
		{	
			$target_Images = $this->searchAssets_targetInstance($packageId,$Asset_Type='Image');
			$submitted_JSON1= $submitted_JSON->images;
			foreach($submitted_JSON1 as $key=>$val)
			{
				foreach($target_Images->elements as $key1=>$val1)
				{
					if($val->name == $val1->name)
					{
						deploy_model::$source_url = $val->fullImageUrl;
						deploy_model::$target_url = $val1->fullImageUrl;
						
						if(isset($val1->thumbnailUrl)&&isset($val->thumbnailUrl))
						{
							$target_thumbnail_url = $val1->thumbnailUrl;
							$source_thumbnail_url = $val->thumbnailUrl;
							$val->thumbnailUrl = str_replace($source_thumbnail_url,$target_thumbnail_url,$val->thumbnailUrl);
						}
						
						$newArray = json_decode(json_encode($submitted_JSON),true);
						
						if(is_array($newArray))
						{
							array_walk_recursive($newArray,'deploy_model::test_print');
							$newArray = $this->getImageUrlsReplaced($newArray);
							$submitted_JSON = json_encode($newArray);
							$newArray_temp=json_decode($submitted_JSON );
							$image_url_replace=$newArray_temp->images;
							foreach($image_url_replace as $imgKey=>$imgVal)
							{
								$imgVal->thumbnailUrl=$val->thumbnailUrl;
								$imgVal->fullImageUrl=deploy_model::$target_url;
							}
							$newArray_temp->images=$image_url_replace;
							$submitted_JSON = json_encode($newArray_temp);
						}	
						else
						{	
							$newArray_temp=json_decode($newArray);
							$image_url_replace=$newArray_temp->images;
							foreach($image_url_replace as $imgKey=>$imgVal)
							{
								$imgVal->thumbnailUrl=$val->thumbnailUrl;
								$imgVal->fullImageUrl=deploy_model::$target_url;
							}
							$newArray_temp->images=$image_url_replace;
							$submitted_JSON = json_encode($newArray_temp);
						}	
					}					
					
				}
				
			}
		}
		else
		{
			$submitted_JSON = json_encode($submitted_JSON);
		}
		return $submitted_JSON;
	}
	
	function getSiteIdBySiteName($siteName){
		$this -> db -> select('Site_Id');
		$this -> db -> from('instance_');
		$this -> db -> where('Site_Name', $siteName);
		$query = $this -> db -> get();		
		$siteId = $query->result()[0]->Site_Id;
		return $siteId;
	}
	
	public function replaceSiteName_image($submitted_JSON,$packageId)
	{
		$submitted_JSON2=$submitted_JSON;
		$submitted_JSON=json_decode($submitted_JSON,true);
		
		$Package_details = $this->deploy_model->getPackage($packageId);
		$SourceSiteId=$this->getSiteIdBySiteName($Package_details[0]->Source_Site_Name);
		$targetSiteId=$this->getSiteIdBySiteName($Package_details[0]->Target_Site_Name);			
		
		$getSourceImageBaseURL  = $this->settings_model->getImageURL($SourceSiteId);
		$getTargetImageBaseURL  = $this->settings_model->getImageURL($targetSiteId);
		
		// echo '<pre>submitted_JSON x <br>';
		// print_r($submitted_JSON);
		// echo '<pre><br>';
		
		if(isset($submitted_JSON['images']) && (!empty($submitted_JSON['images'])))
		{	
			$target_Images = $this->searchAssets_targetInstance($packageId,$Asset_Type='Image');
			$target_Images = json_decode(json_encode($target_Images),true);
			$submitted_JSON1= $submitted_JSON['images'];			
			
			foreach($submitted_JSON1 as $key=>$val)
			{
				$IsAssetPresentInTheMapping = $this->deploy_helper_model->IsAssetPresentInTheMapping('Image',$packageId,$val['id']);
				
					
				// echo '<pre>val x <br>';
					// print_r($val);
				// echo '<pre><br>';
				
				// $val = $this->deploy_helper_model->updateJsonNameForPost($val);
				// $val = array($val);
				
				// echo '<pre>val y <br>';
					// print_r($val);
				// echo '<pre><br>';
				
				if(!$IsAssetPresentInTheMapping)
				{
					foreach($target_Images['elements'] as $key1=>$val1)
					{	
						$imageName = $val['name'];
						// $imageNamePattern = '/[&*#!$%+^",]/';
						// if(preg_match($imageNamePattern,  $imageName))
						// {
							// $imageName = str_replace('amp;','&',$imageName);
							// $imageName= preg_replace('/[&*#!$%+^�]/','_',$val->imageName);
						// }
						
						// echo '<pre>imageName old <br>';
							// print_r($imageName);
						// echo '<pre><br>';
						$imageName = $this->deploy_helper_model->updateJsonNameInParent($val);
						// echo '<pre>imageName new <br>';
							// print_r($imageName);
						// echo '<pre><br>';
		
						if($imageName == $val1['name'])
						{
							// echo '<pre>';
						
							// print_r($val1);
						
							deploy_model::$source_url = $val['fullImageUrl'];
							deploy_model::$target_url = $val1['fullImageUrl'];
							$submitted_JSON = $this->getImageUrlsReplaced($submitted_JSON,$val['thumbnailUrl'],$val1['thumbnailUrl']);
							$submitted_JSON = $this->getImageUrlsReplaced($submitted_JSON,$val['fullImageUrl'],$val1['fullImageUrl']);
						}			
					}
				}			
			}			
			if(isset($getSourceImageBaseURL) && isset($getTargetImageBaseURL))
			{
				$submitted_JSON  = $this->getImageUrlsReplaced($submitted_JSON,$getSourceImageBaseURL,$getTargetImageBaseURL);
			}
		}		
		$submitted_JSON = json_encode($submitted_JSON);
		return $submitted_JSON;
	}
	
	public function replaceSiteName_files($submitted_JSON,$packageId)
	{
		$submitted_JSON2=$submitted_JSON;
		$submitted_JSON=json_decode($submitted_JSON,true);
		
		$Package_details = $this->deploy_model->getPackage($packageId);
		$SourceSiteId=$this->getSiteIdBySiteName($Package_details[0]->Source_Site_Name);
		$targetSiteId=$this->getSiteIdBySiteName($Package_details[0]->Target_Site_Name);			
		
		$getSourceImageBaseURL  = $this->settings_model->getImageURL($SourceSiteId);
		$getTargetImageBaseURL  = $this->settings_model->getImageURL($targetSiteId);
		
		
		
		if(isset($submitted_JSON['files']) && (!empty($submitted_JSON['files'])))
		{	
			$target_Images = $this->searchAssets_targetInstance($packageId,$Asset_Type='File Storage');
			$target_Images = json_decode(json_encode($target_Images),true);
			$submitted_JSON1= $submitted_JSON['files'];			
			
			// echo '<pre>Before----------<br>';
			// print_r($submitted_JSON['files']);
			// echo '<pre><br>';
		
			foreach($submitted_JSON1 as $key=>$val)
			{
				$IsAssetPresentInTheMapping = $this->deploy_helper_model->IsAssetPresentInTheMapping('File Storage',$packageId,$val['id']);
				
					
				// echo '<pre>val x <br>';
					// print_r($val);
				// echo '<pre><br>';
				
				// $val = $this->deploy_helper_model->updateJsonNameForPost($val);
				// $val = array($val);
				
				// echo '<pre>val y <br>';
					// print_r($val);
				// echo '<pre><br>';
				
				if(!$IsAssetPresentInTheMapping)
				{
					foreach($target_Images['elements'] as $key1=>$val1)
					{	
						$imageName = $val['name'];
						// print_r($imageName);
						// $imageNamePattern = '/[&*#!$%+^",]/';
						// if(preg_match($imageNamePattern,  $imageName))
						// {
							// $imageName = str_replace('amp;','&',$imageName);
							// $imageName= preg_replace('/[&*#!$%+^�]/','_',$val->imageName);
						// }
						
						// echo '<pre>imageName old <br>';
							// print_r($imageName);
						// echo '<pre><br>';
						$imageName = $this->deploy_helper_model->updateJsonNameInParent($val);
						// echo '<pre>imageName new <br>';
							// print_r($imageName);
						// echo '<pre><br>';
		
						if($imageName == $val1['name'])
						{
							// echo 'here<pre>';
							// print_r($val1);
							// echo '<br>';
							
							// if(isset($val1['link']) && isset($val['link']))
							// {
								// $submitted_JSON = $this->getImageUrlsReplaced($submitted_JSON,$val['link'],$val1['link']);
								// echo '<pre>';								
								// print_r($submitted_JSON['files']);
							// }
							if(isset($val1['redirectLink']) && isset($val['redirectLink']))
							{
								$submitted_JSON = $this->getImageUrlsReplaced($submitted_JSON,$val['redirectLink'],$val1['redirectLink']);
							}
							if(isset($val1['trackedLink']) && isset($val['trackedLink']))
							{
								$submitted_JSON = $this->getImageUrlsReplaced($submitted_JSON,$val['trackedLink'],$val1['trackedLink']);
							}
							
						}			
					}
				}			
			}			
			if(isset($getSourceImageBaseURL) && isset($getTargetImageBaseURL))
			{
				$submitted_JSON  = $this->getImageUrlsReplaced($submitted_JSON,$getSourceImageBaseURL,$getTargetImageBaseURL);
				$submitted_JSON  = $this->getImageUrlsReplaced($submitted_JSON,$Package_details[0]->Source_Site_Name,$Package_details[0]->Target_Site_Name);				
			}
		}	
		
		$submitted_JSON = json_encode($submitted_JSON);
		return $submitted_JSON;
	}

	public static function test_print(&$item, $key)
	{
		$item = str_replace(deploy_model::$source_url,deploy_model::$target_url,$item);
	}
	
	public function getImageUrlsReplaced($newArray,$sourceUrl,$targetUrl)
	{
		// echo 'source url';print_r($sourceUrl);echo '<br>';
		// echo 'target url';print_r($targetUrl);echo '<br>';
		$Deployment_Package_Item_Id=40406;
		$Deployment_Package_Id=2916;
		$data = array();
		$sourceUrl = addcslashes($sourceUrl,'/');
		$targetUrl = addcslashes($targetUrl,'/');
		$url = 'http://transporter.portqii.com:8080/Test1/ReplaceSourceWithTarget';
		$submitted = json_encode($newArray);
		$data_json =$submitted.'|||'.$sourceUrl.'|||'.$targetUrl;
		$this->logging_model->logT($Deployment_Package_Id,$Deployment_Package_Item_Id,'getImageUrlsReplaced','data_json',$data_json);
		// //print_r($data_json);
		$ch=curl_init();
		$headers = array('Content-Type:text/plain');
		curl_setopt($ch, CURLOPT_URL, $url); 
		curl_setopt($ch, CURLOPT_POST, true); 
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS,$data_json);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		$result 		= curl_exec($ch);
		// //print_r('after replace');
		//$result = str_replace('en17','en25',$result);
		$result_temp=json_decode($result,true);
		return $result_temp;
	}
	
	public function searchAssets_targetInstance($packageId,$Asset_Type)
	{
		$this -> db -> select('Target_Site_Name');
		$this -> db -> from('deployment_package dp');
		$this -> db -> where('dp.Deployment_Package_Id', $packageId); 	
		$query1 = $this -> db -> get();
		$targerSiteName = $query1 -> result()[0] -> Target_Site_Name;
		
		$this -> db -> select('Base_Url, Token, Site_Name, Token_Expiry_Time, Refresh_Token');
		$this -> db -> from('instance_ inst');	
		$this -> db -> where('inst.Site_Name',$targerSiteName);
		$this -> db -> limit(1);
		$query23   = $this -> db -> get();
		
		$token 	   = $query23 -> result()[0] -> Token;
		$Base_Url  = $query23 -> result()[0] -> Base_Url;	
		$targetOrg = $query23 -> result()[0];
		
		if(time()+1800 > $query23 -> result()[0] -> Token_Expiry_Time)
		{
		   $result2 = $this -> eloqua -> refreshToken($targetOrg);
		   $query -> result()[0] -> Token_Expiry_Time = $result2['Token']['access_token'];
		}
		
		$this -> db -> select('*');
		$this -> db -> from('rsys_asset_endpoint rae');
		$this -> db -> where('rae.Asset_Type', $Asset_Type);
		$this -> db -> where('rae.Endpoint_Type', 'Find Asset');
		$query22 = $this -> db -> get();
		$endpoint = $query22->result();
		
		$url = $Base_Url .$endpoint[0]->Endpoint_URL ."?depth=complete";
		$result_asset = $this->eloqua->get_request($token, $url);
		$result_asset = json_decode($result_asset['data']);

		return $result_asset;
	}
	
	public function post_image($val)
	{
		$allTarget_Image = $this->searchAssets_targetInstance($val->Deployment_Package_Id,$val->Asset_Type);
		
		// echo '<pre>allTarget_Image <br>';
		// print_r($allTarget_Image);
		// echo '<pre><br>';
						
		$imageFound_flag = 0 ;
		$imageName = $val->Asset_Name;
		$imageNamePattern = '/[&*#!$%+^",]/';
		if(preg_match($imageNamePattern,  $val->Asset_Name))
		{
			$Asset_Name = str_replace('amp;','&',$Asset_Name);
			$imageName= preg_replace('/[&*#!$%+^",]/','_',$val->Asset_Name);
		}
		foreach($allTarget_Image->elements as $key1=>$val1)
		{
			//if($val->Asset_Name==$val1->name)
			
			if($imageName==$val1->name)
			{
				$imageFound_flag = 1;
				$this -> db -> select('*');
				$this -> db -> from('rsys_asset_type at');
				$this -> db -> join('rsys_asset_endpoint ep','at.Asset_Type_Name= ep.Asset_Type', 'LEFT OUTER');
				$this -> db -> where('Asset_Type_Name', $val->Asset_Type);
				$this -> db -> where('Endpoint_Type', 'Read Single');
				$this -> db -> limit(1);
				$query = $this -> db -> get();
				$resultend = $query->result()[0];
				$this -> db -> select('Base_Url, Token, Site_Name, Token_Expiry_Time, Refresh_Token');
				$this -> db -> from('instance_ inst');				
				$this -> db -> join('deployment_package dp','dp.Target_Site_Name = inst.Site_Name', 'LEFT OUTER');
				$this -> db -> join('deployment_package_item dpi','dpi.Deployment_Package_Id= dp.Deployment_Package_Id', 'LEFT OUTER');
				$this -> db -> where('dpi.Deployment_Package_Id', $val->Deployment_Package_Id);
				$this -> db -> limit(1);
				$query 		= $this -> db -> get();
				$token 		= $query -> result()[0] -> Token;
				$Base_Url 	= $query -> result()[0] -> Base_Url;		
				$org	= $query -> result()[0];
				$url = $org->Base_Url .$resultend->Endpoint_URL .'/'.$val1->id.'?depth='.$resultend->Depth;
				
				if(time()+1800 > $org->Token_Expiry_Time)
				{
				   $result2 = $this->eloqua->refreshToken($org);
				   $org->Token = $result2['Token']['access_token'];
				}
				$result = $this->eloqua->get_request($org->Token, $url);
				
				$result2 		= json_decode($result['data']);
		
				$sourceImageJson = json_decode($val->JSON_Asset);
				
				// echo '<pre>sourceImageJson <br>';
				// print_r($sourceImageJson);
				// echo '<pre><br>';
				
				$this -> db -> select('Base_Url, Token, Site_Name, Token_Expiry_Time, Refresh_Token');
				$this -> db -> from('instance_ inst');				
				$this -> db -> join('deployment_package dp','dp.Target_Site_Name = inst.Site_Name', 'LEFT OUTER');
				$this -> db -> join('deployment_package_item dpi','dpi.Deployment_Package_Id= dp.Deployment_Package_Id', 'LEFT OUTER');
				$this -> db -> where('dpi.Deployment_Package_Id', $val->Deployment_Package_Id);
				$this -> db -> limit(1);
				
				$target_query 		= $this -> db -> get();
				$target_token 		= $target_query -> result()[0] -> Token;
				$target_Base_Url 	= $target_query -> result()[0] -> Base_Url;		
				$target_sourceorg	= $target_query -> result()[0];
				
				$folderId = '';
				$JSON_Submitted = json_encode($sourceImageJson);
				
				// echo '<pre>JSON_Submitted <br>';
				// print_r($JSON_Submitted);
				// echo '<pre><br>';
				
				if(isset(json_decode($JSON_Submitted)->folderId))
				{
					$folderId = $this->deploy_helper_model->handleFolderStructure($val,$JSON_Submitted);
				}
				
				if($folderId != '' && $folderId != -1)
				{
					$result2->folderId = $folderId;
				}
				
				
				// $targetImageFolderId = $this->getTargetFolderId($sourceImageJson->folderId,$val);
				// $result2->folderId = $targetImageFolderId;
				
				//update image with target folder
				$url = $target_Base_Url.'/API/REST/2.0/assets/image/'.$result2->id;
				$putResult = $this->eloqua->putRequest($target_token,$url,json_encode($result2));
				
				$image_id		= $result2->id;
				$update_data['Target_Asset_Id'] = $image_id;
				$update_data['JSON_Submitted'] = $result['data'];
				$this -> db -> where('Deployment_Package_Item_Id', $val->Deployment_Package_Item_Id);
				$this -> db -> update('deployment_package_item', $update_data);
				
				//$New_JSON_data['responseInfo'] 	= $responseInfo;
				$New_JSON_data['Output_result'] = $result['data'];
				$New_JSON_data['body']			= $result['data'];
				$New_JSON_data['httpCode'] 		= $result['httpCode'];
				
				$this -> updateNew_JSON_Asset($New_JSON_data, $val);
				
			}
		}
		if($imageFound_flag==0)	
		{
			//to find out source detail
			$this -> db -> select('Base_Url, Token, Site_Name, Token_Expiry_Time, Refresh_Token');
			$this -> db -> from('instance_ inst');				
			$this -> db -> join('deployment_package dp','dp.Source_Site_Name = inst.Site_Name', 'LEFT OUTER');
			$this -> db -> join('deployment_package_item dpi','dpi.Deployment_Package_Id= dp.Deployment_Package_Id', 'LEFT OUTER');
			$this -> db -> where('dpi.Deployment_Package_Id', $val->Deployment_Package_Id);
			$this -> db -> limit(1);
			
			$query 		= $this -> db -> get();
			$token 		= $query -> result()[0] -> Token;
			$Base_Url 	= $query -> result()[0] -> Base_Url;		
			$sourceorg	= $query -> result()[0];
			
			
			if(time()+1800 > $query -> result()[0] -> Token_Expiry_Time)
			{
			   $result2 = $this -> eloqua -> refreshToken($sourceorg);
			   $query -> result()[0] -> Token_Expiry_Time = $result2['Token']['access_token'];
			}
			
			$id 			= $val -> Asset_Id;		
			$url 			= $Base_Url.'/API/REST/2.0/assets/image/'.$id;		
			$Authorization 	= 'Authorization: Bearer' .$token;
			$headers 		= array('Authorization: Bearer '.$token);
			// //////print_r($url);
			$ch 			= curl_init(); 
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
			curl_setopt($ch, CURLOPT_HTTPGET, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);			
			$httpCode 		= curl_getinfo($ch, CURLINFO_HTTP_CODE);
			$header_size 	= curl_getinfo($ch, CURLINFO_HEADER_SIZE);
			$data 			= curl_exec($ch);			
			$responseInfo 	= curl_getinfo($ch);
			
			$json_data 		= json_decode($data);
			
			//Enhancement for extension on 26/07/18
			$json_data 		= $this->deploy_helper_model->updateJsonNameForPost($json_data);
			
			// echo '<pre>json_data a <br>';
			// print_r($json_data);
			// echo '<pre><br>';
						
			//////print_r($json_data);
			$img_url 		= $Base_Url.$json_data->fullImageUrl;
			
			
			//to find out target detail
			$this -> db -> select('Base_Url, Token, Site_Name, Token_Expiry_Time, Refresh_Token');
			$this -> db -> from('instance_ inst');				
			$this -> db -> join('deployment_package dp','dp.Target_Site_Name = inst.Site_Name', 'LEFT OUTER');
			$this -> db -> join('deployment_package_item dpi','dpi.Deployment_Package_Id= dp.Deployment_Package_Id', 'LEFT OUTER');
			$this -> db -> where('dpi.Deployment_Package_Id', $val->Deployment_Package_Id);
			$this -> db -> limit(1);
			
			$target_query 		= $this -> db -> get();
			$target_token 		= $target_query -> result()[0] -> Token;
			$target_Base_Url 	= $target_query -> result()[0] -> Base_Url;		
			$target_sourceorg	= $target_query -> result()[0];
			
			if(time()+1800 > $target_query -> result()[0] -> Token_Expiry_Time)
			{
				$result2 = $this -> eloqua -> refreshToken($target_sourceorg);
				$target_query -> result()[0] -> Token_Expiry_Time = $result2['Token']['access_token'];
			}
			
			$target_url = $target_Base_Url.'/API/REST/2.0/assets/image/content';
			
			$img_info 	= getimagesize($img_url);
			$img_real 	= realpath($img_url);
			$data		= "--ELOQUA_BOUNDARY\r\n".
							"Content-Disposition: form-data; name=\"Filedata\"; filename=\"".$json_data->name."\"\r\n".
							"Content-Type: ".$img_url['mime']."\r\n".	
							"\r\n".
							file_get_contents($img_url)."\r\n".
							"--ELOQUA_BOUNDARY--";
							
			$headers 		= array('Authorization: Bearer '.$target_token, "Content-Length: ".strlen($data),"Content-Type: multipart/form-data; boundary=ELOQUA_BOUNDARY");
			
			$Authorization = $this->config->item('App_Id').':'.$this->config->item('Client_Secret');
			$ch=curl_init();
			curl_setopt($ch, CURLOPT_URL, $target_url); 
			curl_setopt($ch, CURLOPT_USERPWD, $Authorization);  
			curl_setopt($ch, CURLOPT_POST, true); 
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
			curl_setopt($ch, CURLOPT_HEADER, false);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			$result 		= curl_exec($ch);
			$httpCode 		= curl_getinfo($ch, CURLINFO_HTTP_CODE);
			$responseInfo 	= curl_getinfo($ch);
			$header_size 	= curl_getinfo($ch, CURLINFO_HEADER_SIZE);
			$body 			= substr($result, $header_size); 
			curl_close($ch);
			
			//to upload image in target 
			$result2 		= json_decode($result);
			$sourceImageJson = json_decode($val->JSON_Asset);		
			$JSON_Submitted = json_encode($sourceImageJson);
			
			// echo '<pre>result2<br>';
			// print_r($result2);
			// echo '<pre><br>';
			
			// echo '<pre>JSON_Submitted b<br>';
			// print_r($JSON_Submitted);
			// echo '<pre><br>';
			
			if(isset(json_decode($JSON_Submitted)->folderId))
			{
				$folderId = $this->deploy_helper_model->handleFolderStructure($val,$JSON_Submitted);
			}
			
			if($folderId != '' && $folderId != -1)
			{
				$result2->folderId = $folderId;
			}
			// $targetImageFolderId = $this->getTargetFolderId($sourceImageJson->folderId,$val);
			// $result2->folderId = $targetImageFolderId;
			
			//update image with target folder
			$url = $target_Base_Url.'/API/REST/2.0/assets/image/'.$result2->id;
			$putResult = $this->eloqua->putRequest($target_token,$url,json_encode($result2));
			
			$image_id		= $result2->id;
			$update_data['Target_Asset_Id'] = $image_id;
			$this -> db -> where('Deployment_Package_Item_Id', $val->Deployment_Package_Item_Id);
			$this -> db -> update('deployment_package_item', $update_data);
			
			$New_JSON_data['responseInfo'] 	= $responseInfo;
			$New_JSON_data['Output_result'] = $result;
			$New_JSON_data['body']			= $result;
			$New_JSON_data['httpCode'] 		= $httpCode;
			
			$this -> updateNew_JSON_Asset($New_JSON_data, $val);
			// return $New_JSON_data;
		}	
	}
	/*
	public function post_image($val)
	{
		$allTarget_Image = $this->searchAssets_targetInstance($val->Deployment_Package_Id,$val->Asset_Type);
		$imageFound_flag = 0 ;
		
		// echo '<br>val<br><pre>';
			// print_r($val);
		// echo '</pre><br>';
		
		foreach($allTarget_Image->elements as $key1=>$val1)
		{
			if($val->Asset_Name==$val1->name)
			{
				$imageFound_flag = 1;
				$this -> db -> select('*');
				$this -> db -> from('rsys_asset_type at');
				$this -> db -> join('rsys_asset_endpoint ep','at.Asset_Type_Name= ep.Asset_Type', 'LEFT OUTER');
				$this -> db -> where('Asset_Type_Name', $val->Asset_Type);
				$this -> db -> where('Endpoint_Type', 'Read Single');
				$this -> db -> limit(1);
				$query = $this -> db -> get();
				$resultend = $query->result()[0];
				$this -> db -> select('Base_Url, Token, Site_Name, Token_Expiry_Time, Refresh_Token');
				$this -> db -> from('instance_ inst');				
				$this -> db -> join('deployment_package dp','dp.Target_Site_Name = inst.Site_Name', 'LEFT OUTER');
				$this -> db -> join('deployment_package_item dpi','dpi.Deployment_Package_Id= dp.Deployment_Package_Id', 'LEFT OUTER');
				$this -> db -> where('dpi.Deployment_Package_Id', $val->Deployment_Package_Id);
				$this -> db -> limit(1);
				$query 		= $this -> db -> get();
				$token 		= $query -> result()[0] -> Token;
				$Base_Url 	= $query -> result()[0] -> Base_Url;		
				$org	= $query -> result()[0];
				$url = $org->Base_Url .$resultend->Endpoint_URL .'/'.$val1->id.'?depth='.$resultend->Depth;
				
				if(time()+1800 > $org->Token_Expiry_Time)
				{
				   $result2 = $this->eloqua->refreshToken($org);
				   $org->Token = $result2['Token']['access_token'];
				}
				$result = $this->eloqua->get_request($org->Token, $url);
				
				$result2 		= json_decode($result['data']);
		
				$sourceImageJson = json_decode($val->JSON_Asset);
				
				$this -> db -> select('Base_Url, Token, Site_Name, Token_Expiry_Time, Refresh_Token');
				$this -> db -> from('instance_ inst');				
				$this -> db -> join('deployment_package dp','dp.Target_Site_Name = inst.Site_Name', 'LEFT OUTER');
				$this -> db -> join('deployment_package_item dpi','dpi.Deployment_Package_Id= dp.Deployment_Package_Id', 'LEFT OUTER');
				$this -> db -> where('dpi.Deployment_Package_Id', $val->Deployment_Package_Id);
				$this -> db -> limit(1);
				
				$target_query 		= $this -> db -> get();
				$target_token 		= $target_query -> result()[0] -> Token;
				$target_Base_Url 	= $target_query -> result()[0] -> Base_Url;		
				$target_sourceorg	= $target_query -> result()[0];
				
				$folderId = '';
				//if($Package_Item->Asset_Type == 'Segment')
				
				//print_r('<pre>');
				//print_r($val);
				$JSON_Submitted = json_encode($sourceImageJson);
				if(isset(json_decode($JSON_Submitted)->folderId))
				{
					// if($val->Target_Folder_Id >0)
					// {
						// $folderId = $val->Target_Folder_Id;
					// }
					// else
					// {
						$folderId = $this->deploy_helper_model->handleFolderStructure($val,$JSON_Submitted);
					// }
				}
				
				if($folderId != '' && $folderId != -1)
				{
					$result2->folderId = $folderId;
				}
				// $targetImageFolderId = $this->getTargetFolderId($sourceImageJson->folderId,$val);
				// $result2->folderId = $targetImageFolderId;
				
				//update image with target folder
				$url = $target_Base_Url.'/API/REST/2.0/assets/image/'.$result2->id;
				$putResult = $this->eloqua->putRequest($target_token,$url,json_encode($result2));
				
				$image_id		= $result2->id;
				$update_data['Target_Asset_Id'] = $image_id;
				$update_data['JSON_Submitted'] = $result['data'];
				$this -> db -> where('Deployment_Package_Item_Id', $val->Deployment_Package_Item_Id);
				$this -> db -> update('deployment_package_item', $update_data);
				
				//$New_JSON_data['responseInfo'] 	= $responseInfo;
				$New_JSON_data['Output_result'] = $result['data'];
				$New_JSON_data['body']			= $result['data'];
				$New_JSON_data['httpCode'] 		= $result['httpCode'];
				
				$this -> updateNew_JSON_Asset($New_JSON_data, $val);
				
			}
		}
		
		// echo '<br>imageFound_flag<br><pre>';
			// print_r($imageFound_flag);
		// echo '</pre><br>';
		
		if($imageFound_flag==0)	
		{
			//to find out source detail
			$this -> db -> select('Base_Url, Token, Site_Name, Token_Expiry_Time, Refresh_Token');
			$this -> db -> from('instance_ inst');				
			$this -> db -> join('deployment_package dp','dp.Source_Site_Name = inst.Site_Name', 'LEFT OUTER');
			$this -> db -> join('deployment_package_item dpi','dpi.Deployment_Package_Id= dp.Deployment_Package_Id', 'LEFT OUTER');
			$this -> db -> where('dpi.Deployment_Package_Id', $val->Deployment_Package_Id);
			$this -> db -> limit(1);
			
			$query 		= $this -> db -> get();
			$token 		= $query -> result()[0] -> Token;
			$Base_Url 	= $query -> result()[0] -> Base_Url;		
			$sourceorg	= $query -> result()[0];
			
			
			if(time()+1800 > $query -> result()[0] -> Token_Expiry_Time)
			{
			   $result2 = $this -> eloqua -> refreshToken($sourceorg);
			   $query -> result()[0] -> Token_Expiry_Time = $result2['Token']['access_token'];
			}
			
			$id 			= $val -> Asset_Id;		
			$url 			= $Base_Url.'/API/REST/2.0/assets/image/'.$id;		
			$Authorization 	= 'Authorization: Bearer' .$token;
			$headers 		= array('Authorization: Bearer '.$token);
			// //////print_r($url);
			$ch 			= curl_init(); 
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
			curl_setopt($ch, CURLOPT_HTTPGET, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);			
			$httpCode 		= curl_getinfo($ch, CURLINFO_HTTP_CODE);
			$header_size 	= curl_getinfo($ch, CURLINFO_HEADER_SIZE);
			$data 			= curl_exec($ch);			
			$responseInfo 	= curl_getinfo($ch);
			
			$json_data 		= json_decode($data);
			//////print_r($json_data);
			$img_url 		= $Base_Url.$json_data->fullImageUrl;
			
			// echo '<br>json_data<br><pre>';
				// print_r($json_data);
			// echo '</pre><br>';
			// echo '<br>img_url<br><pre>';
				// print_r($img_url);
			// echo '</pre><br>';
			
			// echo 'Image name---';
				// print_r($json_data->name);
			// echo '<br>';
			
			//to find out target detail
			$this -> db -> select('Base_Url, Token, Site_Name, Token_Expiry_Time, Refresh_Token');
			$this -> db -> from('instance_ inst');				
			$this -> db -> join('deployment_package dp','dp.Target_Site_Name = inst.Site_Name', 'LEFT OUTER');
			$this -> db -> join('deployment_package_item dpi','dpi.Deployment_Package_Id= dp.Deployment_Package_Id', 'LEFT OUTER');
			$this -> db -> where('dpi.Deployment_Package_Id', $val->Deployment_Package_Id);
			$this -> db -> limit(1);
			
			$target_query 		= $this -> db -> get();
			$target_token 		= $target_query -> result()[0] -> Token;
			$target_Base_Url 	= $target_query -> result()[0] -> Base_Url;		
			$target_sourceorg	= $target_query -> result()[0];
			
			if(time()+1800 > $target_query -> result()[0] -> Token_Expiry_Time)
			{
				$result2 = $this -> eloqua -> refreshToken($target_sourceorg);
				$target_query -> result()[0] -> Token_Expiry_Time = $result2['Token']['access_token'];
			}
			
			$target_url = $target_Base_Url.'/API/REST/2.0/assets/image/content';
			
			$img_info 	= getimagesize($img_url);
			// print_r($img_url);
			$img_real 	= realpath($img_url);
			//$url_utf8 = rawurlencode(utf8_encode($img_url));
			// $data		= "--ELOQUA_BOUNDARY\r\n".
							// "Content-Disposition: form-data; name=\"Filedata\"; filename=\"".$json_data->name."\"\r\n".
							// "Content-Type: ".$img_url['mime']."\r\n".	
							// "\r\n".
							// file_get_contents($img_url)."\r\n".
							// "--ELOQUA_BOUNDARY--";
			$data		= "--ELOQUA_BOUNDARY\r\n".
							"Content-Disposition: form-data; name=\"Filedata\"; filename=\"".$json_data->name."\"\r\n".
							"Content-Type: ".$img_url['mime']."\r\n".	
							"\r\n".
							file_get_contents($img_url)."\r\n".
							"--ELOQUA_BOUNDARY--"; 
							
			// echo '<br>data<br><pre>';
				// print_r($data);
			// echo '</pre><br>';
			
			$data = mb_convert_encoding($data, 'HTML-ENTITIES', "UTF-8");
							
			$headers 		= array('Authorization: Bearer '.$target_token, "Content-Length: ".strlen($data),"Content-Type: multipart/form-data; boundary=ELOQUA_BOUNDARY");
			
			$Authorization = $this->config->item('App_Id').':'.$this->config->item('Client_Secret');
			$ch=curl_init();
			curl_setopt($ch, CURLOPT_URL, $target_url); 
			curl_setopt($ch, CURLOPT_USERPWD, $Authorization);  
			curl_setopt($ch, CURLOPT_POST, true); 
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
			curl_setopt($ch, CURLOPT_HEADER, false);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			$result 		= curl_exec($ch);
			$httpCode 		= curl_getinfo($ch, CURLINFO_HTTP_CODE);
			$responseInfo 	= curl_getinfo($ch);
			$header_size 	= curl_getinfo($ch, CURLINFO_HEADER_SIZE);
			$body 			= substr($result, $header_size); 
			curl_close($ch);
			
			
			//to upload image in target 
			$result2 		= json_decode($result);
			// echo '<br>result<br><pre>';
				// print_r($result);
			// echo '</pre><br>';
			// echo '<br>result2<br><pre>';
				// print_r($result2);
			// echo '</pre><br>';
			
			// echo '<pre>';
				// print_r($result2);
			// echo '</pre>';
			$sourceImageJson = json_decode($val->JSON_Asset);
			
			// echo '<br>sourceImageJson<br><pre>';
				// print_r($sourceImageJson);
			// echo '</pre><br>';
			
			$JSON_Submitted = json_encode($sourceImageJson);
			
			// echo '<br>JSON_Submitted<br><pre>';
				// print_r($JSON_Submitted);
			// echo '</pre><br>';
			
			if(isset(json_decode($JSON_Submitted)->folderId))
			{
				// if($Package_Item->Target_Folder_Id >0)
				// {
					// $folderId = $Package_Item->Target_Folder_Id;
				// }
				// else
				// {
				$folderId = $this->deploy_helper_model->handleFolderStructure($Package_Item,$JSON_Submitted);
				//}
			}
			
			if($folderId != '' && $folderId != -1)
			{
				$result2->folderId = $folderId;
			}
			// $targetImageFolderId = $this->getTargetFolderId($sourceImageJson->folderId,$val);
			// $result2->folderId = $targetImageFolderId;
			
			//update image with target folder
			$url = $target_Base_Url.'/API/REST/2.0/assets/image/'.$result2->id;
			$putResult = $this->eloqua->putRequest($target_token,$url,json_encode($result2));
			
			// echo '<br>url2<br><pre>';
				// print_r($url);
			// echo '</pre><br>';
			
			// echo '<br>putResult<br><pre>';
				// print_r($putResult);
			// echo '</pre><br>';
			
			$image_id		= $result2->id;
			$update_data['Target_Asset_Id'] = $image_id;
			$this -> db -> where('Deployment_Package_Item_Id', $val->Deployment_Package_Item_Id);
			$this -> db -> update('deployment_package_item', $update_data);
			
			$New_JSON_data['responseInfo'] 	= $responseInfo;
			$New_JSON_data['Output_result'] = $result;
			$New_JSON_data['body']			= $result;
			$New_JSON_data['httpCode'] 		= $httpCode;
			
			$this -> updateNew_JSON_Asset($New_JSON_data, $val);
			// return $New_JSON_data;
		}	
	}
	*/
	public function search_files_in_target($DPI)
	{
		$getsearch_endpoints = $this->getalleloqua_endpoints();
		$package = $this->getPackage($DPI->Deployment_Package_Id);
		$target_instance = $this->getInstance($package[0]->Target_Site_Name);
		
		//changes to asset name for special characters
		if(preg_match($this->pattern,  $DPI->Asset_Name))
		{
			$tempAssetName = str_replace('-', '_', $DPI->Asset_Name);
			$tempAssetName = preg_replace('/[^A-Za-z0-9\-]/','*',$tempAssetName);
		}
		else
			$tempAssetName=urlencode($DPI->Asset_Name);
						
		$result = $this->eloqua->get_request($target_instance[0]->Token, $target_instance[0]->Base_Url.'/'.$getsearch_endpoints['FileStorage_FindAsset']->Endpoint_URL.'?search="'.$tempAssetName.'"&depth=complete');
		// $result = $this->eloqua->get_request($target_instance[0]->Token, $target_instance[0]->Base_Url.'/'.$getsearch_endpoints['FileStorage_FindAsset']->Endpoint_URL.'?search="all.css"&depth=complete');
							
							
		$result_decode = json_decode($result['data']);
		
		$assetArray = array();
		$assetCount = 0;
		foreach($result_decode->elements as $rtKey=>$rtVal)
		{
			if($rtVal->name==$DPI->Asset_Name)
			{
				  array_push($assetArray,$rtVal);
				  $assetCount++;
			}
		}
		$result_decode->elements=$assetArray;
		$result_decode->total=$assetCount;
		
		if($result_decode->total>0)
		{
			return $result_decode->elements[0];
			// foreach($result_decode->elements as $rKey=>$rVal)
			// {
				// if($rVal->name==$DPI->Asset_Name)
					// return $rVal;
			// }
		}
		return null;
	}
	
	public function post_files($val)
	{
		
		$target_file= $this->search_files_in_target($val);
		//print_r('<pre>');
		if($target_file!=null)
		{
			// //print_r($val);
			$imageFound_flag = 1;
			$this -> db -> select('*');
			$this -> db -> from('rsys_asset_type at');
			$this -> db -> join('rsys_asset_endpoint ep','at.Asset_Type_Name= ep.Asset_Type', 'LEFT OUTER');
			$this -> db -> where('Asset_Type_Name', $val->Asset_Type);
			$this -> db -> where('Endpoint_Type', 'Read Single');
			$this -> db -> limit(1);
			$query = $this -> db -> get();
			$resultend = $query->result()[0];
			$this -> db -> select('Base_Url, Token, Site_Name, Token_Expiry_Time, Refresh_Token');
			$this -> db -> from('instance_ inst');				
			$this -> db -> join('deployment_package dp','dp.Target_Site_Name = inst.Site_Name', 'LEFT OUTER');
			$this -> db -> join('deployment_package_item dpi','dpi.Deployment_Package_Id= dp.Deployment_Package_Id', 'LEFT OUTER');
			$this -> db -> where('dpi.Deployment_Package_Id', $val->Deployment_Package_Id);
			$this -> db -> limit(1);
			$query 		= $this -> db -> get();
			$token 		= $query -> result()[0] -> Token;
			$Base_Url 	= $query -> result()[0] -> Base_Url;		
			$org	= $query -> result()[0];
			
			$url = $org->Base_Url .$resultend->Endpoint_URL .'/'.$target_file->id.'?depth='.$resultend->Depth;
			// //print_r($url);
			if(time()+1800 > $org->Token_Expiry_Time)
			{
			   $result2 = $this->eloqua->refreshToken($org);
			   $org->Token = $result2['Token']['access_token'];
			}
			$result = $this->eloqua->get_request($org->Token, $url);
			
			$result2 		= json_decode($result['data']);
			//print_r($result);
			$sourceImageJson = json_decode($val->JSON_Asset);
			
			$this -> db -> select('Base_Url, Token, Site_Name, Token_Expiry_Time, Refresh_Token');
			$this -> db -> from('instance_ inst');				
			$this -> db -> join('deployment_package dp','dp.Target_Site_Name = inst.Site_Name', 'LEFT OUTER');
			$this -> db -> join('deployment_package_item dpi','dpi.Deployment_Package_Id= dp.Deployment_Package_Id', 'LEFT OUTER');
			$this -> db -> where('dpi.Deployment_Package_Id', $val->Deployment_Package_Id);
			$this -> db -> limit(1);
			
			$target_query 		= $this -> db -> get();
			$target_token 		= $target_query -> result()[0] -> Token;
			$target_Base_Url 	= $target_query -> result()[0] -> Base_Url;		
			$target_sourceorg	= $target_query -> result()[0];
			
			$folderId = '';
			//if($Package_Item->Asset_Type == 'Segment')
			
			//print_r('<pre>');
			$JSON_Submitted = json_encode($sourceImageJson);
			if(isset(json_decode($JSON_Submitted)->folderId))
			{
				// if($Package_Item->Target_Folder_Id >0)
				// {
					// $folderId = $Package_Item->Target_Folder_Id;
				// }
				// else
				// {
					$folderId = $this->deploy_helper_model->handleFolderStructure($val,$JSON_Submitted);
				//}
			}
			
			if($folderId != '' && $folderId != -1)
			{
				$result2->folderId = $folderId;
			}
			//$targetImageFolderId = $this->getTargetFolderId($sourceImageJson->folderId,$val);
			
			
			//update image with target folder
			$url = $target_Base_Url.'/API/REST/2.0/assets/importedfile/'.$result2->id;
			$putResult = $this->eloqua->putRequest($target_token,$url,json_encode($result2));
			
			$image_id		= $result2->id;
			$update_data['Target_Asset_Id'] = $image_id;
			$update_data['JSON_Submitted'] = $result['data'];
			$this -> db -> where('Deployment_Package_Item_Id', $val->Deployment_Package_Item_Id);
			$this -> db -> update('deployment_package_item', $update_data);
			
			//$New_JSON_data['responseInfo'] 	= $responseInfo;
			$New_JSON_data['Output_result'] = $result['data'];
			$New_JSON_data['body']			= $result['data'];
			$New_JSON_data['httpCode'] 		= $result['httpCode'];
			
			$this -> updateNew_JSON_Asset($New_JSON_data, $val);
			
		}
		else	
		{
			//to find out source detail
			$this -> db -> select('Base_Url, Token, Site_Name, Token_Expiry_Time, Refresh_Token');
			$this -> db -> from('instance_ inst');				
			$this -> db -> join('deployment_package dp','dp.Source_Site_Name = inst.Site_Name', 'LEFT OUTER');
			$this -> db -> join('deployment_package_item dpi','dpi.Deployment_Package_Id= dp.Deployment_Package_Id', 'LEFT OUTER');
			$this -> db -> where('dpi.Deployment_Package_Id', $val->Deployment_Package_Id);
			$this -> db -> limit(1);
			
			$query 		= $this -> db -> get();
			$token 		= $query -> result()[0] -> Token;
			$Base_Url 	= $query -> result()[0] -> Base_Url;		
			$sourceorg	= $query -> result()[0];
			
			
			if(time()+1800 > $query -> result()[0] -> Token_Expiry_Time)
			{
			   $result2 = $this -> eloqua -> refreshToken($sourceorg);
			   $query -> result()[0] -> Token_Expiry_Time = $result2['Token']['access_token'];
			}
			
			$id 			= $val -> Asset_Id;		
			$url 			= $Base_Url.'/API/REST/2.0/assets/importedfile/'.$id;		
			$Authorization 	= 'Authorization: Bearer' .$token;
			$headers 		= array('Authorization: Bearer '.$token);
			// //////print_r($url);
			$ch 			= curl_init(); 
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
			curl_setopt($ch, CURLOPT_HTTPGET, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);			
			$httpCode 		= curl_getinfo($ch, CURLINFO_HTTP_CODE);
			$header_size 	= curl_getinfo($ch, CURLINFO_HEADER_SIZE);
			$data 			= curl_exec($ch);			
			$responseInfo 	= curl_getinfo($ch);
			
			$json_data 		= json_decode($data);
			
			//Enhancement for extension on 27/07/18
			$json_data 		= $this->deploy_helper_model->updateJsonNameForPost($json_data);
			
			// //print_r($json_data);
			$img_url 		= $json_data->link;
			
			$image_url_new = rawurlencode(utf8_encode($img_url));
			//to find out target detail
			$this -> db -> select('Base_Url, Token, Site_Name, Token_Expiry_Time, Refresh_Token');
			$this -> db -> from('instance_ inst');				
			$this -> db -> join('deployment_package dp','dp.Target_Site_Name = inst.Site_Name', 'LEFT OUTER');
			$this -> db -> join('deployment_package_item dpi','dpi.Deployment_Package_Id= dp.Deployment_Package_Id', 'LEFT OUTER');
			$this -> db -> where('dpi.Deployment_Package_Id', $val->Deployment_Package_Id);
			$this -> db -> limit(1);
			
			$target_query 		= $this -> db -> get();
			$target_token 		= $target_query -> result()[0] -> Token;
			$target_Base_Url 	= $target_query -> result()[0] -> Base_Url;		
			$target_sourceorg	= $target_query -> result()[0];
			
			if(time()+1800 > $target_query -> result()[0] -> Token_Expiry_Time)
			{
				$result2 = $this -> eloqua -> refreshToken($target_sourceorg);
				$target_query -> result()[0] -> Token_Expiry_Time = $result2['Token']['access_token'];
			}
			
			$imageName = utf8_encode($json_data->name);
			$target_url = $target_Base_Url.'/API/REST/2.0/assets/importedfile/content';
			// //print_r($img_url);
			//$img_info 	= filesize($img_url);
			$img_real 	= realpath($img_url);
			// $data		= "--ELOQUA_BOUNDARY\r\n".
							// "Content-Disposition: form-data; name=\"Filedata\"; filename=\"".$imageName."\"\r\n".
							// "Content-Type: ".$img_url['mime']."\r\n".	
							// "\r\n".
							// file_get_contents($image_url_new)."\r\n".
							// "--ELOQUA_BOUNDARY--";
							
			$data		= "--ELOQUA_BOUNDARY\r\n".
							"Content-Disposition: form-data; name=\"Filedata\"; filename=\"".$imageName."\"\r\n".
							"Content-Type: ".$img_url['mime']."\r\n".	
							"\r\n".
							file_get_contents($img_url)."\r\n".
							"--ELOQUA_BOUNDARY--";
							
			$headers 		= array('Authorization: Bearer '.$target_token, "Content-Length: ".strlen($data),"Content-Type: multipart/form-data; boundary=ELOQUA_BOUNDARY");
			// //print_r($headers);
			$Authorization = $this->config->item('App_Id').':'.$this->config->item('Client_Secret');
			$ch=curl_init();
			curl_setopt($ch, CURLOPT_URL, $target_url); 
			curl_setopt($ch, CURLOPT_USERPWD, $Authorization);  
			curl_setopt($ch, CURLOPT_POST, true); 
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
			curl_setopt($ch, CURLOPT_HEADER, false);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			$result 		= curl_exec($ch);
			$httpCode 		= curl_getinfo($ch, CURLINFO_HTTP_CODE);
			$responseInfo 	= curl_getinfo($ch);
			$header_size 	= curl_getinfo($ch, CURLINFO_HEADER_SIZE);
			$body 			= substr($result, $header_size); 
			curl_close($ch);
			
			//to upload image in target 
			$result2 		= json_decode($result);
			// //print_r($result2);
			$sourceImageJson = json_decode($val->JSON_Asset);			
			// $targetImageFolderId = $this->getTargetFolderId($sourceImageJson->folderId,$val);
			// $result2->folderId = $targetImageFolderId;
			
			$folderId = '';
			//if($Package_Item->Asset_Type == 'Segment')
			
			//print_r('<pre>');
			$JSON_Submitted = json_encode($sourceImageJson);
			if(isset(json_decode($JSON_Submitted)->folderId))
			{
				// if($Package_Item->Target_Folder_Id >0)
				// {
					// $folderId = $Package_Item->Target_Folder_Id;
				// }
				// else
				// {
					$folderId = $this->deploy_helper_model->handleFolderStructure($val,$JSON_Submitted);
				//}
			}
			
			if($folderId != '' && $folderId != -1)
			{
				$result2->folderId = $folderId;
			}
			
			//update image with target folder
			$url = $target_Base_Url.'/API/REST/2.0/assets/importedfile/'.$result2->id;
			$putResult = $this->eloqua->putRequest($target_token,$url,json_encode($result2));
			
			$image_id		= $result2->id;
			$update_data['Target_Asset_Id'] = $image_id;
			$this -> db -> where('Deployment_Package_Item_Id', $val->Deployment_Package_Item_Id);
			$this -> db -> update('deployment_package_item', $update_data);
			
			$New_JSON_data['responseInfo'] 	= $responseInfo;
			$New_JSON_data['Output_result'] = $result;
			$New_JSON_data['body']			= $result;
			$New_JSON_data['httpCode'] 		= $httpCode;
			
			$this -> updateNew_JSON_Asset($New_JSON_data, $val);
			// return $New_JSON_data;
		}	
	}
	
	function getTargetFolderId($sourceFolderId,$val)
	{
		//get source Image foldername
		$this -> db -> select('Base_Url, Token, Site_Name, Token_Expiry_Time, Refresh_Token');
		$this -> db -> from('instance_ inst');				
		$this -> db -> join('deployment_package dp','dp.Source_Site_Name = inst.Site_Name', 'LEFT OUTER');
		$this -> db -> join('deployment_package_item dpi','dpi.Deployment_Package_Id= dp.Deployment_Package_Id', 'LEFT OUTER');
		$this -> db -> where('dpi.Deployment_Package_Id', $val->Deployment_Package_Id);
		$this -> db -> limit(1);
		$query 		= $this -> db -> get();
		$token 		= $query -> result()[0] -> Token;
		$Base_Url 	= $query -> result()[0] -> Base_Url;		
		$org	= $query -> result()[0];
		if($val->Asset_Type == 'Image')
		{
			$url = $org->Base_Url .'/API/REST/2.0/assets/image/folder/'.$sourceFolderId.'?depth=complete';
		}
		else if($val->Asset_Type == 'File Storage')
		{
			$url = $org->Base_Url .'/API/REST/2.0/assets/importedfile/folder/'.$sourceFolderId.'?depth=complete';
		}
		
		if(time()+1800 > $org->Token_Expiry_Time)
		{
		   $result2 = $this->eloqua->refreshToken($org);
		   $org->Token = $result2['Token']['access_token'];
		}
		$result = $this->eloqua->get_request($org->Token, $url);
		//$result = $result['data'];
		
		$result2 		= json_decode($result['data']);
		//////print_r($result2 );
		
		//Get target folder with source image folder name 
		$this -> db -> select('Base_Url, Token, Site_Name, Token_Expiry_Time, Refresh_Token');
		$this -> db -> from('instance_ inst');				
		$this -> db -> join('deployment_package dp','dp.Target_Site_Name = inst.Site_Name', 'LEFT OUTER');
		$this -> db -> join('deployment_package_item dpi','dpi.Deployment_Package_Id= dp.Deployment_Package_Id', 'LEFT OUTER');
		$this -> db -> where('dpi.Deployment_Package_Id', $val->Deployment_Package_Id);
		$this -> db -> limit(1);
		$query1 		= $this -> db -> get();
		$token 		= $query1 -> result()[0] -> Token;
		$Base_Url 	= $query1 -> result()[0] -> Base_Url;		
		$org1	= $query1 -> result()[0];
		$name = $result2->name;
		$name = str_replace(' ', '%20', $name);
		$url = $org1->Base_Url ."/API/REST/2.0/assets/folders?search='$name'";
		//////print_r($url);
		//////print_r($org1);
		if(time()+1800 > $org1->Token_Expiry_Time)
		{
		   $result2 = $this->eloqua->refreshToken($org1);
		   $org1->Token = $result2['Token']['access_token'];
		}
		$result3    = $this->eloqua->get_request($org1->Token, $url);
		$result4 	= json_decode($result3['data']);
		
		if(!empty($result4->elements))
		{
			return $result4->elements[0]->id;
		}
		return $sourceFolderId;
		
	}
	
	function decideStatus($deploy_package_id)
	{
		$this->db->select('*');
		$this->db->from('deployment_package_item');
		$this->db->where('Deployment_Package_Id',$deploy_package_id);
		$query = $this->db->get();
		$totalRows = $query->num_rows();
		$errored = 0;
		$completed = 0;
		$actionRequired = 0;
		if($totalRows > 0)
		{
			$result = $query->result();
			foreach($result as $key=>$val)
			{
				if($val->Status == 'Errored')
				{
					$errored++;
				}
				else if($val->Status == 'Completed')
				{
					$completed++;
				}
				else if($val->Status == 'Action Required')
				{
					$actionRequired++;
				}	
			}
		}
		if($errored>0)
		{
			return 'Errored';
		}
		else if($actionRequired>0)
		{
			return 'Action Required';
		}	
		else 
		{
			return 'Deploy Completed';
		}
	}
		
	function ChangeParaValue($JSON_Submitted,$ChangeParaConfig)
	{
		if(!is_array($ChangeParaConfig) && !is_object($ChangeParaConfig))
		{
			$ChangeParaConfig = json_decode($ChangeParaConfig);
		}
		
		if($ChangeParaConfig !='')
		{
			if(is_array($JSON_Submitted))
			{
				$replaceList = (new JSONPath($JSON_Submitted))->find($ChangeParaConfig->JSON_Expressions);
			}
			else
			{
				$replaceList = (new JSONPath(json_decode($JSON_Submitted)))->find($ChangeParaConfig->JSON_Expressions);	
			}	
				
			$oldStr= '';
			$NewPara = '';
				
			foreach($replaceList as $key=>$val)
			{	
				$oldStr = $val;
				$removeStr =substr($val,strpos($val, $ChangeParaConfig->Para)+strlen($ChangeParaConfig->Para),$ChangeParaConfig->removeLength);
				$NewPara =str_replace($removeStr,$ChangeParaConfig->ChangeToStr,$val);			
			}
			$result = str_replace($oldStr,$NewPara, $JSON_Submitted);
		}
		else
		{
			$result = $JSON_Submitted;
		}	
		return $result;
	}	
		
	function replaceJSONKey($exclusion, $jsonAsset )
	{		
		if(is_array($exclusion) || is_object($exclusion))
		{
			foreach($exclusion as $key=>$val)
			{	
				if($val->basekey!="")
				{
					foreach($val->basekey as $key1=>$val1)
					{						
						foreach($val1 as $key2=>$val2)
						{					
							if(is_array($jsonAsset))
							{
								$jsonAsset[$val2] = $jsonAsset[$key2];
								unset($jsonAsset[$key2]);
							}
							if(is_object($jsonAsset))
							{
								$jsonAsset->$val2 = $jsonAsset->$key2;
								unset($jsonAsset->$key2);
							}
						}
					}
				}
			}
		}
		return $jsonAsset;
	}
	
	function getExclusionResult($exclusion, $jsonAsset )
	{
		if(is_array($exclusion) || is_object($exclusion))
		{
			foreach($exclusion as $key=>$val)
			{
				if($val->basekey!="")
				{
					$exSplit2 = explode(",",$val->basekey);
					foreach($exSplit2 as $key1=>$val1){
						if(is_array($jsonAsset) )
						{
							if(isset($jsonAsset[$val1]))
							unset($jsonAsset[$val1]);
						}
						if(is_object($jsonAsset))
						{
							if(isset($jsonAsset->$val1))
							unset($jsonAsset->$val1);
						}
					}
				}
			}
			$jsonAsset = json_encode($jsonAsset);
			$jsonAsset = json_decode($jsonAsset,true);
			$store = new JsonStore();
			foreach($exclusion as $key=>$val)
			{
				if(isset($val->jsonpath))
				{
					foreach($val->jsonpath as $k=>$v)
					{
						$store->remove($jsonAsset, $v->path);					
					}
				}	
			}	
		}
		return $jsonAsset;
	}
		
	function injSON($jsonObj,$node,$key,$type)
	{
		if(is_array($jsonObj->$node))
		{
			foreach($jsonObj->$node  as $key1=>$val1)
			{
				unset($val1->$key);
			}
			
		}
		if(is_object($jsonObj->$node))
		{
			//$this->injSON($jsonObj,$node,$key,"object");
		}	
		return $jsonObj;
	}
	
	function getalleloqua_endpoints()
	{
		$Asset_Type = array('Find Asset', 'Update','Create');
		$this->db->select('*');
		$this -> db -> from('rsys_asset_endpoint');
		$this -> db -> where_in('Endpoint_Type',$Asset_Type);
		$this->db->order_by("Asset_Type", "asc");
		$query = $this -> db -> get();
	    $eloqua_endpoints = $query->result();
		$alleloqua_endpoints = array();
		foreach($eloqua_endpoints as $key=>$val)
		{
			$alleloqua_endpoints[str_replace(' ', '', $val->Asset_Type).'_'.str_replace(' ', '',$val->Endpoint_Type)] = $val;
		}
		return $alleloqua_endpoints;
	}
	
	function getPackage($packageId)
	{
		$this->db->select('*');
		$this -> db -> from('deployment_package');
		$this -> db -> where('Deployment_Package_Id',$packageId);
		$this->db->limit(1);
		$query = $this -> db -> get();
	    $result = $query->result();
		return $result;
	}
	
	function getInstance($Site_Name)
	{
		$this->db->select('*');
		$this -> db -> from('instance_');
		$this -> db -> where('Site_Name',$Site_Name);
		$this->db->limit(1);
		$query = $this -> db -> get();
		$instance = $query->result();		
		return $instance;
	}

	function ReplaceSiteId($JSON_Submitted,$TargetSiteId,$Package_Item)
	{
		$temp__Submitted = $JSON_Submitted;
		
		$this->db->select("inst.Site_Id");
		$this->db->from("deployment_package dp");
		$this->db->join("instance_ inst","inst.Site_Name = dp.Source_Site_Name","LEFT OUTER");
		$this->db->where("Deployment_Package_Id",$Package_Item->Deployment_Package_Id);
		
		$result = $this->db->get();
		if($result->num_rows()>0)
		{
			$SourceSiteId = $result->result();
			$SourceSiteId = $SourceSiteId[0]->Site_Id;
			$temp__Submitted = str_replace($SourceSiteId,$TargetSiteId,$temp__Submitted);			
		}
		return $temp__Submitted;
	}
	
	function ProcessChildAssetSwitch($temp__Submitted,$val)
	{
		if($val->Asset_Type =="Custom Object") 
		{ 
			$this->db->select('*');
			$this -> db -> from('rsys_asset_child_replacement');
			$this -> db -> where('Asset_Type ',$val->Asset_Type);
			$Child_Replace_list = $this -> db -> get()->result();
			foreach($Child_Replace_list as $crkey=>$crval)
			{
				$replaceList = (new JSONPath(json_decode($temp__Submitted)))->find($crval->JSON_Expressions);
				
				foreach($replaceList as $repkey=>$repval)
				{
					$Source_Expression = str_replace('{X}',$repval,$crval->Source_Asset_Expression);
					
					$SourceIDList = (new JSONPath(json_decode($val->Source_Asset_JSON)))->find($Source_Expression);

					if(isset($SourceIDList[0]))
					{
						$Target_Expression = str_replace('{X}',$SourceIDList[0],$crval->Target_Asset_Expression);
						$TargetIDList = (new JSONPath(json_decode($val->Target_Asset_JSON)))->find($Target_Expression);
						
						if(isset($TargetIDList[0]))
						{
							$temp__Submitted = str_replace($crval->JSON_Node_Value.'":"'.$repval,$crval->JSON_Node_Value.'":"'.$TargetIDList[0],$temp__Submitted);
						}
						else
						{
							//////echo '<br/> TargetID List is missing </br>';
						}
					}
				}
			}
		}
		return $temp__Submitted;
	}
	
	function ProcessNegativeIds($Package_Item,$JSON_Submitted,$instance)
	{
		$temp__Submitted = $JSON_Submitted;
		// //print_r('Process Negative Ids<br/>'.$temp__Submitted);
		$store = new JsonStore();
		
		$this->db->select('JSON_Negative_Id_Component');
		$this -> db -> from('rsys_asset_endpoint');
		$this -> db -> where('Asset_Type',$Package_Item->Asset_Type);
		if($Package_Item->Target_Asset_Id > 0)
		{
			$this -> db -> where('Endpoint_Type',"Update");
		}
		else
		{	
			$this -> db -> where('Endpoint_Type',"Create");
		}
		$query = $this -> db -> get();
		$JSON_Negative_list = $query->result();
		$replace_pairs = array();
		
		foreach($JSON_Negative_list as $jnkey=>$jnval)
		{
			if($jnval->JSON_Negative_Id_Component =='')
			{
				continue; 
			}
			$jnval = explode(",", $jnval->JSON_Negative_Id_Component);	
			
			foreach($jnval as $expjnkey => $expjnval)
			{ 
				$replaceKey =  trim(substr($expjnval, strrpos($expjnval, '.') + 1));
				$replaceList = (new JSONPath(json_decode($temp__Submitted)))->find($expjnval);
				
				
				foreach($replaceList as $repkey=>$repval)
				{
					if($repval >=0)
					{						
						$repvalList = explode(' ',$repval);
						foreach($repvalList as $key=>$val)
						{	
							if(is_numeric($val))
							{
								$repvalList[$key]= '-'. $val;
							}
						}
						$newrepval = implode(' ',$repvalList);
						$temp__Submitted = str_replace('"'.$replaceKey.'":"'. $repval.'"','"'.$replaceKey.'":"'. $newrepval.'"',$temp__Submitted);
						$temp__Submitted = str_replace('"'.$replaceKey.'":'. $repval,'"'.$replaceKey.'":"'. $newrepval.'"',$temp__Submitted);
						
						$temp__Submitted1=json_decode($temp__Submitted,true);
						if( isset($temp__Submitted1['id']) && ($temp__Submitted1['id']<0) )
						{
							$temp = abs( $temp__Submitted1['id']);
							$temp__Submitted1['id'] = (string)$temp;
							$temp__Submitted = json_encode($temp__Submitted1);
						}	
					}
				}
			}
		} 
		////print_r('Process Negative Ids<br/>'.$temp__Submitted);
		return $temp__Submitted;
 	}
	
	//function to compare source and target custom object fields
	function compare_customObjectFields($source_co_JSON,$target_co_JSON,$JSON_Submitted)
	{
		$source_co_JSON = json_decode( json_encode($JSON_Submitted),true);
		$target_co_JSON = json_decode($target_co_JSON,true);
		// //print_r($source_co_JSON);
		$source_fields = $source_co_JSON['fields'];
		$target_fields = $target_co_JSON['fields'];
		$i = count($source_fields)-1;
		while($i>=0)
		{
			foreach($target_fields as $key=>$val)
			{
				if($source_fields[$i]['name'] == $val['name'])
				{
					$source_fields[$i]['id'] = $val['id'];
				}
				else
				{
					$flag = false;
					foreach($target_fields as $key1=>$val1)
					{
						if($val1['name'] == $source_fields[$i]['name'])
						{
							$flag = true;
						}
					}
					if(!$flag)
					{
						if(strpos($source_fields[$i]['id'], '-') !== false)
						{
							
						}
						else
						{
							$source_fields[$i]['id'] = '-'.$source_fields[$i]['id'];
						}
					}
				}
			}
			$i--;
		}
		
		foreach($target_fields as $tkey=>$tval)
		{
			$found = 0;
			foreach($source_fields as $skey=>$sval)
			{
				if($tval['name'] == $sval['name'])
				{
					$found =1;
				}
			}
			if($found == 0)
			{
				$source_fields[] = $tval;
			}
		}
		// //print_r($source_fields);
		$target_co_JSON['fields']=$source_fields;
		return json_encode($target_co_JSON);
	}	
	
	//Fetch the custom object from target if exits
	function getTarget_customObject($targerSiteName,$CO_id)
	{	
		$this -> db -> select('Base_Url, Token, Site_Name, Token_Expiry_Time, Refresh_Token');
		$this -> db -> from('instance_ inst');	
		$this -> db -> where('inst.Site_Name',$targerSiteName);
		$this -> db -> limit(1);
		$query23   = $this -> db -> get();
		
		$token 	   = $query23 -> result()[0] -> Token;
		$Base_Url  = $query23 -> result()[0] -> Base_Url;	
		$targetOrg = $query23 -> result()[0];
		
		if(time()+1800 > $query23 -> result()[0] -> Token_Expiry_Time)
		{
		   $result2 = $this -> eloqua -> refreshToken($targetOrg);
		   $query -> result()[0] -> Token_Expiry_Time = $result2['Token']['access_token'];
		}
		
		$this -> db -> select('*');
		$this -> db -> from('rsys_asset_endpoint rae');
		$this -> db -> where('rae.Asset_Type', 'Custom Object');
		$this -> db -> where('rae.Endpoint_Type', 'Read Single');
		$query22 = $this -> db -> get();
		$endpoint = $query22->result();
		$url = $Base_Url .$endpoint[0]->Endpoint_URL .'/'.$CO_id;
		$result_asset = $this->eloqua->get_request($token, $url);
		$result_asset = json_decode($result_asset['data']);
		return json_encode($result_asset);
	}
	
	function CDO_Update($JSON_Submitted,$instance,$Package_Item)
	{
		
		$JSON_Submitted = json_decode($JSON_Submitted);
		
		if(isset($JSON_Submitted->id)&& $JSON_Submitted->type=='CustomObject')
		{	
			$target_customObject = $this->getTarget_customObject($instance[0]->Site_Name,$JSON_Submitted->id);
			// //print_r($Package_Item->JSON_Asset);
			$JSON_Submitted = $this->compare_customObjectFields($Package_Item->JSON_Asset,$target_customObject,$JSON_Submitted);
		}
		else
		{
			$JSON_Submitted = json_encode($JSON_Submitted);
		}
		return $JSON_Submitted;	
	}

	function getSourceCDOName($CO_id,$packageId)
	{
		$Package_details = $this->deploy_model->getPackage($packageId);
			
		$this -> db -> select('Base_Url, Token, Site_Name, Token_Expiry_Time, Refresh_Token');
		$this -> db -> from('instance_ inst');	
		$this -> db -> where('inst.Site_Name',$Package_details[0]->Source_Site_Name);
		$this -> db -> limit(1);
		$query23   = $this -> db -> get();		
		$token 	   = $query23 -> result()[0] -> Token;
		$Base_Url  = $query23 -> result()[0] -> Base_Url;	
		$targetOrg = $query23 -> result()[0];
		
		if(time()+1800 > $query23 -> result()[0] -> Token_Expiry_Time)
		{
		   $result2 = $this -> eloqua -> refreshToken($targetOrg);
		   $query -> result()[0] -> Token_Expiry_Time = $result2['Token']['access_token'];
		}
		
		$this -> db -> select('*');
		$this -> db -> from('rsys_asset_endpoint rae');
		$this -> db -> where('rae.Asset_Type', 'Custom Object');
		$this -> db -> where('rae.Endpoint_Type', 'Read Single');
		$query22 = $this -> db -> get();
		$endpoint = $query22->result();
		
		$url = $Base_Url .$endpoint[0]->Endpoint_URL .'/'.$CO_id;		
		$result_asset = $this->eloqua->get_request($token, $url);		
		$result_asset = json_decode($result_asset['data']);	
		// //print_r($result_asset->name);
		$targetCDO=$this->getTargetcustomObjectJSONByName($Package_details[0]->Target_Site_Name,$result_asset->name);
		// //print_r($targetCDO);
		return $targetCDO;
	}
	
	//Fetch the custom object from target if exits
	function getTargetcustomObjectJSONByName($targerSiteName,$CDOName)
	{			
		$this -> db -> select('Base_Url, Token, Site_Name, Token_Expiry_Time, Refresh_Token');
		$this -> db -> from('instance_ inst');	
		$this -> db -> where('inst.Site_Name',$targerSiteName);
		$this -> db -> limit(1);
		$query23   = $this -> db -> get();
		
		$token 	   = $query23 -> result()[0] -> Token;
		$Base_Url  = $query23 -> result()[0] -> Base_Url;	
		$targetOrg = $query23 -> result()[0];
		
		if(time()+1800 > $query23 -> result()[0] -> Token_Expiry_Time)
		{
		   $result2 = $this -> eloqua -> refreshToken($targetOrg);
		   $query -> result()[0] -> Token_Expiry_Time = $result2['Token']['access_token'];
		}
		
		$this -> db -> select('*');
		$this -> db -> from('rsys_asset_endpoint rae');
		$this -> db -> where('rae.Asset_Type', 'Custom Object');
		$this -> db -> where('rae.Endpoint_Type', 'Find Asset');
		$query22 = $this -> db -> get();
		$endpoint = $query22->result();
		
		//changes to asset name for special characters
		if(preg_match($this->pattern,  $CDOName))
		{
			$tempAssetName = str_replace('-', '_', $CDOName);
			$tempAssetName = preg_replace('/[^A-Za-z0-9\-]/','*',$tempAssetName);
		}
		else
		{
			$tempAssetName=urlencode($CDOName);
		}
		$url = $Base_Url .$endpoint[0]->Endpoint_URL .'?depth=complete&search="'.str_replace(' ','%20',$tempAssetName).'"';
		////print_r($url);
		$result_asset = $this->eloqua->get_request($token, $url);
		$result_asset = json_decode($result_asset['data']);
		
		$assetArray = array();
		$assetCount = 0;
		foreach($result_asset->elements as $rtKey=>$rtVal)
		{
			if($rtVal->name==$CDOName)
			{
				  array_push($assetArray,$rtVal);
				  $assetCount++;
			}
		}
		$result_asset->elements=$assetArray;
		$result_asset->total=$assetCount;
		
		if(isset($result_asset->elements[0]))
		{
			$resultAsset=$result_asset->elements[0];
			
			// foreach($result_asset_->elements as $rKey=>$rVal)
			// {
				// if($rVal->name==$CDOName)
				// {
					// $resultAsset=$rVal;
					// break;
				// }
			// }
		}
		return $resultAsset;
	}
	
	/* replace id recursive logic*/	
	// function replace(&$temp__Submitted1,$val)
	// {	
		// try{
		// echo 'temp__Submitted1<br><pre>';
			// print_r($temp__Submitted1);
		// echo '<br></pre>';
		// echo 'val<br><pre>';
			// print_r($val);
		// echo '<br></pre>';
		// foreach($temp__Submitted1 as $key1=>$val1)
		// {
		// echo 'val1<br><pre>';
			// print_r($val1);
		// echo '<br></pre>';
		// echo 'key1<br><pre>';
			// print_r($key1);
		// echo '<br></pre>';
			// if(is_array($val1) && $val1!='permissions')
			// if(is_array($val1) && $key1!='permissions')
			// {
				// $this->replace($temp__Submitted1[$key1],$val);
			// }
			// else
			// {
				 
		
				// if(is_numeric($key1))
				// {
					// echo 'Asset_Id<br><pre>';
						// print_r($val->Asset_Id);
					// echo '<br></pre>';
					
					// echo 'Asset_Id<br><pre>';
						// print_r($val->Asset_Id);
					// echo '<br></pre>';
					
					// if($val1==$val->Asset_Id)
					// {
						// $temp__Submitted1[$key1] = $val->Target_Asset_Id.'vikas-';
					// }	
				// }
				// else if($key1==$val->JSON_Node_Value)
				// {
					// echo 'JSON_Node_Value<br><pre>';
						// print_r($val->JSON_Node_Value);
					// echo '<br></pre>';
					// if($val1==$val->Asset_Id)
					// {
						// $temp__Submitted1[$key1] = $val->Target_Asset_Id.'vikas-';
					// }
				// }	
			// }
		// }
		// }
		// catch(Exception $e) {
		// echo 'Message: ' .$e->getMessage();
// }
	// }	
	
	/* replace id recursive logic*/ 
	 function replace(&$temp__Submitted1,$val)
	 {  
		  foreach($temp__Submitted1 as $key1=>$val1)
		  {
			   if(is_array($val1) && $val1!='permissions')
			   {
					$this->replace($temp__Submitted1[$key1],$val);
			   }
			   else
			   {
					if(is_numeric($key1))
					{
						 if($val1==$val->Asset_Id)
						 {
							$temp__Submitted1[$key1] = $val->Target_Asset_Id.'vikas-';
						 } 
					}
					else if($key1==$val->JSON_Node_Value)
					{
						 if($val1==$val->Asset_Id)
						 {
							$temp__Submitted1[$key1] = $val->Target_Asset_Id.'vikas-';
						 }
					} 
			   }
		  }
	 }
	
	function getDeploymentPackageValidation($Package_Item,$JSON_Submitted,$instance,$endPoint)
	{
		$this->logging_model->logT($Package_Item->Deployment_Package_Id,$Package_Item->Deployment_Package_Item_Id,'getDeploymentPackageValidation','beginning of getDeploymentPackageValidation',$JSON_Submitted);
		
		$JSON_submitted_CDO_change='';
		$this->db->select('*');
		$this -> db -> from('deployment_package_validation_list');
		$this -> db -> where('Deployment_Package_Item_Id',$Package_Item->Deployment_Package_Item_Id);
		$query = $this -> db -> get();
				
		$JSON_Submitted = $this->ReplaceSiteId($JSON_Submitted,$instance[0]->Site_Id,$Package_Item);
		$this->logging_model->logT($Package_Item->Deployment_Package_Id,$Package_Item->Deployment_Package_Item_Id,'getDeploymentPackageValidation','after ReplaceSiteId',$JSON_Submitted);			
			
		$JSON_submitted_CDO_FieldMerge = $this->deploy_helper_model->CDO_FieldMerge($JSON_Submitted,$instance,$Package_Item);
		$JSON_Submitted = $JSON_submitted_CDO_FieldMerge;
		$this->logging_model->logT($Package_Item->Deployment_Package_Id,$Package_Item->Deployment_Package_Item_Id,'getDeploymentPackageValidation','after CDO_FieldMerge',$JSON_Submitted);	
		
		$JSON_submitted_CDO_SL = $this->deploy_helper_model->signatureRule_CDO($JSON_Submitted,$instance,$Package_Item);
		$JSON_Submitted = $JSON_submitted_CDO_SL;
		$this->logging_model->logT($Package_Item->Deployment_Package_Id,$Package_Item->Deployment_Package_Item_Id,'getDeploymentPackageValidation','after signatureRule_CDO',$JSON_Submitted);
		
		$JSON_Submitted_CDO_DynamicContent=$this->deploy_helper_model->CDO_DynamicContent($JSON_Submitted,$instance,$Package_Item);
		$JSON_Submitted = $JSON_Submitted_CDO_DynamicContent; 
		$this->logging_model->logT($Package_Item->Deployment_Package_Id,$Package_Item->Deployment_Package_Item_Id,'getDeploymentPackageValidation','after CDO_DynamicContent',$JSON_Submitted);
		
		$JSON_Submitted_Form_ProcessingSteps_CDO_FormData = $this->deploy_helper_model->CDO_Form_PS_Form_Data($JSON_Submitted,$instance,$Package_Item);
		$JSON_Submitted = $JSON_Submitted_Form_ProcessingSteps_CDO_FormData;
		$this->logging_model->logT($Package_Item->Deployment_Package_Id,$Package_Item->Deployment_Package_Item_Id,'getDeploymentPackageValidation','after CDO_Form_PS_Form_Data',$JSON_Submitted);
		
		$JSON_Submitted_Form_ProcessingSteps_CDO_CustomValue = $this->deploy_helper_model->CDO_Form_PS_CustomValue($JSON_Submitted,$instance,$Package_Item);
		$JSON_Submitted = $JSON_Submitted_Form_ProcessingSteps_CDO_CustomValue;
		$this->logging_model->logT($Package_Item->Deployment_Package_Id,$Package_Item->Deployment_Package_Item_Id,'getDeploymentPackageValidation','after CDO_Form_PS_CustomValue',$JSON_Submitted);
		
		$JSON_Submitted_Segment_CDO = $this->deploy_helper_model->CDO_Segment($JSON_Submitted,$Package_Item);
		$JSON_Submitted = $JSON_Submitted_Segment_CDO;
		$this->logging_model->logT($Package_Item->Deployment_Package_Id,$Package_Item->Deployment_Package_Item_Id,'getDeploymentPackageValidation','after CDO_Segment',$JSON_Submitted);
				
		$JSON_Submitted_elqTrackId = $this->deploy_helper_model->elqTrackId_handler($JSON_Submitted,$Package_Item);
		$JSON_Submitted = $JSON_Submitted_elqTrackId;
		$this->logging_model->logT($Package_Item->Deployment_Package_Id,$Package_Item->Deployment_Package_Item_Id,'getDeploymentPackageValidation','after elqTrackId_handler',$JSON_Submitted);
		
		
		$JSON_Submitted_quickList = $this->deploy_helper_model->JSON_Submitted_quickList($JSON_Submitted);
		$JSON_Submitted = $JSON_Submitted_quickList;
		$this->logging_model->logT($Package_Item->Deployment_Package_Id,$Package_Item->Deployment_Package_Item_Id,'getDeploymentPackageValidation','after JSON_Submitted_quickList',$JSON_Submitted);
		
		$JSON_Submitted_add2move2Campaign = $this->deploy_helper_model->JSON_Submitted_add2move2Campaign_handler($JSON_Submitted,$Package_Item,$this->setSkeletonFlag);
		$JSON_Submitted = $JSON_Submitted_add2move2Campaign['JSON_Submitted'];
		if($this->setSkeletonFlag==0)
		{
			$this->setSkeletonFlag= $JSON_Submitted_add2move2Campaign['setSkeletonFlag'];
		}
		
		$this->logging_model->logT($Package_Item->Deployment_Package_Id,$Package_Item->Deployment_Package_Item_Id,'getDeploymentPackageValidation','after JSON_Submitted_add2move2Campaign_handler',$JSON_Submitted);
			
		$JSON_Submitted_FormHiddenEmailCampaignId = $this->deploy_helper_model->FormHiddenEmailCampaignId($JSON_Submitted,$Package_Item);
		$JSON_Submitted = $JSON_Submitted_FormHiddenEmailCampaignId;
		$this->logging_model->logT($Package_Item->Deployment_Package_Id,$Package_Item->Deployment_Package_Item_Id,'getDeploymentPackageValidation','after JSON_Submitted_FormHiddenEmailCampaignId',$JSON_Submitted);
				
		$JSON_Submitted_LPFileStorage = $this->deploy_helper_model->LPFileStorage($JSON_Submitted,$Package_Item);
		$JSON_Submitted = $JSON_Submitted_LPFileStorage;
		$this->logging_model->logT($Package_Item->Deployment_Package_Id,$Package_Item->Deployment_Package_Item_Id,'getDeploymentPackageValidation','after LPFileStorage',$JSON_Submitted);
		
		$JSON_Submitted_LPMicrositeIdReplace = $this->deploy_helper_model->LPMicrositeIdReplace($JSON_Submitted,$Package_Item);
		$JSON_Submitted = $JSON_Submitted_LPMicrositeIdReplace;
		$this->logging_model->logT($Package_Item->Deployment_Package_Id,$Package_Item->Deployment_Package_Item_Id,'getDeploymentPackageValidation','after LPMicrositeIdReplace',$JSON_Submitted);		
		
		$JSON_Submitted_domainNameReplace = $this->deploy_helper_model->LPDomainNameReplace($JSON_Submitted,$Package_Item);
		$JSON_Submitted = $JSON_Submitted_domainNameReplace;
		$this->logging_model->logT($Package_Item->Deployment_Package_Id,$Package_Item->Deployment_Package_Item_Id,'getDeploymentPackageValidation','after LPDomainNameReplace',$JSON_Submitted);
			
		$JSON_Submitted_ReplaceMicrositeIdForExistingLP = $this->deploy_helper_model->ReplaceMicrositeIdForExistingLP($JSON_Submitted,$Package_Item);
		$JSON_Submitted = $JSON_Submitted_ReplaceMicrositeIdForExistingLP;
		$this->logging_model->logT($Package_Item->Deployment_Package_Id,$Package_Item->Deployment_Package_Item_Id,'getDeploymentPackageValidation','after ReplaceMicrositeIdForExistingLP',$JSON_Submitted);		
		
		$JSON_Submitted_ContactFilter_CDO = $this->deploy_helper_model->CDO_ContactFilter($JSON_Submitted,$Package_Item);
		$JSON_Submitted = $JSON_Submitted_ContactFilter_CDO;
		$this->logging_model->logT($Package_Item->Deployment_Package_Id,$Package_Item->Deployment_Package_Item_Id,'getDeploymentPackageValidation','after CDO_ContactFilter',$JSON_Submitted);
		
		$JSON_Submitted_ContactFiletr = $this->deploy_helper_model->statementNegative_contactFilter($JSON_Submitted,$Package_Item);
		$JSON_Submitted = $JSON_Submitted_ContactFiletr;
		$this->logging_model->logT($Package_Item->Deployment_Package_Id,$Package_Item->Deployment_Package_Item_Id,'getDeploymentPackageValidation','after statementNegative_contactFilter',$JSON_Submitted);		
		
		//Form Processing Steps Condition Custom Object Handler 06 Nov Anamika
		$JSON_Submitted_Form_PS_ConditionCDO = $this->CDO_FormPSCondition($JSON_Submitted,$Package_Item);
		$JSON_Submitted = $JSON_Submitted_Form_PS_ConditionCDO;
		$this->logging_model->logT($Package_Item->Deployment_Package_Id,$Package_Item->Deployment_Package_Item_Id,'getDeploymentPackageValidation','after CDO_FormPSCondition',$JSON_Submitted);		
		
		$programCanvasFeeder= json_decode($JSON_Submitted);
		if($programCanvasFeeder->type=='Program' && (strpos($JSON_Submitted, 'CanvasFeeder') !== false))
		{
			
			$message='Canvas Feeder Listener';
			$this->deploy_helper_model->updateActionRequiredToDeploymentPackageItem($Package_Item->Deployment_Package_Item_Id,$message);
			if($this->setSkeletonFlag==0)
			{
				$this->setSkeletonFlag=1;
			}
		}
		$programCanvasFeeder1= json_decode($JSON_Submitted);
		if($programCanvasFeeder1->type=='Program' && strpos($JSON_Submitted, 'CampaignCloudAction') !== false)
		{
			$message='Campaign Cloud Action';
			$this->deploy_helper_model->updateActionRequiredToDeploymentPackageItem($Package_Item->Deployment_Package_Item_Id,$message);
			if($this->setSkeletonFlag==0)
			{
				$this->setSkeletonFlag=1;
			}
		}
		$programCanvasFeeder2= json_decode($JSON_Submitted);
		if($programCanvasFeeder2->type=='Program' && strpos($JSON_Submitted, 'CanvasRunUpdateRuleSet') !== false)
		{
			$message='Update Rule Set';
			$this->deploy_helper_model->updateActionRequiredToDeploymentPackageItem($Package_Item->Deployment_Package_Item_Id,$message);
			if($this->setSkeletonFlag==0)
			{
				$this->setSkeletonFlag=1;
			}
		}	
		$programCanvasFeeder3= json_decode($JSON_Submitted);
		if($programCanvasFeeder3->type=='Program' && strpos($JSON_Submitted, 'CampaignCloudFeeder') !== false)
		{
			$message='Campaign Cloud Feeder';
			$this->deploy_helper_model->updateActionRequiredToDeploymentPackageItem($Package_Item->Deployment_Package_Item_Id,$message);
			if($this->setSkeletonFlag==0)
			{
				$this->setSkeletonFlag=1;
			}
		}	
		$programCanvasFeeder4= json_decode($JSON_Submitted);
		if($programCanvasFeeder4->type=='Program' && strpos($JSON_Submitted, 'CampaignCloudRule') !== false)
		{
			$message='Campaign Cloud Rule';
			$this->deploy_helper_model->updateActionRequiredToDeploymentPackageItem($Package_Item->Deployment_Package_Item_Id,$message);
			if($this->setSkeletonFlag==0)
			{
				$this->setSkeletonFlag=1;
			}
		}	
		$JSON_Submitted_ProgramBuilderInProgram = $this->deploy_helper_model->supportOfProgramBuilderInProgram($JSON_Submitted,$Package_Item,$this->setSkeletonFlag);
		$JSON_Submitted = $JSON_Submitted_ProgramBuilderInProgram['modifiedJSON'];
		if($this->setSkeletonFlag==0)
		{
			$this->setSkeletonFlag= $JSON_Submitted_ProgramBuilderInProgram['setSkeletonFlag'];
		}
		$this->logging_model->logT($Package_Item->Deployment_Package_Id,$Package_Item->Deployment_Package_Item_Id,'getDeploymentPackageValidation','after supportOfProgramBuilderInProgram',$JSON_Submitted);
		
		$JSON_Submitted_add2move2CampaignInProgram = $this->deploy_helper_model->JSON_Submitted_add2move2CampaignInProgram_handler($JSON_Submitted,$Package_Item);
		$JSON_Submitted = $JSON_Submitted_add2move2CampaignInProgram;
		$this->logging_model->logT($Package_Item->Deployment_Package_Id,$Package_Item->Deployment_Package_Item_Id,'getDeploymentPackageValidation','after JSON_Submitted_add2move2Program_handler',$JSON_Submitted);
		
		$JSON_Submitted_add2move2ProgramInProgram = $this->deploy_helper_model->JSON_Submitted_add2move2ProgramInProgram_handler($JSON_Submitted,$Package_Item,$this->setSkeletonFlag);
		$JSON_Submitted = $JSON_Submitted_add2move2ProgramInProgram['modifiedJSON'];
		if($this->setSkeletonFlag==0)
		{
			$this->setSkeletonFlag= $JSON_Submitted_add2move2ProgramInProgram['setSkeletonFlag'];
		}
		$this->logging_model->logT($Package_Item->Deployment_Package_Id,$Package_Item->Deployment_Package_Item_Id,'getDeploymentPackageValidation','after JSON_Submitted_add2move2ProgramInProgram',$JSON_Submitted);
		
		// Enhancement 03/04/2018 - Action Required from Unsupported to Action Required for Email Singature Rule
		$tempJSONForESR = json_decode($JSON_Submitted);
		if($tempJSONForESR->type=='EmailSignatureRule' && $Package_Item->verified == 1)
		{			
			$defaultSenderTargetUserId = $this->deploy_helper_model->getDefaultSenderTargertUserId($Package_Item);
			if($defaultSenderTargetUserId>0)
			{
				$tempJSONForESR->defaultSenderId = $defaultSenderTargetUserId;
			}	
			if($tempJSONForESR->eventRegistrationId)
			{
				unset($tempJSONForESR->eventRegistrationFieldId);
				unset($tempJSONForESR->eventRegistrationId);
				$message='Event Registration';				
				$this->deploy_helper_model->updateDeploymentPackageItem_StatusAndMessage($Package_Item->Deployment_Package_Item_Id,$message,'Action Required','ActionRequired_Message');
				if($this->setSkeletonFlag==0)
				{
					$this->setSkeletonFlag=1;
				}		
			}
			$message='Complete the Default Sender field and Mappings in Signature Rule in the target instance';				
			$this->deploy_helper_model->updateDeploymentPackageItem_StatusAndMessage($Package_Item->Deployment_Package_Item_Id,$message,'Action Required','ActionRequired_Message');
			if($this->setSkeletonFlag==0)
			{
				$this->setSkeletonFlag=1;
			}	
			
			$JSON_Submitted = json_encode($tempJSONForESR);	
		}
		
		$tempJSONForCS = json_decode($JSON_Submitted);
		if($tempJSONForCS->type=='ContactSegment' && strpos(json_encode($tempJSONForCS), 'ContactListSegmentElement') !== false)
		{			
			foreach($tempJSONForCS->elements as $clKey=>$clVal)
			{
				if(isset($clVal->list))
				{
					$message='Add Contacts to ' .$clVal->list->name;
					$this->deploy_helper_model->updateActionRequiredToDeploymentPackageItem($Package_Item->Deployment_Package_Item_Id,$message);
					if($this->setSkeletonFlag==0)
					{
						$this->setSkeletonFlag=1;
					}
				}
			}
		}
		
		$tempJSONForSL = json_decode($JSON_Submitted);
		if($tempJSONForSL->type=='ContactList')
		{	
			$message='Add Contacts to' .$tempJSONForSL->name;
			$this->deploy_helper_model->updateActionRequiredToDeploymentPackageItem($Package_Item->Deployment_Package_Item_Id,$message);
			if($this->setSkeletonFlag==0)
			{
				$this->setSkeletonFlag=1;
			}		
		}
		
		//Enhancement 11/07/2018 - Action Required from Unsupported for Landing Page
		
		$this->logging_model->logT($Package_Item->Deployment_Package_Id,$Package_Item->Deployment_Package_Item_Id,'getDeploymentPackageValidation','before removeFMOfTypeEventFromLandingPage',$JSON_Submitted);			
		$tempJSONForLP = json_decode($JSON_Submitted);
		if($tempJSONForLP->type=='LandingPage' && $Package_Item->verified==1)
		{
			$tempJSON_FMRemoved = $this->deploy_helper_model->removeFMOfTypeEventFromLandingPage($JSON_Submitted,$Package_Item,$this->setSkeletonFlag);					
			$JSON_Submitted = $tempJSON_FMRemoved['JSONSubmitted'];
			if($this->setSkeletonFlag==0)
			{
				$this->setSkeletonFlag= $tempJSON_FMRemoved['setSkeletonFlag'];
			}
		}
		$this->logging_model->logT($Package_Item->Deployment_Package_Id,$Package_Item->Deployment_Package_Item_Id,'getDeploymentPackageValidation','after removeFMOfTypeEventFromLandingPage',$JSON_Submitted);
		
		//Enhancement 11/07/2018 - Action Required in Shared Content for Field Merge of type event & event session 	
		$tempJSONForSCFM = json_decode($JSON_Submitted);
		if($tempJSONForSCFM->type=='ContentSection' && $Package_Item->verified==1)
		{
			$tempJSON_FMRemoved = $this->deploy_helper_model->removeFMOfTypeEventFromSharedContent($JSON_Submitted,$Package_Item,$this->setSkeletonFlag);					
			$JSON_Submitted = $tempJSON_FMRemoved['JSONSubmitted'];
			if($this->setSkeletonFlag==0)
			{
				$this->setSkeletonFlag= $tempJSON_FMRemoved['setSkeletonFlag'];
			}
		}
		
		//Enhancement 11/07/2018 - Action Required in Shared Content for Field Merge of type event & event session 	
		$tempJSONForDCFM = json_decode($JSON_Submitted);
		if($tempJSONForDCFM->type=='DynamicContent' && $Package_Item->verified==1)
		{
			$tempJSON_FMRemoved = $this->deploy_helper_model->removeFMOfTypeEventFromDynamicContent($JSON_Submitted,$Package_Item,$this->setSkeletonFlag);					
			$JSON_Submitted = $tempJSON_FMRemoved['JSONSubmitted'];
			if($this->setSkeletonFlag==0)
			{
				$this->setSkeletonFlag= $tempJSON_FMRemoved['setSkeletonFlag'];
			}
		}
		
		$createdFromShellFlag=$this->getAssetCreatedFromShell($Package_Item->Deployment_Package_Id,$Package_Item->Deployment_Package_Item_Id);
		if($createdFromShellFlag==1)
		{
			$message='Empty assets created';
			$this->deploy_helper_model->updateDeploymentPackageItem_StatusAndMessage($Package_Item->Deployment_Package_Item_Id,$message,'Action Required','ActionRequired_Message');
		}
		
		$folderId = '';				
		if(isset(json_decode($JSON_Submitted)->folderId))
		{
			$folderId = $this->deploy_helper_model->handleFolderStructure($Package_Item,$JSON_Submitted);
		}			
			
		$jsonSubmittedDecoded = json_decode($JSON_Submitted,true);
		if($folderId != '' && $folderId != -1)
		{
			$jsonSubmittedDecoded['folderId'] = $folderId;
			$JSON_Submitted = json_encode($jsonSubmittedDecoded);
		}
		else
		{
			unset($jsonSubmittedDecoded['folderId']);
			$JSON_Submitted = json_encode($jsonSubmittedDecoded);
		}		
		
		if($query -> num_rows() == 0)
		{		
			$JSON_Submitted = $this->ProcessNegativeIds($Package_Item,$JSON_Submitted,$instance);
			$this->logging_model->logT($Package_Item->Deployment_Package_Id,$Package_Item->Deployment_Package_Item_Id,'getDeploymentPackageValidation','after ProcessNegativeIds IF',$JSON_Submitted);			
			
			$JSON_Submitted_unset_campaignInput = $this->deploy_helper_model->JSON_Submitted_unset_campaignInput($JSON_Submitted,$instance,$Package_Item);
			$JSON_Submitted = $JSON_Submitted_unset_campaignInput;
			$this->logging_model->logT($Package_Item->Deployment_Package_Id,$Package_Item->Deployment_Package_Item_Id,'getDeploymentPackageValidation','after JSON_Submitted_unset_campaignInput IF',$JSON_Submitted);
			
			$JSON_Submitted_hyperlinkLPIDReplace = $this->deploy_helper_model->hyperlinkLhref($JSON_Submitted,$Package_Item->Deployment_Package_Id);
			$JSON_Submitted = $JSON_Submitted_hyperlinkLPIDReplace;
			$this->logging_model->logT($Package_Item->Deployment_Package_Id,$Package_Item->Deployment_Package_Item_Id,'getDeploymentPackageValidation','after hyperlinkLhref IF',$JSON_Submitted);
			
			$JSON_Submitted_CDO_Update = $this->CDO_Update($JSON_Submitted,$instance,$Package_Item);
			$JSON_Submitted = $JSON_Submitted_CDO_Update;
			$this->logging_model->logT($Package_Item->Deployment_Package_Id,$Package_Item->Deployment_Package_Item_Id,'getDeploymentPackageValidation','after CDO_Update IF',$JSON_Submitted);
			
			
			if ($Package_Item->Target_Asset_Id == 0)
			{	
				$hyperlinkJSON = json_decode($JSON_Submitted);
				if($hyperlinkJSON->type=='Hyperlink' && $hyperlinkJSON->hyperlinkType=='ExternalURL')
				{	
					$data123['JSON_Submitted'] = $JSON_Submitted;
					if(time()+1800 > $instance[0]->Token_Expiry_Time)
					{
					   $result2 = $this->eloqua->refreshToken($instance[0]);
					   $instance[0]->Token = $result2['Token']['access_token'];
					}
					if($Package_Item->verified == 1 && $Package_Item->isCircular!=-1)
					{
						if(!isset($hyperlinkJSON->id))
						{
							$result = $this->eloqua->postRequest($instance[0]->Token, $instance[0]->Base_Url.''.$endPoint,$JSON_Submitted);
						}
						else
						{
							$result = $this->eloqua->putRequest($instance[0]->Token, $instance[0]->Base_Url.''.$endPoint.'/'.$hyperlinkJSON->id,$JSON_Submitted);
						}
						
						$this->updateNew_JSON_Asset($result,$Package_Item);
						$this->db->where('Deployment_Package_Item_Id',$Package_Item->Deployment_Package_Item_Id);
						$this->db->update('deployment_package_item',$data123);
						// echo '<br/> Put Request  Hyperlink<br/><pre>';
							//print_r($result);
						// echo '</pre>';
					}
					
				}
				else 
				{
					if(time()+1800 > $instance[0]->Token_Expiry_Time)
					{
					   $result2 = $this->eloqua->refreshToken($instance[0]);
					   $instance[0]->Token = $result2['Token']['access_token'];
					}
					if($Package_Item->verified == 1 && $Package_Item->isCircular != -1)
					{
						$result = $this->eloqua->postRequest($instance[0]->Token, $instance[0]->Base_Url.''.$endPoint,$JSON_Submitted);
						//Sleep 15 seconds for Forms
						if($Package_Item->Asset_Type=='Form')
						{
							sleep(30);
						}
						// echo '<pre>';
							// print_r('Post Request 1'.$result);
						// echo '<pre>';
					}
				}	
			}
			else
			{	
				$temp__Submitted = $this->CDO_Update($JSON_Submitted,$instance,$Package_Item);
				$this->logging_model->logT($Package_Item->Deployment_Package_Id,$Package_Item->Deployment_Package_Item_Id,'getDeploymentPackageValidation','after CDO_Update ELSE PUT 1',$temp__Submitted);
				
				$temp__Submitted_camp= $this->deploy_helper_model->CDO_Campaign($temp__Submitted,$Package_Item);
				$temp__Submitted=$temp__Submitted_camp;
				$this->logging_model->logT($Package_Item->Deployment_Package_Id,$Package_Item->Deployment_Package_Item_Id,'getDeploymentPackageValidation','after CDO_Campaign ELSE PUT 1',$temp__Submitted);
				
				$JSON_Submitted = $temp__Submitted;
				
				if(time()+1800 > $instance[0]->Token_Expiry_Time)
				{
				   $result2 = $this->eloqua->refreshToken($instance[0]);
				   $instance[0]->Token = $result2['Token']['access_token'];
				}
				
				if($Package_Item->verified == 1 && $Package_Item->isCircular != -1)
				{
					$result = $this->eloqua->putRequest($instance[0]->Token, $instance[0]->Base_Url.''.$endPoint.'/'.$Package_Item->Target_Asset_Id,$JSON_Submitted);
					//Sleep 15 seconds for Forms
					if($Package_Item->Asset_Type=='Form')
					{
						sleep(30);
					}
				}				    
				// echo '<br/> Put Request 1<br/><pre>';
					//print_r($result);
				// echo '</pre>';
			    
			}			
			if($Package_Item->verified == 1 && $Package_Item->isCircular != -1)
			{
				$data123['JSON_Submitted'] = $JSON_Submitted;
				$this->db->where('Deployment_Package_Item_Id',$Package_Item->Deployment_Package_Item_Id);
				$this->db->update('deployment_package_item',$data123);
				$this->updateNew_JSON_Asset($result,$Package_Item);
			}			
		}
		else
		{			
			$package_validation_list = $query->result();
			
			$temp__Submitted = $this->CDO_Update($JSON_Submitted,$instance,$Package_Item);
			$this->logging_model->logT($Package_Item->Deployment_Package_Id,$Package_Item->Deployment_Package_Item_Id,'getDeploymentPackageValidation','after CDO_Update ELSE',$temp__Submitted);			
			
			$temp__Submitted_camp= $this->deploy_helper_model->CDO_Campaign($temp__Submitted,$Package_Item);
			$temp__Submitted=$temp__Submitted_camp;
			$this->logging_model->logT($Package_Item->Deployment_Package_Id,$Package_Item->Deployment_Package_Item_Id,'getDeploymentPackageValidation','after CDO_Campaign ELSE',$temp__Submitted);			
			
			$temp__Submitted1=json_decode($temp__Submitted,true);
			
			$CFNameForForm=array();
			//Call the dependency endpoint to remove contact field from Form - commented on 15 May due to incomplete state
			foreach($package_validation_list as $key=>$val)
			{
				if($temp__Submitted1['type']=='Form' && $val->Asset_Type=='Contact Field' && $val->Target_Asset_Id==-1)
				{
					$replaceContactField = $this->callDependencyEnd($val->Asset_Id,$temp__Submitted1,$Package_Item);$temp__Submitted1 = json_decode($replaceContactField,true);	
					array_push($CFNameForForm,$val->Asset_Name);					
				}									
			}
			
			if(sizeof($CFNameForForm)>0)
			{
				foreach($CFNameForForm as $nameKey=>$nameVal)
				{
					$message = 'Contact Field '.$nameVal .' mapped to none';						
					$this->deploy_helper_model->updateDeploymentPackageItem_StatusAndMessage($Package_Item->Deployment_Package_Item_Id,$message,'Action Required','ActionRequired_Message');
					if($this->setSkeletonFlag==0)
					{
						$this->setSkeletonFlag=1;
					}
				}
			}
			
			$this->db->distinct();
			$this->db->select('*');			
			$this -> db -> from('deployment_package_validation_list');
			$this -> db -> where('Deployment_Package_Item_Id',$Package_Item->Deployment_Package_Item_Id);
			$query1 = $this -> db -> get();
			$package_validation_list1 = $query1->result();		

			$CFListToBeRemoved=array();	
			$CampFieldListToBeRemoved=array();
			$PickListToBeRemoved=array();
			
			$AssetNameListToBeRemoved=array();
			$CampFieldNameListToBeRemoved=array();
			$PickListNameListToBeRemoved=array();			
			//$AssetNameListToBeRemoved=array();	
			
			foreach($package_validation_list1 as $key1=>$val1)
			{
				if(($temp__Submitted1['type']=='ContactSegment' || $temp__Submitted1['type']=='Campaign' || $temp__Submitted1['type']=='Program' || $temp__Submitted1['type']=='ContactView' || $temp__Submitted1['type']=='ContactFilter' || $temp__Submitted1['type']=='DynamicContent') && $val1->Asset_Type=='Contact Field' && $val1->Target_Asset_Id==-1)
				{
					array_push($CFListToBeRemoved,$val1->Asset_Id);	
					array_push($AssetNameListToBeRemoved,$val1->Asset_Name);	
					
				}
				
				if(($temp__Submitted1['type']=='Campaign' || $temp__Submitted1['type']=='ContactFilter' || $temp__Submitted1['type']=='ContactSegment') && $val1->Asset_Type=='Campaign Field' && $val1->Target_Asset_Id==-1)
				{
					array_push($CampFieldListToBeRemoved,$val1->Asset_Id);	
					array_push($CampFieldNameListToBeRemoved,$val1->Asset_Name);						
				}	
				if(($temp__Submitted1['type']=='ContactSegment' || $temp__Submitted1['type']=='ContactFilter' || $temp__Submitted1['type']=='Form') && $val1->Asset_Type=='Picklist' && $val1->Target_Asset_Id==-1)
				{
					array_push($PickListToBeRemoved,$val1->Asset_Id);
					array_push($PickListNameListToBeRemoved,$val1->Asset_Name);					
				}	
			}
			
			if(sizeof($CFListToBeRemoved)>0)
			{
				$replaceContactFieldInAsset = $this->callTransporterJavaEnd($temp__Submitted1,$CFListToBeRemoved,$temp__Submitted1['type'],$Package_Item,'Contact Field');
				$replaceContactFieldInAsset = utf8_encode($replaceContactFieldInAsset);				
				$temp__Submitted1 = json_decode($replaceContactFieldInAsset,true);
				$childAssetType='';
				foreach($AssetNameListToBeRemoved as $nameKey=>$nameVal)
				{
					$message = 'Contact Field '.$nameVal .' mapped to none';
					//print_r($message);						
					$this->deploy_helper_model->updateDeploymentPackageItem_StatusAndMessage($Package_Item->Deployment_Package_Item_Id,$message,'Action Required','ActionRequired_Message');
					if($this->setSkeletonFlag==0)
					{
						$this->setSkeletonFlag=1;
					}
				}				
			}	
			
			
			//print_r($AssetNameListToBeRemoved);
			if(sizeof($PickListToBeRemoved)>0)
			{
				//print_r($childAssetType);
				$replaceContactFieldInAsset = $this->callTransporterJavaEnd($temp__Submitted1,$PickListToBeRemoved,$temp__Submitted1['type'],$Package_Item,'Picklist');
				$replaceContactFieldInAsset = utf8_encode($replaceContactFieldInAsset);				
				$temp__Submitted1 = json_decode($replaceContactFieldInAsset,true);
				foreach($PickListNameListToBeRemoved as $nameKey=>$nameVal)
				{
					$message = 'Picklist '.$nameVal .' mapped to none';
					//print_r($message);						
					$this->deploy_helper_model->updateActionRequiredToDeploymentPackageItem($Package_Item->Deployment_Package_Item_Id,$message);
					if($this->setSkeletonFlag==0)
					{
						$this->setSkeletonFlag=1;
					} 
				}				
			}
			
			if(sizeof($CampFieldListToBeRemoved)>0)
			{
				//print_r($childAssetType);
				$replaceContactFieldInAsset = $this->callTransporterJavaEnd($temp__Submitted1,$CampFieldListToBeRemoved,$temp__Submitted1['type'],$Package_Item,'Campaign Field');
				$replaceContactFieldInAsset = utf8_encode($replaceContactFieldInAsset);				
				$temp__Submitted1 = json_decode($replaceContactFieldInAsset,true);
				foreach($CampFieldNameListToBeRemoved as $nameKey=>$nameVal)
				{
					$message = 'Campaign Field '.$nameVal .' mapped to none';
					//print_r($message);						
					$this->deploy_helper_model->updateActionRequiredToDeploymentPackageItem($Package_Item->Deployment_Package_Item_Id,$message);
					if($this->setSkeletonFlag==0)
					{
						$this->setSkeletonFlag=1;
					}
				}				
			}
			
			// echo '<br>before rep <pre>';
				// print_r(json_decode($temp__Submitted));
			// echo '</pre><br>';
			
			foreach($package_validation_list as $key=>$val)
			{
				$this->replace($temp__Submitted1,$val); 
				$this->logging_model->logT($Package_Item->Deployment_Package_Id,$Package_Item->Deployment_Package_Item_Id,'getDeploymentPackageValidation','after replace ELSE',json_encode($temp__Submitted1));
				$this->logging_model->logT($Package_Item->Deployment_Package_Id,$Package_Item->Deployment_Package_Item_Id,'getDeploymentPackageValidation','val after replace ELSE ',json_encode($val));
				$temp__Submitted = json_encode($temp__Submitted1);  
			}			
			
			if(strpos($temp__Submitted, 'vikas-' ) !== false ) 
			{
				$temp__Submitted=str_replace("vikas-","",$temp__Submitted);
			}
			
			// echo '<br>after rep <pre>';
				// print_r(json_decode($temp__Submitted));
			// echo '</pre><br>';

			foreach($package_validation_list as $key=>$val)
			{   
				$temp__Submitted = $this->ProcessChildAssetSwitch($temp__Submitted,$val);
				if(isset($val->Target_Asset_Name) && $val->Target_Asset_Name!='' && ($val->Target_Asset_Name!="None" || $val->Target_Asset_Name !=-1))
				{
				 $temp__Submitted = json_decode($temp__Submitted);
				 $temp__Submitted = $this->deploy_helper_model->handleTargetAssetIdReplacementInJson($temp__Submitted,$val);
				 $temp__Submitted = json_encode($temp__Submitted);
				 $temp__Submitted1 = json_decode($temp__Submitted,true);
				}
			}
				
			$this->logging_model->logT($Package_Item->Deployment_Package_Id,$Package_Item->Deployment_Package_Item_Id,'getDeploymentPackageValidation','after handleTargetAssetIdReplacementInJson',$temp__Submitted);
			
			
			// echo 'before process negative ids <pre>';
					// print_r($temp__Submitted1);
			// echo '</pre><br>';			
				
			$temp__Submitted = $this->ProcessNegativeIds($Package_Item,$temp__Submitted,$instance);
			$this->logging_model->logT($Package_Item->Deployment_Package_Id,$Package_Item->Deployment_Package_Item_Id,'getDeploymentPackageValidation','after ProcessNegativeIds ELSE',$temp__Submitted);
			
			$temp = json_decode($temp__Submitted);
			// if($temp->type=='Form')
			// {
				// echo 'fps17 <pre>';
					// print_r($temp);
				// echo '</pre><br>';
			// }

			$temp__Submitted = $this->removeNegativeIdForGlobalContactFilterInSegment($temp__Submitted);
			$this->logging_model->logT($Package_Item->Deployment_Package_Id,$Package_Item->Deployment_Package_Item_Id,'getDeploymentPackageValidation','after removeNegativeIdFromGlobalContactFilterInSegment',$temp__Submitted);
			
			// $temp = json_decode($temp__Submitted);
			// if($temp->type=='Form')
			// {
				// echo 'fps18 <pre>';
					// print_r($temp->processingSteps);
				// echo '</pre><br>';
			// }
			
			$temp__Submitted_camp= $this->deploy_helper_model->CDO_Campaign($temp__Submitted,$Package_Item);
			$temp__Submitted=$temp__Submitted_camp;
			$this->logging_model->logT($Package_Item->Deployment_Package_Id,$Package_Item->Deployment_Package_Item_Id,'getDeploymentPackageValidation','after CDO_Campaign ELSE',$temp__Submitted);
			
			// $temp = json_decode($temp__Submitted);
			// if($temp->type=='Form')
			// {
				// echo 'fps19 <pre>';
					// print_r($temp->processingSteps);
				// echo '</pre><br>';
			// }
			
			$this->logging_model->logT($Package_Item->Deployment_Package_Id,$Package_Item->Deployment_Package_Item_Id,'getDeploymentPackageValidation','after ProcessChildAssetSwitch ELSE',$temp__Submitted);
			
			$JSON_Submitted_Replace_Target_Element_Id = $this->deploy_helper_model->JSON_Submitted_Replace_Target_Element_Id($temp__Submitted,$instance,$Package_Item);
			$temp__Submitted = $JSON_Submitted_Replace_Target_Element_Id;
			$this->logging_model->logT($Package_Item->Deployment_Package_Id,$Package_Item->Deployment_Package_Item_Id,'getDeploymentPackageValidation','after JSON_Submitted_Replace_Target_Element_Id ELSE',$temp__Submitted);
			
			// $temp = json_decode($temp__Submitted);
			// if($temp->type=='Form')
			// {
				// echo 'fps20 <pre>';
					// print_r($temp->processingSteps);
				// echo '</pre><br>';
			// }
		
			$JSON_Submitted_unset_campaignInput = $this->deploy_helper_model->JSON_Submitted_unset_campaignInput($temp__Submitted,$instance,$Package_Item);
			$temp__Submitted = $JSON_Submitted_unset_campaignInput;
			$this->logging_model->logT($Package_Item->Deployment_Package_Id,$Package_Item->Deployment_Package_Item_Id,'getDeploymentPackageValidation','after JSON_Submitted_unset_campaignInput ELSE',$temp__Submitted);
			
			// $temp = json_decode($temp__Submitted);
			// if($temp->type=='Form')
			// {
				// echo 'fps21 <pre>';
					// print_r($temp->processingSteps);
				// echo '</pre><br>';
			// }
					
			// $JSON_Submitted_HyperlinkLPID = $this->deploy_helper_model->LPHyperlinkLPID($temp__Submitted,$package_validation_list);
			// $temp__Submitted = $JSON_Submitted_HyperlinkLPID;	
			
			// //print_r(json_decode($JSON_Submitted_HyperlinkLPID));
			
			$temp__Submitted_temp=$this->deploy_helper_model->handleHTMLChanges($temp__Submitted,$package_validation_list);
			$temp__Submitted=$temp__Submitted_temp;
			$this->logging_model->logT($Package_Item->Deployment_Package_Id,$Package_Item->Deployment_Package_Item_Id,'getDeploymentPackageValidation','after handleHTMLChanges ELSE',$temp__Submitted);
			
			// $temp = json_decode($temp__Submitted);
			// if($temp->type=='Form')
			// {
				// echo 'fps22 <pre>';
					// print_r($temp->processingSteps);
				// echo '</pre><br>';
			// }
			
			
			$JSON_Submitted_hyperlinkLPIDReplace = $this->deploy_helper_model->hyperlinkLhref($temp__Submitted,$Package_Item->Deployment_Package_Id);
			$temp__Submitted = $JSON_Submitted_hyperlinkLPIDReplace;
			$this->logging_model->logT($Package_Item->Deployment_Package_Id,$Package_Item->Deployment_Package_Item_Id,'getDeploymentPackageValidation','after hyperlinkLhref ELSE',$temp__Submitted);
			
			// $temp = json_decode($temp__Submitted);
			// if($temp->type=='Form')
			// {
				// echo 'fps23 <pre>';
					// print_r($temp->processingSteps);
				// echo '</pre><br>';
			// }
			
			// echo '<pre>1';
				// print_r(json_decode($temp__Submitted));
			// echo '</pre>';
			
			$JSON_Submitted_HyperlinkAsLP = $this->deploy_helper_model->hyperlinkAsLP($temp__Submitted,$instance,$Package_Item);
			$temp__Submitted = $JSON_Submitted_HyperlinkAsLP;
			$this->logging_model->logT($Package_Item->Deployment_Package_Id,$Package_Item->Deployment_Package_Item_Id,'getDeploymentPackageValidation','after hyperlinkAsLP Else',$temp__Submitted);

			// $temp = json_decode($temp__Submitted);
			// if($temp->type=='Form')
			// {
				// echo 'fps24 <pre>';
					// print_r($temp->processingSteps);
				// echo '</pre><br>';
			// }
			// echo '<pre>2';
				// print_r(json_decode($temp__Submitted));
			// echo '</pre>';
		
			$data123['JSON_Submitted'] = $temp__Submitted;
			
			
			
			if($Package_Item->Target_Asset_Id == 0)
			{
				$hyperlinkJSON = json_decode($temp__Submitted);			
							
				if($hyperlinkJSON->type=='Hyperlink' && $hyperlinkJSON->hyperlinkType=='ExternalURL')
				{
					if(time()+1800 > $instance[0]->Token_Expiry_Time)
					{
					   $result2 = $this->eloqua->refreshToken($instance[0]);
					   $instance[0]->Token = $result2['Token']['access_token'];
					}
					
					if($Package_Item->verified == 1 && $Package_Item->isCircular != -1)
					{
						if(isset($hyperlinkJSON->id))
						{
							$result = $this->eloqua->putRequest($instance[0]->Token, $instance[0]->Base_Url.''.$endPoint.'/'.$hyperlinkJSON->id,$temp__Submitted);
						}
						else
						{
							$result = $this->eloqua->postRequest($instance[0]->Token, $instance[0]->Base_Url.''.$endPoint,$temp__Submitted);
						}
						//$result = $this->eloqua->putRequest($instance[0]->Token, $instance[0]->Base_Url.''.$endPoint.'/'.$hyperlinkJSON->id,$temp__Submitted);
						$this->updateNew_JSON_Asset($result,$Package_Item);
						$this->db->where('Deployment_Package_Item_Id',$Package_Item->Deployment_Package_Item_Id);
						$this->db->update('deployment_package_item',$data123);
					}					
					// echo '<br/> Put Request  Hyperlink ExternalURL <br/><pre>';
						// print_r($result);
					// echo '</pre>';
				}
				else if($hyperlinkJSON->type=='Hyperlink' && $hyperlinkJSON->hyperlinkType=='LandingPageURL')
				{	
					if(time()+1800 > $instance[0]->Token_Expiry_Time)
					{
					   $result2 = $this->eloqua->refreshToken($instance[0]);
					   $instance[0]->Token = $result2['Token']['access_token'];
					}
					
					if($Package_Item->verified == 1 && $Package_Item->isCircular != -1)
					{
						if(isset($hyperlinkJSON->id))
						{
							$result = $this->eloqua->putRequest($instance[0]->Token, $instance[0]->Base_Url.''.$endPoint.'/'.$hyperlinkJSON->id,$temp__Submitted);
						}
						else
						{
							$result = $this->eloqua->postRequest($instance[0]->Token, $instance[0]->Base_Url.''.$endPoint,$temp__Submitted);
						}
						//$result = $this->eloqua->putRequest($instance[0]->Token, $instance[0]->Base_Url.''.$endPoint.'/'.$hyperlinkJSON->id,$temp__Submitted);
						
						$this->updateNew_JSON_Asset($result,$Package_Item);
						$this->db->where('Deployment_Package_Item_Id',$Package_Item->Deployment_Package_Item_Id);
						$this->db->update('deployment_package_item',$data123);
					}					
					// echo '<br/> Put Request  Hyperlink Landing page<br/><pre>';
						// print_r($result);
					// echo '</pre>';
				}
				else 
				{
					if(time()+1800 > $instance[0]->Token_Expiry_Time)
					{
					   $result2 = $this->eloqua->refreshToken($instance[0]);
					   $instance[0]->Token = $result2['Token']['access_token'];
					}				
					
					if($Package_Item->verified == 1 && $Package_Item->isCircular != -1)
					{
						$this->logging_model->logT($Package_Item->Deployment_Package_Id,$Package_Item->Deployment_Package_Item_Id,'getDeploymentPackageValidation','Before POST Request 2',$temp__Submitted);
						
						$result = $this->eloqua->postRequest($instance[0]->Token, $instance[0]->Base_Url.''.$endPoint,$temp__Submitted);
						
						if($Package_Item->Asset_Type=='Form')
						{
							sleep(30);
						}	
					}					
				}
				// echo '<br/> Post Request 2<br/><pre>';
					// print_r(json_decode($result['body']));
				// echo '</pre>';
			}
			else
			{ 	
				//echo 'packageitem';
				
				if(time()+1800 > $instance[0]->Token_Expiry_Time)
				{
				   $result2 = $this->eloqua->refreshToken($instance[0]);
				   $instance[0]->Token = $result2['Token']['access_token'];
				}				
					
				if($Package_Item->verified == 1 && $Package_Item->isCircular != -1)
				{	
					// if($Package_Item->)
					// {
						// echo '<pre>';
						// print_r($Package_Item);
						// echo '<br>';
						// print_r($instance[0]->Base_Url.''.$endPoint.'/'.$Package_Item->Target_Asset_Id);
						// echo '<br>';
						// print_r($temp__Submitted);
					// }
					
					$this->logging_model->logT($Package_Item->Deployment_Package_Id,$Package_Item->Deployment_Package_Item_Id,'getDeploymentPackageValidation','Before PUT Request 2',$temp__Submitted);
					
					$result = $this->eloqua->putRequest($instance[0]->Token, $instance[0]->Base_Url.''.$endPoint.'/'.$Package_Item->Target_Asset_Id,$temp__Submitted);
					//Sleep 15 seconds for Forms
					if($Package_Item->Asset_Type=='Form')
					{
						// echo '<pre>';
							// print_r(json_decode($temp__Submitted));
						// echo '<pre>';
						sleep(30);
					}
				}
					
				// echo '<br/> Put Request 2<br/><pre>';
					// print_r($result['body']);
				// echo '</pre>';
			}
			if($Package_Item->verified == 1 && $Package_Item->isCircular != -1)
			{
				if($result['httpCode']==201 || $result['httpCode']==200)
				{	
					if($Package_Item->Asset_Type=='Form')
					{
						$Form_source_id=$Package_Item->Asset_Id;
						$result_form=$this->formHTMLBodyIdReplacement($result['body'],$Form_source_id);
						
						$this->logging_model->logT($Package_Item->Deployment_Package_Id,$Package_Item->Deployment_Package_Item_Id,'getDeploymentPackageValidation','after formHTMLBodyIdReplacement extra PUT for form',$result_form['result']);
						
						if($Package_Item->verified == 1 && $Package_Item->isCircular != -1)
						{
							$result = $this->eloqua->putRequest($instance[0]->Token, $instance[0]->Base_Url.''.$endPoint.'/'.$result_form['Target_Asset_Id'],$result_form['result']);
							//Sleep 15 seconds for Forms
							if($Package_Item->Asset_Type=='Form')
							{
								sleep(30);
							}
							// //print_r($result_form);
							// $data123['JSON_Submitted']=$result['body'];
							$data123['JSON_Submitted']=$result_form['result'];
						}
					}
				}
			}
			
			if($Package_Item->verified == 1 && $Package_Item->isCircular != -1)
			{
				$this->updateNew_JSON_Asset($result,$Package_Item);
				$this->db->where('Deployment_Package_Item_Id',$Package_Item->Deployment_Package_Item_Id);
				$this->db->update('deployment_package_item',$data123);
			}
			
			// unset($data123);
		}
	}
	
	function formHTMLBodyIdReplacement($result_temp,$Form_source_id)
	{
		$result_temp_=$result_temp;
		$temp__Submitted_formIdReplace=json_decode($result_temp_);
		
		// echo '<pre>Before<br>';
			// print_r($temp__Submitted_formIdReplace);
		// echo '<pre> After<br>';
		if($temp__Submitted_formIdReplace->type=='Form')
		{
			$Form_target_id=$temp__Submitted_formIdReplace->id;
			$Form_body=json_encode($temp__Submitted_formIdReplace);
			$data_json =$Form_body.'|||'.$Form_source_id. '|||'.$Form_target_id;
			$url = 'http://transporter.portqii.com:8080/Test1/SendBackWithFormIdChanged';
			$ch=curl_init();
			$headers = array('Content-Type:text/plain');
			// echo '<pre>';
				// //print_r($temp__Submitted_formIdReplace);
			// echo '</pre>';	
			curl_setopt($ch, CURLOPT_URL, $url); 
			curl_setopt($ch, CURLOPT_POST, true); 
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
			curl_setopt($ch, CURLOPT_POSTFIELDS,$data_json);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			$result_		= curl_exec($ch);
			$result_form['result']=$result_;
			$result_form['Target_Asset_Id']=$Form_target_id;
			$result=$result_form;
			// //print_r(json_decode($result_form['result']));
		}
		else
		{
			$result=$result_temp;
		}
		// echo '<pre>';
			// print_r($result);
		// echo '<pre> After<br>';
		return 	$result;
	}
	
	//Form Processing Steps Condition Custom Object Handler 06 Nov
	function CDO_FormPSCondition($JSON_Submitted,$Package_Item)
	{
		$JSON_Submitted_temp=json_decode($JSON_Submitted);
		$source_CDO_replace = array();
		$source_CDO_replace_temp=array();
		$t=0;
		$CDO_Replace_Flag=0;
		if($JSON_Submitted_temp->type=='Form' && isset($JSON_Submitted_temp->processingSteps))
		{
			$form_processingSteps = $JSON_Submitted_temp->processingSteps;
			$form_processingSteps_temp = $form_processingSteps;
			foreach($form_processingSteps as $psKey=>$psVal)
			{
				if(isset($psVal->condition))
				{
					if($psVal->condition->type=='ProcessingStepCondition' && isset($psVal->condition->conditionalFieldCriteria))
					{
						foreach($psVal->condition->conditionalFieldCriteria as $psKey1=>$psVal1)
						{
							if($psVal1->type=='CustomObjectFieldComparisonCriteria')
							{
								$source_CDO_replace_temp['fieldId']=$psVal1->fieldId;
								$source_CDO_replace_temp['conditionalCustomObjectId']=$psVal1->conditionalCustomObjectId;
								$source_CDO_replace[$t++] = $source_CDO_replace_temp;	
								$CDO_Replace_Flag=1;
							}
							else
							{
								$result=$JSON_Submitted_temp;					
							}							
						}
					}		
					else
					{
						$result=$JSON_Submitted_temp;					
					}
				}
				else
				{
					$result=$JSON_Submitted_temp;					
				}		
			}
			if($CDO_Replace_Flag==1)
			{
				$k=0;
				$target_fields_ids=array();
				foreach($source_CDO_replace as $psKey2=>$psVal2)
				{
					$target_fields_ids[$k++]=$this->get_FormPSCondition_CDOIDs($psVal2['conditionalCustomObjectId'],$psVal2['fieldId'],$Package_Item->Deployment_Package_Id);
				} 				
				$replaced_fields_FormData=$this->replace_FormPSCondition_CDOIDs($target_fields_ids,$form_processingSteps_temp);
				$JSON_Submitted_temp->processingSteps = $replaced_fields_FormData;
				$result = $JSON_Submitted_temp;
			}
			else
			{
				$result=$JSON_Submitted_temp;
			}		
		}	
		else
		{
			$result=$JSON_Submitted_temp;
		}	
		return json_encode($result);
	}
	
	function get_FormPSCondition_CDOIDs($customObjectId,$customObjectFieldId,$packageId)
	{
		$CDO_DB = $this->deploy_helper_model->CDO_from_DB($customObjectId,$packageId);
		$source_CDO=$CDO_DB['source_CDO'];
		$target_CDO=$CDO_DB['target_CDO'];	
		foreach($source_CDO->fields as $sKey=>$sVal)
		{
			if($sVal->id==$customObjectFieldId)
			{
				$source_field_name=$sVal->name;
				$source_field_id=$sVal->id;
			}	
		}
		foreach($target_CDO->fields as $tKey=>$tVal)
		{
			if($tVal->name==$source_field_name)
			{
				$target_field_id=$tVal->id;
			}	
		}
		$tCDO[$source_field_id] = $target_field_id;
		$tCDO[$source_CDO->id] = $target_CDO->id;
		return $tCDO;		
	}
	
	function replace_FormPSCondition_CDOIDs($target_fields_ids,$form_processingSteps_temp)
	{
		foreach($form_processingSteps_temp as $psKey=>$psVal)
		{
			if(isset($psVal->condition))
			{
				if($psVal->condition->type=='ProcessingStepCondition' && isset($psVal->condition->conditionalFieldCriteria))
				{
					foreach($psVal->condition->conditionalFieldCriteria as $psKey1=>$psVal1)
					{
						if($psVal1->type=='CustomObjectFieldComparisonCriteria')
						{	
							foreach($target_fields_ids as $tKey=>$tVal)
							{
								foreach($tVal as $tKey1=>$tVal1)
								{
									if($psVal1->fieldId==$tKey1)
									{
										$psVal1->fieldId=$tVal1;
									}
									if($psVal1->conditionalCustomObjectId==$tKey1)
									{
										$psVal1->conditionalCustomObjectId=$tVal1;
									}
								}
							}						
						}
					}
				}
			}	
		}	
		return $form_processingSteps_temp;
	}
	
	function updateNew_JSON_Asset($New_JSON_data,$Package_Item)
	{
		if($New_JSON_data['httpCode'] =='200' || $New_JSON_data['httpCode'] =='201')
		{
			$data['New_JSON_Asset'] = $New_JSON_data['Output_result'];
			$data['Error_Description'] = '';
			
			$actionRequiredMsgFlag= $this->actionRequiredMsg($Package_Item->Deployment_Package_Id,$Package_Item->Deployment_Package_Item_Id);
			
			// print_r($actionRequiredMsgFlag);
			// echo '<br>';
			// print_r($this->setSkeletonFlag);
			// echo '<br>';
			// print_r($Package_Item->Asset_Type);
			
			if($this->setSkeletonFlag==1 && ($Package_Item->Asset_Type=='Signature Rule' || $Package_Item->Asset_Type=='Form'||$Package_Item->Asset_Type=='Segment'||$Package_Item->Asset_Type=='Shared Filter' || $Package_Item->Asset_Type=='Campaign' || $Package_Item->Asset_Type=='Shared List' || $Package_Item->Asset_Type=='Landing Page' || $Package_Item->Asset_Type=='Shared Content' || $Package_Item->Asset_Type=='Dynamic Content') && $actionRequiredMsgFlag==1)
			{
				$data['Status'] = 'Action Required';
				//echo 'here';
			}	
			else
			{	
				if($this->getAssetCreatedFromShell($Package_Item->Deployment_Package_Id,$Package_Item->Deployment_Package_Item_Id)==1)
				{
					$data['Status'] = 'Action Required';
				}	
				else
				{
					$data['Status'] = 'Completed';
				}	
			}
			$this->setSkeletonFlag	= 0;		
			$Target_Asset_Id =  json_decode($New_JSON_data['body'])->id ;
			$valid_data['Target_Asset_Id'] = $Target_Asset_Id;
			$valid_data['Target_Asset_JSON'] = $New_JSON_data['body'];
			$this->db->where('Deployment_Package_Id',$Package_Item->Deployment_Package_Id);
			$this->db->where('Asset_Id',$Package_Item->Asset_Id);
			$this->db->where('Asset_Type',$Package_Item->Asset_Type);
			$this->db->update('deployment_package_validation_list',$valid_data);
			$this->db->where('Deployment_Package_Item_Id',$Package_Item->Deployment_Package_Item_Id);
			$this->db->update('deployment_package_item',$data);
		}
		//else if($New_JSON_data['httpCode'] =='500' || $New_JSON_data['httpCode'] =='400')
		else if($this->executionCounter<2)
		{
			$this->executionCounter=2;
			$package = $this->getPackage($Package_Item->Deployment_Package_Id);
			$instance = $this->getInstance($package[0]->Target_Site_Name);
			$getsearch_endpoints = $this->getalleloqua_endpoints();	
			$this->pre_deploy_package_item_branchOut($Package_Item,$instance,$getsearch_endpoints,$Package_Item->Deployment_Package_Id);
		}	
		else
		{
			$data['Error_Description'] = $New_JSON_data['Output_result'];
			$data['New_JSON_Asset'] = '';
			$data['Status'] = 'Errored';
			$this->db->where('Deployment_Package_Item_Id',$Package_Item->Deployment_Package_Item_Id);
			$this->db->update('deployment_package_item',$data);
		}		
	}
	
	function removeNegativeIdForGlobalContactFilterInSegment($temp__Submitted)
	{
		$temp_JSON = json_decode($temp__Submitted,true);
		if($temp_JSON['type']=='ContactSegment' && (strpos($temp__Submitted, 'ContactFilterSegmentElement') !== false || strpos($temp__Submitted, 'ContactListSegmentElement') !== false) && strpos($temp__Submitted, 'global') !== false)
		{
			if(isset($temp_JSON['elements']))
			{
				foreach($temp_JSON['elements'] as $key=>$val)
				{					
					if($val['type']=='ContactFilterSegmentElement' && $val['filter']['scope']=='global')
					{
						$temp_JSON['elements'][$key]['filter']['id']=str_replace('-','',$temp_JSON['elements'][$key]['filter']['id']);
					}	
					if($val['type']=='ContactListSegmentElement' && $val['list']['scope']=='global')
					{
						$temp_JSON['elements'][$key]['list']['id']=str_replace('-','',$temp_JSON['elements'][$key]['list']['id']);
					}
				}
				$result = $temp_JSON;				
			}
			else
			{
				$result = $temp_JSON;
			}
		}	
		else
		{
			$result = $temp_JSON;
		}	
		return json_encode($result);	
	}
	
	function callNodeEnd($packageId)
	{ 
		// create the cURL resource 
		// $url = urlencode($url);
		$url = "http://106.51.69.18:8080/updateMissing?packageId".$packageId;
		$ch = curl_init(); 
		
		// set cURL options 		
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HTTPGET, true);
			
			
		$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		$header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
		// execute request and retrieve the response 
		$data = curl_exec($ch); 	
		// print_r($data);
		$responseInfo = curl_getinfo($ch);
		// ////echo($responseInfo['http_code']);		exit;
		curl_close($ch);
		$result['data']=$data;
		// $result['data']= iconv("UTF-8", "ISO-8859-7//TRANSLIT", $data);
		$result['httpCode']=$responseInfo['http_code'];
		return $result; 
	}
	
	function callDependencyEnd($childAssetId,$temp__Submitted1,$Package_Item)
	{		
		$this->logging_model->logT($Package_Item->Deployment_Package_Id,$Package_Item->Deployment_Package_Item_Id,'callDependencyEnd','START','');		
		$formJSON=json_encode($temp__Submitted1);
		$data_json =$childAssetId.'|||'.$formJSON;
		$this->logging_model->logT($Package_Item->Deployment_Package_Id,$Package_Item->Deployment_Package_Item_Id,'callDependencyEnd','data_json',$data_json);
		//print_r($childAssetId);
		// echo '<br>';
		// echo '<pre>';
		// print_r($temp__Submitted1);
		// echo '</pre>';
		$url = 'https://dependency-dev.portqii.com/DependencyApp/removeAssetFromForm';
		$ch=curl_init();
		$headers = array('Content-Type:text/plain');
		curl_setopt($ch, CURLOPT_URL, $url); 
		curl_setopt($ch, CURLOPT_POST, true); 
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS,$data_json);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		$result_form		= curl_exec($ch);
		// echo '<pre>';	
		// print_r(json_decode($result_form));
		// echo '</pre>';
		$this->logging_model->logT($Package_Item->Deployment_Package_Id,$Package_Item->Deployment_Package_Item_Id,'callDependencyEnd','return result from ',$result_form);
		$this->logging_model->logT($Package_Item->Deployment_Package_Id,$Package_Item->Deployment_Package_Item_Id,'callDependencyEnd','END','');
		return 	$result_form;
	}
			 	
	function callTransporterJavaEnd($jsonSubmitted,$CFListToBeRemoved,$assetType,$Package_Item,$childAssetType)
	{ 
	
		$this->logging_model->logT($Package_Item->Deployment_Package_Id,$Package_Item->Deployment_Package_Item_Id,'callTransporterJaveEnd','START','');

		$data_json = json_encode($CFListToBeRemoved).'|||'.json_encode($jsonSubmitted).'|||'.$assetType;

		$this->logging_model->logT($Package_Item->Deployment_Package_Id,$Package_Item->Deployment_Package_Item_Id,'callTransporterJaveEnd','data_json',$data_json);
		
		
		//$data_json = json_encode($CFListToBeRemoved).'|'.json_encode($jsonSubmitted).'|'.$assetType;
		if($childAssetType=='Contact Field')
		{
			//$url = "https://appcloud-dev.portqii.com:8443/Transporter/removeCFFromAssets";
			$url = "http://transporter-dev.portqii.com:8080/Transporter/removeCFFromAssets";
		}
		if($childAssetType=='Picklist')
		{
			//$url = "https://appcloud-dev.portqii.com:8443/Transporter/removePicklistFromAssets";
			$url = "http://transporter-dev.portqii.com:8080/Transporter/removePicklistFromAssets";
		}
		if($childAssetType=='Campaign Field')
		{
			//$url = "https://transporter-dev.portqii.com:8443/Transporter/removeCampaignFieldFromAsset";
			$url = "http://transporter-dev.portqii.com:8080/Transporter/removeCampaignFieldFromAsset";
		}
		$ch=curl_init();
		$headers = array('Content-Type:text/plain');
		curl_setopt($ch, CURLOPT_URL, $url); 
		curl_setopt($ch, CURLOPT_POST, true); 
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS,$data_json);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		$resultData		= curl_exec($ch);
		curl_close($ch);
		
		// echo '<br>result<br>';
		// print_r($resultData);
		$this->logging_model->logT($Package_Item->Deployment_Package_Id,$Package_Item->Deployment_Package_Item_Id,'callTransporterJaveEnd','return result from ',$resultData);
		
		$this->logging_model->logT($Package_Item->Deployment_Package_Id,$Package_Item->Deployment_Package_Item_Id,'callTransporterJaveEnd','END','');
		
		return $resultData;   
	}	
	
	function getSourceAssetJSONById($AssetType,$AssetName,$packageId)
	{
		$Package_details = $this->deploy_model->getPackage($packageId);		
		$instance_details = $this->deploy_model->getInstance($Package_details[0]->Target_Site_Name);
		
		$token 	   = $instance_details [0] -> Token;
		$Base_Url  = $instance_details [0]-> Base_Url;	
		$targetOrg = $instance_details [0];
		$tokenExpiryTime = $instance_details [0]-> Token_Expiry_Time;
		
		$this -> db -> select('*');
		$this -> db -> from('rsys_asset_endpoint');
		$this -> db -> where('Asset_Type', $AssetType);
		$this -> db -> where('Endpoint_Type', 'Find Asset');
		$query22 = $this -> db -> get();
		$endpoint = $query22->result();

		//changes to asset name for special characters
		if(preg_match($this->pattern,  $AssetName))
		{
			$tempAssetName = str_replace('-', '_', $AssetName);
			$tempAssetName = preg_replace('/[^A-Za-z0-9\-]/','*',$tempAssetName);
		}
		else
			$tempAssetName=urlencode($AssetName);
		
		$url = $Base_Url .$endpoint[0]->Endpoint_URL .'?search="'.$tempAssetName .'"' ;
		
		// echo 'URL<br><pre>';
			// print_r ($url);
		// echo '</pre><br>';
		
		if(time()+1800 > $tokenExpiryTime)
		{
		   $result2 = $this->eloqua->refreshToken($instance_details[0]);
		   $token = $result2['Token']['access_token'];
		}			
		$result_asset = $this->eloqua->get_request($token, $url);
		$result_asset_=json_decode($result_asset['data']);
		
		$assetArray = array();
		$assetCount = 0;
		foreach($result_asset_->elements as $rtKey=>$rtVal)
		{
			if($rtVal->name==$AssetName)
			{
				  array_push($assetArray,$rtVal);
				  $assetCount++;
			}
		}
		$result_asset_->elements=$assetArray;
		$result_asset_->total=$assetCount;

		// echo 'returned assets<br><pre>';
			// print_r ($result_asset_);
		// echo '</pre><br>';		
		
		return 	$result_asset_;
	}
	
	function callTransporterJaveEndNew()
	{ 
		$url = "https://appcloud-dev.portqii.com:8443/Transporter/home_new";
		$ch=curl_init();
		//$headers = array('Content-Type:text/plain');
		curl_setopt($ch, CURLOPT_URL, $url); 
		curl_setopt($ch, CURLOPT_HTTPGET, true); 
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		//curl_setopt($ch, CURLOPT_POSTFIELDS,$data_json);
		//curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		$resultData		= curl_exec($ch);
		$responseInfo = curl_getinfo($ch);
		curl_close($ch);
			echo '<pre>';
				print_r($responseInfo);
				print_r($resultData);
			echo '</pre>';//print_r("---retrun data---<br>");
		print_r($resultData);
		return $resultData;   
	}

	function updateMissingAssetJsonAndStatus($packageId)
	{	
		$packageItemArray = array();
		$this -> db -> select('*');
		$this -> db -> from('deployment_package dp');
		$this -> db -> join('instance_ inst','dp.Target_Site_Name= inst.Site_Name', 'LEFT OUTER');
		$this -> db -> where('dp.Deployment_Package_Id', $packageId);
		// $this -> db -> where('dp.Created_By_Contact_Id', $Contact_Id);
		$this -> db -> limit(1);
		$orgQuery = $this -> db -> get();
		
		$org = $orgQuery->result()[0];
		
		$this -> db -> select('Deployment_Package_Item_Id,Asset_Id,Asset_Type,JSON_Asset,missing_target,verified,Status');
		$this -> db -> from('deployment_package_item');
		$this -> db -> where('Deployment_Package_Id', $packageId);
		$dpiQuery = $this -> db -> get();
		
		if($dpiQuery->num_rows()>0)
		{
			$packageItemArray = $dpiQuery->result();
			foreach($packageItemArray as $pKey=>$pVal)
			{
				if(($pVal->Status=='New' || $pVal->JSON_Asset==null || $pVal->JSON_Asset=='')&&($pVal->missing_target==1 || $pVal->verified==1))
				{	
					$assetType = $pVal->Asset_Type;
					$assetId = $pVal->Asset_Id;
					$deploymentPackageItemId = $pVal->Deployment_Package_Item_Id;
					
					$this -> db -> select('*');
					$this -> db -> from('rsys_asset_endpoint');
					$this -> db -> where('Asset_Type', $assetType);
					$this -> db -> where('Endpoint_Type', 'Read Single');
					$epQuery = $this -> db -> get();
					$endpoint = $epQuery->result();
					
					if($endpoint)
					{	
						$url = $org->Base_Url .$endpoint[0]->Endpoint_URL.'/'.$assetId;

						if(time()+1800 > $org->Token_Expiry_Time)
						{
						   $result2 = $this->eloqua->refreshToken($org);
						   $org->Token = $result2['Token']['access_token'];
						}
						$result = $this->eloqua->get_request($org->Token, $url);
						if($result['httpCode'] =='200')
						{
							$resultJson = json_decode($result['data']);
							$data1['JSON_Asset'] = json_encode($resultJson);
							$data1['Status'] = 'Valid';
							$this->db->where('Deployment_Package_Item_Id',$deploymentPackageItemId);
							$this->db->update('deployment_package_item',$data1);
						}
					}
				}
			}
		}
	}		

	function actionRequiredMsg($packageId,$packageItemId)
	{
		$this->db->select('ActionRequired_Asset_Message');
		$this->db->from('deployment_package_item');
		$this->db->where('Deployment_Package_Id',$packageId);
		$this->db->where('Deployment_Package_Item_Id',$packageItemId);
		$DPIQuery = $this->db->get();
		$DPIResult = $DPIQuery->result()[0];
		$returnResult=0;
		if(isset($DPIResult->ActionRequired_Asset_Message) && $DPIResult->ActionRequired_Asset_Message!=''){
			$returnResult=1;
		}
		return $returnResult;
	}
	
	function getAssetCreatedFromShell($packageId,$packageItemId)
	{
		$this->db->select('From_Shell');
		$this->db->from('deployment_package_item');
		$this->db->where('Deployment_Package_Id',$packageId);
		$this->db->where('Deployment_Package_Item_Id',$packageItemId);
		$DPIQuery = $this->db->get();
		$returnResult = $DPIQuery->result()[0]->From_Shell;		
		return $returnResult;
	}
}
?>