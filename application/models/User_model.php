<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class User_model extends CI_Model{
     function __construct()
     {
          // Call the Model constructor
          parent::__construct();
     }

     //read the department list from db
	function get_user_list()
	{
		$sql = 'select * from contact';
		$query = $this->db->query($sql);
		$result = $query->result();
		return $result;
	}
	 
	function get_user_info($id)
	{
		$this -> db -> select('User_Id,User_Name,Site_Id,Site_Name');
		$this -> db -> from('user_info');
		$this -> legacy_db -> where('contact_id', $id);
		$this -> legacy_db -> limit(1);
	    $query = $this -> legacy_db -> get();
	    $result = $query -> result();
        return $result;
	}
	
	function changeProfilePic($UserId, $profileimg)
	{
		$data['Image']= $profileimg;		
		$this -> db -> where('contact.contact_id', $UserId);
		$this -> db -> update('contact', $data);
		
		$this -> legacy_db -> where('contact.contact_id', $UserId);
		$this -> legacy_db -> update('contact', $data);
		return true;
	}
	
	function get_licence_id($email)
	{
		$this -> db -> select('instance_.License_Id');
		$this -> db -> from('instance_');
		$this -> db -> join('license lic',"lic.License_Id = instance_.License_Id","LEFT OUTER");
		$this -> db -> where('Email_Address', $email);
		$this -> db -> limit(1);
	    $query = $this -> db -> get();
	    $result = $query->result();
        return $result;
	}
	
	function login($username, $password)
	{
	   $this -> db -> select('id, email, first_name, last_name,	role_id,last_login');
	   $this -> db -> from('contact');
	   $this -> db -> where('email', $username);
	   $this -> db -> where('password', MD5($password));
	   $this -> db -> where('status', 1);
	   $this -> db -> limit(1);
	   $query = $this -> db -> get();
	   if($query -> num_rows() == 1)
	   {
		 return $query->result();
	   }
	   else
	   {
		 return false;
	   }
	}
	
	function change_password($userid, $old_password, $new_password, $confirm_new_password)
	{
		$this -> legacy_db -> select('contact_id, email, password, first_name, last_name');
		$this -> legacy_db -> from('Contact');
		$this -> legacy_db -> where('contact_id', $userid);
		$this -> legacy_db -> where('password', MD5($old_password));
		$this -> legacy_db -> limit(1);
		$query = $this -> legacy_db -> get();
		if($query -> num_rows() == 1)
		{	
			if($new_password == $confirm_new_password){
				$this -> legacy_db -> where('contact_id', $userid);
				$data = array('password' => MD5($new_password));
				$this -> legacy_db -> update('Contact' ,$data);
				return true;
			}
		}
		else
		{
			return false;
		}
	}

	function register_user($data)
	{
		if($data['accept'] == 'on' && $data['password'] == $data['confirmpassword'])
		{
			$this -> db -> select('id, email');
			$this -> db -> from('contact');
			$this -> db -> where('email', $data['email']);
			$query = $this -> db -> get();
			if($query -> num_rows() > 0)
			{
				return "This Email Already Exist.";
			}
			else
			{
				$tdata['first_name'] 	= $data['firstName'];
				$tdata['last_name'] 	= $data['lastName'];
				$tdata['email'] 		= $data['email'];
				$tdata['CompanyName'] 	= $data['companyName'];
				$tdata['mobile'] 		= $data['mobile'];		
				$tdata['password'] 		= md5($data['password']);
				$tdata['role_id'] 		= 2;
				$tdata['created_at'] 	= date('Y-m-d H:i:s');
				$this->db->insert('contact', $tdata);
				$insertId = $this->db->insert_id();
				$this->send_Email($insertId, $data['email']);
				
				return true;
			}
		}
		else
		{
			return false;
		}
	}
	
	function send_Email($userId, $email)
	{
		$this->load->helper('string');
		$resetcode = random_string('alnum', 40);				
		$this -> db -> where('id', $userId);
		$data = array('email_verificationCode' => $resetcode);
		$this->db->update('contact' ,$data);
				
		$this->load->library('email');
		$config['mailtype'] = 'html';
		$this->email->initialize($config);

		$this->email->from('no-reply@portqii.com', 'PortQii');
		$this->email->to($email); 	
		$this->email->subject('Verify Email');
		
		$reseturl = 'Thanks for signing up!
					Your account has been created, you can login with the following credentials 
					after you have activated your account by pressing the url below.
					Please click this link to activate your account:
					<a href='.base_url()."login/verifyEmail?id=".$userId."&code=".$resetcode.'>Click Here</a>';
		$this->email->message($reseturl);	
		$this->email->send();	
		return true;
	}
	
	function checkCode($id, $code)
	{
		$this->db->select('*');
		$this->db->from('contact');
		$this->db->where('id', $id);
		$this->db->where('PasswordResetCode', $code);
		$email = $this -> db -> get();
		if($email -> num_rows() > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	function updateCode($id, $code)
	{
		$this->db->select('*');
		$this->db->from('contact');
		$this->db->where('id', $id);
		$this->db->where('email_verificationCode', $code);
		$email = $this -> db -> get();
		if($email -> num_rows() > 0)
		{
			$data = array('email_verificationCode' => NULL, 'email_Verified' => 1 );
			$this->db->where('id', $id);
			$this->db->update('contact', $data);			
			return true;
		}
		else
		{
			return false;
		}
	}
	

	function resetPassword($username)
	{
		$this -> db -> select('id');
		$this -> db -> from('contact');
		$this -> db -> where('email', $username);		
		$query = $this -> db -> get();		
		if($query -> num_rows() == 1)
		{
			$result = $query->result();		
			$userid = $result[0]->id;
			$this->load->helper('string');
			$resetcode = random_string('alnum', 40);
			$this -> db -> where('id', $userid);
			$data = array('PasswordResetCode' => $resetcode);
			$this->db->update('contact' ,$data);
			
			$this->load->library('email');
			$config['mailtype'] = 'html';
   			$this->email->initialize($config);
			$this->email->from('no-reply@portqii.com', 'PortQii');
			$this->email->to($username); 	
			$this->email->subject('Password reset');
			$reseturl = '<br/><br/><a href="'.base_url().'login/verify?PasswordResetCode='.$resetcode.'&id='.$userid.'">'.base_url().'login/verify?PasswordResetCode='.$resetcode.'&id='.$userid.'</a>';
			$this->email->message('You have requested to reset password, Click the link to reset your password. '.$reseturl);	
			$this->email->send();	
			return true;	  
   
		}
		else
		{
			return false;
		}
	}
	function putPasswordReset($userid, $new_password, $code)
	{
		$this -> db -> select('id');
		$this -> db -> from('contact');
		$this -> db -> where('id', $userid);
		$query = $this -> db -> get();
		if($query -> num_rows() > 0)
		{
			$data = array('password' => MD5($new_password), 'PasswordResetCode' => NULL);
			$this -> db -> where('id', $userid);
			$this -> db -> update('contact' ,$data);
			return true;
		}
		else
		{
			return false;
		}
	}
	
	function editProfile($userId, $data)
	{
		$this -> db -> where('contact_id', $userId);
		$this->db->update('contact' ,$data);
		
		$this -> legacy_db -> where('contact_id', $userId);
		$this -> legacy_db -> update('contact' ,$data);
	}
	
	
}

?>