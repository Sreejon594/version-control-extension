<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require(APPPATH.'/third_party/json_path/vendor/autoload.php');
use Flow\JSONPath\JSONPath;
use Flow\JSONPath\JSONPathLexer;
use \Peekmo\JsonPath\JsonPath as PeekmoJsonPath;
use \Peekmo\JsonPath\JsonStore;
class Deploy_model_updated extends CI_Model
{
	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->load->model('eloqua','',TRUE);
		$this->load->model('deploy_helper_model','',TRUE);
		$this->load->model('logging_model','',TRUE);		
    }
	
	protected $globalStack = array();
	protected $childAssetCalls = array();
	public $finalSubmitted =null;
	protected static $source_url;
	protected static $target_url;
	protected $visited = array();
	protected $recursive = array();
	//protected $emailGroupRecursiveCalled = 0;   //in case of circular dependency involving emailgroup
	//read the department list from db
    function get_OrgList($id)
    {
		$this->db->select('id,users_id,base_url,userid,password');
		$this -> db -> from('Eloqua_Instance');
		$this -> db -> where('users_id',$id);
		$this -> db -> where('enable',1);
		$result = $this->db->get()->result();
		return $result;
    }
	
	function get_OrgInfo($id,$orgid)
    {
		$this->db->select('id,users_id,base_url,userid,password');
		$this -> db -> from('Eloqua_Instance');
		$this -> db -> where('users_id',$id);
		$this -> db -> where('id',$orgid);
		$this -> db -> where('enable',1);
		$result = $this->db->get()->result();
		return $result;
    }
	
	function new_Org($id ,$data)
    {
		$this->db->select('id');
		$this -> db -> from('Eloqua_Instance');
		$this -> db -> where('userid',$data['userid']);
		$temp =$this->db->count_all_results();
		if($temp==0)
		{									
			$this->db->select_max('id');
			$this -> db -> from('Eloqua_Instance');
			$temprow = $this->db->get()->result();
			$data['id']=$temprow[0]->id+1;	
			$data['enable']=1;	
			$data['users_id']= $id;	
			$this->db->insert('Eloqua_Instance', $data); 		
			$result['id'] = $data['id'];
			$result['success'] = true;		
			return $result;
		}
		else
		{
			$result['success'] = false;		
			$result['msg'] = 'login Name Can\'t be Duplicate.';		
			return $result;
		}
	}
	
	function delete_Org($id , $data)
    {
		$this->db->where_in('id', $data['id']);
		$this->db->where('users_id', $id);
		$this->db->delete('Eloqua_Instance');
	}
	
	function update_Org($id , $data)
    {
		$this->db->select('id');
		$this -> db -> from('Eloqua_Instance');
		$this -> db -> where('userid',$data['userid']);
		$this -> db -> where_not_in('id',$data['id']);
		$temp =$this->db->count_all_results();
		if($temp==0)
		{								
			$this->db->where('id', $data['id']);
			$this->db->update('Eloqua_Instance' ,$data);
			$result['success'] = true;		
			$result['msg'] = 'Account is Updated..';		
			return $result;
		}
		else
		{
			$result['success'] = false;		
			$result['msg'] = 'Login Name Can\'t be Duplicate.';		
			return $result;
		}
	}
	
	function get_Preference($id)
    {
		$this->db->select('id,user_id,Contact,Account,CDO,Segments,Filters,Emails,Forms,Landing_Pages ,Campaigns,Programs,Shared_Lists,Email_Groups,Option_Lists,Events,Contact_Views,Account_Views');
		$this -> db -> from('Asset_Type_Preference');
		$this -> db -> where('user_id',$id);
		//$this -> db -> where('enable',1);
		$result = $this->db->get()->result();
		return $result;
    }
	
	function update_Preference($id , $data)
    {
		$this->db->where('user_id', $id);
		$this->db->update('Asset_Type_Preference' ,$data);
		//return $this->db->_error_message();
		$result['success'] = true;		
		$result['msg'] = 'Account is Updated..';		
		return $result;
	}
	
	function deploy_inqueue($package_id)
    {
		$this -> db -> select('*');
		$this -> db -> from('deployment_queue');
		$this -> db -> where('Deployment_Package_Id',$package_id);
		$this -> db -> where('Status','In Queue');
		$this->db->limit(1);
		$query = $this -> db -> get();
	    $deployment_queue = $query->result();
		
		$date = date('Y-m-d H:i:s');
		$data['Deployment_Package_Id'] = $package_id;
		$data['Status'] = 'In Queue';
		$data['Rn_Create_Date'] = $date; 
		
		$data1['Status'] = 'Deployment In Queue';
		$this->db->where('Deployment_Package_Id',$package_id);
		$this->db->update('deployment_package',$data1);
		
		$msg="";
		try 
		{
			if(sizeof($deployment_queue)==0)
			{
				$this->db->insert('deployment_queue',$data);
				$msg = "success";
			}
			else{
				$msg = "alreadyinqueue";
			}
		} 
		catch (Exception $e) 
		{
			$msg = "Caught exception:".$e->getMessage();
		}		
		return $msg;
	}
	
	function deploy_queue()
    {
		$date = date('Y-m-d H:i:s');		
		$this->db->select('*');
		$this -> db -> from('deployment_queue');
		$this -> db -> where('Status','In Queue');
		$this->db->order_by("Rn_Create_Date", "asc");
		$query = $this -> db -> get();
	    $deployment_queue = $query->result();
		$msg="";
		foreach($deployment_queue as $key=>$val)
		{
			$data1['Status'] = 'In Progress'; 
			$data1['Start_'] = date('Y-m-d H:i:s');
			try 
			{  
				$this->db->where('Deployment_Queue_Id',$val->Deployment_Queue_Id);
				$this->db->update('deployment_queue',$data1);
				
				////echo '<br/>Validate Package Function<br/>';
				//$validateResult = $this->eloqua->validatepackage($val->Deployment_Package_Id);
				////echo '<br> Validate Package Result'.$validateResult;
				
				//echo '<br/>Validate Package Function<br/>';
				// $validateResult = $this->validatepackage_recursive($val->Deployment_Package_Id,1);
				$validateResult = $this->validatepackage_recursive($val->Deployment_Package_Id);
				//echo '<br> Validate Package Result'.$validateResult;
				
				//Handles the circular dependency which is identified during validate
				$this->handleCircularDependencybeforeDeploy($val->Deployment_Package_Id);
				//
				//echo '<br/>Set Deploy Package Status Function<br/>';
				$deployResult = $this->deploy_package($val->Deployment_Package_Id);
				//echo '<br> Validate Package Result'.$deployResult;
				
				//echo '<br/>Pre_Deploy_Package_Item Function<br/>';
				$predeployResult = $this->pre_deploy_package_item($val->Deployment_Package_Id,$val->Deployment_Queue_Id);			
				//echo '<br> Validate Package Result'.$predeployResult;
				
				// $this->handleCircularDependencyafterDeploy($val->Deployment_Package_Id);
				$msg = "success";
			} 
			catch (Exception $e) 
			{
				$msg = "Caught exception:".$e->getMessage();
			}		
		}	
		return $msg;		
	}
	
	function handleCircularDependencyafterDeploy($packageId)
	{
		$this -> db -> select('Base_Url, Token, Site_Name, Token_Expiry_Time, Refresh_Token');
		$this -> db -> from('instance_ inst');				
		$this -> db -> join('deployment_package dp','dp.Target_Site_Name = inst.Site_Name', 'LEFT OUTER');
		$this -> db -> join('deployment_package_item dpi','dpi.Deployment_Package_Id= dp.Deployment_Package_Id', 'LEFT OUTER');
		$this -> db -> where('dpi.Deployment_Package_Id', $packageId);
		$this -> db -> limit(1);
		$query1 		= $this -> db -> get();
		$token 		= $query1 -> result()[0] -> Token;
		$Base_Url 	= $query1 -> result()[0] -> Base_Url;		
		$org1	= $query1 -> result()[0];
		
		// print_r($org1);
		
		$this->db->select('*');
		$this->db->from('deployment_package_item dp');
		$this->db->where('Deployment_Package_Id',$packageId);
		$this->db->where('isCircular',-1);
		$this->db->order_by("Deploy_Ordinal", "desc");
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			$result = $query->result();
			// print_r('<pre>');
			// print_r($query->num_rows());
			
			// exit;
			
			foreach($result as $key=>$val)
			{
				$this->db->select('Minimal_JSON');
				$this->db->from('rsys_asset_type');
				$this->db->where('Asset_Type_Name',$val->Asset_Type);
				$query_at = $this->db->get();
				if($query_at->num_rows()>0)
				{
					$result_at = $query_at->result();
					$minimalJSON = $result_at[0]->Minimal_JSON;
					print_r('<pre>');
					// print_r('handleCircularDependency');
					// print_r('rsys_asset_type loop');
					$this -> db -> select('*');
					$this -> db -> from('rsys_asset_endpoint rae');
					$this -> db -> where('rae.Asset_Type', $val->Asset_Type);
					$this -> db -> where('rae.Endpoint_Type', 'Create');
					$query_dpvl = $this -> db -> get();
					$endpoint_dpvl = $query_dpvl->result();
					if($query_dpvl->num_rows()>0)
					{
						// print_r('rsys_asset_endpoint loop');
						$minimalJSON_array = json_decode($minimalJSON,true);
						$minimalJSON_array['name'] = $val->Asset_Name;
						$minimalJSON = json_encode($minimalJSON_array);
						
						$url = $Base_Url .$endpoint_dpvl[0]->Endpoint_URL; 
						print_r($url.'/'.$val->Target_Asset_Id );
						print_r('<br>');
						// print_r($val->JSON_Submitted);
						if(time()+1800 > $org1->Token_Expiry_Time)
						{
						   $result2 = $this->eloqua->refreshToken($org1);
						   $org1->Token = $result2['Token']['access_token'];
						}
						// $result = $this->eloqua->postRequest($org1->Token, $url,$minimalJSON);
						$result = $this->eloqua->putRequest($token, $url.'/'.$val->Target_Asset_Id,$val->JSON_Submitted);
						print_r($result );
						if($result['httpCode'] =='200' || $result['httpCode'] =='201')
						{
							
							// $Target_Asset_Id =  json_decode($result['body'])->id ;
							// $valid_data['Target_Asset_Id'] = $Target_Asset_Id;
							// $this->db->where('Deployment_Package_Id',$packageId);
							// $this->db->where('Asset_Id',$val->Asset_Id);
							// $this->db->where('Asset_Type',$val->Asset_Type);
							// $this->db->update('deployment_package_validation_list',$valid_data);
							
							// $data['missing_target'] = 0;
							// $data['Target_Asset_Id'] = $Target_Asset_Id;
							// $data['isCircular'] = -1;
							// $this->db->where('Deployment_Package_Item_Id',$val->Deployment_Package_Item_Id);
							// $this->db->update('deployment_package_item',$data);
						}
					}
				}
			}
		}
	}
	
	//Handles the circular dependency which is identified during validate
	function handleCircularDependencybeforeDeploy($packageId)
	{
		$this->logging_model->logT($packageId,0,'handleCircularDependencybeforeDeploy','start','');
		
		$this -> db -> select('Base_Url, Token, Site_Name, Token_Expiry_Time, Refresh_Token');
		$this -> db -> from('instance_ inst');				
		$this -> db -> join('deployment_package dp','dp.Target_Site_Name = inst.Site_Name', 'LEFT OUTER');
		$this -> db -> join('deployment_package_item dpi','dpi.Deployment_Package_Id= dp.Deployment_Package_Id', 'LEFT OUTER');
		$this -> db -> where('dpi.Deployment_Package_Id', $packageId);
		$this -> db -> limit(1);
		$query1 		= $this -> db -> get();
		$token 		= $query1 -> result()[0] -> Token;
		$Base_Url 	= $query1 -> result()[0] -> Base_Url;		
		$org1	= $query1 -> result()[0];
		
		// print_r($org1);
		
		$this->db->select('*');
		$this->db->from('deployment_package_item dp');
		$this->db->where('Deployment_Package_Id',$packageId);
		$this->db->where('isCircular >',0);
		// $this->db->where('missing_target',1);
		$this->db->order_by("Deploy_Ordinal", "desc");
		$query = $this->db->get();
		
		
		if($query->num_rows()>0)
		{
			$result = $query->result();
			// print_r('<pre>');
			// print_r($query->num_rows());
			// exit;
			$i=0;
			foreach($result as $key=>$val)
			{
				if($i==1)
				{
					break;
				}
				$i++;
				$this->db->select('Minimal_JSON');
				$this->db->from('rsys_asset_type');
				$this->db->where('Asset_Type_Name',$val->Asset_Type);
				$query_at = $this->db->get();
				if($query_at->num_rows()>0)
				{
					$result_at = $query_at->result();
					$minimalJSON = $result_at[0]->Minimal_JSON;
					print_r('<pre>');
					// print_r('handleCircularDependency');
					print_r('rsys_asset_type loop');
					$this -> db -> select('*');
					$this -> db -> from('rsys_asset_endpoint rae');
					$this -> db -> where('rae.Asset_Type', $val->Asset_Type);
					$this -> db -> where('rae.Endpoint_Type', 'Create');
					$query_dpvl = $this -> db -> get();
					$endpoint_dpvl = $query_dpvl->result();
					if($query_dpvl->num_rows()>0)
					{
						print_r('rsys_asset_endpoint loop');
						$minimalJSON_array = json_decode($minimalJSON,true);
						$minimalJSON_array['name'] = $val->Asset_Name;
						$minimalJSON = json_encode($minimalJSON_array);
						
						$url = $org1->Base_Url .$endpoint_dpvl[0]->Endpoint_URL; 
				
						if(time()+1800 > $org1->Token_Expiry_Time)
						{
						   $result2 = $this->eloqua->refreshToken($org1);
						   $org1->Token = $result2['Token']['access_token'];
						}
						if($val->missing_target == 0)
						{
							$result = $this->eloqua->putRequest($token, $url.'/'.$val->Target_Asset_Id,$val->JSON_Submitted);
						}
						else
						{
							$result = $this->eloqua->postRequest($org1->Token, $url,$minimalJSON);
						}
						
						// print_r($result );
						if($result['httpCode'] =='200' || $result['httpCode'] =='201')
						{
							
							$Target_Asset_Id =  json_decode($result['body'])->id ;
							$valid_data['Target_Asset_Id'] = $Target_Asset_Id;
							$this->db->where('Deployment_Package_Id',$packageId);
							$this->db->where('Asset_Id',$val->Asset_Id);
							$this->db->where('Asset_Type',$val->Asset_Type);
							$this->db->update('deployment_package_validation_list',$valid_data);
							
							$data['missing_target'] = 0;
							$data['Target_Asset_Id'] = $Target_Asset_Id;
							// $data['isCircular'] = -1;
							$data['Deploy_Ordinal'] = -1;
							$data['verified']  = 1;
							$this->db->where('Deployment_Package_Item_Id',$val->Deployment_Package_Item_Id);
							$this->db->update('deployment_package_item',$data);
						}
					}
				}
			}
		}
	}
	
	
	function validate_inqueue($package_id,$auto)
    {
		$this->db->select('*');
		$this -> db -> from('validate_queue');
		$this -> db -> where('Deployment_Package_Id',$package_id);
		$this -> db -> where('Status','In Queue');
		$this->db->limit(1);
		$query = $this -> db -> get();
	    $validate_queue = $query->result();
		
		$date = date('Y-m-d H:i:s');
		$data['Deployment_Package_Id'] = $package_id;
		$data['Status'] = 'Validate In Queue';
		$data['Rn_Create_Date'] = $date; 
		
		$data1['Status'] = 'Validate In Queue';
		$data1['auto_include_missing_assets'] = $auto;
		$this->db->where('Deployment_Package_Id',$package_id);
		$this->db->update('deployment_package',$data1);
		
		$msg="";
		try 
		{
			if(sizeof($validate_queue)==0)
			{
				$this->db->insert('validate_queue',$data);
				$msg = "success";
				return true;
			}
			else
			{
				$msg = "Already Inqueue";
				return true;
			}
			
		} 
		catch (Exception $e) 
		{
			$msg = "Caught exception:".$e->getMessage();
		}		
		return $msg;
	}
	
	//This function loops through the queue that are set for validation 
	function validate_queue_recursive()
	{
		
		$date = date('Y-m-d H:i:s');		
		$this->db->select('*');
		$this -> db -> from('validate_queue');
		$this -> db -> where('Status','Validate In Queue');
		$this->db->order_by("Rn_Create_Date", "asc");
		$query = $this -> db -> get();
	    $validate_queue = $query->result();
		$msg="";
		foreach($validate_queue as $key=>$val)
		{
			$data1['Status'] = 'In Progress'; 
			$data1['Start_'] = date('Y-m-d H:i:s');
			try 
			{  
				$this->db->where('Validate_Queue_Id',$val->Validate_Queue_Id);
				$this->db->update('validate_queue',$data1);				
				// $this->validatepackage_recursive($val->Deployment_Package_Id,0);
				$this->validatepackage_recursive($val->Deployment_Package_Id);
				
				$msg = "success";
				//print_r("success");
				$data1['Status'] = 'Processed'; 
				$data1['End_'] = date('Y-m-d H:i:s');
				$this->db->where('Validate_Queue_Id',$val->Validate_Queue_Id);
				$this->db->update('validate_queue',$data1);	
				
				
			} 
			catch (Exception $e) 
			{
				$msg = "Caught exception:".$e->getMessage();
			}		
		}		
		return $msg;
	}
	
	function validatepackage_recursive($packageId)
	{
		$this->logging_model->logT($packageId,0,'validatepackage_recursive','start','');
		$resetdata['Status'] = 'Validating';
		$this -> db -> where('Deployment_Package_Id', $packageId);
		$this -> db -> update('deployment_package_item', $resetdata);
		
		$this -> db -> select('*');
		$this -> db -> from('deployment_package_item dp');
		$this -> db -> where('dp.Deployment_Package_Id', $packageId);
		// if($fromDeploy == 1)
		// {
			// $this -> db -> where('dp.verified', 1);
		// }
		$query = $this -> db -> get();
	    $package_item = $query->result();
		$this->db->where('Deployment_Package_Id', $packageId);
		$this->db->delete('deployment_package_validation_list');
		foreach($package_item as $key=>$val)
		{
			$AssetChildData = $this->eloqua->getAssetChild($packageId,$val->Deployment_Package_Item_Id, $val->Asset_Id,$val->Asset_Type ,$Endpoint_Type = 'Read Single');
			
			if(is_array($AssetChildData) || is_object($AssetChildData))
			{
				foreach($AssetChildData as $ChildKey=>$ChildVal)
				{
					$this->logging_model->logT($packageId,$val->Deployment_Package_Item_Id,'validate_queue_recursive','inserting child into validation list','');
					$data['Deployment_Package_Item_Id'] = $val->Deployment_Package_Item_Id;
					$data['Deployment_Package_Id'] = $val->Deployment_Package_Id;
					$data['Asset_Type'] = $ChildVal['Asset_Type'];
					$data['Asset_Id'] = $ChildVal['Asset_Id'];
					$data['Asset_Name'] = $ChildVal['Display_Name'];
					$data['JSON_Node_Value'] = $ChildVal['JSON_Node_Value'];
					$data['Status'] = 'New';
					$this->db->insert('deployment_package_validation_list',$data);
				}
			}
		}
		
		foreach( $package_item as $key=>$val)
		{
			$this->buildGlobalStack($val,$packageId);
		}
		
		
		$this->updateMissingElements($packageId);
		$this->updateDeployOrdinal($packageId);
		$this->identifyCircular_initiate_updated($packageId);
		$this->deploy_helper_model->updateUnsupportedAssets($packageId);
		$this->deploy_helper_model->checkDuplicatehtmlName($packageId);
		
		$data2['Status'] = $this->decideStatusValidate($val->Deployment_Package_Id);
		$this->db->where('Deployment_Package_Id',$val->Deployment_Package_Id);
		$this->db->update('deployment_package',$data2);
		// $this->handleCircularDependency($packageId);
		$this->logging_model->logT($packageId,0,'validatepackage_recursive','end','');
	}
	
	function updatePackageStatus($packageId)
	{
		$this -> db -> select('*');
		$this -> db -> from('deployment_package_item dp');
		$this -> db -> where('dp.Deployment_Package_Id', $packageId);
		$query = $this -> db -> get();
		
		$missing_assets = false;
	    $package_item = $query->result();
		//print_r('<pre>');
		if($query->num_rows()>0)
		{
			foreach($package_item  as $key=>$val)
			{
				// print_r($val->missing_target);
				
				//print_r('   '.$val->verified.'  '.$val->Deployment_Package_Item_Id);
				//print_r('</br>');
				if($val->missing_target == 1 &&  $val->verified == 0)
				{
					$missing_assets = true;
					break;
				}
			}
		}
		if($missing_assets)
		{
			$tdata['Status'] = 'Missing Assets';
			$this->db->where('Deployment_Package_Id',$packageId);
			$this->db->update('deployment_package',$tdata);
		}
	}
	
	function updateDeployOrdinal($packageId)
	{
		$this->logging_model->logT($packageId,'',0,'updating deploy ordinal','');
		foreach($this->globalStack as $key=>$val)
		{
			$data['Deploy_Ordinal'] = $key;
			$this->db->where('Deployment_Package_Item_Id',$val);
			$this->db->update('deployment_package_item',$data);
		}
	}
	
	function updateMissingElements($packageId)
	{
		$this -> db -> select('*');
		$this -> db -> from('deployment_package_item dp');
		$this -> db -> where('dp.Deployment_Package_Id', $packageId);
		$query = $this -> db -> get();
	    $package_item = $query->result();
		
		$this -> db -> select('*');
		$this -> db -> from('deployment_package dp');
		$this -> db -> join('instance_ inst','dp.Target_Site_Name= inst.Site_Name', 'LEFT OUTER');
		$this -> db -> where('dp.Deployment_Package_Id', $packageId);
		// $this -> db -> where('dp.Created_By_Contact_Id', $Contact_Id);
		$this -> db -> limit(1);
		$query = $this -> db -> get();
		
		$org = $query->result()[0];
		
		foreach( $package_item as $key=>$val)
		{
			//update missing elements
			$this->logging_model->logT($packageId,$val->Deployment_Package_Item_Id,'updateMissingElements','','');
			$this -> db -> select('*');
			$this -> db -> from('rsys_asset_endpoint rae');
			$this -> db -> where('rae.Asset_Type', $val->Asset_Type);
			$this -> db -> where('rae.Endpoint_Type', 'Find Asset');
			$query = $this -> db -> get();
			$endpoint = $query->result();
			if($endpoint)
			{	
				$url = $org->Base_Url .$endpoint[0]->Endpoint_URL .'?search="'.str_replace(' ', '%20', $val->Asset_Name) .'"' ; 
				
				if(time()+1800 > $org->Token_Expiry_Time)
				{
				   $result2 = $this->eloqua->refreshToken($org);
				   $org->Token = $result2['Token']['access_token'];
				}
				$result = $this->eloqua->get_request($org->Token, $url);
				$result = json_decode($result['data']);
				if(isset($result->total) && $result->total >0)
				{
					if($result->total >1)
					{
						if($val->verified == 1)
						{
							// echo '<pre>';
							// print_r($result);
							// echo '</pre>';
							$duplicates=array();
							$duplicate;
							foreach($result->elements as $DKey=>$DVal)
							{
								$duplicate['Asset_Name']=$DVal->name;
								$duplicate['Asset_Id']=$DVal->id;
								array_push($duplicates,$duplicate);
							}
							$targetAssetDetails['duplicates_from_target'] = $duplicates;
							$targetAssetDetails['parent_asset_type'] = '';
							$targetAssetDetails['parent_asset_name'] = '';
							// echo '<pre>';
								// print_r(json_encode($duplicates));
							// echo '</pre>';
							$pdata['missing_target'] = 0;
							if($val->Target_Asset_Id==0 || $val->Target_Asset_Id=='')
							{
								$pdata['Status'] = 'Duplicate';
							}
							$pdata['verified'] = 1;
							$pdata['Target_Duplicate_Assets'] = json_encode($targetAssetDetails);
							$this->db->where('Deployment_Package_Item_Id',$val->Deployment_Package_Item_Id);
							$this->db->update('deployment_package_item',$pdata);
							unset($pdata);
						} 
						
						//too critical for deploy
						if($val->Target_Asset_Id > 0)
						{
							$tdata['Target_Asset_Id'] = $val->Target_Asset_Id;
							$this->db->where('Deployment_Package_Id',$val->Deployment_Package_Id);
							$this->db->where('Asset_Type',$val->Asset_Type);
							$this->db->where('Asset_Id',$val->Asset_Id);
							$this->db->update('deployment_package_validation_list',$tdata);
						}
						else
						{
							$tdata['Target_Asset_Id'] = $result->elements[0]->id;
							$this->db->where('Deployment_Package_Id',$val->Deployment_Package_Id);
							$this->db->where('Asset_Type',$val->Asset_Type);
							$this->db->where('Asset_Id',$val->Asset_Id);
							$this->db->update('deployment_package_validation_list',$tdata);
						}
						
						unset($tdata);
					}
					else
					{
						$pdata['missing_target'] = 0;
						$this->db->where('Deployment_Package_Item_Id',$val->Deployment_Package_Item_Id);
						$this->db->update('deployment_package_item',$pdata);
						unset($pdata);
						
						//too critical for deploy
						$tdata['Target_Asset_Id'] = $result->elements[0]->id;
						$this->db->where('Deployment_Package_Id',$val->Deployment_Package_Id);
						$this->db->where('Asset_Type',$val->Asset_Type);
						$this->db->where('Asset_Id',$val->Asset_Id);
						$this->db->update('deployment_package_validation_list',$tdata);
						
					}
				}
				else
				{
					$pdata['missing_target'] = 1;
					//$pdata['verified'] = $org->auto_include_missing_assets;
					$this->db->where('Deployment_Package_Item_Id',$val->Deployment_Package_Item_Id);
					$this->db->update('deployment_package_item',$pdata);
					//$pdata['missing_target'] = 1
					//$pdata['verified'] = $org->auto_include_missing_assets;
				}
			}
			
			
		}
		
		$this -> db -> select('*');
		$this -> db -> from('deployment_package_item dp');
		$this -> db -> where('dp.Deployment_Package_Id', $packageId);
		$this -> db -> where('verified',1);
		$query = $this -> db -> get();
	    $package_item1 = $query->result();
		
		$this -> db -> select('*');
		$this -> db -> from('deployment_package dp');
		$this -> db -> join('instance_ inst','dp.Target_Site_Name= inst.Site_Name', 'LEFT OUTER');
		$this -> db -> where('dp.Deployment_Package_Id', $packageId);
		// $this -> db -> where('dp.Created_By_Contact_Id', $Contact_Id);
		$this -> db -> limit(1);
		$query1 = $this -> db -> get();
		
		$org1 = $query1->result()[0];
		foreach($package_item1 as $key1=>$val1)
		{ 
			//This is to identify duplicates from one level child
			$this->db->select();
			$this->db->from('deployment_package_validation_list');
			$this->db->where('Deployment_Package_Item_Id',$val1->Deployment_Package_Item_Id);
			$query = $this->db->get();
			
			if($query->num_rows()>0)
			{
				$package_validation_list = $query->result();
				
				foreach($package_validation_list as $pvlkey=>$pvlval)
				{     
					$this -> db -> select('*');
					$this -> db -> from('rsys_asset_endpoint rae');
					$this -> db -> where('rae.Asset_Type', $pvlval->Asset_Type);
					$this -> db -> where('rae.Endpoint_Type', 'Find Asset');
					$query = $this -> db -> get();
					$endpoint_dpvl = $query->result();
					$url = $org1->Base_Url .$endpoint_dpvl[0]->Endpoint_URL .'?search="'.str_replace(' ', '%20', $pvlval->Asset_Name) .'"' ; 
				
					if(time()+1800 > $org1->Token_Expiry_Time)
					{
					   $result2 = $this->eloqua->refreshToken($org1);
					   $org1->Token = $result2['Token']['access_token'];
					}
					$result_dpvl = $this->eloqua->get_request($org1->Token, $url);
					if($val->Deployment_Package_Item_Id == 16168)
					{
						print_r($url);
					}
					$result_dpvl = json_decode($result_dpvl['data']);
					// print_r($result_dpvl->total);
					if(isset($result_dpvl->total) && $result_dpvl->total >1)
					{
						$duplicates_dpvl = array();
						$duplicate_dpvl;
						foreach($result_dpvl->elements as $DpvlKey=>$DpvlVal)
						{
							$duplicate_dpvl['Asset_Name']=$DpvlVal->name;
							$duplicate_dpvl['Asset_Id']=$DpvlVal->id;
							array_push($duplicates_dpvl,$duplicate_dpvl);
						}
						$targetAssetDetails_dpvl['duplicates_from_target'] = $duplicates_dpvl;
						$targetAssetDetails_dpvl['parent_asset_type'] = $val1->Asset_Type;
						$targetAssetDetails_dpvl['parent_asset_name'] = $val1->Asset_Name;
						$pdata['missing_target'] = 0;
						// if($val1->Target_Asset_Id==0 || $val1->Target_Asset_Id=='')
						// {
							// $pdata['Status'] = 'Duplicate';
						// }
						$pdata['verified'] = 1;
						$pdata['Target_Duplicate_Assets'] = json_encode($targetAssetDetails_dpvl);
						$this->db->where('Deployment_Package_Id',$val1->Deployment_Package_Id);
						$this->db->where('Asset_Type',$pvlval->Asset_Type);
						$this->db->where('Asset_Id',$pvlval->Asset_Id);
						$this->db->update('deployment_package_item',$pdata);
						unset($pdata);
						
						$pdata['Status'] = 'Duplicate';
						$this->db->where('Deployment_Package_Id',$val1->Deployment_Package_Id);
						$this->db->where('Asset_Type',$pvlval->Asset_Type);
						$this->db->where('Asset_Id',$pvlval->Asset_Id);
						$this->db->where('Target_Asset_Id',0);
						$this->db->update('deployment_package_item',$pdata);
						
						$this->db->select('*');
						$this->db->from('deployment_package_item');
						$this->db->where('Deployment_Package_Id',$val1->Deployment_Package_Id);
						$this->db->where('Asset_Type',$pvlval->Asset_Type);
						$this->db->where('Asset_Id',$pvlval->Asset_Id);
						$result_final = $this->db->get()->result();
						if($result_final[0]->Target_Asset_Id > 0)
						{
							$tempdata['Target_Asset_Id'] = $result_final[0]->Target_Asset_Id;
							$this->db->where('Deployment_Package_Id',$val1->Deployment_Package_Id);
							$this->db->where('Asset_Type',$pvlval->Asset_Type);
							$this->db->where('Asset_Id',$pvlval->Asset_Id);
							$this->db->update('deployment_package_validation_list',$tempdata);
							
						}
						unset($pdata);
					}
				}
			}
		}
		$this->updateVerifiedforMissingElements($packageId);
	}
	
	function updateVerifiedforMissingElements($packageId)
	{
		$this -> db -> select('*');
		$this -> db -> from('deployment_package_item dp');
		$this -> db -> where('dp.Deployment_Package_Id', $packageId);
		$query = $this -> db -> get();
	    $package_item = $query->result();
		
		$this -> db -> select('*');
		$this -> db -> from('deployment_package dp');
		$this -> db -> join('instance_ inst','dp.Source_Site_Name = inst.Site_Name', 'LEFT OUTER');
		$this -> db -> where('dp.Deployment_Package_Id', $packageId);
		// $this -> db -> where('dp.Created_By_Contact_Id', $Contact_Id);
		$this -> db -> limit(1);
		$query = $this -> db -> get();
		$org = $query->result()[0];
		
		foreach( $package_item as $key=>$val)
		{
			$this->logging_model->logT($packageId,$val->Deployment_Package_Item_Id,'updateVerifiedforMissingElements','','');
			$pdata['verified'] = $org->auto_include_missing_assets;
			$this->db->where('Deployment_Package_Item_Id',$val->Deployment_Package_Item_Id);
			$this->db->where('missing_target',1);
			$this->db->where('verified',0);
			$this->db->update('deployment_package_item',$pdata);
		}
	}
	
	function validate_queue()
    {		
		$date = date('Y-m-d H:i:s');		
		$this->db->select('*');
		$this -> db -> from('validate_queue');
		$this -> db -> where('Status','Validate In Queue');
		$this->db->order_by("Rn_Create_Date", "asc");
		$query = $this -> db -> get();
	    $validate_queue = $query->result();
		$msg="";
		foreach($validate_queue as $key=>$val)
		{
			$data1['Status'] = 'In Progress'; 
			$data1['Start_'] = date('Y-m-d H:i:s');
			try 
			{  
				$this->db->where('Validate_Queue_Id',$val->Validate_Queue_Id);
				$this->db->update('validate_queue',$data1);				
				$this->eloqua->validatepackage($val->Deployment_Package_Id);
				$msg = "success";
				$data1['Status'] = 'Processed'; 
				$data1['End_'] = date('Y-m-d H:i:s');
				$this->db->where('Validate_Queue_Id',$val->Validate_Queue_Id);
				$this->db->update('validate_queue',$data1);	
				
				$data2['Status'] = $this->decideStatusValidate($val->Deployment_Package_Id);
				$this->db->where('Deployment_Package_Id',$val->Deployment_Package_Id);
				$this->db->update('deployment_package',$data2);
			} 
			catch (Exception $e) 
			{
				$msg = "Caught exception:".$e->getMessage();
			}		
		}		
		return $msg;	
	}
	
	function decideStatusValidate($deploy_package_id)
	{
		$this->db->select('*');
		$this->db->from('deployment_package_item');
		$this->db->where('Deployment_Package_Id',$deploy_package_id);
		$query = $this->db->get();
		$totalRows = $query->num_rows();
		$errored = 0;
		$completed = 0;
		$unsupported = 0;
		$missing_assets = 0;
		$duplicates = 0;
		if($totalRows > 0)
		{
			$result = $query->result();
			foreach($result as $key=>$val)
			{
				if($val->Status == 'Invalid')
				{
					$errored++;
				}
				else if($val->Status == 'Unsupported')
				{
					$unsupported++;
				}
				else if($val->Status == 'Valid')
				{
					$completed++;
				}
				else if($val->Status == 'Duplicate')
				{
					$duplicates++;
				}
				if($val->missing_target == 1 &&  $val->verified == 0)
				{
					$missing_assets++;
				}
			}
		}
		if($errored>0)
		{
			return 'Invalid';
		}
		else if($unsupported>0)
		{
			return 'Unsupported';
		}
		else if($missing_assets>0)
		{
			return 'Missing Assets';
		}
		else if($duplicates>0)
		{
			return 'Duplicates Found';
		}
		else
		{
			return 'Validate Completed';
		}
	}
	
	
	function deploy_package($packageId)
    {
		$this->db->select('Status');
		$this->db->from('deployment_package');
		$this->db->where('Status','Validate Completed');
		$this->db->where('Deployment_Package_Id',$packageId);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			$data1['Status'] = 'Deployment In Queue';
			$this->db->where('Deployment_Package_Id',$packageId);
			$this->db->update('deployment_package',$data1);
		}
		
	}
	
	function invalid_deploy_package_item($packageId)
    {
		$this->db->select('*');
		$this -> db -> from('deployment_package');
		// $this -> db -> where('Deployment_Package_Id',$packageId);
		$where = "Deployment_Package_Id = ".$packageId." AND (Status='Missing Assets' OR Status='Unsupported' OR Status='Duplicates Found')";
		$this->db->where($where);
		//$this -> db -> where('Status','Missing Assets');
		$query123 = $this -> db -> get();
	    $invalid_deploy_package_item = $query123->result();
		// print_r('<pre>');
		// print_r($invalid_deploy_package_item);
		if(sizeof($invalid_deploy_package_item)>0)
		{
			return false;
		}
		else
		{	
			return true;
		}	
	}
	
	function updateValidStatus($package_item_id)
	{
		$data['Status'] = 'Valid';
		$this->db->where('Deployment_Package_Item_Id',$package_item_id);
		$this->db->update('deployment_package_item',$data);
	}
	
	function updateDPValidationList($package_item,$ChildVal)
	{
		$this->db->select('*');
		$this->db->from('deployment_package_validation_list');
		$this->db->where('Deployment_Package_Item_Id',$package_item->Deployment_Package_Item_Id);
		$this->db->where('Deployment_Package_Id',$package_item->Deployment_Package_Id);
		$this->db->where('Asset_Type',$ChildVal['Asset_Type']);
		$this->db->where('Asset_Id',$ChildVal['Asset_Id']);
		$this->db->where('Asset_Name',$ChildVal['Display_Name']);
		$query = $this->db->get();
		if($query->num_rows()==0)
		{
			$data['Deployment_Package_Item_Id'] = $package_item->Deployment_Package_Item_Id;
			$data['Deployment_Package_Id'] = $package_item->Deployment_Package_Id;
			$data['Asset_Type'] = $ChildVal['Asset_Type'];
			$data['Asset_Id'] = $ChildVal['Asset_Id'];
			$data['Asset_Name'] = $ChildVal['Display_Name'];
			$data['JSON_Node_Value'] = $ChildVal['JSON_Node_Value'];
			$data['Status'] = 'New';
			$this->db->insert('deployment_package_validation_list',$data);
		}
		
	}
	
	// function identifyCircular_initiate($packageId)
	// {
		// $data['isCircular'] = 0;
		// $this->db->where('Deployment_Package_Id',$packageId);
		// $this->db->update('deployment_package_item',$data);
		
		// $this->db->select('*');
		// $this->db->from('deployment_package_item');
		// $this->db->where('Deployment_Package_Id',$packageId);
		// $query = $this->db->get();
		// if($query->num_rows()>0)
		// {
			// $result_rows = $query->result();
			// foreach($result_rows as $key=>$val)
			// {
				// $this->identifyCircular($val,$packageId);
			// }
		// }
		// print_r('<pre>');
		// print_r($this->childAssetCalls);
	// }
	
	function identifyCircular_initiate_updated($packageId)
	{
		print_r('<pre>');
		// print_r('identifyCircular_initiate_updated');
		$data['isCircular'] = 0;
		$this->db->where('Deployment_Package_Id',$packageId);
		$this->db->update('deployment_package_item',$data);
		
		$this->db->select('*');
		$this->db->from('deployment_package_item');
		$this->db->where('Deployment_Package_Id',$packageId);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			$result_rows = $query->result();
			foreach($result_rows as $key1=>$val1)
			{
				$this->visited[$val1->Deployment_Package_Item_Id] = false;
				$this->recursive[$val1->Deployment_Package_Item_Id] = false;
			}
			foreach($result_rows as $key=>$val)
			{
				$temp = $this->identifyCircular_new($val,$packageId);
				// print_r($temp);
			}
		}
	}
	
	function identifyCircular_new($val,$packageId)
	{
		if($this->visited[$val->Deployment_Package_Item_Id] != true)
		{
			$this->visited[$val->Deployment_Package_Item_Id] = true;
			$this->recursive[$val->Deployment_Package_Item_Id] = true;
			$childAssetArray = $this->eloqua->getAssetChild($val->Deployment_Package_Id,$val->Deployment_Package_Item_Id,$val->Asset_Id,$val->Asset_Type);
			if((is_array($childAssetArray) ||  is_object($childAssetArray)) && sizeof($childAssetArray)>0)
			{
				if(sizeof($childAssetArray)>0)
				{
					for($i=0;$i<sizeof($childAssetArray);$i++)
					{
						$asset_type = $childAssetArray[$i]['Asset_Type'];
						$asset_id = $childAssetArray[$i]['Asset_Id'];
						$childPackageItem = $this->getPackageItem($packageId,$asset_type,$asset_id);
						if($childPackageItem!=false)
						{
							if ( !$this->visited[$childPackageItem->Deployment_Package_Item_Id] && $this->identifyCircular_new($childPackageItem,$packageId) )
							{
								
								print_r('<br> child '.$childPackageItem->Asset_Type.' -> '.$childPackageItem->Asset_Name);
								print_r('<br> parent '.$val->Asset_Type.' -> '.$val->Asset_Name);
								print_r('<br>');
								print_r('<br>');
								$data['isCircular'] = $val->Deployment_Package_Item_Id;
								$this->db->where('Deployment_Package_Item_Id',$childPackageItem->Deployment_Package_Item_Id);
								$this->db->update('deployment_package_item',$data);
									return true;
							}	
							else if ($this->recursive[$childPackageItem->Deployment_Package_Item_Id])
							{
								print_r('<br> child '.$childPackageItem->Asset_Type.' -> '.$childPackageItem->Asset_Name);
								print_r('<br> parent '.$val->Asset_Type.' -> '.$val->Asset_Name);
								print_r('<br>');
								print_r('<br>');
								
								$data['isCircular'] = $val->Deployment_Package_Item_Id;
								$this->db->where('Deployment_Package_Item_Id',$childPackageItem->Deployment_Package_Item_Id);
								$this->db->update('deployment_package_item',$data);
								return true;
							}
									
						}
					}
				}
			}
			
			
		}
		$this->recursive[$val->Deployment_Package_Item_Id] = false;  // remove the vertex from recursion stack
		return false;
	}
	
	//Identifying the circular dependency
	function identifyCircular($package_item_fixed,$packageId_fixed)
	{	
		$packageItems = array();
		$package_item = $package_item_fixed;
		while($package_item!=null )
		{
			print_r('<pre><br>');
			print_r('inside while '.$package_item->Asset_Type);
			$childAssetArray = $this->eloqua->getAssetChild($package_item->Deployment_Package_Id,$package_item->Deployment_Package_Item_Id,$package_item->Asset_Id,$package_item->Asset_Type);
			if((is_array($childAssetArray) ||  is_object($childAssetArray)) && sizeof($childAssetArray)>0)
			{
				if(sizeof($childAssetArray)>0)
				{
					for($i=0;$i<sizeof($childAssetArray);$i++)
					{
						$asset_type = $childAssetArray[$i]['Asset_Type'];
						$asset_id = $childAssetArray[$i]['Asset_Id'];
						$childPackageItem = $this->getPackageItem($packageId_fixed,$asset_type,$asset_id);
						if($childPackageItem!=false)
						{
							if(($childPackageItem->Asset_Type == $package_item_fixed->Asset_Type) &&($childPackageItem->Asset_Id  == $package_item_fixed->Asset_Id))
							{
								print_r('<pre><br>');
								print_r('matchfound');
								print_r($package_item->Asset_Type);
								$data['isCircular'] = $package_item->Deployment_Package_Item_Id;
								$this->db->where('Deployment_Package_Item_Id',$package_item_fixed->Deployment_Package_Item_Id);
								$this->db->update('deployment_package_item',$data);
								$package_item = null;
							}
							else
							{
								if(!in_array($childPackageItem->Deployment_Package_Item_Id, $packageItems))
								{
									array_push($packageItems,$childPackageItem->Deployment_Package_Item_Id);
									$package_item = $childPackageItem;
								}
								else
								{
									$package_item = null;
								}
								
							}
							
						}
						else
						{
							$package_item = null;
						}
					}
				}
				else
				{
					$package_item = null;
				}
			}
			else
			{
				$package_item = null;
			}
		}
	}
	
	function buildGlobalStack($package_item,$packageId)
	{
		
		// print_r('<pre><br>');
		// print_r('if');
		// print_r('<br>');
		// print_r($package_item->Deployment_Package_Item_Id);
		$this->logging_model->logT($packageId,$package_item->Deployment_Package_Item_Id,'buildGlobalStack','building a recursive stack','');
		$this->putOnGlobalStack($package_item->Deployment_Package_Item_Id);
		$this->updateValidStatus($package_item->Deployment_Package_Item_Id);
		$childAssets = $this->eloqua->getAssetChild($package_item->Deployment_Package_Id,$package_item->Deployment_Package_Item_Id,$package_item->Asset_Id,$package_item->Asset_Type);
		//print_r(count($childAssets));
		if(is_array($childAssets) || is_object($childAssets))
		{
			foreach($childAssets as $ChildKey=>$ChildVal)
			{
				$this->updateDPValidationList($package_item,$ChildVal);
			}
		}
		if((is_array($childAssets) ||  is_object($childAssets)) && sizeof($childAssets)>0)
		{
			$childAssetArray = $childAssets;
			if(sizeof($childAssetArray)>0)
			{
				for($i=0;$i<sizeof($childAssetArray);$i++)
				{
					$asset_type = $childAssetArray[$i]['Asset_Type'];
					$asset_id = $childAssetArray[$i]['Asset_Id'];
					$childPackageItem = $this->getPackageItem($packageId,$asset_type,$asset_id);
					if($childPackageItem!=false)
					{
						//if($package_item->Asset_Type == 'Email Group' && $emailGroupRecursiveCalled == 0) //in case of circular dependency involving email group
						if($package_item->Asset_Type == 'Email Group')
						{
							// $emailGroupRecursiveCalled = 1; //in case of circular dependency involving email group
							$this->buildGlobalStack($childPackageItem,$packageId);
						}
					}
					else
					{
						$pdata['Asset_Id'] = $asset_id;
						$pdata['Asset_Name'] = $childAssetArray[$i]['Display_Name'];
						$pdata['Asset_Type'] = $asset_type;
						$pdata['Deployment_Package_Id'] = $packageId;
						$this->changeset->addtopackage(0,$pdata);
						$childPackageItem = $this->getPackageItem($packageId,$asset_type,$asset_id);
						$this->buildGlobalStack($childPackageItem,$packageId);
					}
				}
			}
		}
		
		
	}
	
	function getPackageItem($packageId,$AssetType,$AssetId)
	{
		$this->db->select('*');
		$this -> db -> from('deployment_package_item');
		$this -> db -> where('Deployment_Package_Id',$packageId);
		$this->db->where('Asset_Type',$AssetType);
		$this->db->where('Asset_Id',$AssetId);
		$query = $this -> db -> get();
		$deployment_package_items = $query->result();
		if($query->num_rows()>0)
		{
			return $deployment_package_items[0];
		}
		return false;
	}
	
	function putOnGlobalStack($currentAsset)
	{
		$isPresent = array_search($currentAsset, $this->globalStack);
		if($isPresent  !== FALSE)
		{
			unset($this->globalStack[array_search($currentAsset, $this->globalStack)]);
		}
		array_push($this->globalStack,$currentAsset);
	}
	
	function isAssetChildCalled($currentAsset)
	{
		$isPresent = array_search($currentAsset,$this->childAssetCalls);
		if($isPresent !== FALSE)
		{
			// print_r('<pre><br>');
			// print_r('already there'.$currentAsset);
			// print_r('<br>');
			array_push($this->childAssetCalls,$currentAsset);
			return 1;
		}
		else
		{
			// print_r('<pre><br>');
			// print_r('inserting there'.$currentAsset);
			// print_r('<br>');
			array_push($this->childAssetCalls,$currentAsset);
			return 0;
		}
	}
	
	function pre_deploy_package_item($packageId,$queueId)
	{
		// echo '<pre>';
			// print_r($JSON_Asset);
			// print_r('hello');
		// echo '</pre>';
		$this->logging_model->logT($packageId,0,'pre_deploy_package_item','start','');
		$invalid_package_item = $this->invalid_deploy_package_item($packageId);
		$status_details = '';
		
		if($invalid_package_item)
		{
			$getsearch_endpoints = $this->getalleloqua_endpoints();	
			$package = $this->getPackage($packageId);
			// print_r($packageId);
			$instance = $this->getInstance($package[0]->Target_Site_Name);
					
			$this->db->select('*');
			$this -> db -> from('deployment_package_item');
			$this -> db -> where('Deployment_Package_Id',$packageId);
		   // $this -> db -> where('verified',1);
			$this->db->order_by('Deploy_Ordinal', 'desc');
			$query123 = $this -> db -> get();
			$deployment_package_item = $query123->result();
			
			$valid_err = false;
			//print_r('<pre>');
			$package_item_arr = array();
			foreach($deployment_package_item as $key=>$val)
			{
				if(sizeof($instance)>0 )
				{
					if($val->Asset_Type != 'Image' && $val->Asset_Type != 'File Storage')
					{
						if($val->JSON_Asset!=null)
						{
							$JSON_Asset = json_decode($val->JSON_Asset);
							// echo '<pre>';
								// print_r($val);
							// echo '</pre>';
							$this->logging_model->logT($packageId,$val->Deployment_Package_Item_Id,'pre_deploy_package_item','json modificaton start',$JSON_Asset);
							$result = $this->eloqua->get_request($instance[0]->Token, $instance[0]->Base_Url.''.$getsearch_endpoints[str_replace(' ', '', $val->Asset_Type).'_FindAsset']->Endpoint_URL.'?search="'.urlencode($JSON_Asset->name).'"');
							
							// print_r($result);
							$result_decode = json_decode($result['data']);	
							
							$xJsonIdReplace = null;
							
							if(isset($result_decode->total))
							{
								if($result_decode->total >0)
								{
									$JSON_Asset = $this->getExclusionResult(json_decode($getsearch_endpoints[str_replace(' ', '', $val->Asset_Type).'_Update']->JSON_Body_Exclusion),$JSON_Asset);
									$this->logging_model->logT($packageId,$val->Deployment_Package_Item_Id,'pre_deploy_package_item','After getExclusionResult Update',$JSON_Asset);
									
									$JSON_Asset = $this->replaceJSONKey(json_decode($getsearch_endpoints[str_replace(' ', '', $val->Asset_Type).'_Update']->JSON_Key_Replace),$JSON_Asset);
									$this->logging_model->logT($packageId,$val->Deployment_Package_Item_Id,'pre_deploy_package_item','After replaceJSONKey Update',$JSON_Asset);
									
									$JSON_Asset = $this->ChangeParaValue($JSON_Asset,$getsearch_endpoints[str_replace(' ', '', $val->Asset_Type).'_Update']->Change_Para_Config);
									$this->logging_model->logT($packageId,$val->Deployment_Package_Item_Id,'pre_deploy_package_item','After ChangeParaValue Update',$JSON_Asset);
									
									if(is_array($JSON_Asset))
									{
										foreach($JSON_Asset as $key1=>$val1)
										{
											if($key1=='id'){
												if($result_decode->total > 1)
												{
													$xJsonIdReplace[$key1]=$val->Target_Asset_Id;		
												}
												else
												{
													$xJsonIdReplace[$key1]=$result_decode->elements[0]->id;		
												}
																			
											}
											else
											{
												$xJsonIdReplace[$key1]=$val1;
											}
											
										}  
									}
									$data1['JSON_Submitted'] = json_encode($xJsonIdReplace);
									if($result_decode->total > 1)
									{
										$data1['Target_Asset_Id'] = $val->Target_Asset_Id;
									}
									else
									{
										$data1['Target_Asset_Id'] = $result_decode->elements[0]->id;
									}
									
									// print_r('Test');
									// print_r($data1['Target_Asset_Id']);
									
								}
								else
								{
									$JSON_Asset = $this->getExclusionResult(json_decode($getsearch_endpoints[str_replace(' ', '', $val->Asset_Type).'_Create']->JSON_Body_Exclusion),$JSON_Asset);
									$this->logging_model->logT($packageId,$val->Deployment_Package_Item_Id,'pre_deploy_package_item','After getExclusionResult Create',$JSON_Asset);
								
									$JSON_Asset = $this->replaceJSONKey(json_decode($getsearch_endpoints[str_replace(' ', '', $val->Asset_Type).'_Create']->JSON_Key_Replace),$JSON_Asset);
									$this->logging_model->logT($packageId,$val->Deployment_Package_Item_Id,'pre_deploy_package_item','After replaceJSONKey Create',$JSON_Asset);
									
									$JSON_Asset = $this->ChangeParaValue($JSON_Asset,$getsearch_endpoints[str_replace(' ', '', $val->Asset_Type).'_Create']->Change_Para_Config);
									$this->logging_model->logT($packageId,$val->Deployment_Package_Item_Id,'pre_deploy_package_item','After ChangeParaValue Create',$JSON_Asset);

									$data1['JSON_Submitted'] = json_encode($JSON_Asset);
									$data1['Target_Asset_Id'] = 0;
								}
							}
							else
							{
								$JSON_Asset = $this->getExclusionResult(json_decode($getsearch_endpoints[str_replace(' ', '', $val->Asset_Type).'_Create']->JSON_Body_Exclusion),$JSON_Asset);
								$this->logging_model->logT($packageId,$val->Deployment_Package_Item_Id,'pre_deploy_package_item','After getExclusionResult Create_Else',$JSON_Asset);
								
								$JSON_Asset = $this->replaceJSONKey(json_decode($getsearch_endpoints[str_replace(' ', '', $val->Asset_Type).'_Create']->JSON_Key_Replace),$JSON_Asset);
								$this->logging_model->logT($packageId,$val->Deployment_Package_Item_Id,'pre_deploy_package_item','After replaceJSONKey Create_Else',$JSON_Asset);
								
								$JSON_Asset = $this->ChangeParaValue($JSON_Asset,$getsearch_endpoints[str_replace(' ', '', $val->Asset_Type).'_Create']->Change_Para_Config);
								$this->logging_model->logT($packageId,$val->Deployment_Package_Item_Id,'pre_deploy_package_item','After ChangeParaValue Create_Else',$JSON_Asset);
								
								$data1['JSON_Submitted'] = json_encode($JSON_Asset);
								$data1['Target_Asset_Id'] = 0;
								
							}

							$data1['JSON_Submitted'] = $this->replaceSiteName_image($data1['JSON_Submitted'],$packageId);
			
							if(is_array($data1['JSON_Submitted']) || is_object($data1['JSON_Submitted']))
							{
								$data1['JSON_Submitted'] = json_encode($data1['JSON_Submitted']);
							}
							
							$this->db->where('Deployment_Package_Item_Id',$val->Deployment_Package_Item_Id);
							$this->db->update('deployment_package_item',$data1);
							
							$val->Target_Asset_Id = $data1['Target_Asset_Id'];
							$val->JSON_Submitted = $data1['JSON_Submitted'];
							
							$this->getDeploymentPackageValidation($val,$data1['JSON_Submitted'],$instance,$getsearch_endpoints[str_replace(' ', '', $val->Asset_Type).'_Update']->Endpoint_URL);
						}
					}
					else
					{
						//add only verified elements
						if($val->verified == 1 && $val->isCircular!=-1)
						{
							if($val->Asset_Type == 'File Storage')
							{
								
								$this->post_files($val);
							}
							else 
							{
								$this->post_image($val);
							}
						}
						
						
					}
				}
			}
			
			$data['Status'] = $this->decideStatus($packageId);
			$this->db->where('Deployment_Package_Id',$packageId);
			$this->db->update('deployment_package',$data);
			
			$dqdata1['Status'] = 'Processed';
			$dqdata1['End_'] = date('Y-m-d H:i:s');
			$this->db->where('Deployment_Queue_Id',$queueId);
			$this->db->update('deployment_queue',$dqdata1);
		}
		else
		{
			// $data['Status'] = 'Errored';
			// $data['Status_Details'] =$status_details ;
			// $this->db->where('Deployment_Package_Id',$packageId);
			// $this->db->update('deployment_package',$data);
			
			$data1['Status'] = 'Processed � Error';
			$data1['End_'] = date('Y-m-d H:i:s');
			$this->db->where('Deployment_Queue_Id',$queueId);
			$this->db->update('deployment_queue',$data1);
			
		}
	}
	
	public function replaceSiteName_image_copy($submitted_JSON,$packageId)
	{
		$submitted_JSON1='';
		$submitted_JSON=json_decode($submitted_JSON);
	
		if(isset($submitted_JSON->images) && (!empty($submitted_JSON->images)))
		{	
			$target_Images = $this->searchAssets_targetInstance($packageId,$Asset_Type='Image');
			//print_r($target_Images); exit;
			$submitted_JSON1= $submitted_JSON->images;
			foreach($submitted_JSON1 as $key=>$val)
			{
				foreach($target_Images->elements as $key1=>$val1)
				{
					if($val->name == $val1->name)
					{
						deploy_model::$source_url = $val->fullImageUrl;
						deploy_model::$target_url = $val1->fullImageUrl;
						
						if(isset($val1->thumbnailUrl)&&isset($val->thumbnailUrl))
						{
							$target_thumbnail_url = $val1->thumbnailUrl;
							$source_thumbnail_url = $val->thumbnailUrl;
							$val->thumbnailUrl = str_replace($source_thumbnail_url,$target_thumbnail_url,$val->thumbnailUrl);
						}
						
						$newArray = json_decode(json_encode($submitted_JSON),true);
						
						if(is_array($newArray))
						{
							array_walk_recursive($newArray,'deploy_model::test_print');
							$newArray = $this->getImageUrlsReplaced($newArray);
							$submitted_JSON = json_encode($newArray);
							$newArray_temp=json_decode($submitted_JSON );
							$image_url_replace=$newArray_temp->images;
							foreach($image_url_replace as $imgKey=>$imgVal)
							{
								$imgVal->thumbnailUrl=$val->thumbnailUrl;
								$imgVal->fullImageUrl=deploy_model::$target_url;
							}
							$newArray_temp->images=$image_url_replace;
							$submitted_JSON = json_encode($newArray_temp);
						}	
						else
						{	
							$newArray_temp=json_decode($newArray);
							$image_url_replace=$newArray_temp->images;
							foreach($image_url_replace as $imgKey=>$imgVal)
							{
								$imgVal->thumbnailUrl=$val->thumbnailUrl;
								$imgVal->fullImageUrl=deploy_model::$target_url;
							}
							$newArray_temp->images=$image_url_replace;
							$submitted_JSON = json_encode($newArray_temp);
						}	
					}					
					
				}
				
			}
		}
		else
		{
			$submitted_JSON = json_encode($submitted_JSON);
		}
		return $submitted_JSON;
	}
	
	public function replaceSiteName_image($submitted_JSON,$packageId)
	{
		$submitted_JSON2=$submitted_JSON;
		$submitted_JSON=json_decode($submitted_JSON,true);
		
		if(isset($submitted_JSON['images']) && (!empty($submitted_JSON['images'])))
		{	
			$target_Images = $this->searchAssets_targetInstance($packageId,$Asset_Type='Image');
			$target_Images = json_decode(json_encode($target_Images),true);
			$submitted_JSON1= $submitted_JSON['images'];
			foreach($submitted_JSON1 as $key=>$val)
			{
				foreach($target_Images['elements'] as $key1=>$val1)
				{
					if($val['name'] == $val1['name'])
					{
						deploy_model::$source_url = $val['fullImageUrl'];
						deploy_model::$target_url = $val1['fullImageUrl'];
						$submitted_JSON = $this->getImageUrlsReplaced($submitted_JSON,$val['thumbnailUrl'],$val1['thumbnailUrl']);
						$submitted_JSON = $this->getImageUrlsReplaced($submitted_JSON,$val['fullImageUrl'],$val1['fullImageUrl']);
					}					
					
				}
				
			}
		}
		$submitted_JSON = json_encode($submitted_JSON);
		return $submitted_JSON;
	}
	
	public static function test_print(&$item, $key)
	{
		$item = str_replace(deploy_model::$source_url,deploy_model::$target_url,$item);
	}
	
	public function getImageUrlsReplaced($newArray,$sourceUrl,$targetUrl)
	{
		$data = array();
		$sourceUrl = addcslashes($sourceUrl,'/');
		$targetUrl = addcslashes($targetUrl,'/');
		$url = 'http://transporter.portqii.com:8080/Test1/ReplaceSourceWithTarget';
		$submitted = json_encode($newArray);
		$data_json =$submitted.'|||'.$sourceUrl.'|||'.$targetUrl ;
		// print_r($data_json);
		$ch=curl_init();
		$headers = array('Content-Type:text/plain');
		curl_setopt($ch, CURLOPT_URL, $url); 
		curl_setopt($ch, CURLOPT_POST, true); 
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS,$data_json);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		$result 		= curl_exec($ch);
		// print_r('after replace');
		//$result = str_replace('en17','en25',$result);
		$result_temp=json_decode($result,true);
		return $result_temp;
	}
	
	public function searchAssets_targetInstance($packageId,$Asset_Type)
	{
		$this -> db -> select('Target_Site_Name');
		$this -> db -> from('deployment_package dp');
		$this -> db -> where('dp.Deployment_Package_Id', $packageId); 	
		$query1 = $this -> db -> get();
		$targerSiteName = $query1 -> result()[0] -> Target_Site_Name;
		
		$this -> db -> select('Base_Url, Token, Site_Name, Token_Expiry_Time, Refresh_Token');
		$this -> db -> from('instance_ inst');	
		$this -> db -> where('inst.Site_Name',$targerSiteName);
		$this -> db -> limit(1);
		$query23   = $this -> db -> get();
		
		$token 	   = $query23 -> result()[0] -> Token;
		$Base_Url  = $query23 -> result()[0] -> Base_Url;	
		$targetOrg = $query23 -> result()[0];
		
		if(time()+1800 > $query23 -> result()[0] -> Token_Expiry_Time)
		{
		   $result2 = $this -> eloqua -> refreshToken($targetOrg);
		   $query -> result()[0] -> Token_Expiry_Time = $result2['Token']['access_token'];
		}
		
		$this -> db -> select('*');
		$this -> db -> from('rsys_asset_endpoint rae');
		$this -> db -> where('rae.Asset_Type', $Asset_Type);
		$this -> db -> where('rae.Endpoint_Type', 'Find Asset');
		$query22 = $this -> db -> get();
		$endpoint = $query22->result();
		
		$url = $Base_Url .$endpoint[0]->Endpoint_URL;
		$result_asset = $this->eloqua->get_request($token, $url);
		$result_asset = json_decode($result_asset['data']);

		return $result_asset;
	}
	
	public function post_image($val)
	{
		$allTarget_Image = $this->searchAssets_targetInstance($val->Deployment_Package_Id,$val->Asset_Type);
		$imageFound_flag = 0 ;
		
		foreach($allTarget_Image->elements as $key1=>$val1)
		{
			if($val->Asset_Name==$val1->name)
			{
				$imageFound_flag = 1;
				$this -> db -> select('*');
				$this -> db -> from('rsys_asset_type at');
				$this -> db -> join('rsys_asset_endpoint ep','at.Asset_Type_Name= ep.Asset_Type', 'LEFT OUTER');
				$this -> db -> where('Asset_Type_Name', $val->Asset_Type);
				$this -> db -> where('Endpoint_Type', 'Read Single');
				$this -> db -> limit(1);
				$query = $this -> db -> get();
				$resultend = $query->result()[0];
				$this -> db -> select('Base_Url, Token, Site_Name, Token_Expiry_Time, Refresh_Token');
				$this -> db -> from('instance_ inst');				
				$this -> db -> join('deployment_package dp','dp.Target_Site_Name = inst.Site_Name', 'LEFT OUTER');
				$this -> db -> join('deployment_package_item dpi','dpi.Deployment_Package_Id= dp.Deployment_Package_Id', 'LEFT OUTER');
				$this -> db -> where('dpi.Deployment_Package_Id', $val->Deployment_Package_Id);
				$this -> db -> limit(1);
				$query 		= $this -> db -> get();
				$token 		= $query -> result()[0] -> Token;
				$Base_Url 	= $query -> result()[0] -> Base_Url;		
				$org	= $query -> result()[0];
				$url = $org->Base_Url .$resultend->Endpoint_URL .'/'.$val1->id.'?depth='.$resultend->Depth;
				
				if(time()+1800 > $org->Token_Expiry_Time)
				{
				   $result2 = $this->eloqua->refreshToken($org);
				   $org->Token = $result2['Token']['access_token'];
				}
				$result = $this->eloqua->get_request($org->Token, $url);
				
				$result2 		= json_decode($result['data']);
		
				$sourceImageJson = json_decode($val->JSON_Asset);
				
				$this -> db -> select('Base_Url, Token, Site_Name, Token_Expiry_Time, Refresh_Token');
				$this -> db -> from('instance_ inst');				
				$this -> db -> join('deployment_package dp','dp.Target_Site_Name = inst.Site_Name', 'LEFT OUTER');
				$this -> db -> join('deployment_package_item dpi','dpi.Deployment_Package_Id= dp.Deployment_Package_Id', 'LEFT OUTER');
				$this -> db -> where('dpi.Deployment_Package_Id', $val->Deployment_Package_Id);
				$this -> db -> limit(1);
				
				$target_query 		= $this -> db -> get();
				$target_token 		= $target_query -> result()[0] -> Token;
				$target_Base_Url 	= $target_query -> result()[0] -> Base_Url;		
				$target_sourceorg	= $target_query -> result()[0];
				
				
				$targetImageFolderId = $this->getTargetFolderId($sourceImageJson->folderId,$val);
				$result2->folderId = $targetImageFolderId;
				
				//update image with target folder
				$url = $target_Base_Url.'/API/REST/2.0/assets/image/'.$result2->id;
				$putResult = $this->eloqua->putRequest($target_token,$url,json_encode($result2));
				
				$image_id		= $result2->id;
				$update_data['Target_Asset_Id'] = $image_id;
				$update_data['JSON_Submitted'] = $result['data'];
				$this -> db -> where('Deployment_Package_Item_Id', $val->Deployment_Package_Item_Id);
				$this -> db -> update('deployment_package_item', $update_data);
				
				//$New_JSON_data['responseInfo'] 	= $responseInfo;
				$New_JSON_data['Output_result'] = $result['data'];
				$New_JSON_data['body']			= $result['data'];
				$New_JSON_data['httpCode'] 		= $result['httpCode'];
				
				$this -> updateNew_JSON_Asset($New_JSON_data, $val);
				
			}
		}
		if($imageFound_flag==0)	
		{
			//to find out source detail
			$this -> db -> select('Base_Url, Token, Site_Name, Token_Expiry_Time, Refresh_Token');
			$this -> db -> from('instance_ inst');				
			$this -> db -> join('deployment_package dp','dp.Source_Site_Name = inst.Site_Name', 'LEFT OUTER');
			$this -> db -> join('deployment_package_item dpi','dpi.Deployment_Package_Id= dp.Deployment_Package_Id', 'LEFT OUTER');
			$this -> db -> where('dpi.Deployment_Package_Id', $val->Deployment_Package_Id);
			$this -> db -> limit(1);
			
			$query 		= $this -> db -> get();
			$token 		= $query -> result()[0] -> Token;
			$Base_Url 	= $query -> result()[0] -> Base_Url;		
			$sourceorg	= $query -> result()[0];
			
			
			if(time()+1800 > $query -> result()[0] -> Token_Expiry_Time)
			{
			   $result2 = $this -> eloqua -> refreshToken($sourceorg);
			   $query -> result()[0] -> Token_Expiry_Time = $result2['Token']['access_token'];
			}
			
			$id 			= $val -> Asset_Id;		
			$url 			= $Base_Url.'/API/REST/2.0/assets/image/'.$id;		
			$Authorization 	= 'Authorization: Bearer' .$token;
			$headers 		= array('Authorization: Bearer '.$token);
			// ////print_r($url);
			$ch 			= curl_init(); 
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
			curl_setopt($ch, CURLOPT_HTTPGET, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);			
			$httpCode 		= curl_getinfo($ch, CURLINFO_HTTP_CODE);
			$header_size 	= curl_getinfo($ch, CURLINFO_HEADER_SIZE);
			$data 			= curl_exec($ch);			
			$responseInfo 	= curl_getinfo($ch);
			
			$json_data 		= json_decode($data);
			////print_r($json_data);
			$img_url 		= $Base_Url.$json_data->fullImageUrl;
			
			
			//to find out target detail
			$this -> db -> select('Base_Url, Token, Site_Name, Token_Expiry_Time, Refresh_Token');
			$this -> db -> from('instance_ inst');				
			$this -> db -> join('deployment_package dp','dp.Target_Site_Name = inst.Site_Name', 'LEFT OUTER');
			$this -> db -> join('deployment_package_item dpi','dpi.Deployment_Package_Id= dp.Deployment_Package_Id', 'LEFT OUTER');
			$this -> db -> where('dpi.Deployment_Package_Id', $val->Deployment_Package_Id);
			$this -> db -> limit(1);
			
			$target_query 		= $this -> db -> get();
			$target_token 		= $target_query -> result()[0] -> Token;
			$target_Base_Url 	= $target_query -> result()[0] -> Base_Url;		
			$target_sourceorg	= $target_query -> result()[0];
			
			if(time()+1800 > $target_query -> result()[0] -> Token_Expiry_Time)
			{
				$result2 = $this -> eloqua -> refreshToken($target_sourceorg);
				$target_query -> result()[0] -> Token_Expiry_Time = $result2['Token']['access_token'];
			}
			
			$target_url = $target_Base_Url.'/API/REST/2.0/assets/image/content';
			
			$img_info 	= getimagesize($img_url);
			$img_real 	= realpath($img_url);
			$data		= "--ELOQUA_BOUNDARY\r\n".
							"Content-Disposition: form-data; name=\"Filedata\"; filename=\"".$json_data->name."\"\r\n".
							"Content-Type: ".$img_url['mime']."\r\n".	
							"\r\n".
							file_get_contents($img_url)."\r\n".
							"--ELOQUA_BOUNDARY--";
							
			$headers 		= array('Authorization: Bearer '.$target_token, "Content-Length: ".strlen($data),"Content-Type: multipart/form-data; boundary=ELOQUA_BOUNDARY");
			
			$Authorization = $this->config->item('App_Id').':'.$this->config->item('Client_Secret');
			$ch=curl_init();
			curl_setopt($ch, CURLOPT_URL, $target_url); 
			curl_setopt($ch, CURLOPT_USERPWD, $Authorization);  
			curl_setopt($ch, CURLOPT_POST, true); 
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
			curl_setopt($ch, CURLOPT_HEADER, false);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			$result 		= curl_exec($ch);
			$httpCode 		= curl_getinfo($ch, CURLINFO_HTTP_CODE);
			$responseInfo 	= curl_getinfo($ch);
			$header_size 	= curl_getinfo($ch, CURLINFO_HEADER_SIZE);
			$body 			= substr($result, $header_size); 
			curl_close($ch);
			
			//to upload image in target 
			$result2 		= json_decode($result);
			$sourceImageJson = json_decode($val->JSON_Asset);			
			$targetImageFolderId = $this->getTargetFolderId($sourceImageJson->folderId,$val);
			$result2->folderId = $targetImageFolderId;
			
			//update image with target folder
			$url = $target_Base_Url.'/API/REST/2.0/assets/image/'.$result2->id;
			$putResult = $this->eloqua->putRequest($target_token,$url,json_encode($result2));
			
			$image_id		= $result2->id;
			$update_data['Target_Asset_Id'] = $image_id;
			$this -> db -> where('Deployment_Package_Item_Id', $val->Deployment_Package_Item_Id);
			$this -> db -> update('deployment_package_item', $update_data);
			
			$New_JSON_data['responseInfo'] 	= $responseInfo;
			$New_JSON_data['Output_result'] = $result;
			$New_JSON_data['body']			= $result;
			$New_JSON_data['httpCode'] 		= $httpCode;
			
			$this -> updateNew_JSON_Asset($New_JSON_data, $val);
			// return $New_JSON_data;
		}	
	}
	
	public function search_files_in_target($DPI)
	{
		$getsearch_endpoints = $this->getalleloqua_endpoints();
		$package = $this->getPackage($DPI->Deployment_Package_Id);
		$target_instance = $this->getInstance($package[0]->Target_Site_Name);
		
		$result = $this->eloqua->get_request($target_instance[0]->Token, $target_instance[0]->Base_Url.'/'.$getsearch_endpoints['FileStorage_FindAsset']->Endpoint_URL.'?search="'.urlencode($DPI->Asset_Name).'"&depth=complete');
		// $result = $this->eloqua->get_request($target_instance[0]->Token, $target_instance[0]->Base_Url.'/'.$getsearch_endpoints['FileStorage_FindAsset']->Endpoint_URL.'?search="all.css"&depth=complete');
							
							
		$result_decode = json_decode($result['data']);
		if($result_decode->total>0)
		{
			return $result_decode->elements[0];
		}
		return null;
	}
	
	public function post_files($val)
	{
		
		$target_file= $this->search_files_in_target($val);
		print_r('<pre>');
		if($target_file!=null)
		{
			// print_r($val);
			$imageFound_flag = 1;
			$this -> db -> select('*');
			$this -> db -> from('rsys_asset_type at');
			$this -> db -> join('rsys_asset_endpoint ep','at.Asset_Type_Name= ep.Asset_Type', 'LEFT OUTER');
			$this -> db -> where('Asset_Type_Name', $val->Asset_Type);
			$this -> db -> where('Endpoint_Type', 'Read Single');
			$this -> db -> limit(1);
			$query = $this -> db -> get();
			$resultend = $query->result()[0];
			$this -> db -> select('Base_Url, Token, Site_Name, Token_Expiry_Time, Refresh_Token');
			$this -> db -> from('instance_ inst');				
			$this -> db -> join('deployment_package dp','dp.Target_Site_Name = inst.Site_Name', 'LEFT OUTER');
			$this -> db -> join('deployment_package_item dpi','dpi.Deployment_Package_Id= dp.Deployment_Package_Id', 'LEFT OUTER');
			$this -> db -> where('dpi.Deployment_Package_Id', $val->Deployment_Package_Id);
			$this -> db -> limit(1);
			$query 		= $this -> db -> get();
			$token 		= $query -> result()[0] -> Token;
			$Base_Url 	= $query -> result()[0] -> Base_Url;		
			$org	= $query -> result()[0];
			
			$url = $org->Base_Url .$resultend->Endpoint_URL .'/'.$target_file->id.'?depth='.$resultend->Depth;
			// print_r($url);
			if(time()+1800 > $org->Token_Expiry_Time)
			{
			   $result2 = $this->eloqua->refreshToken($org);
			   $org->Token = $result2['Token']['access_token'];
			}
			$result = $this->eloqua->get_request($org->Token, $url);
			
			$result2 		= json_decode($result['data']);
			// print_r($result);
			$sourceImageJson = json_decode($val->JSON_Asset);
			
			$this -> db -> select('Base_Url, Token, Site_Name, Token_Expiry_Time, Refresh_Token');
			$this -> db -> from('instance_ inst');				
			$this -> db -> join('deployment_package dp','dp.Target_Site_Name = inst.Site_Name', 'LEFT OUTER');
			$this -> db -> join('deployment_package_item dpi','dpi.Deployment_Package_Id= dp.Deployment_Package_Id', 'LEFT OUTER');
			$this -> db -> where('dpi.Deployment_Package_Id', $val->Deployment_Package_Id);
			$this -> db -> limit(1);
			
			$target_query 		= $this -> db -> get();
			$target_token 		= $target_query -> result()[0] -> Token;
			$target_Base_Url 	= $target_query -> result()[0] -> Base_Url;		
			$target_sourceorg	= $target_query -> result()[0];
			
			
			$targetImageFolderId = $this->getTargetFolderId($sourceImageJson->folderId,$val);
			$result2->folderId = $targetImageFolderId;
			
			//update image with target folder
			$url = $target_Base_Url.'/API/REST/2.0/assets/importedfile/'.$result2->id;
			$putResult = $this->eloqua->putRequest($target_token,$url,json_encode($result2));
			
			$image_id		= $result2->id;
			$update_data['Target_Asset_Id'] = $image_id;
			$update_data['JSON_Submitted'] = $result['data'];
			$this -> db -> where('Deployment_Package_Item_Id', $val->Deployment_Package_Item_Id);
			$this -> db -> update('deployment_package_item', $update_data);
			
			//$New_JSON_data['responseInfo'] 	= $responseInfo;
			$New_JSON_data['Output_result'] = $result['data'];
			$New_JSON_data['body']			= $result['data'];
			$New_JSON_data['httpCode'] 		= $result['httpCode'];
			
			$this -> updateNew_JSON_Asset($New_JSON_data, $val);
			
		}
		else	
		{
			//to find out source detail
			$this -> db -> select('Base_Url, Token, Site_Name, Token_Expiry_Time, Refresh_Token');
			$this -> db -> from('instance_ inst');				
			$this -> db -> join('deployment_package dp','dp.Source_Site_Name = inst.Site_Name', 'LEFT OUTER');
			$this -> db -> join('deployment_package_item dpi','dpi.Deployment_Package_Id= dp.Deployment_Package_Id', 'LEFT OUTER');
			$this -> db -> where('dpi.Deployment_Package_Id', $val->Deployment_Package_Id);
			$this -> db -> limit(1);
			
			$query 		= $this -> db -> get();
			$token 		= $query -> result()[0] -> Token;
			$Base_Url 	= $query -> result()[0] -> Base_Url;		
			$sourceorg	= $query -> result()[0];
			
			
			if(time()+1800 > $query -> result()[0] -> Token_Expiry_Time)
			{
			   $result2 = $this -> eloqua -> refreshToken($sourceorg);
			   $query -> result()[0] -> Token_Expiry_Time = $result2['Token']['access_token'];
			}
			
			$id 			= $val -> Asset_Id;		
			$url 			= $Base_Url.'/API/REST/2.0/assets/importedfile/'.$id;		
			$Authorization 	= 'Authorization: Bearer' .$token;
			$headers 		= array('Authorization: Bearer '.$token);
			// ////print_r($url);
			$ch 			= curl_init(); 
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
			curl_setopt($ch, CURLOPT_HTTPGET, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);			
			$httpCode 		= curl_getinfo($ch, CURLINFO_HTTP_CODE);
			$header_size 	= curl_getinfo($ch, CURLINFO_HEADER_SIZE);
			$data 			= curl_exec($ch);			
			$responseInfo 	= curl_getinfo($ch);
			
			$json_data 		= json_decode($data);
			// print_r($json_data);
			$img_url 		= $json_data->link;
			
			
			//to find out target detail
			$this -> db -> select('Base_Url, Token, Site_Name, Token_Expiry_Time, Refresh_Token');
			$this -> db -> from('instance_ inst');				
			$this -> db -> join('deployment_package dp','dp.Target_Site_Name = inst.Site_Name', 'LEFT OUTER');
			$this -> db -> join('deployment_package_item dpi','dpi.Deployment_Package_Id= dp.Deployment_Package_Id', 'LEFT OUTER');
			$this -> db -> where('dpi.Deployment_Package_Id', $val->Deployment_Package_Id);
			$this -> db -> limit(1);
			
			$target_query 		= $this -> db -> get();
			$target_token 		= $target_query -> result()[0] -> Token;
			$target_Base_Url 	= $target_query -> result()[0] -> Base_Url;		
			$target_sourceorg	= $target_query -> result()[0];
			
			if(time()+1800 > $target_query -> result()[0] -> Token_Expiry_Time)
			{
				$result2 = $this -> eloqua -> refreshToken($target_sourceorg);
				$target_query -> result()[0] -> Token_Expiry_Time = $result2['Token']['access_token'];
			}
			
			$target_url = $target_Base_Url.'/API/REST/2.0/assets/importedfile/content';
			// print_r($img_url);
			//$img_info 	= filesize($img_url);
			$img_real 	= realpath($img_url);
			$data		= "--ELOQUA_BOUNDARY\r\n".
							"Content-Disposition: form-data; name=\"Filedata\"; filename=\"".$json_data->name."\"\r\n".
							"Content-Type: ".$img_url['mime']."\r\n".	
							"\r\n".
							file_get_contents($img_url)."\r\n".
							"--ELOQUA_BOUNDARY--";
							
			$headers 		= array('Authorization: Bearer '.$target_token, "Content-Length: ".strlen($data),"Content-Type: multipart/form-data; boundary=ELOQUA_BOUNDARY");
			// print_r($headers);
			$Authorization = $this->config->item('App_Id').':'.$this->config->item('Client_Secret');
			$ch=curl_init();
			curl_setopt($ch, CURLOPT_URL, $target_url); 
			curl_setopt($ch, CURLOPT_USERPWD, $Authorization);  
			curl_setopt($ch, CURLOPT_POST, true); 
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
			curl_setopt($ch, CURLOPT_HEADER, false);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			$result 		= curl_exec($ch);
			$httpCode 		= curl_getinfo($ch, CURLINFO_HTTP_CODE);
			$responseInfo 	= curl_getinfo($ch);
			$header_size 	= curl_getinfo($ch, CURLINFO_HEADER_SIZE);
			$body 			= substr($result, $header_size); 
			curl_close($ch);
			
			//to upload image in target 
			$result2 		= json_decode($result);
			// print_r($result2);
			$sourceImageJson = json_decode($val->JSON_Asset);			
			$targetImageFolderId = $this->getTargetFolderId($sourceImageJson->folderId,$val);
			$result2->folderId = $targetImageFolderId;
			
			//update image with target folder
			$url = $target_Base_Url.'/API/REST/2.0/assets/importedfile/'.$result2->id;
			$putResult = $this->eloqua->putRequest($target_token,$url,json_encode($result2));
			
			$image_id		= $result2->id;
			$update_data['Target_Asset_Id'] = $image_id;
			$this -> db -> where('Deployment_Package_Item_Id', $val->Deployment_Package_Item_Id);
			$this -> db -> update('deployment_package_item', $update_data);
			
			$New_JSON_data['responseInfo'] 	= $responseInfo;
			$New_JSON_data['Output_result'] = $result;
			$New_JSON_data['body']			= $result;
			$New_JSON_data['httpCode'] 		= $httpCode;
			
			$this -> updateNew_JSON_Asset($New_JSON_data, $val);
			// return $New_JSON_data;
		}	
	}
	
	function getTargetFolderId($sourceFolderId,$val)
	{
		//get source Image foldername
		$this -> db -> select('Base_Url, Token, Site_Name, Token_Expiry_Time, Refresh_Token');
		$this -> db -> from('instance_ inst');				
		$this -> db -> join('deployment_package dp','dp.Source_Site_Name = inst.Site_Name', 'LEFT OUTER');
		$this -> db -> join('deployment_package_item dpi','dpi.Deployment_Package_Id= dp.Deployment_Package_Id', 'LEFT OUTER');
		$this -> db -> where('dpi.Deployment_Package_Id', $val->Deployment_Package_Id);
		$this -> db -> limit(1);
		$query 		= $this -> db -> get();
		$token 		= $query -> result()[0] -> Token;
		$Base_Url 	= $query -> result()[0] -> Base_Url;		
		$org	= $query -> result()[0];
		if($val->Asset_Type == 'Image')
		{
			$url = $org->Base_Url .'/API/REST/2.0/assets/image/folder/'.$sourceFolderId.'?depth=complete';
		}
		else if($val->Asset_Type == 'File Storage')
		{
			$url = $org->Base_Url .'/API/REST/2.0/assets/importedfile/folder/'.$sourceFolderId.'?depth=complete';
		}
		
		if(time()+1800 > $org->Token_Expiry_Time)
		{
		   $result2 = $this->eloqua->refreshToken($org);
		   $org->Token = $result2['Token']['access_token'];
		}
		$result = $this->eloqua->get_request($org->Token, $url);
		//$result = $result['data'];
		
		$result2 		= json_decode($result['data']);
		////print_r($result2 );
		
		//Get target folder with source image folder name 
		$this -> db -> select('Base_Url, Token, Site_Name, Token_Expiry_Time, Refresh_Token');
		$this -> db -> from('instance_ inst');				
		$this -> db -> join('deployment_package dp','dp.Target_Site_Name = inst.Site_Name', 'LEFT OUTER');
		$this -> db -> join('deployment_package_item dpi','dpi.Deployment_Package_Id= dp.Deployment_Package_Id', 'LEFT OUTER');
		$this -> db -> where('dpi.Deployment_Package_Id', $val->Deployment_Package_Id);
		$this -> db -> limit(1);
		$query1 		= $this -> db -> get();
		$token 		= $query1 -> result()[0] -> Token;
		$Base_Url 	= $query1 -> result()[0] -> Base_Url;		
		$org1	= $query1 -> result()[0];
		$name = $result2->name;
		$name = str_replace(' ', '%20', $name);
		$url = $org1->Base_Url ."/API/REST/2.0/assets/folders?search='$name'";
		////print_r($url);
		////print_r($org1);
		if(time()+1800 > $org1->Token_Expiry_Time)
		{
		   $result2 = $this->eloqua->refreshToken($org1);
		   $org1->Token = $result2['Token']['access_token'];
		}
		$result3    = $this->eloqua->get_request($org1->Token, $url);
		$result4 	= json_decode($result3['data']);
		
		if(!empty($result4->elements))
		{
			return $result4->elements[0]->id;
		}
		return $sourceFolderId;
		
	}
	
	function decideStatus($deploy_package_id)
	{
		$this->db->select('*');
		$this->db->from('deployment_package_item');
		$this->db->where('Deployment_Package_Id',$deploy_package_id);
		$query = $this->db->get();
		$totalRows = $query->num_rows();
		$errored = 0;
		$completed = 0;
		if($totalRows > 0)
		{
			$result = $query->result();
			foreach($result as $key=>$val)
			{
				if($val->Status == 'Errored')
				{
					$errored++;
				}
				else if($val->Status == 'Completed')
				{
					$completed++;
				}
			}
		}
		if($errored>0)
		{
			return 'Errored';
		}
		else 
		{
			return 'Deploy Completed';
		}
	}
	
	
	function ChangeParaValue($JSON_Submitted,$ChangeParaConfig)
	{
		if(!is_array($ChangeParaConfig) && !is_object($ChangeParaConfig))
		{
			$ChangeParaConfig = json_decode($ChangeParaConfig);
		}
		
		if($ChangeParaConfig !='')
		{
			if(is_array($JSON_Submitted))
			{
				$replaceList = (new JSONPath($JSON_Submitted))->find($ChangeParaConfig->JSON_Expressions);
			}
			else
			{
				$replaceList = (new JSONPath(json_decode($JSON_Submitted)))->find($ChangeParaConfig->JSON_Expressions);	
			}	
				
			$oldStr= '';
			$NewPara = '';
				
			foreach($replaceList as $key=>$val)
			{	
				$oldStr = $val;
				$removeStr =substr($val,strpos($val, $ChangeParaConfig->Para)+strlen($ChangeParaConfig->Para),$ChangeParaConfig->removeLength);
				$NewPara =str_replace($removeStr,$ChangeParaConfig->ChangeToStr,$val);			
			}
			$result = str_replace($oldStr,$NewPara, $JSON_Submitted);
		}
		else
		{
			$result = $JSON_Submitted;
		}	
		return $result;
	}	
	
	
	function replaceJSONKey($exclusion, $jsonAsset )
	{		
		if(is_array($exclusion) || is_object($exclusion))
		{
			foreach($exclusion as $key=>$val)
			{	
				if($val->basekey!="")
				{
					foreach($val->basekey as $key1=>$val1)
					{						
						foreach($val1 as $key2=>$val2)
						{					
							if(is_array($jsonAsset))
							{
								$jsonAsset[$val2] = $jsonAsset[$key2];
								unset($jsonAsset[$key2]);
							}
							if(is_object($jsonAsset))
							{
								$jsonAsset->$val2 = $jsonAsset->$key2;
								unset($jsonAsset->$key2);
							}
						}
					}
				}
			}
		}
		return $jsonAsset;
	}
	
	function getExclusionResult($exclusion, $jsonAsset )
	{
		if(is_array($exclusion) || is_object($exclusion))
		{
			foreach($exclusion as $key=>$val)
			{
				if($val->basekey!="")
				{
					$exSplit2 = explode(",",$val->basekey);
					foreach($exSplit2 as $key1=>$val1){
						if(is_array($jsonAsset) )
						{
							if(isset($jsonAsset[$val1]))
							unset($jsonAsset[$val1]);
						}
						if(is_object($jsonAsset))
						{
							if(isset($jsonAsset->$val1))
							unset($jsonAsset->$val1);
						}
					}
				}
			}
			$jsonAsset = json_encode($jsonAsset);
			$jsonAsset = json_decode($jsonAsset,true);
			$store = new JsonStore();
			foreach($exclusion as $key=>$val)
			{
				if(isset($val->jsonpath))
				{
					foreach($val->jsonpath as $k=>$v)
					{
						$store->remove($jsonAsset, $v->path);					
					}
				}	
			}	
		}
		return $jsonAsset;
	}
	
	
	function injSON($jsonObj,$node,$key,$type)
	{
		if(is_array($jsonObj->$node))
		{
			foreach($jsonObj->$node  as $key1=>$val1)
			{
				unset($val1->$key);
			}
			
		}
		if(is_object($jsonObj->$node))
		{
			//$this->injSON($jsonObj,$node,$key,"object");
		}	
		return $jsonObj;
	}
	
	function getalleloqua_endpoints()
	{
		$Asset_Type = array('Find Asset', 'Update','Create');
		$this->db->select('*');
		$this -> db -> from('rsys_asset_endpoint');
		$this -> db -> where_in('Endpoint_Type',$Asset_Type);
		$this->db->order_by("Asset_Type", "asc");
		$query = $this -> db -> get();
	    $eloqua_endpoints = $query->result();
		$alleloqua_endpoints = array();
		foreach($eloqua_endpoints as $key=>$val)
		{
			$alleloqua_endpoints[str_replace(' ', '', $val->Asset_Type).'_'.str_replace(' ', '',$val->Endpoint_Type)] = $val;
		}
		return $alleloqua_endpoints;
	}
	
	function getPackage($packageId)
	{
		$this->db->select('*');
		$this -> db -> from('deployment_package');
		$this -> db -> where('Deployment_Package_Id',$packageId);
		$this->db->limit(1);
		$query = $this -> db -> get();
	    $result = $query->result();
		return $result;
	}
	
	function getInstance($Site_Name)
	{
		$this->db->select('*');
		$this -> db -> from('instance_');
		$this -> db -> where('Site_Name',$Site_Name);
		$this->db->limit(1);
		$query = $this -> db -> get();
		$instance = $query->result();
		
		return $instance;
	}

	function ReplaceSiteId($JSON_Submitted,$TargetSiteId,$Package_Item)
	{
		$temp__Submitted = $JSON_Submitted;
		
		$this->db->select("inst.Site_Id");
		$this->db->from("deployment_package dp");
		$this->db->join("instance_ inst","inst.Site_Name = dp.Source_Site_Name","LEFT OUTER");
		$this->db->where("Deployment_Package_Id",$Package_Item->Deployment_Package_Id);
		
		$result = $this->db->get();
		if($result->num_rows()>0)
		{
			$SourceSiteId = $result->result();
			$SourceSiteId = $SourceSiteId[0]->Site_Id;
			$temp__Submitted = str_replace($SourceSiteId,$TargetSiteId,$temp__Submitted);			
		}
		return $temp__Submitted;
	}
	
	function ProcessChildAssetSwitch($temp__Submitted,$val)
	{
		if($val->Asset_Type =="Custom Object") 
		{ 
			$this->db->select('*');
			$this -> db -> from('rsys_asset_child_replacement');
			$this -> db -> where('Asset_Type ',$val->Asset_Type);
			$Child_Replace_list = $this -> db -> get()->result();
			foreach($Child_Replace_list as $crkey=>$crval)
			{
				$replaceList = (new JSONPath(json_decode($temp__Submitted)))->find($crval->JSON_Expressions);
				
				foreach($replaceList as $repkey=>$repval)
				{
					$Source_Expression = str_replace('{X}',$repval,$crval->Source_Asset_Expression);
					
					$SourceIDList = (new JSONPath(json_decode($val->Source_Asset_JSON)))->find($Source_Expression);

					if(isset($SourceIDList[0]))
					{
						$Target_Expression = str_replace('{X}',$SourceIDList[0],$crval->Target_Asset_Expression);
						$TargetIDList = (new JSONPath(json_decode($val->Target_Asset_JSON)))->find($Target_Expression);
						
						if(isset($TargetIDList[0]))
						{
							$temp__Submitted = str_replace($crval->JSON_Node_Value.'":"'.$repval,$crval->JSON_Node_Value.'":"'.$TargetIDList[0],$temp__Submitted);
						}
						else
						{
							////echo '<br/> TargetID List is missing </br>';
						}
					}
				}
			}
		}
		return $temp__Submitted;
	}
	
	function ProcessNegativeIds($Package_Item,$JSON_Submitted,$instance)
	{
		$temp__Submitted = $JSON_Submitted;
		// print_r('Process Negative Ids<br/>'.$temp__Submitted);
		$store = new JsonStore();
		
		$this->db->select('JSON_Negative_Id_Component');
		$this -> db -> from('rsys_asset_endpoint');
		$this -> db -> where('Asset_Type',$Package_Item->Asset_Type);
		if($Package_Item->Target_Asset_Id > 0)
		{
			$this -> db -> where('Endpoint_Type',"Update");
		}
		else
		{	
			$this -> db -> where('Endpoint_Type',"Create");
		}
		$query = $this -> db -> get();
		$JSON_Negative_list = $query->result();
		$replace_pairs = array();
		
		foreach($JSON_Negative_list as $jnkey=>$jnval)
		{
			if($jnval->JSON_Negative_Id_Component =='')
			{
				continue; 
			}
			$jnval = explode(",", $jnval->JSON_Negative_Id_Component);	
			
			foreach($jnval as $expjnkey => $expjnval)
			{ 
				$replaceKey =  trim(substr($expjnval, strrpos($expjnval, '.') + 1));
				$replaceList = (new JSONPath(json_decode($temp__Submitted)))->find($expjnval);
				
				
				foreach($replaceList as $repkey=>$repval)
				{
					if($repval >=0)
					{						
						$repvalList = explode(' ',$repval);
						foreach($repvalList as $key=>$val)
						{	
							if(is_numeric($val))
							{
								$repvalList[$key]= '-'. $val;
							}
						}
						$newrepval = implode(' ',$repvalList);
						$temp__Submitted = str_replace('"'.$replaceKey.'":"'. $repval.'"','"'.$replaceKey.'":"'. $newrepval.'"',$temp__Submitted);
						$temp__Submitted1=json_decode($temp__Submitted,true);
						if( isset($temp__Submitted1['id']) && ($temp__Submitted1['id']<0) )
						{
							$temp = abs( $temp__Submitted1['id']);
							$temp__Submitted1['id'] = (string)$temp;
							$temp__Submitted = json_encode($temp__Submitted1);
						}	
					}
				}
			}
		} 
		//print_r('Process Negative Ids<br/>'.$temp__Submitted);
		return $temp__Submitted;
 	}
	
	//function to compare source and target custom object fields
	function compare_customObjectFields($source_co_JSON,$target_co_JSON,$JSON_Submitted)
	{
		$source_co_JSON = json_decode( json_encode($JSON_Submitted),true);
		$target_co_JSON = json_decode($target_co_JSON,true);
		// print_r($source_co_JSON);
		$source_fields = $source_co_JSON['fields'];
		$target_fields = $target_co_JSON['fields'];
		$i = count($source_fields)-1;
		while($i>=0)
		{
			foreach($target_fields as $key=>$val)
			{
				if($source_fields[$i]['name'] == $val['name'])
				{
					$source_fields[$i]['id'] = $val['id'];
				}
				else
				{
					$flag = false;
					foreach($target_fields as $key1=>$val1)
					{
						if($val1['name'] == $source_fields[$i]['name'])
						{
							$flag = true;
						}
					}
					if(!$flag)
					{
						if(strpos($source_fields[$i]['id'], '-') !== false)
						{
							
						}
						else
						{
							$source_fields[$i]['id'] = '-'.$source_fields[$i]['id'];
						}
					}
				}
			}
			$i--;
		}
		
		foreach($target_fields as $tkey=>$tval)
		{
			$found = 0;
			foreach($source_fields as $skey=>$sval)
			{
				if($tval['name'] == $sval['name'])
				{
					$found =1;
				}
			}
			if($found == 0)
			{
				$source_fields[] = $tval;
			}
		}
		// print_r($source_fields);
		$target_co_JSON['fields']=$source_fields;
		return json_encode($target_co_JSON);
	}	
	
	//Fetch the custom object from target if exits
	function getTarget_customObject($targerSiteName,$CO_id)
	{	
		$this -> db -> select('Base_Url, Token, Site_Name, Token_Expiry_Time, Refresh_Token');
		$this -> db -> from('instance_ inst');	
		$this -> db -> where('inst.Site_Name',$targerSiteName);
		$this -> db -> limit(1);
		$query23   = $this -> db -> get();
		
		$token 	   = $query23 -> result()[0] -> Token;
		$Base_Url  = $query23 -> result()[0] -> Base_Url;	
		$targetOrg = $query23 -> result()[0];
		
		if(time()+1800 > $query23 -> result()[0] -> Token_Expiry_Time)
		{
		   $result2 = $this -> eloqua -> refreshToken($targetOrg);
		   $query -> result()[0] -> Token_Expiry_Time = $result2['Token']['access_token'];
		}
		
		$this -> db -> select('*');
		$this -> db -> from('rsys_asset_endpoint rae');
		$this -> db -> where('rae.Asset_Type', 'Custom Object');
		$this -> db -> where('rae.Endpoint_Type', 'Read Single');
		$query22 = $this -> db -> get();
		$endpoint = $query22->result();
		$url = $Base_Url .$endpoint[0]->Endpoint_URL .'/'.$CO_id;
		$result_asset = $this->eloqua->get_request($token, $url);
		$result_asset = json_decode($result_asset['data']);
		return json_encode($result_asset);
	}
	
	function CDO_Update($JSON_Submitted,$instance,$Package_Item)
	{
		
		$JSON_Submitted = json_decode($JSON_Submitted);
		
		if(isset($JSON_Submitted->id)&& $JSON_Submitted->type=='CustomObject')
		{	
			$target_customObject = $this->getTarget_customObject($instance[0]->Site_Name,$JSON_Submitted->id);
			// print_r($Package_Item->JSON_Asset);
			$JSON_Submitted = $this->compare_customObjectFields($Package_Item->JSON_Asset,$target_customObject,$JSON_Submitted);
		}
		else
		{
			$JSON_Submitted = json_encode($JSON_Submitted);
		}
		return $JSON_Submitted;	
	}

	function getSourceCDOName($CO_id,$packageId)
	{
		$Package_details = $this->deploy_model->getPackage($packageId);
			
		$this -> db -> select('Base_Url, Token, Site_Name, Token_Expiry_Time, Refresh_Token');
		$this -> db -> from('instance_ inst');	
		$this -> db -> where('inst.Site_Name',$Package_details[0]->Source_Site_Name);
		$this -> db -> limit(1);
		$query23   = $this -> db -> get();		
		$token 	   = $query23 -> result()[0] -> Token;
		$Base_Url  = $query23 -> result()[0] -> Base_Url;	
		$targetOrg = $query23 -> result()[0];
		
		if(time()+1800 > $query23 -> result()[0] -> Token_Expiry_Time)
		{
		   $result2 = $this -> eloqua -> refreshToken($targetOrg);
		   $query -> result()[0] -> Token_Expiry_Time = $result2['Token']['access_token'];
		}
		
		$this -> db -> select('*');
		$this -> db -> from('rsys_asset_endpoint rae');
		$this -> db -> where('rae.Asset_Type', 'Custom Object');
		$this -> db -> where('rae.Endpoint_Type', 'Read Single');
		$query22 = $this -> db -> get();
		$endpoint = $query22->result();
		
		$url = $Base_Url .$endpoint[0]->Endpoint_URL .'/'.$CO_id;		
		$result_asset = $this->eloqua->get_request($token, $url);		
		$result_asset = json_decode($result_asset['data']);	
		// print_r($result_asset->name);
		$targetCDO=$this->getTargetcustomObjectJSONByName($Package_details[0]->Target_Site_Name,$result_asset->name);
		// print_r($targetCDO);
		return $targetCDO;
	}
	
	//Fetch the custom object from target if exits
	function getTargetcustomObjectJSONByName($targerSiteName,$CDOName)
	{	
		$this -> db -> select('Base_Url, Token, Site_Name, Token_Expiry_Time, Refresh_Token');
		$this -> db -> from('instance_ inst');	
		$this -> db -> where('inst.Site_Name',$targerSiteName);
		$this -> db -> limit(1);
		$query23   = $this -> db -> get();
		
		$token 	   = $query23 -> result()[0] -> Token;
		$Base_Url  = $query23 -> result()[0] -> Base_Url;	
		$targetOrg = $query23 -> result()[0];
		
		if(time()+1800 > $query23 -> result()[0] -> Token_Expiry_Time)
		{
		   $result2 = $this -> eloqua -> refreshToken($targetOrg);
		   $query -> result()[0] -> Token_Expiry_Time = $result2['Token']['access_token'];
		}
		
		$this -> db -> select('*');
		$this -> db -> from('rsys_asset_endpoint rae');
		$this -> db -> where('rae.Asset_Type', 'Custom Object');
		$this -> db -> where('rae.Endpoint_Type', 'Find Asset');
		$query22 = $this -> db -> get();
		$endpoint = $query22->result();
		$url = $Base_Url .$endpoint[0]->Endpoint_URL .'?depth=complete&search="'.str_replace(' ','%20',$CDOName).'"';
		//print_r($url);
		$result_asset = $this->eloqua->get_request($token, $url);
		$result_asset = json_decode($result_asset['data']);
		if(isset($result_asset->elements[0]))
		{
			$resultAsset=$result_asset->elements[0];
		}
		return $resultAsset;
	}
	
	/* replace id recursive logic*/	
	function replace(&$temp__Submitted1,$val)
	{
		
		foreach($temp__Submitted1 as $key1=>$val1)
		{
			if(is_array($val1) && $val1!='permissions')
			{
				$this->replace($temp__Submitted1[$key1],$val);
			}
			else
			{
				if(is_numeric($key1))
				{
					if($val1==$val->Asset_Id)
					{
						$temp__Submitted1[$key1] = $val->Target_Asset_Id.'vikas-';
					}	
				}
				else if($key1==$val->JSON_Node_Value)
				{
					if($val1==$val->Asset_Id)
					{
						$temp__Submitted1[$key1] = $val->Target_Asset_Id.'vikas-';
					}
				}	
			}
		}
	}	
	
	function getDeploymentPackageValidation($Package_Item,$JSON_Submitted,$instance,$endPoint)
	{
		$this->logging_model->logT($Package_Item->Deployment_Package_Id,$Package_Item->Deployment_Package_Item_Id,'getDeploymentPackageValidation','beginning of getDeploymentPackageValidation',$JSON_Submitted);
		
		$JSON_submitted_CDO_change='';
		// $this->db->select('dpv.Asset_Id,dpv.Asset_Type,dp.Target_Asset_Id,dpv.JSON_Node_Value as JSON_Node_Value');
		$this->db->select('*');
		// $this -> db -> from('deployment_package_item');
		// $this -> db -> join('deployment_package_validation_list dpv','dp.Deployment_Package_Id = dpv.Deployment_Package_Id','LEFT OUTER');
		$this -> db -> from('deployment_package_validation_list');
		$this -> db -> where('Deployment_Package_Item_Id',$Package_Item->Deployment_Package_Item_Id);
		
		$query = $this -> db -> get();
		// print_r($query->result());
		$JSON_Submitted = $this->ReplaceSiteId($JSON_Submitted,$instance[0]->Site_Id,$Package_Item);
		$this->logging_model->logT($Package_Item->Deployment_Package_Id,$Package_Item->Deployment_Package_Item_Id,'getDeploymentPackageValidation','after ReplaceSiteId',$JSON_Submitted);	
			
			
		$JSON_submitted_CDO_FieldMerge = $this->deploy_helper_model->CDO_FieldMerge($JSON_Submitted,$instance,$Package_Item);
		$JSON_Submitted = $JSON_submitted_CDO_FieldMerge;
		$this->logging_model->logT($Package_Item->Deployment_Package_Id,$Package_Item->Deployment_Package_Item_Id,'getDeploymentPackageValidation','after CDO_FieldMerge',$JSON_Submitted);	
		
		$JSON_submitted_CDO_SL = $this->deploy_helper_model->signatureRule_CDO($JSON_Submitted,$instance,$Package_Item);
		$JSON_Submitted = $JSON_submitted_CDO_SL;
		$this->logging_model->logT($Package_Item->Deployment_Package_Id,$Package_Item->Deployment_Package_Item_Id,'getDeploymentPackageValidation','after signatureRule_CDO',$JSON_Submitted);
		
		$JSON_Submitted_CDO_DynamicContent=$this->deploy_helper_model->CDO_DynamicContent($JSON_Submitted,$instance,$Package_Item);
		$JSON_Submitted = $JSON_Submitted_CDO_DynamicContent; 
		$this->logging_model->logT($Package_Item->Deployment_Package_Id,$Package_Item->Deployment_Package_Item_Id,'getDeploymentPackageValidation','after CDO_DynamicContent',$JSON_Submitted);
		
		$JSON_Submitted_Form_ProcessingSteps_CDO_FormData = $this->deploy_helper_model->CDO_Form_PS_Form_Data($JSON_Submitted,$instance,$Package_Item);
		$JSON_Submitted = $JSON_Submitted_Form_ProcessingSteps_CDO_FormData;
		$this->logging_model->logT($Package_Item->Deployment_Package_Id,$Package_Item->Deployment_Package_Item_Id,'getDeploymentPackageValidation','after CDO_Form_PS_Form_Data',$JSON_Submitted);
		
		$JSON_Submitted_Form_ProcessingSteps_CDO_CustomValue = $this->deploy_helper_model->CDO_Form_PS_CustomValue($JSON_Submitted,$instance,$Package_Item);
		$JSON_Submitted = $JSON_Submitted_Form_ProcessingSteps_CDO_CustomValue;
		$this->logging_model->logT($Package_Item->Deployment_Package_Id,$Package_Item->Deployment_Package_Item_Id,'getDeploymentPackageValidation','after CDO_Form_PS_CustomValue',$JSON_Submitted);
		
		$JSON_Submitted_Segment_CDO = $this->deploy_helper_model->CDO_Segment($JSON_Submitted,$Package_Item);
		$JSON_Submitted = $JSON_Submitted_Segment_CDO;
		$this->logging_model->logT($Package_Item->Deployment_Package_Id,$Package_Item->Deployment_Package_Item_Id,'getDeploymentPackageValidation','after CDO_Segment',$JSON_Submitted);
				
		$JSON_Submitted_elqTrackId = $this->deploy_helper_model->elqTrackId_handler($JSON_Submitted,$Package_Item);
		$JSON_Submitted = $JSON_Submitted_elqTrackId;
		$this->logging_model->logT($Package_Item->Deployment_Package_Id,$Package_Item->Deployment_Package_Item_Id,'getDeploymentPackageValidation','after elqTrackId_handler',$JSON_Submitted);
		
		$JSON_Submitted_quickList = $this->deploy_helper_model->JSON_Submitted_quickList($JSON_Submitted);
		$JSON_Submitted = $JSON_Submitted_quickList;
		$this->logging_model->logT($Package_Item->Deployment_Package_Id,$Package_Item->Deployment_Package_Item_Id,'getDeploymentPackageValidation','after JSON_Submitted_quickList',$JSON_Submitted);
		
		$JSON_Submitted_add2move2Campaign = $this->deploy_helper_model->JSON_Submitted_add2move2Campaign_handler($JSON_Submitted,$Package_Item);
		$JSON_Submitted = $JSON_Submitted_add2move2Campaign;
		$this->logging_model->logT($Package_Item->Deployment_Package_Id,$Package_Item->Deployment_Package_Item_Id,'getDeploymentPackageValidation','after JSON_Submitted_add2move2Campaign_handler',$JSON_Submitted);
		
		$JSON_Submitted_FormHiddenEmailCampaignId = $this->deploy_helper_model->FormHiddenEmailCampaignId($JSON_Submitted,$Package_Item);
		$JSON_Submitted = $JSON_Submitted_FormHiddenEmailCampaignId;
		$this->logging_model->logT($Package_Item->Deployment_Package_Id,$Package_Item->Deployment_Package_Item_Id,'getDeploymentPackageValidation','after JSON_Submitted_FormHiddenEmailCampaignId',$JSON_Submitted);
		
		$temp_New = json_decode($JSON_Submitted_FormHiddenEmailCampaignId,true);
		if($temp_New['type']=='LandingPage')
		{	echo 'After JSON_Submitted_FormHiddenEmailCampaignId<br>';
			echo '<pre>';
				print_r($temp_New['name']);
			echo '</pre>';
		}
		
		$JSON_Submitted_LPFileStorage = $this->deploy_helper_model->LPFileStorage($JSON_Submitted,$Package_Item);
		$JSON_Submitted = $JSON_Submitted_LPFileStorage;
		$this->logging_model->logT($Package_Item->Deployment_Package_Id,$Package_Item->Deployment_Package_Item_Id,'getDeploymentPackageValidation','after LPFileStorage',$JSON_Submitted);
		
		// $temp_New = json_decode($JSON_Submitted_LPFileStorage,true);
		// if($temp_New['type']=='LandingPage')
		// {	echo 'After LPFileStorage<br>';
			// echo '<pre>';
				// print_r($temp_New);
			// echo '</pre>';
		// }
		
		$JSON_Submitted_LPMicrositeIdReplace = $this->deploy_helper_model->LPMicrositeIdReplace($JSON_Submitted,$Package_Item);
		$JSON_Submitted = $JSON_Submitted_LPMicrositeIdReplace;
		$this->logging_model->logT($Package_Item->Deployment_Package_Id,$Package_Item->Deployment_Package_Item_Id,'getDeploymentPackageValidation','after LPMicrositeIdReplace',$JSON_Submitted);		
		
		$JSON_Submitted_domainNameReplace = $this->deploy_helper_model->LPDomainNameReplace($JSON_Submitted,$Package_Item);
		$JSON_Submitted = $JSON_Submitted_domainNameReplace;
		$this->logging_model->logT($Package_Item->Deployment_Package_Id,$Package_Item->Deployment_Package_Item_Id,'getDeploymentPackageValidation','after LPDomainNameReplace',$JSON_Submitted);
			
		$JSON_Submitted_ReplaceMicrositeIdForExistingLP = $this->deploy_helper_model->ReplaceMicrositeIdForExistingLP($JSON_Submitted,$Package_Item);
		$JSON_Submitted = $JSON_Submitted_ReplaceMicrositeIdForExistingLP;
		$this->logging_model->logT($Package_Item->Deployment_Package_Id,$Package_Item->Deployment_Package_Item_Id,'getDeploymentPackageValidation','after ReplaceMicrositeIdForExistingLP',$JSON_Submitted);		
		
		$JSON_Submitted_ContactFilter_CDO = $this->deploy_helper_model->CDO_ContactFilter($JSON_Submitted,$Package_Item);
		$JSON_Submitted = $JSON_Submitted_ContactFilter_CDO;
		$this->logging_model->logT($Package_Item->Deployment_Package_Id,$Package_Item->Deployment_Package_Item_Id,'getDeploymentPackageValidation','after CDO_ContactFilter',$JSON_Submitted);
		
		$JSON_Submitted_ContactFiletr = $this->deploy_helper_model->statementNegative_contactFilter($JSON_Submitted,$Package_Item);
		$JSON_Submitted = $JSON_Submitted_ContactFiletr;
		$this->logging_model->logT($Package_Item->Deployment_Package_Id,$Package_Item->Deployment_Package_Item_Id,'getDeploymentPackageValidation','after statementNegative_contactFilter',$JSON_Submitted);
		
		if($query -> num_rows() == 0)
		{
			$JSON_Submitted = $this->ProcessNegativeIds($Package_Item,$JSON_Submitted,$instance);
			$this->logging_model->logT($Package_Item->Deployment_Package_Id,$Package_Item->Deployment_Package_Item_Id,'getDeploymentPackageValidation','after ProcessNegativeIds IF',$JSON_Submitted);			
			
			$JSON_Submitted_unset_campaignInput = $this->deploy_helper_model->JSON_Submitted_unset_campaignInput($JSON_Submitted,$instance,$Package_Item);
			$JSON_Submitted = $JSON_Submitted_unset_campaignInput;
			$this->logging_model->logT($Package_Item->Deployment_Package_Id,$Package_Item->Deployment_Package_Item_Id,'getDeploymentPackageValidation','after JSON_Submitted_unset_campaignInput IF',$JSON_Submitted);
			
			
			$JSON_Submitted_hyperlinkLPIDReplace = $this->deploy_helper_model->hyperlinkLhref($JSON_Submitted,$Package_Item->Deployment_Package_Id);
			$JSON_Submitted = $JSON_Submitted_hyperlinkLPIDReplace;
			$this->logging_model->logT($Package_Item->Deployment_Package_Id,$Package_Item->Deployment_Package_Item_Id,'getDeploymentPackageValidation','after hyperlinkLhref IF',$JSON_Submitted);
			
			$JSON_Submitted_CDO_Update = $this->CDO_Update($JSON_Submitted,$instance,$Package_Item);
			$JSON_Submitted = $JSON_Submitted_CDO_Update;
			$this->logging_model->logT($Package_Item->Deployment_Package_Id,$Package_Item->Deployment_Package_Item_Id,'getDeploymentPackageValidation','after CDO_Update IF',$JSON_Submitted);
			
			
			if ($Package_Item->Target_Asset_Id == 0)
			{	
				$hyperlinkJSON = json_decode($JSON_Submitted);
				if($hyperlinkJSON->type=='Hyperlink' && $hyperlinkJSON->hyperlinkType=='ExternalURL')
				{	
					$data123['JSON_Submitted'] = $JSON_Submitted;
					if(time()+1800 > $instance[0]->Token_Expiry_Time)
					{
					   $result2 = $this->eloqua->refreshToken($instance[0]);
					   $instance[0]->Token = $result2['Token']['access_token'];
					}
					if($Package_Item->verified == 1 && $Package_Item->isCircular!=-1)
					{
						$result = $this->eloqua->putRequest($instance[0]->Token, $instance[0]->Base_Url.''.$endPoint.'/'.$hyperlinkJSON->id,$JSON_Submitted);
						$this->updateNew_JSON_Asset($result,$Package_Item);
						$this->db->where('Deployment_Package_Item_Id',$Package_Item->Deployment_Package_Item_Id);
						$this->db->update('deployment_package_item',$data123);
						echo '<br/> Put Request  Hyperlink<br/><pre>';
							print_r($result);
						echo '</pre>';
					}
					
				}
				else 
				{
					if(time()+1800 > $instance[0]->Token_Expiry_Time)
					{
					   $result2 = $this->eloqua->refreshToken($instance[0]);
					   $instance[0]->Token = $result2['Token']['access_token'];
					}
					if($Package_Item->verified == 1 && $Package_Item->isCircular != -1)
					{
						$result = $this->eloqua->postRequest($instance[0]->Token, $instance[0]->Base_Url.''.$endPoint,$JSON_Submitted);
					}
					
				}	
			}
			else
			{	
				$temp__Submitted = $this->CDO_Update($JSON_Submitted,$instance,$Package_Item);
				$this->logging_model->logT($Package_Item->Deployment_Package_Id,$Package_Item->Deployment_Package_Item_Id,'getDeploymentPackageValidation','after CDO_Update ELSE PUT 1',$temp__Submitted);
				
				$temp__Submitted_camp= $this->deploy_helper_model->CDO_Campaign($temp__Submitted,$Package_Item);
				$temp__Submitted=$temp__Submitted_camp;
				$this->logging_model->logT($Package_Item->Deployment_Package_Id,$Package_Item->Deployment_Package_Item_Id,'getDeploymentPackageValidation','after CDO_Campaign ELSE PUT 1',$temp__Submitted);
				
				$JSON_Submitted = $temp__Submitted;
				
				if(time()+1800 > $instance[0]->Token_Expiry_Time)
				{
				   $result2 = $this->eloqua->refreshToken($instance[0]);
				   $instance[0]->Token = $result2['Token']['access_token'];
				}
				
				if($Package_Item->verified == 1 && $Package_Item->isCircular != -1)
				{
					$result = $this->eloqua->putRequest($instance[0]->Token, $instance[0]->Base_Url.''.$endPoint.'/'.$Package_Item->Target_Asset_Id,$JSON_Submitted);
				}
				
				
			    
				// echo '<br/> Put Request 1<br/><pre>';
					// print_r($result);
				// echo '</pre>';
			    
			}
			
			if($Package_Item->verified == 1 && $Package_Item->isCircular != -1)
			{
				$data123['JSON_Submitted'] = $JSON_Submitted;
				$this->db->where('Deployment_Package_Item_Id',$Package_Item->Deployment_Package_Item_Id);
				$this->db->update('deployment_package_item',$data123);
				$this->updateNew_JSON_Asset($result,$Package_Item);
			}
			
		}
		else
		{ 
			
			$package_validation_list = $query->result();
			
			$temp__Submitted = $this->CDO_Update($JSON_Submitted,$instance,$Package_Item);
			$this->logging_model->logT($Package_Item->Deployment_Package_Id,$Package_Item->Deployment_Package_Item_Id,'getDeploymentPackageValidation','after CDO_Update ELSE',$temp__Submitted);
				
			$temp__Submitted = $this->ProcessNegativeIds($Package_Item,$temp__Submitted,$instance);
			$this->logging_model->logT($Package_Item->Deployment_Package_Id,$Package_Item->Deployment_Package_Item_Id,'getDeploymentPackageValidation','after ProcessNegativeIds ELSE',$temp__Submitted);			
			
			$temp__Submitted_camp= $this->deploy_helper_model->CDO_Campaign($temp__Submitted,$Package_Item);
			$temp__Submitted=$temp__Submitted_camp;
			$this->logging_model->logT($Package_Item->Deployment_Package_Id,$Package_Item->Deployment_Package_Item_Id,'getDeploymentPackageValidation','after CDO_Campaign ELSE',$temp__Submitted);
			
			$temp__Submitted1=json_decode($temp__Submitted,true);
			// if($temp__Submitted1['type'] == 'Form')
			// {
				// print_r('<pre>');
				// print_r('before replace');
				// print_r($package_validation_list);
			// }
			
			foreach($package_validation_list as $key=>$val)
			{
				$this->replace($temp__Submitted1,$val);
				$temp__Submitted = json_encode($temp__Submitted1);
				if( strpos( $temp__Submitted, 'vikas-' ) !== false ) 
				{
					$temp__Submitted=str_replace("vikas-","",$temp__Submitted);
				}	
				$temp__Submitted = $this->ProcessChildAssetSwitch($temp__Submitted,$val);
			}	
			// if($temp__Submitted1['type'] == 'Form')
			// {
				// print_r('<pre>');
				// print_r('after replace');
				// print_r($temp__Submitted1);
				// print_r($temp__Submitted1);
			// }
			$this->logging_model->logT($Package_Item->Deployment_Package_Id,$Package_Item->Deployment_Package_Item_Id,'getDeploymentPackageValidation','after ProcessChildAssetSwitch ELSE',$temp__Submitted);
			// print_r(json_decode($temp__Submitted));
			
			$JSON_Submitted_Replace_Target_Element_Id = $this->deploy_helper_model->JSON_Submitted_Replace_Target_Element_Id($temp__Submitted,$instance,$Package_Item);
			$temp__Submitted = $JSON_Submitted_Replace_Target_Element_Id;
			$this->logging_model->logT($Package_Item->Deployment_Package_Id,$Package_Item->Deployment_Package_Item_Id,'getDeploymentPackageValidation','after JSON_Submitted_Replace_Target_Element_Id ELSE',$temp__Submitted);
			
			$JSON_Submitted_unset_campaignInput = $this->deploy_helper_model->JSON_Submitted_unset_campaignInput($temp__Submitted,$instance,$Package_Item);
			$temp__Submitted = $JSON_Submitted_unset_campaignInput;
			$this->logging_model->logT($Package_Item->Deployment_Package_Id,$Package_Item->Deployment_Package_Item_Id,'getDeploymentPackageValidation','after JSON_Submitted_unset_campaignInput ELSE',$temp__Submitted);
					
			// $JSON_Submitted_HyperlinkLPID = $this->deploy_helper_model->LPHyperlinkLPID($temp__Submitted,$package_validation_list);
			// $temp__Submitted = $JSON_Submitted_HyperlinkLPID;	
			
			// print_r(json_decode($JSON_Submitted_HyperlinkLPID));
			
			$temp__Submitted_temp=$this->deploy_helper_model->handleHTMLChanges($temp__Submitted,$package_validation_list);
			$temp__Submitted=$temp__Submitted_temp;
			$this->logging_model->logT($Package_Item->Deployment_Package_Id,$Package_Item->Deployment_Package_Item_Id,'getDeploymentPackageValidation','after handleHTMLChanges ELSE',$temp__Submitted);
			
			
			$JSON_Submitted_hyperlinkLPIDReplace = $this->deploy_helper_model->hyperlinkLhref($temp__Submitted,$Package_Item->Deployment_Package_Id);
			$temp__Submitted = $JSON_Submitted_hyperlinkLPIDReplace;
			$this->logging_model->logT($Package_Item->Deployment_Package_Id,$Package_Item->Deployment_Package_Item_Id,'getDeploymentPackageValidation','after hyperlinkLhref ELSE',$temp__Submitted);
			
			// echo '<pre>';
				// print_r(json_decode($JSON_Submitted_hyperlinkLPIDReplace));
			// echo '</pre>';
			
			$data123['JSON_Submitted'] = $temp__Submitted;
			
			if ($Package_Item->Target_Asset_Id == 0)
			{
				$hyperlinkJSON = json_decode($temp__Submitted);
				if($hyperlinkJSON->type=='Hyperlink' && $hyperlinkJSON->hyperlinkType=='ExternalURL')
				{
					if(time()+1800 > $instance[0]->Token_Expiry_Time)
					{
					   $result2 = $this->eloqua->refreshToken($instance[0]);
					   $instance[0]->Token = $result2['Token']['access_token'];
					}
					
					if($Package_Item->verified == 1 && $Package_Item->isCircular != -1)
					{
						$result = $this->eloqua->putRequest($instance[0]->Token, $instance[0]->Base_Url.''.$endPoint.'/'.$hyperlinkJSON->id,$temp__Submitted);
						$this->updateNew_JSON_Asset($result,$Package_Item);
						$this->db->where('Deployment_Package_Item_Id',$Package_Item->Deployment_Package_Item_Id);
						$this->db->update('deployment_package_item',$data123);
					}
					
					// echo '<br/> Put Request  Hyperlink<br/><pre>';
						// print_r($result);
					// echo '</pre>';
				}
				else 
				{
					if(time()+1800 > $instance[0]->Token_Expiry_Time)
					{
					   $result2 = $this->eloqua->refreshToken($instance[0]);
					   $instance[0]->Token = $result2['Token']['access_token'];
					}
					
					if($Package_Item->verified == 1 && $Package_Item->isCircular != -1)
					{
						$result = $this->eloqua->postRequest($instance[0]->Token, $instance[0]->Base_Url.''.$endPoint,$temp__Submitted);
					}
					
				}
				// echo '<br/> Post Request 2<br/><pre>';
					// print_r($result);
				// echo '</pre>';
			}
			else
			{ 	
				
				if(time()+1800 > $instance[0]->Token_Expiry_Time)
				{
				   $result2 = $this->eloqua->refreshToken($instance[0]);
				   $instance[0]->Token = $result2['Token']['access_token'];
				}
				
				if($Package_Item->verified == 1 && $Package_Item->isCircular != -1)
				{
					$result = $this->eloqua->putRequest($instance[0]->Token, $instance[0]->Base_Url.''.$endPoint.'/'.$Package_Item->Target_Asset_Id,$temp__Submitted);
				}
				
				
				
				// echo '<br/> Put Request 2<br/><pre>';
					// print_r($result);
				// echo '</pre>';
			}
			if($Package_Item->verified == 1 && $Package_Item->isCircular != -1)
			{
				if($result['httpCode']==201 || $result['httpCode']==200)
				{	
					if($Package_Item->Asset_Type=='Form')
					{
						// print_r(json_decode($result['body']));echo '<br>';
						$Form_source_id=$Package_Item->Asset_Id;
						$result_form=$this->formHTMLBodyIdReplacement($result['body'],$Form_source_id);
						$this->logging_model->logT($Package_Item->Deployment_Package_Id,$Package_Item->Deployment_Package_Item_Id,'getDeploymentPackageValidation','after formHTMLBodyIdReplacement ELSE',$result_form['result']);
						if($Package_Item->verified == 1 && $Package_Item->isCircular != -1)
						{
							$result = $this->eloqua->putRequest($instance[0]->Token, $instance[0]->Base_Url.''.$endPoint.'/'.$result_form['Target_Asset_Id'],$result_form['result']);
							// print_r($result_form);
							$data123['JSON_Submitted']=$result['body'];
						}
						
					}
				}
			}
			
			
			if($Package_Item->verified == 1 && $Package_Item->isCircular != -1)
			{
				$this->updateNew_JSON_Asset($result,$Package_Item);
				$this->db->where('Deployment_Package_Item_Id',$Package_Item->Deployment_Package_Item_Id);
				$this->db->update('deployment_package_item',$data123);
			}
			
			// unset($data123);
		}
	}
	
	function formHTMLBodyIdReplacement($result_temp,$Form_source_id)
	{
		$result_temp_=$result_temp;
		$temp__Submitted_formIdReplace=json_decode($result_temp_);
		
		if($temp__Submitted_formIdReplace->type=='Form')
		{
			$Form_target_id=$temp__Submitted_formIdReplace->id;
			$Form_body=json_encode($temp__Submitted_formIdReplace);
			$data_json =$Form_body.'|||'.$Form_source_id. '|||'.$Form_target_id;
			$url = 'http://transporter.portqii.com:8080/Test1/SendBackWithFormIdChanged';
			$ch=curl_init();
			$headers = array('Content-Type:text/plain');
			// echo '<pre>';
				// print_r($temp__Submitted_formIdReplace);
			// echo '</pre>';	
			curl_setopt($ch, CURLOPT_URL, $url); 
			curl_setopt($ch, CURLOPT_POST, true); 
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
			curl_setopt($ch, CURLOPT_POSTFIELDS,$data_json);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			$result_		= curl_exec($ch);
			$result_form['result']=$result_;
			$result_form['Target_Asset_Id']=$Form_target_id;
			$result=$result_form;
			// print_r(json_decode($result_form['result']));
		}
		else
		{
			$result=$result_temp;
		}
		return 	$result;
	}
	
	function updateNew_JSON_Asset($New_JSON_data,$Package_Item)
	{
		if($New_JSON_data['httpCode'] =='200' || $New_JSON_data['httpCode'] =='201')
		{
			$data['New_JSON_Asset'] = $New_JSON_data['Output_result'];
			$data['Error_Description'] = '';
			$data['Status'] = 'Completed';
			$Target_Asset_Id =  json_decode($New_JSON_data['body'])->id ;
			$valid_data['Target_Asset_Id'] = $Target_Asset_Id;
			$valid_data['Target_Asset_JSON'] = $New_JSON_data['body'];
			$this->db->where('Deployment_Package_Id',$Package_Item->Deployment_Package_Id);
			$this->db->where('Asset_Id',$Package_Item->Asset_Id);
			$this->db->where('Asset_Type',$Package_Item->Asset_Type);
			$this->db->update('deployment_package_validation_list',$valid_data);
		}
		else
		{
			$data['Error_Description'] = $New_JSON_data['Output_result'];
			$data['New_JSON_Asset'] = '';
			$data['Status'] = 'Errored';
		}
		$this->db->where('Deployment_Package_Item_Id',$Package_Item->Deployment_Package_Item_Id);
		$this->db->update('deployment_package_item',$data);
	}
}
?>