<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Changeset extends CI_Model{


	protected $pattern = '/[\':;`!^£$%&*()}{@#~?><>,|=+¬-]/';
	
	function __construct()
    {
        // Call the Model constructor
        parent::__construct();	
		$this->load->model('eloqua','',TRUE);
		$this->load->model('deploy_model','',TRUE);		
		$this->load->model('license_model','',TRUE);		
    }
	
	//Method to create new package and update package
	function create_new_package($Package_Id,$package_name,$package_description,$source_siteName,$target_siteName,$siteId,$userName,$jsonShell)
	{
		$data['Package_Name'] = $package_name;
		$data['Status_Details'] = $package_description;
		$data['enable_JSONShell'] = $jsonShell;
		$current_date_time = new DateTime();
		if($Package_Id == "")
		{
			$data['Source_Site_Name'] = $source_siteName;
			$data['Target_Site_Name'] = $target_siteName;
			$data['parent_site_id'] = $siteId;			
			$data['User_Name'] = $userName;		
			$data['Created_At'] = $current_date_time->format('Y-m-d H:i:s');
			$this -> db -> insert('deployment_package' ,$data);
			$Package_Id = $this -> db -> insert_id();
		}
		else
		{				
			$data['Updated_At'] = $current_date_time->format('Y-m-d H:i:s');
			$this -> db -> where('Deployment_Package_Id', $Package_Id);
			$this -> db -> where('parent_site_id', $siteId);
			$this -> db -> update('deployment_package' ,$data);
		}
		return $Package_Id;
	}
	
	
	//read the department list from db
    function getChangesetToOrg($id, $changesetid)
	{	
		$this -> db -> select('cs.Deployment_Package_Id,cs.Status_Details,cs.Source_Site_Name,cs.Target_Site_Name,cs.Package_Name,cs.status,cs.created_at,cs.enable_JSONShell');
		$this -> db -> from('deployment_package cs');
		$this -> db -> join('instance_ org', 'org.Site_Name = cs.Source_Site_Name','LEFT OUTER');
		$this -> db -> join('instance_ org1', 'org1.Site_Name = cs.Target_Site_Name','LEFT OUTER');
		// $this -> db -> where('cs.parent_site_id', $id);
		$this -> db -> where('cs.Deployment_Package_Id', $changesetid);
		$result = $this->db->get()->result();
		return $result;
	}
	
	function getChangesetList($parent_siteId,$parent_siteName)
	{	
		$licencedInstances = $this->license_model->associatedSitesForSiteName($parent_siteName);
		if(empty($licencedInstances))
		{
			$result = 'Licence Expired';
		}
		else
		{			
			$instanceCount=0;
			for($i=0;$i<count($licencedInstances);$i++)
			{
				if(isset($licencedInstances[$i]))
				{
					$instanceCount++;
				}	
			}
			if($instanceCount>1)
			{
				$this->db->select('cs.Deployment_Package_Id,cs.User_Name,cs.Package_Name,cs.Source_Site_Name,cs.Target_Site_Name,cs.created_at,cs.Status');
				$this -> db -> from('deployment_package cs');
				$this -> db -> order_by("cs.Deployment_Package_Id", "desc");
				$this->db->where_in('cs.Source_Site_Name', $licencedInstances);
				$this->db->where_in('cs.Target_Site_Name', $licencedInstances);
				$result = $this->db->get()->result();
			}
			else
			{
				
				$result = 'Instance Needed';
			}	
		}		
		return $result;
	}
	
	function getChangesetList_search($parent_siteId, $text,$parent_siteName)
	{		
		$licencedInstances = $this->license_model->associatedSitesForSiteName($parent_siteName);
		$this->db->select('cs.Deployment_Package_Id,cs.User_Name,cs.Package_Name,cs.Source_Site_Name,cs.Target_Site_Name,cs.created_at,cs.Status');
		$this -> db -> from('deployment_package cs');
		$this -> db -> order_by("cs.Deployment_Package_Id", "desc");
		$concatenatedSites = $this->getSiteNameConcatenated($licencedInstances);
		
		$where = "(cs.Deployment_Package_Id LIKE '%".$text."%' OR cs.Package_Name LIKE '%".$text."%' OR cs.Source_Site_Name LIKE '%".$text."%' OR cs.Target_Site_Name LIKE '%".$text."%'OR cs.User_Name LIKE '%".$text."%' OR cs.Status LIKE '%".$text."%') AND ( cs.Source_Site_Name in ('".$concatenatedSites."') OR cs.Target_Site_Name in ('".$concatenatedSites."') )";
		$this->db->where($where);
		$result = $this->db->get()->result();
		return $result;
	}
	
	function getSiteNameConcatenated($sites)
	{
		$concatenatedSites = '';
		if(sizeof($sites)>0)
		{
			if(sizeof($sites) == 1)
			{
				return "'".$sites[0]."'";
			}
			else
			{
				foreach($sites as $key=>$val)
				{
					if($concatenatedSites == '')
					{
						$concatenatedSites = $val;
					}
					else
					{
						$concatenatedSites = $concatenatedSites."','".$val;
					}
					
				}
			}
		}
		return $concatenatedSites;
	}
	
	function getComponentList($siteId, $changeset_id)
	{
		$this->db->select('*,pi.Status');
		$this -> db -> from('deployment_package_item pi');
		$this->db->join('deployment_package dp', 'dp.Deployment_Package_Id = pi.Deployment_Package_Id');
		// $this -> db -> where('dp.parent_site_id', $siteId);
		$this -> db -> where('pi.Deployment_Package_Id', $changeset_id);
		$this -> db -> where('pi.verified',1);
		$result = $this->db->get()->result();
		return $result;
	}
	

	
	function verify_existing($siteId, $Package_Id, $verify_existing_value)
	{
		$data1['Status'] = 'New';
		$this -> db -> where('Deployment_Package_Id', $Package_Id);
		// $this -> db -> where('missing_target', $verify_existing_value);
		$this -> db -> where('verified', 0);
		$this -> db -> update('deployment_package_item' ,$data1);
		
		$data['verified'] = 1;
		$this -> db -> where('Deployment_Package_Id', $Package_Id);
		$this -> db -> where('missing_target', $verify_existing_value);
		$this -> db -> update('deployment_package_item' ,$data);
		
		$this -> db -> select('*,pi.Status');
		$this -> db -> from('deployment_package_item pi');
		$this -> db -> join('deployment_package dp', 'dp.Deployment_Package_Id = pi.Deployment_Package_Id');
		$this -> db -> where('dp.parent_site_id', $siteId);
		$this -> db -> where('pi.Deployment_Package_Id', $Package_Id);
		$this -> db-> where('pi.verified',1);
		$result = $this -> db -> get() -> result();
		return $result;
	}
	
	// function verify_missing($siteId, $Package_Id)
	// {
		// $data['verified'] = 1;
		// $this -> db -> where('Deployment_Package_Id', $Package_Id);
		// $this -> db -> where('missing_target', 1);
		// $this -> db -> update('deployment_package_item' ,$data);
		
		// $this -> db -> select('*,pi.Status');
		// $this -> db -> from('deployment_package_item pi');
		// $this -> db -> join('deployment_package dp', 'dp.Deployment_Package_Id = pi.Deployment_Package_Id');
		// $this -> db -> where('dp.parent_site_id', $siteId);
		// $this -> db -> where('pi.Deployment_Package_Id', $Package_Id);
		// $this -> db-> where('pi.verified',1);
		// $result = $this -> db -> get() -> result();
		// return $result;
	// }
	
	function addtopackage($id, $data)
	{
		$this -> db -> select('Source_Site_Name, Target_Site_Name');
		$this -> db -> from('deployment_package');
		$this -> db -> where('Deployment_Package_Id',$data['Deployment_Package_Id']);
		$result = $this -> db -> get()->result();
		if($result[0]->Source_Site_Name != $result[0]->Target_Site_Name || $result[0]->Source_Site_Name != "" || $result[0]->Target_Site_Name != "")
		{	
			$this -> db -> select('Deployment_Package_Id');
			$this -> db -> from('deployment_package_item');
			$this -> db -> where('Asset_Id',$data['Asset_Id']);
			$this -> db -> where('Deployment_Package_Id',$data['Deployment_Package_Id']);
			$this -> db -> where('Asset_Type',$data['Asset_Type']);
			// $query = $this->db->get();
			// $result_query = $query->result();
			$temp =$this -> db -> count_all_results();
			if($temp==0)
			{	
				$this -> db -> insert('deployment_package_item',$data);
				return true;
			}
			else
			{
				//if($data['verified'] == 1)
				//Modified on 08 june 2018 - valid item added from existing validation report 	
				// if($data['verified'] == 0)
				// {
					$data['Status'] = 'New';
				// }
				$this -> db -> where('Asset_Id',$data['Asset_Id']);
				$this -> db -> where('Deployment_Package_Id',$data['Deployment_Package_Id']);
				$this -> db -> where('Asset_Type',$data['Asset_Type']);
				$this -> db -> update('deployment_package_item',$data);
				
				// $this -> db -> where('Status','Draft');
				// $this -> db -> update('deployment_package',$data);
				
			}
			return true;
		}
	}
	
	function postChangesetList($id,$data)
	{
		$this->db->select('Deployment_Package_Id');
		$this -> db -> from('deployment_package');
		$this -> db -> where('Created_By_Contact_Id',$id);
		$this -> db -> where('Package_Name',$data['Package_Name']);
		$temp =$this->db->count_all_results();
		if($temp==0){	
			$data['Created_At'] = date("Y-m-d h:m:s");
			$data['Created_By_Contact_Id'] = $id;
			$this->db->insert('deployment_package', $data); 		
			
			$result['success'] = true;					
			$result['id'] = $this->db->insert_id();						
		}else{
			$result['success'] = false;		
			$result['msg'] = 'List Name Can\'t be Duplicate.';
		}
		return $result;
	}
	function postChangesetComponent($id,$data)
	{
		// foreach($data as $key=>$val)
		// {
			// if(is_array($val)){
				// foreach($val as $key2=>$val2)
				// {
					// $this->db->select('id');
					// $this -> db -> from('Component_List');
					// $this -> db -> where('user_id',$id);
					// $this -> db -> where('changeset_id',$data['changeset_id']);
					// $this -> db -> where('meta_type',$key);
					// $this -> db -> where('object_id',$val2);					
					// $temp =$this->db->count_all_results();
					// if($temp==0){	
						// $tempdata['user_id'] = $id;
						// $tempdata['changeset_id'] = $data['changeset_id'];
						// $tempdata['meta_type'] = $key;
						// $tempdata['object_id'] = $val2;
						// $this->db->insert('component_list', $tempdata); 		
						// $result['success'][] = true;					
						// $result['id'][] = $this->db->insert_id();				
					// }else{
						// $result['success'][] = false;		
						// $result['msg'][] = 'List Name Can\'t be Duplicate.';
					// }
				// }			
			// }
		// }
		// return $result;
	}
	
	function deleteChangeset($id,$changeset_id)
	{	
		// try{
			// $this->db->where('changeset_id', $changeset_id);
			// $this->db->where('user_id', $id);
			// $this->db->delete('Component_List');
			
			// $this->db->where_in('id', $changeset_id);
			// $this->db->where('user_id', $id);
			// $this->db->delete('deployment_package');		
			// return true;
		// }
		// catch (Exception $e) {
			// return false;
		// }		
	}
	function deleteComponent($id,$data)
	{	
		try
		{
			$this->db->where('Deployment_Package_Item_Id', $data['component_id']);
			$this->db->where('Deployment_Package_Id', $data['changeset_id']);
			// $this->db->where('user_id', $id);
			// $tdata['verified']=0;
			// $tdata['status']='New';
			
			// $this->db->update('deployment_package_item',$tdata);
			$this->db->delete('deployment_package_item');
			return true;
		}
		catch (Exception $e) 
		{
			return false;
		}
		
	}
	function getDeployHistory($UserId,$changeset_id)
	{
		// $this->db->select('*');
		// $this -> db -> from('deployment_package cs');
		// $this->db->join('instance_ org', 'org.id = cs.Source_Site_Name');
		// $result = $this->db->get()->result();		
		// return $result;
	}
	function getAssets($Company_Id)
	{
		//print_r($Company_Id);exit;
		$this->db->select('*');
		$this -> db -> from('rsys_asset_type at');
		// $this -> db -> join('asset_type_preference atp', 'at.RSys_Asset_Type_Id =atp.Asset_Type','LEFT OUTER');
		$this->db->order_by("at.Display_Ordinal","ASC");
		$result = $this->db->get()->result();
		return $result;
		
		// $this->db->select('*');
		// $this -> db -> from('Asset_Type_Preference');
		// $this -> db -> where('Company_Id',$Company_Id);
		// $result2 = $this->db->get()->result();
		// $preference = array();
		
		// foreach($result as $key => $val)
		// {
			// foreach($result2 as $key2 =>$val2)
			// {
				// if($val2->Asset_Type == $val->Asset_Type_Id)
				// {
					// $preference[] = $val;
				// }
			// }
		// }
		// return $preference;
		
	}
	function getAssetType($parent_site_id)
	{
		$this -> db -> select('*');
		$this -> db -> from('rsys_asset_type at');
		//$this -> db -> where('Company_Id',$Company_Id);
		$this -> db -> order_by("Asset_Type_Name","ASC");
		//$this -> db -> where('Asset_Type_Name !=','Campaign Field');
		// $this -> db -> where('Asset_Type_Name !=','FM Folder');
		// $this -> db -> where('Asset_Type_Name !=','Email Folder');
		// $this -> db -> where('Asset_Type_Name !=','Folder');
		$result = $this -> db -> get() -> result();		
		return $result;
	}
	
	function getAssetNameById($Asset_Type_Id)
	{
		$this -> db -> select('*');
		$this -> db -> from('rsys_asset_type at');
		$this -> db -> where('Asset_Type_Name', $Asset_Type_Id);
		$result = $this->db->get()->result()[0]->Asset_Type_Name;		
		return $result;
	}
	function rename_package_name($Contact_Id, $package_name, $package_id, $package_description)
	{
		$current_date_time = new DateTime();
		if($package_id == '' || $package_id == 'null')
		{
			$data['Package_Name'] = $package_name;
			$data['Status_Details'] = $package_description;
			$data['Created_By_Contact_Id']= $Contact_Id;
			$data['Created_At'] = $current_date_time->format('Y-m-d H:i:s');	
			$this -> db -> insert('deployment_package' ,$data);
			return $this -> db -> insert_id();
		}
		else{
			$data['Package_Name'] = $package_name;
			$data['Status_Details'] = $package_description;
			$data['Updated_At'] = $current_date_time->format('Y-m-d H:i:s');
			$this -> db -> where('Deployment_Package_Id', $package_id);
			$this -> db -> update('deployment_package' ,$data);
			return $package_id;
		}
	}
	function copy_Deployment_Package($deployment_Package_Id,$loggedinUser)
	{
		$this -> db -> select('*');
		$this -> db -> from('deployment_package');
		$this -> db -> where('Deployment_Package_Id', $deployment_Package_Id);
		$query = $this -> db -> get() -> result();
		$current_date_time = new DateTime();
		$data = array(
			// 'License_Id' => $query[0]->License_Id,
			'Package_Name' => "Copy of ".$query[0]->Package_Name,
			'Status' => 'New',
			'Source_Site_Name' => $query[0]->Source_Site_Name,
			'Target_Site_Name' => $query[0]->Target_Site_Name,
			'Created_At' =>  $current_date_time->format('Y-m-d H:i:s'),
			'User_Name' => $loggedinUser,
			'Updated_At' => $current_date_time->format('Y-m-d H:i:s'),
			'parent_site_id' => $query[0]->parent_site_id,
			'enable_JSONShell' => $query[0]->enable_JSONShell
		);
		$this -> db -> insert('deployment_package', $data);
		$insert_id = $this->db->insert_id();
		
		$this -> db -> select('*');
		$this -> db -> from('deployment_package_item');
		$this -> db -> where('Deployment_Package_Id', $deployment_Package_Id);
		$this -> db -> where('verified',1);
		$query2 = $this -> db -> get() -> result();
		
		foreach($query2 as $val)
		{
			$data2 = array(
				'Deployment_Package_Id' => $insert_id,
				'Asset_Type' => $val->Asset_Type,
				'Asset_Name' => $val->Asset_Name,
				'Asset_Id' => $val->Asset_Id,
				'Status' => 'New',
				//'JSON_Asset' => $val->JSON_Asset,
				'verified'   => $val->verified
			);
			$result2 = $this -> db -> insert('deployment_package_item', $data2);
		}
		return true;
	}
	
	function delete_Deployment_Package($deployment_Package_Id)
	{
		$this -> db -> where('deployment_Package_Id', $deployment_Package_Id);
		$this -> db -> delete('deployment_queue');
		
		$this -> db -> where('deployment_Package_Id', $deployment_Package_Id);
		$this -> db -> delete('validate_queue');	
		
		$this -> db -> where('deployment_Package_Id', $deployment_Package_Id);
		$this -> db -> delete('deployment_package');	
		
		// $this -> db -> where('deployment_Package_Id', $deployment_Package_Id);
		// $this -> db -> delete('deployment_package_item');
		
		// $this -> db -> where('deployment_Package_Id', $deployment_Package_Id);
		// $this -> db -> delete('deployment_package_validation_list');
		
		return true;
	}
	
	function update_source_site_name($Parent_Site_Id, $Package_Id ,$Source_Site_Name, $package_description, $package_name)
	{
		$data['Source_Site_Name'] = $Source_Site_Name;
		$data['Status_Details'] = $package_description;
		$data['Package_Name'] = $package_name;
		if($Package_Id == "" || $Package_Id == 'null')
		{
			$data['parent_site_id'] = $Parent_Site_Id;
			$data['Created_At'] = date("Y-m-d h:m:s");
			$this -> db -> insert('deployment_package' ,$data);
			$Package_Id = $this -> db -> insert_id();
		}
		else
		{				
			$this -> db -> select('Target_Site_Name');
			$this -> db -> from('deployment_package');
			$this -> db -> where('Deployment_Package_Id', $Package_Id);
			$result = $this -> db -> get();
			if($result -> num_rows() > 0)
			{
				$result = $result -> result();
				if($result[0]->Target_Site_Name != $Source_Site_Name)
				{
					$data['Updated_At'] = date("Y-m-d h:m:s");
					$this -> db -> where('Deployment_Package_Id', $Package_Id);
					$this -> db -> where('parent_site_id', $Parent_Site_Id);
					$this -> db -> update('deployment_package' ,$data);
				}
			}
		}
		return $Package_Id;

	}
	
	function update_target_site_name($Parent_Site_Id, $Package_Id, $Target_Site_Name)
	{
		$this -> db -> select('Source_Site_Name');
		$this -> db -> from('deployment_package');
		$this -> db -> where('Deployment_Package_Id', $Package_Id);
		$result = $this -> db -> get()->result();
		
		if($result[0]->Source_Site_Name != $Target_Site_Name)
		{
			$data['Target_Site_Name'] = $Target_Site_Name;
			$data['Updated_At'] = date("Y-m-d h:m:s");
			$this -> db -> where('Deployment_Package_Id', $Package_Id);
			$this -> db -> where('parent_site_id', $Parent_Site_Id);
			$this -> db -> update('deployment_package' ,$data);
			return $Package_Id;
		}
	}
	
	function delete_deployment_package_item($Package_Id)
	{
		$tdata['Deployment_Package_Id'] = $Package_Id;
		$this->db->delete('deployment_package_item',$tdata);
	}
	
	function getHomeURL($session_data)
	{		
		$homeURL=null;
		if(isset($session_data))
		{
			$homeURL= "transport/doTransport?appId=".$session_data['appId']."&installId=".$session_data['installId']."&userName=".$session_data['userName'] ."&siteName=".$session_data['siteName']."&siteId=".$session_data['siteId']."&UserId=".$session_data['userId']."&UserCulture=".$session_data['UserCulture']."&oauth_consumer_key=".$session_data['oauth_consumer_key']."&oauth_nonce=".$session_data['oauth_nonce']."&oauth_signature_method=".$session_data['oauth_signature_method']."&oauth_timestamp=".$session_data['oauth_timestamp']."&oauth_version=".$session_data['oauth_version']."&oauth_signature=".$session_data['oauth_signature'];
		}
		return $homeURL;
	}
	
	function getAllLandingPages($packageId)
	{
		$this -> db -> select('Asset_Id,Asset_Name,JSON_Asset,Target_Microsite_JSON');
		$this -> db -> from('deployment_package_item');
		$this -> db -> where('Deployment_Package_Id', $packageId);
		$this -> db -> where('Asset_Type', 'Landing Page');
		$this -> db -> where('verified', 1);
		$query = $this -> db -> get();
		$LPResults = $query->result();
		$returnResult=array();
		$lp=0;
		$dependenciesFlag=true;
		$returnResult=null;
		// if($LPResults > 0)
		// {	
			foreach($LPResults as $LPKey=>$LPVal)
			{
				if(isset($LPVal->JSON_Asset) ||  $LPVal->JSON_Asset!='')
				{
					$LPJSON=json_decode($LPVal->JSON_Asset);
					$MSJSON=json_decode($LPVal->Target_Microsite_JSON);
				
					$dependenciesStatus=$this->searchLandingpageDependenciesInTarget($LPJSON->name,$packageId);
					if($dependenciesStatus=='Found')
					{
						$this->unsetDefaultMicrosite($LPJSON->name,$LPJSON->id,$packageId);
						continue;
					}				
					$micrositeFoundStatus=$this->searchLandingpageWithMicrositeInTarget($LPJSON->name,$packageId);
					if($micrositeFoundStatus=='Found')
					{
						continue;
					}	
					if(isset($LPJSON->micrositeId))
					{
						if(isset($MSJSON->name) && isset($MSJSON->id))
						{
							$returnResult[$lp]['MicrositeNameSaved']=$MSJSON->name;
							$returnResult[$lp]['MicrositeIdSaved']=$MSJSON->id;
						}
						$returnResult[$lp]['LandingpageName']=$LPJSON->name;
						$returnResult[$lp]['LandingpageId']=$LPJSON->id;
						$returnResult[$lp++]['MicrositeName']=$this->getMicrositeName($LPJSON->micrositeId,$packageId);	
					}	
				}		
			}
		// }	
		$result['Landingpages'] = $returnResult;
		$result['Microsites'] = $this->getAllTargetMicrosites($packageId);
		$result['defaultTargetMicrosite']=$this->getDefaultTargetMicrosite($packageId);
		return $result;
	}
	
	function getMicrositeName($micrositeId,$packageId)
	{
		$Package_details = $this->deploy_model->getPackage($packageId);
		$instance_details = $this->deploy_model->getInstance($Package_details[0]->Source_Site_Name);
		
		$token 	   = $instance_details [0] -> Token;
		$Base_Url  = $instance_details [0]-> Base_Url;	
		$targetOrg = $instance_details [0];
		
		$url = $Base_Url .'/API/REST/2.0/assets/microsite/'.$micrositeId.'?depth=complete';
		$result_asset = $this->eloqua->get_request($token, $url);
		$result_asset_=json_decode($result_asset['data']);
		
		return $result_asset_->name;
	}
	
	function getAllTargetMicrosites($packageId)
	{
		$Package_details = $this->deploy_model->getPackage($packageId);
		$instance_details = $this->deploy_model->getInstance($Package_details[0]->Target_Site_Name);
		
		$token 	   = $instance_details [0] -> Token;
		$Base_Url  = $instance_details [0]-> Base_Url;	
		$targetOrg = $instance_details [0];
		$tokenExpiryTme = $instance_details [0]-> Token_Expiry_Time;
		
		$url = $Base_Url .'/API/REST/2.0/assets/microsites?depth=complete';
		if(time()+1800 > $tokenExpiryTme)
		{
		   $result2 = $this->eloqua->refreshToken($instance_details[0]);
		   $token = $result2['Token']['access_token'];
		}
		$result_asset = $this->eloqua->get_request($token, $url);
		
		$targetMicrositeNames=array();
		$i=0;
		$result_asset_=json_decode($result_asset['data']);
		
		foreach($result_asset_->elements as $MKey=>$MVal)
		{
			if(isset($MVal->domains[0]) && $MVal->name!='Your First Hypersite')
			{
				$targetMicrositeNames[$i]['id']=$MVal->id;
				$targetMicrositeNames[$i]['name']=$MVal->name;
				$targetMicrositeNames[$i]['domains']=$MVal->domains[0];
			}	
			$i++;
		}	
		return $targetMicrositeNames;
	}
	
	function Save_MicrositeJSON_LP($LPName,$LPID,$MSJSON,$packageId)
	{
		$afftectedRows_DPI=NULL;
		$this -> db -> select('Deployment_Package_Item_Id,Target_Microsite_JSON');
		$this -> db -> from('deployment_package_item');
		$this -> db -> where('Deployment_Package_Id', $packageId);
		$this -> db -> where('Asset_Type', 'Landing Page');
		$this -> db -> where('Asset_Name', $LPName);
		$this -> db -> where('Asset_Id', $LPID);
		$query = $this -> db -> get();
		$LPResults = $query->result();
		//print_r($LPResults[0]->Target_Microsite_JSON);
		if(isset($LPResults[0]))
		{
			$data['Target_Microsite_JSON'] = json_encode($MSJSON);
			$this -> db -> where('Deployment_Package_Id', $packageId);
			$this -> db -> where('Deployment_Package_Item_Id', $LPResults[0]->Deployment_Package_Item_Id);
			$this -> db -> update('deployment_package_item' ,$data);
			$afftectedRows_DPI = $this->db->affected_rows();
		}
		return $afftectedRows_DPI;	
	}
	
	//Method to check Existing Landing has Dependencies in target
	function searchLandingpageDependenciesInTarget($LPName,$packageId)
	{
		$destination='target';
		$needed='id';
		$getLPTargetId = $this->getTargetLPID($LPName,$packageId);
		if($getLPTargetId>0)
		{
			$dependenciesStatus=$this->findDependeciesOfTargetLP($getLPTargetId,$packageId);
		}	
		else
		{
			$dependenciesStatus='Not Found';
		}	
		return $dependenciesStatus;
	}
	
	//Method to check Existing Landing has Microsite assigned
	function searchLandingpageWithMicrositeInTarget($LPName,$packageId)
	{
		$Package_details = $this->deploy_model->getPackage($packageId);
		$instance_details = $this->deploy_model->getInstance($Package_details[0]->Target_Site_Name);
		
		$token 	   = $instance_details [0] -> Token;
		$Base_Url  = $instance_details [0]-> Base_Url;	
		$targetOrg = $instance_details [0];
		
		$this -> db -> select('*');
		$this -> db -> from('rsys_asset_endpoint');
		$this -> db -> where('Asset_Type', 'Landing Page');
		$this -> db -> where('Endpoint_Type', 'Find Asset');
		$query22 = $this -> db -> get();
		$endpoint = $query22->result();
		
		//changes to asset name for special characters
		if(preg_match($this->pattern,  $LPName))
		{
			$tempAssetName = str_replace('-', '_', $LPName);
			$tempAssetName = preg_replace('/[^A-Za-z0-9\-]/','*',$tempAssetName);
		}
		else
			$tempAssetName=urlencode($LPName);
		
		// $url = $Base_Url .$endpoint[0]->Endpoint_URL .'?depth=complete&search="'.str_replace(' ', '%20', $LPName).'"';
		$url = $Base_Url .$endpoint[0]->Endpoint_URL .'?depth=complete&search="'.str_replace(' ', '%20', $tempAssetName).'"';
		
		$result_asset = $this->eloqua->get_request($token, $url);
		$result_asset_=json_decode($result_asset['data'])->elements;
		
		if(isset($result_asset_[0]) && isset($result_asset_[0]->micrositeId))
		{
			$returnResult='Found';
		}
		else
		{
			$returnResult='Not Found';
		}	
		return $returnResult;
	}
	
	function findDependeciesOfTargetLP($targetLPID,$packageId)
	{
		$Package_details = $this->deploy_model->getPackage($packageId);
		$instance_details = $this->deploy_model->getInstance($Package_details[0]->Target_Site_Name);
		$token 	   = $instance_details [0] -> Token;
		$Base_Url  = $instance_details [0]-> Base_Url;	
		$targetOrg = $instance_details [0];
		
		$this -> db -> select('*');
		$this -> db -> from('rsys_asset_endpoint');
		$this -> db -> where('Asset_Type', 'Landing Page');
		$this -> db -> where('Endpoint_Type', 'Read Single');
		$query22 = $this -> db -> get();
		$endpoint = $query22->result();
		
		$url = $Base_Url .$endpoint[0]->Endpoint_URL .'/' .$targetLPID. '/dependencies';
		
		$result_asset = $this->eloqua->get_request($token, $url);
		//in case of no dependencies nothing is returned hence 204. It may change in the future
		if($result_asset['httpCode']==200)
		{
			$returnResult = 'Found';
		}	
		else
		{
			$returnResult = 'Not Found';
		}	
		return $returnResult;
	}
	
	function getTargetLPID($targetLPName,$packageId)
	{
		$Package_details = $this->deploy_model->getPackage($packageId);
		$instance_details = $this->deploy_model->getInstance($Package_details[0]->Target_Site_Name);
		
		$token 	   = $instance_details [0] -> Token;
		$Base_Url  = $instance_details [0]-> Base_Url;	
		$targetOrg = $instance_details [0];
		
		$this -> db -> select('*');
		$this -> db -> from('rsys_asset_endpoint');
		$this -> db -> where('Asset_Type', 'Landing Page');
		$this -> db -> where('Endpoint_Type', 'Find Asset');
		$query22 = $this -> db -> get();
		$endpoint = $query22->result();
		
		//changes to asset name for special characters
		if(preg_match($this->pattern,  $targetLPName))
		{
			$tempAssetName = str_replace('-', '_', $targetLPName);
			$tempAssetName = preg_replace('/[^A-Za-z0-9\-]/','*',$tempAssetName);
		}
		else
			$tempAssetName=urlencode($targetLPName);
		
		// $url = $Base_Url .$endpoint[0]->Endpoint_URL .'?depth=complete&search="'.str_replace(' ', '%20', $targetLPName).'"';
		$url = $Base_Url .$endpoint[0]->Endpoint_URL .'?depth=complete&search="'.str_replace(' ', '%20', $tempAssetName).'"';
		
		$result_asset = $this->eloqua->get_request($token, $url);
		$result_asset_=json_decode($result_asset['data'])->elements;
		
		if(isset($result_asset_[0]))
		{
			$returnResult=$result_asset_[0]->id;
		}
		else
		{
			$returnResult=-1;
		}	
		return $returnResult;
	}
	
	function targetDuplicatesAssets($packageId)
	{
		$this -> db -> select('Deployment_Package_Item_Id,Target_Asset_Id,Asset_Id,Asset_Name,Asset_Type,Target_Duplicate_Assets');
		$this -> db -> from(' deployment_package_item');
		$this -> db -> where('Deployment_Package_Id', $packageId);
		$this -> db -> where('Target_Duplicate_Assets !=','');
		$query = $this -> db -> get();
		$duplicateResults = $query->result();
		return $duplicateResults;
	}

	//Function to save the target selected asset id to db
	function Save_SelectedTargetAssetId($AssetId,$AssetType,$AssetName,$TargetAssetId,$Package_Id,$Package_Item_Id)
	{
		$afftectedRows_DPI=NULL;
		$afftectedRows_DPVL=NULL;
		$assetsWithDuplicate=$this->targetDuplicatesAssets($Package_Id);
		if(isset($assetsWithDuplicate))
		{
			$dupData['Status']='Valid';
			$dupData['Target_Asset_Id']=$TargetAssetId;
			$this -> db -> where('Deployment_Package_Id', $Package_Id);
			$this->db->where('Deployment_Package_Item_Id', $Package_Item_Id);		
			$this->db->update('deployment_package_item', $dupData);
			$afftectedRows_DPI = $this->db->affected_rows();
			
			$dpvl['Target_Asset_Id'] = $TargetAssetId;
			$this->db->where('Deployment_Package_Id',$Package_Id);
			$this->db->where('Asset_Type',$AssetType);
			$this->db->where('Asset_Id',$AssetId);
			$this->db->update('deployment_package_validation_list', $dpvl);
			$afftectedRows_DPVL = $this->db->affected_rows();
		}
		return 	$afftectedRows_DPI;
	}
	
	//By Sunil
	function getPackageStatus($changeset_id)
	{
		$this->db->select('Status');
		$this -> db -> from('deployment_package');
	    $this -> db -> where('Deployment_Package_Id', $changeset_id);
		$result = $this->db->get()->result();
	    return $result;
	}	
	
	function getIncludedAssetCount($changeset_id)
	{
		$this->db->select('count(*) as total');
		$this -> db -> from('deployment_package_item');
	    $this -> db -> where('Deployment_Package_Id', $changeset_id);
	    $result = $this->db->get()->result();
	    return $result[0]->total;
	}
	
	
	function getVariFiedAssetCount($changeset_id)
	{
		$this->db->select('count(*) as total');
		$this -> db -> from('deployment_package_item');
	    $this -> db -> where('Deployment_Package_Id', $changeset_id);
		$this -> db -> where('verified', 1);
	    $result = $this->db->get()->result();
	    return $result[0]->total;
	}	
	
	function getDuplicateAssetCount($changeset_id)
	{
		$this->db->select('count(*) as total');
		$this -> db -> from('deployment_package_item');
	    $this -> db -> where('Deployment_Package_Id', $changeset_id);
		$this -> db -> where('Status', 'Duplicate');
	    $result = $this->db->get()->result();
	    return $result[0]->total;
	}
	
	function getDeployedAssetCount($changeset_id)
	{
		$this->db->select('count(*) as total');
		$this -> db -> from('deployment_package_item');
	    $this -> db -> where('Deployment_Package_Id', $changeset_id);
		$this -> db -> where("(status='Completed' OR status='Action Required')");
		
	    $result = $this->db->get()->result();
	    return $result[0]->total;
	}
	
    function getValidatedAssetCount($changeset_id)
	{
		$this->db->select('count(*) as total');
		$this -> db -> from('deployment_package_item');
	    $this -> db -> where('Deployment_Package_Id', $changeset_id);
		$this -> db -> where('Status' , 'Valid');
	    $result = $this->db->get()->result();
	    return $result[0]->total;
	}	
	
	function getNewAssetCount($changeset_id)
	{
		$this->db->select('count(*) as total');
		$this -> db -> from('deployment_package_item');
	    $this -> db -> where('Deployment_Package_Id', $changeset_id);
		$this -> db -> where('Status' , 'New');
	    $result = $this->db->get()->result();
	    return $result[0]->total;
	}	
	
	function getDefaultTargetMicrosite($packageId)
	{
		$this -> db -> select('target_default_microsite');
		$this -> db -> from('deployment_package');
		$this -> db -> where('Deployment_Package_Id', $packageId);
		$query = $this -> db -> get();
		$defaultMSResults = $query->result()[0];
		return json_decode($defaultMSResults->target_default_microsite)	;	
	}
	function saveDefaultMicrosite($packageId,$MSJSON)
	{
		$afftectedRows_DPI=NULL;
		$this -> db -> select('*');
		$this -> db -> from('deployment_package');
		$this -> db -> where('Deployment_Package_Id', $packageId);
		$query = $this -> db -> get();
		$defaultMSResults = $query->result();
		
		if(isset($defaultMSResults[0]))
		{
			$data['target_default_microsite'] = json_encode($MSJSON);
			$this -> db -> where('Deployment_Package_Id', $packageId);
			$this -> db -> update('deployment_package' ,$data);
			$afftectedRows_DPI = $this->db->affected_rows();
		}
		return $afftectedRows_DPI;
	}
	
	function unsetDefaultMicrosite($landingPageName,$landingPageId,$packageId)
	{
		$this -> db -> select('Target_Microsite_JSON');
		$this -> db -> from('deployment_package_item');
		$this -> db -> where('Deployment_Package_Id', $packageId);
		$this -> db -> where('Asset_Name', $landingPageName);
		$this -> db -> where('Asset_Id', $landingPageId);
		$query = $this -> db -> get();
		if($query->num_rows()>0)
		{
			$defaultMSResults = $query->result();
			foreach($defaultMSResults as $key=>$val)
			{
				if(isset($val->Target_Microsite_JSON) || $val->Target_Microsite_JSON !='')
				{
					$this->db->set('Target_Microsite_JSON', null);
					$this->db->where('Deployment_Package_Id', $packageId);
					$this -> db -> where('Asset_Name', $landingPageName);
					$this -> db -> where('Asset_Id', $landingPageId);
					$this->db->update('deployment_package_item');
				}
			}
		}		
	}

	function skipValidationOnDeploy($packageId,$skipValidationFlag)
	{
		$data['skip_validation']=$skipValidationFlag;
		$this->db->where('Deployment_Package_Id', $packageId);
		$this->db->update('deployment_package', $data);
	}

	function getSkipValidation($packageId)
	{
		$this->db->select('skip_validation');
		$this->db->from('deployment_package');
		$this->db->where('Deployment_Package_Id',$packageId);
		$skipValidationFlag = $this -> db -> get()->result()[0];
		return $skipValidationFlag->skip_validation;
	}
	
	function defaultMicrositeAssignedStatus($packageId)
	{
		$AssetTyeArr = array('Account Field','Account View', 'Campaign Field' , 'Contact Field', 'Contact View', 'Custom Object', 'Field Merge', 'File Storage', 'Image', 'Picklist' ,'Signature Rule');
		
		$assetWithLPFlag=0;
		$defaultMicrositeFlag=0;
		
		$this->db->select('Asset_Id');
		$this->db->from('deployment_package_item');
		$this->db->where('verified',1);
		$this->db->where_not_in('Asset_Type', $AssetTyeArr);
		$this->db->where('Deployment_Package_Id',$packageId);
		$AssetWithLP = $this -> db -> get();
		if($AssetWithLP->num_rows()>0){
			$assetWithLPFlag=1;
		}
		
		$this->db->select('target_default_microsite');
		$this->db->from('deployment_package');
		$this->db->where('Deployment_Package_Id',$packageId);
		$defaultMicrosite = $this -> db -> get()->result()[0]->target_default_microsite;	
		if($defaultMicrosite =='' || $defaultMicrosite ==null )
		{
			$defaultMicrositeFlag = 1;
		}
	
		if($defaultMicrositeFlag==1 && $assetWithLPFlag==1)
		{
			$return=1;
		}
		else{
			$return=0;
		}
		return $return;
	}
	
	function getShellParentChildConfig()
	{
		$this->db->select('Parent_Asset_Type,Child_Asset_Type');
		$this->db->from('rsys_asset_shellJSON');
		$ShellParentChildConfig = $this -> db -> get();
		if($ShellParentChildConfig->num_rows()>0)
		{
			
			$ShellParentChildConfigResult=$ShellParentChildConfig->result();
		}
		else{
			$ShellParentChildConfigResult='No Configuration exists.';
		}
		return $ShellParentChildConfigResult;
	}
}		
?>