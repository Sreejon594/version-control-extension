<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class License_model extends CI_Model{
	function __construct()
	{
		parent::__construct();
	}

	function isSiteLicensed($siteName)
	{
		$instanceId = $this->getInstanceIdForSiteName($siteName);
		$licenses = $this->getLicensesInvolvedInInstance($instanceId);
		foreach($licenses as $key=>$val)
		{
			if($this->isLicenseValid($val))
			{
				return true;
			}
		}
		return false;
	}
	
	//Sends out the list of related sites, which share common lisenses, This is for admin panel to display sites. 
	function associatedSitesForSiteName($siteName)
	{
		//gets the licenses in which this site is involved then get the sites that are involde in those lisenses.
		$associatedSites = Array();
		$associatedinstances = Array();
		$this -> db -> select('*');
		$this -> db -> from('instance_');
		$this -> db -> where('Site_Name' ,$siteName);
		$query =  $this -> db -> get();
		if($query->num_rows()>0)
		{
			$result = $query -> result_array();
			$instance_id = $result[0]['Instance__Id'];
			$licenses = $this->getLicensesInvolvedInInstance($instance_id);
			
			if(sizeof($licenses)>0)
			{
				foreach($licenses as $key=>$licenseVal)
				{
					if($this->isLicenseValid($licenseVal))
					{
						$instancesForLicense = $this->getInstacesInvolvedInLicense($licenseVal);
						if(sizeof($instancesForLicense)>0)
						{
							foreach($instancesForLicense as $key2 => $instancesVal)
							{
								$siteForInstance = $this->getSiteNameForInstance($instancesVal);
								array_push($associatedSites,$siteForInstance);
							}
						}
					}	
				}
			}			
		}
		return $associatedSites;
	}
	
	function isLicenseValid($lisenseId)
	{
		$today = date('Y/m/d');
		$this -> db -> select('*');
		$this -> db -> from('license');
		$this -> db -> where('license_id' ,$lisenseId);
		$this -> db -> where('valid_till >=', $today);
		$query =  $this -> db -> get();
		if($query->num_rows()>0)
		{
			return true;
		}
		return false;
	}
	
	function getInstanceIdForSiteName($siteName)
	{
		$this -> db -> select('*');
		$this -> db -> from('instance_');
		$this -> db -> where('site_name' ,$siteName);
		$query =  $this -> db -> get();
		if($query->num_rows()>0)
		{
			$result = $query -> result_array();
			return $result[0]['Instance__Id'];
		}
	}	
	
	function getSiteNameForInstance($instanceId)
	{
		$this -> db -> select('*');
		$this -> db -> from('instance_');
		$this -> db -> where('instance__id' ,$instanceId);
		$this -> db -> where('Token !=', '');
		$query =  $this -> db -> get();
		if($query->num_rows()>0)
		{
			$result = $query -> result_array();
			return $result[0]['Site_Name'];
		}
	}
	
	function getInstacesInvolvedInLicense($lisenseId)
	{
		$instances = Array();
		$this -> db -> select('*');
		$this -> db -> from('license_instance');
		$this -> db -> where('license_id' ,$lisenseId);
		$query = $this -> db -> get();
		if($query->num_rows()>0)
		{
			$result = $query->result();
			foreach($result as $key=>$val)
			{
				array_push($instances,$val->instance_id);
			}
		}
		return $instances;
	}
	
	function getLicensesInvolvedInInstance($instanceId)
	{
		$licenses = Array();
		$this -> db -> select('*');
		$this -> db -> from('license_instance');
		$this -> db -> where('instance_id' ,$instanceId);
		$query = $this -> db -> get();
		if($query->num_rows()>0)
		{
			$result = $query->result();
			foreach($result as $key=>$val)
			{
				array_push($licenses,$val->license_id);
			}
		}
		return $licenses;
	}
	
	function haveCommonLicenses($siteName1,$siteName2)
	{
		$instance1 = $this->getInstanceIdForSiteName($siteName1);
		$instance2 = $this->getInstanceIdForSiteName($siteName2);
		$licenses1 = $this->getLicensesInvolvedInInstance($instance1);
		$licenses2 = $this->getLicensesInvolvedInInstance($instance2);
		foreach($licenses1 as $key=>$val)
		{
			foreach($licenses2 as $key1=>$val1)
			{
				if($val==$val1)
				{
					return true;
				}
			}
		}
		return false;
	}
}
?>