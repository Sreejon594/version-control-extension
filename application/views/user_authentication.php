<style>	
.removed {
	background-color: #fd7f7f;
}
.added {
	background-color: #8bff7f;
}
.changed {
	background-color: #fcff7f;
}

#page-wrapper{
	//margin-top: 50px;
	font-size: 12px !important;
}
#page-wrapper{
	margin-top: 20px;
	margin-left: 15%;
}
.alert{
	border: 0px solid transparent !important ; 
}
.mainContainer {
    margin-left: -127px !important;
}
</style>

<link href="https://transporter-dev.portqii.com/assets/plugins/jqgrid/css/ui.jqgrid.css" rel="stylesheet" type="text/css" />
<link href="https://transporter-dev.portqii.com/assets/css/layout-jqgrid.css" rel="stylesheet" type="text/css" />
				
<!-- Navigation -->
<div id="page-wrapper" class="col-lg-8 col-md-8 col-sm-10 col-xs-10 col-lg-offset-1 col-md-offset-1" >
	<div class="container-fluid">                
		<!-- /.row -->
		<div class="row">
			<div class="col-lg-12" style="font-size:13px !important;">
		<?php 
			if($sessionExpireStatus==true)
			{
		?>
				<div class="alert alert-warning"><strong>Warning !</strong>	
					Session is expired. Re-launch application from your Eloqua Instance.
				</div>	
		<?php	
			}
			else if($userPermissionStatus==false)
			{	
		?>
				<div class="alert alert-warning"><strong>Warning !</strong>	
					You are not authorized to access this application. Contact your system administrator to get access.
				</div>	
		<?php 
			}
			else if($securityCheckStatus==false)
			{
		?>	
				<div class="alert alert-warning"><strong>Warning !</strong>	
					Invalid Package Details.
				</div>	
		<?php 
			}
		?>		
			</div>
		</div>
	</div>
</div>