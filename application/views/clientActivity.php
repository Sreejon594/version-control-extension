<!DOCTYPE html>
<!--[if IE 8]>			<html class="ie ie8"> <![endif]-->
<!--[if IE 9]>			<html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->	<html> <!--<![endif]-->
	<head>
		<title><?php echo $page_title;  ?></title>
		<meta charset="utf-8" />
		<meta name="keywords" content="HTML5,CSS3,Template" />
		<meta name="description" content="" />
		<link rel="shortcut icon" href="/assets/image/favicon.ico" type="image/x-icon" />
		<link rel="icon" href="/assets/image/favicon.ico" type="image/x-icon"> 

		<!-- mobile settings -->
		<meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0" />
		<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->

		<!-- WEB FONTS : use %7C instead of | (pipe) -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400%7CRaleway:300,400,500,600,700%7CLato:300,400,400italic,600,700" rel="stylesheet" type="text/css" />
		<!-- SWIPER SLIDER 
		<link href="<?php echo base_url();?>assets/plugins/slider.swiper/dist/css/swiper.min.css" rel="stylesheet" type="text/css" />-->

		<!-- CORE CSS -->
		<link href="<?php echo base_url(); ?>assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
		
		<!-- THEME CSS -->
		<link href="<?php echo base_url(); ?>assets/css/essentials.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url(); ?>assets/css/layout.css" rel="stylesheet" type="text/css" />

		<!-- PAGE LEVEL SCRIPTS -->
		<link href="<?php echo base_url(); ?>assets/css/header-1.css" rel="stylesheet" type="text/css" />
		
		<link href="<?php echo base_url(); ?>assets/css/layout-datatables.css" rel="stylesheet" type="text/css" />	
		 		
		
		<style>
			
			img.pull-left 		
			{ 
				margin:0 34px 10px 0px !important;  
			}
			.sticky.clearfix.fixed .navbar-collapse
			{
				margin-top: 10px !important;
			}
			.mobile_button{
				margin-top: -10px !important;
			}
			.dropdown-menu{
				//height: 41px !important;
				min-width: 150px !important;
			}
			#topNav div.submenu-dark ul.dropdown-menu>li a {
				color: black !important;
			}
			#topNav div.submenu-dark ul.dropdown-menu {
				background-color: white !important;
			}
			.clearfix{
				position: fixed !important;
				border-bottom: rgba(0,0,0,0.08) 1px solid !important;
			}
			#page-wrapper{
				padding-top: 2px !important;
			}
			body{
				padding-top: 0px !important;
			}
			.profileNav{
				list-style-type: none;
				//margin-top: 5px;
				font-size: 12px;
			}
			@font-face {
			  font-family: "expansiva";
			  src: url(<?php echo base_url();?>assets/font/kimberle.ttf) format("otf");
			}
			.expansiva
			{
				font-family: "expansiva", sans-serif ;
				margin:0px 0px 0px -16px;
			}
			#eloquaUsersTable_paginate{
				border: 1px solid #ddd;
				padding-top:0px;
			}
			.form-control{
				border:1px solid #ddd !important;
				border-radius: 1px;

			}
			.mark , mark{
				background-color: #ff0 !important;
			}
			
		</style>
		<!-- jQuery -->
		<script src="<?php echo base_url(); ?>assets/js/jquery.js"></script>
		<script type="text/javascript">var plugin_path = '<?php echo base_url(); ?>assets/plugins/';</script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/jquery/jquery-2.1.4.min.js"></script>
		
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/datatables/js/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/datatables/js/dataTables.tableTools.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/datatables/js/dataTables.colReorder.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/datatables/js/dataTables.scroller.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/select2/js/select2.full.min.js"></script>
	
		<!-- START Of Online JS libraries of Data Table -->
		<script src="https://cdn.jsdelivr.net/mark.js/8.6.0/jquery.mark.min.js"></script>
		<script src="https://cdn.jsdelivr.net/datatables.mark.js/2.0.0/datatables.mark.min.js"></script>
		<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.css">	
		<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>
		<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css"/>
		<!-- END Of Online JS libraries of Data Table -->
	</head>
	<body style="font-size: 12px;">
		<div id="page-wrapper" style="margin-top:-12px;">
			<div class="container-fluid" style="">                
				<div class="row profileDiv" style="">
					<div class="col-md-2" style=""></div>
					<div class="col-sm-12 col-xs-12 col-md-8 col-lg-8 mainContainer" style="height:auto;padding:0px 0px 0px 0px;">
						<nav class="navbar navbar-inverse" style="background-color: #fff;    border-color: #fff;">
							<div class="container-fluid">
							<div class="navbar-header">
							  <a class="navbar-brand" href="#" style="cursor:default;"><h3 class="expansiva">Oracle Eloqua Transporter</h3></a>
							</div>
							<ul class="nav navbar-nav">
							 
							</ul>
							<ul class="nav navbar-nav navbar-right">
								<li style="margin-top:10px;">								
									<!-- New PORTQII LOGO Code  -->
									<a href="http://www.portqii.com" target="_blank">
										<p class="color_change1 poweredby pull-left" style="font-weight: bold; margin-top:-6px;margin-right: -13px;margin-bottom: 0px;font-size: 12px !important;color:black;">powered by</p>
										<img class="pull-right" src="<?php echo base_url(); ?>assets/image/portqii_newLogo.png" style="height: 28px;margin-right:-15px;margin-top: -15px;">
									</a>
								</li>
							</ul>
						  </div>
						</nav>
					</div>
					<div class="col-md-2" style=""></div>
				</div>
				<div class="row profileDiv" style="">
					<div class="col-lg-10 col-lg-offset-1" style="padding-right: 0px !important; padding-left: 0px !important;">
						<div class="col-md-12" style="margin-bottom:0px; padding:0px;">
							<div class="col-md-12 searchUser" style="padding:0px;">
								<div class="col-md-2" style="padding:0px;">
									<b class="userName">Select Site name :</b>
								</div>
								<div class="col-md-3" style="padding:0px;margin-left: -45px;">
									<select class="form-control siteName_C" id="siteName_I" name="assetType" style="height:27px;padding: 1px 12px;font-size: 12px;">
										<option value=""></option>
										<option value="TechnologyPartnerPortQii">TechnologyPartnerPortQii</option>
										<option value="TechnologyPartnerPortqiiPteLtd">TechnologyPartnerPortQiiPTELTD</option>
										<option value="JanssenNATest">JanssenNATest</option>
										<option value="ClarivateAnalytics">ClarivateAnalytics</option>
										<option value="ThomsonReutersScience">ThomsonReutersScience</option>
									</select>
								</div>
								<div class="col-md-2" style="padding:0px;padding-left: 5px;">		
									<a class="btn btn-default btn-sm searchText pull-center" id ="searchTextId" onclick="getUsersOnType()" style="height: 25px;margin-top: 0px;border:1px solid #ddd;">Search</a>
									<img align="middle" id = "LoaderOnLoad" style ="opacity:1;top:18%;left:53%;z-index: 1;position: absolute; display: none;" src="https://transporter-dev.portqii.com/assets/image/Loader.gif"/>
								</div>			
								
								<div class="col-md-6" style="padding:0px;">		
									
								</div>
								
								<div class="col-md-11" style="padding:5px 0px 5px 0px;">		
									<div class="msgBox" style="margin-bottom:0px;padding: 7px;font-weight: bold;padding-left: 11px;">
									<hr>
									</div>
								</div>
							</div>
							<div class="col-md-12 eloquaUsers" style="padding:0px;display:none;">
								<table class="table table-striped"
								Style="margin-top: 5px;border: 1px solid #ccc; width: 100% !important;" id="eloquaUsersTable"></table>
							</div>	
							<br/>
						</div>
					</div>
				</div>
			</div>
		</div>
	
		<script type="text/javascript">
			$(document).ready(function()
			{ 
				$.extend(true, $.fn.dataTable.defaults, {
					mark: true
				});
		
				$('#eloquaUsersTable').DataTable(
				{
					"dom" : '<"top">rt<"btm"><"clear">',
					responsive : true,
					data : null,
					columns : [					
							{
								title : "Package Id",width:"10%"
							},
							{
								title : "Package Name",width:"15%"
							},
							{
								title : "Source Site Name",width:"15%"
							},
							{
								title : "Target Site Name",width:"15%"
							},
							{
								title : "Created By",width:"15%"
							},
							{
								title : "Created On",width:"15%"
							},
							{
								title : "Status",width:"15%"
							},				
					],
				});
			});
			
			function getUsersOnType() 
			{			
				var searchText = $('.siteName_C :selected').val();
				
				var table = $('#eloquaUsersTable').DataTable()
				$('.eloquaUsers').hide();
				table.destroy();
				if(searchText==null)
				{	
					$('.siteName_C').css("border-color", "red");
				}
				else 
				{					
					// alert(searchText);
					
					$.ajax(
					{
						type : 'POST',
						url : 'https://transporter-dev.portqii.com/settings/getPackageListBySiteName',
						data : {searchText:searchText},
						dataType : 'json',
						beforeSend : function() {
							$('#LoaderOnLoad').show();	
						},
						success : function(result) 
						{
							console.log(result);
							$('#LoaderOnLoad').hide();
							$('#eloquaUsersTable').find('.loader').hide();					
							var serchedUserDetails = []; 
							if(result!=null && result.length>0)
							{
								for (var i = 0; i < result.length; i++) 
								{
									var userDetails = [];
									var packageId = result[i].Deployment_Package_Id;
									var packageName = result[i].Package_Name;
									var sourceSiteName = result[i].Source_Site_Name;
									var targetSiteName = result[i].Target_Site_Name;
									var createdBy = result[i].User_Name;
									var createdOn = result[i].Created_At;
									var status = result[i].Status;
										
									userDetails.push(packageId);
									userDetails.push(packageName);
									userDetails.push(sourceSiteName);
									userDetails.push(targetSiteName);
									userDetails.push(createdBy);
									userDetails.push(createdOn);
									userDetails.push(status);				
									serchedUserDetails.push(userDetails);							
								}
								//$('.eloquaUsers').show();
								console.log(serchedUserDetails);		
							}										
							populateEloquaUser(serchedUserDetails);
							$('.eloquaUsers').show();
						},
						error : function() 
						{
							// loadUser=true;
						}
					});						
				}	
			}

			function populateEloquaUser(userDetails) 
			{
				$('#eloquaUsersTable').DataTable().destroy();
				$('#eloquaUsersTable').DataTable(
				{
					"dom" : '<"top"f>rt<"btm"pi><"clear">',
					responsive : true,
					data : userDetails,
					columns : [					
							{
								title : "Package Id",width:"10%"
							},
							{
								title : "Package Name",width:"15%"
							},
							{
								title : "Source Site Name",width:"15%"
							},
							{
								title : "Target Site Name",width:"15%"
							},
							{
								title : "Created By",width:"15%"
							},
							{
								title : "Created On",width:"15%"
							},
							{
								title : "Status",width:"15%"
							},			

					],
					

					"order" : [ [ 0, "des" ] ],
					"language" : {
						"sSearch" : "",
						"pagingType" : "scrolling",
						"infoEmpty" : "No records available",
						"lengthMenu" : "_MENU_",
						"searchPlaceholder" : "Filter"
					},
					"fnDrawCallback" : function(oSettings) {
						if (Math.ceil((this.fnSettings().fnRecordsDisplay())/ this.fnSettings()._iDisplayLength) > 1) 
						{
							$('#eloquaUsersTable_paginate').css("font-size", "12px");
							$(".dataTables_empty").css("display","block");
							$(".form-control").css("font-weight","normal");
							$('#eloquaUsersTable_filter').css("font-size","12px");
							$('#eloquaUsersTable_paginate').css("display","block");
							$('#eloquaUsersTable_paginate').css("margin-top", "-10px");
							$('#eloquaUsersTable_paginate').css("margin-bottom", "-10px")
							$('#eloquaUsersTable_infoEmpty').css("display", "block")
							$('table.dataTable thead .sorting_desc').css("background-image", "none").css("font-size", "12px");
							$('table.dataTable thead .sorting_asc').css("background-image", "none").css("font-size", "12px");
							$('table.dataTable thead .sorting').css("background-image", "none").css("font-size", "12px");
							$('#eloquaUsersTable_info').css("display","none");					
						} 
						else 
						{
							$('#eloquaUsersTable_info').css("display","none");
							$(".form-control").css("font-weight","normal");
							$('#eloquaUsersTable_filter').css("display","none");
							$('#eloquaUsersTable_paginate').css("display","none");
							$(".dataTables_empty").css("display","none");
							$('#eloquaUsersTable_info').css("font-size","12px");
							$('table.dataTable thead .sorting_desc').css("background-image", "none").css("font-size", "12px");
							$('table.dataTable thead .sorting_asc').css("background-image", "none").css("font-size", "12px");
							$('table.dataTable thead .sorting').css("background-image", "none").css("font-size", "12px");
						}
					},
					"iDisplayLength" : 10,
				});
				$('#eloquaUsersTable').removeClass('dataTable');
				$('div.dataTables_filter input').addClass('form-control');
			}
	
		</script>
	</body>
</html>	