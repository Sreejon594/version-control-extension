	<style>	
	.removed {
		background-color: #fd7f7f;
	}
	.added {
		background-color: #8bff7f;
	}
	.changed {
		background-color: #fcff7f;
	}
	#diffinfo{
		display: none;
		border: solid #337ab7;	
		position: fixed; 
		bottom: 0px;
		top:50px;
		background-color: #333;
		color: #fff;
		height: auto; 
		overflow: auto;
	}
	#diffinfo .panel-heading
	{
		padding-bottom: 25px;
		padding-top: 0px;
		border-bottom: 2px solid;
	}
	#diffinfo .info_body
	{
		overflow: auto;
		margin-top: 5px;
	}
	#diffinfo .panel-title
	{
		float: left;
	}
	#diffinfo .close
	{
		float: right;
		opacity: 1;
		color: #ffffff;
	}
	#page-wrapper{
		margin-top: 50px;
		font-size: 13px !important;
	}
	
	</style>		
        <!-- Navigation -->
        <div id="page-wrapper">
            <div class="container-fluid">                
                <!-- /.row -->
				<div class="row">
					<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 col-lg-offset-1 col-md-offset-1">						
							<div class="pull-left">
								<p class="lead"> <b>DEPLOYMENT PACKAGE</b></p>
							</div>
							<div class="pull-left">
								<div class="row" style=" margin-bottom: 10px;">									
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
										<p style="font-family: 'Open Sans',Arial,Helvetica,sans-serif !important;
											font-size: 13px !important;
											line-height: 1.5; !important">Catch Your Data Before It's Gone
											Oracle Eloqua's data retention policy is changing soon! Find out how this change impacts users and what solutions are available. Details Here
											A Podcast for Marketers by Marketers Real Experiences and inspirational stories of marketing leaders that are transforming their organization using the Oracle Marketing Cloud. Listen Now →
										</p>
									</div>							
									
									<div style="" class="col-xs-12 col-sm-12 col-md-12">	
										<div class="col-md-4 col-md-offset-4">
											<a href="<?php echo base_url();?>home/newlist" role="button" class="btn btn-sm btn-default" >NEW PACKAGE</a>
											<!--<a href="/compare" role="button" class="btn btn-default" >Compare</a>-->
										</div>							
									</div>
								</div>						
								<?php
								if( isset($page_data['changesetList']))
								{
								?>		
								<div class="row">
									<div class="col-lg-12">
										<div class="">											
											<div class="">
												<div class="table-responsive">
													<div class="container">
														  <?php 
														  echo '<table class="table table-striped table-bordered table-hover" id="datatable_sample2">
																			<thead>
																			<tr>
																				<th>ID #</th>
																				<th>Source Org Name</th>
																				<th>Difference Type</th>
																				<th>Created Date</th>
																				<th>Status</th>
																			</tr>
																		</thead>
																		<tbody> ';
																foreach($page_data['changesetList'] as $key =>$val)
																{		
																	// echo '<form id="form'.$key.'" role="form" action="/changesetinfo?id='.$val->id.'" method="post">';											
																	// echo '<input type="hidden" name="org_id" value="'.$val->org_id.'"/>';
																	// echo '<input type="hidden" name="changeset_id" value="'.$val->id.'"/>';		
																	echo '<tr onclick="window.location = \''.base_url().'changesetinfo?id='.$val->id.'\'" style="vertical-align: top; cursor:pointer;">
																		<td>'.$val->id.'</td>
																		<td>'.$val->OrgName.'</td>
																		<td>'.$val->list_name.'</td>
																		<td>'.$val->created_at.'</td>';
																		if($val->status = 0)
																		echo '<td><span class="label label-sm label-success">Not yet deployed </span></td>';
																		elseif($val->status = 1)
																		echo '<td><span class="label label-sm label-success">Deployed Successfully</span></td>';
																		elseif($val->status = 2)
																		echo '<td><span class="label label-sm label-success">Partially Deployed</span></td>';
																echo '</tr>';
																// echo '</form>';
																}													
																echo '</tbody>
																	</table>';  
																 
														  ?>	
													</div>
													
												</div>								
											</div>
										</div>
									</div>					
								</div>
								<?php } ?>
							</div>				
									
					</div>
				</div>
            </div>
		</div>
        <!-- /#page-wrapper -->
   <script>
	$(document).ready(function(){
	if (jQuery().dataTable) {

				function initTable6(tabname) {
					var table = jQuery(tabname);

					table.dataTable({
						"columns": [{
							"orderable": false
						}, {
							"orderable": true
						}, {
							"orderable": false
						}, {
							"orderable": false
						}, {
							"orderable": true
						}
						],
						"lengthMenu": [
							[5, 15, 20, -1],
							[5, 15, 20, "All"] // change per page values here
						],
						// set the initial value
						"pageLength": 5,            
						"pagingType": "bootstrap_full_number",
						"language": {
							"lengthMenu": "  _MENU_ records",
							"paginate": {
								"previous":"Prev",
								"next": "Next",
								"last": "Last",
								"first": "First"
							}
						},
						"columnDefs": [{  // set default column settings
							'orderable': false,
							'targets': [0]
						}, {
							"searchable": false,
							"targets": [0]
						}],
						"order": [
							[1, "asc"]
						] // set first column as a default sort by asc
					});

					var tableWrapper = jQuery('#datatable_sample_wrapper');

					table.find('.group-checkable').change(function () {
						var set = jQuery(this).attr("data-set");
						var checked = jQuery(this).is(":checked");
						jQuery(set).each(function () {
							if (checked) {
								jQuery(this).attr("checked", true);
								jQuery(this).parents('tr').addClass("active");
							} else {
								jQuery(this).attr("checked", false);
								jQuery(this).parents('tr').removeClass("active");
							}
						});
						jQuery.uniform.update(set);
					});

					table.on('change', 'tbody tr .checkboxes', function () {
						jQuery(this).parents('tr').toggleClass("active");
					});

					tableWrapper.find('.dataTables_length select').addClass("form-control input-xsmall input-inline"); // modify table per page dropdown

				}
				
				initTable6('#datatable_sample2');
				initTable6('#datatable_sample');

			} });
</script>				
<style>
.checkbox label:after, 
.radio label:after {
    content: '';
    display: table;
    clear: both;
}

.checkbox .cr,
.radio .cr {
    position: relative;
    display: inline-block;
    border: 1px solid #a9a9a9;
    border-radius: .25em;
    width: 1.3em;
    height: 1.3em;
    float: left;
    margin-right: .5em;
}

.radio .cr {
    border-radius: 50%;
}

.checkbox .cr .cr-icon,
.radio .cr .cr-icon {
    position: absolute;
    font-size: .8em;
    line-height: 0;
    top: 50%;
    left: 20%;
}

.radio .cr .cr-icon {
    margin-left: 0.04em;
}

.checkbox label input[type="checkbox"],
.radio label input[type="radio"] {
    display: none;
}

.checkbox label input[type="checkbox"] + .cr > .cr-icon,
.radio label input[type="radio"] + .cr > .cr-icon {
    transform: scale(3) rotateZ(-20deg);
    opacity: 0;
    transition: all .3s ease-in;
}

.checkbox label input[type="checkbox"]:checked + .cr > .cr-icon,
.radio label input[type="radio"]:checked + .cr > .cr-icon {
    transform: scale(1) rotateZ(0deg);
    opacity: 1;
}

.checkbox label input[type="checkbox"]:disabled + .cr,
.radio label input[type="radio"]:disabled + .cr {
    opacity: .5;
}
.checkbox, .radio {
    margin-top: 0px;
    margin-bottom: 0px;
}
.checkbox label, .radio label {
    min-height: 20px;
    padding-left: 5px;
}
</style>