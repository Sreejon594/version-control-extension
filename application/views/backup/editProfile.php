<style>
.material-switch > input[type="checkbox"] {
    display: none;   
}

.material-switch > label {
    cursor: pointer;
    height: 0px;
    position: relative; 
    width: 40px;  
}

.material-switch > label::before {
    background: rgb(0, 0, 0);
    box-shadow: inset 0px 0px 10px rgba(0, 0, 0, 0.5);
    border-radius: 8px;
    content: '';
    height: 16px;
    margin-top: -8px;
    position:absolute;
    opacity: 0.3;
    transition: all 0.4s ease-in-out;
    width: 40px;
}
.material-switch > label::after {
    background: rgb(255, 255, 255);
    border-radius: 16px;
    box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.3);
    content: '';
    height: 24px;
    left: -4px;
    margin-top: -8px;
    position: absolute;
    top: -4px;
    transition: all 0.3s ease-in-out;
    width: 24px;
}
.material-switch > input[type="checkbox"]:checked + label::before {
    background: inherit;
    opacity: 0.5;
}
.material-switch > input[type="checkbox"]:checked + label::after {
    background: inherit;
    left: 20px;
}
.profileDiv{
	margin-top: 50px ;
}
</style>
<div class="container" style="background:#fff;">
      <div class="row profileDiv">
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-3 toppad" >
   
   
          <div class="">
            <div class="panel-heading">
              <h3 class="" ><?php echo $user_info[0]->first_name.' '.$user_info[0]->last_name  ?></h3>
            </div>
            <div class="panel-body">
				<div class="row">
					<form action="<?php echo base_url();?>/settings/editprofile" method="post" enctype="multipart/form-data">           
						<div > 
						
						  <table class="table table-user-information">
							<tbody>							 
							   <tr>
									<td>Company:</td>
									<td><?php echo $user_info[0]->Company_Name;?></td>
								</tr>							 
							  <tr>
								<td>Email</td>
								<td><?php echo $user_info[0]->email;?></td>
							  </tr>
							   <tr>
								<td>Mobile Number</td>
								<td><input type="text" name="mobile" class="form-control" value="<?php echo $user_info[0]->mobile;?>" /></td>                           
							  </tr>
							   <tr>
								<td>Admin Debug Mode</td>
								
								
								
								<td>
									<div class="material-switch pull-right">
										<input name="DebugMode" type="checkbox" <?php echo $user_info[0]->admin_access==1 ? 'checked':'';?> />
										<label for="DebugMode" class="label-primary"></label>
									</div>
								</td>
							
							
							  </tr>
							 
							</tbody>
						  </table>
						
						</div>
					</form>
              </div>
			  <span class="pull-left">
					<a class="btn btn-default" href="<?php echo base_url();?>Settings/password" >Change Password</a>
					
				</span>
			  <span class="pull-right">
					<a href="<?php echo base_url();?>settings/editprofile" data-original-title="Edit this user" data-toggle="tooltip" type="button" class="btn btn-sm btn-default">Save Profile</a>
				</span>
			  
            </div>            
          </div>
        </div>
      </div>
    </div>
	<?php 
//echo '<pre>';
//print_r ($page_data);
//echo '</pre>';
?>

<style>
.kv-avatar .file-preview-frame,.kv-avatar .file-preview-frame:hover {
    margin: 0;
    padding: 0;
    border: none;
    box-shadow: none;
    text-align: center;
}
.kv-avatar .file-input {
    display: table-cell;
    max-width: 220px;
}
</style>


