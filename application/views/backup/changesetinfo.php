	<!-- PAGE LEVEL STYLE -->
	<style>
	form {
		margin-bottom: 5px !important; 
	}
	.nestableDiv{
		margin-bottom: 15px;
		margin-top: 15px;
	}
	.profileDiv{
		margin-top: 50px ;
	}
	.inlineForm{		
		display: inline !important;
		margin-left: 50px;
		margin-right: 20px;
	}
	.nestableForm{
		margin-bottom: 55px;
		margin-top: 55px;
	}
	.item-box figure {
		text-align: left;
	}
	#demo3{
		padding-left: 30px;
	}
	#SubMenu1{
		padding-left: 50px;
	}
	.asset_list {
		margin-left: 20px;
	}
	.asset_struct {
		margin-left: 20px;
	}
	.asset_struct{
		display: none;
	}
	.source,.destination{
		max-height: 300px;
		overflow: overlay;
	}
	.asset_item{
		padding: 10px 12px;
		padding-right: 100px;
	}
	.list-group-item {
		padding: 10px 15px !important;
		margin-bottom: -1px !important;
		border: 0px !important;
	}
	.borderBottom{
		border-bottom: rgba(0,0,0,0.05) 3px solid !important;
	}
	.mix{
		border: 1px solid #dddddd;
		height: 280px;
	}
	.searchCompButton{
		margin-top:15px;
	}
	.inlineFormInput{
		margin-right: 20px;
	}
	.inlineFormButton{
		margin-bottom: -18px;
	}
</style>    
	<!-- Navigation -->
        <div id="page-wrapper"  >
            <div class="container-fluid">                
                <!-- /.row -->
			<div class="row profileDiv">
			<div class="">			
				<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 col-lg-offset-1 col-md-offset-1">
					<div class="">
						<h5 class="lead"><b>Deployment Package </b>
							<?php	
								print_r($page_data['changeset_info']->list_name);
							?>
						</h5>
					</div>
					<div class="panel-body">
						<div class="row" style=" margin-bottom: 10px;">
							<div style="" class="col-xs-12 col-sm-12 col-md-12">	
							<!--<div class="col-md-4 col-md-offset-5">
								<form  role="form" action="/compare" method="post" style="float: left;margin-right: 5px;" >
									<input type="hidden" name="org_id" value=""/>
									<input type="hidden" name="changeset_id" value="<?php// echo isset($page_data['changeset_id'])?$page_data['changeset_id']:'';?>"/>
									<input type="submit" value="Edit" role="button" class="btn btn-primary" />
								</form>
								<form  role="form" action="/changesetinfo/delete" method="post" style="float: left;margin-right: 5px;" onSubmit="if(!confirm('Are you sure you want to delete this change set?')){return false;}">
									<input type="hidden" name="org_id" value=""/>
									<input type="hidden" name="changeset_id" value="<?php //echo isset($page_data['changeset_id'])?$page_data['changeset_id']:'';?>"/>
									<input type="submit" value="Delete" role="button" class="btn btn-primary" />
								</form>
								<form  role="form" action="/deploy" method="post" style="float: left;margin-right: 5px;">
									<input type="hidden" name="org_id" value=""/>
									<input type="hidden" name="changeset_id" value="<?php //echo isset($page_data['changeset_id'])?$page_data['changeset_id']:'';?>"/>
									<input type="submit" value="Deploy" role="button" class="btn btn-primary" />
								</form>
								<!--<a href="#" role="button" class="btn btn-primary" >Delete</a>
								<a href="#" role="button" class="btn btn-primary" >Deploy</a>
							</div>	-->						
							</div>
						</div>
						
						
						
						<div class="row ">
						<?php //print_r($page_data['changeset_info']->org_id); ?>
								<div>
									<div class="col-xs-12 col-sm-12 col-md-5 col-lg-5 pull-left" style="margin-bottom: 20px !important;"><p>Source System </p>
										<input type="hidden" class="list_type" value="source" />
										<select class="form-control selectSys" id="orgID1" class="OrgId" name="orgID1" >
											<?php										
												foreach($page_data['orglist'] as $key=>$val)
												{
													if(isset($page_data['changeset_id'])){
														if($page_data['changeset_info']->org_id == $val->id)
															echo '<option value="'.$val->id.'">'.$val->userid.'</option>';
														continue;
													}
													$select = ($page_data['org1']->id == $val->id)?"selected":'';
													echo '<option value="'.$val->id.'"'.$select.'>'.$val->userid.'</option>';
												}
											?>
										</select><br/>
										<div class="mix source" style="padding: 10px;"><!-- item -->
											<div class="">
												<div>
													<div class="list-group panel">
														<?php  
														// echo '<pre>';
														// print_r($page_data['changeset_info']);
														// print_r($page_data['AssetType']);
														// echo '</pre>';
														echo '<input type="hidden" class="OrgId" name="OrgId" value="'.$page_data['changeset_info']->org_id.'"/>';	
															foreach($page_data['AssetType'] as $atkey=>$atval)
															{
														echo '<div class="borderBottom">
															<input type="hidden" name="Asset_Type_Id" value="'.$atval->Asset_Type_Id.'"/>
																<a href="#source_asset_type'.$atval->Asset_Type_Id.'" class="list-group-item asset_type" data-toggle="collapse" data-parent="#MainMenu" aria-expanded="false" >
																'.$atval->Asset_Type_Name.'<span class="fa fa-plus-square-o pull-right"></span></a>
															<div class="collapse asset_list" id="source_asset_type'.$atval->Asset_Type_Id.'">
																
															</div>
															</div>';
															}
														?>											
													</div>
													
												</div>	
											</div>
										</div>
										<button type="button" class="btn btn-sm btn-default search_show searchCompButton" >Search</button>
									</div>
									
									<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 pull-center" style="text-align: center;">
										<span style="margin-top: 55px; color:orange;" class="glyphicon glyphicon-circle-arrow-right fa-2x "></span>
									</div>									
									<div style="" class="col-xs-12 col-sm-12 col-md-5 col-lg-5 pull-right" > <p>Target System</p>
										<input type="hidden" class="list_type" value="destination" />
										<select class="form-control selectSys2" id="orgID2" class="OrgId" name="orgID2">
											<option value="NULL"></option>
											<?php
												foreach($page_data['orglist'] as $key=>$val)
												{
													if($page_data['changeset_info']->org_id == $val->id){
													continue;
													}
													
													echo '<option value="'.$val->id.'">'.$val->userid.'</option>';
												}
											?>
										</select><br/>
										<div class="mix destination" style="display: none;">
											<div class="col-sm-12 col-md-12">
												<div class="list-group panel">
													<?php  
													// echo '<pre>';
													// print_r($page_data['changeset_info']);
													// print_r($page_data['AssetType']);
													// echo '</pre>';
													echo '<input type="hidden" class="OrgId" name="OrgId" value="'.$page_data['changeset_info']->org_id .'"/>';	
														foreach($page_data['AssetType'] as $atkey=>$atval)
														{
													echo '<div class="borderBottom">
														<input type="hidden" name="Asset_Type_Id" value="'.$atval->Asset_Type_Id.'"/>
															<a href="#asset_type_'.$page_data['changeset_info']->org_id .'_'.$atval->Asset_Type_Id.'" class="list-group-item asset_type" data-toggle="collapse" data-parent="#MainMenu" aria-expanded="false">
															'.$atval->Asset_Type_Name.'<span class="fa fa-plus-square-o pull-right"></span></a>
														<div class="collapse asset_list" id="asset_type_'.$page_data['changeset_info']->org_id .'_'.$atval->Asset_Type_Id.'">
															
														</div>
														</div>';
														}
													?>										
												</div>
												
											</div>
										</div>
										<button type="button" class="btn btn-sm btn-default search_show searchButton searchCompButton" style="display: none;">Search</button>
									</div>
									
								</div>
									
									
						</div>
						
						<?php
							if( isset($page_data['component_list']))
							{
						?>		
						<div class="row">
							<div class="col-lg-12">
								<div class="panel panel-default">
									<div class="panel-heading">
										<h3 class="panel-title"><i class="fa fa-list-alt fa-fw"></i>Assets Included in Deployment Package</h3>
									</div>
									<div class="panel-body">
										<div class="table-responsive">
											  <?php 
											  //print_r($page_data);
											  echo '<table class="table table-bordered table-hover tablesorter">
																<thead>
																<tr>
																	<th>ID #</th>
																	<th>Package	Name</th>
																	<th>Meta Type</th>
																	<th>Object Id</th>
																	<th>Remove</th>
																</tr>
															</thead>
															<tbody> ';
													foreach($page_data['component_list'] as $key =>$val)
													{		
														echo '<tr ondblclick="" class="" style="vertical-align: top;"> 														
																<td>'.$val->id.'</td>
																<td>'.$val->list_name.'</td>
																<td>'.$val->meta_type.'</td>
																<td>'.$val->object_id.'</td>
																<td>';
															echo '<form id="form'.$key.'" role="form" action="'.base_url().'/changesetinfo/deleteComponent" method="post" onSubmit="if(!confirm(\'Are you sure you want to delete this from Packages?\')){return false;}">';		
															echo '<input type="hidden" name="component_id" value="'.$val->id.'"/>
																	<input type="hidden" name="changeset_id" value="'.$page_data['changeset_id'].'"/>
																	';
															echo '<button role="button" ondblclick="document.getElementById(\'form'.$key.'\').submit();" type="submit" class="btn btn-danger btn-xs" data-title="Delete" data-toggle="modal" data-target="#delete" ><span class="glyphicon glyphicon-trash"></span></button>';
															echo '</form>';
														echo '</td>';
														echo '</tr>';
														
													}													
													echo '</tbody>
														</table>';  
													 
											  ?>							
										</div>								
									</div>
								</div>
							</div>
							
						</div>
						<?php } ?>	
						
						
					</div>				
				</div>				
			</div>
			</div>
		
		
		

                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->
		<!-- Small Modal >-->


				<div class="search_popup" style="position: fixed; top: 0; width: 100%; height: 100%; background-color: rgba(0, 0, 0, 0.5); display: none;" >
					<div class="modal-dialog modal-sm" style=" top: 20%;  width: 40%;">
						<div class="modal-content">
							<!-- header modal -->
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<h4 class="modal-title" id="mySmallModalLabel">Search for Assets in <span id="systemName"> <?php										
									foreach($page_data['orglist'] as $key=>$val)
									{
										if(isset($page_data['changeset_id'])){
											if($page_data['changeset_info']->org_id == $val->id)
												echo '<option value="'.$val->id.'">'.$val->OrgName.'</option>';
											continue;
										}
										$select = ($page_data['org1']->id == $val->id)?"selected":'';
										echo '<option value="'.$val->id.'"'.$select.'>'.$val->OrgName.'</option>';
									}											
								?></span>
								
								
								</h4>
							</div>
							<!-- body modal -->
							<div class="modal-body">
								<div class="form-inline" >
									<input type="hidden" class="OrgId" name="OrgId" value=""/>
									<input type="hidden" class="list_type" name="list_type" value=""/>
									<div class="form-group ">								
										<label ><b>Asset Type:</b></label>
										<select class="form-control inlineFormInput" name="Asset_Type_Id">
										<option value=""></option>
										<?php 
											foreach($page_data['AssetType'] as $atkey=>$atval)
											{
												echo '<option value="'.$atval->Asset_Type_Id.'">'.$atval->Asset_Type_Name.'</option>';										
											}
										?>
										</select>
									</div>
									<div class="form-group">
										<label  ><b>Asset Name:</b></label>
										<input type="name" class="form-control inlineFormInput" name="AssetName">									
									</div>
									<div class="form-group">
										<label  ><b></b></label>
										<a class="btn btn-default searchAsset inlineFormButton"> Search</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
</div>
   <div id="diffinfo" class="panel-body col-lg-10 col-xs-12 col-sm-9">
	</div>
<script>
	function show_search_popup(orgid,list_type){
		$('.search_popup').find('.OrgId').val(orgid);
		$('.search_popup').find('.list_type').val(list_type);
		$('.search_popup').show();
	}
	$(document).ready(function(){
		
		$('.selectSys').change(function(){
			var name = $('.selectSys :selected').text();
			$('#systemName').html(name);
		});
		$('.selectSys2').change(function(){
			var name = $('.selectSys2 :selected').text();
			$('#systemName').html(name);
		});
		
		$('.search_popup .close').click(function(){
			$(this).parent().parent().parent().parent().hide();
		});
		$('.search_show').click(function(){
			var orgid = $(this).parent().find('.OrgId').val();
			var list_type = $(this).parent().find('.list_type').val();
			console.log(orgid);
			console.log(list_type);
			show_search_popup(orgid,list_type)
		});

	});
</script>
<style>
.checkbox label:after, 
.radio label:after {
    content: '';
    display: table;
    clear: both;
}

.checkbox .cr,
.radio .cr {
    position: relative;
    display: inline-block;
    border: 1px solid #a9a9a9;
    border-radius: .25em;
    width: 1.3em;
    height: 1.3em;
    float: left;
    margin-right: .5em;
}

.radio .cr {
    border-radius: 50%;
}

.checkbox .cr .cr-icon,
.radio .cr .cr-icon {
    position: absolute;
    font-size: .8em;
    line-height: 0;
    top: 50%;
    left: 20%;
}

.radio .cr .cr-icon {
    margin-left: 0.04em;
}

.checkbox label input[type="checkbox"],
.radio label input[type="radio"] {
    display: none;
}

.checkbox label input[type="checkbox"] + .cr > .cr-icon,
.radio label input[type="radio"] + .cr > .cr-icon {
    transform: scale(3) rotateZ(-20deg);
    opacity: 0;
    transition: all .3s ease-in;
}

.checkbox label input[type="checkbox"]:checked + .cr > .cr-icon,
.radio label input[type="radio"]:checked + .cr > .cr-icon {
    transform: scale(1) rotateZ(0deg);
    opacity: 1;
}

.checkbox label input[type="checkbox"]:disabled + .cr,
.radio label input[type="radio"]:disabled + .cr {
    opacity: .5;
}
.checkbox, .radio {
    margin-top: 0px;
    margin-bottom: 0px;
}
.checkbox label, .radio label {
    min-height: 20px;
    padding-left: 5px;
}
.loader{
    float: right;
    text-align: center;
    vertical-align: middle;
}
.glyphicon-refresh-animate {
    -animation: spin .7s infinite linear;
    -webkit-animation: spin2 .7s infinite linear;
}

@-webkit-keyframes spin2 {
    from { -webkit-transform: rotate(0deg);}
    to { -webkit-transform: rotate(360deg);}
}

@keyframes spin {
    from { transform: scale(1) rotate(0deg);}
    to { transform: scale(1) rotate(360deg);}
}
</style>

<!-- JAVASCRIPT FILES -->
		<script type="text/javascript">var plugin_path = 'assets/plugins/';</script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/jquery/jquery-2.1.4.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/app.js"></script>
		<script type="text/javascript">
			loadScript(plugin_path + "nestable/jquery.nestable.js", function(){
				if(jQuery().nestable) {

					var updateOutput = function (e) {
						var list = e.length ? e : $(e.target),
							output = list.data('output');
						if (window.JSON) {
							output.val(window.JSON.stringify(list.nestable('serialize'))); //, null, 2));
						} else {
							output.val('JSON browser support required for this demo.');
						}
					};


					// Nestable list 1
					jQuery('#nestable_list_1').nestable({
						group: 1
					}).on('change', updateOutput);
					// Nestable list 1
					jQuery('#nestable_list_2').nestable({
						group: 1
					}).on('change', updateOutput);


					// output initial serialised data
					updateOutput(jQuery('#nestable_list_1').data('output', jQuery('#nestable_list_1_output')));
					updateOutput(jQuery('#nestable_list_2').data('output', jQuery('#nestable_list_2_output')));
					
					// Expand All
					jQuery("button[data-action=expand-all]").bind("click", function() {
						jQuery('.dd').nestable('expandAll');
					});

					// Collapse All
					jQuery("button[data-action=collapse-all]").bind("click", function() {
						jQuery('.dd').nestable('collapseAll');
					});

				}

			});

$(document).ready(function() { 
$('#orgID2').change(function(){
	$('.destination').find('.OrgId').val($(this).val());
	$('.destination').find('.asset_list').html('');
	
	if( $(this).val() == "NULL"){
		$('.destination').hide();
		$('.searchButton').hide();
	}
	else{
		$('.destination').show();
		$('.searchButton').show();
	}
	
});
    function getAssetList(Asset,assetlist,tempdata)
	{
		$.ajax(
			{
				type: 'post',
				url: '<?php echo base_url();?>/changesetinfo/getAssetData',
				data: tempdata,
				beforeSend: function()
				{
					$(Asset).append('<div class="loader">'+
						'<span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span>'+
					'</div></a>');
				},
				success: function(data)
				{   
					$(Asset).find('.loader').hide();
					//$('.search_popup').hide();
					var list = JSON.parse( data );
					$(list.elements).each(function(){
					var out = printObject(this);
					$(assetlist).append( '<div class="list-group-item "><input type="checkbox" value="'+this.id+'"/><a href="#id_'+tempdata.OrgId+'_'+ this.id +'" class="asset_item" data-toggle="collapse" >'+this.name +'</a></div>' );
					// $(assetlist).append( '<div id="id_'+tempdata.OrgId+'_'+ this.id +'" class="list-group-item asset_struct collapse">'+out+'</div>' );
					});	
					
					if(list.page * list.Page_Size < list.total ){
						var pageno = list.page +1;
						$(assetlist).append( '<div class="load_more_asset"><input class="page" type="hidden" value="'+pageno+'" /> <a class="btn btn-xs btn-info">Load More ('+list.page * list.Page_Size +'/'+list.total+') </a></div>' );
					}					
					$('.load_more_asset').click(function(){
					var tempdata = {
								"Asset_Type_Id":$(this).parent().parent().find('input[name="Asset_Type_Id"]').val(),
								"OrgId": $(this).parent().parent().parent().find('.OrgId').val(),
								"page": $(this).find('.page').val(),
							};
					var assetlist = $(this).parent().parent().find('.asset_list');
						
						getAssetList(this,assetlist,tempdata);
						$(this).remove();
					});
					
					
				},
				error: function()
				{
					result = false;
				}
			});
	}
	
	$('.asset_type').click(function(){
		if($(this).attr('aria-expanded') == "false"){
			//console.log($(this).attr('aria-expanded'));
			$(this).find(".fa-plus-square-o").addClass("fa-minus-square-o").removeClass("fa-plus-square-o");
		}else{
			$(this).find(".fa-minus-square-o").addClass("fa-plus-square-o").removeClass("fa-minus-square-o");
		}
		var tempdata = {
					"Asset_Type_Id":$(this).parent().find('input[name="Asset_Type_Id"]').val(),
					"OrgId": $(this).parent().parent().find('.OrgId').val(),
				};
		var assetlist = $(this).parent().find('.asset_list');
		if($.trim($(this).parent().find('.asset_list').html()) =='')
			getAssetList(this,assetlist,tempdata);
		
	});
	
	function getSearchAsset(Asset,assetlist,tempdata)
	{
		$.ajax(
			{
				type: 'post',
				url: '<?php echo base_url();?>/changesetinfo/searchAsset',
				data: tempdata,
				beforeSend: function()
				{
					$(Asset).append('<div class="loader">'+
						'<span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span>'+
					'</div></a>');
				},
				success: function(data)
				{   
					$(Asset).find('.loader').hide();
					$('.search_popup').hide();
					var list = JSON.parse( data );
					$(list.elements).each(function(){
						var out = printObject(this);
				$(assetlist).append( '<div class="list-group-item "><input type="checkbox" value="'+this.id+'"/><a href="#id_'+tempdata.OrgId+'_'+ this.id +'" class="asset_item" data-toggle="collapse" >'+this.name +'</a></div>' );
				// $(assetlist).append( '<div id="id_'+tempdata.OrgId+'_'+ this.id +'" class="list-group-item asset_struct collapse">'+out+'</div>' );
					});	
					
					if(list.page * list.Page_Size < list.total ){
						var pageno = list.page +1;
						$(assetlist).append( '<div class="load_more_asset"><input class="page" type="hidden" value="'+pageno+'" /> <a class="btn btn-xs btn-info">Load More ('+list.page * list.Page_Size +'/'+list.total+') </a></div>' );
					}					
					$('.load_more_asset').click(function(){
					var tempdata = {
								"Asset_Type_Id":$(this).parent().parent().find('input[name="Asset_Type_Id"]').val(),
								"OrgId": $(this).parent().parent().parent().find('.OrgId').val(),
								"page": $(this).find('.page').val(),
							};
					var assetlist = $(this).parent().parent().find('.asset_list');
						
						getAssetList(this,assetlist,tempdata);
						$(this).remove();
					});
					$(assetlist).parent().find('.asset_type').trigger( "click" );			
					
				},
				error: function()
				{
					result = false;
				}
			});
	}
	
	$('.searchAsset').click(function(){
		var tempdata = {
					"Asset_Type_Id":$(this).parent().parent().find('select[name="Asset_Type_Id"]').val(),
					"OrgId": $(this).parent().parent().find('.OrgId').val(),
					"AssetName": $(this).parent().parent().find('input[name="AssetName"]').val(),
					"page": 1,
				};
		var AssetId=$(this).parent().parent().find('select[name="Asset_Type_Id"]').val();
		var OrgId=$(this).parent().parent().find('.OrgId').val();		
		
		if($(this).parent().parent().find('.list_type').val() == 'source' ){
			$('#source_asset_type'+AssetId).parent().find('.asset_list').html('');		
			var assetlist = $('#source_asset_type'+AssetId).parent().find('.asset_list');
			//console.log($(this).parent().parent().find('.list_type').val());
		}
		else{
			$('#asset_type_2_'+AssetId).parent().find('.asset_list').html('');		
			var assetlist = $('#asset_type_2_'+AssetId).parent().find('.asset_list');
			//console.log($(this).parent().parent().find('.list_type').val());
			//console.log('#asset_type_2_'+AssetId);
		}
		if($.trim($(this).parent().find('.asset_list').html()) =='')
			getSearchAsset(this,assetlist,tempdata);
		
	});
	
	
	
	
	function printObject(o) {
	  var out = '';
	  for (var p in o) {
		  if(typeof o[p] === 'object')
		  {
			o[p] = printObject(o[p]);
			o[p] = '{ <br/> '+o[p] + '} <br/>';
		  }
		out += p + ': ' + o[p] + '<br/>';
	  }
	  return out;
	}
});
</script>