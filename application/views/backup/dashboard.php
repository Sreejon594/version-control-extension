<!DOCTYPE html>
<!--[if IE 8]>			<html class="ie ie8"> <![endif]-->
<!--[if IE 9]>			<html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->	<html> <!--<![endif]-->
	<head>
		<meta charset="utf-8" />
		<title>Smarty - Multipurpose + Admin</title>
		<meta name="keywords" content="HTML5,CSS3,Template" />
		<meta name="description" content="" />
		<meta name="Author" content="CRMSCI" />

		<!-- mobile settings -->
		<meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0" />
		<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->

		<!-- WEB FONTS : use %7C instead of | (pipe) -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400%7CRaleway:300,400,500,600,700%7CLato:300,400,400italic,600,700" rel="stylesheet" type="text/css" />

		<!-- CORE CSS -->
		<link href="<?php echo base_url();?>assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
		
		<!-- THEME CSS -->
		<link href="<?php echo base_url();?>assets/css/essentials.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url();?>assets/css/layout.css" rel="stylesheet" type="text/css" />

		<!-- PAGE LEVEL SCRIPTS -->
		<link href="<?php echo base_url();?>assets/css/header-1.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url();?>assets/css/color_scheme/green.css" rel="stylesheet" type="text/css" id="color_scheme" />
	</head>
	<style>
		#topNav .navbar-collapse {
			float: left !important;
		}
	</style>

	<body class="smoothscroll enable-animation">

		<!-- wrapper -->
		<div id="wrapper">

			<div id="header" class="sticky clearfix">

				<!-- TOP NAV -->
				<header id="topNav">
					<div class="container">

						<!-- Mobile Menu Button -->
						<button class="btn btn-mobile" data-toggle="collapse" data-target=".nav-main-collapse">
							<i class="fa fa-bars"></i>
						</button>

						<!-- Logo -->
						<a class="logo pull-left" href="<?php echo base_url(); ?>dashboard">
							<img src="<?php echo base_url(); ?>assets/image/portQii.png" alt="" />
						</a>

						<div class="navbar-collapse nav-main-collapse collapse">
							<nav class="nav-main">
								<ul id="topMain" class="nav nav-pills nav-main">
									<li class="dropdown"><!-- HOME -->
										<a href="<?php echo base_url(); ?>dashboard">
											HOME
										</a>
									</li>
									<li class="dropdown "><!-- SHORTCODES -->
										<a href="<?php echo base_url(); ?>apps">
											APPS
										</a>
									</li>
										
										</ul>
									</li>
								</ul>

							</nav>
						</div>

					</div>
				</header>
				<!-- /Top Nav -->

			</div>


			<section class="page-header page-header-xs dark">
				<div class="container">

					<h1>Dashboard</h1>

					<!-- breadcrumbs -->
					<ol class="breadcrumb">
						<li><a href="#">TOTAL APPS</a></i></li>
						<li><a href="#">ACTIVE APPS</a></i></li>
						<li><a href="#">TOTAL APP CONFIGS</a></i></li>
						<li><a href="#">ACTIVE APP CONFIGS</a></i></li>
					</ol><!-- /breadcrumbs -->

				</div>
			</section>
			<!-- /PAGE HEADER -->


<style>
	.section.dark.page-header {
		color: #00b0f0;
		background-color: #ababab !important;
	}
</style>

			<!-- -->
			<section>
				<div class="container">
					<div class=" ">
						<div class="">
							<h3 class="Lead">My App Configurations</h3>
						</div>
						
						<div class="">
							<p >Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur pellentesque neque eget diam posuere porta. Quisque ut nulla at nunc vehicula lacinia. Proin adipiscing porta tellus, ut feugiat nibh adipiscing sit amet.</p>
							
							<div class="divider divider-center divider-color"><!-- divider -->
								<i class="fa fa-chevron-down"></i>
							</div>
							<div class="table table-responsive">
								<div class="">
									<table class="table table-striped table-bordered table-hover" id="datatable_sample2">	
										<thead>
											<tr>
												<td>Actions</td>
												<td>App Name</td>
												<td>Config Name</td>
												<td>Owner</td>
												<td>Description</td>
											</tr>
										</thead>
										
										<tr>
											<td>1</td>
											<td>2</td>
											<td>3</td>
											<td>4</td>
											<td>5</td>
										</tr>
										
									</table>
								</div>								
							</div>
							

						</div>
					<!-- /FEATURED BOXES 3 -->
					</div>

				</div>
			</section>
			<!-- / -->



			<!-- -->
			<section>
				<div class="container">

					<div class="row">
						<div class="col-md-6">
							<h3 class="size-20">Why chose us?</h3>
							<ul class="list-unstyled">
								<li><i class="fa fa-check"></i> Fully responsive so your content will always look good on any screen size</li>
								<li><i class="fa fa-check"></i> Awesome sliders give you the opportunity to showcase important content</li>
								<li><i class="fa fa-check"></i> Unlimited color options with a backed color picker, including the gradients</li>
								<li><i class="fa fa-check"></i> Multiple layout options for home pages, portfolio section &amp; blog section</li>
								<li><i class="fa fa-check"></i> We offer free support because we care about your site as much as you do.</li>
							</ul>
						</div>

						<div class="col-md-6">
							<h3 class="size-20 text-center">What Smarty clients say?</h3>
							<!-- 
								Note: remove class="rounded" from the img for squared image!
							-->
							<ul class="row clearfix testimonial-dotted list-unstyled">
								<li class="col-md-6">
									<div class="testimonial">
										
										<div class="testimonial-content">
											<p>Incidunt deleniti blanditiis quas aperiam recusandae consequatur ullam!</p>
											<cite>
												Joana Doe
												<span>Company Ltd.</span>
											</cite>
										</div>
									</div>
								</li>
								<li class="col-md-6">
									<div class="testimonial">
										
										<div class="testimonial-content">
											<p>Incidunt deleniti blanditiis quas aperiam recusandae consequatur ullam!</p>
											<cite>
												Melissa Doe
												<span>Company Ltd.</span>
											</cite>
										</div>
									</div>
								</li>
							</ul>
						</div>
					</div>

				</div>
			</section>
			<!-- / -->




			<!-- FOOTER -->
			<footer id="footer">

				<div class="copyright">
					<div class="container">
						<ul class="pull-right nomargin list-inline mobile-block">
							<li><a href="#">Terms &amp; Conditions</a></li>
							<li>&bull;</li>
							<li><a href="#">Privacy</a></li>
						</ul>
						&copy; All Rights Reserved, Company LTD
					</div>
				</div>
			</footer>
			<!-- /FOOTER -->

		</div>
		<!-- /wrapper -->


		<!-- SCROLL TO TOP
		<a href="#" id="toTop"></a> -->


		<!-- PRELOADER -->
		<div id="preloader">
			<div class="inner">
				<span class="loader"></span>
			</div>
		</div><!-- /PRELOADER -->
		<script>
	$(document).ready(function(){
	if (jQuery().dataTable) {

				function initTable6(tabname) {
					var table = jQuery(tabname);

					table.dataTable({
						"columns": [{
							"orderable": false
						}, {
							"orderable": true
						}, {
							"orderable": false
						}, {
							"orderable": false
						}, {
							"orderable": true
						}
						],
						"lengthMenu": [
							[5, 15, 20, -1],
							[5, 15, 20, "All"] // change per page values here
						],
						// set the initial value
						"pageLength": 5,            
						"pagingType": "bootstrap_full_number",
						"language": {
							"lengthMenu": "  _MENU_ records",
							"paginate": {
								"previous":"Prev",
								"next": "Next",
								"last": "Last",
								"first": "First"
							}
						},
						"columnDefs": [{  // set default column settings
							'orderable': false,
							'targets': [0]
						}, {
							"searchable": false,
							"targets": [0]
						}],
						"order": [
							[1, "asc"]
						] // set first column as a default sort by asc
					});

					var tableWrapper = jQuery('#datatable_sample_wrapper');

					table.find('.group-checkable').change(function () {
						var set = jQuery(this).attr("data-set");
						var checked = jQuery(this).is(":checked");
						jQuery(set).each(function () {
							if (checked) {
								jQuery(this).attr("checked", true);
								jQuery(this).parents('tr').addClass("active");
							} else {
								jQuery(this).attr("checked", false);
								jQuery(this).parents('tr').removeClass("active");
							}
						});
						jQuery.uniform.update(set);
					});

					table.on('change', 'tbody tr .checkboxes', function () {
						jQuery(this).parents('tr').toggleClass("active");
					});

					tableWrapper.find('.dataTables_length select').addClass("form-control input-xsmall input-inline"); // modify table per page dropdown

				}
				
				initTable6('#datatable_sample2');
				initTable6('#datatable_sample');

			} });
</script>				
	</body>
</html>