<!DOCTYPE html>
<!--[if IE 8]>			<html class="ie ie8"> <![endif]-->
<!--[if IE 9]>			<html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->	<html> <!--<![endif]-->
	<head>
		<meta charset="utf-8" />
		<title>Smarty - Multipurpose + Admin</title>
		<meta name="keywords" content="HTML5,CSS3,Template" />
		<meta name="description" content="" />
		<meta name="Author" content="Dorin Grigoras [www.stepofweb.com]" />

		<!-- mobile settings -->
		<meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0" />
		<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->

		<!-- WEB FONTS : use %7C instead of | (pipe) -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400%7CRaleway:300,400,500,600,700%7CLato:300,400,400italic,600,700" rel="stylesheet" type="text/css" />

		<!-- CORE CSS -->
		<link href="<?php echo base_url();?>assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
		
		<!-- THEME CSS -->
		<link href="<?php echo base_url(); ?>assets/css/essentials.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url(); ?>assets/css/layout.css" rel="stylesheet" type="text/css" />

		<!-- PAGE LEVEL SCRIPTS -->
		<link href="<?php echo base_url(); ?>assets/css/header-1.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url(); ?>assets/css/color_scheme/green.css" rel="stylesheet" type="text/css" id="color_scheme" />
	</head>
	<style>
		#topNav .navbar-collapse {
			float: left !important;
		}
	</style>
	
	<body class="smoothscroll enable-animation">

		<!-- wrapper -->
		<div id="wrapper">

			
			<div id="header" class="sticky clearfix">

				<!-- TOP NAV -->
				<header id="topNav">
					<div class="container">

						<!-- Mobile Menu Button -->
						<button class="btn btn-mobile" data-toggle="collapse" data-target=".nav-main-collapse">
							<i class="fa fa-bars"></i>
						</button>

						<!-- Logo -->
						<a class="logo pull-left" href="<?php echo base_url(); ?>dashboard">
							<img src="<?php echo base_url(); ?>assets/image/portQii.png" alt="" />
						</a>

						<!-- 
							Top Nav 
							
							AVAILABLE CLASSES:
							submenu-dark = dark sub menu
						-->
						<div class="navbar-collapse nav-main-collapse collapse">
							<nav class="nav-main">
								<ul id="topMain" class="nav nav-pills nav-main">
									<li class="dropdown"><!-- HOME -->
										<a href="<?php echo base_url(); ?>dashboard">
											HOME
										</a>
									</li>
									<li class="dropdown "><!-- SHORTCODES -->
										<a href="<?php echo base_url(); ?>apps">
											APPS
										</a>
									</li>
									
								</ul>

							</nav>
						</div>

					</div>
				</header>
				<!-- /Top Nav -->

			</div>


			<section class="page-header page-header-xs dark">
				<div class="container">

					<h1>Dashboard</h1>

					<!-- breadcrumbs -->
					<ol class="breadcrumb">
						<li><a href="#">TOTAL APPS</a></i></li>
						<li><a href="#">ACTIVE APPS</a></i></li>
						<li><a href="#">TOTAL APP CONFIGS</a></i></li>
						<li><a href="#">ACTIVE APP CONFIGS</a></i></li>
					</ol><!-- /breadcrumbs -->

				</div>
			</section>
			<!-- /PAGE HEADER -->


			<style>
				.section.dark.page-header {
					color: #00b0f0;
					background-color: #ababab !important;
				}
				h4{
					text-align: left !important;
					padding: 12px  !important;
				}
				p{
					text-align: left !important;
				}
				.gobutton{
					margin-right: 15px !important;
				}
			</style>

			<!-- -->
			<section>
				<div class="container">
					<div class="col-md-4 col-sm-6"><!-- item -->
	
								<span class="lead">ELOQUA TRANSPORTER</span> <br/><br/>
								<p>Lead Capture App No more hand-typing of business cards, renting event badge scanners or missing lead lists from sales team members. Capture leads using technology that is already at your finger tips.
								</p>
								<a class="btn btn-sm btn-success pull-right gobutton" href="/transporter" >Go</a>							
					</div><!-- /item -->
					
					<div class="col-md-4 col-sm-6"><!-- item -->

						<div>
							<figure>
								<span class="lead">ELOQUA ASSET GOVERNANCE</span><br/><br/>
								<p>Lead Capture App No more hand-typing of business cards, renting event badge scanners or missing lead lists from sales team members. Capture leads using technology that is already at your finger tips.
								</p>
								<a class="btn btn-sm btn-default pull-right gobutton" href=
								"/assetgovernance">Go</a>
							</figure>
						</div>

					</div><!-- /item -->
					

				</div>
			</section>
			<!-- / -->







		</div>	

			<!-- FOOTER -->
			<footer id="footer">

				<div class="copyright">
					<div class="container">
						<ul class="pull-right nomargin list-inline mobile-block">
							<li><a href="#">Terms &amp; Conditions</a></li>
							<li>&bull;</li>
							<li><a href="#">Privacy</a></li>
						</ul>
						&copy; All Rights Reserved, Company LTD
					</div>
				</div>
			</footer>
			<!-- /FOOTER -->

		
		<!-- /wrapper -->


		<!-- SCROLL TO TOP
		<a href="#" id="toTop"></a> -->


		<!-- PRELOADER -->
		<div id="preloader">
			<div class="inner">
				<span class="loader"></span>
			</div>
		</div><!-- /PRELOADER -->
		<script>
	$(document).ready(function(){
	if (jQuery().dataTable) {

				function initTable6(tabname) {
					var table = jQuery(tabname);

					table.dataTable({
						"columns": [{
							"orderable": false
						}, {
							"orderable": true
						}, {
							"orderable": false
						}, {
							"orderable": false
						}, {
							"orderable": true
						}
						],
						"lengthMenu": [
							[5, 15, 20, -1],
							[5, 15, 20, "All"] // change per page values here
						],
						// set the initial value
						"pageLength": 5,            
						"pagingType": "bootstrap_full_number",
						"language": {
							"lengthMenu": "  _MENU_ records",
							"paginate": {
								"previous":"Prev",
								"next": "Next",
								"last": "Last",
								"first": "First"
							}
						},
						"columnDefs": [{  // set default column settings
							'orderable': false,
							'targets': [0]
						}, {
							"searchable": false,
							"targets": [0]
						}],
						"order": [
							[1, "asc"]
						] // set first column as a default sort by asc
					});

					var tableWrapper = jQuery('#datatable_sample_wrapper');

					table.find('.group-checkable').change(function () {
						var set = jQuery(this).attr("data-set");
						var checked = jQuery(this).is(":checked");
						jQuery(set).each(function () {
							if (checked) {
								jQuery(this).attr("checked", true);
								jQuery(this).parents('tr').addClass("active");
							} else {
								jQuery(this).attr("checked", false);
								jQuery(this).parents('tr').removeClass("active");
							}
						});
						jQuery.uniform.update(set);
					});

					table.on('change', 'tbody tr .checkboxes', function () {
						jQuery(this).parents('tr').toggleClass("active");
					});

					tableWrapper.find('.dataTables_length select').addClass("form-control input-xsmall input-inline"); // modify table per page dropdown

				}
				
				initTable6('#datatable_sample2');
				initTable6('#datatable_sample');

			} });
</script>				
	</body>
</html>