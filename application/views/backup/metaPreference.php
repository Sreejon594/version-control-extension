<style>
.material-switch > input[type="checkbox"] {
    display: none;   
}

.material-switch > label {
    cursor: pointer;
    height: 0px;
    position: relative; 
    width: 40px;  
}

.material-switch > label::before {
    background: rgb(0, 0, 0);
    box-shadow: inset 0px 0px 10px rgba(0, 0, 0, 0.5);
    border-radius: 8px;
    content: '';
    height: 16px;
    margin-top: -8px;
    position:absolute;
    opacity: 0.3;
    transition: all 0.4s ease-in-out;
    width: 40px;
}
.material-switch > label::after {
    background: rgb(255, 255, 255);
    border-radius: 16px;
    box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.3);
    content: '';
    height: 24px;
    left: -4px;
    margin-top: -8px;
    position: absolute;
    top: -4px;
    transition: all 0.3s ease-in-out;
    width: 24px;
}
.material-switch > input[type="checkbox"]:checked + label::before {
    background: inherit;
    opacity: 0.5;
}
.material-switch > input[type="checkbox"]:checked + label::after {
    background: inherit;
    left: 20px;
}
.profileDiv{
	margin-top: 50px ;
}
.saveButton{
	padding: 9px;
	text-align: center;
}
</style>
<div id="page-wrapper">
	<div class="container-fluid">
		<!-- Page Heading -->
		<div class="row profileDiv">
			<div class="col-lg-10 col-lg-offset-1">
				
				<form role="form" action="<?php echo base_url();?>/settings/manageMeta" method="post">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h3 class="panel-title"> <span class="glyphicon glyphicon-th"></span> Manage Preference</h3>
					</div>
					<div class="panel-body">					

	
<div class="container">
    <div class="row">
		<div style="" class="col-xs-12 col-sm-12 col-md-12">
			<div class="form-group col-lg-12">										
				<?php echo validation_errors();?>
			</div>
		</div>
        <div class="col-xs-12 col-sm-6 col-md-4 ">
            <div class="panel panel-default">
                 <?php //print_r ($page_data['PreferenceList']); ?>
                 <?php $PreferenceList = $page_data['PreferenceList']; ?>
                <!-- List group -->
                <ul class="list-group">
					<?php 
						foreach($page_data['PreferenceList'] as $key =>$val)
						{ $checked = (isset($val->Active_) && $val->Active_==1 )?'checked':'';
						echo '<li class="list-group-item">
                        '.$val->Asset_Type_Name .'
                        <div class="material-switch pull-right">
                        <input value="'.$val->Asset_Type_Id.'" id="'.$val->Asset_Type_Id.'" name="PreferenceList[]" type="checkbox" '.$checked.'/>
                            <label for="'.$val->Asset_Type_Id.'" class="label-primary"></label>
                        </div>
                    </li>';
						 }
					?>
                </ul>
            </div>            
        </div>
    </div>
</div>
					</div>
					
					<div >
						<div >
							<div class="col-xs-4 col-sm-4 col-md-5"></div>
							<div class="saveButton"> 
								<button class="btn btn-success btn-lg" type="submit">Save</button>
							</div>
						</div>
					</div>
				</div>
				</form>
			</div>
		</div>
	</div>
</div>
		
		