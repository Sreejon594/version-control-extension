<!DOCTYPE html>
<html> <!--<![endif]-->
	<style>
		section{
			padding-bottom: 50px !important;
		}
		input{
			font-size: 13px !important;
		}
	</style>
	<?php 
	$data['page']= 'login';
	$this->load->view('common/public_header',$data); ?>
	<body class="smoothscroll enable-animation">
		<!-- -->
			<section>
				<div class="container">
					
					<div class="row">

						<div class="col-xs-12 col-sm-5 col-md-5 col-lg-4 col-md-push-7 col-lg-push-8 col-sm-push-7">

							<!-- ALERT -->
							<?php $result2 = validation_errors(); ?>
							<?php if ((isset($result) && $result != '') || $result2!='')
							{ ?>
							<div class="alert alert-danger alert-dismissable">
								<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
								<?php echo $result2; ?>
								<?php echo isset($result) ?$result: '' ; ?>
							</div>
							<?php }?>
							<!-- /ALERT -->

							<!-- login form -->
							<form method="post" action="" autocomplete="off" class="sky-form boxed">
								<header><i class="fa fa-users"></i> Sign In</header>
								
								<fieldset class="nomargin">	
								
									<label class="input">
										<i class="ico-append fa fa-envelope"></i>
										<input required="" type="email" name="username" placeholder="E-mail" value="<?php echo set_value('username');?>">
										<b class="tooltip tooltip-bottom-right">Needed to verify your account</b>
									</label>
								
									<label class="input">
										<i class="ico-append fa fa-lock"></i>
										<input required="" type="password" placeholder="Password" name="password">
										<b class="tooltip tooltip-bottom-right">Type your account password</b>
									</label>
									<label class="checkbox margin-top-20">
										<input type="checkbox" name="checkbox-inline">
										<i></i> Keep me logged in
									</label>

								</fieldset>

								<footer class="celarfix">
									<button type="submit" class="btn btn-primary noradius pull-right"><i class="fa fa-check"></i> OK, LOG IN</button>
									<div class="login-forgot-password pull-left">
										<a class="no-text-decoration size-13 margin-top-10 block bold" href="/login/resetPassword">Forgot Password?</a>
									</div>
								</footer>
							</form>							
						</div>


						<div class="col-xs-12 col-sm-7 col-md-7 col-lg-8 col-lg-pull-4 col-md-pull-5 col-sm-pull-5">

							<h2 class="size-20 text-center-xs">Why Smarty?</h2>

							<p>Lorem ipsum dolor sit amet. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa.Lorem ipsum dolor sit amet. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa.</p>

							<ul class="list-unstyled login-features">
								<li>
									<i class="glyphicon glyphicon-road"></i> <strong>Lorem ipsum</strong> dolor sit amet.
								</li>
								<li>
									<i class="glyphicon glyphicon-cog"></i> <strong>Sed ut perspiciatis</strong> unde omnis iste.
								</li>
								<li>
									<i class="glyphicon glyphicon-tint"></i> <strong>Et harum quidem</strong> rerum facilis est et expedita distinctio. 
								</li>
								<li>
									<i class="glyphicon glyphicon-screenshot"></i> <strong>Nam libero</strong> tempore, cum soluta nobis.
								</li>
								<li>
									<i class="glyphicon glyphicon-fire"></i> <strong>Est eligendi</strong> voluptatem accusantium.
								</li>
							</ul>

						</div>

					</div>


				</div>
			</section>
			<!-- / -->
		
		
	</body>
		<?php $this->load->view('common/public_footer'); ?>
		