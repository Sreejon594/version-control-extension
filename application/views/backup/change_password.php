<style>
	.profileDiv{
		margin-top: 50px ;
	}

	.saveButton{
		padding: 9px;
		text-align: center;
	}
</style>
<div id="page-wrapper">
	<div class="container-fluid">
		<!-- Page Heading -->
		<div class="row profileDiv">
			<div class="col-lg-8 col-lg-offset-2">
				
				<form role="form" action="<?php echo base_url();?>settings/password" method="post">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h3 class="panel-title"> <span class="glyphicon glyphicon-th"></span> Change password</h3>
					</div>
					<div class="panel-body">
						<div class="row">
							
							<div style="" class="col-xs-12 col-sm-12 col-md-12 login-box">
								<div class="form-group col-lg-12">										
									<?php echo validation_errors();?>
								</div>
							<div class="form-group">
								<div class="input-group">
									<div class="input-group-addon">
										<span class="glyphicon glyphicon-lock"></span>
									</div> 
									<input class="form-control" name="old_password" type="password" placeholder="Current Password">
								</div>
							</div>
							<div class="form-group">
								<div class="input-group">
									<div class="input-group-addon">
									<span class="glyphicon glyphicon-log-in"></span></div> 
									<input class="form-control" name="new_password" type="password" placeholder="New Password">
								</div>
							</div>
							<div class="form-group">
								<div class="input-group">
									<div class="input-group-addon">
									<span class="glyphicon glyphicon-log-in"></span></div> 
									<input class="form-control" name="confirm_new_password" type="password" placeholder="Confirm New Password">
								</div>
							</div>
						</div>
						</div>
					</div>
					
					
					<div >
						<div >
							<div class="col-xs-4 col-sm-4 col-md-5"></div>
							<div class="saveButton" > 
								<button class="btn btn-success btn-lg" type="submit">Save</button>
							</div>
						</div>
					</div>
				</div>
				</form>
			</div>
		</div>
	</div>
</div>
		
		