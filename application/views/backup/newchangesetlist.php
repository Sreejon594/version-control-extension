	<script>
		function getdiff(listNo,listname,difference,diffkey)
		{
			//$('#diffinfo').css("display","block");
			//$("#diffinfo").slideUp();
			 $("#diffinfo").slideDown();			 
			document.getElementById("diffinfo").innerHTML =
				'<div class="panel-heading">'+
					'<h3 class="panel-title"><i class="fa fa-list-alt fa-fw"></i> Compared info</h3>'+
					'<div id="diffinfo_close" class="close" onclick="hide_diffinfo();">X</div>'+
				'</div>';
			if(difflist[listNo][diffkey].difference == 'changed')
			{
				document.getElementById("diffinfo").innerHTML +=
				'<div class="info_body row">'+
				//'<div style="" class="col-xs-12 col-sm-12 col-md-12">'+difflist[listNo][diffkey].difference+'</div>'+
				'<div style="" class="col-xs-6 col-sm-12 col-md-6">'+
				'<h5 style="margin-top: 3px; margin-bottom: 3px;">Source :- '+Source+'</h5>'+
				'<pre>'+ JSON.stringify(difflist[listNo][diffkey].obj, undefined, 2) + '</pre>'+ 
				'</div>'+
				'<div style="" class="col-xs-6 col-sm-12 col-md-6">'+
				'<h5 style="margin-top: 3px; margin-bottom: 3px;">Destination :- '+Destination+'</h5>'+
				'<pre>'+ JSON.stringify(difflist[listNo][diffkey].obj2, undefined, 2) + '</pre>'+ 
				'</div> </div>';
			}
			else if(difflist[listNo][diffkey].difference == 'removed')
			{
				document.getElementById("diffinfo").innerHTML +=
				'<div class="info_body row">'+
				//'<div style="" class="col-xs-12 col-sm-12 col-md-12">'+difflist[listNo][diffkey].difference+'</div>'+
				'<div style="" class="col-xs-6 col-sm-6 col-md-6"></div>'+
				'<div style="" class="col-xs-6 col-sm-6 col-md-6">'+
				'<h5 style="margin-top: 3px; margin-bottom: 3px;">Destination :- '+Destination+'</h5>'+
				'<pre>'+ JSON.stringify(difflist[listNo][diffkey].obj, undefined, 2) + '</pre>'+ 
				'</div> </div>';
			}
			else
			{
				document.getElementById("diffinfo").innerHTML +=
				'<div class="info_body row">'+
				//'<div style="" class="col-xs-12 col-sm-12 col-md-12">'+difflist[listNo][diffkey].difference+'</div>'+
				'<div style="" class="col-xs-6 col-sm-6 col-md-6">'+
				'<h5 style="margin-top: 3px; margin-bottom: 3px;">Source :- '+Source+'</h5>'+
				'<pre>'+ JSON.stringify(difflist[listNo][diffkey].obj, undefined, 2) + '</pre>'+ 
				'</div> </div>';
			}
			$('.table-responsive').css("padding-bottom","250px");
		}
		function hide_diffinfo(){
				$("#diffinfo").slideUp();
				$('.table-responsive').css("padding-bottom","0px");
		}
	
	</script>
	<style>
	
	.removed {
		background-color: #fd7f7f;
	}
	.added {
		background-color: #8bff7f;
	}
	.changed {
		background-color: #fcff7f;
	}
	#diffinfo{
		display: none;
		border: solid #337ab7;	
		position: fixed; 
		bottom: 0px;
		top:50px;
		background-color: #333;
		color: #fff;
		height: auto; 	
		overflow: auto;
	
	}
	#diffinfo .panel-heading
	{
		padding-bottom: 25px;
		padding-top: 0px;
		border-bottom: 2px solid;
	}
	#diffinfo .info_body
	{
	
		overflow: auto;
		margin-top: 5px;
	}
	#diffinfo .panel-title
	{
		float: left;
	}
	#diffinfo .close
	{
		float: right;
		opacity: 1;
		color: #ffffff;

	}
	.profileDiv{
		margin-top: 50px ;
	}
	.chnageset_box{
		margin: 14px;
	}
	
	</style>	
	
        <!-- Navigation -->
		<div id="page-wrapper">
            <div class="container-fluid">                
                <!-- /.row -->
			<div class="row profileDiv">
			<div class="col-xs-12 col-sm-12 col-md-10 col-lg-10 col-md-offset-1 col-lg-offset-1">			
				<div class="">
					
					<div class="">
						<div class="row" style=" margin-bottom: 10px;">
							<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-md-offset-2 col-lg-offset-2 sky-form ">	
								<div class="chnageset_box" >
									<div class="lead"><b>CREATE NEW DEPLOYMENT PACKAGE</b></div><br>
									
									
									<!-- ALERT -->
									<?php $result2 = validation_errors(); ?>
									<?php if ((isset($page_errors) && $page_errors != '') || $result2!='')
									{ ?>
									<div class="alert alert-danger alert-dismissable">
										<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
										<?php echo $result2; ?>
										<?php echo isset($page_errors) ?$page_errors: '' ; ?>
									</div>
									<?php }?>
									<!-- /ALERT -->
							
							
									<form role="form" action="<?php echo base_url();?>/home/newlist" method="post" >
										<input class="form-control" type="text"  size="20" name="list_name" placeholder="Give Name to Your Package">
										<select class="form-control" id="orgID1" name="orgID">
											<option>Select Source Org</option>
											<?php
												foreach($page_data['orglist'] as $key=>$val)
												{
													$select = ($page_data['org1']->id == $val->id)?"selected":'';
													echo '<option value="'.$val->id.'"'.$select.'>'.$val->userid.'</option>';
												}
											?>
										</select>
										<br/>
										<input type="submit" name="Save" value="CREATE PACKAGE" class="login pull-right btn btn-sm btn-default"/>
								   </form>

								</div>					
							</div>
						</div>			
					</div>				
				</div>				
			</div>
			</div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->
   <div id="diffinfo" class="panel-body col-lg-10 col-xs-12 col-sm-9">
								   
								 </div>
<style>
@import url(http://fonts.googleapis.com/css?family=Roboto);

/****** LOGIN MODAL ******/
.loginmodal-container {
  padding: 30px;
  max-width: 450px;
  width: 100% !important;
  background-color: #F7F7F7;
  margin: 0 auto;
  border-radius: 2px;
  box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
  overflow: hidden;
  font-family: roboto;
}

.loginmodal-container h1 {
  text-align: center;
  font-size: 1.8em;
  font-family: roboto;
}

.loginmodal-container input[type=submit] {
  width: 100%;
  display: block;
  margin-bottom: 10px;
  position: relative;
}

.loginmodal-container input[type=text], input[type=password] {
  height: 44px;
  font-size: 16px;
  width: 100%;
  margin-bottom: 10px;
  -webkit-appearance: none;
  background: #fff;
  border: 1px solid #d9d9d9;
  border-top: 1px solid #c0c0c0;
  /* border-radius: 2px; */
  padding: 0 8px;
  box-sizing: border-box;
  -moz-box-sizing: border-box;
}

.loginmodal-container input[type=text]:hover, input[type=password]:hover {
  border: 1px solid #b9b9b9;
  border-top: 1px solid #a0a0a0;
  -moz-box-shadow: inset 0 1px 2px rgba(0,0,0,0.1);
  -webkit-box-shadow: inset 0 1px 2px rgba(0,0,0,0.1);
  box-shadow: inset 0 1px 2px rgba(0,0,0,0.1);
}

.loginmodal {
  text-align: center;
  font-size: 14px;
  font-family: 'Arial', sans-serif;
  font-weight: 700;
  height: 36px;
  padding: 0 8px;
/* border-radius: 3px; */
/* -webkit-user-select: none;
  user-select: none; */
}

.loginmodal-submit {
  /* border: 1px solid #3079ed; */
  border: 0px;
  color: #fff;
  text-shadow: 0 1px rgba(0,0,0,0.1); 
  background-color: #4d90fe;
  padding: 17px 0px;
  font-family: roboto;
  font-size: 14px;
  /* background-image: -webkit-gradient(linear, 0 0, 0 100%,   from(#4d90fe), to(#4787ed)); */
}

.loginmodal-submit:hover {
  /* border: 1px solid #2f5bb7; */
  border: 0px;
  text-shadow: 0 1px rgba(0,0,0,0.3);
  background-color: #357ae8;
  /* background-image: -webkit-gradient(linear, 0 0, 0 100%,   from(#4d90fe), to(#357ae8)); */
}

.loginmodal-container a {
  text-decoration: none;
  color: #666;
  font-weight: 400;
  text-align: center;
  display: inline-block;
  opacity: 0.6;
  transition: opacity ease 0.5s;
} 

.login-help{
  font-size: 12px;
}
</style>