	<script>
		function getdiff(listNo,listname,difference,diffkey)
		{
			//$('#diffinfo').css("display","block");
			//$("#diffinfo").slideUp();
			 $("#diffinfo").slideDown();			 
			document.getElementById("diffinfo").innerHTML =
				'<div class="panel-heading">'+
					'<h3 class="panel-title"><i class="fa fa-list-alt fa-fw"></i> Compared info</h3>'+
					'<div id="diffinfo_close" class="close" onclick="hide_diffinfo();">X</div>'+
				'</div>';
			if(difflist[listNo][diffkey].difference == 'changed')
			{
				document.getElementById("diffinfo").innerHTML +=
				'<div class="info_body row">'+
				//'<div style="" class="col-xs-12 col-sm-12 col-md-12">'+difflist[listNo][diffkey].difference+'</div>'+
				'<div style="" class="col-xs-6 col-sm-12 col-md-6">'+
				'<h5 style="margin-top: 3px; margin-bottom: 3px;">Source :- '+Source+'</h5>'+
				'<pre>'+ JSON.stringify(difflist[listNo][diffkey].obj, undefined, 2) + '</pre>'+ 
				'</div>'+
				'<div style="" class="col-xs-6 col-sm-12 col-md-6">'+
				'<h5 style="margin-top: 3px; margin-bottom: 3px;">Destination :- '+Destination+'</h5>'+
				'<pre>'+ JSON.stringify(difflist[listNo][diffkey].obj2, undefined, 2) + '</pre>'+ 
				'</div> </div>';
			}
			else if(difflist[listNo][diffkey].difference == 'removed')
			{
				document.getElementById("diffinfo").innerHTML +=
				'<div class="info_body row">'+
				//'<div style="" class="col-xs-12 col-sm-12 col-md-12">'+difflist[listNo][diffkey].difference+'</div>'+
				'<div style="" class="col-xs-6 col-sm-6 col-md-6"></div>'+
				'<div style="" class="col-xs-6 col-sm-6 col-md-6">'+
				'<h5 style="margin-top: 3px; margin-bottom: 3px;">Destination :- '+Destination+'</h5>'+
				'<pre>'+ JSON.stringify(difflist[listNo][diffkey].obj, undefined, 2) + '</pre>'+ 
				'</div> </div>';
			}
			else
			{
				document.getElementById("diffinfo").innerHTML +=
				'<div class="info_body row">'+
				//'<div style="" class="col-xs-12 col-sm-12 col-md-12">'+difflist[listNo][diffkey].difference+'</div>'+
				'<div style="" class="col-xs-6 col-sm-6 col-md-6">'+
				'<h5 style="margin-top: 3px; margin-bottom: 3px;">Source :- '+Source+'</h5>'+
				'<pre>'+ JSON.stringify(difflist[listNo][diffkey].obj, undefined, 2) + '</pre>'+ 
				'</div> </div>';
			}
			$('.table-responsive').css("padding-bottom","250px");
		}
		function hide_diffinfo(){
				$("#diffinfo").slideUp();
				$('.table-responsive').css("padding-bottom","0px");
		}
	</script>
	<style>
	
	.removed {
		background-color: #fd7f7f;
	}
	.added {
		background-color: #8bff7f;
	}
	.changed {
		background-color: #fcff7f;
	}
	#diffinfo{
		display: none;
		border: solid #337ab7;	
		position: fixed; 
		bottom: 0px;
		top:50px;
		background-color: #333;
		color: #fff;

		height: auto; 
	/*	max-height: 300px;  */
		overflow: auto;
	/*  margin-left: -43px; */
	}
	#diffinfo .panel-heading
	{
		padding-bottom: 25px;
		padding-top: 0px;
		border-bottom: 2px solid;
	}
	#diffinfo .info_body
	{
		/* max-height: 228px; */
		overflow: auto;
		margin-top: 5px;
	}
	#diffinfo .panel-title
	{
		float: left;
	}
	#diffinfo .close
	{
		float: right;
		opacity: 1;
		color: #ffffff;

	}
	#page-wrapper{
		margin-top: 50px;
	}
	.metadata{
		padding: 7px;
	}
	.checkbox .cr .cr-icon, .radio .cr .cr-icon {
		font-size: 1em !important;
		padding-top: 8px !important;
		top: 1% !important;
		left: 1% !important;
	}
	</style>	
	
        <!-- Navigation -->
        

        <div id="page-wrapper">

            <div class="container-fluid">
                
                <!-- /.row -->
			<div class="row">
			<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 col-lg-offset-1 col-md-offset-1">			
				<div class="">
					<!--<div class="">
						<p  class="lead"> Select Org To Compare.</p>
					</div>			-->	
					<div class="">
						<div class="row">
							<div class="col-sm-12 col-xs-12 col-lg-offset-4 col-md-offset-4 ">
								<p class="lead" style="color:orange;"><b>COMPARE ORGANIZATIONS</b></p>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-12 login-box">
								<form role="form" action="/compare" method="post">
									<input type="hidden" name="changeset_id" value="<?php echo isset($page_data['changeset_id'])?$page_data['changeset_id']:'';?>" />  
									<div class="form-group col-lg-12">										
										<?php 	
											if(!isset($page_data['changeset_id']))
											echo validation_errors();
												//echo '<pre>';
												//print_r($page_data['orglist']);
												//echo '</pre>';
										?>
									</div>	
									<div class="col-xs-12 col-sm-12 col-md-5 col-lg-5 lead"><h5>Source Organization </h5>
										<!--<div class="metadata" >Metadata Location: <input type="radio" > Eloqua Transporter</div>-->
										<select class="form-control" id="orgID1" name="orgID1">
											<option></option>
											<?php										
												foreach($page_data['orglist'] as $key=>$val)
												{
													if(isset($page_data['changeset_id'])){
														if($page_data['org1']->id == $val->id)
															echo '<option value="'.$val->id.'">'.$val->OrgName.'</option>';
														continue;
													}
													$select = ($page_data['org1']->id == $val->id)?"selected":'';
													echo '<option value="'.$val->id.'"'.$select.'>'.$val->OrgName.'</option>';
												}
											
											?>
										</select>
									</div>
									<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2" style="text-align: center;">
										<span style="margin-top: 55px; color:orange;" class="glyphicon glyphicon-circle-arrow-right fa-2x "></span>
									</div>									
									<div class="col-xs-12 col-sm-12 col-md-5 col-lg-5 lead"> <h5>Destination Organization</h5>
										<!--<div class="metadata">Metadata Location: <input type="radio" > Eloqua Transporter</div>-->
										<select class="form-control" id="orgID2" name="orgID2">
											<option></option>
											<?php
												foreach($page_data['orglist'] as $key=>$val)
												{
													$select = ($page_data['org2']->id == $val->id)?"selected":'';
													echo '<option value="'.$val->id.'"'.$select.'>'.$val->OrgName.'</option>';
												}
											?>
										</select>
									</div>
									<div class="text-center col-md-offset-4 col-xs-3 col-sm-3 col-md-3"> <br/>
										<button style="margin-left: 100px;" class="text-center btn btn-default btn-sm icon-btn-save margin-top-20 margin-bottom-20" type="submit"> 
											<span class=" btn-save-label"></span>COMPARE
										</button>
									</div>
								</form>								
							</div>
						</div>
					</div>				
				</div>				
			</div>
			</div>
		
		
		
<?php
//print_r($page_data);
if( isset($page_data['CompareList']))
{
	$org2Id = isset($page_data['org2']->id)?$page_data['org2']->id:'';
	//echo '<pre>';print_r($page_data); echo '</pre>';
?>		
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-list-alt fa-fw"></i> Compare Package</h3>
                            </div>
                            <div class="panel-body"  class="col-xs-4 col-sm-4 col-md-9 col-lg-9">
								<form role="form" action="<?php echo base_url();?>/compare/deploylist" method="post">
									<input type="hidden" name="org1" value="<?php echo $page_data['org1']->id;?>" />  
									<input type="hidden" name="org2" value="<?php echo $org2Id;?>" />  
									<input type="hidden" name="changeset_id" value="<?php echo isset($page_data['changeset_id'])?$page_data['changeset_id']:'';?>" />  
									
									<div style="" class="col-xs-4 col-sm-4 col-md-7 col-lg-5">
										<select class="form-control" id="changesetId" name="changesetId">
											<?php
												foreach($page_data['changeset_list'] as $key=>$val)
												{
													if(isset($page_data['changeset_id']) && $page_data['changeset_id'] != NULL){
														if($page_data['changeset_id'] == $val->id)
														echo '<option value="'.$val->id.'">'.$val->list_name.'</option>';
														continue;
													}
													echo '<option value="'.$val->id.'"'.$select.'>'.$val->list_name.'</option>';
												}
											?>
										</select>
									</div>
									<button class="btn icon-btn-save btn-default btn-md" type="submit" style="margin-bottom: 10px;"> 
										<span class="btn-save-label"></span>Add to Package
									</button>
								
									<div class="table-responsive" style="margin-left: 15px;" class="col-xs-4 col-sm-4 col-md-9 col-lg-9">
										<div class="accordion" id="accordion2">
											<?php 
											  $count=1;
											  $org2userid =isset($page_data['org2']->userid)?$page_data['org2']->userid:'';
											  echo '<script> var difflist = new Array(); 
															var Source = "'.$page_data['org1']->userid.'";
															var Destination = "'.$org2userid.'";
											  </script>';
											  foreach($page_data['CompareList'] as $key => $value)
											  {	
												$tempjson = '\''.base64_encode(json_encode($value['difflist'])).'\'';
												echo '<script>												
														difflist['.$count.'] = '.$tempjson.';
														difflist['.$count.'] = JSON.parse(atob(difflist['.$count.']));
												</script>';
												//var difflist = JSON.parse(\''.$tempjson.'\');
												
												echo '<div class="accordion-group">
												<div class="accordion-heading" style="padding: 10px; border: #012 solid 2px;">
												   <h3 class="panel-title"><a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#'.$key.'">
													'.$key.'
												  </a></h3>
												</div>
												<div id="'.$key.'" class="accordion-body collapse">
												  <div class="accordion-inner" >';											
													echo '<table class="table table-bordered table-hover tablesorter">
																<thead>
																<tr>
																	<th></th>
																	<th>ID #</th>
																	<th>Name</th>
																	<th>Difference Type</th>
																	
																</tr>
															</thead>
															<tbody>
													';
													//<th>Created Date</th>
													foreach($value['difflist'] as $diffkey =>$diffvall)
													{		
														echo '<tr ondblclick="getdiff('.$count.',\''.$key.'\',\''.$diffvall['difference'].'\','.$diffkey.');" class="'.$diffvall['difference'].'" style="vertical-align: top;"> 
																<td style="width:45px;">';
																if($diffvall['difference'] == 'changed' || $diffvall['difference']=='added')
																{
																echo '<div class="checkbox">
																		<label style="font-size: 1em">
																			<input type="checkbox" value="'.$diffvall['obj']->id.'" name="'.$key.'[]">
																			<span class="cr"><i class="cr-icon fa fa-check"></i></span>															
																		</label>
																	</div>';
																}
														echo '</td>
																<td>'.$diffvall['obj']->id.'</td>
																<td>'.$diffvall['obj']->name.'</td>
																<td>'.$diffvall['difference'].'</td>';
																//<td>'.$diffvall['obj']->createdAt.'</td>
															echo '</tr>';
													}													
													echo '</tbody>
														</table>';
													
														
											echo'</div>
												</div>
											  </div>';
											  $count++;
											  }	
											  ?>
										
										</div>	
									</div>	
								</form>
							</div>
                        </div>
                    </div>
					
                </div>
<?php
}
?>	
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->
		 <div id="diffinfo" class="panel-body col-lg-10 col-xs-12 col-sm-9">
								   
								 </div>
<style>
.checkbox label:after, 
.radio label:after {
    content: '';
    display: table;
    clear: both;
}

.checkbox .cr,
.radio .cr {
    position: relative;
    display: inline-block;
    border: 1px solid #a9a9a9;
    border-radius: .25em;
    width: 1.3em;
    height: 1.3em;
    float: left;
    margin-right: .5em;
}

.radio .cr {
    border-radius: 50%;
}

.checkbox .cr .cr-icon,
.radio .cr .cr-icon {
    position: absolute;
    font-size: .8em;
    line-height: 0;
    top: 50%;
    left: 20%;
}

.radio .cr .cr-icon {
    margin-left: 0.04em;
}

.checkbox label input[type="checkbox"],
.radio label input[type="radio"] {
    display: none;
}

.checkbox label input[type="checkbox"] + .cr > .cr-icon,
.radio label input[type="radio"] + .cr > .cr-icon {
    transform: scale(3) rotateZ(-20deg);
    opacity: 0;
    transition: all .3s ease-in;
}

.checkbox label input[type="checkbox"]:checked + .cr > .cr-icon,
.radio label input[type="radio"]:checked + .cr > .cr-icon {
    transform: scale(1) rotateZ(0deg);
    opacity: 1;
}

.checkbox label input[type="checkbox"]:disabled + .cr,
.radio label input[type="radio"]:disabled + .cr {
    opacity: .5;
}
.checkbox, .radio {
    margin-top: 0px;
    margin-bottom: 0px;
}
.checkbox label, .radio label {
    min-height: 20px;
    padding-left: 5px;
}
</style>
