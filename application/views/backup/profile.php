<style>
	.profileDiv{
		margin-top: 50px ;
	}
</style>
	<div class="container" style="background:#fff;">
      <div class="row profileDiv">
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-3 toppad" >   
			<div class="">
				<div class="panel-heading">
				  <h3 class="" ><?php echo $user_info[0]->first_name.' '.$user_info[0]->last_name  ?></h3>
				</div>
				<div class="panel-body">
					<div class="row">
						
						<div class=" col-md-12 col-lg-12 "> 
							<table class="table table-user-information">
								<tbody>
								                    
								 
								   <tr>
									<td>Company:</td>
									<td><?php echo $user_info[0]->Company_Name;?></td>
								  </tr> 
								 
								  <tr>
									<td>Email</td>
									<td><?php echo $user_info[0]->email;?></td>
								  </tr>
								   <tr>
									<td>Mobile Number</td>
									<td><?php echo $user_info[0]->mobile;?></td>                           
								  </tr>
								   <tr>
									<td>Admin Debug Mode</td>
									<td><?php echo ($user_info[0]->admin_access==0)?'OFF':'ON';?></td>                           
								  </tr>
								 
								</tbody>
							</table>
						</div>
					</div>
					<span class="pull-right">
						<a href="<?php echo base_url();?>settings/editprofile" data-original-title="Edit this user" data-toggle="tooltip" type="button" class="btn btn-sm btn-default"><i class="glyphicon glyphicon-edit"></i>Edit</a>
						
					</span>
				</div>            
			</div>
        </div>
      </div>
    </div>	