<!DOCTYPE html>
<html> <!--<![endif]-->
	<head>
		<meta charset="utf-8" />
		<title><?php echo $page_title;?></title>
		<meta name="keywords" content="HTML5,CSS3,Template" />
		<meta name="description" content="" />
		<meta name="Author" content="Dorin Grigoras [www.stepofweb.com]" />

		<!-- mobile settings -->
		<meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0" />
		<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->

		<!-- WEB FONTS : use %7C instead of | (pipe) -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400%7CRaleway:300,400,500,600,700%7CLato:300,400,400italic,600,700" rel="stylesheet" type="text/css" />

		<!-- CORE CSS -->
		<link href="<?php echo base_url();?>assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
		
		<!-- THEME CSS -->
		<link href="<?php echo base_url();?>assets/css/essentials.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url();?>assets/css/layout.css" rel="stylesheet" type="text/css" />

		<!-- PAGE LEVEL SCRIPTS -->
		<link href="<?php echo base_url();?>assets/css/header-1.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url();?>assets/css/color_scheme/blue.css" rel="stylesheet" type="text/css" id="color_scheme" />


	</head>

	<body class="smoothscroll enable-animation">
	<section style="border-bottom:none;">
				<div class="container">

					<div class="row">
						<div class="col-md-6 col-md-offset-3">

							<div class="toggle toggle-transparent toggle-accordion toggle-noicon">

								<div class="toggle active">
									<label class="size-20"><i class="fa fa-leaf"></i> &nbsp; Enter Your Email Id</label>
									<div class="toggle-content">


										<?php if(validation_errors()){ ?>
										<div class="alert alert-mini alert-danger margin-bottom-30">
											<strong>Oh snap!</strong> <?php echo validation_errors(); ?> 
										</div>
										<?php } ?>

										<form class="sky-form" method="post" action="" autocomplete="off">
											<div class="clearfix">

												<!-- Email -->
												<div class="form-group">
													<label>Email</label>
													<label class="input margin-bottom-10">
														<i class="ico-append fa fa-envelope"></i>
														<input required="" type="email" name="username">
													</label>
												</div>

											</div>

											<div class="row">

												<div class="col-md-6 col-sm-6 col-xs-6">
													
													<!-- Inform Tip -->                                        
													<div class="form-tip pt-20">
														<a class="no-text-decoration size-13 margin-top-10 block bold" href="/login">Go To Login</a>
													</div>
													
												</div>

												<div class="col-md-6 col-sm-6 col-xs-6 text-right">

													<button class="btn btn-primary"><i class="fa fa-check"></i> Send Reset Link</button>

												</div>

											</div>

										</form>


									</div>
								</div>
						</div>
					</div>
				</div>
	</div>
</section>
</body>		
<script type="text/javascript">var plugin_path = '/assets/plugins/';</script>
		<script type="text/javascript" src="/assets/plugins/jquery/jquery-2.1.4.min.js"></script>

		<script type="text/javascript" src="/assets/js/scripts.js"></script>

		<!-- PAGE LEVEL SCRIPTS -->
		<script type="text/javascript">

			/**
				Checkbox on "I agree" modal Clicked!
			**/
			jQuery("#terms-agree").click(function(){
				jQuery('#termsModal').modal('toggle');

				// Check Terms and Conditions checkbox if not already checked!
				if(!jQuery("#checked-agree").checked) {
					jQuery("input.checked-agree").prop('checked', true);
				}
				
			});
		</script>