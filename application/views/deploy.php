<style>
	.profileDiv{
		margin-top: 50px ;
	}
</style> 
 <!-- Navigation -->
        <div id="page-wrapper">
            <div class="container-fluid">                
                <!-- /.row -->
			<div class="row profileDiv">
			<div class="col-lg-12">			
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h3 class="panel-title"> <span class="glyphicon glyphicon-th"></span> Deployment Information.</h3>
					</div>
					<div class="panel-body">
						<div class="row " style=" margin-bottom: 10px;">
							<form  role="form" action="<?php echo base_url();?>/deploy/send_deploy" method="post" style="float: left;margin-right: 5px; width: 100%;">		
								<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-md-offset-2 col-lg-offset-2 ">								
									<h3>Select Destination Instance </h3>								
									<select class="form-control" id="orgID2" name="orgID2">
										<?php										
											foreach($page_data['orglist'] as $key=>$val)
											{
												if($page_data['org1'][0]->id == $val->id)
												{		continue;
												}		
												echo '<option value="'.$val->id.'">'.$val->OrgName.'</option>';
											}										
										?>
									</select>
									<br/>
									<input type="hidden" name="org_id" value="<?php echo $page_data['org1'][0]->id; ?>"/>
									<input type="hidden" name="changeset_id" value="<?php echo isset($page_data['changeset_id'])?$page_data['changeset_id']:'';?>"/>
									<input type="submit" value="Deploy" role="button" class="pull-right btn btn-primary btn-lg" />								
								</div>
							</form>
						</div>
						
						<?php
						
if( isset($page_data['meta']))
{
?>		
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-list-alt fa-fw"></i> Component Package</h3>
                            </div>
                            <div class="panel-body">
							    <div class="table-responsive">
									  <?php 
									  //print_r($page_data);
									  echo '<table class="table table-bordered table-hover tablesorter">
														<thead>
														<tr>
															<th>ID #</th>
															<th>Package Name</th>
															<th>Meta Type</th>
															<th>Object Id</th>															
														</tr>
													</thead>
													<tbody> ';
													// echo '<pre>';
													// print_r($page_data);
													// echo '</pre>';
											foreach($page_data['meta'] as $key =>$val2)
											{		
												// print_r($val2);
												if(is_array($val2) || is_object($val2)){
												echo '<tr ondblclick="" class="" style="vertical-align: top;"> 														
														<td>'.$val2->id.'</td>
														<td>'.$val2->name.'</td>
														<td>'.$val2->type.'</td>
														<td>'.$val2->internalName.'</td>';													
												echo '</tr>';
												}
											}													
											echo '</tbody>
												</table>';  
									  		 
									  ?>							
                                </div>								
                            </div>
                        </div>
                    </div>
					
                </div>
<?php
}

echo '<pre>';
						print_r($page_data['meta']);
						echo '</pre>';
?>	
						
						
					</div>				
				</div>				
			</div>
			</div>
		
		
		

                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->
   <div id="diffinfo" class="panel-body col-lg-10 col-xs-12 col-sm-9">
								   
								 </div>
<style>
.checkbox label:after, 
.radio label:after {
    content: '';
    display: table;
    clear: both;
}

.checkbox .cr,
.radio .cr {
    position: relative;
    display: inline-block;
    border: 1px solid #a9a9a9;
    border-radius: .25em;
    width: 1.3em;
    height: 1.3em;
    float: left;
    margin-right: .5em;
}

.radio .cr {
    border-radius: 50%;
}

.checkbox .cr .cr-icon,
.radio .cr .cr-icon {
    position: absolute;
    font-size: .8em;
    line-height: 0;
    top: 50%;
    left: 20%;
}

.radio .cr .cr-icon {
    margin-left: 0.04em;
}

.checkbox label input[type="checkbox"],
.radio label input[type="radio"] {
    display: none;
}

.checkbox label input[type="checkbox"] + .cr > .cr-icon,
.radio label input[type="radio"] + .cr > .cr-icon {
    transform: scale(3) rotateZ(-20deg);
    opacity: 0;
    transition: all .3s ease-in;
}

.checkbox label input[type="checkbox"]:checked + .cr > .cr-icon,
.radio label input[type="radio"]:checked + .cr > .cr-icon {
    transform: scale(1) rotateZ(0deg);
    opacity: 1;
}

.checkbox label input[type="checkbox"]:disabled + .cr,
.radio label input[type="radio"]:disabled + .cr {
    opacity: .5;
}
.checkbox, .radio {
    margin-top: 0px;
    margin-bottom: 0px;
}
.checkbox label, .radio label {
    min-height: 20px;
    padding-left: 5px;
}
</style>
