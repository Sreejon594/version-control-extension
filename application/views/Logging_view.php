<?php
	// print_r($line);
	$lines = file($_SERVER['DOCUMENT_ROOT']."/application/assets/log/Log_file.txt", FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
    $data = array_map(function($v){
        list($dateTime, $functionMethos , $addittionalInfo) = explode("|", $v);
        return ["dateTime" => $dateTime, "functionMethos" => $functionMethos , "addittionalInfo" => $addittionalInfo];
    }, $lines);

    
?>

<html>
	<head>
		<title> Error Log </title>
	</head>
	
	<style>

	</style>
	
	<body>
		<table width="1000" border="1">
			<tr>
				<td width="">Date & Time</td>
				<td width="100">Function/Methos</td>
				<td width="100">Additional Info</td>
			</tr>
			<?php foreach($data as $user){ ?>
				<tr>
					<td><?php echo $user["dateTime"]; ?></td>
					<td><?php echo $user["functionMethos"]; ?></td>
					<td><?php echo $user["addittionalInfo"]; ?></td>
				</tr>
			<?php } ?>
		</table>	
	</body>

</html>
