<style>
	#page-wrapper{
		margin-top: 20px ;
	}
	.lead{
		font-size: 18px !important;
		margin-left: 22px;
		margin-bottom: 0px !important;
	}
	.label{
		line-height: 1.5 !important;
	}
	.btn.pull-right, .btn-group.pull-right {
		margin: 0px !important;
	}
	
</style>
<?php //print_r($user_info[0]);exit;  ?>
	<div class="container" style="background:#fff;">
		<div class="row profileDiv" id="page-wrapper">
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-3 toppad" >   
				<div class="">
					<div class="">
					  <h3 class="lead" ><b><?php echo $user_info[0]->first_name.' '.$user_info[0]->last_name  ?></b></h3>
					</div>
					<div class="panel-body">
						<div class="row">
							<div class=" col-md-12 col-lg-12 "> 
								<table class="table table-user-information">
									<tbody>
										<tr>
											<td>First Name:</td>
											<td><?php echo $user_info[0]->first_name;?></td>
										</tr> 
									 
										<tr>
											<td>Last Name</td>
											<td><?php echo $user_info[0]->last_name;?></td>
										</tr>
										<tr>
											<td>Email</td>
											<td><?php echo $user_info[0]->Email_Address;?></td>                           
										</tr>
										<tr>
											<td>Company</td>
											<td><?php echo $user_info[0]->Company_Name;?></td>                           
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<span class="pull-right">
							<a style="padding: 5px;" href="<?php echo base_url();?>settings/editprofile" data-original-title="Edit this user" data-toggle="tooltip" type="button" class="btn btn-sm btn-info">EDIT</a>
							
						</span>
					</div>            
				</div>
			</div>
		</div>
    </div>	