<!DOCTYPE html>
<!--[if IE 8]>			<html class="ie ie8"> <![endif]-->
<!--[if IE 9]>			<html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->	<html> <!--<![endif]-->
	<head>
		<title><?php echo $page_title;  ?></title>
		<meta charset="utf-8" />
		<meta name="keywords" content="HTML5,CSS3,Template" />
		<meta name="description" content="" />
		<!--meta http-equiv="refresh" content="5" /-->
		<link rel="shortcut icon" href="/assets/image/favicon.ico" type="image/x-icon" />
		<link rel="icon" href="/assets/image/favicon.ico" type="image/x-icon"> 

		<!-- mobile settings -->
		<meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0" />
		<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->

		<!-- WEB FONTS : use %7C instead of | (pipe) -->
		<link href="https://fonts.googleapis.com/css?family=Open+S   ans:300,400%7CRaleway:300,400,500,600,700%7CLato:300,400,400italic,600,700" rel="stylesheet" type="text/css" />
		<!-- SWIPER SLIDER 
		<link href="<?php echo base_url();?>assets/plugins/slider.swiper/dist/css/swiper.min.css" rel="stylesheet" type="text/css" />-->

		<!-- CORE CSS -->
		<link href="<?php echo base_url(); ?>assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
		
		<!-- THEME CSS -->
		<link href="<?php echo base_url(); ?>assets/css/essentials.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url(); ?>assets/css/layout.css" rel="stylesheet" type="text/css" />

		<!-- PAGE LEVEL SCRIPTS -->
		<link href="<?php echo base_url(); ?>assets/css/header-1.css" rel="stylesheet" type="text/css" />
		
		<link href="<?php echo base_url(); ?>assets/css/layout-datatables.css" rel="stylesheet" type="text/css" />
		
		<link href="assets/plugins/jqgrid/css/ui.jqgrid.css" rel="stylesheet" type="text/css" />
		<link href="assets/css/layout-jqgrid.css" rel="stylesheet" type="text/css" />

		<!-- jQuery -->
		<script src="<?php echo base_url(); ?>assets/js/jquery-3.1.1.min.js"></script>
		<script type="text/javascript">var plugin_path = '<?php echo base_url(); ?>assets/plugins/';</script>
		<!--<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/jquery/jquery-2.1.4.min.js"></script>-->
		
		<style>
			@-moz-document url-prefix() {
				.sNav {
					margin-left: 16.5% !important;
				}
			}
			
			.ui-jqgrid-titlebar{
				color: #f5f5f5 !important;
				background:#f5f5f5 !important;
				// border: 2px solid #e3e3e3 !important;
				border-radius: 4px !important;
				color:black !important;
				height: 42px !important;
			}
			.ui-jqgrid-caption
			{
				font-weight: 700 !important;
				font-size: 12px !important;
				color:black;
			}
			@media (min-width: 768px) { 
				.validation-report-content{
						margin-left:-43%; 
				}
			}
			
			@media (min-width: 768px) { 
				.validation-report-content{
						margin-left:-43%; 
				}
			}
			
			@media (min-width: 1400px) { 
				.validation-report-content{
						margin-left:-23%; 
				}
			}
				
			.specialHand, .well{ 
				cursor: pointer; cursor: hand; 
			}

			#load_jqgrid {
				display:none !important;
			}
			.form-control{
				font-size: 11px;
				height: 32px;
			}
			.edit_packageDesc,.edit_packageName{
				font-size: 14px !important;
				margin-left: 4px !important;
				margin-top: 4px !important;
			}
			.dropdown-menu{
				min-width: 150px !important;
			}
			
			.clearfix{
				position: fixed !important;
				border-bottom: rgba(0,0,0,0.08) 1px solid !important;
			}
			
			body{
				padding-top: 0px !important;
			}
			
			form {
				margin-bottom: 5px !important; 
			}
			.nestableDiv{
				margin-bottom: 15px;
				margin-top: 15px;
			}
			.profileDiv{
				font-size: 12px !important;
			}
			.inlineForm{		
				display: inline !important;
				margin-left: 50px;
				//margin-right: 20px;
			}
			.nestableForm{
				margin-bottom: 55px;
				margin-top: 55px;
			}
			.item-box figure {
				text-align: left;
			}
			#demo3{
				padding-left: 30px;
			}
			#SubMenu1{
				padding-left: 50px;
			}
			.asset_list {
				margin-left: 20px;
				//margin-top: 1px !important;
			}
			.asset_struct {
				margin-left: 20px;
			}
			.asset_struct,.HeaderButton{
				display: none;
			}
			.source,.destination{
				max-height: 300px;
				overflow: overlay;
			}
			.form-inline{
				margin-bottom:10px;
			}
			.asset_item{
				padding: 10px 12px;
			}
			.list-group-item {		
				white-space: nowrap;
				max-width: 100%;
				overflow: hidden;
			}
			.list-group-item {
				padding: 5px 15px !important;
				//margin-bottom: 0px !important;
				border: 0px !important;
			}
			.borderBottom{
				border-bottom: rgba(0,0,0,0.05) 3px solid !important;
			}
			.mix{
				border: 1px solid #dddddd;
				height: 280px;
				margin-bottom: 10px;
			}
			.searchCompButton{
				margin-top:15px;
			}
			.checkbox label:after, 
			.radio label:after {
				content: '';
				display: table;
				clear: both;
			}

			.checkbox .cr,
			.radio .cr {
				position: relative;
				display: inline-block;
				border: 1px solid #a9a9a9;
				border-radius: .25em;
				width: 1.3em;
				height: 1.3em;
				float: left;
				margin-right: .5em;
			}

			.radio .cr {
				border-radius: 50%;
			}

			.checkbox .cr .cr-icon,
			.radio .cr .cr-icon {
				position: absolute;
				font-size: .8em;
				line-height: 0;
				top: 50%;
				left: 20%;
			}

			.radio .cr .cr-icon {
				margin-left: 0.04em;
			}

			.checkbox label input[type="checkbox"],
			.radio label input[type="radio"] {
				display: none;
			}

			.checkbox label input[type="checkbox"] + .cr > .cr-icon,
			.radio label input[type="radio"] + .cr > .cr-icon {
				transform: scale(3) rotateZ(-20deg);
				opacity: 0;
				transition: all .3s ease-in;
			}

			.checkbox label input[type="checkbox"]:checked + .cr > .cr-icon,
			.radio label input[type="radio"]:checked + .cr > .cr-icon {
				transform: scale(1) rotateZ(0deg);
				opacity: 1;
			}

			.checkbox label input[type="checkbox"]:disabled + .cr,
			.radio label input[type="radio"]:disabled + .cr {
				opacity: .5;
			}
			.checkbox, .radio {
				margin-top: 0px;
				margin-bottom: 0px;
			}
			.checkbox label, .radio label {
				min-height: 20px;
				padding-left: 5px;
			}
			.loader{
				float: right;
				text-align: center;
				vertical-align: middle;
			}
			.glyphicon-refresh-animate {
				-animation: spin .7s infinite linear;
				-webkit-animation: spin2 .7s infinite linear;
			}

			@-webkit-keyframes spin2 {
				from { -webkit-transform: rotate(0deg);}
				to { -webkit-transform: rotate(360deg);}
			}

			@keyframes spin {
				from { transform: scale(1) rotate(0deg);}
				to { transform: scale(1) rotate(360deg);}
			}

			.edit , .editNew{
				cursor: pointer;
			}
			
			.table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
				padding: 2px 8px;
			}
			h5 {
				margin-bottom: 10px !important;
			}
			
			.profileDiv{
				margin-top: 10px;
			}
			.lead{
				font-size: 18px !important;
			}
			.form-control {
				
				height: 33px !important;
				font-size: 12px !important;
			}
			
			
			.btn-default{
				padding: 1px 10px !important;
			}
			.btn-danger{
				height: 19px;
				font-weight: 400;
				font-size: 75%;
			}
			td 
			{		
				padding-top: 4px !important;
				padding-bottom: 4px !important;
			}
			.lead{
				float: left;
				margin-right: 12px;
				margin-left: 10px;

			}
			
			.fa-edit{
				margin-top: 5px;
			}
			.table-bordered>tbody>tr>td {
				// border: 0px solid #ddd !important;
				text-align: left !important;
			}
			
			.btn{
				padding: 1px 8px !important;
				font-size: 94% !important;
			}
			.description{
				float:left; 
				font-family: 'Open Sans',Arial,Helvetica,sans-serif !important;
				font-size: 13px !important;
			}
			.Completed
			{
				color: #fff !important;
				background-color: #5cb85c !important;;
				border-color: #4cae4c!important;;
			}
			.Deployment.In.Queue ,.Processed ,.Validate.In.Queue,.Deployment.In.Progress
			{
				color: #fff;
				background-color: #f0ad4e;
				border-color: #eea236;
			}
			.Deploy.Completed,{
				color: #fff;
				background-color: #5cb85c;
				border-color: #4cae4c;
			}
			.Errored,.Unsupported,.Missing.Assets,.Duplicate.Found,.Duplicates.Found{
				background-color: #d9534f !important;
				color: #ffffff !important;
			}
			.form-inline{
				margin-left: 15px !important;
			}
			.status_cursor{
				cursor: default !important;
			}
			.ui-jqgrid-labels th div{
				white-space: nowrap;
				overflow: hidden !important;
				height: 30px !important;
			}
			
			.ui-jqgrid .ui-jqgrid-hbox 
			{
				padding-right: 0px !important;
				margin-right: 0px !important;
			}
			
			.ui-jqgrid .ui-jqgrid-view, .ui-jqgrid .ui-paging-info, .ui-jqgrid .ui-pg-selbox, .ui-jqgrid .ui-pg-table {
				font-size: 11px !important;
			}
			.status_cursor.Validate.Completed
			{
				background-color:#337ab7 !important;
				border:#337ab7 !important;
				margin-right: 3%!important;
				width: 92%;
			}	
			.status_cursor.Deploy.Completed
			{
				margin-right: 3% !important;
				width: 92% !important;
			}
			.status_cursor.Errored,.status_cursor.Unsupported,.status_cursor.Duplicate.Found,status_cursor.Duplicates.Found
			{
				margin-right: 3%!important;
				width: 92%;
			}
			.status_cursor.Validate.In.Queue,.status_cursor.Deployment.In.Queue
			{
				margin-right: 3%!important;
				width: 92%;
			}
			.status_cursor.Missing.Assets
			{
				margin-right: 3%!important
				width: 92%;
			}
			body{
				min-height: 750px !important;
			}
			.jqgfirstrow{
				visibility: hidden;
			}
			section.page-header.page-header-xs {
				padding: 5px 0 0px 0 !important;
			}
			.ValidatePackage_Manual,.DeployPackage_close
			{
				background-color: #d9534f !important;
				border: #d9534f !important;
			}
			.expansiva {
				font-family: "expansiva", sans-serif;
				// margin: 0px 0px 0px 9px !important;
			}
			@media (min-width: 768px)
			{
				.navbar-right {
					float: right!important;
					margin-right: -8px !important;
				}
			}
			.Home_PackageName
			{
				//font-family: 'Open Sans',Arial,Helvetica,sans-serif !important;
				-webkit-font-smoothing: antialiased;
				font-weight:bold;
				font-size:12px !important;
				margin-left:20px;
				
			}
			.PackageName
			{
				//font-family: 'Open Sans',Arial,Helvetica,sans-serif !important;
				-webkit-font-smoothing: antialiased;
				font-weight:bold;
				font-size:12px !important;
				color: #000000;
			}
			.ui-jqgrid-btable{
				margin-top: -9px !important;
			}
			.s-ico{
				display: none !important;
			}
			
			.ui-jqgrid-hbox{
				margin-right: 16px !important;
			}
			.Validate.Completed,.Deploy.Completed,.Errored,.Unsupported,.Validate.In.Queue,.Deployment.In.Queue,.Processed ,.Deployment.In.Progress,.Missing.Assets,.Completed,.newButton,.Duplicate .Found,.Duplicates.Found,.Draft
			{
				width:118px !important;
			}
			
			.newBtn,.validBtn,.viewChildBtn,.deleteBtn,.unsprtBtn,.unsprtErr,.unsprtCpltd,.unsprtdup
			{
				width:74px !important;
			}
			.create_new_Package{
				height: 20px;
				margin-right: -5px;
				width: 120px;
			}
			
			@media (min-width: 992px)
			{
				.searchAssetName {
					width: 42% !important;
				}
			}
			@media (min-width: 768px)
			{
				.searchAssetName {
					width: 45% !important;
				}
			}
			@media (min-width: 360px)
			{
				.searchAssetName {
					width: 41% !important;
				}
			}
			@media (min-width: 640px)
			{
				.searchAssetName {
					width: 47% !important;
				}
			}
			@media (min-width: 1024px)
			{
				.searchAssetName {
					width: 43% !important;
				}
			}
			// color_change1:hover {
				// background-color: yellow !important; 
				// display:inline;
			// }
			
		</style>
		<link href="https://appcloud-dev.portqii.com/transporter2.0/assets/plugins/jqgrid/css/ui.jqgrid.css" rel="stylesheet" type="text/css" />
	<link href="https://appcloud-dev.portqii.com/transporter2.0/assets/css/layout-jqgrid.css" rel="stylesheet" type="text/css" />
	</head>
	
<?php 
	// echo '<pre>';
		// echo base_url();print_r($page_data['homeURL']);
	// echo '</pre>';	
	// print_r($page_data['component_list']);
?>	
	<div id="page-wrapper" style="margin-top:-20px;">
		<div class="container-fluid" style="font-family: Helvetica Neue,Helvetica Arial,sans-serif !important;">                
			<div class="row profileDiv" style="">
				<div class="col-md-2" style=""></div>
				<div class="col-md-8 col-lg-8 sNav" style="padding: 0px;">
					<nav class="navbar navbar-default navBarSize" style="min-height:53px;margin-left: auto;margin-right: auto;margin-bottom: -30px !important;">
						<div class="col-sm-12 col-xs-12 col-md-12 col-lg-12 statusNav" style="margin: 4px 0 0 0;padding:0 0 0 0; height:inherit;">
							<div class="col-md-6 col-lg-6 col-sm-6 col-xs-8 pull-left" style="padding:11px 0px 0 0;" >
								<a href="<?php echo base_url().$page_data['homeURL'];?>"><span class="Home_PackageName">HOME > </a><?php
									if(isset($page_data['changeset_info'][0]->Package_Name))
									{
										echo '<span class="PackageName"> ';
										// echo strtoupper($page_data['changeset_info'][0]->Package_Name);
										echo $page_data['changeset_info'][0]->Package_Name;

									}
									else
									{
										echo '<span class="PackageName"> ';
									?>	
										NEW PACKAGE
									<?php 
									}	
									?>
							</div>
							<div class="col-md-3 col-lg-3 col-sm-3 col-xs-3 pull-center" style="padding:0 0 0 0;">
								<input type="hidden" class="package_id_hidden" name ="package_id_hidden" readonly="readonly" value="<?php	echo $page_data['changeset_id'];?>" style="">
							</div>
							<div class="col-md-3 col-lg-3 col-sm-3 col-xs-3 pull-right" style="">
								<div class="inline pull-right" style="margin:5px 0 0 0;">
									<?php if(isset($page_data['changeset_info'][0]->status)){ ?>
										<?php if(($page_data['changeset_info'][0]->status == "") || ($page_data['changeset_info'][0]->status == "New") || ($page_data['changeset_info'][0]->status == "undefined")){ ?>
										<a class="btn btn-sm btn-info pull-right newButton status_cursor" style="margin-right: 3%; width: 92%;font-size: 82% !important;">New</a>
										<?php }else{ ?>
											<a class="btn btn-sm pull-right status_cursor <?php echo $page_data['changeset_info'][0]->status; ?>" style="margin-right: 3%; width: 92%;font-size: 82% !important;"><?php echo $page_data['changeset_info'][0]->status; ?></a>
										<?php } ?>								
									<?php }else{ ?>
										<a class="btn btn-sm btn-info pull-right newButton status_cursor" style="margin-right: 3%;width: 92%; font-size: 82% !important;">New</a>
									<?php } ?>
								</div>
							</div>
						</div>
					</nav>
				</div>
				<div class="col-md-2 col-lg-2"></div>
			</div>
		</div>
	</div>
	<div id="page-wrapper" style="margin-top:40px;">
		<div class="container-fluid" style="font-family: Helvetica Neue,Helvetica Arial,sans-serif !important;">                
			<div class="row profileDiv" style="/border:1px solid pink/;">
				<div class="col-md-2" style="/border:1px solid orange/;"></div>
				<div class="col-sm-12 col-xs-12 col-md-8 col-lg-8 mainContainer" style="height:auto;padding:0px 0px 0px 0px;">
					<!-- Package Details Div Start-->
					<div class="xyx" style="">
						<div class="well specialHand" id="packageDetailsPlus" data-toggle="collapse" data-target="#packageDetails">
							<b style="color:#000000;">DEPLOYMENT PACKAGE</b>
							<span class="glyphicon glyphicon-plus pull-right"></span>
						</div>
						<div class="col-md-12 col-lg-12 packageDetails collapse accordion-body"  id = "packageDetails" style="padding:0px 2px 0px 0px; /border:1px solid green;/">
							<div class="col-md-12 col-lg-12 form-inline" style="/border:1px solid green/;font-size: 11px !important;padding:0 0 0 0 !important; margin-left:0px !important;">
								<div class="col-md-9 col-lg-9" style="padding:0 0 0 0 !important;margin-top: -6px;">
									<div class="">
										<span class="pull-left col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0px 0px 0px 0px;">
											<h6 style="min-width: 155px; margin-bottom:0px !important;">Package Name </h6>
										</span>							
										<span class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fa fa-edit edit_packageName lead" style="display: none;padding:0 0 0 0 !important;"></span>
										<div class="col-xs-12 col-sm-12 col-md-5 col-lg-5" style="padding:0px;">
											<input  maxlength="50" type="text" style="width:376px;" id = "package_name_id" class="package_name package_value form-control  inlineFormInput " name="package_name" placeholder="Enter Your Deployment Package Name" value="<?php echo isset($page_data['changeset_info'][0]->Package_Name) ? $page_data['changeset_info'][0]->Package_Name : "" ;	?>" >
										</div>
									</div>
									<div class="row">
										<span class="pull-left col-lg-12 col-md-12 col-sm-12 col-xs-12"style="padding: 0px 0px 0px 0px;margin-left: 15px;" >
											<h6 style="min-width: 155px;margin-bottom:1px !important;margin-top: 10px;">Package Description </h6>
										</span>

										<span class="col-lg-12 col-md-5 col-sm-12 col-xs-12 fa fa-edit edit_packageDesc lead" style="display: none;"></span>
										<div class="col-xs-12 col-sm-12 col-md-5 col-lg-5" style="padding:0px;">
											<textarea maxlength="200" rows="3" style="width:376px; height: 72px !important;margin-left: 15px;" class="package_description form-control inlineFormInput " id="package_description_id" placeholder="Enter Your Deployment Package Description"><?php echo isset($page_data['changeset_info'][0]->Status_Details) ? $page_data['changeset_info'][0]->Status_Details : "" ;?></textarea>
										</div>	
									</div>
								</div>
								<div class="col-md-3 col-lg-3" style="/border:1px solid pink/;height:10vh;">
									<button type="button" class="btn btn-info create_new_Package pull-right" value="Create Package">Create Package</button>
								</div>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-5 col-lg-5 pull-left" style="margin-bottom: 20px !important;padding:0 0 0 0; /border:1px solid red;/">
								<span class="sourceSystem" style="font-size:12px; padding:0 0 10px 0;"><b>Source</b></span>
								<span class="err_msg" style="color: red; display:none;"><br/>Source and Target must have to be different</span>
								<select class="form-control select_source_System  selectSys1 source_orgID source_OrgId" id="orgID1" name="orgID1" >
									<option value=""></option>
									<?php									
										foreach($page_data['sites_list'] as $key=>$val)
										{
											// print_r($val);
											$select = '';
											if(isset($page_data['changeset_info'][0]->Source_Site_Name) && $page_data['changeset_info'][0]->Source_Site_Name ==$val)
											{
												$select = "selected";
											}
											echo '<option value="'.$val.'"'.$select.'>'.$val.'</option>';
											
										}
									?>
								</select><br/>
							</div>
							
							<!-- Transporter Truck Icon-->		
							<!--
							<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 pull-center" style="text-align: center;margin-top: 18px;"><span style="margin-top: 24px; color:orange;"><i class="icomoon icon-truck icon-2x"></i></span></div>
							-->
							<!-- Transporter Truck Icon-->	
							
							<div class="col-xs-12 col-sm-12 col-md-5 col-lg-5 pull-right" style="padding:0px 0px 0px 0px; /border:1px solid pink;/" > 
								<span class="targetSystem" style="padding:0 0 10px 0; font-size:12px;"><b>Target</b></span>
								<span class="err_msg" style="color: red; display:none;"><br/>Source and Target must have to be different</span>							
								<select class="form-control select_target_system selectSys2 target_orgID target_OrgId" id="orgID2" name="orgID2">
									<option value=""></option>
									<?php
										foreach($page_data['sites_list'] as $key=>$val)
										{
											// print_r();
											$select = '';
											if(isset($page_data['changeset_info'][0]->Target_Site_Name) && $page_data['changeset_info'][0]->Target_Site_Name ==$val)
											{
												$select = "selected";
											}
											echo '<option value="'.$val.'"'.$select.'>'.$val.'</option>';
										}
									?>
								</select><br/>
							</div>
							<!--
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin:-33px 0px 10px 0px;padding:0px 0px 0px 0px;">	
								<button type="button" class="btn btn-info create_new_Package pull-right" value="Create Package" style="height: 30px;">Create Package</button>
							</div>
							-->
						</div>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-12"  style="margin-top:5px; padding:0;">
					<div class="well" id="deploymentDetailsPlus" data-toggle="collapse" data-target="#deploymentDetails1" >
						<b style="color:#000000;">PACKAGE ASSET DETAILS</b>
						<span class="glyphicon glyphicon-plus pull-right"></span>
					</div>
					<div class="col-md-12 col-lg-12 deploymentDetails1 collapse" id="deploymentDetails1" style="/*border:1px solid orange;*/ padding:0px 0px 0px 0px;">
						<div class="col-xs-12 col-sm-12 col-md-5 col-lg-5 pull-left" style="margin-bottom: 20px !important;padding:0 0 0 0;">
							<span class="sourceSystem" style="font-size:12px; padding:0 0 10px 0;"><b>Source </b></span>
							<input type="name" class="form-control inlineFormInput source_system source_system_name selectSys" name="SourceSystemName" placeholder="Source System Name" value="<?php if(isset($page_data['changeset_info'][0]->Source_Site_Name)){echo $page_data['changeset_info'][0]->Source_Site_Name;}?>" disabled><br/>
							<div class="col-md-12 source_asset_tree_view" style="/border:1px solid red/;padding:0px;margin-top: -13px;">
								<div class="row" style="">
									<input type="hidden" class="OrgId OrgId_hidden" id="OrgId" name="OrgId" value=""/>
									<input type="hidden" class="list_type" name="list_type" value="source"/>
									<div class="col-md-4 col-xs-4" style="padding:0px;margin-left: 15px;">
										<select class="form-control inlineFormInput Asset_Type 	Asset_Type_copy" name="Asset_Type_Id"  >
										<option selected="selected" value='Asset Type' disabled>-Asset Type-</option>
										<?php 
											foreach($page_data['AssetType'] as $atkey=>$atval)
											{
												
												if($atval->Asset_Type_Name=='Folder' || $atval->Asset_Type_Name=='FM Folder' || $atval->Asset_Type_Name=='Email Folder' )
												{
													echo '<option style="display:none" value="'.$atval->Asset_Type_Name.'">'.$atval->Asset_Type_Name.'</option>';
												}
												else
												{
													echo '<option value="'.$atval->Asset_Type_Name.'">'.$atval->Asset_Type_Name.'</option>';
												}		
											}
										?>
									</select>
									</div>
									<div class="col-md-4 col-xs-4 col-sm-4 searchAssetName" style="padding:0px;margin-left:2px;">
										<input type="name" class="form-control inlineFormInput Asset_Name Asset_Name_copy" name="AssetName" placeholder="Asset Name" >
									</div>
									<div class="col-md-2 col-xs-2 searchCancel" style="padding:0px;margin-left:2px;">
										<a role="button" class="btn btn-default btn-sm searchAsset" style="margin-left: 2px;height: 32px;width: 31px;padding: 5px 8px !important;"><span class="fa fa-search"></span></a>
										<a role="button" class="btn btn-default btn-sm reset_asset" style="margin-left:-1px;height:32px;width:29px; padding: 5px 8px !important;"><span class="fa fa-times"></span></a>
									</div>
								</div>
								<div class="mix source mix_source" style="padding: 10px; overflow: auto; display: none;margin-top: 6px;"><!-- item -->
									<div class="">
										
											<div class="list-group panel">
												<?php  
													$Source_Site_Name = isset($page_data['changeset_info'][0]->Source_Site_Name)?$page_data['changeset_info'][0]->Source_Site_Name:'';
													echo '<input type="hidden" class="OrgId OrgId_hidden" name="OrgId" value=""/>';	
													foreach($page_data['AssetType'] as $atkey => $atval)
													{
														$Asset_Type_Name = str_replace(" ","_",$atval->Asset_Type_Name);
														if($atval->Asset_Type_Name=='FM Folder' || $atval->Asset_Type_Name=='Folder' || $atval->Asset_Type_Name=='Email Folder')
														{
															echo '<div class="">
															<input type="hidden" name="RSys_Asset_Type_Id" value="'.$atval->Asset_Type_Name .'"/>
																<a href="#source_asset_type'.$Asset_Type_Name.'" class="list-group-item asset_type borderBottom" data-toggle="collapse" data-parent="#MainMenu" aria-expanded="false" style="display:none;" >
																'.$atval->Asset_Type_Name.'<span class="fa fa-plus-square-o pull-right"></span></a>
															<div class="collapse asset_list" style="display:none;" id="source_asset_type'.$Asset_Type_Name.'">
															</div>
															</div>';
														}
														else
														{	
															echo '<div class="">
															<input type="hidden" name="RSys_Asset_Type_Id" value="'.$atval->Asset_Type_Name .'"/>
																<a href="#source_asset_type'.$Asset_Type_Name.'" class="list-group-item asset_type borderBottom" data-toggle="collapse" data-parent="#MainMenu" aria-expanded="false" >
																'.$atval->Asset_Type_Name.'<span class="fa fa-plus-square-o pull-right"></span></a>
															<div class="collapse asset_list" id="source_asset_type'.$Asset_Type_Name.'">
															</div>
															</div>';
														}
													}
												?>
											</div>
										
									</div>
								</div>
							</div>
						</div>	
						<div class="col-xs-12 col-sm-12 col-md-5 col-lg-5 pull-right" style="padding:0px 0px 0px 0px;" > 
							<span class="targetSystem" style="padding:0 0 10px 0; font-size:12px;"><b>Target </b></span>
							<input type="name" class="form-control inlineFormInput target_system target_system_name selectSys2" name="TargetSystemName" placeholder="Target System Name" value="<?php if(isset($page_data['changeset_info'][0]->Target_Site_Name))
								{echo $page_data['changeset_info'][0]->Target_Site_Name;}?>" 
							disabled><br/>	
							<div class="target_asset_tree_view">
								<div class="row searchButton" style="margin-bottom: 8px; margin-top: -10px;">
								<input type="hidden" class="OrgId OrgId_hidden" name="OrgId" value=""/>
								<input type="hidden" class="list_type" name="list_type" value="target"/>
								<div class="col-sm-4 pull-left" style="padding-right:0px;margin-top:-3px;">		
								<select class="form-control inlineFormInput Asset_Type Target_Asset_Type" name="Asset_Type_Id"  >
									<option selected="selected" value='Asset Type' disabled>-Asset Type-</option>
									<?php 
										foreach($page_data['AssetType'] as $atkey=>$atval)
										{
											if($atval->Asset_Type_Name=='Folder' || $atval->Asset_Type_Name=='FM Folder' || $atval->Asset_Type_Name=='Email Folder' )
											{
												echo '<option style="display:none" value="'.$atval->Asset_Type_Name.'">'.$atval->Asset_Type_Name.'</option>';
											}
											else
											{
												echo '<option value="'.$atval->Asset_Type_Name.'">'.$atval->Asset_Type_Name.'</option>';
											}
										} 
									?>
								</select>
								</div>
								<div class=" pull-left " style="padding-left: 1%; padding-right: 0%; width: 47%;margin-top:-3px;">												
									<input type="name" class="form-control inlineFormInput Asset_Name" name="AssetName" placeholder="Asset Name" >									
								</div>
								<div class="col-sm-1 pull-left" style="padding-left: 1%;margin-top:-3px;">
									<a class="btn btn-default btn-sm searchAsset inlineFormButton " style="padding:0px; line-height:26px;width:30px;"> <span class="fa fa-search "></span></a>
								</div>
								<div class="col-sm-1 pull-left" style="padding-left: 1%;margin-top:-3px;">
									<a class="btn btn-default btn-sm reset_asset inlineFormButton " style="padding:0px; line-height:26px;width:28px;">  <span class="fa fa-times "></span></a>
								</div>
											
								</div>
										
								<div class="mix destination" style="display: none; overflow: auto; padding-top: 10px;margin-top: -3px;">
									<div class="col-sm-12 col-md-12">
										<div class="list-group panel">
											<?php  
												$Target_Site_Name = isset($page_data['changeset_info'][0]->Target_Site_Name)?$page_data['changeset_info'][0]->Target_Site_Name:'';
												echo '<input type="hidden" class="OrgId OrgId_hidden" name="OrgId" value=""/>';
												foreach($page_data['AssetType'] as $atkey => $atval)
												{
													$Asset_Type_Name = str_replace(" ","_",$atval->Asset_Type_Name);
													// echo $Asset_Type_Name;
													if($atval->Asset_Type_Name=='FM Folder' || $atval->Asset_Type_Name=='Folder' || $atval->Asset_Type_Name=='Email Folder')
													{
														echo '<div class="">
														<input type="hidden" name="RSys_Asset_Type_Id" value="'.$atval->Asset_Type_Name .'"/>
															<a href="#asset_type_'.$Asset_Type_Name .'" class="list-group-item asset_type borderBottom" data-toggle="collapse" data-parent="#MainMenu" aria-expanded="false" style="display:none !important;" >
															'.$atval->Asset_Type_Name.'<span class="fa fa-plus-square-o pull-right"></span></a>
														<div class="collapse asset_list" style="display:none !important;" id="asset_type_'.$Asset_Type_Name.'">
														</div>
														</div>';
													}
													else
													{		
														echo '<div class="">
														<input type="hidden" name="RSys_Asset_Type_Id" value="'.$atval->Asset_Type_Name .'"/>
															<a href="#asset_type_'.$Asset_Type_Name .'" class="list-group-item asset_type borderBottom" data-toggle="collapse" data-parent="#MainMenu" aria-expanded="false">
															'.$atval->Asset_Type_Name.'<span class="fa fa-plus-square-o pull-right"></span></a>
														<div class="collapse asset_list" id="asset_type_'.$Asset_Type_Name.'">
														</div>
														</div>';
													}
												}
											?>										
										</div>
									</div>
								</div>
							</div>
						</div>	
					
					<!-- Deployment Package Div End-->
					<?php
						if( isset($page_data['component_list']))
							{
						?>	
							<!-- validation and Deploy Button with JQGRID -->
							<div class="row" style="margin-left: -30px;">						
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding: 0 0 0 0 !important;margin-left: 8px;">								
									<div class="text-center">
										<div class="text-center footer_deploy_button">
											<button class="btn btn-sm btn-info pull-center ValidatePackage"> Validate Package</button>
											<button class="btn btn-sm btn-info pull-center ValidationReport" >Validation Report</button>
											<button class="btn btn-sm btn-info pull-center DeployPackage" >Deploy Package</button>
										</div>
										<div class="text-center missingAssetsMessage">
											<span style="color:red;font-size:12px">Include missing assets to the Deployment Package from the Validation Report<span>
										</div>
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 jqgrid_div" style="padding-left: 22px;padding-right: 21px;">											
											<br/>
											<div class="table-responsive">
												<div class="" id="gridWrapper">
													<table id="jqgrid"></table>
													<div id="pager_jqgrid"></div>
												</div>
											</div>
										</div>
									</div>
								</div>							
							</div>
							<!-- validation and Deploy Button with JQGRID -->
						<?php } ?>	
					</div>	
					</div>
				</div>
				<div class="col-md-2"></div>
			</div>
			
			<!-- search_popup Modal Start -->
			<div class="search_popup" style="position: fixed; top: 0; width: 100%; height: 100%; background-color: rgba(0, 0, 0, 0.5); display: none;" >
				<div class="modal-dialog modal-sm" style=" top: 20%;  width: 40%;">
					<div class="modal-content">
						<!-- header modal -->
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title" id="mySmallModalLabel">Child Assets<span id="systemName"> <?php	
								foreach($page_data['orglist'] as $key=>$val)
								{
									if(isset($page_data['changeset_id'])){
										if($page_data['changeset_info'][0]->Source_Site_Name == $val->Site_Name)
											echo '<option value="'.$val->Instance__Id.'">'.$val->Site_Name.'</option>';
										continue;
									}
									$select = ($page_data['org1']->id == $val->Instance__Id)?"selected":'';
									echo '<option value="'.$val->Site_Name.'"'.$select.'>'.$val->Site_Name.'</option>';
								}											
							?></span>
							</h4>
						</div>
						<!-- body modal -->
						<div class="modal-body">
							<div class="form-inline" >
								<input type="hidden" class="OrgId" name="OrgId" value=""/>
								<input type="hidden" class="list_type" name="list_type" value=""/>
								<div class="form-group ">								
									<label ><b>Asset Type:</b></label>
									<select class="form-control inlineFormInput" name="Asset_Type_Id">
									<option value=""></option>
									<?php 
										foreach($page_data['AssetType'] as $atkey=>$atval)
										{
											echo '<option value="'.$atval->Asset_Type_Name.'">'.$atval->Asset_Type_Name.'</option>';										
										}
									?>
									</select>
								</div>
								<div class="form-group">
									<label  ><b>Asset Name:</b></label>
									<input type="name" class="form-control inlineFormInput" name="AssetName">									
								</div>
								<div class="form-group">
									<label  ><b></b></label>
									<a class="btn btn-default searchAsset inlineFormButton"> Search</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>	
			<!-- search_popup Modal End -->			
			
			<!-- Child Info Modal Start -->	
			<div class="childInfo" style="position: fixed; top: 0; width: 100%; height: 100%; background-color: rgba(0, 0, 0, 0.5); display: none;" >
				<div class="modal-dialog modal-sm" style=" top: 20%;  width: 40%;">
					<div class="modal-content">
						<!-- header modal -->
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title" id="mySmallModalLabel">Dependent Child List</h4>
							<span class="ItemAddSuccessMessage" style="font-size: 12px;color: #fff;margin-left: 0px;background-color:#8ac78d;padding: 2px 5px 4px;display:none;border-radius: 2px;">The deployment package has changed. Please validate.</span>
						</div>
						<!-- body modal -->
						
						<div class="modal-body" style="max-height: 375px;overflow: auto;">
							<!--<span class="ItemAddSuccessMessage" style="font-size: 12px;color: #fff;margin-left: 17px;background-color:#8ac78d;padding: 5px 10px 10px;display:none;border-radius: 2px;">The Deployment Package has changed. Please Validate.</span>-->
							<div class="list-group panel" >
								<div class="dependent_child_header" >
								</div>
								<?php  
									// print_r($page_data['changeset_info'][0]);
									$Source_Site_Name = isset($page_data['changeset_info'][0]->Source_Site_Name)?$page_data['changeset_info'][0]->Source_Site_Name:'';
									echo '<input type="hidden" class="OrgId" name="OrgId" value="'. $Source_Site_Name.'"/>';	
									foreach($page_data['AssetType'] as $atkey => $atval)
									{
										$Asset_Type_Name = str_replace(" ","_",$atval->Asset_Type_Name);
										echo '<div class="dependent_childInfo">
										<input type="hidden" name="RSys_Asset_Type_Id" value="'.$atval->Asset_Type_Name .'"/>
											<a href="#child_asset_type'.$Asset_Type_Name.'" class="list-group-item dependent_child childValue childValue'.$Asset_Type_Name.' borderBottom" data-toggle="collapse" data-parent="#MainMenu" aria-expanded="false" >
											'.$atval->Asset_Type_Name.'<span class="fa fa-plus-square-o pull-right"></span></a>
										<div class="collapse childList" id="child_asset_type'.$Asset_Type_Name.'">
											
										</div>
										</div>';
									}
								?>
							</div>			
						</div>
					</div>
				</div>
			</div>
			<!-- Child Info Modal End -->	
				
			<!-- Invalid Asset List Start-->
			<div class="invalidAssets" style="position: fixed; top: 0; width: 100%; height: 100%; background-color: rgba(0, 0, 0, 0.5); display: none;" >
				<div class="modal-dialog modal-sm" style=" top: 20%;  width: 40%;">
					<div class="modal-content">
						<!-- header modal -->
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title" id="mySmallModalLabel">Invalid Assets List</h4>
						</div>
						<!-- body modal -->
						<div class="modal-body" style="max-height: 375px;overflow: auto;">
							<div class="list-group panel" >
								<div class="invalid_child_assets" >
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<!-- Unsupported Asset List Start-->
			<div class="UnsupportedAssets" style="position: fixed; top: 0; width: 100%; height: 100%; background-color: rgba(0, 0, 0, 0.5); display: none;" >
				<div class="modal-dialog modal-sm" style=" top: 20%;  width: 40%;">
					<div class="modal-content">
						<!-- header modal -->
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title" id="mySmallModalLabel">Unsupported Assets List</h4>
						</div>
						<!-- body modal -->
						<div class="modal-body" style="max-height: 375px;overflow: auto;">
							<div class="list-group panel" >
								<div class="Unsupported_child_assets" >
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- Invalid Asset List End-->
			
			<!-- ValidationReport Start-->
			<div class="ValidationReportModel" style="position: fixed; top: 0; width: 100%; height: 100%; background-color: rgba(0, 0, 0, 0.5); display: none;" >
				<div class="modal-dialog modal-lg" style=" top: 0%;  width: 40%;">
					<div class="modal-content modal-lg validation-report-content" style="overflow:auto">
						<!-- header modal -->
						<div class="modal-header modal-lg">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title" id="mySmallModalLabel">Validation Report</h4>
							
						</div>
						<!-- body modal -->
						
						<div class="modal-body modal-lg " style="max-height: 50%;overflow: auto;" >
							<span class="ItemAddSuccessMessage" style="font-size: 12px;color: #fff;margin-left: 0px;background-color:#8ac78d;padding: 2px 5px 4px;display:none;border-radius: 2px;">The deployment package has changed. Please validate.</span>
							<div class="tab-content">
								<ul class="nav nav-tabs">
									<li class="active"><a data-toggle="tab" href="#Existing">Existing</a></li>
									<li><a data-toggle="tab" href="#Missing">Missing</a></li>
									<li><a data-toggle="tab" href="#AssignMicrosite" id="microSiteTab">Assign Microsites</a></li>
									<li><a data-toggle="tab" href="#DuplicateAssets" id="DuplicateAssetTab">Duplicate Assets</a></li>
								</ul>
								
								<div id="Existing" class="tab-pane fade in active" style="max-height: 350px;overflow-y: scroll;">
									<div class="Existing_asset" id="Existing_asset">
										<div class="list-group panel" >
											<br/>
											<!--
											<span class="ItemAddSuccessMessage" style="font-size: 12px;color: #fff;margin-left: 0px;background-color:#8ac78d;padding: 5px 10px 4px;display:none;border-radius:2px;">The deployment package has changed. Please validate.</span>
											-->
											<h5>Dependent Assets existing in target</h5>
											<div class="dependent_child_header" >
											</div>
											<?php  
												// print_r($page_data['changeset_info'][0]);
												$Source_Site_Name = isset($page_data['changeset_info'][0]->Source_Site_Name)?$page_data['changeset_info'][0]->Source_Site_Name:'';
												echo '<input type="hidden" class="OrgId" name="OrgId" value="'. $Source_Site_Name.'"/>';	
												foreach($page_data['AssetType'] as $atkey => $atval)
												{
													$Asset_Type_Name = str_replace(" ","_",$atval->Asset_Type_Name);
													echo '<div class="dependent_childInfo">
													<input type="hidden" name="RSys_Asset_Type_Id" value="'.$atval->Asset_Type_Name .'"/>
														<a href="#Existing_child_asset_type'.$Asset_Type_Name.'" class="list-group-item dependent_child childValue childValue'.$Asset_Type_Name.' borderBottom" data-toggle="collapse" data-parent="#MainMenu" aria-expanded="false" >
														'.$atval->Asset_Type_Name.'<span class="fa fa-plus-square-o pull-right"></span></a>
													<div class="collapse childList v_report" id="Existing_child_asset_type'.$Asset_Type_Name.'">
														
													</div>
													</div>';
												}
											?>
										</div>
										
											<input type="hidden" class="verify_existing_value" value="0" >
											<a style="margin-left: 44%;" class="btn btn-sm btn-info pull-center verify_existing" >Include all</a>
										
									</div>
									
								</div>
								<div id="Missing" class="tab-pane fade" style="max-height: 350px;overflow-y: scroll;">									
									<div class="Missing_asset" id="Missing_asset">
										<div class="list-group panel" >
											<br/>
											<!--
											<span class="ItemAddSuccessMessage" style="font-size: 12px;color: #fff;margin-left: 0px;background-color:#8ac78d;padding: 5px 10px 4px;display:none;border-radius:2px;">The deployment package has changed. Please validate.</span>
											-->
											<h5>Dependent Assets missing in target</h5>
											<div class="dependent_child_header" >
											</div>
											<?php  
												// print_r($page_data['changeset_info'][0]);
												$Source_Site_Name = isset($page_data['changeset_info'][0]->Source_Site_Name)?$page_data['changeset_info'][0]->Source_Site_Name:'';
												echo '<input type="hidden" class="OrgId" name="OrgId" value="'. $Source_Site_Name.'"/>';	
												foreach($page_data['AssetType'] as $atkey => $atval)
												{
													$Asset_Type_Name = str_replace(" ","_",$atval->Asset_Type_Name);
													echo '<div class="dependent_childInfo">
													<input type="hidden" name="RSys_Asset_Type_Id" value="'.$atval->Asset_Type_Name .'"/>
														<a href="#Missing_child_asset_type'.$Asset_Type_Name.'" class="list-group-item dependent_child childValue childValue'.$Asset_Type_Name.' borderBottom" data-toggle="collapse" data-parent="#MainMenu" aria-expanded="false" >
														'.$atval->Asset_Type_Name.'<span class="fa fa-plus-square-o pull-right"></span></a>
													<div class="collapse childList v_report" id="Missing_child_asset_type'.$Asset_Type_Name.'">
														
													</div>
													</div>';
												}
											?>
										</div>
										
											<input type="hidden" class="verify_existing_value" value="1" >
											<a style="margin-left: 44%;" class="btn btn-sm btn-info pull-center verify_existing" >Include all</a>
										
									</div>
								</div>
								<div id="AssignMicrosite" class="tab-pane fade" style="max-height: 350px;overflow-y: scroll;">									
									<div class="Assign_Microsite" id="Assign_Microsite" style="/border:1px solid red/;">
										<table class="table table-striped LP_MicroSite">
											<tr>
												<th>Landing Page Name</th>
												<th>Source Microsites</th>
												<th>Target Microsites</th>
											</tr>
										</table>
										<button style="margin-left: 44%;" class="btn btn-sm btn-info pull-center assignMicrositeSave" >Save</button>
										<br>
										<span class="MSSaved" style="margin-left: 300px;
										color: darkseagreen; display:none;">Changes saved.</span>
									</div>
								</div>
								<!-- Duplicate Assets -->
								<div id="DuplicateAssets" class="tab-pane fade" style="max-height: 350px;overflow-y: scroll;">									
									<div class="Duplicate_Assets" id="Duplicate_Assets" style="/border:1px solid red/;">
										<table class="table table-striped Assets_Duplicates">
											<tr><th>Parent Asset Type</th>
												<th>Parent Asset Name</th>
												<th>Asset Type</th>
												<th>Asset Name</th>
												<th>Duplicates</th>
											</tr>
										</table>
										<button style="margin-left: 44%;" class="btn btn-sm btn-info pull-center assignDuplicateAssetSave" >Save</button>
										<br>
										<span class="DuplicateSaved" style="margin-left: 300px;
										color: darkseagreen; display:none;">Changes saved.</span>
									</div>
								</div>
								<!-- Duplicate Assets -->
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- ValidationReport End-->
			
			<!-- Error of Deployed Assets Start -->
			<div class="errorMessage" style="position: fixed; top: 0; width: 100%; height: 100%; background-color: rgba(0, 0, 0, 0.5); display: none;" >
				<div class="modal-dialog modal-sm" style=" top: 20%;  width: 40%;">
					<div class="modal-content">
						<!-- header modal -->
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title" id="mySmallModalLabel">Error Message</h4>
						</div>
						<!-- body modal -->
						<div class="modal-body" style="max-height: 375px;overflow: auto;">
							<div class="list-group panel" >
								<div class="error_message" >
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- Error of Deployed Assets Start -->

			<!-- Deploy Package Modal Start -->
			<div class="deploy_package" style="position: fixed; top: 0; width: 100%; height: 100%; background-color: rgba(0, 0, 0, 0.5); display: none;" >				
				<div class="modal-dialog modal-sm" style=" top: 20%;  width: 40%;">
					<div class="modal-content">
						<!-- header modal -->
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title" id="mySmallModalLabel">Deploy Package</h4>
						</div>
						<!-- body modal -->
						<div class="modal-body text-center ">
							Are you sure you want to deploy ?
						</div>
						<!-- footer body modal -->
						<div class="modal-footer">
							<button type="button" data-dismiss="modal" class="btn btn-info btn-sm DeployPackage_ok" id="delete">Deploy</button>
							<button type="button" class="btn btn-info btn-sm DeployPackage_close" id="close1" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">Cancel</span></button>
						</div>
					</div>
				</div>
			</div>
			<!-- Deploy Package Modal End -->	
			<!-- Validate Package Modal Start -->
			<div class="validate_package" style="position: fixed; top: 0; width: 100%; height: 100%; background-color: rgba(0, 0, 0, 0.5); display: none;" >				
				<div class="modal-dialog modal-sm" style=" top: 20%;  width: 40%;">
					<div class="modal-content">
						<!-- header modal -->
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title" id="mySmallModalLabel">Validate Package</h4>
						</div>
						<!-- body modal -->
						<div class="modal-body text-center ">
							Would you like to include the missing assets into package ?
						</div>
						<!-- footer body modal -->
						<div class="modal-footer">
							<button type="button" data-dismiss="modal" class="btn btn-info btn-sm ValidatePackage_Auto" id="delete" style="width: 38px;">Yes</button>
							<button type="button" class="btn btn-info btn-sm ValidatePackage_Manual" id="close1" data-dismiss="modal" aria-label="Close" style="width: 38px;"><span aria-hidden="true">No</span></button>
						</div>
					</div>
				</div>
			</div>
			<!-- Validate Package Modal End -->			
		</div>
	</div>
<script>

	function eventList()
	{
		$('.ViewChild').click(function ()
		{
			$('.childInfo').show();
			$('.ItemAddSuccessMessage').hide();			
			var Type = $(this).parent().find('input[name="Asset_Type"]').val();
			var Id = $(this).parent().find('input[name="Asset_Id"]').val();
			var package_id = $(this).parent().find('input[name="changeset_id"]').val();		
			var package_item_id = $(this).parent().find('input[name="component_id"]').val();
			var datavalue = {Package_Id:package_id,Package_Item_Id:package_item_id, Asset_Id:Id , Asset_Type:Type};
			getChildInfo(datavalue);		
		});
		
		$('.viewInvalidAsset').click(function ()
		{
			$('.invalidAssets').show();
			var package_id = $(this).parent().find('input[name="changeset_id"]').val();
			var package_item_id = $(this).parent().find('input[name="component_id"]').val();	
			var datavalue = {Package_Id:package_id,Package_Item_Id:package_item_id};
			getInvalidAsset(datavalue);		
		});
		
		$('.ValidationReport').click(function ()
		{
			$('.ValidationReportModel').show();
			getMissingAsset();		
		});
		
		$('.Unsupported').click(function()
		{
			var assetList= $('.Unsupported_child_assets');
			assetList.html('');
			var unsupportedList = $(this).attr('value').split(",");
			
			var out = '';
			for(unsupported in unsupportedList)
			{
				out += '<div class="list-group-item "><label class="invalidAsset" style="color:#337ab7;">'+unsupportedList[unsupported]+' </label></div>';
			}
			assetList.append(out);
			$('.UnsupportedAssets').show();
		});
	}
	
	$('.close').click(function()
	{
		$(this).parent().parent().parent().parent().hide();
		$(this).parent().parent().parent().find('.asset_list').html('').removeClass('in').attr('aria-expanded','false');
		$(this).parent().parent().parent().find('.asset_type span').addClass('fa-plus-square-o').removeClass('fa-minus-square-o');
	});
	
	$('.search_popup .close').click(function(){
		$(this).parent().parent().parent().parent().hide();
	});
	
	$('.deploy_package #close1').click(function(){
		$(this).parent().parent().parent().parent().hide();
	});
	
	function deploymentInQueue()
	{
		var package_id = $('.package_id_hidden').val();
		$.ajax({			
			type: 'post',
			url: '<?php echo base_url();?>deploy/deploymentinqueue?pId='+package_id,
			//data: {},
			beforeSend: function()
			{
				$('.DeployPackage span').remove();
				$('.DeployPackage').append(' <span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span>');
				$('.DeployPackage').attr("disabled",'true');
				$('.DeployPackage').off('click');
			},
			success: function(data)
			{
				$('.deploy_package').hide();
				$('.deploy_package').find('.modal-body').text('You want to deploy it again');
				$('.DeployPackage span').remove();
				$('.footer_deploy_button').after('<p style="margin-bottom: 2px;">This Package Deployment is in queue.</p>');
				location.reload(1);
			},
			error: function() 
			{
				$('.DeployPackage span').remove();
			}			
			
		});
	}
	
	$(document).ready(function()
	{			
		var newButtonstatus = $('.status_cursor').text();
		if(newButtonstatus == 'Deploy Completed' || newButtonstatus=='New' || newButtonstatus=='Unsupported' || newButtonstatus == 'Validate Completed' || newButtonstatus == 'Errored' || newButtonstatus == 'Deployment In Queue' || newButtonstatus == 'Validate In Queue' || newButtonstatus=='Duplicates Found' || newButtonstatus =='Missing Assets')
		{	
			$(this).css("cursor", "default");
		}
		
		
		var flag = true;
		getPackageinfo();
		
		$('.DeployPackage_ok').click(function (){
			$(this).parent().parent().find('.modal-body').text('');
			$(this).parent().parent().find('.modal-body').append(' <span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span>');
			deploymentInQueue();
		});
		
		$('.DeployPackage').click(function (){
			$('.deploy_package').show();
		});
		//To display the existing package details
		var package_id_hidden = $('.package_id_hidden').val();
		$('.deploymentDetails').hide();
		if(package_id_hidden!='')
		{
			$('#package_name_id').show();
			$('#package_description_id').show();
			$('.edit_packageName').hide();
			$('.edit_packageDesc').hide();
			$('.deploymentDetails').show();
			$('#orgID1').attr('readonly',true);
			$('#orgID2').attr('readonly',true);
			$( ".selectSys1" ).attr('disabled',true);
			$( ".selectSys2" ).attr('disabled',true);
			$( ".create_new_Package" ).text("Update Package Name");
			$('.mix_source').show();
			$('.destination').show();
			var sourceValue= $('.source_system').val();
			var targetValue= $('.target_system').val();
			$('.mix_source').find('.OrgId_hidden').val(sourceValue);
			$('.destination').find('.OrgId_hidden').val(targetValue);
			$('#deploymentDetailsPlus').removeClass('well');	
			$('#deploymentDetailsPlus').addClass('well collapsed');
			$('#deploymentDetailsPlus').attr("aria-expanded","true");
			$('#deploymentDetailsPlus').css('display','block');
			var a = $('#deploymentDetailsPlus').children('span');
			if(a.attr("class")=="glyphicon glyphicon-plus pull-right")
			{	
				a.removeClass("glyphicon glyphicon-plus pull-right");
				a.addClass("glyphicon glyphicon-minus pull-right");
			}
			$('#deploymentDetails1').addClass('collapse in');
		}
				
		if(package_id_hidden=='')
		{
			$('#packageDetailsPlus').removeClass('well specialHand');	
			$('#packageDetailsPlus').addClass('well specialHand collapsed');
			$('#packageDetailsPlus').attr("aria-expanded","true");
			$('#packageDetailsPlus').css('display','block');
			var a = $('#packageDetailsPlus').children('span');
			if(a.attr("class")=="glyphicon glyphicon-plus pull-right")
			{	
				a.removeClass("glyphicon glyphicon-plus pull-right");
				a.addClass("glyphicon glyphicon-minus pull-right");
			}
			$('#packageDetails').addClass('collapse in');
			$('#deploymentDetailsPlus').hide();	
		}
		//to edit the package name
		$('.edit_packageName').click(function(){
			$(this).hide();
			$('#Package_name_label').hide();
			$('#package_name_id').show();
			//$('#package_description').show();
		});
		//on click of edit package description 
		$('.edit_packageDesc').click(function(){
			$(this).hide();
			$('#package_description_label').hide();
			$('.package_description').show();
		});
		
		//to change the text of button on click of the package name
		$('.edit_packageName').click(function()
		{
			$(".create_new_Package").attr('value', 'Update Package Name');
			$(".create_new_Package" ).show();
			
		});	
		
		//To differentiate between source and target site name
		$('.selectSys1 option').each(function()
		{
			var val_one = $('.selectSys1').val();
			$('.selectSys2 option').each(function()
			{
				if($(this).val() == val_one)
					$(this).hide();
				else
					$(this).show();
			});
		});
		
		$('.selectSys1').change(function()
		{
			$('.selectSys1 option').each(function()
			{
				var val_one = $('.selectSys1').val();
				$('.selectSys2 option').each(function()
				{
					if($(this).val() == val_one)
						$(this).hide();
					else
						$(this).show();
				});
			});
		});	
		
		$('.selectSys2').change(function()
		{
			$('.selectSys2 option').each(function()
			{				
				var val_one = $('.selectSys2').val();
				$('.selectSys1 option').each(function()
				{					
					if($(this).val() == val_one)
						$(this).hide();
					else
						$(this).show();
				});
			});
		});
		
		//create new package with required validation
		$( ".create_new_Package" ).click(function() 
		{
			var package_name = $('#package_name_id').val();
			var source_value = $('.selectSys1 :selected').val();
			var target_value = $('.selectSys2 :selected').val();
			if(package_name == '' || source_value == '' || target_value == '')
			{
				//alert('Package_Name,Source System and Target System can not be empty');
				if(package_name=='')
				{	
					$('#package_name_id').css("border-color", "red");
					
				}
				if(source_value=='')
				{	
					$('#orgID1').css("border-color", "red");
				}
				if(target_value=='')
				{	
					$('#orgID2').css("border-color", "red");
				}
				
			}
			$( "#package_name_id" ).keyup(function() {
				$('#package_name_id').css("border-color", "#ddd");
			});	
			
			$('.selectSys1').change(function()
			{
				if($('.selectSys1 :selected').val()=='')
				{
					$('#orgID1').css("border-color", "red");
				}	
				else
				{
					$('#orgID1').css("border-color", "#ddd");
				}
			});	
			$('.selectSys2').change(function()
			{
				if($('.selectSys2 :selected').val()=='')
				{
					$('#orgID2').css("border-color", "red");
				}	
				else
				{
					$('#orgID2').css("border-color", "#ddd");
				}
			});

			if(package_name != '' && source_value != '' && target_value != '')
			{
				var package_name = $('#package_name_id').val();
				var package_description = $('#package_description_id').val();
				var source_value = $('.selectSys1 :selected').val();
				var target_value = $('.selectSys2 :selected').val();
				var package_id_hidden = $('.package_id_hidden').val();
				$.ajax(
				{
					type: 'post',
					url: 'Package_info/create_new_Package',
					data: {package_id:package_id_hidden,packageName: package_name, packageDescription: package_description,sourceName:source_value,targetName:target_value},
					dataType:'json',
					beforeSend: function()
					{	
						$( ".create_new_Package" ).prop('disabled',true);
					},
					success: function(data)
					{
						var package_text = "";
						$( ".create_new_Package" ).prop('disabled',false);
						$( ".selectSys1" ).prop('disabled',true);
						$( ".selectSys2" ).prop('disabled',true);
						$( ".package_id_hidden" ).val(data);	
						package_text = $( ".create_new_Package" ).text();
						$( ".create_new_Package" ).text("Update Package Name");
						$('input[name=SourceSystemName]').val(source_value);
						$('input[name=TargetSystemName]').val(target_value);
						$('.mix_source').show();
						$('.destination').show();
						$('.deploymentDetails').show();
						$('.mix_source').find('.OrgId_hidden').val(source_value);
						$('.destination').find('.OrgId_hidden').val(target_value);
						history.pushState('data to be passed', 'Title of the page', 'https://appcloud-dev.portqii.com/transporter2.0/package_info?id='+data);
						if(package_text != "Update Package Name")
						{
							$('#packageDetailsPlus').attr("aria-expanded","false");
							$('#deploymentDetailsPlus').show();	
							$('#deploymentDetailsPlus').removeClass('well');	
							$('#deploymentDetailsPlus').addClass('well collapsed');
							$('#deploymentDetailsPlus').attr("aria-expanded","true");
							$('#deploymentDetailsPlus').css('display','block');
							$('#deploymentDetailsPlus').addClass('collapse in');
							var a = $('#deploymentDetailsPlus').children('span');
							if(a.attr("class")=="glyphicon glyphicon-plus pull-right")
							{	
								a.removeClass("glyphicon glyphicon-plus pull-right");
								a.addClass("glyphicon glyphicon-minus pull-right");
							}
							$('#deploymentDetails1').addClass('collapse in');
							$('#packageDetails').removeClass('in');
							var a = $('#packageDetailsPlus').children('span');
							if(a.attr("class")=="glyphicon glyphicon-minus pull-right")
							{	
								a.removeClass("glyphicon glyphicon-minus pull-right");
								a.addClass("glyphicon glyphicon-plus pull-right");
							}
						}
					},
					error: function() 
					{
						alert('unsuccessful');
					}
				});	
			}		
		});
		
		//To find the assets using search box
		$(".Asset_Type").blur(function()
		{
			var Asset_Type = $('.Asset_Type :selected').val();
			if(Asset_Type == "Asset Type")
			{
				$(this).css("border-color", "red");
			}
			else
			{
				$(this).css("border-color", "#ddd");
			}
		});
		
		$(".Target_Asset_Type").blur(function()
		{
			var Target_Asset_Type = $('.Target_Asset_Type :selected').val();
			if(Target_Asset_Type == "Asset Type")
			{
				$(this).css("border-color", "red");
			}
			else
			{
				$(this).css("border-color", "#ddd");
			}
		});	
		
		$('.Asset_Type').on('change',function()
		{	
			// $(this).parent().parent().parent().find('.asset_list').hide();
			$(this).parent().find('.asset_list').hide();
		    $(this).parent().parent().parent().find('#source_asset_type'+$(this).val().replace(" ","_")).show();    
			$(this).parent().parent().parent().find('#asset_type_'+$(this).val().replace(" ","_")).show();    
			$(this).parent().parent().parent().find('.asset_type').hide();
			$(this).parent().parent().parent().find('#source_asset_type'+$(this).val().replace(" ","_")).parent().find('.asset_type').show();
			$(this).parent().parent().parent().find('#asset_type_'+$(this).val().replace(" ","_")).parent().find('.asset_type').show();
        });

		$('.reset_asset').click(function()
		{
			$(this).parent().parent().find('.Asset_Type option').removeAttr('selected , disabled');
			$(this).parent().parent().find('.Asset_Type option:first').attr('selected','selected');
			$(this).parent().parent().find('.Asset_Type option:first').attr('disabled', true);
			$(this).parent().parent().find('.Asset_Name').val('');
			$(this).parent().parent().find('.Asset_Type').css("border-color", "#ddd");
			$(this).parent().parent().parent().find('.asset_list').html('').removeClass('in').attr('aria-expanded','false');
			$(this).parent().parent().parent().find('.asset_type span').addClass('fa-plus-square-o').removeClass('fa-minus-square-o');
			$(this).parent().parent().parent().find('.asset_type').attr('aria-expanded','false');
			$(this).parent().parent().parent().find('.asset_type').show();
		});
		
		//on click of search Asset Icon
		$('.searchAsset').click(function()
		{
			var Asset_Type = $(this).parent().parent().find('.Asset_Type :selected').val();
			if(Asset_Type == "Asset Type")
			{
				$(this).parent().parent().find('.Asset_Type').focus();
				$(this).parent().parent().find('.Asset_Type').css("border-color","red");
				return false;
			}
			var AssetId=$(this).parent().parent().find('select[name="Asset_Type_Id"]').val();
			if($(this).parent().parent().parent().find('.list_type').val() == 'source' )
			{
				$('#source_asset_type'+AssetId).parent().find('.asset_list').html('');		
				var assetlist = $('#source_asset_type'+AssetId.replace(" ", "_")).parent().find('.asset_list');
				var tempdata = {
				"Asset_Type_Id" : $(this).parent().parent().find('select[name="Asset_Type_Id"]').val(),
				"OrgId" : $(this).parent().parent().parent().parent().find('.source_system_name').val(),
				"AssetName" : $(this).parent().parent().find('input[name="AssetName"]').val(),
				"page" : 1,
			};
			}
			else
			{
				$('#asset_type_2_'+AssetId).parent().find('.asset_list').html('');		
				var assetlist = $('#asset_type_'+AssetId.replace(" ", "_")).parent().find('.asset_list');
				var tempdata = {
				"Asset_Type_Id" : $(this).parent().parent().find('select[name="Asset_Type_Id"]').val(),
				"OrgId" : $(this).parent().parent().parent().parent().find('.target_system').val(),
				"AssetName" : $(this).parent().parent().find('input[name="AssetName"]').val(),
				"page" : 1,
			};
			}
			if($.trim($(this).parent().find('.asset_list').html()) =='')
				getSearchAsset(this,assetlist,tempdata);
				disabled_checkbox();
			
		});
		
		$('.Asset_Name').keyup(function(e)
		{
			if(e.keyCode == 13)
			{
				var Asset_Type = $(this).parent().parent().find('.Asset_Type :selected').val();
				if(Asset_Type == "Asset Type")
				{
					$(this).parent().parent().find('.Asset_Type').focus();
					$(this).parent().parent().find('.Asset_Type').css("border-color","red");
					return false;
				}
				
				var AssetId=$(this).parent().parent().find('select[name="Asset_Type_Id"]').val();
				// var OrgId=$(this).parent().parent().find('.source_value').val();		
				
				if($(this).parent().parent().parent().find('.list_type').val() == 'source' )
				{
					$('#source_asset_type'+AssetId).parent().find('.asset_list').html('');	
					var tempdata = {
										"Asset_Type_Id" : $(this).parent().parent().find('select[name="Asset_Type_Id"]').val(),
										"OrgId" : $(this).parent().parent().parent().parent().find('.source_system_name').val(),
										"AssetName" : $(this).parent().parent().find('input[name="AssetName"]').val(),
										"page" : 1,
									};					
					var assetlist = $('#source_asset_type'+AssetId.replace(" ", "_")).parent().find('.asset_list');
				}
				else
				{
					$('#asset_type_2_'+AssetId).parent().find('.asset_list').html('');		
					var tempdata = 
					{
						"Asset_Type_Id" : $(this).parent().parent().find('select[name="Asset_Type_Id"]').val(),
						"OrgId" : $(this).parent().parent().parent().parent().find('.target_system').val(),
						"AssetName" : $(this).parent().parent().find('input[name="AssetName"]').val(),
						"page" : 1,
					};
					var assetlist = $('#asset_type_'+AssetId.replace(" ", "_")).parent().find('.asset_list');
				}
				if($.trim($(this).parent().find('.asset_list').html()) =='')
					var search_button = $(this).parent().parent().find('.searchAsset');
					getSearchAsset(search_button, assetlist, tempdata);
			}
		});
		
		$('.asset_type').click(function()
		{	
			if($(this).attr('aria-expanded') == "false")
			{
				$('.asset_list').css('display', 'block');
				$(this).find(".fa-plus-square-o").addClass("fa-minus-square-o").removeClass("fa-plus-square-o");
				
			}
			else
			{
				$(this).find(".fa-minus-square-o").addClass("fa-plus-square-o").removeClass("fa-minus-square-o");
				 // $('.asset_list').attr('aria-expanded', 'false')
				$('.asset_list').css('display', 'none');
				// $('.asset_type').css('display', 'none');
			}
			var tempdata = {
						"Asset_Type_Id":$(this).parent().find('input[name="RSys_Asset_Type_Id"]').val(),
						"OrgId": $(this).parent().parent().find('.OrgId_hidden').val(),
					};
				// alert(tempdata.OrgId);	
			var assetlist = $(this).parent().find('.asset_list');
			
			$.trim($(this).parent().find('.asset_list').empty());
			if($.trim($(this).parent().find('.asset_list').html()) =='')
			{
				getAssetList(this,assetlist,tempdata);
			}
			disabled_checkbox();
		});
		
		function getSearchAsset(Asset,assetlist,tempdata)
		{
			$.ajax(
			{
				type: 'post',
				url: '<?php echo base_url();?>package_info/searchAsset',
				data: tempdata,
				beforeSend: function()
				{
					$(Asset).html('<span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span>');
					//$('.no_asset').hide();
				},
				success: function(data)
				{  
					$(assetlist).empty();
					$(Asset).html('<span class="fa fa-search"></span>');
					$(Asset).find('.loader').hide();
					$('.search_popup').hide();
					var list = JSON.parse( data );
					// alert(list);
					$(list.elements).each(function()
					{
						var out = printObject(this);
						// $(assetlist).append( '<div class="list-group-item "><input class="additem" type="checkbox" value="'+this.id+'"/><a href="#id_'+tempdata.OrgId+'_'+ this.id +'" class="asset_item" data-toggle="collapse" >'+this.name +'</a></div>' );
						// $('.destination').find('.additem').remove();
						var newButtonstatus = $('.status_cursor').text();
						if(newButtonstatus == 'Deploy Completed' || newButtonstatus == 'Deployment In Queue' || newButtonstatus == 'Validate In Queue' || newButtonstatus == 'Errored')
						{
							$(assetlist).append( '<div class="list-group-item "><input name="Asset_Type"  type="hidden" value="'+this.type+'"/><input id="AssetId" name="Asset_Type"  type="hidden" value="'+this.id+'"/><a><i class="fa fa-plus-square additem_" style="color: #46b8da;display:none;" aria-hidden="true" title="Click to add asset"></i></a><a style="cursor:default !important;" href="#id_'+tempdata.OrgId+'_'+ this.id +'" class="asset_item" data-toggle="collapse" >'+this.name +'</a></div>' );
						}
						else
						{
							$(assetlist).append( '<div class="list-group-item "><input name="Asset_Type"  type="hidden" value="'+this.type+'"/><input id="AssetId" name="Asset_Type"  type="hidden" value="'+this.id+'"/><a><i class="fa fa-plus-square additem_" style="color: #46b8da;" aria-hidden="true" title="Click to add asset"></i></a><a style="cursor:default !important;" href="#id_'+tempdata.OrgId+'_'+ this.id +'" class="asset_item" data-toggle="collapse" >'+this.name +'</a></div>' );
						}
						
						$('.destination').find('.additem_').remove();
					});	
					if($('.asset_type').hasClass('collapsed'))
					{
						$('.asset_type').removeClass('collapsed');
						$(this).attr('aria-expanded') = "true";
						//$('.asset_list').addClass('in');
						$('.asset_list').attr('aria-expanded') = "true";
						
					}
					
					$('.asset_list').parent().find(".fa-plus-square-o").addClass("fa-minus-square-o").removeClass("fa-plus-square-o");
					
					if(list.page * list.pageSize < list.total ){
						var pageno = list.page +1;
						$(assetlist).append( '<div class="load_more_asset"><input class="page" type="hidden" value="'+pageno+'" /> <a class="btn btn-xs btn-info">Load More ('+list.page * list.pageSize +'/'+list.total+') </a></div>' );
					}
					if(list.total<=0 ){
						$(assetlist).append( '<div class="no_asset">No asset found.</div>' ).delay(2000).fadeOut(1000);
					}					
					$('.load_more_asset').click(function(){
						// var test= $(".mix.source").find('.OrgId_hidden').val();
						// console.log(test);
					var tempdata = {
								"Asset_Type_Id":$(".Asset_Type").val(),
								// "Asset_Type_Id":$(this).parent().parent().find('input[name="Asset_Type_Id"]').val(),
								// "OrgId": $(".mix.source").find('.OrgId_hidden').val(),
								"OrgId": $(this).parent().parent().parent().find('.OrgId_hidden').val(),
								"AssetName" : $(".Asset_Name ").val(),
								"page": $(this).find('.page').val(),
							};
							// alert(tempdata.AssetName);
					
					var assetlist = $(this).parent().parent().find('.asset_list');						
						// getAssetList(this,assetlist,tempdata);						
						getSearchAsset(this,assetlist,tempdata);						
						$(this).remove();
					});
					disabled_checkbox();
					additem_new();
				},
				error: function()
				{
					result = false;
				}
			});
		}
		
		function getAssetList(Asset,assetlist,tempdata)
		{
			
			if(flag == true)
			{
				$.ajax(
				{
					type: 'post',
					url: '<?php echo base_url();?>package_info/getAssetData',
					data: tempdata,
					beforeSend: function()
					{
						$(Asset).append('<div class="loader">'+
							'<span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span>'+
						'</div></a>');
						flag = false;
					},
					success: function(data)
					{
						$(Asset).find('.loader').hide();
						//$('.search_popup').hide();
						$('.searchAsset').html('<span class="fa fa-search"> </span>');
						var list = JSON.parse( data );
						
						$(list.elements).each(function()
						{
							var out = printObject(this);
							
							// $(assetlist).append( '<div class="list-group-item "><input class="additem"  type="checkbox" value="'+this.id+'"/><a href="#id_'+tempdata.OrgId+'_'+ this.id +'" class="asset_item" data-toggle="collapse" >'+this.name +'</a></div>' );
							// $('.destination').find('.additem').remove();
							var newButtonstatus = $('.status_cursor').text();
							if(newButtonstatus == 'Deploy Completed' || newButtonstatus == 'Deployment In Queue' || newButtonstatus == 'Validate In Queue' || newButtonstatus == 'Errored')
							{
								$(assetlist).append( '<div class="list-group-item "><input name="Asset_Type"  type="hidden" value="'+this.type+'"/><input id="AssetId" name="Asset_Type"  type="hidden" value="'+this.id+'"/><a><i class="fa fa-plus-square additem_" style="color: #46b8da;display:none" aria-hidden="true" title="Click to add asset"></i></a><a style="cursor:default !important;" href="#id_'+tempdata.OrgId+'_'+ this.id +'" class="asset_item" data-toggle="collapse" >'+this.name +'</a></div>' );
								$('.destination').find('.additem_').remove();	
							}
							else
							{
								$(assetlist).append( '<div class="list-group-item "><input name="Asset_Type"  type="hidden" value="'+this.type+'"/><input id="AssetId" name="Asset_Type"  type="hidden" value="'+this.id+'"/><a><i class="fa fa-plus-square additem_" style="color: #46b8da;" aria-hidden="true" title="Click to add asset"></i></a><a style="cursor:default !important;" href="#id_'+tempdata.OrgId+'_'+ this.id +'" class="asset_item" data-toggle="collapse" >'+this.name +'</a></div>' );
								$('.destination').find('.additem_').remove();
							}
													
						});	
						
						if(list.page * list.pageSize < list.total ){
							var pageno = list.page +1;
							$(assetlist).append( '<div class="load_more_asset"><input class="page" type="hidden" value="'+pageno+'" /> <a class="btn btn-xs btn-info">Load More ('+list.page * list.pageSize +'/'+list.total+') </a></div>' );
						}					
						$('.load_more_asset').click(function(){
						var tempdata = {
									"Asset_Type_Id":$(this).parent().parent().find('input[name="RSys_Asset_Type_Id"]').val(),
									"OrgId": $(this).parent().parent().parent().find('.OrgId_hidden').val(),
									"page": $(this).find('.page').val(),
								};
						var assetlist = $(this).parent().parent().find('.asset_list');
							
							getAssetList(this,assetlist,tempdata);
							$(this).remove();
						});
						disabled_checkbox();
						flag = true;
						// additem_new();
						additem_new();
					},
					error: function()
					{
						result = false;
						flag =true;
						//alert("errored");
					}
				});
			}
		}
		
		function printObject(o) 
		{
			var out = '';
			for (var p in o) {
				if(typeof o[p] === 'object')
				{
					o[p] = printObject(o[p]);
					o[p] = '{ <br/> '+o[p] + '} <br/>';
				}
				out += p + ': ' + o[p] + '<br/>';
			}
			return out;
		}
		
		function disabled_checkbox()
		{
			var status = $('.status_cursor').text();
			if(status == 'Deploy Completed' || status == 'Deployment In Queue')
			{
				$("input.additem").attr("disabled", true);
				$(".ValidatePackage").removeClass('.ValidatePackage');
				$(".DeployPackage").removeClass('.DeployPackage');
				
			}
		}
		
		$('.asset_type').click(function()
		{
			// console.log('clicked');
			console.log($('.asset_type').attr('aria-expanded'));
		});	
		
		$('.well.specialHand').click(function()
		{
			var a = $(this).children('span');
			if(a.attr("class")=="glyphicon glyphicon-plus pull-right")
			{	
				a.removeClass("glyphicon glyphicon-plus pull-right");
				a.addClass("glyphicon glyphicon-minus pull-right");
				
			}
			else
			{
				a.removeClass("glyphicon glyphicon-minus pull-right");
				a.addClass("glyphicon glyphicon-plus pull-right");
			}
			
			$(this).parent().children().toggle();
			$(this).toggle();
		});
		
		$('#deploymentDetailsPlus').click(function(){
			
			var a = $(this).children('span');
			// alert(a.attr("class"));
			if(a.attr("class")=="glyphicon glyphicon-plus pull-right")
			{	
				a.removeClass("glyphicon glyphicon-plus pull-right");
				a.addClass("glyphicon glyphicon-minus pull-right");
				
			}
			else
			{
				a.removeClass("glyphicon glyphicon-minus pull-right");
				a.addClass("glyphicon glyphicon-plus pull-right");
			}
			
			$(this).parent().children().toggle();
			$(this).toggle();
		});
		
		$('.verify_existing').click(function ()
		{
			var package_id = $('.package_id_hidden').val();
			var verify_existing_value = $(this).parent().find('.verify_existing_value').val();
			$.ajax(
			{
				type: 'post',
				url: '<?php echo base_url();?>package_info/verify_existing',
				data: {packageId: package_id, verify_existing_value: verify_existing_value},
				dataType:'json',
				beforeSend: function()
				{
					$( ".verify_existing" ).attr('disabled',true);
				},
				success: function(data)
				{
					$('.ItemAddSuccessMessage').show().delay(2000).fadeOut(1000);
					$( ".verify_existing" ).attr('disabled',false);
					get_jqgrid(data);
					var $mygrid =  $("#jqgrid");
					allGridParams = $mygrid.jqGrid("getGridParam");
					allGridParams.data = data;
					$mygrid.trigger("reloadGrid", [{current: true}]);
					
					if(data != ""){
						$('.selectSys , .selectSys *').attr("disabled", true);
						$('.selectSys2 , .selectSys2 *').attr("disabled", true);
					} 
					
				},
				error: function() 
				{
					$( ".verify_existing" ).attr('disabled',false);
				
				}
			});
		});	
		var micrositesList = null;
		$('#microSiteTab').click(function()
		{
			if($('.LP_MicroSite').hasClass('loader'))
			{
				alert('has');
				// $('.loader').display();
			}
			var package_id = $('.package_id_hidden').val();
			$.ajax(
			{
				type: 'post',
				url: '<?php echo base_url();?>package_info/getAllLandingPages',
				data: {packageId: package_id},
				dataType:'json',
				beforeSend: function()
				{
					var spinner='';
					$('.tableRow').hide();
					if($('.LP_MicroSite').find(".glyphicon-refresh-animate"))
					{
						$('.loader').remove();
					}
					spinner +='<div class="loader" style="margin-left: 230px !important;"><span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span></div>';
					$('.LP_MicroSite').append(spinner);
				},
				success: function(data)
				{
					if(data.Landingpages=='')
					{
						$('.tableRow').remove();
						out+='<tr class="tableRow"><td></td><td style="font-style:italic">No Landing Page found in package</td><td></td></tr>';
						$('.LP_MicroSite').append(out);
						$('.assignMicrositeSave').hide();
					}
					// console.log(data.Landingpages);	
					var micrositData = getMicrositeOptionValue(data.Microsites);
					micrositesList = data.Microsites;
					var LandingpageData = data.Landingpages;
					var out='';
					$('.loader').hide();
					
					$.each(LandingpageData, function (LPkey , LPval) 
					{
						var MSSelect = '<select class="trgtMSList" style="padding: 0 0 0 0;width: 254px !important;height:24px !important;" size="1">';
						MSSelect +='<option></option>';
						$.each(micrositData, function (key , val) 
						{
							if(LPval.MicrositeIdSaved == val.id)	
							{
								var selected = '';
								selected = 'selected'
								MSSelect += "<option selected value='" + val.id + "'>" + val.name +" ( "+ val.href + " )</option>";
								$('.assignMicrositeSave').attr('disabled',true);
							}
							else
							{
								var selected = '';
								selected = 'selected';
								MSSelect += "<option value='" + val.id + "'>" + val.name +" ( "+ val.href + " )</option>";
								$('.assignMicrositeSave').attr('disabled',false);
							}
						});
						MSSelect += "</select>";
						out += '<tr class="tableRow"><td class="LPName">'+LPval.LandingpageName+'<input type=hidden class=LPId value='+LPval.LandingpageId+'></td><td>'+LPval.MicrositeName+'</td><td>'+MSSelect+'</td></tr>';
					});
					$('.LP_MicroSite').append(out);
				},
				error: function() 
				{
					// alert('Microsite is saved successfully');
				}
			});
		});	

		function getMicrositeOptionValue(microsites)
		{
			var micrositeArray= [];
			$.each(microsites, function (key , val) 
			{	
				var obj = {"id":val.id, "name":val.name,"href":val.domains};
				micrositeArray.push(obj);
			});
			return micrositeArray;
		}
		
		$('.assignMicrositeSave').click(function()
		{
			var package_id = $('.package_id_hidden').val();
			var JSONFormed= [];
			$('.LP_MicroSite > tbody  > tr:not(:first)').each(function() 
			{
				var MSName=$(this).find("select").val();
				if(MSName=='')
				{
					alert('Please Select Target Microsite');
				}								
			});
			
			$('.LP_MicroSite > tbody  > tr:not(:first)').each(function() 
			{
				var LPId=$(this).find('.LPId').val();	
				var MSID=$(this).find("select").val();								
				var LPName=$(this).find('.LPName').text();
				// alert(LPId);
				var obj = {"LPID":LPId, "LPname":LPName,"MSID":MSID};
				JSONFormed.push(obj);
			});
			
			if(JSONFormed)
			{
				// console.log(micrositesList);
				$.ajax(
				{
					type: 'post',
					url: '<?php echo base_url();?>package_info/Save_MicrositeJSON_LP',
					data: {packageId: package_id,JSONFormed:JSONFormed,micrositesList:micrositesList},
					dataType:'json',
					beforeSend: function()
					{									
					},
					success: function(data)
					{
						alert('Target Microsite is attached to Landing Page');
					},
					error: function() 
					{
						$('.MSSaved').show();
						// alert('Microsite is saved successfully');
						$('.assignMicrositeSave').attr('disabled',true);
					}
				});
			}		
		});
		
		//Ajax Call for Display Duplicates found in target
		$('#DuplicateAssetTab').click(function()
		{
			var package_id = $('.package_id_hidden').val();
			// alert(package_id);
			$.ajax({
				type: 'post',
				url: '<?php echo base_url();?>package_info/targetDuplicatesAssets',
				data: {packageId: package_id},
				dataType:'json',
				beforeSend: function()
				{
					var spinner='';
					$('.tableRow').hide();
					if($('.Assets_Duplicates').find(".glyphicon-refresh-animate"))
					{
						$('.loader').remove();
					}
					spinner +='<div class="loader" style="margin-left: 210px !important;"><span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span></div>';
					$('.Assets_Duplicates').append(spinner);
				},
				success: function(data)
				{
					if(data=='')
					{
						$('.tableRow').remove();
						out+='<tr class="tableRow"><td></td><td style="font-style:italic">No Duplicate Assets found in package</td><td></td></tr>';
						$('.Assets_Duplicates').append(out);
						$('.assignDuplicateAssetSave').hide();
						//return;
					}
					// if($('.Assets_Duplicates').find(".tableRow"))
					// {
						// $('.tableRow').remove();
					// }
					var out='';
					$('.loader').hide();
					
					$.each(data, function (Dupkey , Dupval) 
					{
						// console.log(Dupval.Target_Asset_Id);
						var targetDupAssets = gettargetDuplicateAssetsOptionValue(Dupval.Target_Duplicate_Assets);
						var DupSelect = '<select class="trgtDupList" style="padding: 0 0 0 0;width: 230px !important;height:24px !important;" size="1">';
						DupSelect +='<option></option>';
						$.each(targetDupAssets, function (key , val) 
						{
							if(Dupval.Target_Asset_Id == val.id)	
							{	
								var selected = '';
								selected = 'selected'
								DupSelect += "<option selected value='" + val.id + "'>" + val.name +" ( "+ val.id + " )</option>";
								// $('.assignDuplicateAssetSave').attr('disabled',true);
							}
							else
							{	
								var selected = '';
								selected = 'selected';
								DupSelect += "<option value='" + val.id + "'>" + val.name +" ( "+ val.id + " )</option>";
								// $('.assignDuplicateAssetSave').attr('disabled',false);
							}
							
						});
						var obj_ = $.parseJSON(Dupval.Target_Duplicate_Assets);
						DupSelect += "</select>";
						out += '<tr class="tableRow"><td  >'+obj_.parent_asset_type+'</td> <td >'+obj_.parent_asset_name+'</td><td class="AssetType">'+Dupval.Asset_Type+'<input type=hidden class=DPI_Id value='+Dupval.Deployment_Package_Item_Id+'><input type=hidden class=AssetId value='+Dupval.Asset_Id+'></td><td class="AssetName">'+Dupval.Asset_Name+'</td><td>'+DupSelect+'</td></tr>';
					});	
					$('.Assets_Duplicates').append(out);
				},
				error: function() 
				{
					// alert('error');
				}
			});
		});	
		
		function gettargetDuplicateAssetsOptionValue(targetDupAssets)
		{
			var objTemp = $.parseJSON(targetDupAssets);
			var obj = objTemp.duplicates_from_target;
			var DuplicateArray= [];
			$.each(obj, function (key , val) 
			{	
				var obj = {"id":val.Asset_Id, "name":val.Asset_Name};
				DuplicateArray.push(obj);
			});
			return DuplicateArray;
		}
		
		//save selected asset to db
		$('.assignDuplicateAssetSave').click(function()
		{
			var package_id = $('.package_id_hidden').val();
			var JSONFormed= [];
			var saveStatus=false;
			var selectNotEmpty=0;
			$('.Assets_Duplicates td:nth-child(5)').each(function() 
			{
				var trgtAssetName=$(this).find("select").val();
				if(trgtAssetName.length==0 || trgtAssetName=='')
				{
					$(this).find("select").css('border-color', '#C70039');
					$('.assignDuplicateAssetSave').attr('disabled',true);
				}
				else
				{
					$(this).find("select").css('border-color', '#E5E7E9');
				}
			});
			
			$('.Assets_Duplicates > tbody  > tr:not(:first)').each(function() 
			{
				$('.trgtDupList').on('change', function() 
				{
					$(this).css('border-color', '#E5E7E9');
					enableDisableSaveButton();
				});
					
				var DPI_Id=$(this).find('.DPI_Id').val();	
				var AssetId=$(this).find('.AssetId').val();	
				var AssetType=$(this).find('.AssetType').text();
				var AssetName=$(this).find('.AssetName').text();
				var TargetAssetId=$(this).find("select").val();
				var obj = {"DPI_Id":DPI_Id,"AssetId":AssetId, "AssetType":AssetType,"AssetName":AssetName,"TargetAssetId":TargetAssetId};
				JSONFormed.push(obj);
				saveStatus=true;
			});
			
			function enableDisableSaveButton()
			{
				var allFilled = 1;
				$('.Assets_Duplicates td:nth-child(5)').each(function() 
				{
					var trgtAssetName=$(this).find("select").val();
					if(trgtAssetName.length==0 || trgtAssetName=='')
					{
						allFilled = 0;
						$('.assignDuplicateAssetSave').attr('disabled',true);
					}
					else
					{
						$(this).find("select").css('border-color', '#E5E7E9');
					}
				});
				if(allFilled==0)
				{
					$('.assignDuplicateAssetSave').attr('disabled',true);
				}
				else
				{
					$('.assignDuplicateAssetSave').attr('disabled',false);
				}
			}
			
			$('.Assets_Duplicates td:nth-child(5)').each(function() 
			{
				var trgtAssetName=$(this).find("select").val();
				if(trgtAssetName.length!=0 || trgtAssetName!='')
				{
					saveAssignedDuplicateAsset();
				}
			});	
			
			// function to save target asset id of duplicate asset in to db
			function saveAssignedDuplicateAsset() 
			{	
				$.ajax(
				{
					type: 'post',
					url: '<?php echo base_url();?>package_info/Save_SelectedTargetAssetId',
					data: {packageId: package_id,JSONFormed:JSONFormed},
					dataType:'json',
					beforeSend: function()
					{									
					},
					success: function(data)
					{
						// alert('saved');
					},
					error: function() 
					{
						$('.DuplicateSaved').show();
						// alert('Microsite is saved successfully');
						// $('.assignDuplicateAssetSave').attr('disabled',true);
					}
				});
			}
		});
		
		
		
	});	
	</script>
	
	<!-- JAVASCRIPT FILES -->
	<script type="text/javascript">var plugin_path = 'assets/plugins/';</script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/app.js"></script>
	<script type="text/javascript">
	loadScript(plugin_path + "nestable/jquery.nestable.js", function()
	{
		if(jQuery().nestable) {
			var updateOutput = function (e) {
				var list = e.length ? e : $(e.target),
					output = list.data('output');
					
				if (window.JSON) {
					//output.val(window.JSON.stringify(list.nestable('serialize'))); //, null, 2));
				} else {
					//output.val('JSON browser support required for this demo.');
				}
			};

			// Nestable list 1
			jQuery('#nestable_list_1').nestable({
				group: 1
			}).on('change', updateOutput);
			// Nestable list 1
			jQuery('#nestable_list_2').nestable({
				group: 1
			}).on('change', updateOutput);


			// output initial serialised data
			updateOutput(jQuery('#nestable_list_1').data('output', jQuery('#nestable_list_1_output')));
			updateOutput(jQuery('#nestable_list_2').data('output', jQuery('#nestable_list_2_output')));
			
			// Expand All
			jQuery("button[data-action=expand-all]").bind("click", function() {
				jQuery('.dd').nestable('expandAll');
			});

			// Collapse All
			jQuery("button[data-action=collapse-all]").bind("click", function() {
				jQuery('.dd').nestable('collapseAll');
			});
		}
	});
	
	function addtopackage(package_id,Asset_Type,Asset_Id,Asset_Name,btn)
	{	
		
		// if(Asset_Type=='ContactSegment')
		// {
			// Asset_Type='Segment';
		// }
		// else if(Asset_Type=='LandingPage')
		// {
			// Asset_Type='Landing Page';
		// }		
		var tempdata= {
				Deployment_Package_Id: package_id,
				Asset_Type: Asset_Type,
				Asset_Id: Asset_Id,
				Asset_Name: Asset_Name,
				verified:1,
				//Status: 'Valid'
		};
		var destination = $('.target_system').val();
		if(destination != "" && destination != "NULL")
		{
			$.ajax(
			{
				type: 'post',
				url: '<?php echo base_url();?>package_info/addtopackage',
				data: tempdata,
				beforeSend: function()
				{
					$(btn).css('color','#C4C4C6');
				},
				success: function(data)
				{   
					getPackageinfo();
					// $('#orgID1').prop('readonly', true);
					// $('#orgID2').prop('readonly', true);
					$('#orgID1').prop('disabled', true);
					$('#orgID2').prop('disabled', true);
					$('.additem_').css('color','#46b8da');
					$('.ItemAddSuccessMessage').show().delay(2000).fadeOut(3000);
					
				},
				error: function()
				{
					result = false;
				}
			});
		}
		else
		{
			alert("Please select Target System first");
			var val = Asset_Id;
			$('input:checkbox[value="' + val + '"]').attr('checked', false);
		}
	 
	}
	
	function additem()
	{
		$('.additem').off('click');
		$('.additem').click(function()
		{
			if (this.checked) 
			{
				var package_id = $('.package_id_hidden').val();
				var Asset_Type = $(this).parent().parent().parent().find('input[name="RSys_Asset_Type_Id"]').val();
				if(Asset_Type=='' || Asset_Type == null || Asset_Type == 'undefined')
				{
					Asset_Type = $(this).parent().find('input[name="Asset_Type"]').val();
				}
				var Asset_Id = $(this).val();
				var Asset_Name = $(this).parent().find('.asset_item').html();
				addtopackage(package_id, Asset_Type, Asset_Id, Asset_Name);
				$('.ValidatePackage').removeAttr("disabled");
				$('.DeployPackage').removeAttr("disabled");
				$('#gridWrapper').show();
			}
		});
	}
	
	function additem_new()
	{
		//$('.additem').off('click');
		$('.additem_').click(function()
		{
			var package_id = $('.package_id_hidden').val();
			var Asset_Type = $(this).parent().parent().parent().parent().find('input[name="RSys_Asset_Type_Id"]').val();
			if(Asset_Type=='' || Asset_Type == null || Asset_Type == 'undefined')
			{
				Asset_Type = $(this).parent().find('input[name="Asset_Type"]').val();
				
			}
			if(Asset_Type=='' || Asset_Type == null || Asset_Type == 'undefined')
			{
				Asset_Type = $(this).parent().parent().find('input[name="Asset_Type"]').val();
				
			}
			Asset_Id = $(this).parent().parent().find('#AssetId').val();
			var Asset_Name = $(this).parent().parent().find('.asset_item').html();
			if(package_id !='undefined' && Asset_Type !='undefined'&& Asset_Id !='undefined' && Asset_Name !='undefined')  
			{
				addtopackage(package_id, Asset_Type, Asset_Id, Asset_Name ,this);
			}	
			
			$('.ValidatePackage').removeAttr("disabled");
			$('.DeployPackage').removeAttr("disabled");
			$('#gridWrapper').show();
			
		});
	}
	
	$('.edit').click(function(){
		$(this).hide();
		$('#Package_name_label').hide();
		$('#package_name_id').show();
		//$('#package_description').show();
	});
	
	$('.editNew').click(function(){
		$(this).hide();
		$('#package_description_label').hide();
		$('#package_description').show();
	});
	
	var statusMissing = $('.status_cursor').text();
	$('.missingAssetsMessage').hide()
	if(statusMissing== 'Missing Assets')
	{
		$('.missingAssetsMessage').show();
	}
	
	var statusDplyCmpltd = $('.status_cursor').text();
	if(statusDplyCmpltd == 'Deploy Completed' || status == 'Deployment In Queue')
	{
		$(".ValidatePackage").attr('disabled',true);
		$(".DeployPackage").attr('disabled',true);
		var statusDupFound = $('.status_cursor').text();
		// $(".additem_").css("cursor","default");
	}
	
	var statusDplyVldtQueue = $('.status_cursor').text();
	if(statusDplyVldtQueue == 'Validate In Queue' || statusDplyVldtQueue == 'Deployment In Queue')
	{
		$("#jqgrid").prop('disabled',true);
		$("#delBtn").attr('disabled',true);
		$(".ValidatePackage").attr('disabled',true);
		$(".DeployPackage").attr('disabled',true);
		$(".ValidationReport").attr('disabled',true);
		
	}
	
	var newButtonstatus = $('.status_cursor').text();
	if(newButtonstatus == 'New')
	{	
		//VIkas : 29082017
		//$(".DeployPackage").attr('disabled',true);
		$(".ValidationReport").attr('disabled',true);
	}
	
	var status_unsupported = $('.status_cursor').text();
	if(status_unsupported == 'Unsupported')
	{	
		//VIkas : 29082017
		//$(".DeployPackage").attr('disabled',true);
	}
	
	var status_Errored = $('.status_cursor').text();
	if(status_Errored == 'Errored')
	{	
		$(".DeployPackage").attr('disabled',true);
		$(".ValidatePackage").attr('disabled',true);
	}
	
	$('.dependent_child').click(function ()
	{	
		if($(this).attr('aria-expanded') == "false")
		{
			$(this).find(".fa-plus-square-o").addClass("fa-minus-square-o").removeClass("fa-plus-square-o");
		}
		else
		{
			$(this).find(".fa-minus-square-o").addClass("fa-plus-square-o").removeClass("fa-minus-square-o");
		}
	});
	
	$('.dependent_invalid_child').click(function ()
	{	
		if($(this).attr('aria-expanded') == "false")
		{
			$(this).find(".fa-plus-square-o").addClass("fa-minus-square-o").removeClass("fa-plus-square-o");
		}
		else
		{
			$(this).find(".fa-minus-square-o").addClass("fa-plus-square-o").removeClass("fa-minus-square-o");
		}
	});
	
	function getChildInfo(datavalue)
	{
		var package_id = $('.package_id_hidden').val();
		var packageList= $('.dependent_child_header');
		$.ajax(
		{
			type: 'post',
			url: 'package_info/getchildinfo',
			data: datavalue,
			dataType:'json',
			beforeSend: function()
			{
				//alert(this);
				$('.childList').html('');
				$('.dependent_childInfo .childValue').hide();				
				var child_asset_type = $('.dependent_child').parent().find(this).show();packageList.html('<div class="list-group-item " style="text-align: center;" ><div class="dependent_child_refresh glyphicon glyphicon-refresh glyphicon-refresh-animate"></div></div>');
			},
			success: function(data)
			{
				
		
				packageList.html('');
				$('.ValidatePackage span').remove();
				$('.dependent_child_refresh').remove();
				$('.childList').html('');
				if(data.length == "" || data.length < 0 || data == false)
				{
					packageList.html('No child assets found');
				}
				else
				{
					var childIdList = [];					
					$(data).each(function(){	
						var flag=true;
						var out='';
						var dataAsset_Id=this.Asset_Id;
						var dataAsset_Type=this.Asset_Type;
						$(childIdList).each(function(){
							if(dataAsset_Id == this.Asset_Id && dataAsset_Type == this.Asset_Type ){
								flag = false;
							}
						});
						if(flag){							
							childIdList.push({Asset_Id:this.Asset_Id,Asset_Type:this.Asset_Type});
							var child_asset_type = $('.dependent_child').parent().find('#child_asset_type'+this.Asset_Type.replace(" ","_"));
							
							$('.dependent_childInfo .childValue'+this.Asset_Type.replace(" ","_")).show();
							// out += 	'<div class="list-group-item "><input name="Asset_Type"  type="hidden" value="'+this.Asset_Type+'"/><input class="additem"  type="checkbox" name="chkItem" value="'+this.Asset_Id+'"/><a href="#id_'+ this.Asset_Id +'" class="asset_item" data-toggle="collapse" >'+this.Display_Name +'</a></div>';
							var newButtonstatus = $('.status_cursor').text();
							if(newButtonstatus == 'Deploy Completed' || newButtonstatus == 'Deployment In Queue' || newButtonstatus == 'Validate In Queue' || newButtonstatus == 'Errored')
							{
								out += 	'<div class="list-group-item "><input name="Asset_Type"  type="hidden" value="'+this.Asset_Type+'"/><input id="AssetId" name="Asset_Type"  type="hidden" value="'+this.Asset_Id+'"/><a><i class="fa fa-plus-square additem_" style="color: #46b8da;display:none;" aria-hidden="true" title="Click to add asset"></i></a><a href="#id_'+ this.Asset_Id +'" class="asset_item" data-toggle="collapse" style="cursor:default;">'+this.Display_Name +'</a></div>';
								child_asset_type.append(out);
							}
							else
							{
								out += 	'<div class="list-group-item "><input name="Asset_Type"  type="hidden" value="'+this.Asset_Type+'"/><input id="AssetId" name="Asset_Type"  type="hidden" value="'+this.Asset_Id+'"/><a><i class="fa fa-plus-square additem_" style="color: #46b8da;" aria-hidden="true" title="Click to add asset"></i></a><a href="#id_'+ this.Asset_Id +'" class="asset_item" data-toggle="collapse" style="cursor:default;">'+this.Display_Name +'</a></div>';
								child_asset_type.append(out);
							}
							
						}
					});
					additem_new();
				}
				var statusDupFound = $('.status_cursor').text();
				if(statusDupFound == 'Deploy Completed')
				{	
					$(".additem_").css("cursor","default");
					$(".additem_").removeAttr("title");
				}	
			},
			error: function() 
			{
				//packageList.html('');	
				packageList.html('Connection Timeout Error');
			}
		}); 
	}
	
	function getMissingAsset()
	{
		var package_id = $('.package_id_hidden').val();
		
		var packageList= $('.dependent_child_header');
		$.ajax(
		{
			type: 'post',
			url: 'package_info/getAssetReport',
			data: {Package_Id : package_id},
			dataType:'json',
			beforeSend: function()
			{
				//alert(this);
				$('.childList').html('');
				$('.dependent_childInfo .childValue').hide();				
				var child_asset_type = $('.dependent_child').parent().find(this).show();packageList.html('<div class="list-group-item " style="text-align: center;" ><div class="dependent_child_refresh glyphicon glyphicon-refresh glyphicon-refresh-animate"></div></div>');
			},
			success: function(data)
			{
				packageList.html('');
				$('.ValidatePackage span').remove();
				$('.dependent_child_refresh').remove();
				$('.childList').html('');
				
				
				if(data.length == "" || data.length < 0 || data == false)
				{
					packageList.html('No child assets found');
				}
				else
				{
					var childIdList = [];	
					var childIdList_missing = [];						
					$(data).each(function(){
						var flag=true;
						var out='';
						var dataAsset_Id=this.Asset_Id;
						var dataAsset_Type=this.Asset_Type;
						if(this.missing_target == 1)
						{
							
							$(childIdList_missing).each(function(){
								if(dataAsset_Id == this.Asset_Id && dataAsset_Type == this.Asset_Type ){
									flag = false;
								}
							});
							if(flag){	
  							
								childIdList_missing.push({Asset_Id:this.Asset_Id,Asset_Type:this.Asset_Type});
								var child_asset_type = $('.dependent_child').parent().find('#Missing_child_asset_type'+this.Asset_Type.replace(" ","_"));
								
								
								$('.dependent_childInfo .childValue'+this.Asset_Type.replace(" ","_")).show();
								// out += 	'<div class="list-group-item " ><input name="Asset_Type"  type="hidden" value="'+this.Asset_Type+'"/><input class="additem"  type="checkbox" name="chkItem" value="'+this.Asset_Id+'"/><a href="#id_'+ this.Asset_Id +'" class="asset_item" data-toggle="collapse" >'+this.Asset_Name +'</a></div>';
							
								var newButtonstatus = $('.status_cursor').text();
								if(newButtonstatus == 'Deploy Completed' || newButtonstatus == 'Deployment In Queue' || newButtonstatus == 'Validate In Queue' || newButtonstatus == 'Errored')
								{
									out += 	'<div class="list-group-item "><input name="Asset_Type"  type="hidden" value="'+this.Asset_Type+'"/><input id="AssetId" name="Asset_Type"  type="hidden" value="'+this.Asset_Id+'"/><a><i class="fa fa-plus-square additem_" style="color: #46b8da;display:none" aria-hidden="true" title="Click to add asset"></i></a><a href="#id_'+ this.Asset_Id +'" class="asset_item" data-toggle="collapse" style="cursor:default;   color: black;">'+this.Asset_Name +'</a></div>';
								}
								else
								{
									out += 	'<div class="list-group-item "><input name="Asset_Type"  type="hidden" value="'+this.Asset_Type+'"/><input id="AssetId" name="Asset_Type"  type="hidden" value="'+this.Asset_Id+'"/><a><i class="fa fa-plus-square additem_" style="color: #46b8da;" aria-hidden="true" title="Click to add asset"></i></a><a href="#id_'+ this.Asset_Id +'" class="asset_item" data-toggle="collapse" style="cursor:default;   color: black;">'+this.Asset_Name +'</a></div>';
								}
								
								
								child_asset_type.append(out);
							
							}
							
						}
						else if(this.missing_target == 0)
						{
							$(childIdList).each(function(){
								if(dataAsset_Id == this.Asset_Id && dataAsset_Type == this.Asset_Type ){
									flag = false;
								}
							});
							if(flag){							
								childIdList.push({Asset_Id:this.Asset_Id,Asset_Type:this.Asset_Type});
								var child_asset_type = $('.dependent_child').parent().find('#Existing_child_asset_type'+this.Asset_Type.replace(" ","_"));
								$('.dependent_childInfo .childValue'+this.Asset_Type.replace(" ","_")).show();
								// out += 	'<div class="list-group-item " ><input name="Asset_Type"  type="hidden" value="'+this.Asset_Type+'"/><input class="additem"  type="checkbox" name="chkItem" value="'+this.Asset_Id+'"/><a href="#id_'+ this.Asset_Id +'" class="asset_item" data-toggle="collapse" >'+this.Asset_Name +'</a></div>';
								var newButtonstatus = $('.status_cursor').text();
								if(newButtonstatus == 'Deploy Completed' || newButtonstatus == 'Deployment In Queue' || newButtonstatus == 'Validate In Queue' || newButtonstatus == 'Errored')
								{
									out += 	'<div class="list-group-item "><input name="Asset_Type"  type="hidden" value="'+this.Asset_Type+'"/><input id="AssetId" name="Asset_Type"  type="hidden" value="'+this.Asset_Id+'"/><a><i class="fa fa-plus-square additem_" style="color: #46b8da;display:none;" aria-hidden="true" title="Click to add asset"></i></a><a href="#id_'+ this.Asset_Id +'" class="asset_item" data-toggle="collapse" style="cursor:default;">'+this.Asset_Name +'</a></div>';
								}
								else
								{
									out += 	'<div class="list-group-item "><input name="Asset_Type"  type="hidden" value="'+this.Asset_Type+'"/><input id="AssetId" name="Asset_Type"  type="hidden" value="'+this.Asset_Id+'"/><a><i class="fa fa-plus-square additem_" style="color: #46b8da;" aria-hidden="true" title="Click to add asset"></i></a><a href="#id_'+ this.Asset_Id +'" class="asset_item" data-toggle="collapse" style="cursor:default;">'+this.Asset_Name +'</a></div>';
								}
								child_asset_type.append(out);
								
							}
						}
					});
					
					$('.v_report').each(function(){
						if(!($(this).find('.list-group-item').length))
						{
							$(this).parent().hide();
						}
					});
					
					additem_new();
				}
				var statusDplyCmpltd = $('.status_cursor').text();
				if(statusDplyCmpltd == 'Deploy Completed')
				{
					$(".additem_").css("cursor","default");
					$(".additem_").removeAttr("title");
				}
			},
			error: function() 
			{
				//packageList.html('');	
				packageList.html('Connection Timeout Error');
			}
		}); 
	}
	
	function getInvalidAsset(datavalue){
		var package_id = $('.package_id_hidden').val();
		var assetList= $('.invalid_child_assets');
		$.ajax(
		{
			type: 'post',
			url: 'package_info/getinvalidAssets',
			data: datavalue,
			dataType:'json',
			beforeSend: function()
			{
				assetList.html('<div class="list-group-item " style="text-align: center;" ><div class="dependent_child_refresh glyphicon glyphicon-refresh glyphicon-refresh-animate"></div></div>');
			},
			success: function(data)
			{
				assetList.html('');
				$('.dependent_child_refresh').remove();
				if(data.length == "" || data.length < 0 || data == false)
				{
					assetList.html('No Invalid assets found');
				}
				else
				{
					var childIdList = [];					
					assetList.append("Assets not found in target System or in the Deployment package");
					$(data).each(function(){	
						var flag=true;
						var out='';
						var dataAsset_Id=this.Asset_Id;
						var dataAsset_Type=this.Asset_Type;
						$(childIdList).each(function(){
							if(dataAsset_Id == this.Asset_Id && dataAsset_Type == this.Asset_Type ){
								flag = false;
							}
						});
						
						if(flag){							
							// out += '<div class="list-group-item "><label class="invalidAsset" style="color:#337ab7;"><!--span class="glyphicon glyphicon-ok"></span> '+this.Asset_Name+' </label></div>';
							out += '<div class="list-group-item "><label class="invalidAsset" style="color:#337ab7;">'+this.Asset_Type+' - '+this.Asset_Name+' </label></div>';
							assetList.append(out);
						}
					});
				}
				$(".invalidAsset").attr('title', 'Add these assets in Deployment Package');
			},
			error: function() 
			{
				assetList.html('Connection Timeout Error');
			}
		});
	
	}
	
	$('.ValidatePackage').click(function()
	{
		$('.validate_package').show();
		$('.deleteBtn').attr("disabled",true);
		$('.viewChildBtn').attr("disabled",true);
	});
	
	$('.ValidatePackage_Auto').click(function ()
	{
		var package_id = $('.package_id_hidden').val();
		var datavalue = {PackageId : package_id, Auto : 1};
		
		$.ajax(
		{
			type: 'post',
			// url: 'package_info/validatepackage',
			url: '<?php echo base_url();?>deploy/validationinqueue',
			data: datavalue,
			dataType:'json',
			beforeSend: function()
			{
				
				$('.ValidatePackage span').remove();
				$('.ValidatePackage').append(' <span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span>');
				$('.ValidatePackage').prop("disabled",'true');
				$('.ValidatePackage').off('click');
				//pageReload();
			},
			success: function(data1)
			{
				$('.ValidatePackage span').remove();
				$('.footer_deploy_button').after('<p style="margin-bottom: 2px;">This Package Validation is in queue.</p>');
				location.reload(1);
				// getPackageinfo();
			},
			error: function() 
			{
				// alert('fvdf error');
				location.reload(1);
				$('.ValidatePackage span').remove();
			}
		});
			
	});
	
	$('.ValidatePackage_Manual').click(function ()
	{
		var package_id = $('.package_id_hidden').val();
		var datavalue = {PackageId : package_id, Auto : 0};
		
		$.ajax(
		{
			type: 'post',
			// url: 'package_info/validatepackage',
			url: '<?php echo base_url();?>deploy/validationinqueue',
			data: datavalue,
			dataType:'json',
			beforeSend: function()
			{
				
				$('.ValidatePackage span').remove();
				$('.ValidatePackage').append(' <span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span>');
				$('.ValidatePackage').prop("disabled",'true');
				$('.ValidatePackage').off('click');
				//pageReload();
			},
			success: function(data1)
			{
				$('.ValidatePackage span').remove();
				$('.footer_deploy_button').after('<p style="margin-bottom: 2px;">This Package Validation is in queue.</p>');
				location.reload(1);
				// getPackageinfo();
			},
			error: function() 
			{
				// alert('fvdf error');
				location.reload(1);
				$('.ValidatePackage span').remove();
			}
		});
			
	});
</script>
	
	<script>
	
	$(document).ready(function ()
	{
		function reload()
		{
			var status = $('.status_cursor').text();
			//console.log(status);
			if(status == 'Validate In Queue' || status == 'Deployment In Queue')
			{				
				setTimeout(function () 
				{ 
					location.reload(1); 
					// $('.status_cursor_div').load('.status_cursor_div');
				}, 10000);
			}
		}
		 reload();
		 
		 loadScript("assets/plugins/" + "jqgrid/js/i18n/grid.locale-en.js", function()
		{
			loadScript("assets/plugins/" + "jqgrid/js/jquery.jqGrid.js", function()
			{
				loadScript("https://appcloud-dev.portqii.com/transporter2.0/assets/plugins/" + "jqgrid/js/jquery.fmatter.js", function() 
				{
					loadScript("assets/plugins/" + "bootstrap.datepicker/js/bootstrap-datepicker.min.js", function()
					{
						getPackageinfo();
					});
				});
			});
		});
	});	
	</script>
<!-- PAGE LEVEL SCRIPTS  jqGrid-->
	<script type="text/javascript">
			
	function get_jqgrid(jqgrid_data)
	{		
		jQuery("#jqgrid").jqGrid({
			data : jqgrid_data,
			datatype : "local",
			height : '200',
			// caption : "Assets Included in Deployment Package",
			caption : "ASSETS INCLUDED IN DEPLOYMENT PACKAGE",
			colNames : ['Asset Id', 'Asset Type', 'Asset Name', 'Status','Actions'],
			colModel : 
			[
				{ name : 'Asset_Id' ,width : 60,key: true}, 
				{ name : 'Asset_Type' ,width : 160, editable : false}, 
				{ name : 'Asset_Name' ,width : 260, editable : false }, 
				{ name : 'Status',width : 160, sortable : false, editable : false, align: 'center', formatter: statusformatter},
				{ name : 'act',width : 160,sortable:false,formatter: actformatter,title : false }, 
			],
			rowNum : 10,
			rowList : [10, 20, 30],
			pager : '#pager_jqgrid',
			sortname : 'id',
			toolbarfilter: false,
			viewrecords : true,
			sortorder : "asc",
			gridComplete: function()
			{
				var recs = parseInt($("#jqgrid").getGridParam("records"),10);
				if (isNaN(recs) || recs == 0) {
					$("#gridWrapper").hide();
					$(".footer_deploy_button").hide();
				}
				else {
					$('#gridWrapper').show();
					$(".footer_deploy_button").show();
					eventList();
					//alert('records > 0');
				}				
			},
			multiselect : false,
			autowidth : false,
		});			
		jQuery("#jqgrid").jqGrid('navGrid', "#pager_jqgrid", {
			edit : false,
			add : false,
			del : false,
			search: false,
			refresh: false,
		});
		jQuery("a.get_selected_ids").bind("click", function() {
			s = jQuery("#jqgrid").jqGrid('getGridParam', 'selarrrow');
			// alert(s);
		});

		// Select/Unselect specific Row by id
		jQuery("a.select_unselect_row").bind("click", function() {
			jQuery("#jqgrid").jqGrid('setSelection', "13");
		});

		
		function actformatter(cellvalue, options, rowObject)
		{
			var statusDupFound = $('.status_cursor').text();
			//if(statusDupFound=='Validate In Queue')
			// alert(rowObject.Status)
			if(rowObject.Status == "Deploy Completed" || rowObject.Status == "Errored" || statusDupFound=='Validate In Queue'
			|| statusDupFound=='Deployment In Queue' || statusDupFound=="Errored")
			{
				// alert("Deploy Completed");
				var act = '<input type="hidden" name="component_id" value="'+rowObject.Deployment_Package_Item_Id+'"/>'+
				'<input type="hidden" name="changeset_id" value="'+rowObject.Deployment_Package_Id+'"/>'+
				'<button role="button" title="Delete" style="margin-right: 5px;" ondblclick="document.getElementById(\'form'+this.Deployment_Package_Item_Id+'\').submit();" type="submit" class="btn btn-danger btn-xs deleteBtn delete_package" id ="delBtn" data-title="Delete" data-toggle="modal" data-target="#delete" disabled >Delete</button>'+
				'<input type="hidden" name="Asset_Type" value="'+rowObject.Asset_Type+'"/>'+
				'<input type="hidden" name="Asset_Id" value="'+rowObject.Asset_Id+'"/>'+
				'<button class="btn btn-sm btn-info viewChildBtn ViewChild" title="View Child" disabled>View Child </button>';
				return act;
			}
			else if(statusDupFound == 'Deploy Completed')
			{
				var act = '<input type="hidden" name="component_id" value="'+rowObject.Deployment_Package_Item_Id+'"/>'+
				'<input type="hidden" name="changeset_id" value="'+rowObject.Deployment_Package_Id+'"/>'+
				'<button role="button" title="Delete" style="margin-right: 5px;" ondblclick="document.getElementById(\'form'+this.Deployment_Package_Item_Id+'\').submit();" type="submit" class="btn btn-danger btn-xs deleteBtn delete_package" id ="delBtn" data-title="Delete" data-toggle="modal" data-target="#delete" disabled >Delete</button>'+
				'<input type="hidden" name="Asset_Type" value="'+rowObject.Asset_Type+'"/>'+
				'<input type="hidden" name="Asset_Id" value="'+rowObject.Asset_Id+'"/>'+
				'<button class="btn btn-sm btn-info viewChildBtn ViewChild" title="View Child">View Child </button>';
				return act;
			}
			else
			{
				// alert("not Deploy Completed");
				var act = '<form id="form'+rowObject.Deployment_Package_Item_Id+'" role="form" action="package_info/deletecomponent" method="post" onSubmit="if(!confirm(\'Are you sure you want to delete this from Packages?\')){return false;}" style="float: left;">'+		
					'<input type="hidden" name="component_id" value="'+rowObject.Deployment_Package_Item_Id+'"/>'+
					'<input type="hidden" name="changeset_id" value="'+rowObject.Deployment_Package_Id+'"/>'+
					'<button role="button" title="Delete" style="margin-right: 5px;" ondblclick="document.getElementById(\'form'+this.Deployment_Package_Item_Id+'\').submit();" type="submit" class="btn btn-danger btn-xs deleteBtn delete_package" id ="delBtn" data-title="Delete" data-toggle="modal" data-target="#delete" >Delete</button>'+
					'<input type="hidden" name="Asset_Type" value="'+rowObject.Asset_Type+'"/>'+
					'<input type="hidden" name="Asset_Id" value="'+rowObject.Asset_Id+'"/>'+
					'</form>'+
					'<button class="btn btn-sm btn-info viewChildBtn ViewChild " title="View Child">View Child </button>';
				return act;
			}
		}		
		function statusformatter(cellvalue, options, rowObject)
		{
			if (cellvalue == "Valid")
				return "<a class='btn btn-xs btn-warning validBtn btn-quick' style='cursor:default'>Valid</a>";
			else if(cellvalue == "Invalid")
				return '<input type="hidden" name="changeset_id" value="'+rowObject.Deployment_Package_Id+'"/>'+'<input type="hidden" name="component_id" value="'+rowObject.Deployment_Package_Item_Id+'"/>'+"<a class='btn btn-xs btn-danger btn-quick viewInvalidAsset'>"+cellvalue+"</a>";
			else if(cellvalue == "Deploy Completed")
				return "<a class='btn btn-xs btn-success btn-quick' >"+cellvalue+"</a>";
			else if(cellvalue == "Completed")
				return "<a class='btn btn-xs btn-success unsprtCpltd btn-quick' style='cursor:default'>"+cellvalue+"</a>";
			else if(cellvalue == "Errored")
				return "<a class='btn btn-xs btn-danger unsprtErr btn-quick' style='cursor:default'>"+cellvalue+"</a>";
			else if(cellvalue == "Unsupported")
				return "<a class='btn btn-xs btn-danger unsprtBtn btn-quick Unsupported' value='"+rowObject.Unsupported_Asset_Message+"'>"+cellvalue+"</a>";
			else if(cellvalue == "Duplicate")
				return "<a class='btn btn-xs btn-danger unsprtdup btn-quick Duplicate' style='padding-left:5px!important;cursor:default;'>"+cellvalue+"</a>";
				// return "<a class='btn btn-xs btn-danger unsprtdup btn-quick Duplicate' style='padding-left:5px!important;cursor:default;'>Duplicate</a>";
			else 
				return "<a class='btn btn-xs btn-info newBtn btn-quick' style='cursor:default'>"+cellvalue+"</a>";
		}
		
		// On Resize
		jQuery(window).resize(function() {

			if(window.afterResize) {
				clearTimeout(window.afterResize);
			}
			window.afterResize = setTimeout(function() {

				/**
					After Resize Code
					.................
				**/

				jQuery("#jqgrid").jqGrid('setGridWidth', jQuery("#middle").width() - 32);

			}, 500);

		});
		/**
			@STYLING
		**/
		jQuery(".ui-jqgrid").removeClass("ui-widget ui-widget-content");
		jQuery(".ui-jqgrid-view").children().removeClass("ui-widget-header ui-state-default");
		jQuery(".ui-jqgrid-labels, .ui-search-toolbar").children().removeClass("ui-state-default ui-th-column ui-th-ltr");
		jQuery(".ui-jqgrid-pager").removeClass("ui-state-default");
		jQuery(".ui-jqgrid").removeClass("ui-widget-content");

		jQuery(".ui-jqgrid-htable").addClass("table table-bordered table-hover");
		jQuery(".ui-pg-div").removeClass().addClass("btn btn-sm btn-primary");
		
		jQuery( ".ui-icon.ui-icon-seek-prev" ).wrap( "<div class='btn btn-sm btn-default'></div>" );
		jQuery(".ui-icon.ui-icon-seek-prev").removeClass().addClass("fa fa-backward");

		jQuery( ".ui-icon.ui-icon-seek-first" ).wrap( "<div class='btn btn-sm btn-default'></div>" );
		jQuery(".ui-icon.ui-icon-seek-first").removeClass().addClass("fa fa-fast-backward");		  	

		jQuery( ".ui-icon.ui-icon-seek-next" ).wrap( "<div class='btn btn-sm btn-default'></div>" );
		jQuery(".ui-icon.ui-icon-seek-next").removeClass().addClass("fa fa-forward");

		jQuery( ".ui-icon.ui-icon-seek-end" ).wrap( "<div class='btn btn-sm btn-default'></div>" );
		jQuery(".ui-icon.ui-icon-seek-end").removeClass().addClass("fa fa-fast-forward");
		jQuery(".ui-jqgrid-caption").css("position","static");
	}	
	
	function getPackageinfo()
	{
		var package_id = $('.package_id_hidden').val();
		// alert(package_id);
		$.ajax(
		{
			type: 'post',
			url: '<?php echo base_url();?>package_info/getpackageinfo',
			data: {packageId: package_id},
			dataType:'json',
			beforeSend: function()
			{
				// $('.jqgrid_div').html('<span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span>')
			},
			success: function(data)
			{
				get_jqgrid(data);
				var $mygrid =  $("#jqgrid");
				allGridParams = $mygrid.jqGrid("getGridParam");
				//allGridParams = $("#jqgrid").jqGrid("getGridParam", "data")
				allGridParams.data = data;
				$mygrid.trigger("reloadGrid", [{current: true}]);
				
				if(data != ""){
					$('.selectSys , .selectSys *').attr("disabled", true);
					$('.selectSys2 , .selectSys2 *').attr("disabled", true);
				} 
				
			},
			error: function() 
			{
			
			}
		});
	}
	</script>