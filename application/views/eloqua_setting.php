<style>
	.profileDiv{
		margin-top: 35px ;
	}
	.saveButton{
		padding: 1px;
		text-align: center;
	}
	.tableHeight{
	   line-height: 15px;
	   min-height: 15px;
	   height: 15px;
	}
	.profileDiv{
		margin-top: 35px ;
	}
	.specialHand{ 
		cursor: pointer; cursor: hand; 
	}
	.saveButton{
		padding: 1px;
		text-align: center;
	}
	.lead{
		font-size: 18px !important;
	}
	.font, panel-title{
		font-size: 12px !important;
	}
	.btn{
		padding: 2px 12px !important;
		border-radius: 1px !important;
		border-width: 1px !important;
	}
	.form-group{
		padding: 0px !important;
	}
	.panel-heading{
		padding-top: 4px !important;
		padding-bottom: 4px !important;
	}
	.mul-two{
		border: 1px solid grey;
		margin: 10px;
		margin-left:20px;
		border-radius: 15px
	}
	@media (min-width: 992px)
	{
		.col-md-6 {
			width: 46% !important;
		}
	}
	
	.glyphicon-refresh-animate 
	{
		-animation: spin .7s infinite linear;
		-webkit-animation: spin2 .7s infinite linear;
	}

	@-webkit-keyframes spin2 {
		from { -webkit-transform: rotate(0deg);}
		to { -webkit-transform: rotate(360deg);}
	}

	@keyframes spin {
		from { transform: scale(1) rotate(0deg);}
		to { transform: scale(1) rotate(360deg);}
	}
	
	th {
		white-space: nowrap;
	}
	
	table td 
	{
	  table-layout:fixed;
	  width:20px;
	  overflow:hidden;
	  word-wrap:break-word;
	}
	
	.form-control{
		height: 25px !important;
		font-size:12px !important;
		border-radius: 1px !important;
		border: #ddd 1px solid !important;
	}
	
	#assetMappingTable_paginate{
		border: 1px solid #ebeaea;
		background: #f9f9f9;
		border-radius: 5px;
		box-sizing: border-box !important;
		margin-top: 5px!important;
		padding-top: 0px !important;
	}
	#assetMappingTable_info{
		margin-top: -35px;
	}
	#permittedUsersTable_info{
		margin-top: -30px;
	}
	.mark , mark{
		background-color: #ff0 !important;
	}
	#assetMappingTable_filter .form-control
	{
		margin-top: 6px;
	}
	div.dataTables_filter label
	{
		font-weight: bold !important;
	}
	div.dataTables_filter input{
		float : initial !important;		
	}	
	#assetMappingTable_filter input{
		font-weight: normal !important;
	}
	select {
		border: 1px solid #E5E7E9;
		border-radius: 0px !important;
	}
	#permittedUsersTable_paginate,#eloquaUsersTable_paginate{
		border: 1px solid #ebeaea;
		background: #f9f9f9;
		border-radius: 5px;
		box-sizing: border-box !important;
		margin-top: -13px!important;
		padding-top: 0px !important;
	}
	#permittedUsersTable_filter input{
		font-weight: normal !important;
	}
	div.alert {
		border-left-width: 0px !important;
	}
	
	.switch {
	  position: relative;
	  display: inline-block;
	  width: 60px;
	  height: 24px;
	}

	.switch input {display:none;}

	.slider {
	  position: absolute;
	  cursor: pointer;
	  top: 0;
	  left: 0;
	  right: 0;
	  bottom: 0;
	  background-color: #ccc;
	  -webkit-transition: .4s;
	  transition: .4s;
	}

	.slider:before {
	  position: absolute;
	  content: "";
	  height: 18px;
	  width: 26px;
	  left: 4px;
	  bottom: 3px;
	  background-color: white;
	  -webkit-transition: .4s;
	  transition: .4s;
	}

	input:checked + .slider {
	  background-color: #2196F3;
	}

	input:focus + .slider {
	  box-shadow: 0 0 1px #2196F3;
	}

	input:checked + .slider:before {
	  -webkit-transform: translateX(26px);
	  -ms-transform: translateX(26px);
	  transform: translateX(26px);
	}

</style>

<div id="page-wrapper" style="margin-top:-45px">
	<div class="container-fluid">
		<!-- Page Heading -->
		<div class="row profileDiv">
			<div class="col-lg-8 col-lg-offset-2" style="padding-right: 0px !important; padding-left: 0px !important;">
				<div class="">
					<div class="" >
						<div class="col-xs-12 col-sm-12 col-md-12 "  style="padding:0px">
							<div class="well specialHand" style="padding:5px;" data-toggle="collapse" data-target="#demo"><b style="color:#000000;">EXTERNAL INSTANCES</b><br><font size=2>Here you can add external instances to which you can perform Transporter actions</font size></div>	
							<div id="demo" class="collapse" >
								<div class="" >
									<div class="row font">
										<div style="" class="col-xs-12 col-sm-12 col-md-12 login-box">
											<div class="form-group col-lg-12">
												<div class="panel-group external-instances">
													<div class="">
														<?php 
															//print_r($page_data['acountlist']);exit;
															foreach($page_data['acountlist'] as $key=>$vall)
															{
																// echo '<pre>';
																	// print_r($vall);
																// echo '</pre>';
																echo'<div class = "col-xs-5 col-sm-5 col-md-6 mul-two" ><!--div class="panel-heading " style="border: solid 1px;">
																<div class="panel-title">
																  <a class="font" data-toggle="expand" href="#collapse'.$key.'">'.$vall->Site_Name.'</a>
																</div>
															  </div-->
															  <div id="collapse'.$key.'" class="panel-collapse ">
																<div class="panel-body" style="height:155px">
																	<table class="table table-responsive">
																		<div class="col-lg-8 col-md-8">
																			<tr>
																				<td>Instance Name	</td>
																				<td><b>'.$vall->Site_Name.'</b></td>
																			</tr>
																			<tr>
																				<td>User Name</td>	
																				<td>'.$vall->User_Name.'</td>
																			</tr>
																			<tr>
																				<td>Base URL      </td>
																				<td>'.$vall->Base_Url.' </td>
																			</tr>
																			
																		</div>
																	</table>
																</div>
															  </div>
															  </div>';		
															}
														?>						
													</div>
												</div>
												<?php 
													//echo '<pre>';							//print_r($page_data['acountlist']);//echo '</pre>';
												?>
											</div>							
										</div>
									</div>
								</div>	
							</div>	
						</div>
					<div class="col-xs-12 col-sm-12 col-md-12"  style="margin-top:5px; padding:0;">
						<div class="well specialHand" style="padding:5px" data-toggle="collapse" data-target="#demo1"><b style="color:#000000;">USER PERMISSIONS</b><br><font size=2>Add/Remove the users who has the access to Transporter app</font size></div>	
						
						<div id="demo1" class="collapse" style="font-size: 12px;">	
							<div class="col-md-12" style="margin-bottom:0px; padding:0px;">
								<div class="col-md-12 searchUser" style="padding:0px;">
									<div class="col-md-2" style="padding:0px;">
										<b class="userName">Eloqua Users :</b>
									</div>
									<div class="col-md-3" style="padding:0px;margin-left: -45px;">
										<input onkeydown="if (event.keyCode == 13) document.getElementById('searchTextId').click()" list="selectUser" name="srch-term" id="searchText" placeholder='Enter user name...' type="text" min="3" style="height: 25px;width: 198px;padding-left: 2px; appearance: none" />
										<br>
										<span id="warningDiv" style="display:none;margin-left: 2px;font-size:10px;color:red">User already exists</span>
									</div>
									<div class="col-md-2" style="padding:0px;padding-left: 5px;">		
										<a class="btn btn-default btn-sm searchText pull-center" id ="searchTextId" onclick="getUsersOnType()" style="height: 25px;margin-top: 0px;" >Search</a>
										<img align="middle" id = "LoaderOnLoad" style ="opacity:1;top:18%;left:53%;z-index: 1;position: absolute; display: none;" src="https://transporter-dev.portqii.com/assets/image/Loader.gif"/>
									</div>			
									
									<div class="col-md-6" style="padding:0px;">		
										
									</div>
									<div class="col-md-11" style="padding:5px 0px 5px 0px;">		
										<div class="msgBox" style="margin-bottom:0px;padding: 7px;font-weight: bold;padding-left: 11px;">
										</div>
									</div>
								</div>
								<div class="col-md-12 eloquaUsers" style="padding:0px;display:none;">
									<table class="table table-striped"
									Style="margin-top: 5px;border: 1px solid #ccc; width: 100% !important;" id="eloquaUsersTable"></table>
								</div>	
							</div>
							<div class="col-md-12" style="padding:0px">						
								<table class="table table-striped"
								Style="border: 1px solid #ccc; width: 100% !important;" id="permittedUsersTable"></table>
							</div>
						</div>
					</div>
					<!-- -->
					<div class="col-xs-12 col-sm-12 col-md-12"  style="margin-top:5px; padding:0;">
						<div class="well specialHand" style="padding:5px" data-toggle="collapse" data-target="#demo2"><b style="color:#000000;">SWITCH</b><br><font size=2>Configuring this mapping will allow for child assets to be switched when moving parent assets from source to target.</font size></div>	
						<div id="demo2" class="collapse" >	
							<div class="col-md-12" style="padding:0px;">
							<div class="col-md-4 pull-left" style="padding:0px 3px 0px 0px;">
							<span class="sourceSystem" style="font-size:12px; padding:0 0 0px 0;"><b>Source</b></span>								
								<select class="form-control select_source_System  selectSys1" id="orgID1" name="selectSys1" style="height: 27px;padding: 1px 12px;font-size: 12px;">
									<option value=""></option>
									<?php												
										foreach($page_data['instanceList'] as $key=>$val)
										{
											$select = '';
											echo '<option value="'.$val.'"'.$select.'>'.$val.'</option>';
										}
									?>
								</select>
							</div>							
							<div class="col-md-4 pull-left" style="padding:0px 3px;"> 
								<span class="targetSystem" style="padding:0px 2px; font-size:12px;"><b>Target</b></span>
								<select class="form-control select_target_system selectSys2 " id="orgID2" name="selectSys2" style="height: 27px;padding: 1px 12px;font-size: 12px;">
									<option value=""></option>
									<?php
										foreach($page_data['instanceList'] as $key=>$val)
										{											
											$select='';
											echo '<option value="'.$val.'"'.$select.'>'.$val.'</option>';	
										}
									?>
								</select>
							</div>							
							<div class="col-md-3" style="padding:0px 3px;">
								<span class="assetTypeSpan" style="padding:0px; font-size:12px;"><b>Asset Type</b></span>
								<select class="form-control assetType " id="assetType" name="assetType" style="height:27px;padding: 1px 12px;font-size: 12px;">
									<option value=""></option>
									<option value="Contact Field">Contact Field</option>
									<option value="Campaign Field">Campaign Field</option>
									<option value="Email Header">Email Header</option>
									<option value="Email Footer">Email Footer</option>
									<option value="Email Group">Email Group</option>
									<option value="Field Merge">Field Merge</option>
									<option value="Image">Image</option>
									<option value="Picklist">Picklist</option>
								</select>	
							</div>							
							<div class="col-md-1" style="padding:0px;">
								<br>
								<a class="btn btn-default btn-sm populateAssets pull-center" style="height: 25px;margin-top: 0px;" >Search</a>
							</div>
							</div>
							<div class="col-md-12"><hr style="margin-top: 15px;    margin-bottom: 10px;"></div>						
							<div class="col-md-12 assetList" style="padding-left: 0px;padding-right:0px;font-size:12px;display:none;">
								<table class="table table-condensed table-hover assets" style="margin-bottom:0px;" >
									<thead>
										<tr>
											<th>Source Asset</th>
											<th>Target Asset</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>	
							</div>
							<br>
							<div class="col-md-12 displayMessage">
								<div class="alert alert-info infoBox" style="margin-top: 20px;margin-bottom:0px;padding: 12px;display:none;">
									<span class="infoMsg"></span>
								</div>								
							</div>
							<div class="col-md-12"><hr></div>
							<div class="col-md-12 assetMapping" style="padding-left: 0px;padding-right:0px;font-size:12px;">
								<table id="assetMappingTable" class="table display table-responsive table-list table-striped universalFont" style="margin-bottom:3px !important; width:100%;"></table>
							</div>
							<div class="col-md-12" style="padding-top:10px;"></div>
						</div>	
						<div class="col-xs-12 col-sm-12 col-md-12"  style="margin-top:5px; padding:0;">
							<div class="well specialHand" style="padding:5px" data-toggle="collapse" data-target="#demo3"><b style="color:#000000;">MISC</b><br><font size=2>other configuration.</font size></div>	
							<div id="demo3" class="collapse row" style="padding-bottom:20px;padding-left: 20px;">	
								<div class="col-md-12" style="padding:0px;">
									<div class="col-md-2" style="padding:0px;">
										<b class="">Image :</b>
									</div>
									<div class="col-md-3" style="padding:0px;margin-left: -70px;">
										<input onkeydown="if (event.keyCode == 13) document.getElementById('saveImage').click()" name="srch-term" id="imageUrl" placeholder='Enter image url...' type="text" min="3" style="height: 25px;width: 198px;padding-left: 2px; appearance: none" value="<?php echo $page_data['imageBaseURL'];?>" />
										<br>
									</div>
									<div class="col-md-2" style="padding:0px;padding-left: 5px;">		
										<a class="btn btn-default btn-sm searchText pull-center" id ="saveImage" onclick="saveImage()" style="height: 25px;margin-top: 0px;" >Save</a>
										<img align="middle" id = "LoaderOnImageSave" style ="opacity:1;top:18%;left:53%;z-index: 1;position: absolute; display: none;" src="https://transporter-dev.portqii.com/assets/image/Loader.gif"/>
									</div>			
								</div>
								<hr/>
								<div class="col-md-12" style="padding:0px;border:0px solid red;">
									<div class="col-md-10" style="padding:0px;">
										<b class="col-md-3" style="padding: 0px;">Email Group None :</b> &nbsp;<label class="col-md-3 switch"><input type="checkbox" onchange="emailGroupNone(this)"><span class="slider"></span></label>
									</div>
									
								</div>
							</div>
						</div>							
					<!-- -->
					</div>					
			</div>
		</div>
	</div>
</div>
</div>
<!-- MODAL -->
<div class="search_popup" style="position: fixed; top: 0; width: 100%; height: 100%; background-color: rgba(0, 0, 0, 0.5); display: none;" >
	<div class="modal-dialog modal-sm" style=" top: 20%;">
		<div class="modal-content">
			<!-- header modal -->
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
					&times;
				</button>
				<h4 class="modal-title" id="myModal">ADD NEW ORG</h4>
			</div>
			<!-- body modal -->
			<div class="modal-body">
				<div class="nomargin sky-form ">	
					<div class="form-group">
						<label ><b>Your Company Name:</b></label>
						<label class="input">
							<i class="ico-append fa fa-briefcase"></i>
							<input required="" type="text" name="companyName" placeholder="Company Name">
						</label>
					</div>
					<div class="form-group">
						<label ><b>User Name:</b></label>
						<label class="input">
							<i class="ico-append fa fa-user"></i>
							<input required="" type="text" name="username" placeholder="Username">
						</label>
					</div>
					<div class="form-group">
						<a class="btn btn-sm btn-info"> Save</a>
					</div>
				</div>	
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">

	jQuery("#terms-agree").click(function(){
		jQuery('#termsModal').modal('toggle');

		// Check Terms and Conditions checkbox if not already checked!
		if(!jQuery("#checked-agree").checked) {
			jQuery("input.checked-agree").prop('checked', true);
		}
		
	});
	
	function show_search_popup(orgid,list_type){
		$('.search_popup').find('.OrgId').val(orgid);
		$('.search_popup').find('.list_type').val(list_type);
		$('.search_popup').show();
	}
	
	$(document).ready(function()
	{		
		$.extend(true, $.fn.dataTable.defaults, {
		    mark: true
		});
		
		$('.search_popup .close').click(function(){
			$(this).parent().parent().parent().parent().hide();
		});
		
		$('.search_show').click(function(){
			var orgid = $(this).parent().find('.OrgId').val();
			var list_type = $(this).parent().find('.list_type').val();
			console.log(orgid);
			console.log(list_type);
			show_search_popup(orgid,list_type)
		});
		
		$('.add').on('click', function()
		{
			var row = "";
			row = "<tr><td style='width:57%;'><select style='width: 100%; height:25px; padding:0;' class='new_permitted_users form-control' required><option value=''></option><?php
				
					if(isset($page_data['all_users']))
					{
						foreach($page_data['all_users'] as $key1=>$val1)
						{
							echo "<option value='".$val1['name']."|".$val1['id']."'>".$val1['name']."</option>";
						}
					}
				
			
			?></select></td><td> <a type='button' class='btn btn-default btn-danger btn-sm remove ' onclick='$(this).parent().parent().remove();'>x</a></td></tr>";
			$('.users').append(row);
			
		});
		
		$('.saveUsers').on('click',function(){
			var addedUsers = [];
			$('.new_permitted_users').each(function(index){
					 
					 addedUsers.push($(this).find(':selected').val());
			});
			updateUsers(addedUsers);
		});
		
		$('.selectSys1').change(function()
		{
			$('.selectSys1 option').each(function()
			{
				var val_one = $('.selectSys1').val();
				$('.selectSys2 option').each(function()
				{
					if($(this).val() == val_one)
						$(this).hide();
					else
						$(this).show();
				});
			});
		});	
		
		$('.selectSys2').change(function()
		{
			$('.selectSys2 option').each(function()
			{				
				var val_one = $('.selectSys2').val();
				$('.selectSys1 option').each(function()
				{					
					if($(this).val() == val_one)
						$(this).hide();
					else
						$(this).show();
				});
			});
		});
			
		var assetLoad=false;	
		$('.populateAssets').click(function()
		{			
			if(assetLoad)
			{
				return;
			}	
			assetLoad=true;
			var assetType=$('.assetType').val();
			var sourceSiteName = $('.selectSys1 :selected').val();
			var targetSiteName = $('.selectSys2 :selected').val();
			
			if(assetType == '' || sourceSiteName == '' || targetSiteName == '')
			{
				if(assetType=='')
				{	
					$('.assetType').css("border-color", "red");
					
				}
				if(sourceSiteName=='')
				{	
					$('#orgID1').css("border-color", "red");
				}
				if(targetSiteName=='')
				{	
					$('#orgID2').css("border-color", "red");
				}
				
			}
			
			$('.assetType').change(function()
			{
				if($('.assetType :selected').val()=='')
				{
					$('.assetType').css("border-color", "red");
				}	
				else
				{
					$('.assetType').css("border-color", "#ddd");
				}
			});		
			
			$('.selectSys1').change(function()
			{
				if($('.selectSys1 :selected').val()=='')
				{
					$('#orgID1').css("border-color", "red");
				}	
				else
				{
					$('#orgID1').css("border-color", "#ddd");
				}
			});
			
			$('.selectSys2').change(function()
			{
				if($('.selectSys2 :selected').val()=='')
				{
					$('#orgID2').css("border-color", "red");
				}	
				else
				{
					$('#orgID2').css("border-color", "#ddd");
				}
			});
			
			if(assetType != '' && sourceSiteName != '' && targetSiteName != '')
			{	
				$('.populateAssets').attr("disabled",true);
				$('.assetList').show();				
				$.ajax(
				{
					type: 'post',
					url: 'https://transporter-dev.portqii.com/settings/populateAssets',
					data: {sourceSiteName:sourceSiteName,targetSiteName: targetSiteName,assetType:assetType},
					dataType:'json',
					beforeSend: function()
					{						
						$('.assets').find('tbody').empty().append('<tr class="tableRow"><td></td><td><div class="loader">'+'<span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span>'+
						'</div></td><td></td></tr>');					
					},
					success: function(data)
					{	
						$('.assets').find('.loader').hide();
						assetLoad = false;
						var sourceAssets =getAssetOptionValue(data.Source);
						var targetAssets = getAssetOptionValue(data.Target);
						var out='';
						var SourceSelect = '<select onchange="validateDataType(this)" class="sourceAssetList" id ="sourceAssetID" style="padding:0px;height:27px !important;width:308px;" size="1">';
						SourceSelect +='<option selected="selected" disabled>--Select--</option>';						
						$.each(sourceAssets, function (key , val) 
						{				
							var selected = '';
							selected = 'selected';
							if(val.dataType)
							{
								SourceSelect += "<option value='" + val.id + "' data-dataType='"+ val.dataType+"'>" + val.name +" ("+ val.dataType + ")</option>";
							}
							else
							{
								SourceSelect += "<option value='" + val.id + "'>" + val.name +"</option>";
							}	
								
						});
						SourceSelect += "</select>";

						var TargetSelect = '<select class="targetAssetList" id ="targetAssetID" style="padding:0px;height:27px !important;width: 308px" size="1">';
						TargetSelect +='<option selected="selected" value="0" disabled>--Select--</option>';
						if($('.assetType :selected').val()=='Contact Field' || $('.assetType :selected').val()=='Picklist' || $('.assetType :selected').val()=='Campaign Field'){
							TargetSelect +='<option value="-1" data-dataType="None">None</option>';
						}
						$.each(targetAssets, function (key , val) 
						{				
							var selected = '';
							selected = 'selected';
							if(val.dataType)
							{
								TargetSelect += "<option value='" + val.id + "' data-dataType='"+ val.dataType+"'>" + val.name +" ("+ val.dataType + ")</option>";
							}
							else
							{
								TargetSelect += "<option value='" + val.id + "'>" + val.name +"</option>";
							}		
						});
						TargetSelect += "</select>";
						$('.loader').hide();
						out += '<tr class="tableRow"><td class="sourceSelect">'+SourceSelect+'</td><td class="targetSelect">'+TargetSelect+'</td><td class="action"><a class="btn btn-default btn-sm AddMapping pull-center" style="" onclick="addMapping()">Add</a></td></tr>';
						//$('.assets').append(out);
						
						$('.assets').find('tbody').empty().append(out);
						$('.populateAssets').attr("disabled",false);						
					},
					error: function() 
					{
						assetLoad = false;
						$('.populateAssets').attr("disabled",false);
						$('.infoMsg').text("Info! Unsuccessful");
						$('.infoBox').show();
						$('.infoBox').delay(1000).fadeOut(1000);
					}
				});	
			}	
		});	
		
		//DataTable for permitted users
		$('#permittedUsersTable').DataTable({
		  
			"dom": '<"top">rt<"#btm"><"clear">',
			 responsive: true,
			 columns: [
						{ title: "Eloqua Users" , width:"18%"},					
						{ data: null, render: function ( data, type, row, meta ) {
						  return '<a id="deleteUsers" style="color:#ca170a;" onclick="deleteUsers(this,'+data[1]+')"><span class="glyphicon glyphicon-trash"></span></a>';
						}, title: "Action" , width:"10%"},
					  
					  
				  ], 
			language: {
				"sSearch": "Filter :",
				"pagingType": "scrolling",
				"emptyTable":"No records available",
				"lengthMenu": "_MENU_",
				"info": "Showing _START_ to _END_ of _TOTAL_ entries",
				"infoEmpty": "No records available",
				"infoFiltered": "(filtered from _MAX_ total records)",
				"view": 'Page _PAGE_ of _PAGES_',
			},
			
			"aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0 ] }, 
							 { "bSearchable": false, "aTargets": [ 0 ]},
							 { "className": "dt-center", "targets": [0]},
							 ] ,
			"order": [[ 0, "asc" ]],
			createdRow: function( row, data, dataIndex ) {
				$( row ).find('td:eq(0)').attr('title', data[0]);
				$( row ).find('td:eq(1)').attr('title', data[1]);			
			  },
			"iDisplayLength": 5,
			"aoColumnDefs" : 
			[  
				{
					targets: 0,
					render: function ( data, type, row ) 
					{
						return type === 'display' && data.length > 60 ?data.substr( 0, 40 )  +"..." : data;
					} 
				},			
			],
			 "fnDrawCallback": function(oSettings) {
				if (Math.ceil((this.fnSettings().fnRecordsDisplay()) / this.fnSettings()._iDisplayLength) > 1) { 
					$('#permittedUsersTable_paginate').css("display", "block");	
					$('#permittedUsersTable_length').css("display", "block");
					$('#permittedUsersTable_info').css("display", "block")
					} else {
					$('#permittedUsersTable_paginate').css("display", "none");
					$('#permittedUsersTable_filter').css("display", "none");
					$('#permittedUsersTable_info').css("display", "none");
					}
			}, 	
		});
		$('#permittedUsersTable').removeClass('dataTables_paginate');	
		$('#permittedUsersTable').removeClass('dataTable');  
		$('div.dataTables_filter input').addClass('form-control');
		
		var existingUsers = <?php echo json_encode($page_data['permitted_users']);?>;
		//console.log(existingUsers);
		var existingUserList = [];
		for(var i=0; i < existingUsers.length; i++)
		{
			var arrayEach = [];		
			var userDetails_temp=existingUsers[i].full_name +"( " +existingUsers[i].email_address+" )";
			arrayEach.push(userDetails_temp);		
			arrayEach.push(existingUsers[i].user_permissions_id);			
			existingUserList.push(arrayEach);
		}	
		//console.log(existingUserList);	
		populateUsers(existingUserList);
	
		//DataTable for mapping for assets
		$('#assetMappingTable').DataTable({
				  
				"dom": '<"top">rt<"#btm"><"clear">',
			   	 responsive: true,
			   	 columns: [
							{ title: "Source Site Name" , width:"18%"},
							{ title: "Target Site Name" , width:"18%"},
							{ title: "Asset Type" , width:"18%"},
							{ title: "Source Asset" , width:"18%"},
							{ title: "Target Asset" , width:"18%"},
							{ data: null, render: function ( data, type, row, meta ) {
			            	  return '<a id="deleteMapping" style="color:#ca170a;" onclick="deleteMapping(this,'+data[5]+')"><span class="glyphicon glyphicon-trash"></span></a>';
			        	    }, title: "Action" , width:"10%"},
			              
			              
			          ], 
			    language: {
			    	"sSearch": "Filter :",
			    	"pagingType": "scrolling",
			    	"emptyTable":"No records available",
			        "lengthMenu": "_MENU_",
		            "info": "Showing _START_ to _END_ of _TOTAL_ entries",
		            "infoEmpty": "No records available",
		            "infoFiltered": "(filtered from _MAX_ total records)",
		            "view": 'Page _PAGE_ of _PAGES_',
			    },
				
			  	"aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0 ] }, 
				                 { "bSearchable": false, "aTargets": [ 0 ]},
				                 { "className": "dt-center", "targets": [0]},
				                 ] ,
				"order": [[ 2, "asc" ]],
				createdRow: function( row, data, dataIndex ) {
                    $( row ).find('td:eq(0)').attr('title', data[0]);
                    $( row ).find('td:eq(1)').attr('title', data[1]);
                    $( row ).find('td:eq(3)').attr('title', data[3]);
                    $( row ).find('td:eq(4)').attr('title', data[4]);
                  },
      
				"aoColumnDefs" : 
                [  
                    {
                        targets: 0,
                        render: function ( data, type, row ) 
						{
							return type === 'display' && data.length > 25 ?data.substr( 0, 23 )  +"..." : data;
						} 
					},
					{
                        targets: 1,
                        render: function ( data, type, row ) 
						{
							return type === 'display' && data.length > 25 ?data.substr( 0, 23 )  +"..." : data;
						} 
					},					
					{
                        targets: 3,
                        render: function ( data, type, row ) 
						{
							return type === 'display' && data.length > 25 ?data.substr( 0, 23 )  +"..." : data;
						} 
					},
					{
                        targets: 4,
                        render: function ( data, type, row ) 
						{
							return type === 'display' && data.length > 25 ?data.substr( 0, 23 )  +"..." : data;
						} 
					},
				],
				 "fnDrawCallback": function(oSettings) {
					if (Math.ceil((this.fnSettings().fnRecordsDisplay()) / this.fnSettings()._iDisplayLength) > 1) { 
						$('#assetMappingTable_paginate').css("display", "block");	
						$('#assetMappingTable_length').css("display", "block");
						$('#assetMappingTable_info').css("display", "block")
						} else {
						$('#assetMappingTable_paginate').css("display", "none");
						//$('#assetMappingTable_filter').css("display", "none");
						$('#assetMappingTable_info').css("display", "none");
						}
			    }, 	
		});
		$('#assetMappingTable').removeClass('dataTables_paginate');	
		$('#assetMappingTable').removeClass('dataTable');  
		$('div.dataTables_filter input').addClass('form-control');
		
		//var assetsMapping[];
		var assetsMapping = <?php echo json_encode($page_data['assetMapping']);?>;
		var assetMappingList = [];
		for(var i=0; i < assetsMapping.length; i++)
		{
			var arrayEach = [];
			arrayEach.push(assetsMapping[i].Source_Site_Name);
			arrayEach.push(assetsMapping[i].Target_Site_Name);
			arrayEach.push(assetsMapping[i].Child_Asset_Type);
			var sourceAsset_temp=assetsMapping[i].Source_Asset_Name +"( " +assetsMapping[i].Source_Asset_Id+" )";			
			arrayEach.push(sourceAsset_temp);
			var targetAsset_temp=null;
			if(assetsMapping[i].Target_Asset_Name=='None')
			{
				targetAsset_temp=assetsMapping[i].Target_Asset_Name	
			}
			else
			{
				targetAsset_temp=assetsMapping[i].Target_Asset_Name +"( " +assetsMapping[i].Target_Asset_Id+" )";
			}
			arrayEach.push(targetAsset_temp);
			arrayEach.push(assetsMapping[i].source_target_mapping_id);			
			assetMappingList.push(arrayEach);
		}					
		populateAssets(assetMappingList);

		$('#eloquaUsersTable').DataTable(
		{
			"dom" : '<"top">rt<"btm"><"clear">',
			responsive : true,
			data : null,
			columns : [					
					{
						title : "User Id",width:"20%"
					},
					{
						title : "User Name"
					},
					{ 
						data: null,
						render: function ( data, type, row, meta ) 
						{
							return '<a style = "cursor:pointer;text-decoration:none;" onclick="addUser(this)" class="glyphicon glyphicon-plus"></a>';
						},
						title: "" ,width:"10%"
					},

			],
		});
		
	}); //document onload closing
	
	function updateUsers(addedUsers)
	{
		var siteId = <?php echo $page_data['siteId'];?>;
			
		$.post("https://transporter-dev.portqii.com/settings/saveusers",
		{							
			addedUsers:addedUsers,
			siteId:siteId
		},
		function(data, status){
			//alert("Data: " + data + "\nStatus: " + status);
		});
	}
	
	function getAssetOptionValue(sourceAssets)
	{
		var sourceAssetArray= [];
		$.each(sourceAssets, function (key , val) 
		{	
			if(val.dataType)
			{
				var obj = {"id":val.id, "name":val.name,"dataType":val.dataType};
			}
			else
			{
				var obj = {"id":val.id, "name":val.name};
			}		
			sourceAssetArray.push(obj);
		});
		return sourceAssetArray;
	}
	
	function addMapping()
	{
		var sourceAssetList = $('.sourceAssetList').val();
		var targetAssetList = $('.targetAssetList').val();
		
		if(sourceAssetList == null || targetAssetList == null)
		{	
			if(sourceAssetList==null)
			{	
				$('.sourceAssetList').css("border-color", "red");
			}
			if(targetAssetList==null)
			{	
				$('.targetAssetList').css("border-color", "red");
			}			
		}
			
		$('.sourceAssetList').change(function()
		{
			if($('.sourceAssetList :selected').val()=='')
			{
				$('.sourceAssetList').css("border-color", "red");
			}	
			else
			{
				$('.sourceAssetList').css("border-color", "#ddd");
			}
		});
		
		$('.targetAssetList').change(function()
		{
			if($('.targetAssetList :selected').val()=='')
			{
				$('.targetAssetList').css("border-color", "red");
			}	
			else
			{
				$('.targetAssetList').css("border-color", "#ddd");
			}
		});
		
		if(sourceAssetList != null && targetAssetList != null)	
		{
			var sourceSiteName = $('.selectSys1 :selected').val();
			var targetSiteName = $('.selectSys2 :selected').val();
			
			var assetType=$('.assetType').val();
			var sourceSiteId = <?php echo $page_data['siteId'];?>;
			var targetSiteId=null;	
			
			var instances = [];
			instances =    <?php echo json_encode($page_data['acountlist']);?>;
			instances.forEach( function(instance) 
			{ 
				if(instance.Site_Name==targetSiteName)
				{
					targetSiteId = instance.Site_Id;
				}				
			});
			var sourceAssetId = $('.sourceAssetList').val();
			var sourceAssetName = $('.sourceAssetList option:selected').text();
			var targetAssetId = $('.targetAssetList').val();
			var targetAssetName = $('.targetAssetList option:selected').text();
			
			if(assetType=='Contact Field')
			{
				var sourceAssetName_temp =null;
				if (sourceAssetName.indexOf("(") >= 0)
				{
					sourceAssetName_temp = sourceAssetName.substr(0, sourceAssetName.lastIndexOf("("));
					sourceAssetName = sourceAssetName_temp;
					console.log(sourceAssetName);
				}									
				var targetAssetName_temp = null;				
				if (targetAssetName.indexOf("(") >= 0)
				{
					targetAssetName_temp=targetAssetName.substr(0, targetAssetName.lastIndexOf("("));
					targetAssetName = targetAssetName_temp;
				}				
			}	
			if(sourceAssetName == targetAssetName)		
			{
				$('.infoMsg').text("Info! Invalid mapping");
				$('.infoBox').show();
				$('.infoBox').delay(1000).fadeOut(1030);			
			}
			else
			{
				$.ajax(
				{
					type: 'post',
					url: 'https://transporter-dev.portqii.com/settings/saveAssetMapping',
					data: {sourceSiteName:sourceSiteName,targetSiteName: targetSiteName, sourceSiteId: sourceSiteId,targetSiteId:targetSiteId,assetType:assetType,sourceAssetId:sourceAssetId,sourceAssetName:sourceAssetName,targetAssetId:targetAssetId,targetAssetName:targetAssetName},
					dataType:'json',
					beforeSend: function()
					{	
						$( ".AddMapping" ).prop('disabled',true);
					},
					success: function(data)
					{					
						//alert(data.returnResult);					
						$('.infoMsg').text("Info! "+data.returnResult);
						$('.infoBox').show();
						$('.infoBox').delay(1000).fadeOut(1030);						
						
						var out='';
						if(data.returnResult=='Asset mapped successfully.')
						{
							var assetMappingList = [];
							assetsMapping = data.assetMapping;
							for(var i=0; i < assetsMapping.length; i++)
							{
								var arrayEach = [];
								arrayEach.push(assetsMapping[i].Source_Site_Name);
								arrayEach.push(assetsMapping[i].Target_Site_Name);
								arrayEach.push(assetsMapping[i].Child_Asset_Type);
								arrayEach.push(assetsMapping[i].Source_Asset_Name);
								arrayEach.push(assetsMapping[i].Target_Asset_Name);
								arrayEach.push(assetsMapping[i].source_target_mapping_id);
								assetMappingList.push(arrayEach);
							}					
							populateAssets(assetMappingList);
						}	
					},
					error: function() 
					{
						$('.infoMsg').text("Info! Unsuccessful");
						$('.infoBox').show();
						$('.infoBox').delay(1000).fadeOut(1030);
					}
				});	
			}
		}			
	}
	
	function deleteMapping(self,source_target_mapping_id) 
	{		
		var currentRow=$(self).closest("tr"); 			
		$.ajax(
		{
			type: 'post',
			url: 'https://transporter-dev.portqii.com/settings/deleteMapping',
			data: {source_target_mapping_id:source_target_mapping_id},
			dataType:'json',
			beforeSend: function()
			{	
				$( "#deleteMapping" ).prop('disabled',true);
			},
			success: function(data)
			{				
				var out='';
				if(data.returnResult=='Asset mapping deleted successfully.' && data.assetMapping.length>0)
				{
					var assetMappingList = [];
					assetsMapping = data.assetMapping;
					for(var i=0; i < assetsMapping.length; i++)
					{
						var arrayEach = [];
						arrayEach.push(assetsMapping[i].Source_Site_Name);
						arrayEach.push(assetsMapping[i].Target_Site_Name);
						arrayEach.push(assetsMapping[i].Child_Asset_Type);
						arrayEach.push(assetsMapping[i].Source_Asset_Name);
						arrayEach.push(assetsMapping[i].Target_Asset_Name);	
						arrayEach.push(assetsMapping[i].source_target_mapping_id);		
						assetMappingList.push(arrayEach);
					}					
					populateAssets(assetMappingList);
		
					$('.infoMsg').text("Info! "+data.returnResult);
					$('.infoBox').show();
					$('.infoBox').delay(1000).fadeOut(1030);
				}	
				else
				{
					out +='<tr style=""><td><b><i>No mapping exists</i></b></td><td></td><td></td><td></td><td></td><td></td></tr>';
					$('.assetMapping').find('tbody').empty().append(out);
				}	
			},
			error: function() 
			{
				$('.infoMsg').text("Info! Unsuccessful");
				$('.infoBox').show();
				$('.infoBox').delay(1000).fadeOut(1030);	
			}
		});				    
	}
		
	function populateUsers(permittedUsers)
	{
		$('#permittedUsersTable').DataTable().destroy();
		$('#permittedUsersTable').DataTable({		  
			"dom": '<"top"f>rt<"btm"pi><"clear">',
			responsive: true,
			data: permittedUsers,		
			columns: [
						{ title: "Eloqua Users" , width:"18%"},					
						{ data: null, render: function ( data, type, row, meta ) {
						  return '<a id="deleteUsers" style="color:#ca170a;" onclick="deleteUsers(this,'+data[1]+')"><span class="glyphicon glyphicon-trash"></span></a>';
						}, title: "Action" , width:"10%"},
					  
					  
				  ], 
			language: {
				"sSearch": "Filter :",
				"pagingType": "scrolling",
				"emptyTable":"No records available",
				"lengthMenu": "_MENU_",
				"info": "Showing _START_ to _END_ of _TOTAL_ entries",
				"infoEmpty": "No records available",
				"infoFiltered": "(filtered from _MAX_ total records)",
				"view": 'Page _PAGE_ of _PAGES_',
			},
			
			"aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0 ] }, 
							 { "bSearchable": false, "aTargets": [ 0 ]},
							 { "className": "dt-center", "targets": [0]},
							 ] ,
			"order": [[ 0, "asc" ]],
			createdRow: function( row, data, dataIndex ) {
				$( row ).find('td:eq(0)').attr('title', data[0]);						
			},
			"iDisplayLength": 5,
			"aoColumnDefs" : 
			[  
				{
					targets: 0,
					render: function ( data, type, row ) 
					{
						return type === 'display' && data.length > 60 ?data.substr( 0, 40 )  +"..." : data;
					} 
				},			
			],
			 "fnDrawCallback": function(oSettings) {
				if (Math.ceil((this.fnSettings().fnRecordsDisplay()) / this.fnSettings()._iDisplayLength) > 1) 
					{ 
						$('#permittedUsersTable_paginate').css("display", "block");	
						$('#permittedUsersTable_length').css("display", "block");
						$('#permittedUsersTable_info').css("display", "block")
					} 
					else 
					{
						$('#permittedUsersTable_paginate').css("display", "none");
						$('#permittedUsersTable_info').css("display", "none");
						
					}
				}, 	
		});
		$('#permittedUsersTable').removeClass('dataTables_paginate');	
		$('#permittedUsersTable').removeClass('dataTable');  
		$('div.dataTables_filter input').addClass('form-control');
	}
	
	function populateAssets(AssetsMapping)
	{	
		$('#assetMappingTable').DataTable().destroy();
			$('#assetMappingTable').DataTable({
				"dom": '<"top"f>rt<"btm"pi><"clear">',
				 responsive: true,
				 data: AssetsMapping,		
			   	 responsive: true,
			   	 columns: [
							{ title: "Source Site Name" , width:"18%"},
							{ title: "Target Site Name" , width:"18%"},
							{ title: "Asset Type" , width:"18%"},
							{ title: "Source Asset" , width:"18%"},
							{ title: "Target Asset" , width:"18%"},
							{ data: null, render: function ( data, type, row, meta ) {
			            	  return '<a id="deleteMapping" style="color:#ca170a;" onclick="deleteMapping(this,'+ data[5]+')"><span class="glyphicon glyphicon-trash"></span></a>';
			        	    }, title: "Action" , width:"10%"},
			            ], 
			    language: {
			    	"sSearch": "Filter :",
			    	"pagingType": "scrolling",
			    	"emptyTable":"No records available",
			        "lengthMenu": "_MENU_",
		            "info": "Showing _START_ to _END_ of _TOTAL_ entries",
		            "infoEmpty": "No records available",
		            "infoFiltered": "(filtered from _MAX_ total records)",
		            "view": 'Page _PAGE_ of _PAGES_',
			    },
			  	"aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0 ] }, 
				                 { "bSearchable": false, "aTargets": [ 0 ]},
				                 { "className": "dt-center", "targets": [0]},
				                 ] ,
				"order": [[ 2, "asc" ]],
				createdRow: function( row, data, dataIndex ) {
                    $( row ).find('td:eq(0)').attr('title', data[0]);
                    $( row ).find('td:eq(1)').attr('title', data[1]);
                    $( row ).find('td:eq(3)').attr('title', data[3]);
                    $( row ).find('td:eq(4)').attr('title', data[4]);
                  },
				"iDisplayLength": 5,
				"aoColumnDefs" : 
                [  
                    {
                        targets: 0,
                        render: function ( data, type, row ) 
						{
							return type === 'display' && data.length > 25 ?data.substr( 0, 23 )  +"..." : data;
						} 
					},
					{
                        targets: 1,
                        render: function ( data, type, row ) 
						{
							return type === 'display' && data.length > 25 ?data.substr( 0, 23 )  +"..." : data;
						} 
					},
					{
                        targets: 3,
                        render: function ( data, type, row ) 
						{
							return type === 'display' && data.length > 25 ?data.substr( 0, 23 )  +"..." : data;
						} 
					},
					{
                        targets: 4,
                        render: function ( data, type, row ) 
						{
							return type === 'display' && data.length > 25 ?data.substr( 0, 23 )  +"..." : data;
						} 
					},
				],
				 "fnDrawCallback": function(oSettings) {
					if (Math.ceil((this.fnSettings().fnRecordsDisplay()) / this.fnSettings()._iDisplayLength) > 1) { 
						$('#assetMappingTable_paginate').css("display", "block");	
						$('#assetMappingTable_length').css("display", "block");
						$('#assetMappingTable_info').css("display", "block")
						} else {
						$('#assetMappingTable_paginate').css("display", "none");
						//$('#assetMappingTable_filter').css("display", "none");
						$('#assetMappingTable_info').css("display", "none");
						}
			    }, 	
		});
		$('#assetMappingTable').removeClass('dataTables_paginate');	
		$('#assetMappingTable').removeClass('dataTable');  
		$('div.dataTables_filter input').addClass('form-control');
	}
	
	function validateDataType(self)
	{
		$(".targetAssetList").val(null);		
		var sourceAssetDataType = $('.sourceAssetList option:selected').attr('data-dataType');
		if(typeof sourceAssetDataType != 'undefined')
		{
			$('.targetAssetList option').each(function()
			{
				var targetAssetDataType = $(this).attr('data-dataType');				
				if($(this).attr('data-dataType') == sourceAssetDataType)
				{	$(this).show();
				}
				else if($(this).attr('data-dataType') =='None')
				{
					$(this).show();
				}						
				else
				{
					$(this).hide();
				}	
			});
		}	
	}

	function getUsersOnType() 
	{			
		var searchText = $('#searchText').val();
		$('#warningDiv').hide();
		var table = $('#eloquaUsersTable').DataTable()
		$('.eloquaUsers').hide();
		table.destroy();
		if(searchText.length < 3) 
		{
			$('#warningDiv').show();
			$('#warningDiv').html("Minimum three characters are required")
		}
		else if(searchText.length >= 3) 
		{					
			$.ajax(
			{
				type : 'POST',
				url : 'https://transporter-dev.portqii.com/settings/getEloquaUserBySearchText',
				data : {searchText:searchText},
				dataType : 'json',
				beforeSend : function() {
					$('#LoaderOnLoad').show();	
				},
				success : function(result) 
				{
					$('#LoaderOnLoad').hide();
					$('#eloquaUsersTable').find('.loader').hide();					
					var serchedUserDetails = []; 
					if(result.userDetails !=null && result.userDetails.length>0)
					{
						for (var i = 0; i < result.userDetails.length; i++) 
						{
							var userDetails = [];
							var userId = result.userDetails[i].userId;
							var fullName = result.userDetails[i].UserName;
							var emailAddress = result.userDetails[i].UserEmail;
								
							userDetails.push(userId);
							userDetails.push(fullName + " " + "(" + emailAddress + ")");
							
							serchedUserDetails.push(userDetails);							
						}
						$('.eloquaUsers').show();
						//console.log(serchedUserDetails);					
						populateEloquaUser(serchedUserDetails);
					}
					else
					{
						$('.msgBox').removeClass('alert-danger alert-success alert-warning').addClass('alert alert-info');
						$('.msgBox').text('Info ! User does not exist.');
						$('.msgBox').show();
						$('.msgBox').delay(1000).fadeOut(1200);
					}		
				},
				error : function() 
				{
					//loadUser=true;
				}
			});
		}	
	}
		
	function populateEloquaUser(userDetails) 
	{
		$('#eloquaUsersTable').DataTable(
		{
			"dom" : '<"top">rt<"btm"pi><"clear">',
			responsive : true,
			data : userDetails,
			columns : [					
					{
						title : "User Id",width:"20%"
					},
					{
						title : "User Name"
					},
					{ 
						data: null,
						render: function ( data, type, row, meta ) 
						{
							return '<a style = "cursor:pointer;text-decoration:none;" onclick="addUser(this)" class="glyphicon glyphicon-plus"></a>';
						},
						title: "Action" ,width:"20%"
					},

			],
			"aoColumnDefs" : [
				{
					"aTargets" : [ 1 ],
					"sClass" : "tdtext"
				}, 
				{
					"aTargets" : [ 2 ],
					"sClass" : "tdtext2"
				}, 
				{
					"aTargets" : [ 0 ],
					"sClass" : "columnText"			
				} 
			],

			"order" : [ [ 1, "asc" ] ],
			"language" : {
				"sSearch" : "",
				"pagingType" : "scrolling",
				"infoEmpty" : "No records available",
				"lengthMenu" : "_MENU_",
				"searchPlaceholder" : "Filter"
			},
			"fnDrawCallback" : function(oSettings) {
				if (Math.ceil((this.fnSettings().fnRecordsDisplay())/ this.fnSettings()._iDisplayLength) > 1) 
				{
					$('#eloquaUsersTable_paginate').css("font-size", "12px");
					$(".dataTables_empty").css("display","block");
					$(".form-control").css("font-weight","normal");
					$('#eloquaUsersTable_filter').css("font-size","12px");
					$('#eloquaUsersTable_paginate').css("display","block");
					$('#eloquaUsersTable_paginate').css("margin-top", "-10px");
					$('#eloquaUsersTable_paginate').css("margin-bottom", "-10px")
					$('#eloquaUsersTable_infoEmpty').css("display", "block")
					$('table.dataTable thead .sorting_desc').css("background-image", "none").css("font-size", "12px");
					$('table.dataTable thead .sorting_asc').css("background-image", "none").css("font-size", "12px");
					$('table.dataTable thead .sorting').css("background-image", "none").css("font-size", "12px");
					$('#eloquaUsersTable_info').css("display","none");					
				} 
				else 
				{
					$('#eloquaUsersTable_info').css("display","none");
					$(".form-control").css("font-weight","normal");
					$('#eloquaUsersTable_filter').css("display","none");
					$('#eloquaUsersTable_paginate').css("display","none");
					$(".dataTables_empty").css("display","none");
					$('#eloquaUsersTable_info').css("font-size","12px");
					$('table.dataTable thead .sorting_desc').css("background-image", "none").css("font-size", "12px");
					$('table.dataTable thead .sorting_asc').css("background-image", "none").css("font-size", "12px");
					$('table.dataTable thead .sorting').css("background-image", "none").css("font-size", "12px");
				}
			},
			"iDisplayLength" : 5,
		});
		$('#eloquaUsersTable').removeClass('dataTable');
		$('div.dataTables_filter input').addClass('form-control');
	}

	function addUser(self)
	{
		var rowDom = $(self).parent().parent();
		var searchUserTable = $("#eloquaUsersTable").DataTable();		
		var data = searchUserTable.rows(rowDom[0]).data();		
		var nameWithEmail_temp = data[0][1];
		var userId=data[0][0];
		var userName =null;
		var userEmail =null;		
		if (nameWithEmail_temp.indexOf("(") >= 0)
		{
			userName = nameWithEmail_temp.substr(0, nameWithEmail_temp.lastIndexOf("("));
			userEmail = nameWithEmail_temp.substring(nameWithEmail_temp.lastIndexOf("(")+1,nameWithEmail_temp.length-1);
		}
		if((userId != null || userId !='undefined')&& (userName != null || userName !='undefined') && (userEmail != null || userEmail !='undefined'))
		{
			$.ajax(
			{
				type : 'POST',
				url : 'https://transporter-dev.portqii.com/settings/saveUserDetails',
				data : {userId:userId,userName:userName,userEmail:userEmail},
				dataType : 'json',				
				success : function(result) 
				{					
					var serchedUserDetails = []; 
					if(result.returnResult=='User added successfully.')
					{
						$('.msgBox').removeClass('alert-danger alert-info alert-warning').addClass('alert alert-success');
						$('.msgBox').text('Success ! '+result.returnResult);
						$('.msgBox').show();
						$('.msgBox').delay(1000).fadeOut(1030);
					
						var existingUserList = [];						
						for (var i = 0; i < result.savedUserDetails.length; i++) 
						{
							var arrayEach = [];		
							var userDetails_temp=result.savedUserDetails[i].full_name +"( " +result.savedUserDetails[i].email_address+" )";
							arrayEach.push(userDetails_temp);		
							arrayEach.push(result.savedUserDetails[i].user_permissions_id);existingUserList.push(arrayEach);
						}														
						populateUsers(existingUserList);
					}
					else
					{
						$('.msgBox').removeClass('alert-danger alert-info alert-success').addClass('alert alert-warning');
						$('.msgBox').text('Warning ! '+result.returnResult);
						$('.msgBox').show();
						$('.msgBox').delay(1000).fadeOut(1200);
					}		
				},
				error : function() 
				{
					$('.msgBox').addClass('alert alert-danger');
					$('.msgBox').text('Danger! Some error occurred.');	
					$('.msgBox').show();
					$('.msgBox').delay(1000).fadeOut(1200);
				}
			});
		}		
	}
	
	function deleteUsers(self,user_permissions_id) 
	{		
		var currentRow=$(self).closest("tr"); 	
		console.log(user_permissions_id);	
		$.ajax(
		{
			type: 'post',
			url: 'https://transporter-dev.portqii.com/settings/deleteUser',
			data: {user_permissions_id:user_permissions_id},
			dataType:'json',
			beforeSend: function()
			{	
				//$( "#deleteMapping" ).prop('disabled',true);
			},
			success: function(result)
			{				
				if(result.returnResult=='User removed successfully.')
				{
					$('.msgBox').removeClass('alert-danger alert-info alert-warning').addClass('alert alert-success');
					$('.msgBox').text('Success ! '+result.returnResult);
					$('.msgBox').show();
					$('.msgBox').delay(1000).fadeOut(1200);
					var existingUserList = [];						
					for (var i = 0; i < result.savedUserDetails.length; i++) 
					{
						var arrayEach = [];		
						var userDetails_temp=result.savedUserDetails[i].full_name +"( " +result.savedUserDetails[i].email_address+" )";
						arrayEach.push(userDetails_temp);		
						arrayEach.push(result.savedUserDetails[i].user_permissions_id);existingUserList.push(arrayEach);
					}														
					populateUsers(existingUserList);
				}	
					
			},
			error: function() 
			{
				$('.msgBox').removeClass('alert-success alert-info alert-warning').addClass('alert alert-danger');
				$('.msgBox').text('Danger ! Some error occurred.');	
				$('.msgBox').show();
				$('.msgBox').delay(1000).fadeOut(1200);
			}
		});				    
	}
	
	function saveImage()
	{
		var imageUrl = $('#imageUrl').val();
		var siteId = <?php echo $page_data['siteId'];?>;
		console.log(imageUrl , siteId);
		if(imageUrl)
		{
		$.ajax(
			{
				type : 'POST',
				url : 'https://transporter-dev.portqii.com/settings/saveImageBaseURL',
				data : {imgBaseURL:imageUrl ,siteId : siteId},
				dataType : 'json',
				beforeSend : function() 
				{
					$('#LoaderOnImageSave').show();	
				},
				success : function(result) 
				{
					$('#LoaderOnImageSave').hide();
				}
	        });
		}				
	}
	
	function emailGroupNone(self){
		var value = $(self).val();
		alert(value);
	}
</script>		