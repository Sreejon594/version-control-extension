<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo base_url(); ?>"> Oracle Eloqua Transporter</a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> 
					<?php echo $user_info[0]->first_name.' '; echo $user_info[0]->last_name; ?> 
					<b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <!--<li>
                            <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                        </li> -->
                        <li>
                            <a href="<?php echo base_url(); ?>/settings"><i class="fa fa-fw fa-gear"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="<?php echo base_url(); ?>/home/logout"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
							<?php //print_r ($user_info); ?>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
				<?php
					foreach($left_nav as $key =>$value)
					{	//left_nav  
						$active =$value['active']?'class="active"': '';
						echo '<li '.$active.'><a href="'.$value['href'].'"><i class="'.$value['icon_class'].'"></i> '.$value['lable'].'</a></li>';
					}
				?>                   
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>