<!DOCTYPE html>
<!--[if IE 8]>			<html class="ie ie8"> <![endif]-->
<!--[if IE 9]>			<html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->	<html> <!--<![endif]-->
	<head>
		<title><?php echo $page_title;  ?></title>
		<meta charset="utf-8" />
		<meta name="keywords" content="HTML5,CSS3,Template" />
		<meta name="description" content="" />
		<link rel="shortcut icon" href="/assets/image/favicon.ico" type="image/x-icon" />
		<link rel="icon" href="/assets/image/favicon.ico" type="image/x-icon"> 

		
		<!-- mobile settings -->
		<meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0" />
		<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->

		<!-- WEB FONTS : use %7C instead of | (pipe) -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400%7CRaleway:300,400,500,600,700%7CLato:300,400,400italic,600,700" rel="stylesheet" type="text/css" />

		<!-- CORE CSS -->
		<link href="<?php echo base_url(); ?>assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
		
		<!-- THEME CSS -->
		<link href="<?php echo base_url(); ?>assets/css/essentials.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url(); ?>assets/css/layout.css" rel="stylesheet" type="text/css" />

		<!-- PAGE LEVEL SCRIPTS -->
		<link href="<?php echo base_url(); ?>assets/css/header-1.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url(); ?>assets/css/color_scheme/green.css" rel="stylesheet" type="text/css" id="color_scheme" />
		<style>
			.sticky.clearfix .navbar-collapse
			{
				margin-top: 30px;
			}
			.sticky.clearfix.fixed .navbar-collapse
			{
				margin-top: 10px;
			}
			.mobile_button{
				margin-top: -10px !important;
			}
		</style>
	</head><!-- TOP NAV -->
	<div id="header" class="sticky clearfix">
		<!-- TOP NAV -->
		<header id="topNav">
			<div class="container">					
				
				<!-- Mobile Menu Button -->
				<button class="btn btn-mobile" data-toggle="collapse" data-target=".nav-main-collapse">
					<?php if($page == 'register'){ ?>
					<div><a class="btn btn-primary mobile_button" href="<?php echo base_url(); ?>/login">
						Login
					</a>
					</div>	
					<?php }else{ ?>
					<div><a class="btn btn-primary mobile_button" href="<?php echo base_url(); ?>/login/register">
						Create
					</a>
					</div>
					<?php } ?>
				</button>
				
				<!-- Logo -->
				<a class="logo pull-left" href="#">
					<img src="<?php echo base_url(); ?>assets/image/portqii.png" alt="" />
				</a>

				<div class="navbar-collapse pull-right nav-main-collapse collapse">
					<nav class="nav-main">
					<?php if($page == 'register'){ ?>
					<div><span> Already have an account?  &nbsp </span><a class="btn btn-primary" href="<?php echo base_url(); ?>/login">
						Login
					</a>
					</div>	
					<?php }else{ ?>
					<div><span> Need an account? &nbsp </span><a class="btn btn-primary" href="<?php echo base_url(); ?>/login/register">
						Create
					</a>
					</div>
					<?php } ?>
					</nav>
				</div>

			</div>
		</header>
		<!-- /Top Nav -->

	</div>

