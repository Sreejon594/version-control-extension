<!DOCTYPE html>
<!--[if IE 8]>			<html class="ie ie8"> <![endif]-->
<!--[if IE 9]>			<html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->	<html> <!--<![endif]-->
	<head>
		<title><?php echo $page_title;  ?></title>
		<meta charset="utf-8" />
		<meta name="keywords" content="HTML5,CSS3,Template" />
		<meta name="description" content="" />
		<link rel="shortcut icon" href="/assets/image/favicon.ico" type="image/x-icon" />
		<link rel="icon" href="/assets/image/favicon.ico" type="image/x-icon"> 

		<!-- mobile settings -->
		<meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0" />
		<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->

		<!-- WEB FONTS : use %7C instead of | (pipe) -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400%7CRaleway:300,400,500,600,700%7CLato:300,400,400italic,600,700" rel="stylesheet" type="text/css" />
		<!-- SWIPER SLIDER 
		<link href="<?php echo base_url();?>assets/plugins/slider.swiper/dist/css/swiper.min.css" rel="stylesheet" type="text/css" />-->

		<!-- CORE CSS -->
		<link href="<?php echo base_url(); ?>assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
		
		<!-- THEME CSS -->
		<link href="<?php echo base_url(); ?>assets/css/essentials.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url(); ?>assets/css/layout.css" rel="stylesheet" type="text/css" />

		<!-- PAGE LEVEL SCRIPTS -->
		<link href="<?php echo base_url(); ?>assets/css/header-1.css" rel="stylesheet" type="text/css" />
		
		<link href="<?php echo base_url(); ?>assets/css/layout-datatables.css" rel="stylesheet" type="text/css" />
		<style>
			
			img.pull-left 		
			{ 
				margin:0 34px 10px 0px !important;  
			}
			.sticky.clearfix.fixed .navbar-collapse
			{
				margin-top: 10px !important;
			}
			.mobile_button{
				margin-top: -10px !important;
			}
			.dropdown-menu{
				//height: 41px !important;
				min-width: 150px !important;
			}
			#topNav div.submenu-dark ul.dropdown-menu>li a {
				color: black !important;
			}
			#topNav div.submenu-dark ul.dropdown-menu {
				background-color: white !important;
			}
			.clearfix{
				position: fixed !important;
				border-bottom: rgba(0,0,0,0.08) 1px solid !important;
			}
			#page-wrapper{
				padding-top: 2px !important;
			}
			body{
				padding-top: 0px !important;
			}
			.profileNav{
				list-style-type: none;
				//margin-top: 5px;
				font-size: 12px;
			}
			@font-face {
			  font-family: "expansiva";
			  src: url(<?php echo base_url();?>assets/font/kimberle.ttf) format("otf");
			}
			.expansiva
			{
				font-family: "expansiva", sans-serif ;
				margin:0px 0px 0px -16px;
			}
		</style>
		<!-- jQuery -->
		<script src="<?php echo base_url(); ?>assets/js/jquery.js"></script>
		<script type="text/javascript">var plugin_path = '<?php echo base_url(); ?>assets/plugins/';</script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/jquery/jquery-2.1.4.min.js"></script>
	</head>
	<div id="page-wrapper" style="margin-top:-12px;">
		<div class="container-fluid" style="">                
			<div class="row profileDiv" style="">
				<div class="col-md-2" style=""></div>
				<div class="col-sm-12 col-xs-12 col-md-8 col-lg-8 mainContainer" style="height:auto;padding:0px 0px 0px 0px;">
					<nav class="navbar navbar-inverse" style="background-color: #fff;    border-color: #fff;">
						<div class="container-fluid">
						<div class="navbar-header">
						  <a class="navbar-brand" href="#" style="cursor:default;"><h3 class="expansiva">Oracle Eloqua Transporter</h3></a>
						</div>
						<ul class="nav navbar-nav">
						 
						</ul>
						<ul class="nav navbar-nav navbar-right">
							<li style="margin-top:10px;">
								<!-- Old PORTQII LOGO Code 
								<a href="#" style="cursor: default;pointer-events: none;">
									<p class="color_change1 poweredby pull-left" style="font-weight: bold; margin-top: 5px;margin-right: -20px;margin-bottom: 0px;font-size: 12px !important;">powered by</p>
									<img class="pull-right" src="<?php //echo base_url(); ?>assets/image/portQii.png" style="height: 40px;margin-right: -30px;margin-top: -15px;">
								</a>
								-->
								<!-- New PORTQII LOGO Code  -->
								<a href="http://www.portqii.com" target="_blank">
									<p class="color_change1 poweredby pull-left" style="font-weight: bold; margin-top:-6px;margin-right: -13px;margin-bottom: 0px;font-size: 12px !important;color:black;">powered by</p>
									<img class="pull-right" src="<?php echo base_url(); ?>assets/image/portqii_newLogo.png" style="height: 28px;margin-right:-15px;margin-top: -15px;">
								</a>
							</li>
						</ul>
					  </div>
					</nav>
				</div>
				<div class="col-md-2" style=""></div>
			</div>
		</div>
	</div>		