	</div>
    <!-- /#wrapper -->   
	<!-- jQuery -->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/jquery/jquery-2.1.4.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/scripts.js"></script>
	
	<!-- SWIPER SLIDER -->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/slider.swiper/dist/js/swiper.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/demo.swiper_slider.js"></script>
	
	
	
	<!-- plugin for table sort start-->
    <script src="<?php echo base_url(); ?>/assets/js/jquery.tablesorter.js"></script>
    <script src="<?php echo base_url(); ?>/assets/js/jquery-latest.js"></script>
    <!-- Plugin for table sort start-->
	<!-- Additional JS script -->
	<script src="<?php echo base_url(); ?>/assets/js/script.js"></script>
    
	<!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url(); ?>/assets/js/bootstrap.min.js"></script>

    <!-- Morris Charts JavaScript -->
	<!--
	<script src="<?php echo base_url(); ?>/assets/js/plugins/morris/raphael.min.js"></script>
    <script src="<?php echo base_url(); ?>/assets/js/plugins/morris/morris.min.js"></script>
    <script src="<?php echo base_url(); ?>/assets/js/plugins/morris/morris-data.js"></script>
	-->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/datatables/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/datatables/js/dataTables.tableTools.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/datatables/js/dataTables.colReorder.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/datatables/js/dataTables.scroller.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/select2/js/select2.full.min.js"></script>
		
	
	<!-- START Of Online JS libraries of Data Table -->
	<script src="https://cdn.jsdelivr.net/mark.js/8.6.0/jquery.mark.min.js"></script>
	<script src="https://cdn.jsdelivr.net/datatables.mark.js/2.0.0/datatables.mark.min.js"></script>
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.css">	
	<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css"/> 		
	<!-- END Of Online JS libraries of Data Table -->

</body>

</html>