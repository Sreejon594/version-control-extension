		<title><?php echo $page_title;  ?></title>
		<meta charset="utf-8" />
		<meta name="keywords" content="HTML5,CSS3,Template" />
		<meta name="description" content="" />
		<link rel="shortcut icon" href="/assets/image/favicon.ico" type="image/x-icon" />
		<link rel="icon" href="/assets/image/favicon.ico" type="image/x-icon"> 

		
		<!-- mobile settings -->
		<meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0" />
		<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->

		<!-- WEB FONTS : use %7C instead of | (pipe) -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400%7CRaleway:300,400,500,600,700%7CLato:300,400,400italic,600,700" rel="stylesheet" type="text/css" />
		<!-- SWIPER SLIDER 
		<link href="<?php echo base_url(); ?>assets/plugins/slider.swiper/dist/css/swiper.min.css" rel="stylesheet" type="text/css" />-->

		<!-- CORE CSS -->
		<link href="<?php echo base_url(); ?>assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
		
		<!-- THEME CSS -->
		<link href="<?php echo base_url(); ?>assets/css/essentials.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url(); ?>assets/css/layout.css" rel="stylesheet" type="text/css" />

		<!-- PAGE LEVEL SCRIPTS -->
		<link href="<?php echo base_url(); ?>assets/css/header-1.css" rel="stylesheet" type="text/css" />
		
		<link href="<?php echo base_url(); ?>assets/css/layout-datatables.css" rel="stylesheet" type="text/css" />
		<style>
			.sticky.clearfix.fixed .navbar-collapse
			{
				margin-top: 10px;
			}
			.mobile_button{
				margin-top: -10px !important;
			}
		</style>
	<!-- jQuery -->
    <script src="<?php echo base_url(); ?>/assets/js/jquery.js"></script>
	
	<script type="text/javascript">var plugin_path = '<?php echo base_url(); ?>assets/plugins/';</script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/jquery/jquery-2.1.4.min.js"></script>

