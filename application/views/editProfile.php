<style>
	#page-wrapper{
		margin-top: 20px ;
	}

	.saveButton{
		padding: 9px;
		text-align: center;
	}
	.lead{
		font-size: 18px !important;
		margin-left: 10px;
		margin-bottom: 0px !important;
	}
	.form-control {
		font-size: 12px !important;
	}
	.btn.pull-right, .btn-group.pull-right {
		margin: 0px !important;
	}
</style>
<?php //echo $user_info[0];  ?>
	<div class="container" style="background:#fff;">
		<div class="row profileDiv" id="page-wrapper">
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-3 toppad" >
				<div class="">
					<div class="">
						<h3 class="lead" ><b><?php echo $user_info[0]->first_name.' '.$user_info[0]->last_name  ?></b></h3>
					</div>
					<div class="panel-body">
						<div class="row">
							<form action="<?php echo base_url();?>settings/editProfile" method="post" enctype="multipart/form-data">           
								<div > 
									<?php $result2 = validation_errors(); ?>
										<?php if ((isset($result) && $result != '') || $result2!='')
										{ ?>
										<div class="alert alert-success alert-dismissable">
											<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
											<?php echo $result2; ?>
											<?php echo isset($result) ?$result: '' ; ?>
										</div>
									<?php }?>
						
									<table class="table table-user-information">
										<tbody>	
											<tr>
												<td>First Name</td>
												<td><input type="text" name="first_name" class="form-control" value="<?php echo $user_info[0]->first_name;?>" /></td>                           
											</tr>
											<tr>
												<td>Last Name</td>
												<td><input type="text" name="last_name" class="form-control" value="<?php echo $user_info[0]->last_name;?>" /></td>                           
											</tr>
											<tr>
												<td>Email</td>
												<td><?php echo $user_info[0]->Email_Address;?></td>
											</tr>
											<tr>
												<td>Company:</td>
												<td><?php echo $user_info[0]->Company_Name;?></td>
											</tr>							 
											
																					 
										</tbody>
									</table>
						
								</div>
								<span class="pull-left">
									<a class="btn btn-sm btn-info" href="<?php echo base_url();?>Settings/password" >Change Password</a>			
								</span>
								<span class="pull-right">
									<td><button type="submit" class="btn btn-sm btn-info pull-right">Save Profile</button></td>
								</span>
							</form>
						</div>            
					</div>
				</div>
			</div>
		</div>
	</div>




