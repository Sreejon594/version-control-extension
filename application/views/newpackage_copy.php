<!DOCTYPE html>
<!--[if IE 8]>			<html class="ie ie8"> <![endif]-->
<!--[if IE 9]>			<html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->	<html> <!--<![endif]-->
	<head>
		<title><?php echo $page_title;  ?></title>
		<meta charset="utf-8" />
		<meta name="keywords" content="HTML5,CSS3,Template" />
		<meta name="description" content="" />
		<!--meta http-equiv="refresh" content="5" /-->
		<link rel="shortcut icon" href="/assets/image/favicon.ico" type="image/x-icon" />
		<link rel="icon" href="/assets/image/favicon.ico" type="image/x-icon"> 

		<!-- mobile settings -->
		<meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0" />
		<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->

		<!-- WEB FONTS : use %7C instead of | (pipe) -->
		<link href="https://fonts.googleapis.com/css?family=Open+S   ans:300,400%7CRaleway:300,400,500,600,700%7CLato:300,400,400italic,600,700" rel="stylesheet" type="text/css" />
		<!-- SWIPER SLIDER 
		<link href="<?php echo base_url();?>assets/plugins/slider.swiper/dist/css/swiper.min.css" rel="stylesheet" type="text/css" />-->

		<!-- CORE CSS -->
		<link href="<?php echo base_url(); ?>assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
		
		<!-- THEME CSS -->
		<link href="<?php echo base_url(); ?>assets/css/essentials.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url(); ?>assets/css/layout.css" rel="stylesheet" type="text/css" />

		<!-- PAGE LEVEL SCRIPTS -->
		<link href="<?php echo base_url(); ?>assets/css/header-1.css" rel="stylesheet" type="text/css" />
		
		<link href="<?php echo base_url(); ?>assets/css/layout-datatables.css" rel="stylesheet" type="text/css" />
		
		<link href="assets/plugins/jqgrid/css/ui.jqgrid.css" rel="stylesheet" type="text/css" />
		<link href="assets/css/layout-jqgrid.css" rel="stylesheet" type="text/css" />

		<!-- jQuery -->
		<script src="<?php echo base_url(); ?>assets/js/jquery-3.1.1.min.js"></script>
		<script type="text/javascript">var plugin_path = '<?php echo base_url(); ?>assets/plugins/';</script>
		 <script src="https://appcloud-dev.portqii.com/assetnamingassistantOAuth//assets/js/jquery.tablesorter.js"></script>
		<script src="https://appcloud-dev.portqii.com/assetnamingassistantOAuth//assets/js/jquery-latest.js"></script>
 
		<!--<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/jquery/jquery-2.1.4.min.js"></script>-->
		
		<style>
			
			.ui-jqgrid-titlebar{
				color: #f5f5f5 !important;
				background:#f5f5f5 !important;
				// border: 2px solid #e3e3e3 !important;
				border-radius: 1px !important;
				color:black !important;
				height: 42px !important;
			}
			.ui-jqgrid-caption
			{
				font-weight: 700 !important;
				font-size: 12px !important;
				color:black;
			}
			@media (min-width: 768px) { 
				.validation-report-content{
						margin-left:-36%; 
				}
			}
			
			@media (min-width: 1400px) { 
				.validation-report-content{
						margin-left:-23%; 
				}
			}	
				
			.specialHand, .well{ 
				cursor: pointer; cursor: hand; 
			}

			#load_jqgrid {
				display:none !important;
			}
			.form-control{
				font-size: 11px;
				height: 32px;
			}
			.edit_packageDesc,.edit_packageName{
				font-size: 14px !important;
				margin-left: 4px !important;
				margin-top: 4px !important;
			}
			.dropdown-menu{
				min-width: 150px !important;
			}
			
			.clearfix{
				position: fixed !important;
				border-bottom: rgba(0,0,0,0.08) 1px solid !important;
			}
			
			body{
				padding-top: 0px !important;
			}
			
			form {
				margin-bottom: 5px !important; 
			}
			.nestableDiv{
				margin-bottom: 15px;
				margin-top: 15px;
			}
			.profileDiv{
				font-size: 12px !important;
			}
			.inlineForm{		
				display: inline !important;
				margin-left: 50px;
				//margin-right: 20px;
			}
			.nestableForm{
				margin-bottom: 55px;
				margin-top: 55px;
			}
			.item-box figure {
				text-align: left;
			}
			#demo3{
				padding-left: 30px;
			}
			#SubMenu1{
				padding-left: 50px;
			}
			.asset_list {
				margin-left: 20px;
				//margin-top: 1px !important;
			}
			.asset_struct {
				margin-left: 20px;
			}
			.asset_struct,.HeaderButton{
				display: none;
			}
			.source,.destination{
				max-height: 300px;
				overflow: overlay;
			}
			.form-inline{
				margin-bottom:10px;
			}
			.asset_item{
				padding: 10px 12px;
			}
			.list-group-item {		
				white-space: nowrap;
				max-width: 100%;
				overflow: hidden;
			}
			.list-group-item {
				padding: 5px 15px !important;
				//margin-bottom: 0px !important;
				border: 0px !important;
			}
			.borderBottom{
				border-bottom: rgba(0,0,0,0.05) 3px solid !important;
			}
			.mix{
				border: 1px solid #dddddd;
				height: 280px;
				margin-bottom: 10px;
			}
			.searchCompButton{
				margin-top:15px;
			}
			.checkbox label:after, 
			.radio label:after {
				content: '';
				display: table;
				clear: both;
			}
			.checkbox .cr,
			.radio .cr {
				position: relative;
				display: inline-block;
				border: 1px solid #a9a9a9;
				border-radius: .25em;
				width: 1.3em;
				height: 1.3em;
				float: left;
				margin-right: .5em;
			}

			.radio .cr {
				border-radius: 50%;
			}

			.checkbox .cr .cr-icon,
			.radio .cr .cr-icon {
				position: absolute;
				font-size: .8em;
				line-height: 0;
				top: 50%;
				left: 20%;
			}

			.radio .cr .cr-icon {
				margin-left: 0.04em;
			}

			.checkbox label input[type="checkbox"],
			.radio label input[type="radio"] {
				display: none;
			}

			.checkbox label input[type="checkbox"] + .cr > .cr-icon,
			.radio label input[type="radio"] + .cr > .cr-icon {
				transform: scale(3) rotateZ(-20deg);
				opacity: 0;
				transition: all .3s ease-in;
			}

			.checkbox label input[type="checkbox"]:checked + .cr > .cr-icon,
			.radio label input[type="radio"]:checked + .cr > .cr-icon {
				transform: scale(1) rotateZ(0deg);
				opacity: 1;
			}

			.checkbox label input[type="checkbox"]:disabled + .cr,
			.radio label input[type="radio"]:disabled + .cr {
				opacity: .5;
			}
			.checkbox, .radio {
				margin-top: 0px;
				margin-bottom: 0px;
			}
			.checkbox label, .radio label {
				min-height: 20px;
				padding-left: 5px;
			}
			.loader{
				float: right;
				text-align: center;
				vertical-align: middle;
			}
			.glyphicon-refresh-animate {
				-animation: spin .7s infinite linear;
				-webkit-animation: spin2 .7s infinite linear;
			}

			@-webkit-keyframes spin2 {
				from { -webkit-transform: rotate(0deg);}
				to { -webkit-transform: rotate(360deg);}
			}

			@keyframes spin {
				from { transform: scale(1) rotate(0deg);}
				to { transform: scale(1) rotate(360deg);}
			}

			.edit , .editNew{
				cursor: pointer;
			}
			
			.table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
				padding: 2px 8px;
			}
			h5 {
				margin-bottom: 10px !important;
			}
			
			.profileDiv{
				margin-top: 10px;
			}
			.lead{
				font-size: 18px !important;
			}
			.form-control {
				
				height: 33px !important;
				font-size: 12px !important;
			}
			
			
			.btn-default{
				padding: 1px 10px !important;
			}
			.btn-danger{
				height: 19px;
				font-weight: 400;
				font-size: 75%;
			}
			td 
			{		
				padding-top: 4px !important;
				padding-bottom: 4px !important;
			}
			.lead{
				float: left;
				margin-right: 12px;
				margin-left: 10px;

			}
			
			.fa-edit{
				margin-top: 5px;
			}
			.table-bordered>tbody>tr>td {
				// border: 0px solid #ddd !important;
				text-align: left !important;
			}
			
			.btn{
				padding: 1px 5px !important;
				font-size: 94% !important;
			}
			.description{
				float:left; 
				font-family: 'Open Sans',Arial,Helvetica,sans-serif !important;
				font-size: 13px !important;
			}
			.Completed
			{
				color: #fff !important;
				background-color: #5cb85c !important;;
				border-color: #4cae4c!important;;
			}
			.actionRequired
			{
				background-color: #ff9999 !important;
				color: #ffffff !important;
				border-color: #ff9999 !important;
			}
			.Deployment.In.Queue ,.Processed ,.Validate.In.Queue,.Deployment.In.Progress , .In.Queue
			{
				color: #fff;
				background-color: #f0ad4e;
				border-color: #eea236;
			}
			
			.Errored,.Unsupported,.Missing.Assets,.Duplicate.Found,.Duplicates.Found,.Missing.Tokens,.Invalid{
				background-color: #d9534f !important;
				color: #ffffff !important;
			}
			.form-inline{
				margin-left: 15px !important;
			}
			.status_cursor{
				cursor: default !important;
			}
			.ui-jqgrid-labels th div{
				white-space: nowrap;
				overflow: hidden !important;
				height: 30px !important;
			}
			
			.ui-jqgrid .ui-jqgrid-hbox 
			{
				padding-right: 0px !important;
				margin-right: 0px !important;
			}
			
			.ui-jqgrid .ui-jqgrid-view, .ui-jqgrid .ui-paging-info, .ui-jqgrid .ui-pg-selbox, .ui-jqgrid .ui-pg-table {
				font-size: 11px !important;
			}
			.status_cursor.Validate.Completed
			{
				background-color:#337ab7 !important;
				border:#337ab7 !important;
				margin-right: 3%!important;
				color: white;
				width: 92%;
			}
			.status_cursor.Action.Required
			{
				background-color:#ff9999 !important;
				border:#ff9999 !important;
				margin-right: 3%!important;
				color: white;
				width: 92%;
			}	
			
			.status_cursor.Deploy.Completed
			{
				margin-right: 3% !important;
				//width: 92% !important;
			}
			.status_cursor.Errored,.status_cursor.Unsupported,.status_cursor.Duplicate.Found,status_cursor.Duplicates.Found,.status_cursor.Invalid
			{
				margin-right: 3%!important;
				width: 118px !important;
			}
			.status_cursor.Validate.In.Queue,.status_cursor.Deployment.In.Queue , .status_cursor.In.Queue
			{
				margin-right: 3%!important;
				width: 92%;
			}
			.status_cursor.Missing.Assets
			{
				margin-right: 3%!important
				width: 92%;
			}
			
			.Deploy.Completed,{
				color: #fff;
				background-color: #5cb85c;
				border-color: #4cae4c;
			}
			body{
				min-height: 750px !important;
			}
			.jqgfirstrow{
				visibility: hidden;
			}
			section.page-header.page-header-xs {
				padding: 5px 0 0px 0 !important;
			}
			.ValidatePackage_Manual,.DeployPackage_close
			{
				background-color: #d9534f !important;
				border: #d9534f !important;
			}
			.expansiva {
				font-family: "expansiva", sans-serif;
				// margin: 0px 0px 0px 9px !important;
			}
			@media (min-width: 768px)
			{
				.navbar-right {
					float: right!important;
					margin-right: -8px !important;
				}
			}
			.Home_PackageName
			{
				//font-family: 'Open Sans',Arial,Helvetica,sans-serif !important;
				-webkit-font-smoothing: antialiased;
				font-weight:bold;
				font-size:12px !important;
				margin-left:20px;
				
			}
			.PackageName
			{
				//font-family: 'Open Sans',Arial,Helvetica,sans-serif !important;
				-webkit-font-smoothing: antialiased;
				font-weight:bold;
				font-size:12px !important;
				color: #000000;
			}
			.ui-jqgrid-btable{
				margin-top: -9px !important;
			}
			.s-ico{
				display: none !important;
			}
			
			.ui-jqgrid-hbox{
				margin-right: 16px !important;
			}
			.Validate.Completed,.Deploy.Completed,.Errored,.Unsupported,.Validate.In.Queue,.Deployment.In.Queue,.Processed ,.Deployment.In.Progress,.Missing.Assets,.Completed,.newButton,.Duplicate .Found,.Duplicates.Found,.Draft,.Action.Required,.Invalid
			{
				width:118px !important;
			}			
			
			.viewChildBtn,.deleteBtn
			{
				width:74px !important;
			}			
			.newBtn,.validBtn,.unsprtBtn,.unsprtErr,.unsprtCpltd,.unsprtdup,.actionRequired,.invalidBtn
			{
				width:85px !important;
				margin-left:1px !important;
			}
			
			.create_new_Package{
				height: 20px;
				margin-right: -5px;
				width: 120px;
			}
			
			@media (min-width: 992px)
			{
				.searchAssetName {
					width: 42% !important;
				}
			}
			@media (min-width: 768px)
			{
				.searchAssetName {
					width: 45% !important;
				}
			}
			@media (min-width: 360px)
			{
				.searchAssetName {
					width: 41% !important;
				}
			}
			@media (min-width: 640px)
			{
				.searchAssetName {
					width: 47% !important;
				}
			}
			@media (min-width: 1024px)
			{
				.searchAssetName {
					width: 43% !important;
				}
			}
			@-moz-document url-prefix() 
			{ 
				.sNav 
				{
					margin-left: 16.6% !important;
					margin-bottom: -13px;
				}
				.list-group-item {
					padding: 1px 16px !important;
				}
				.validation-report-content {
					margin-left: -36% !important;
				}	
			}
			#jqgh_jqgrid_Status,#jqgh_jqgrid_act
			{
				pointer-events: none !important;
			}
			/*#childName{
				font-size: 14px !important;
				font-family: "Helvetica Neue", Helvetica, Arial, sans-serif !important;
				line-height: 1.42857;
				display:inline-block !important;
				font-weight:300;
				//display: inline-block;
		
			}*/
			.longFolderName
			{
				visibility: visible;
				width: 80%;
				white-space: nowrap;
				overflow: hidden;
				text-overflow:ellipsis;
				margin-bottom: -5px;
				display:inline-block !important;
			}
			ul {
				list-style-type: none;
				margin-top: 2px !important;
			}
			label{
				font-size: 12px !important;
				margin-bottom: 2px !important;
			}
			
		</style>
		<link href="https://transporter-dev.portqii.com/assets/plugins/jqgrid/css/ui.jqgrid.css" rel="stylesheet" type="text/css" />
		<link href="https://transporter-dev.portqii.com/assets/css/layout-jqgrid.css" rel="stylesheet" type="text/css" />
	</head>
	
<?php 
	// print_r($page_data['sites_list']);	
?>	
	<div id="page-wrapper" style="margin-top:-20px;">
		<div class="container-fluid" style="font-family: Helvetica Neue,Helvetica Arial,sans-serif !important;">                
			<div class="row profileDiv" style="">
				<div class="col-md-2" style=""></div>
				<div class="col-sm-12 col-xs-12 col-md-8 col-lg-8 mainContainer" style="height:auto;padding:0px 0px 0px 0px;">
				<div class="col-md-8 col-lg-8 sNav" style="padding: 0px;width: 100%;">
					<div class="well navBarSize" style="min-height:53px;margin-left: auto;margin-right: auto;margin-bottom: -30px !important;padding-left: 0px;padding-right: 0px;cursor:default;">
						<div class="col-sm-12 col-xs-12 col-md-12 col-lg-12 statusNav" style="margin: -13px 0 0 0;padding:0 0 0 0; height:inherit;">
							<div class="col-md-6 col-lg-6 col-sm-6 col-xs-8 pull-left" style="padding:11px 0px 0 0;" >
								<a href="<?php echo base_url().$page_data['homeURL'];?>"><span class="Home_PackageName">HOME > </a><?php
									if(isset($page_data['changeset_info'][0]->Package_Name))
									{
										echo '<span class="PackageName"> ';
										// echo strtoupper($page_data['changeset_info'][0]->Package_Name);
										echo $page_data['changeset_info'][0]->Package_Name;

									}
									else
									{
										echo '<span class="PackageName"> ';
									?>	
										NEW PACKAGE
									<?php 
									}	
									?>
							</div>
							
								<input type="hidden" class="package_id_hidden" name ="package_id_hidden" readonly="readonly" value="<?php	echo $page_data['changeset_id'];?>" style="">
							
							<button id="validateAssets" style="height: 16px;background-color: #5bc0de;margin-top: 11px;color: white;border:0px;width: 240px;border-radius: 5px;pointer-events:none;font-size: 10px;display:none">
                               <span id="validateAssetsCount" style="font-size: 11px;">
                               </span>
                           <span><i id="loaderOnStatusBar" style="margin-left:5px;" class="fa fa-spinner fa-spin"></i></span></button>
                          <div class="col-md-3 col-lg-3 col-sm-3 col-xs-3 pull-right" style="width: 150px">
								<div class="inline pull-right" style="margin:5px 0 0 0;">
									<?php if(isset($page_data['changeset_info'][0]->status)){ ?>
										<?php if(($page_data['changeset_info'][0]->status == "") || ($page_data['changeset_info'][0]->status == "New") || ($page_data['changeset_info'][0]->status == "undefined")){ ?>
										<a class="btn btn-sm btn-info pull-right newButton status_cursor" style="margin-right: 3%; width: 92%;font-size: 82% !important;     pointer-events: none;">New</a>
										<?php }else{ ?>
											<a class="btn btn-sm pull-right status_cursor <?php echo $page_data['changeset_info'][0]->status; ?>" style="margin-right: 3%; width: 92%;font-size: 82% !important;    pointer-events: none;"><?php echo $page_data['changeset_info'][0]->status; ?></a>
										<?php } ?>								
									<?php }else{ ?>
										<a class="btn btn-sm btn-info pull-right newButton status_cursor" style="margin-right: 3%;width: 92%; font-size: 82% !important;    pointer-events: none;">New</a>
									<?php } ?>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-2 col-lg-2"></div>
			<!--</div>
		</div>
	</div>
	<div id="page-wrapper" style="margin-top:40px;">
		<div class="container-fluid" style="font-family: Helvetica Neue,Helvetica Arial,sans-serif !important;">                
			<div class="row profileDiv" style="/border:1px solid pink/;">
				<div class="col-md-2" style="/border:1px solid orange/;"></div> 
				<div class="col-sm-12 col-xs-12 col-md-8 col-lg-8 mainContainer" style="height:auto;padding:0px 0px 0px 0px;">
					Package Details Div Start-->
					<div class="xyx" style="">
						<div class="well specialHand" id="packageDetailsPlus" data-toggle="collapse" data-target="#packageDetails" style ="margin-top: 76px;">
							<b class="col-md-6 col-lg-6 col-sm-6 col-xs-8 pull-left" style="color:#000000;padding-left: 0px;">DEPLOYMENT PACKAGE</b>
							<?php 
								// print_r($page_data['changeset_info']);
								// exit;
								if(sizeof($page_data['changeset_info'])>0)
								{
									if($page_data['changeset_info'][0]->enable_JSONShell==1)
									{
										echo '<button style="background-color: #5bc0de;color: white;border:0px;width: 240px;border-radius: 5px;pointer-events:none;font-size: 11px;" id="childStatus">
	Create empty child assets - Enabled</button>';	
									}
									else{
										echo '<button style="background-color: #5bc0de;color: white;border:0px;width: 240px;border-radius: 5px;pointer-events:none;font-size: 11px;" id="childStatus">
	Create empty child assets - Disabled</button>';	
									}
								}
                                else
								{
									 echo '<button style="background-color: #5bc0de;color: white;border:0px;width: 240px;border-radius: 5px;pointer-events:none;font-size: 11px;" id="childStatus">
	Create empty child assets - Disabled</button>';	
								}									
							?>
							
							<span class="glyphicon glyphicon-plus pull-right"></span>
						</div>
						<div class="col-md-12 col-lg-12 packageDetails collapse accordion-body"  id = "packageDetails" style="padding:0px 2px 0px 0px; /border:1px solid green;/">
							<div class="col-md-12 col-lg-12 form-inline" style="/border:1px solid green/;font-size: 11px !important;padding:0 0 0 0 !important; margin-left:0px !important;">
								<div class="col-md-9 col-lg-9" style="padding:0 0 0 0 !important;margin-top: -6px;">
									<div class="">
										<span class="pull-left col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0px 0px 0px 0px;">
											<h6 style="min-width: 155px; margin-bottom:0px !important;">Package Name </h6>
										</span>							
										<span class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fa fa-edit edit_packageName lead" style="display: none;padding:0 0 0 0 !important;"></span>
										<div class="col-xs-12 col-sm-12 col-md-5 col-lg-5" style="padding:0px;">
											<input  maxlength="50" type="text" style="width:376px;" id = "package_name_id" class="package_name package_value form-control  inlineFormInput " name="package_name" placeholder="Enter Your Deployment Package Name" value="<?php echo isset($page_data['changeset_info'][0]->Package_Name) ? $page_data['changeset_info'][0]->Package_Name : "" ;	?>" >
										</div>
									</div>
									<div class="row">
										<span class="pull-left col-lg-12 col-md-12 col-sm-12 col-xs-12"style="padding: 0px 0px 0px 0px;margin-left: 15px;" >
											<h6 style="min-width: 155px;margin-bottom:1px !important;margin-top: 10px;">Package Description </h6>
										</span>

										<span class="col-lg-12 col-md-5 col-sm-12 col-xs-12 fa fa-edit edit_packageDesc lead" style="display: none;"></span>
										<div class="col-xs-12 col-sm-12 col-md-5 col-lg-5" style="padding:0px;">
											<textarea maxlength="200" rows="3" style="width:376px; height: 72px !important;margin-left: 15px;" class="package_description form-control inlineFormInput " id="package_description_id" placeholder="Enter Your Deployment Package Description"><?php echo isset($page_data['changeset_info'][0]->Status_Details) ? $page_data['changeset_info'][0]->Status_Details : "" ;?></textarea>
										</div>
										
										<div>
											<span class="pull-right col-lg-5 col-md-5 col-sm-12 col-xs-12"style="padding: 0px 0px 0px 0px;margin-right: -128px;">
											<h6 style="min-width: 155px;margin-bottom:1px !important;">Create empty child assets <i id='parentChild'  onclick="showPopUp()" class="fa btn" style="font-size:17px!important;color:gray;height:21px !important;padding:0px!important">&#xf059;</i></h6>&#160;
											<?php 
												if(sizeof($page_data['changeset_info'])>0)
												{	
													if($page_data['changeset_info'][0]->status=='Completed' || $page_data['changeset_info'][0]->status=='Action Required' || $page_data['changeset_info'][0]->status=='Errored'|| $page_data['changeset_info'][0]->status=='Validate In Queue' || $page_data['changeset_info'][0]->status=='Deployment In Queue'|| $page_data['changeset_info'][0]->status=='Validate Completed')
													{ 	
														if($page_data['changeset_info'][0]->enable_JSONShell==1)
														{
															echo '<input type="checkbox" name="jsonShell" id="jsonShell" checked disabled>';
														} 
														else 
														{
															echo '<input type="checkbox" name="jsonShell" id="jsonShell" disabled>';
														}
													}
													else
													{
														if($page_data['changeset_info'][0]->status=='New')
														{
															echo '<input type="checkbox" name="jsonShell" id="jsonShell" checked>';
														}
														else
														{
															if($page_data['changeset_info'][0]->enable_JSONShell==1)
															{
																echo '<input type="checkbox" name="jsonShell" id="jsonShell" checked>';
															} 
															else 
															{
																echo '<input type="checkbox" name="jsonShell" id="jsonShell">';
															}
														}	
													}
												}
												else
												{
													echo '<input type="checkbox" name="jsonShell" id="jsonShell">';
												}
											?>
											
											</span>
										</div>	
									</div>
								</div>
								<div class="col-md-3 col-lg-3" style="/border:1px solid pink/;height:10vh;">
									<button type="button" class="btn btn-info create_new_Package pull-right" value="Create Package">Create Package</button>
								</div>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-5 col-lg-5 pull-left" style="margin-bottom: 20px !important;padding:0 0 0 0; /border:1px solid red;/">
								<span class="sourceSystem" style="font-size:12px; padding:0 0 10px 0;"><b>Source</b></span>
								<span class="err_msg" style="color: red; display:none;"><br/>Source and Target must have to be different</span>
								<select class="form-control select_source_System  selectSys1 source_orgID source_OrgId" id="orgID1" name="orgID1" >
									<option value=""></option>
									<?php									
										foreach($page_data['sites_list'] as $key=>$val)
										{
											// print_r($val);
											$select = '';
											if(isset($page_data['changeset_info'][0]->Source_Site_Name) && $page_data['changeset_info'][0]->Source_Site_Name ==$val)
											{
												$select = "selected";
											}
											echo '<option value="'.$val.'"'.$select.'>'.$val.'</option>';
											
										}
									?>
								</select><br/>
							</div>
							
							<!-- Transporter Truck Icon-->		
							<!--
							<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 pull-center" style="text-align: center;margin-top: 18px;"><span style="margin-top: 24px; color:orange;"><i class="icomoon icon-truck icon-2x"></i></span></div>
							-->
							<!-- Transporter Truck Icon-->	
							
							<div class="col-xs-12 col-sm-12 col-md-5 col-lg-5 pull-right" style="padding:0px 0px 0px 0px; /border:1px solid pink;/" > 
								<span class="targetSystem" style="padding:0 0 10px 0; font-size:12px;"><b>Target</b></span>
								<span class="err_msg" style="color: red; display:none;"><br/>Source and Target must have to be different</span>							
								<select class="form-control select_target_system selectSys2 target_orgID target_OrgId" id="orgID2" name="orgID2">
									<option value=""></option>
									<?php
										foreach($page_data['sites_list'] as $key=>$val)
										{
											// print_r();
											$select = '';
											if(isset($page_data['changeset_info'][0]->Target_Site_Name) && $page_data['changeset_info'][0]->Target_Site_Name ==$val)
											{
												$select = "selected";
											}
											echo '<option value="'.$val.'"'.$select.'>'.$val.'</option>';
										}
									?>
								</select><br/>
							</div>
							<!--
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin:-33px 0px 10px 0px;padding:0px 0px 0px 0px;">	
								<button type="button" class="btn btn-info create_new_Package pull-right" value="Create Package" style="height: 30px;">Create Package</button>
							</div>
							-->
						</div>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-12"  style="margin-top:5px; padding:0;">
					<div class="well" id="deploymentDetailsPlus" data-toggle="collapse" data-target="#deploymentDetails1" >
						<b style="color:#000000;">PACKAGE ASSET DETAILS</b>
						<span class="glyphicon glyphicon-plus pull-right"></span>
					</div>
					<div class="col-md-12 col-lg-12 deploymentDetails1 collapse" id="deploymentDetails1" style="/*border:1px solid orange;*/ padding:0px 0px 0px 0px;">
						<div class="col-xs-12 col-sm-12 col-md-5 col-lg-5 pull-left" style="margin-bottom: 20px !important;padding:0 0 0 0;">
							<span class="sourceSystem" style="font-size:12px; padding:0 0 10px 0;"><b>Source </b></span>
							<input type="name" class="form-control inlineFormInput source_system source_system_name selectSys" name="SourceSystemName" placeholder="Source System Name" value="<?php if(isset($page_data['changeset_info'][0]->Source_Site_Name)){echo $page_data['changeset_info'][0]->Source_Site_Name;}?>" disabled><br/>
							<div class="col-md-12 source_asset_tree_view" style="/border:1px solid red/;padding:0px;margin-top: -13px;">
								<div class="row" style="">
									<input type="hidden" class="OrgId OrgId_hidden" id="OrgId" name="OrgId" value=""/>
									<input type="hidden" class="list_type" name="list_type" value="source"/>
									<div class="col-md-4 col-xs-4" style="padding:0px;margin-left: 15px;">
										<select class="form-control inlineFormInput Asset_Type 	Asset_Type_copy" name="Asset_Type_Id"  >
											<option selected="selected" value='Asset Type' disabled>-Asset Type-</option>
											<?php 
												foreach($page_data['AssetType'] as $atkey=>$atval)
												{
													
													if($atval->Asset_Type_Name=='Folder' || $atval->Asset_Type_Name=='FM Folder' || $atval->Asset_Type_Name=='Email Folder' )
													{
														echo '<option style="display:none" value="'.$atval->Asset_Type_Name.'">'.$atval->Asset_Type_Name.'</option>';
													}
													else
													{
														echo '<option value="'.$atval->Asset_Type_Name.'">'.$atval->Asset_Type_Name.'</option>';
													}		
												}
											?>
										</select>
									</div>
									<div class="col-md-4 col-xs-4 col-sm-4 searchAssetName" style="padding:0px;margin-left:2px;">
										<input type="name" class="form-control inlineFormInput Asset_Name Asset_Name_copy" name="AssetName" placeholder="Asset Name" >
									</div>
									<div class="col-md-2 col-xs-2 searchCancel" style="padding:0px;margin-left:2px;">
										<a role="button" class="btn btn-default btn-sm searchAsset" style="margin-left: 2px;height: 32px;width: 31px;padding: 5px 8px !important;"><span class="fa fa-search"></span></a>
										<a role="button" class="btn btn-default btn-sm reset_asset" style="margin-left:-1px;height:32px;width:29px; padding: 5px 8px !important;"><span class="fa fa-times"></span></a>
									</div>
								</div>
								<div class="mix source mix_source" style="padding: 10px; overflow: auto; display: none;margin-top: 6px;"><!-- item -->
									<div class="">
										
											<div class="list-group panel">
												<?php  
													$Source_Site_Name = isset($page_data['changeset_info'][0]->Source_Site_Name)?$page_data['changeset_info'][0]->Source_Site_Name:'';
													echo '<input type="hidden" class="OrgId OrgId_hidden" name="OrgId" value=""/>';	
													foreach($page_data['AssetType'] as $atkey => $atval)
													{
														$Asset_Type_Name = str_replace(" ","_",$atval->Asset_Type_Name);
														if($atval->Asset_Type_Name=='FM Folder' || $atval->Asset_Type_Name=='Folder' || $atval->Asset_Type_Name=='Email Folder')
														{
															echo '<div class="">
															<input type="hidden" name="RSys_Asset_Type_Id" value="'.$atval->Asset_Type_Name .'"/>
																<a href="#source_asset_type'.$Asset_Type_Name.'" class="list-group-item asset_type borderBottom" data-toggle="collapse" data-parent="#MainMenu" aria-expanded="false" style="display:none;" >
																'.$atval->Asset_Type_Name.'<span class="fa fa-plus-square-o pull-right"></span></a>
															<div class="collapse asset_list" style="display:none;" id="source_asset_type'.$Asset_Type_Name.'">
															</div>
															</div>';
														}
														else
														{	
															echo '<div class="">
															<input type="hidden" name="RSys_Asset_Type_Id" value="'.$atval->Asset_Type_Name .'"/>
																<a href="#source_asset_type'.$Asset_Type_Name.'" class="list-group-item asset_type borderBottom" data-toggle="collapse" data-parent="#MainMenu" aria-expanded="false" >
																'.$atval->Asset_Type_Name.'<span class="fa fa-plus-square-o pull-right"></span></a>
															<div class="collapse asset_list" id="source_asset_type'.$Asset_Type_Name.'">
															</div>
															</div>';
														}
													}
												?>
											</div>
										
									</div>
								</div>
							</div>
						</div>	
						<div class="col-xs-12 col-sm-12 col-md-5 col-lg-5 pull-right" style="padding:0px 0px 0px 0px;" > 
							<span class="targetSystem" style="padding:0 0 10px 0; font-size:12px;"><b>Target </b></span>
							<input type="name" class="form-control inlineFormInput target_system target_system_name selectSys2" name="TargetSystemName" placeholder="Target System Name" value="<?php 
							if(isset($page_data['changeset_info'][0]->Target_Site_Name))
								{echo $page_data['changeset_info'][0]->Target_Site_Name;}?>" 
							disabled><br/>	
							<div class="target_asset_tree_view">
								<div class="row searchButton" style="margin-bottom: 8px; margin-top: -10px;">
								<input type="hidden" class="OrgId OrgId_hidden" name="OrgId" value=""/>
								<input type="hidden" class="list_type" name="list_type" value="target"/>
								<div class="col-sm-4 pull-left" style="padding-right:0px;margin-top:-3px;">		
								<select class="form-control inlineFormInput Asset_Type Target_Asset_Type" name="Asset_Type_Id"  >
									<option selected="selected" value='Asset Type' disabled>-Asset Type-</option>
									<?php 
										foreach($page_data['AssetType'] as $atkey=>$atval)
										{
											if($atval->Asset_Type_Name=='Folder' || $atval->Asset_Type_Name=='FM Folder' || $atval->Asset_Type_Name=='Email Folder' )
											{
												echo '<option style="display:none" value="'.$atval->Asset_Type_Name.'">'.$atval->Asset_Type_Name.'</option>';
											}
											else
											{
												echo '<option value="'.$atval->Asset_Type_Name.'">'.$atval->Asset_Type_Name.'</option>';
											}
										} 
									?>
								</select>
								</div>
								<div class=" pull-left " style="padding-left: 1%; padding-right: 0%; width: 47%;margin-top:-3px;">												
									<input type="name" class="form-control inlineFormInput Asset_Name" name="AssetName" placeholder="Asset Name" >									
								</div>
								<div class="col-sm-1 pull-left" style="padding-left: 1%;margin-top:-3px;">
									<a class="btn btn-default btn-sm searchAsset inlineFormButton " style="padding:0px; line-height:26px;width:30px;"> <span class="fa fa-search "></span></a>
								</div>
								<div class="col-sm-1 pull-left" style="padding-left: 1%;margin-top:-3px;">
									<a class="btn btn-default btn-sm reset_asset inlineFormButton " style="padding:0px; line-height:26px;width:28px;">  <span class="fa fa-times "></span></a>
								</div>
											
								</div>
										
								<div class="mix destination" style="display: none; overflow: auto; padding-top: 10px;margin-top: -3px;">
									<div class="col-sm-12 col-md-12">
										<div class="list-group panel">
											<?php  
												$Target_Site_Name = isset($page_data['changeset_info'][0]->Target_Site_Name)?$page_data['changeset_info'][0]->Target_Site_Name:'';
												echo '<input type="hidden" class="OrgId OrgId_hidden" name="OrgId" value=""/>';
												foreach($page_data['AssetType'] as $atkey => $atval)
												{
													$Asset_Type_Name = str_replace(" ","_",$atval->Asset_Type_Name);
													// echo $Asset_Type_Name;
													if($atval->Asset_Type_Name=='FM Folder' || $atval->Asset_Type_Name=='Folder' || $atval->Asset_Type_Name=='Email Folder')
													{
														echo '<div class="">
														<input type="hidden" name="RSys_Asset_Type_Id" value="'.$atval->Asset_Type_Name .'"/>
															<a href="#asset_type_'.$Asset_Type_Name .'" class="list-group-item asset_type borderBottom" data-toggle="collapse" data-parent="#MainMenu" aria-expanded="false" style="display:none !important;" >
															'.$atval->Asset_Type_Name.'<span class="fa fa-plus-square-o pull-right"></span></a>
														<div class="collapse asset_list" style="display:none !important;" id="asset_type_'.$Asset_Type_Name.'">
														</div>
														</div>';
													}
													else
													{		
														echo '<div class="">
														<input type="hidden" name="RSys_Asset_Type_Id" value="'.$atval->Asset_Type_Name .'"/>
															<a href="#asset_type_'.$Asset_Type_Name .'" class="list-group-item asset_type borderBottom" data-toggle="collapse" data-parent="#MainMenu" aria-expanded="false">
															'.$atval->Asset_Type_Name.'<span class="fa fa-plus-square-o pull-right"></span></a>
														<div class="collapse asset_list" id="asset_type_'.$Asset_Type_Name.'">
														</div>
														</div>';
													}
												}
											?>										
										</div>
									</div>
								</div>
							</div>
						</div>	
					
					<!-- Deployment Package Div End-->
					<?php
						if( isset($page_data['component_list']))
							{
						?>	
							<!-- validation and Deploy Button with JQGRID -->
							<div class="row" style="margin-left: -30px;">						
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding: 0 0 0 0 !important;margin-left: 8px;">								
									<div class="text-center">
										<div class="text-center footer_deploy_button">
											<button class="btn btn-sm btn-info pull-center ValidatePackage"> Validate Package</button>
											<button class="btn btn-sm btn-info pull-center ValidationReport" >Validation Report</button>
											<button class="btn btn-sm btn-info pull-center DeployPackage" disabled >Deploy Package</button>
										</div>
										<div class="text-center missingAssetsMessage">
											<span style="color:red;font-size:12px">Include missing assets to the Deployment Package from the Validation Report<span>
										</div>
										<div class="text-center missingTokens" style="color:red;font-size:12px">
											
										</div>
										
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 jqgrid_div" style="padding-left: 22px;padding-right: 21px;">											
											<br/>
											<div class="table-responsive">
												<div class="" id="gridWrapper">
													<table id="jqgrid"></table>
													<div id="pager_jqgrid"></div>
												</div>
											</div>
										</div>
									</div>
								</div>							
							</div>
							<!-- validation and Deploy Button with JQGRID -->
						<?php } ?>	
					</div>	
					</div>
				</div>
				<div class="col-md-2"></div>
			</div>
			
			<!-- search_popup Modal Start -->
			<div class="search_popup" style="position: fixed; top: 0; width: 100%; height: 100%; background-color: rgba(0, 0, 0, 0.5); display: none;" >
				<div class="modal-dialog modal-sm" style=" top: 20%;  width: 40%;">
					<div class="modal-content">
						<!-- header modal -->
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title" id="mySmallModalLabel">Child Assets<span id="systemName"> <?php	
								foreach($page_data['orglist'] as $key=>$val)
								{
									if(isset($page_data['changeset_id'])){
										if($page_data['changeset_info'][0]->Source_Site_Name == $val->Site_Name)
											echo '<option value="'.$val->Instance__Id.'">'.$val->Site_Name.'</option>';
										continue;
									}
									$select = ($page_data['org1']->id == $val->Instance__Id)?"selected":'';
									echo '<option value="'.$val->Site_Name.'"'.$select.'>'.$val->Site_Name.'</option>';
								}											
							?></span>
							</h4>
						</div>
						<!-- body modal -->
						<div class="modal-body">
							<div class="form-inline" >
								<input type="hidden" class="OrgId" name="OrgId" value=""/>
								<input type="hidden" class="list_type" name="list_type" value=""/>
								<div class="form-group ">								
									<label ><b>Asset Type:</b></label>
									<select class="form-control inlineFormInput" name="Asset_Type_Id">
									<option value=""></option>
									<?php 
										foreach($page_data['AssetType'] as $atkey=>$atval)
										{
											echo '<option value="'.$atval->Asset_Type_Name.'">'.$atval->Asset_Type_Name.'</option>';										
										}
									?>
									</select>
								</div>
								<div class="form-group">
									<label  ><b>Asset Name:</b></label>
									<input type="name" class="form-control inlineFormInput" name="AssetName">									
								</div>
								<div class="form-group">
									<label  ><b></b></label>
									<a class="btn btn-default searchAsset inlineFormButton"> Search</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>	
			<!-- search_popup Modal End -->			
			
			<!-- parentChild_popup Modal Start -->
			<div class="parentChildPopUp" style="position: fixed; top: 0; width: 100%; height: 100%; background-color: rgba(0, 0, 0, 0.5); display: none;" >
				<div class="modal-dialog modal-sm" style=" top: 20%;  width: 40%;">
					<div class="modal-content">
						<!-- header modal -->
						<div class="modal-header">
						   <h4 style='display:inline'> Shell Support for Parent-Child Assets Combination</h4>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<i style='font-size:12px;font-weight:400;display:block'>Empty child assets will be created for the following parent-child asset combination if this option is selected.
                            </i>

						</div>
						<!-- body modal -->
						<div class="modal-body" style='max-height:260px;overflow:auto'>
							<div class="" style=''>
					            <table style="width:100%;font-size:12px;font-weight:400;" id='parentChildTable'>
								  <tr style='border-bottom:1px solid #ddd'>
									<th>Parent Asset</th>
									<th>Child Asset</th> 
								  </tr>
								  <?php				
                        				foreach($page_data['ShellParentChildConfigList'] as $key=>$val)
										{
											echo '<tr style="border-bottom:1px solid #ddd"><td>'.$val->Parent_Asset_Type.'</td><td>'.$val->Child_Asset_Type.'</td></tr>';
										}
								   ?>
								</table>
							</div>
						</div>
					   </div>			
					  </div>
			      </div>				
			    <!-- parentChild_popup Modal End -->	
			
			
					<!-- Child Info Modal Start -->	
					<div class="childInfo" style="position: fixed; top: 0; width: 100%; height: 100%; background-color: rgba(0, 0, 0, 0.5); display: none;" >			<div class="modal-dialog modal-sm" style=" top: 20%;  width: 40%;">				<div class="modal-content">
						<!-- header modal -->
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title" id="mySmallModalLabel">Dependent Child List</h4>
							<span class="ItemAddSuccessMessage" style="font-size: 12px;color: #fff;margin-left: 0px;background-color:#8ac78d;padding: 2px 5px 4px;display:none;border-radius: 2px;">The deployment package has changed. Please validate.</span>
						</div>
						<!-- body modal -->
						
						<div class="modal-body" style="max-height: 375px;overflow: auto;">
							<!--
							<span class="ItemAddSuccessMessage" style="font-size: 12px;color: #fff;margin-left: 17px;background-color:#8ac78d;padding: 5px 10px 10px;display:none;border-radius: 2px;">The Deployment Package has changed. Please Validate.</span>
							-->
							<div class="list-group panel" >
								<div class="dependent_child_header" >
								</div>
								<?php  
									// print_r($page_data['changeset_info'][0]);
									$Source_Site_Name = isset($page_data['changeset_info'][0]->Source_Site_Name)?$page_data['changeset_info'][0]->Source_Site_Name:'';
									echo '<input type="hidden" class="OrgId" name="OrgId" value="'. $Source_Site_Name.'"/>';	
									foreach($page_data['AssetType'] as $atkey => $atval)
									{
										$Asset_Type_Name = str_replace(" ","_",$atval->Asset_Type_Name);
										echo '<div class="dependent_childInfo">
										<input type="hidden" name="RSys_Asset_Type_Id" value="'.$atval->Asset_Type_Name .'"/>
											<a href="#child_asset_type'.$Asset_Type_Name.'" class="list-group-item dependent_child childValue childValue'.$Asset_Type_Name.' borderBottom" data-toggle="collapse" data-parent="#MainMenu" aria-expanded="false" >
											'.$atval->Asset_Type_Name.'<span class="fa fa-plus-square-o pull-right"></span></a>
										<div class="collapse childList" id="child_asset_type'.$Asset_Type_Name.'">
											
										</div>
										</div>';
									}
								?>
							</div>			
						</div>
					</div>
				</div>
			</div>
			<!-- Child Info Modal End -->	
				
			<!-- Invalid Asset List Start-->
			<div class="invalidAssets" style="position: fixed; top: 0; width: 100%; height: 100%; background-color: rgba(0, 0, 0, 0.5); display: none;" >
				<div class="modal-dialog modal-sm" style=" top: 20%;  width: 40%;">
					<div class="modal-content">
						<!-- header modal -->
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title" id="mySmallModalLabel">Invalid</h4>
						</div>
						<!-- body modal -->
						<div class="modal-body" style="max-height: 375px;overflow: auto;">
							<div class="list-group panel" >
								<div class="invalid_child_assets" >
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- Invalid Asset List End-->
			<!-- Unsupported Asset List START-->
			<div class="UnsupportedAssets" style="position: fixed; top: 0; width: 100%; height: 100%; background-color: rgba(0, 0, 0, 0.5); display: none;" >
				<div class="modal-dialog modal-sm" style=" top: 20%;  width: 40%;">
					<div class="modal-content">
						<!-- header modal -->
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title" id="mySmallModalLabel">Unsupported Assets List</h4>
						</div>
						<!-- body modal -->
						<div class="modal-body" style="max-height: 375px;overflow: auto;">
							<div class="list-group panel" >
								<div class="Unsupported_child_assets" >
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- Unsupported Asset List END-->
			
			<!-- Action Required Asset List START-->
			<div class="ActionRequiredAssets" style="position: fixed; top: 0; width: 100%; height: 100%; background-color: rgba(0, 0, 0, 0.5); display: none;" >
				<div class="modal-dialog modal-sm" style=" top: 20%;  width: 40%;">
					<div class="modal-content">
						<!-- header modal -->
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title" id="mySmallModalLabel">Action Required to following elements</h4>
						</div>
						<!-- body modal -->
						<div class="modal-body" style="max-height: 375px;overflow: auto;">
							<div>
								Please add/review the following elements in the target asset
							</div>
							<div class="list-group panel" >
								<div class="actionRequired_child_assets" >
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- Action Required Asset List END-->
			
			<!-- ValidationReport Start-->
			<div class="ValidationReportModel" style="position: fixed; top: 0; width: 100%; height: 100%; background-color: rgba(0, 0, 0, 0.5); display: none;" >
				<div class="modal-dialog modal-lg" style=" top: 0%;  width: 40%;">
					<div class="modal-content modal-lg validation-report-content" style="overflow:auto">
						<!-- header modal -->
						<div  class="modal-header modal-lg">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title" id="mySmallModalLabel">Validation Report</h4>
							
						</div>
						<!-- body modal -->
						
						<div class="modal-body modal-lg " style="max-height: 50%;overflow: auto;" >
							<!-- Message for Item included into package --> 
							<span class="ItemAddSuccessMessage" style="font-size: 12px;color: #fff;margin-left: 0px;background-color:#8ac78d;padding: 2px 5px 4px;display:none;border-radius: 2px;">The deployment package has changed. Please validate.</span>
							<!-- Save Message for landing page microsite assign --> 
							<span class="MSSaved" style="font-size: 12px;color: #fff;margin-left: 0px;background-color: #8ac78d;padding: 2px 5px 4px;display: none;border-radius: 2px;width: fit-content;">Changes saved.</span>
							<!-- Save Message for  duplicate asset assign --> 
							<span class="DuplicateSaved" style="font-size: 12px;color: #fff;margin-left: 0px;background-color: #8ac78d;padding: 2px 5px 4px;display: none;border-radius: 2px;width: fit-content;">Changes saved.</span>
							
							<!-- Warning message for assign asset with different status --> 
							<span class="EmailWarningDifferent" style="font-size: 12px;color: #8a6d3b;margin-left: 0px;background-color: #fcf8e3;padding: 2px 5px 4px;display: none;border-radius: 2px;width: fit-content;border-color: #faebcc;"><strong>Warning ! </strong>Email with different Email Group has been disabled for selection</span>
							<span class="FormWarningDifferent" style="font-size: 12px;color: #8a6d3b;margin-left: 0px;background-color: #fcf8e3;padding: 2px 5px 4px;display: none;border-radius: 2px;width: fit-content;border-color: #faebcc;"><strong>Warning ! </strong>Form with different htmlname has been disabled for selection</span>
							<div class="tab-content">
								<ul class="nav nav-tabs">
									<li class="active"><a data-toggle="tab" href="#Existing">Existing</a></li>
									<li><a data-toggle="tab" href="#Missing">Missing</a></li>
									<li><a data-toggle="tab" href="#AssignMicrosite" id="microSiteTab">Assign Microsites</a></li>
									<li><a data-toggle="tab" href="#DuplicateAssets" id="DuplicateAssetTab">Duplicate Assets</a></li>
								</ul>
								
								<div id="Existing" class="tab-pane fade in active" style="max-height: 350px;overflow-y: scroll;">
									
									<div class="Existing_asset" id="Existing_asset">
										<div class="list-group panel" >
											<br/>
											<!--
											<span class="ItemAddSuccessMessage" style="font-size: 12px;color: #fff;margin-left: 0px;background-color:#8ac78d;padding: 5px 10px 4px;display:none;border-radius:2px;">The deployment package has changed. Please validate.</span>
											-->
											<h5>Dependent Assets existing in target</h5>
											<div class="dependent_child_header" >
											</div>
											<?php  
												// print_r($page_data['changeset_info'][0]);
												if($page_data['changeset_info'] != NULL)
												{
												 if($page_data['changeset_info'][0]->status != "New")
												 {
													$Source_Site_Name = isset($page_data['changeset_info'][0]->Source_Site_Name)?$page_data['changeset_info'][0]->Source_Site_Name:'';
													echo '<input type="hidden" class="OrgId" name="OrgId" value="'. $Source_Site_Name.'"/>';	
													foreach($page_data['AssetType'] as $atkey => $atval)
													{
														$Asset_Type_Name = str_replace(" ","_",$atval->Asset_Type_Name);
														echo '<div class="dependent_childInfo">
														<input type="hidden" name="RSys_Asset_Type_Id" value="'.$atval->Asset_Type_Name .'"/>
															<a href="#Existing_child_asset_type'.$Asset_Type_Name.'" class="list-group-item dependent_child childValue childValue'.$Asset_Type_Name.' borderBottom" data-toggle="collapse" data-parent="#MainMenu" aria-expanded="false" >
															'.$atval->Asset_Type_Name.'<span class="fa fa-plus-square-o pull-right"></span></a>
														<div class="collapse childList v_report" id="Existing_child_asset_type'.$Asset_Type_Name.'">
															
														</div>
														</div>';
													}
												  }	
												}
											?>
										</div>
										
											<input type="hidden" class="verify_existing_value" value="0" >
											<a style="margin-left: 44%;" class="btn btn-sm btn-info pull-center verify_existing" >Include all</a>
										
									</div>
									
								</div>
								<div id="Missing" class="tab-pane fade" style="max-height: 350px;overflow-y: scroll;">	
								<div class="Missing_asset" id="Missing_asset">
										<div class="list-group panel" >
											<br/>
											<h5>Dependent Assets missing in target</h5>
											<div class="dependent_child_header" >
											</div>
											<?php  
												if($page_data['changeset_info'] != NULL)
												{
												  if($page_data['changeset_info'][0]->status != "New")
												 {
													$Source_Site_Name = isset($page_data['changeset_info'][0]->Source_Site_Name)?$page_data['changeset_info'][0]->Source_Site_Name:'';
													echo '<input type="hidden" class="OrgId" name="OrgId" value="'. $Source_Site_Name.'"/>';	
													foreach($page_data['AssetType'] as $atkey => $atval)
													{
														$Asset_Type_Name = str_replace(" ","_",$atval->Asset_Type_Name);
														echo '<div class="dependent_childInfo">
														<input type="hidden" name="RSys_Asset_Type_Id" value="'.$atval->Asset_Type_Name .'"/>
															<a href="#Missing_child_asset_type'.$Asset_Type_Name.'" class="list-group-item dependent_child childValue childValue'.$Asset_Type_Name.' borderBottom" data-toggle="collapse" data-parent="#MainMenu" aria-expanded="false" >
															'.$atval->Asset_Type_Name.'<span class="fa fa-plus-square-o pull-right"></span></a>
														<div class="collapse childList v_report" id="Missing_child_asset_type'.$Asset_Type_Name.'">
															
														</div>
														</div>';
													}
												  }	
												}
											?>
										</div>
										
											<input type="hidden" class="verify_existing_value" value="1" >
											<a style="margin-left: 44%;" class="btn btn-sm btn-info pull-center verify_existing" >Include all</a>
										
									</div>
								</div>
								<div id="AssignMicrosite" class="tab-pane fade" style="max-height: 350px;overflow-y: scroll;">									
									
									<div class="Assign_Microsite" id="Assign_Microsite" style="/border:1px solid red/;">
										
									<div class="defaultMicrosite" style="heigt:5vh; /*! border:1px solid red; */display: flex;padding: 12px;">
										<label style="font-weight: bold;margin-top: 3px;">Assign Default Microsite : </label>
										<span class="defaultMicrositeDD" style="/*! display: initial; */margin-left: 35px;">
										</span>
										<select class="DMSList" id ="DMSListID" style="padding: 0 0 0 0;width: 254px !important;height:24px !important;" size="1"></select>
										<button style="margin-left: 4%;height: 23px;" class="btn btn-sm btn-info pull-center saveDefaultMS">Save Default Microsite</button>
										</div>
										
										<table class="table table-striped LP_MicroSite">
											<thead>
												<th>Landing Page Name</th>
												<th>Source Microsites</th>
												<th>Target Microsites</th>
											</thead>
											<tbody>
											</tbody>
										</table>
										<button style="margin-left: 44%;" class="btn btn-sm btn-info pull-center assignMicrositeSave" >Save</button>
										<br>
										<!--
										<span class="MSSaved" style="margin-left: 348px;
										color: darkseagreen; display:none;">Changes saved.</span>
										-->
									</div>
								</div>
								<!-- Duplicate Assets -->
								<div id="DuplicateAssets" class="tab-pane fade" style="max-height: 350px;overflow-y: scroll;">									
									<div class="Duplicate_Assets" id="Duplicate_Assets" style="/border:1px solid red/;">
										<table class="table table-striped Assets_Duplicates">
											<tr>
												<th>Parent Asset Type</th>
												<th>Parent Asset Name</th>
												<th>Asset Type</th>
												<th>Asset Name</th>
												<th>Duplicates</th>
											</tr>
										</table>
										<button style="margin-left: 44%;" class="btn btn-sm btn-info pull-center assignDuplicateAssetSave" >Save</button>
										<br>
										<!--
										<span class="DuplicateSaved" style="margin-left: 348px;
										color: darkseagreen; display:none;">Changes saved.</span>
										-->
									</div>
								</div>
								<!-- Duplicate Assets -->
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- ValidationReport End-->
			
			<!-- Error of Deployed Assets Start -->
			<div class="errorMessage" style="position: fixed; top: 0; width: 100%; height: 100%; background-color: rgba(0, 0, 0, 0.5); display: none;" >
				<div class="modal-dialog modal-sm" style=" top: 20%;  width: 40%;">
					<div class="modal-content">
						<!-- header modal -->
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title" id="mySmallModalLabel">Error Message</h4>
						</div>
						<!-- body modal -->
						<div class="modal-body" style="max-height: 375px;overflow: auto;">
							<div class="list-group panel" >
								<div class="error_message" >
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- Error of Deployed Assets Start -->

			<!-- Deploy Package Modal Start -->
			<div class="deploy_package" style="position: fixed; top: 0; width: 100%; height: 100%; background-color: rgba(0, 0, 0, 0.5); display: none;" >				
				<div class="modal-dialog modal-sm" style=" top: 20%;  width: 40%;">
					<div class="modal-content">
						<!-- header modal -->
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title" id="mySmallModalLabel">Deploy Package</h4>
						</div>
						<!-- body modal -->
						<div class="modal-body text-center ">
							Are you sure you want to deploy?
						
						</div>
						
						<!-- footer body modal -->
						<div class="modal-footer">
							<button type="button" data-dismiss="modal" class="btn btn-info btn-sm DeployPackage_ok" onclick= "deploymentInQueue(1)" id="delete">Validate & Deploy</button>
							
							<button type="button" class="btn btn-info btn-sm DeployPackage_close" id="close1" data-dismiss="modal" aria-label="Close" onclick='deploymentInQueue(0)'>Deploy Only</button>
						</div>
					</div>
				</div>
			</div>
			<!-- Deploy Package Modal End -->	
			<!-- Validate Package Modal Start -->
			<div class="validate_package" style="position: fixed; top: 0; width: 100%; height: 100%; background-color: rgba(0, 0, 0, 0.5); display: none;" >				
				<div class="modal-dialog modal-sm" style=" top: 20%;  width: 40%;">
					<div class="modal-content">
						<!-- header modal -->
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title" id="mySmallModalLabel">Validate Package</h4>
						</div>
						<!-- body modal -->
						<div class="modal-body text-center ">
							Would you like to include the missing assets into package ?
						</div>
						<!-- footer body modal -->
						<div class="modal-footer">
							<button type="button" data-dismiss="modal" class="btn btn-info btn-sm ValidatePackage_Auto" id="delete" style="width: 38px;">Yes</button>
							<button type="button" class="btn btn-info btn-sm ValidatePackage_Manual" id="close1" data-dismiss="modal" aria-label="Close" style="width: 38px;"><span aria-hidden="true">No</span></button>
						</div>
					</div>
				</div>
			</div>
			<!-- Validate Package Modal End -->
			<!-- Token Verification Start -->
			<div class="Token_verification" style="position: fixed; top: 0; width: 100%; height: 100%; background-color: rgba(0, 0, 0, 0.5); display: none;" >	
				<div class="modal-dialog modal-sm" style=" top: 20%;  width: 40%;">
					<div class="modal-content">
						<!-- header modal -->
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title" id="mySmallModalLabel">Missing Tokens</h4>	
						</div>
						<!-- body modal -->
						<div class="modal-body text-center Token_Error_Message">
						</div>
						<!-- footer body modal -->
						<div class="modal-footer">
						</div>
					</div>
				</div>
			</div>
			<!-- Token Verification End -->
			<!-- Default Microsite Start -->
			<div class="Default_Microsite" style="position: fixed; top: 0; width: 100%; height: 100%; background-color: rgba(0, 0, 0, 0.5); display: none;" >	
				<div class="modal-dialog modal-sm" style=" top: 20%;  width: 40%;">
					<div class="modal-content">
						<!-- header modal -->
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title" id="mySmallModalLabel">Default Microsite Status</h4>	
						</div>
						<!-- body modal -->
						<div class="modal-body text-center Default_Microsite_Message">
						</div>
						<!-- footer body modal -->
						<div class="modal-footer">
						</div>
					</div>
				</div>
			</div>
			<!-- Default Microsite End -->
			<!-- Folder structure start -->
			<div class="Folder_structure" style="position: fixed; overflow-y: scroll; top: 0; width: 100%; height: 100%; background-color: rgba(0, 0, 0, 0.5); display: none;" >	
				<div class="modal-dialog modal-sm" style=" top: 20%;  width: 40%;">
					<div class="modal-content">
						<!-- header modal -->
						<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title" id="mySmallModalLabel">Select Folder</h4>	
						</div>
						<!-- body modal -->
						<div class="modal-body text-center ">
							<input type="hidden" id="folderId" name="folderId" value="">
							<input type="hidden" id="						  <tr>
								" value="">
							
							<div id="folder_container" class="panel panel-default">
							</div>
						</div>
						<!-- footer body modal -->
						<div class="modal-footer">
						<button type="button" class="btn btn-info chooseFolder" style="display: inline-block;float: center; height:50px;">Choose</button>
						</div>
					</div>
				</div>
			</div>
			
			<!-- Folder structure end -->
			
		</div>
	</div>
<script>

	function eventList()
	{
		$('.ViewChild').click(function ()
		{
			$('.childInfo').show();
			$('.ItemAddSuccessMessage').hide();	
			var Type = $(this).parent().find('input[name="Asset_Type"]').val();
			var Id = $(this).parent().find('input[name="Asset_Id"]').val();
			var package_id = $(this).parent().find('input[name="changeset_id"]').val();		
			var package_item_id = $(this).parent().find('input[name="component_id"]').val();
			var datavalue = {Package_Id:package_id,Package_Item_Id:package_item_id, Asset_Id:Id , Asset_Type:Type};
			getChildInfo(datavalue);		
		});
		
		$('.viewInvalidAsset').click(function ()
		{
			$('.invalidAssets').show();
			var package_id = $(this).parent().find('input[name="changeset_id"]').val();
			var package_item_id = $(this).parent().find('input[name="component_id"]').val();	
			var datavalue = {Package_Id:package_id,Package_Item_Id:package_item_id};
			getInvalidAsset(datavalue);		
		});
		
		$('.ValidationReport').click(function ()
		{
			var statusDplyCmpltd = $('.status_cursor').text();
			if(statusDplyCmpltd == 'Deploy Completed' || statusDplyCmpltd == 'Deployment In Queue' || statusDplyCmpltd == 'Validate In Queue' || statusDplyCmpltd == 'Errored' || statusDplyCmpltd=='Action Required' || statusDplyCmpltd=='New')
			{
				$('.verify_existing').addClass('disabled');
			}
			//alert(statusDplyCmpltd);
			if(statusDplyCmpltd !== 'New')
			{
				getMissingAsset();
			}
            $('.ValidationReportModel').show();			
		});
		
		$('.Unsupported').click(function()
		{
			var assetList= $('.Unsupported_child_assets');
			assetList.html('');
			console.log($(this).attr('value'));
			var unsupportedList = $(this).attr('value').split(",");
			
			var out = '';
			for(unsupported in unsupportedList)
			{
				out += '<div class="list-group-item "><label class="invalidAsset" style="color:#337ab7;">'+unsupportedList[unsupported]+' </label></div>';
			}
			assetList.append(out);
			$('.UnsupportedAssets').show();
		});
		
		$('.actionRequired').click(function()
		{
			var assetList= $('.actionRequired_child_assets');
			assetList.html('');
			console.log($(this).attr('value'));
			var actionRequiredList = $(this).attr('value').split(",");
			
			var out = '';
			for(actionRequired in actionRequiredList)
			{
				out += '<div class="list-group-item "><label class="required" style="color:#337ab7;white-space: nowrap;overflow: hidden !important;text-overflow: ellipsis;" title="'+actionRequiredList[actionRequired]+'">'+actionRequiredList[actionRequired]+' </label></div>';
			}
			assetList.append(out);
			$('.ActionRequiredAssets').show();
		});
		
		$('.Invalid').click(function()
		{
			var MsgList= $('.invalid_child_assets');
			MsgList.html('');			
			var invalidAssetList = $(this).attr('value').split(",");			
			console.log(invalidAssetList);
			var out = '';
			for(invalid in invalidAssetList)
			{
				out += '<div class="list-group-item "><label class="required" style="color:#337ab7;white-space: nowrap;overflow: hidden !important;text-overflow: ellipsis;" title="'+invalidAssetList[invalid]+'">'+invalidAssetList[invalid]+' </label></div>';
			}
			MsgList.append(out);			
			$('.invalidAssets').show();
		});
		
		$('.setFolder').click(function()
		{
			var package_item_id = $(this).attr('id');
		    getFolderStructure(package_item_id);
		});
	}
	
	$('.close').click(function()
	{
		$(this).parent().parent().parent().parent().hide();
		$(this).parent().parent().parent().find('.asset_list').html('').removeClass('in').attr('aria-expanded','false');
		$(this).parent().parent().parent().find('.asset_type span').addClass('fa-plus-square-o').removeClass('fa-minus-square-o');
	});
	
	$('.search_popup .close').click(function(){
		$(this).parent().parent().parent().parent().hide();
	});
	
	$('.deploy_package #close1').click(function(){
		$(this).parent().parent().parent().parent().hide();
	});
	
	function deploymentInQueue(skipValidationFlag)
	{
		var package_id = $('.package_id_hidden').val();
		$.ajax(
		{			
			type: 'post',
			url: '<?php echo base_url();?>deploy/deploymentinqueue?pId='+package_id,
			data: {skipValidationFlag : skipValidationFlag},
			beforeSend: function()
			{
				$('.DeployPackage span').remove();
				$('.DeployPackage').append(' <span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span>');
				$('.DeployPackage').attr("disabled",'true');
				$('.DeployPackage').off('click');
				$('.deploy_package').hide();
				
				getTotalValidateAssets();
			},
			success: function(data)
			{
				$('.deploy_package').hide();
				$('.deploy_package').find('.modal-body').text('You want to deploy it again');
				$('.DeployPackage span').remove();
				//location.reload(1);
				//$('.footer_deploy_button').after('<p style="margin-bottom: 2px;">This Package Deployment is in queue.</p>');
			//	location.reload(1);
			},
			error: function() 
			{
				$('.DeployPackage span').remove();
			}
		});
	}
	
	$(document).ready(function()
	{			
		var newButtonstatus = $('.status_cursor').text();
		if(newButtonstatus == 'Deploy Completed' || newButtonstatus=='New' || newButtonstatus=='Unsupported' || newButtonstatus == 'Validate Completed' || newButtonstatus == 'Errored' || newButtonstatus == 'Deployment In Queue' || newButtonstatus == 'Validate In Queue' || newButtonstatus=='Duplicates Found' || newButtonstatus =='Missing Assets' || newButtonstatus =='Action Required'|| newButtonstatus =='Invalid')
		{	
			$(this).css("cursor", "default");
		}
		
		var flag = true;
		getPackageinfo();

		$('.DeployPackage_ok').click(function (){
			$(this).parent().parent().find('.modal-body').text('');
			$(this).parent().parent().find('.modal-body').append(' <span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span>');
			//deploymentInQueue();
		});
		
		$('.DeployPackage').click(function (){
			//$('.deploy_package').show();
			checkForTokenAvailability('Deploy');
		});
		//To display the existing package details
		var package_id_hidden = $('.package_id_hidden').val();
		$('.deploymentDetails').hide();
		if(package_id_hidden!='')
		{
			$('#loaderOnStatusBar').show();
			$('#validateAssets').show();
			$('#package_name_id').show();
			$('#package_description_id').show();
			$('.edit_packageName').hide();
			$('.edit_packageDesc').hide();
			$('.deploymentDetails').show();
			$('#orgID1').attr('readonly',true);
			$('#orgID2').attr('readonly',true);
			$( ".selectSys1" ).attr('disabled',true);
			$( ".selectSys2" ).attr('disabled',true);
			$( ".create_new_Package" ).text("Update Package Name");
			$('.mix_source').show();
			$('.destination').show();
			var sourceValue= $('.source_system').val();
			var targetValue= $('.target_system').val();
			$('.mix_source').find('.OrgId_hidden').val(sourceValue);
			$('.destination').find('.OrgId_hidden').val(targetValue);
			$('#deploymentDetailsPlus').removeClass('well');	
			$('#deploymentDetailsPlus').addClass('well collapsed');
			$('#deploymentDetailsPlus').attr("aria-expanded","true");
			$('#deploymentDetailsPlus').css('display','block');
			var a = $('#deploymentDetailsPlus').children('span');
			
			if(a.attr("class")=="glyphicon glyphicon-plus pull-right")
			{	
				a.removeClass("glyphicon glyphicon-plus pull-right");
				a.addClass("glyphicon glyphicon-minus pull-right");
			}
			$('#deploymentDetails1').addClass('collapse in');
			//$('#validateAssets').show();
			
			// if(newButtonstatus == 'Invalid' || newButtonstatus == 'Unsupported' || )
			// {
				// $('#loaderOnStatusBar').show();
			//}
			
			if(newButtonstatus == 'Validate In Queue' || newButtonstatus == 'Deployment In Queue')
			{
				$('#loaderOnStatusBar').show();
			}
			if(newButtonstatus   == "Validate Completed")
			{
			  $('.DeployPackage').removeAttr("disabled");
			}
		    getTotalValidateAssets();
          //  getStatusColor();			
		}
				
		if(package_id_hidden=='')
		{
			$('#packageDetailsPlus').removeClass('well specialHand');	
			$('#packageDetailsPlus').addClass('well specialHand collapsed');
			$('#packageDetailsPlus').attr("aria-expanded","true");
			$('#packageDetailsPlus').css('display','block');
			var a = $('#packageDetailsPlus').children('span');
			if(a.attr("class")=="glyphicon glyphicon-plus pull-right")
			{	
				a.removeClass("glyphicon glyphicon-plus pull-right");
				a.addClass("glyphicon glyphicon-minus pull-right");
			}
			$('#packageDetails').addClass('collapse in');
			$('#deploymentDetailsPlus').hide();	
		}
		//to edit the package name
		$('.edit_packageName').click(function(){
			$(this).hide();
			$('#Package_name_label').hide();
			$('#package_name_id').show();
			//$('#package_description').show();
		});
		//on click of edit package description 
		$('.edit_packageDesc').click(function(){
			$(this).hide();
			$('#package_description_label').hide();
			$('.package_description').show();
		});
		
		//to change the text of button on click of the package name
		$('.edit_packageName').click(function()
		{
			$(".create_new_Package").attr('value', 'Update Package Name');
			$(".create_new_Package" ).show();
			
		});	
		
		//To differentiate between source and target site name
		$('.selectSys1 option').each(function()
		{
			var val_one = $('.selectSys1').val();
			$('.selectSys2 option').each(function()
			{
				if($(this).val() == val_one)
					$(this).hide();
				else
					$(this).show();
			});
		});
		
		$('.selectSys1').change(function()
		{
			$('.selectSys1 option').each(function()
			{
				var val_one = $('.selectSys1').val();
				$('.selectSys2 option').each(function()
				{
					if($(this).val() == val_one)
						$(this).hide();
					else
						$(this).show();
				});
			});
		});	
		
		$('.selectSys2').change(function()
		{
			$('.selectSys2 option').each(function()
			{				
				var val_one = $('.selectSys2').val();
				$('.selectSys1 option').each(function()
				{					
					if($(this).val() == val_one)
						$(this).hide();
					else
						$(this).show();
				});
			});
		});
		
		//create new package with required validation
		$( ".create_new_Package" ).click(function() 
		{
			var status = $('.status_cursor').text();
			if(status == 'Validate In Queue' || status == 'Deployment In Queue')
			{
				if($(this).hasClass(disabled))
				{
					return false;
				}
			}
			var jsonShell='';
			if($('#jsonShell').is(':checked'))
			{
				jsonShell=1; 
				$('#childStatus').html('Create empty child assets - Enabled');
				$('#childStatus').show();
				
			}	
			else{
				jsonShell=0;
				$('#childStatus').html('Create empty child assets - Disabled');
                $('#childStatus').show();	
			}
				
			console.log(jsonShell);
			var package_name = $('#package_name_id').val();
			var source_value = $('.selectSys1 :selected').val();
			var target_value = $('.selectSys2 :selected').val();
			if(package_name == '' || source_value == '' || target_value == '')
			{
				if(package_name=='')
				{	
					$('#package_name_id').css("border-color", "red");
					
				}
				if(source_value=='')
				{	
					$('#orgID1').css("border-color", "red");
				}
				if(target_value=='')
				{	
					$('#orgID2').css("border-color", "red");
				}
				
			}
			$( "#package_name_id" ).keyup(function() {
				$('#package_name_id').css("border-color", "#ddd");
			});	
			
			$('.selectSys1').change(function()
			{
				if($('.selectSys1 :selected').val()=='')
				{
					$('#orgID1').css("border-color", "red");
				}	
				else
				{
					$('#orgID1').css("border-color", "#ddd");
				}
			});	
			$('.selectSys2').change(function()
			{
				if($('.selectSys2 :selected').val()=='')
				{
					$('#orgID2').css("border-color", "red");
				}	
				else
				{
					$('#orgID2').css("border-color", "#ddd");
				}
			});

			if(package_name != '' && source_value != '' && target_value != '')
			{
				var package_name = $('#package_name_id').val();
				var package_description = $('#package_description_id').val();
				var source_value = $('.selectSys1 :selected').val();
				var target_value = $('.selectSys2 :selected').val();
				var package_id_hidden = $('.package_id_hidden').val();
				$.ajax(
				{
					type: 'post',
					url: 'Package_info/create_new_Package',
					data: {package_id:package_id_hidden,packageName: package_name, packageDescription: package_description,sourceName:source_value,targetName:target_value,jsonShell:jsonShell},
					dataType:'json',
					beforeSend: function()
					{	
						$( ".create_new_Package" ).prop('disabled',true);
					},
					success: function(data)
					{
						console.log(data);
						var package_text = "";
						$( ".create_new_Package" ).prop('disabled',false);
						$( ".selectSys1" ).prop('disabled',true);
						$( ".selectSys2" ).prop('disabled',true);
						$( ".package_id_hidden" ).val(data);	
						package_text = $( ".create_new_Package" ).text();
						$( ".create_new_Package" ).text("Update Package Name");
						$('input[name=SourceSystemName]').val(source_value);
						$('input[name=TargetSystemName]').val(target_value);
						$('.mix_source').show();
						$('.destination').show();
						$('.deploymentDetails').show();
						$('.mix_source').find('.OrgId_hidden').val(source_value);
						$('.destination').find('.OrgId_hidden').val(target_value);
						history.pushState('data to be passed', 'Title of the page', 'https://transporter-dev.portqii.com/package_info?id='+data);
						if(package_text != "Update Package Name")
						{
							$('#packageDetailsPlus').attr("aria-expanded","false");
							$('#deploymentDetailsPlus').show();	
							$('#deploymentDetailsPlus').removeClass('well');	
							$('#deploymentDetailsPlus').addClass('well collapsed');
							$('#deploymentDetailsPlus').attr("aria-expanded","true");
							$('#deploymentDetailsPlus').css('display','block');
							$('#deploymentDetailsPlus').addClass('collapse in');
							var a = $('#deploymentDetailsPlus').children('span');
							if(a.attr("class")=="glyphicon glyphicon-plus pull-right")
							{	
								a.removeClass("glyphicon glyphicon-plus pull-right");
								a.addClass("glyphicon glyphicon-minus pull-right");
							}
							$('#deploymentDetails1').addClass('collapse in');
							$('#packageDetails').removeClass('in');
							var a = $('#packageDetailsPlus').children('span');
							if(a.attr("class")=="glyphicon glyphicon-minus pull-right")
							{	
								a.removeClass("glyphicon glyphicon-minus pull-right");
								a.addClass("glyphicon glyphicon-plus pull-right");
							}
						}
						// $(".navBarSize").load(" .navBarSize");
						// $(".statusNav").load(" .statusNav");
					},
					error: function() 
					{
						alert('Unable to create the package');
					}
				});	
			}
		});
		
		//To find the assets using search box
		$(".Asset_Type").blur(function()
		{
			var Asset_Type = $('.Asset_Type :selected').val();
			if(Asset_Type == "Asset Type")
			{
				$(this).css("border-color", "red");
			}
			else
			{
				$(this).css("border-color", "#ddd");
			}
		});
		
		$(".Target_Asset_Type").blur(function()
		{
			var Target_Asset_Type = $('.Target_Asset_Type :selected').val();
			if(Target_Asset_Type == "Asset Type")
			{
				$(this).css("border-color", "red");
			}
			else
			{
				$(this).css("border-color", "#ddd");
			}
		});	
		
		$('.Asset_Type').on('change',function()
		{	
			$(this).parent().parent().parent().find('.asset_list').hide();
			if($(this).parent().parent().parent().parent().find('.asset_type').attr('aria-expanded') == "false")
			{
				if($(this).find(".fa-minus-square-o"))
				{
					console.log("hello");
					$(this).removeClass("fa-minus-square-o").addClass("fa-plus-square-o");
				}		
			}						
		    $(this).parent().parent().parent().find('#source_asset_type'+$(this).val().replace(" ","_")).show();    
			$(this).parent().parent().parent().find('#asset_type_'+$(this).val().replace(" ","_")).show();    
			$(this).parent().parent().parent().find('.asset_type').hide();
			$(this).parent().parent().parent().find('.asset_list').html('');
			$(this).parent().parent().parent().find('#source_asset_type'+$(this).val().replace(" ","_")).parent().find('.asset_type').show();
			$(this).parent().parent().parent().find('#asset_type_'+$(this).val().replace(" ","_")).parent().find('.asset_type').show();
        });

		$('.reset_asset').click(function()
		{
			$(this).parent().parent().find('.Asset_Type option').removeAttr('selected , disabled');
			$(this).parent().parent().find('.Asset_Type option:first').attr('selected','selected');
			$(this).parent().parent().find('.Asset_Type option:first').attr('disabled', true);
			$(this).parent().parent().find('.Asset_Name').val('');
			$(this).parent().parent().find('.Asset_Type').css("border-color", "#ddd");
			$(this).parent().parent().parent().find('.asset_list').html('').removeClass('in').attr('aria-expanded','false');
			$(this).parent().parent().parent().find('.asset_type span').addClass('fa-plus-square-o').removeClass('fa-minus-square-o');
			$(this).parent().parent().parent().find('.asset_type').attr('aria-expanded','false');
			$(this).parent().parent().parent().find('.asset_type').show();
		});
		
		//on click of search Asset Icon
		$('.searchAsset').click(function()
		{
			var Asset_Type = $(this).parent().parent().find('.Asset_Type :selected').val();
			if(Asset_Type == "Asset Type")
			{
				$(this).parent().parent().find('.Asset_Type').focus();
				$(this).parent().parent().find('.Asset_Type').css("border-color","red");
				return false;
			}
			var AssetId=$(this).parent().parent().find('select[name="Asset_Type_Id"]').val();
			if($(this).parent().parent().parent().find('.list_type').val() == 'source' )
			{
				
				$('#source_asset_type'+AssetId).parent().find('.asset_list').html('');		
				var assetlist = $('#source_asset_type'+AssetId.replace(" ", "_")).parent().find('.asset_list');
				var tempdata = {
				"Asset_Type_Id" : $(this).parent().parent().find('select[name="Asset_Type_Id"]').val(),
				"OrgId" : $(this).parent().parent().parent().parent().find('.source_system_name').val(),
				"AssetName" : $(this).parent().parent().find('input[name="AssetName"]').val(),
				"page" : 1,
				};
			}
			else
			{
				$('#asset_type_2_'+AssetId).parent().find('.asset_list').html('');		
				var assetlist = $('#asset_type_'+AssetId.replace(" ", "_")).parent().find('.asset_list');
				var tempdata = {
				"Asset_Type_Id" : $(this).parent().parent().find('select[name="Asset_Type_Id"]').val(),
				"OrgId" : $(this).parent().parent().parent().parent().find('.target_system').val(),
				"AssetName" : $(this).parent().parent().find('input[name="AssetName"]').val(),
				"page" : 1,
			};
			}
			if($.trim($(this).parent().find('.asset_list').html()) =='')
				getSearchAsset(this,assetlist,tempdata);
				disabled_checkbox();
			
		});
		
		$('.Asset_Name').keyup(function(e)
		{
			if(e.keyCode == 13)
			{
				var Asset_Type = $(this).parent().parent().find('.Asset_Type :selected').val();
				if(Asset_Type == "Asset Type")
				{
					$(this).parent().parent().find('.Asset_Type').focus();
					$(this).parent().parent().find('.Asset_Type').css("border-color","red");
					return false;
				}
				
				var AssetId=$(this).parent().parent().find('select[name="Asset_Type_Id"]').val();
				// var OrgId=$(this).parent().parent().find('.source_value').val();		
				
				if($(this).parent().parent().parent().find('.list_type').val() == 'source' )
				{
					$('#source_asset_type'+AssetId).parent().find('.asset_list').html('');	
					var tempdata = {
										"Asset_Type_Id" : $(this).parent().parent().find('select[name="Asset_Type_Id"]').val(),
										"OrgId" : $(this).parent().parent().parent().parent().find('.source_system_name').val(),
										"AssetName" : $(this).parent().parent().find('input[name="AssetName"]').val(),
										"page" : 1,
									};					
					var assetlist = $('#source_asset_type'+AssetId.replace(" ", "_")).parent().find('.asset_list');
				}
				else
				{
					$('#asset_type_2_'+AssetId).parent().find('.asset_list').html('');		
					var tempdata = 
					{
						"Asset_Type_Id" : $(this).parent().parent().find('select[name="Asset_Type_Id"]').val(),
						"OrgId" : $(this).parent().parent().parent().parent().find('.target_system').val(),
						"AssetName" : $(this).parent().parent().find('input[name="AssetName"]').val(),
						"page" : 1,
					};
					var assetlist = $('#asset_type_'+AssetId.replace(" ", "_")).parent().find('.asset_list');
				}
				if($.trim($(this).parent().find('.asset_list').html()) =='')
					var search_button = $(this).parent().parent().find('.searchAsset');
					getSearchAsset(search_button, assetlist, tempdata);
			}
		});
		
		$('.asset_type').click(function()
		{	
			if($(this).attr('aria-expanded') == "false")
			{
				$('.asset_list').css('display', 'block');
				$(this).find(".fa-plus-square-o").addClass("fa-minus-square-o").removeClass("fa-plus-square-o");
				
			}
			else
			{
				$(this).find(".fa-minus-square-o").addClass("fa-plus-square-o").removeClass("fa-minus-square-o");
				$('.asset_list').css('display', 'none');
			}
			var tempdata = {
						"Asset_Type_Id":$(this).parent().find('input[name="RSys_Asset_Type_Id"]').val(),
						"OrgId": $(this).parent().parent().find('.OrgId_hidden').val(),
					};
				// alert(tempdata.OrgId);	
			var assetlist = $(this).parent().find('.asset_list');
			
			$.trim($(this).parent().find('.asset_list').empty());
			if($.trim($(this).parent().find('.asset_list').html()) =='')
			{
				getAssetList(this,assetlist,tempdata);
			}
			disabled_checkbox();
		});
		
		function getSearchAsset(Asset,assetlist,tempdata)
		{
			$.ajax(
			{
				type: 'post',
				url: '<?php echo base_url();?>package_info/searchAsset',
				data: tempdata,
				beforeSend: function()
				{
					$(Asset).html('<span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span>');
					//$('.no_asset').hide();
				},
				success: function(data)
				{  
					$(assetlist).empty();
					$(Asset).html('<span class="fa fa-search"></span>');
					$(Asset).find('.loader').hide();
					$('.search_popup').hide();
					var list = JSON.parse( data );
					// alert(list);
					$(list.elements).each(function()
					{
						var out = printObject(this);
						// $(assetlist).append( '<div class="list-group-item "><input class="additem" type="checkbox" value="'+this.id+'"/><a href="#id_'+tempdata.OrgId+'_'+ this.id +'" class="asset_item" data-toggle="collapse" >'+this.name +'</a></div>' );
						// $('.destination').find('.additem').remove();
						var newButtonstatus = $('.status_cursor').text();
						if(newButtonstatus == 'Deploy Completed' || newButtonstatus == 'Deployment In Queue' || newButtonstatus == 'Validate In Queue' || newButtonstatus == 'Errored' || newButtonstatus =='Action Required')
						{
							$(assetlist).append( '<div class="list-group-item "><input name="Asset_Type"  type="hidden" value="'+this.type+'"/><input id="AssetId" name="Asset_Type"  type="hidden" value="'+this.id+'"/><a><i class="fa fa-plus-square additem_" style="color: #46b8da;display:none;" aria-hidden="true" title="Click to add asset"></i></a><a style="cursor:default !important;" href="#id_'+tempdata.OrgId+'_'+ this.id +'" class="asset_item" data-toggle="collapse" >'+this.name +'</a></div>' );
						}
						else
						{
							$(assetlist).append( '<div class="list-group-item "><input name="Asset_Type"  type="hidden" value="'+this.type+'"/><input id="AssetId" name="Asset_Type"  type="hidden" value="'+this.id+'"/><a><i class="fa fa-plus-square additem_" style="color: #46b8da;" aria-hidden="true" title="Click to add asset"></i></a><a style="cursor:default !important;" href="#id_'+tempdata.OrgId+'_'+ this.id +'" class="asset_item" data-toggle="collapse" >'+this.name +'</a></div>' );
						}
						
						$('.destination').find('.additem_').remove();
					});	
					if($('.asset_type').hasClass('collapsed'))
					{
						$('.asset_type').removeClass('collapsed');
						$(this).attr('aria-expanded') = "true";
						//$('.asset_list').addClass('in');
						$('.asset_list').attr('aria-expanded') = "true";
						
					}
					
					$('.asset_list').parent().find(".fa-plus-square-o").addClass("fa-minus-square-o").removeClass("fa-plus-square-o");
					
					if(list.page * list.pageSize < list.total ){
						var pageno = list.page +1;
						$(assetlist).append( '<div class="load_more_asset"><input class="page" type="hidden" value="'+pageno+'" /> <a class="btn btn-xs btn-info">Load More ('+list.page * list.pageSize +'/'+list.total+') </a></div>' );
					}
					if(list.total<=0 ){
						$(assetlist).append( '<div class="no_asset">No asset found.</div>' );
						// $(assetlist).append( '<div class="no_asset">No asset found.</div>' ).delay(2000).fadeOut(1000);
					}					
					$('.load_more_asset').click(function(){
						// var test= $(".mix.source").find('.OrgId_hidden').val();
						// console.log(test);
					var tempdata = {
								"Asset_Type_Id":$(".Asset_Type").val(),
								// "Asset_Type_Id":$(this).parent().parent().find('input[name="Asset_Type_Id"]').val(),
								// "OrgId": $(".mix.source").find('.OrgId_hidden').val(),
								"OrgId": $(this).parent().parent().parent().find('.OrgId_hidden').val(),
								"AssetName" : $(".Asset_Name ").val(),
								"page": $(this).find('.page').val(),
							};
							// alert(tempdata.AssetName);
					
					     var assetlist = $(this).parent().parent().find('.asset_list');						
						// getAssetList(this,assetlist,tempdata);						
						getSearchAsset(this,assetlist,tempdata);						
						$(this).remove();
					});
					disabled_checkbox();
					additem_new();
				},
				error: function()
				{
					result = false;
				}
			});
		}
		
		function getAssetList(Asset,assetlist,tempdata)
		{			
			if(flag == true)
			{
				$.ajax(
				{
					type: 'post',
					url: '<?php echo base_url();?>package_info/getAssetData',
					data: tempdata,
					beforeSend: function()
					{
						$(Asset).append('<div class="loader">'+
							'<span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span>'+
						'</div></a>');
						flag = false;
					},
					success: function(data)
					{
						$(Asset).find('.loader').hide();
						//$('.search_popup').hide();
						$('.searchAsset').html('<span class="fa fa-search"> </span>');
						var list = JSON.parse( data );
						
						$(list.elements).each(function()
						{
							var out = printObject(this);
							
							// $(assetlist).append( '<div class="list-group-item "><input class="additem"  type="checkbox" value="'+this.id+'"/><a href="#id_'+tempdata.OrgId+'_'+ this.id +'" class="asset_item" data-toggle="collapse" >'+this.name +'</a></div>' );
							// $('.destination').find('.additem').remove();
							var newButtonstatus = $('.status_cursor').text();
							if(newButtonstatus == 'Deploy Completed' || newButtonstatus == 'Deployment In Queue' || newButtonstatus == 'Validate In Queue' || newButtonstatus == 'Errored'|| newButtonstatus == 'Action Required' )
							{
								$(assetlist).append( '<div class="list-group-item "><input name="Asset_Type"  type="hidden" value="'+this.type+'"/><input id="AssetId" name="Asset_Type"  type="hidden" value="'+this.id+'"/><a><i class="fa fa-plus-square additem_" style="color: #46b8da;display:none" aria-hidden="true" title="Click to add asset"></i></a><a style="cursor:default !important;" href="#id_'+tempdata.OrgId+'_'+ this.id +'" class="asset_item" data-toggle="collapse" >'+this.name +'</a></div>' );
								$('.destination').find('.additem_').remove();	
							}
							else
							{
								$(assetlist).append( '<div class="list-group-item "><input name="Asset_Type"  type="hidden" value="'+this.type+'"/><input id="AssetId" name="Asset_Type"  type="hidden" value="'+this.id+'"/><a><i class="fa fa-plus-square additem_" style="color: #46b8da;" aria-hidden="true" title="Click to add asset"></i></a><a style="cursor:default !important;" href="#id_'+tempdata.OrgId+'_'+ this.id +'" class="asset_item" data-toggle="collapse" >'+this.name +'</a></div>' );
								$('.destination').find('.additem_').remove();
							}
													
						});	
						
						if(list.page * list.pageSize < list.total ){
							var pageno = list.page +1;
							$(assetlist).append( '<div class="load_more_asset"><input class="page" type="hidden" value="'+pageno+'" /> <a class="btn btn-xs btn-info">Load More ('+list.page * list.pageSize +'/'+list.total+') </a></div>' );
						}					
						$('.load_more_asset').click(function(){
						var tempdata = {
									"Asset_Type_Id":$(this).parent().parent().find('input[name="RSys_Asset_Type_Id"]').val(),
									"OrgId": $(this).parent().parent().parent().find('.OrgId_hidden').val(),
									"page": $(this).find('.page').val(),
								};
						var assetlist = $(this).parent().parent().find('.asset_list');
							
							getAssetList(this,assetlist,tempdata);
							$(this).remove();
						});
						disabled_checkbox();
						flag = true;
						// additem_new();
						additem_new();
					},
					error: function()
					{
						result = false;
						flag =true;
						//alert("errored");
					}
				});
			}
		}
		
		function printObject(o) 
		{
			var out = '';
			for (var p in o) {
				if(typeof o[p] === 'object')
				{
					o[p] = printObject(o[p]);
					o[p] = '{ <br/> '+o[p] + '} <br/>';
				}
				out += p + ': ' + o[p] + '<br/>';
			}
			return out;
		}
		
		function disabled_checkbox()
		{
			var status = $('.status_cursor').text();
			if(status == 'Deploy Completed' || status == 'Deployment In Queue')
			{
				$("input.additem").attr("disabled", true);
				$(".ValidatePackage").removeClass('.ValidatePackage');
				$(".DeployPackage").removeClass('.DeployPackage');
				
			}
		}	
		
		$('.well.specialHand').click(function()
		{
			var a = $(this).children('span');
			if(a.attr("class")=="glyphicon glyphicon-plus pull-right")
			{	
				a.removeClass("glyphicon glyphicon-plus pull-right");
				a.addClass("glyphicon glyphicon-minus pull-right");
				
			}
			else
			{
				a.removeClass("glyphicon glyphicon-minus pull-right");
				a.addClass("glyphicon glyphicon-plus pull-right");
			}
			
			$(this).parent().children().toggle();
			$(this).toggle();
		});
		
		$('#deploymentDetailsPlus').click(function(){
			
			var a = $(this).children('span');
			// alert(a.attr("class"));
			if(a.attr("class")=="glyphicon glyphicon-plus pull-right")
			{	
				a.removeClass("glyphicon glyphicon-plus pull-right");
				a.addClass("glyphicon glyphicon-minus pull-right");
				
			}
			else
			{
				a.removeClass("glyphicon glyphicon-minus pull-right");
				a.addClass("glyphicon glyphicon-plus pull-right");
			}
			
			$(this).parent().children().toggle();
			$(this).toggle();
		});
		
		$('.verify_existing').click(function ()
		{
			var package_id = $('.package_id_hidden').val();
			var verify_existing_value = $(this).parent().find('.verify_existing_value').val();
			$.ajax(
			{
				type: 'post',
				url: '<?php echo base_url();?>package_info/verify_existing',
				data: {packageId: package_id, verify_existing_value: verify_existing_value},
				dataType:'json',
				beforeSend: function()
				{
					$( ".verify_existing" ).attr('disabled',true);
				},
				success: function(data)
				{
					$('.ItemAddSuccessMessage').show().delay(2000).fadeOut(1000);
					$( ".verify_existing" ).attr('disabled',false);
					get_jqgrid(data);
					var $mygrid =  $("#jqgrid");
					allGridParams = $mygrid.jqGrid("getGridParam");
					allGridParams.data = data;
					$mygrid.trigger("reloadGrid", [{current: true}]);
					
					if(data != ""){
						$('.selectSys , .selectSys *').attr("disabled", true);
						$('.selectSys2 , .selectSys2 *').attr("disabled", true);
					} 
					
				},
				error: function() 
				{
					$( ".verify_existing" ).attr('disabled',false);
				
				}
			});
		});	
		
		var micrositesList = null;
		var loadingMS = false;

		$('#microSiteTab').click(function()
		{	
			var status = $('.status_cursor').text();
			if(status == 'Deploy Completed' || status == 'Errored')
			{
				$('.assignMicrositeSave').addClass('disabled');	
			}
			$('#DMSListID').attr("disabled" , "disabled");
			loadingMS = true;
			var package_id = $('.package_id_hidden').val();
			$.ajax(
			{
				type: 'post',
				url: '<?php echo base_url();?>package_info/getAllLandingPages',
				data: {packageId: package_id},
				dataType:'json',
				beforeSend: function()
				{
					//alert("before ajax");
					
					var spinner='';
					$('.tableRow').hide();
					if($('.LP_MicroSite').find(".glyphicon-refresh-animate"))
					{
						$('.loader').remove();
					}
					spinner +='<div class="loader" style="margin-left: 230px !important;"><span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span></div>';
					$('.LP_MicroSite').append(spinner);
					
				//	$('.assignMicrositeSave').hide();
				},
				success: function(data)
				{			
			        $('.LP_MicroSite tbody > tr').remove();
					$('.assignMicrositeSave').show();
					var micrositData = getMicrositeOptionValue(data.Microsites);
					//console.log(data.defaultTargetMicrosite.id);
					if(data.Microsites!='')
					{	
				        $('#DMSListID').empty();
						//'<select class="DMSList" id ="DMSListID" style="padding: 0 0 0 0;width: 254px !important;height:24px !important;" size="1">';
						var DMSSelect = '<option selected="selected" value="0" disabled>--Select--</option>';
						$.each(micrositData, function (key , val) 
						{							
							if(data.defaultTargetMicrosite!=null && data.defaultTargetMicrosite.id==val.id)
							{
								DMSSelect += "<option selected value='" + val.id + "'>" + val.name +" ( "+ val.href + " )</option>";
							}
							else
							{
								DMSSelect += "<option value='" + val.id + "'>" + val.name +" ( "+ val.href + " )</option>";
							}	
							//$('.saveDefaultMicrosite').attr('disabled',false);
						});
						//DMSSelect += "</select>";
						$('#DMSListID').append(DMSSelect);
						$('#DMSListID').removeAttr("disabled" , "disabled");
						//$('DMSList').show();
					}
					if(data.Landingpages=='' || data.Landingpages==null)
					{
						$('.tableRow').remove();
						// console.log(data);
						$('.assignMicrositeSave').hide();
						$('.loader').hide();
						$('#DMSListID').removeAttr("disabled" , "disabled");
						out+='<tr class="tableRow"><td></td><td style="font-style:italic">No new Landing Page found in package</td><td></td></tr>';
						$('.LP_MicroSite').append(out);
						return;	
					}
					micrositesList = data.Microsites;
					var LandingpageData = data.Landingpages;
					var out='';
					$('.loader').hide();
					
					$.each(LandingpageData, function (LPkey , LPval) 
					{
						if(LPval.MicrositeIdSaved !='' || LPval.MicrositeIdSaved !='undefined')
						{
							var MSSelect = '<select class="trgtMSList" id ="trgtMSListID" onchange = "enableDisableSaveBtn(this)" style="padding: 0 0 0 0;width: 254px !important;height:24px !important;" size="1">';
							MSSelect +='<option selected="selected"  value="0" disabled>--Select--</option>';
							$.each(micrositData, function (key , val) 
							{
                               if(LPval.MicrositeIdSaved == val.id)	
								{
									var selected = '';
									selected = 'selected'
									MSSelect += "<option selected value='" + val.id + "'>" + val.name +" ( "+ val.href + " )</option>";
									$('.assignMicrositeSave').attr('disabled',true);
								}
								else
								{
									MSSelect += "<option value='" + val.id + "'>" + val.name +" ( "+ val.href + " )</option>";
									$('.assignMicrositeSave').attr('disabled',false);
								}
							});
							MSSelect += "</select>";
							out += '<tr class="tableRow"><td class="LPName">'+LPval.LandingpageName+'<input type=hidden class=LPId value='+LPval.LandingpageId+'></td><td>'+LPval.MicrositeName+'</td><td>'+MSSelect+'</td></tr>';
						}
					});
					
					$('.LP_MicroSite').append(out);
					var status = $('.status_cursor').text();
					if(status == 'Deploy Completed' || status == 'Errored' || status == 'Action Required')
					{
						$(".assignMicrositeSave").addClass('disabled');
						$('.trgtMSList').prop('disabled', true);
					}	
					loadingMS = false;
				},
				error: function() 
				{
					// alert('Microsite is saved successfully');
					loadingMS = false;
				}
			});
		});	

		
		
		function getMicrositeOptionValue(microsites)
		{
			var micrositeArray= [];
			$.each(microsites, function (key , val) 
			{	
				var obj = {"id":val.id, "name":val.name,"href":val.domains};
				micrositeArray.push(obj);
			});
			return micrositeArray;
		}
		
		//Function to save selected microsites
		$('.assignMicrositeSave').click(function()
		{
			/*if($(this).hasClass('disabled'))
			{
				return false;
			}*/
			var status = $('.status_cursor').text();
			if(status == 'Deploy Completed' || status == 'Errored')
			{
				if($(this).hasClass('disabled'))
				{
					return false;
				}	
			}	
			var package_id = $('.package_id_hidden').val();
			var JSONFormed= [];			
			
			$('.LP_MicroSite td:nth-child(3)').each(function() 
			{
				var trgtMicrositeId=$(this).find("select").val();
				if(trgtMicrositeId== null || trgtMicrositeId.length==0 || trgtMicrositeId=='')
				{
					$(this).find("select").css('border-color', '#C70039');
					$('.assignMicrositeSave').attr('disabled',true);
				}
				else
				{
					$(this).find("select").css('border-color', '#E5E7E9');
				}
			});
			
			$('.LP_MicroSite > tbody  > tr').each(function()
			{	
				$('.trgtMSList').on('change', function() 
				{	
					$(this).css('border-color', '#E5E7E9');					
					enableDisableSaveButton();
				});
					
				var LPId=$(this).find('.LPId').val();	
				var MSID=$(this).find("select").val();								
				var LPName=$(this).find('.LPName').text();
				var obj = {"LPID":LPId, "LPname":LPName,"MSID":MSID};
				JSONFormed.push(obj);
			});
			
			function enableDisableSaveButton()
			{
				var allFilled = 1;
				$('.LP_MicroSite td:nth-child(3)').each(function() 
				{
					var trgtMicrositeId=$(this).find("select").val();
					
					if(trgtMicrositeId.length==0 || trgtMicrositeId=='')
					{
						allFilled = 0;
						$('.assignMicrositeSave').attr('disabled',true);
					}
					else
					{
						$(this).find("select").css('border-color', '#E5E7E9');
					}
				});
				
				//console.log(allFilled);
				
				if(allFilled==0)
				{
					$('.assignMicrositeSave').attr('disabled',true);
				}
				else
				{	
					$('.assignMicrositeSave').attr('disabled',false);
				}
			}
			
			$('.LP_MicroSite td:nth-child(3)').each(function() 
			{
				var trgtMicrositeId=$(this).find("select").val();
				console.log(trgtMicrositeId);
				if(trgtMicrositeId!= null || trgtMicrositeId.length!=0 || trgtMicrositeId!='')
				{
					saveAssignedMicrosite();
				}
			});
			
			// function to save target microsite json into db
			function saveAssignedMicrosite() 
			{	
				$.ajax(
				{
					type: 'post',
					url: '<?php echo base_url();?>package_info/Save_MicrositeJSON_LP',
					data: {packageId: package_id,JSONFormed:JSONFormed,micrositesList:micrositesList},
					dataType:'json',
					beforeSend: function()
					{									
					},
					success: function(data)
					{
						$('.MSSaved').show().delay(1000).fadeOut(500);
						//alert('success');
						$('.assignMicrositeSave').attr('disabled',true);
					},
					error: function() 
					{
						alert('error');
					}
				});
			}		
		});
		
		
		// $('.Assets_Duplicates td:nth-child(5)').each(function() 
		// {
			/*
			$(document).on('change', ".trgtDupList", function () 
			{
				var status4different = $(this).find('option:selected').data('value2');
				if(status4different=='Different')
				{
					$('.WarningDifferent').show();
					$('.assignDuplicateAssetSave').attr('disabled',true);
				}
				else
				{
					$('.WarningDifferent').show();
					$('.assignDuplicateAssetSave').attr('disabled',false);
				}
			});
			*/
		// });

		//Ajax Call for Display Duplicates found in target
		var loadingDup = false;
		$('#DuplicateAssetTab').click(function()
		{	
			var status = $('.status_cursor').text();
			if(status == 'Deploy Completed' || status == 'Errored')
			{
				$(".assignDuplicateAssetSave").addClass('disabled');
			}
			if (loadingDup) 
			{
                return ;
            }
            loadingDup = true;
			var package_id = $('.package_id_hidden').val();
			$.ajax({
				type: 'post',
				url: '<?php echo base_url();?>package_info/targetDuplicatesAssets',
				data: {packageId: package_id},
				dataType:'json',
				beforeSend: function()
				{
					var spinner='';
					$('.tableRow').hide();
					if($('.Assets_Duplicates').find(".glyphicon-refresh-animate"))
					{
						$('.loader').remove();
					}
					spinner +='<div class="loader" style="margin-left: 210px !important;"><span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span></div>';
					$('.Assets_Duplicates').append(spinner);
					$('.assignDuplicateAssetSave').hide();
					
				},
				success: function(data)
				{
					$('.assignDuplicateAssetSave').show();
					if(data=='')
					{
						$('.tableRow').remove();
						out+='<tr class="tableRow"><td></td><td></td><td style="font-style:italic">No Duplicate Assets found in package</td><td></td><td></td></tr>';
						$('.Assets_Duplicates').append(out);
						$('.assignDuplicateAssetSave').hide();
					}
					var out='';
					$('.loader').hide();
					$.each(data, function (Dupkey , Dupval) 
					{
						var targetDupAssets = gettargetDuplicateAssetsOptionValue(Dupval.Target_Duplicate_Assets);
						var DupSelect = '<select class="trgtDupList" style="padding: 0 0 0 0;width: 230px !important;height:24px !important;" size="1">';
						DupSelect +='<option selected="selected" disabled>--Select--</option>';
						$.each(targetDupAssets, function (key , val) 
						{
							if(Dupval.Target_Asset_Id == val.id)	
							{	
								var selected = '';
								selected = 'selected'
								DupSelect += "<option selected value='" + val.id + "'>" + val.name +" ( "+ val.id + " )</option>";
							}
							else
							{	
								var selected = '';
								selected = 'selected';
								if(val.status=='Different')
								{
									DupSelect += "<option value='" + val.id + "'disabled>" + val.name +" ( "+ val.id + " )</option>";
								}
								else
								{
									DupSelect += "<option value='" + val.id + "'>" + val.name +" ( "+ val.id + " )</option>";
								}
							}
							// Show warning message for different email group and form html  name
							if(val.status=='Different')
							{
								if(val.type=="Form")
								{
									$('.FormWarningDifferent').show();
								}
								else if(val.type=="Email")
								{
									$('.EmailWarningDifferent').show();
								}
							}	
						});
						var obj_ = $.parseJSON(Dupval.Target_Duplicate_Assets);
						DupSelect += "</select>";
						out += '<tr class="tableRow"><td  >'+obj_.parent_asset_type+'</td> <td >'+obj_.parent_asset_name+'</td><td class="AssetType">'+Dupval.Asset_Type+'<input type=hidden class=DPI_Id value='+Dupval.Deployment_Package_Item_Id+'><input type=hidden class=AssetId value='+Dupval.Asset_Id+'></td><td class="AssetName">'+Dupval.Asset_Name+'</td><td>'+DupSelect+'</td></tr>';
					});	
					$('.Assets_Duplicates').append(out);
					var status = $('.status_cursor').text();
					if(status == 'Deploy Completed' || status == 'Errored' || status == 'Action Required')
					{
						$(".assignDuplicateAssetSave").addClass('disabled');
						$('.trgtDupList').prop('disabled', true);
					}	
					loadingDup = false;
				},
				error: function() 
				{
					// alert('error');
					loadingDup = false;
				}
			});
		});	
		
		function gettargetDuplicateAssetsOptionValue(targetDupAssets)
		{
			var objTemp = $.parseJSON(targetDupAssets);
			var obj = objTemp.duplicates_from_target;
			var DuplicateArray= [];
			$.each(obj, function (key , val) 
			{	
				var obj = {"id":val.Asset_Id, "name":val.Asset_Name , "status":val.Status ,"type":val.Asset_Type};
				DuplicateArray.push(obj);
			});
			return DuplicateArray;
		}
		
		//save selected asset's target Id to db
		$('.assignDuplicateAssetSave').click(function()
		{
			if($(this).hasClass('disabled'))
			{
				return false;
			}
			var package_id = $('.package_id_hidden').val();
			var JSONFormed= [];
			var saveStatus=false;
			var selectNotEmpty=0;
			$('.Assets_Duplicates td:nth-child(5)').each(function() 
			{
				var trgtAssetName=$(this).find("select").val();
				if(trgtAssetName== null || trgtAssetName.length==0 || trgtAssetName=='')
				{
					$(this).find("select").css('border-color', '#C70039');
					$('.assignDuplicateAssetSave').attr('disabled',true);
				}
				else
				{
					$(this).find("select").css('border-color', '#E5E7E9');
				}
			});
			
			$('.Assets_Duplicates > tbody  > tr:not(:first)').each(function() 
			{
				$('.trgtDupList').on('change', function() 
				{
					$(this).css('border-color', '#E5E7E9');
					enableDisableSaveButton();
				});
					
				var DPI_Id=$(this).find('.DPI_Id').val();	
				var AssetId=$(this).find('.AssetId').val();	
				var AssetType=$(this).find('.AssetType').text();
				var AssetName=$(this).find('.AssetName').text();
				var TargetAssetId=$(this).find("select").val();
				var obj = {"DPI_Id":DPI_Id,"AssetId":AssetId, "AssetType":AssetType,"AssetName":AssetName,"TargetAssetId":TargetAssetId};
				JSONFormed.push(obj);
				saveStatus=true;
			});
			
			function enableDisableSaveButton()
			{
				var allFilled = 1;
				$('.Assets_Duplicates td:nth-child(5)').each(function() 
				{
					var trgtAssetName=$(this).find("select").val();
					if(trgtAssetName.length==0 || trgtAssetName=='')
					{
						allFilled = 0;
						$('.assignDuplicateAssetSave').attr('disabled',true);
					}
					else
					{
						$(this).find("select").css('border-color', '#E5E7E9');
					}
				});
				if(allFilled==0)
				{
					$('.assignDuplicateAssetSave').attr('disabled',true);
				}
				else
				{
					$('.assignDuplicateAssetSave').attr('disabled',false);
				}
			}
			
			$('.Assets_Duplicates td:nth-child(5)').each(function() 
			{
				var trgtAssetName=$(this).find("select").val();
				if(trgtAssetName.length!=0 || trgtAssetName!='')
				{
					saveAssignedDuplicateAsset();
				}
			});	
			
			// function to save target asset id of duplicate asset in to db
			function saveAssignedDuplicateAsset() 
			{	
				$.ajax(
				{
					type: 'post',
					url: '<?php echo base_url();?>package_info/Save_SelectedTargetAssetId',
					data: {packageId: package_id,JSONFormed:JSONFormed},
					dataType:'json',
					beforeSend: function()
					{									
					},
					success: function(data)
					{
						$('.DuplicateSaved').show().delay(1000).fadeOut(500);
					},
					error: function() 
					{
						
					}
				});
			}
		});

		/* Date : 17-05-2018 , Pursose: Save Default Microsite */
		$('.saveDefaultMS').click(function()
		{
			var package_id = $('.package_id_hidden').val();
			if($('.DMSList')!=null || $('.DMSList')!=0 )
			{
				var DMSID = $('.DMSList').val();
				$.ajax(
				{
					type: 'post',
					url: '<?php echo base_url();?>package_info/saveDefaultMicrosite',
					data: {packageId: package_id,DMSID:DMSID,micrositesList:micrositesList},
					dataType:'json',
					beforeSend: function()
					{									
					},
					success: function(data)
					{
						$('.MSSaved').show().delay(1000).fadeOut(500);
						//$('.assignMicrositeSave').attr('disabled',true);
					},
					error: function() 
					{
					}
				});
			}
		});	
		
		
	});	
	
		function enableDisableSaveBtn(self)
		{
			if($(self).val() !== 0)
			{
			  $('.assignMicrositeSave').attr('disabled',false);;
			}
		}
	
	
	
	
	</script>
	
	<!-- JAVASCRIPT FILES -->
	<script type="text/javascript">var plugin_path = 'assets/plugins/';</script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/app.js"></script>
	<script type="text/javascript">
	loadScript(plugin_path + "nestable/jquery.nestable.js", function()
	{
		if(jQuery().nestable) {
			var updateOutput = function (e) {
				var list = e.length ? e : $(e.target),
					output = list.data('output');
					
				if (window.JSON) {
					//output.val(window.JSON.stringify(list.nestable('serialize'))); //, null, 2));
				} else {
					//output.val('JSON browser support required for this demo.');
				}
			};

			// Nestable list 1
			jQuery('#nestable_list_1').nestable({
				group: 1
			}).on('change', updateOutput);
			// Nestable list 1
			jQuery('#nestable_list_2').nestable({
				group: 1
			}).on('change', updateOutput);


			// output initial serialised data
			updateOutput(jQuery('#nestable_list_1').data('output', jQuery('#nestable_list_1_output')));
			updateOutput(jQuery('#nestable_list_2').data('output', jQuery('#nestable_list_2_output')));
			
			// Expand All
			jQuery("button[data-action=expand-all]").bind("click", function() {
				jQuery('.dd').nestable('expandAll');
			});

			// Collapse All
			jQuery("button[data-action=collapse-all]").bind("click", function() {
				jQuery('.dd').nestable('collapseAll');
			});
		}
	});
	
	function addtopackage(package_id,Asset_Type,Asset_Id,Asset_Name,btn)
	{		
		var tempdata= {
				Deployment_Package_Id: package_id,
				Asset_Type: Asset_Type,
				Asset_Id: Asset_Id,
				Asset_Name: Asset_Name,
				verified:1,
				User_Added: 1,
		};
	
		var destination = $('.target_system').val();
		if(destination != "" && destination != "NULL")
		{
			$.ajax(
			{
				type: 'post',
				url: '<?php echo base_url();?>package_info/addtopackage',
				data: tempdata,
				beforeSend: function()
				{
					$(btn).css('color','#C4C4C6');
				},
				success: function(data)
				{   
					getPackageinfo();
					$('#orgID1').prop('disabled', true);
					$('#orgID2').prop('disabled', true);
					$('.additem_').css('color','#46b8da');
					$('.ItemAddSuccessMessage').show().delay(2000).fadeOut(1000);
					
				},
				error: function()
				{
					result = false;
				}
			});
		}
		else
		{
			alert("Please select Target System first");
			var val = Asset_Id;
			$('input:checkbox[value="' + val + '"]').attr('checked', false);
		}
	 
	}
	
	
	
	function additem()
	{
		$('.additem').off('click');
		$('.additem').click(function()
		{
			if (this.checked) 
			{
				var package_id = $('.package_id_hidden').val();
				var Asset_Type = $(this).parent().parent().parent().find('input[name="RSys_Asset_Type_Id"]').val();
				if(Asset_Type=='' || Asset_Type == null || Asset_Type == 'undefined')
				{
					Asset_Type = $(this).parent().find('input[name="Asset_Type"]').val();
				}
				var Asset_Id = $(this).val();
				var Asset_Name = $(this).parent().find('.asset_item').html();
				addtopackage(package_id, Asset_Type, Asset_Id, Asset_Name);
				$('.ValidatePackage').removeAttr("disabled");
				//$('.DeployPackage').removeAttr("disabled");
				$('#gridWrapper').show();
			}
		});
	}
	
	function additem_new()
	{
		$('.additem_').click(function()
		{
			var package_id = $('.package_id_hidden').val();
			var Asset_Type = $(this).parent().parent().parent().parent().find('input[name="RSys_Asset_Type_Id"]').val();
			if(Asset_Type=='' || Asset_Type == null || Asset_Type == 'undefined')
			{
				Asset_Type = $(this).parent().find('input[name="Asset_Type"]').val();
				
			}
			if(Asset_Type=='' || Asset_Type == null || Asset_Type == 'undefined')
			{
				Asset_Type = $(this).parent().parent().find('input[name="Asset_Type"]').val();
				
			}
			Asset_Id = $(this).parent().parent().find('#AssetId').val();
			var Asset_Name = $(this).parent().parent().find('.asset_item').html();
			if(package_id !='undefined' && Asset_Type !='undefined'&& Asset_Id !='undefined' && Asset_Name !='undefined')  
			{
				addtopackage(package_id, Asset_Type, Asset_Id, Asset_Name ,this);
			}	
			
			$('.ValidatePackage').removeAttr("disabled");
		//	$('.DeployPackage').removeAttr("disabled");
			$('#gridWrapper').show();
			
		});
	}
	
	$('.edit').click(function(){
		$(this).hide();
		$('#Package_name_label').hide();
		$('#package_name_id').show();
		//$('#package_description').show();
	});
	
	$('.editNew').click(function(){
		$(this).hide();
		$('#package_description_label').hide();
		$('#package_description').show();
	});
	
	var status = $('.status_cursor').text();
	buttonEnableDisable(status);
	
	function buttonEnableDisable(status)
	{
			var statusMissing = status;
			
			$('.missingAssetsMessage').hide()
			if(statusMissing== 'Missing Assets')
			{
				$('.missingAssetsMessage').show();
			}
			
			$('.missingTokens').hide();
			if(statusMissing== 'Missing Tokens')
			{
				checkForTokenAvailability('missingTokens');
			}
			
			var statusDplyCmpltd = status;
			if(statusDplyCmpltd == 'Deploy Completed' || statusDplyCmpltd == 'Deployment In Queue' || statusDplyCmpltd == 'Action Required')
			{
				$(".ValidatePackage").attr('disabled',true);
				$(".DeployPackage").attr('disabled',true);
				$(".package_name ").attr("disabled",true);
				$(".package_description ").attr("disabled",true);
				$(".create_new_Package ").addClass("disabled");
			}
			
			var statusDplyVldtQueue = status;
			if(statusDplyVldtQueue == 'Validate In Queue' || statusDplyVldtQueue == 'Deployment In Queue')
			{
				$("#jqgrid").prop('disabled',true);
				$("#delBtn").attr('disabled',true);
				$(".ValidatePackage").attr('disabled',true);
				$(".DeployPackage").attr('disabled',true);
				$(".ValidationReport").attr('disabled',true);
				$(".package_name ").attr("disabled",true);
				$(".package_description ").attr("disabled",true);
				$(".create_new_Package ").addClass("disabled");
			}
			
			var newButtonstatus = status;
	
	
			if(newButtonstatus == 'New')
			{	
				//VIkas : 29082017
				//$(".DeployPackage").attr('disabled',true);
				//$(".ValidationReport").attr('disabled',true); //changed by anamika 17-05-2018
			}
			
			var status_unsupported = status;
			
			if(status_unsupported == 'Unsupported')
			{	
				//VIkas : 29082017
				$(".DeployPackage").attr('disabled',true);
			}
			
			var status_Errored = status;
			if(status_Errored == 'Errored')
			{	
				$(".DeployPackage").attr('disabled',true);
				$(".ValidatePackage").attr('disabled',true);
				$(".package_name ").attr("disabled",true);
				$(".package_description ").attr("disabled",true);
				$(".create_new_Package ").addClass("disabled");
			}
			
			var status_Invalid = status;
			if(status_Errored == 'Invalid')
			{	
				$(".DeployPackage").attr('disabled',true);		
			}
	}
	
	$('.dependent_child').click(function ()
	{	
		if($(this).attr('aria-expanded') == "false")
		{
			$(this).find(".fa-plus-square-o").addClass("fa-minus-square-o").removeClass("fa-plus-square-o");
		}
		else
		{
			$(this).find(".fa-minus-square-o").addClass("fa-plus-square-o").removeClass("fa-minus-square-o");
		}
	});
	
	// $('.dependent_invalid_child').click(function ()
	// {	
		// if($(this).attr('aria-expanded') == "false")
		// {
			// $(this).find(".fa-plus-square-o").addClass("fa-minus-square-o").removeClass("fa-plus-square-o");
		// }
		// else
		// {
			// $(this).find(".fa-minus-square-o").addClass("fa-plus-square-o").removeClass("fa-minus-square-o");
		// }
	// });
	
	function getChildInfo(datavalue)
	{
		var package_id = $('.package_id_hidden').val();
		var packageList= $('.dependent_child_header');
		$.ajax(
		{
			type: 'post',
			url: 'package_info/getchildinfo',
			data: datavalue,
			dataType:'json',
			beforeSend: function()
			{
				//alert(this);
				$('.childList').html('');
				$('.dependent_childInfo .childValue').hide();				
				var child_asset_type = $('.dependent_child').parent().find(this).show();packageList.html('<div class="list-group-item " style="text-align: center;" ><div class="dependent_child_refresh glyphicon glyphicon-refresh glyphicon-refresh-animate"></div></div>');
			},
			success: function(data)
			{
				
		
				packageList.html('');
				$('.ValidatePackage span').remove();
				$('.dependent_child_refresh').remove();
				$('.childList').html('');
				if(data.length == "" || data.length < 0 || data == false)
				{
					packageList.html('No child assets found');
				}
				else
				{
					var childIdList = [];					
					$(data).each(function(){	
						var flag=true;
						var out='';
						var dataAsset_Id=this.Asset_Id;
						var dataAsset_Type=this.Asset_Type;
						$(childIdList).each(function(){
							if(dataAsset_Id == this.Asset_Id && dataAsset_Type == this.Asset_Type ){
								flag = false;
							}
						});
						if(flag){							
							childIdList.push({Asset_Id:this.Asset_Id,Asset_Type:this.Asset_Type});
							var child_asset_type = $('.dependent_child').parent().find('#child_asset_type'+this.Asset_Type.replace(" ","_"));
							
							$('.dependent_childInfo .childValue'+this.Asset_Type.replace(" ","_")).show();
							// out += 	'<div class="list-group-item "><input name="Asset_Type"  type="hidden" value="'+this.Asset_Type+'"/><input class="additem"  type="checkbox" name="chkItem" value="'+this.Asset_Id+'"/><a href="#id_'+ this.Asset_Id +'" class="asset_item" data-toggle="collapse" >'+this.Display_Name +'</a></div>';
							var newButtonstatus = $('.status_cursor').text();
							if(newButtonstatus == 'Deploy Completed' || newButtonstatus == 'Deployment In Queue' || newButtonstatus == 'Validate In Queue' || newButtonstatus == 'Errored' || newButtonstatus == 'Action Required')
							{
								out += 	'<div class="list-group-item "><input name="Asset_Type"  type="hidden" value="'+this.Asset_Type+'"/><input id="AssetId" name="Asset_Type"  type="hidden" value="'+this.Asset_Id+'"/><a><i class="fa fa-plus-square additem_" style="color: #46b8da;display:none;" aria-hidden="true" title="Click to add asset"></i></a><a href="#id_'+ this.Asset_Id +'" class="asset_item" data-toggle="collapse" style="cursor:default;">'+this.Display_Name +'</a></div>';
								child_asset_type.append(out);
							}
							else
							{
								out += 	'<div class="list-group-item "><input name="Asset_Type"  type="hidden" value="'+this.Asset_Type+'"/><input id="AssetId" name="Asset_Type"  type="hidden" value="'+this.Asset_Id+'"/><a><i class="fa fa-plus-square additem_" style="color: #46b8da;" aria-hidden="true" title="Click to add asset"></i></a><a href="#id_'+ this.Asset_Id +'" class="asset_item" data-toggle="collapse" style="cursor:default;">'+this.Display_Name +'</a></div>';
								child_asset_type.append(out);
							}
							
						}
					});
					additem_new();
				}
				var statusDupFound = $('.status_cursor').text();
				if(statusDupFound == 'Deploy Completed' || statusDupFound == 'Action Required')
				{	
					$(".additem_").css("cursor","default");
					$(".additem_").removeAttr("title");
				}	
			},
			error: function() 
			{
				//packageList.html('');	
				packageList.html('Connection Timeout Error');
			}
		}); 
	}
	
	function getMissingAsset()
	{
		var package_id = $('.package_id_hidden').val();
		
		var packageList= $('.dependent_child_header');
		$.ajax(
		{
			type: 'post',
			url: 'package_info/getAssetReport',
			data: {Package_Id : package_id},
			dataType:'json',
			beforeSend: function()
			{
				$('.childList').html('');
				$('.dependent_childInfo .childValue').hide();				
				var child_asset_type = $('.dependent_child').parent().find(this).show();packageList.html('<div class="list-group-item " style="text-align: center;" ><div class="dependent_child_refresh glyphicon glyphicon-refresh glyphicon-refresh-animate"></div></div>');
			//	alert("before ajax");
				// $('.verify_existing').addClass('disabled');
				// $('.verify_missing').addClass('disabled');
			},
			success: function(data)
			{
				//alert("Success");
				packageList.html('');
				$('.ValidatePackage span').remove();
				$('.dependent_child_refresh').remove();
				$('.childList').html('');
				if(data.length == "" || data.length < 0 || data == false)
				{
					packageList.html('No child assets found');
				}
				else
				{
					var childIdList = [];	
					var childIdList_missing = [];						
					$(data).each(function(){
						var flag=true;
						var out='';
						var dataAsset_Id=this.Asset_Id;
						var dataAsset_Type=this.Asset_Type;
						if(this.missing_target == 1)
						{
							$(childIdList_missing).each(function(){
								if(dataAsset_Id == this.Asset_Id && dataAsset_Type == this.Asset_Type ){
									flag = false;
								}
							});
							if(flag)
							{	
  							
								childIdList_missing.push({Asset_Id:this.Asset_Id,Asset_Type:this.Asset_Type});
								var child_asset_type = $('.dependent_child').parent().find('#Missing_child_asset_type'+this.Asset_Type.replace(" ","_"));
								
								$('.dependent_childInfo .childValue'+this.Asset_Type.replace(" ","_")).show();
								
								var newButtonstatus = $('.status_cursor').text();
								
								if(newButtonstatus == 'Deploy Completed' || newButtonstatus == 'Deployment In Queue' || newButtonstatus == 'Validate In Queue' || newButtonstatus == 'Errored' || newButtonstatus == 'Action Required')
								{
									out += 	'<div class="list-group-item "><input name="Asset_Type"  type="hidden" value="'+this.Asset_Type+'"/><input id="AssetId" name="Asset_Type"  type="hidden" value="'+this.Asset_Id+'"/><a><i class="fa fa-plus-square additem_" style="color: #46b8da;display:none" aria-hidden="true" title="Click to add asset"></i></a><a href="#id_'+ this.Asset_Id +'" class="asset_item" data-toggle="collapse" style="cursor:default;color: black;">'+this.Asset_Name +'</a></div>';
								}
								else
								{
									out += 	'<div class="list-group-item "><input name="Asset_Type"  type="hidden" value="'+this.Asset_Type+'"/><input id="AssetId" name="Asset_Type"  type="hidden" value="'+this.Asset_Id+'"/><a><i class="fa fa-plus-square additem_" style="color: #46b8da;" aria-hidden="true" title="Click to add asset"></i></a><a href="#id_'+ this.Asset_Id +'" class="asset_item" data-toggle="collapse" style="cursor:default;color: black;">'+this.Asset_Name +'</a></div>';
								}			
								child_asset_type.append(out);
								var status = $('.status_cursor').text();
								if(status == 'Deploy Completed' || status == 'Errored' || status == 'Action Required')
								{
									$('.verify_missing').addClass('disabled');
								}
								else
								{
									$('.verify_missing').removeClass('disabled');
								}
							}
						}
						else if(this.missing_target == 0)
						{
							$(childIdList).each(function(){
								if(dataAsset_Id == this.Asset_Id && dataAsset_Type == this.Asset_Type ){
									flag = false;
								}
							});
							if(flag)
							{							
								childIdList.push({Asset_Id:this.Asset_Id,Asset_Type:this.Asset_Type});
								var child_asset_type = $('.dependent_child').parent().find('#Existing_child_asset_type'+this.Asset_Type.replace(" ","_"));
								$('.dependent_childInfo .childValue'+this.Asset_Type.replace(" ","_")).show();
								
								var newButtonstatus = $('.status_cursor').text();
								
								if(newButtonstatus == 'Deploy Completed' || newButtonstatus == 'Deployment In Queue' || newButtonstatus == 'Validate In Queue' || newButtonstatus == 'Errored' || newButtonstatus == 'Action Required')
								{
									out += 	'<div class="list-group-item "><input name="Asset_Type"  type="hidden" value="'+this.Asset_Type+'"/><input id="AssetId" name="Asset_Type"  type="hidden" value="'+this.Asset_Id+'"/><a><i class="fa fa-plus-square additem_" style="color: #46b8da;display:none;" aria-hidden="true" title="Click to add asset"></i></a><a href="#id_'+ this.Asset_Id +'" class="asset_item" data-toggle="collapse" style="cursor:default;color: black;">'+this.Asset_Name +'</a></div>';
								}
								else
								{
									out += 	'<div class="list-group-item "><input name="Asset_Type"  type="hidden" value="'+this.Asset_Type+'"/><input id="AssetId" name="Asset_Type"  type="hidden" value="'+this.Asset_Id+'"/><a><i class="fa fa-plus-square additem_" style="color: #46b8da;" aria-hidden="true" title="Click to add asset"></i></a><a href="#id_'+ this.Asset_Id +'" class="asset_item" data-toggle="collapse" style="cursor:default;color: black;">'+this.Asset_Name +'</a></div>';
								}
								child_asset_type.append(out);
								var status = $('.status_cursor').text();
								if(status == 'Deploy Completed' || status == 'Errored' || status == 'Action Required')
								{
									$('.verify_existing').addClass('disabled');
								}
								else
								{
									$('.verify_existing').removeClass('disabled');
								}
							}
						}
					});
					
					$('.v_report').each(function(){
						if(!($(this).find('.list-group-item').length))
						{
							$(this).parent().hide();
						}
					});
					
					additem_new();
				}
				var statusDplyCmpltd = $('.status_cursor').text();
				if(statusDplyCmpltd == 'Deploy Completed' || statusDplyCmpltd == 'Action Required')
				{
					$(".additem_").css("cursor","default");
					$(".additem_").removeAttr("title");
				}
			},
			error: function() 
			{
				packageList.html('Connection Timeout Error');
			}
		}); 
	}
	
	function getFolderStructure(packageItem)
	{
		var assetList= $('.Folder_structure');
		//alert(packageItem);
		$("#folder_container").html('');
		$.ajax(
		{
			
			type : 'get',
			url : 'package_info/getFolderStructure?package_item_id='+packageItem,
			dataType: 'json',
			beforeSend: function()
			{
				assetList.show();
			},
			success: function(data)
			{
				console.log(data);
				$('#deploymentPackageItem').val(packageItem);
				$('#folderId').val(0);
				foldersStructure(data);				
				collapseAllFolders();
				//viewMappedTree(4837,data);
			},
			error: function() 
			{
				assetList.html('Connection Timeout Error');
			}		
		});
	}
	
	$('.Folder_structure').on('click','.chooseFolder',function(){
		var selectedFolder = $('#folderId').val();
		var packageItem = $('#deploymentPackageItem').val();
		if(selectedFolder == 0)
		{
			alert('No folder was selected');
		}
		else
		{
			$.ajax(
			{
				
				type : 'get',
				url : 'package_info/updateFolderForPackageItem?package_item_id='+packageItem+'&folderId='+selectedFolder,
				dataType: 'json',
				beforeSend: function()
				{
					
				},
				success: function()
				{
					
				},
				error: function() 
				{
					
				}
			});
			
		}
	});
	
	var calledBuild = false;
	var MapedFolderId = 0;
	function foldersStructure(folderjson)
		{
			//console.log(folderjson);
			$.each(folderjson, function(key, val) 
			{
				if(val.description=="Root" && val.isSystem=="true")// || val.folderId !='')
				// if(val.folderId !='' )
				{
					//root=val.id;
					//root=val.folderId;
					root=val.id;
					folderjson=setParent(folderjson);
					//if(!calledBuild)
					{
						//calledBuild = true;
						
						$("#folder_container").append( buildNestedList(folderjson,root));
					}
				}
				if(MapedFolderId>0)
				{					
					$("#folderId").val(MapedFolderId);
					isParent = true;
				}
			});
		}
		
		function viewMappedTree(MapedFolderId,folderjson)
		{
			// console.log(folderjson);
			console.log("Mapped Folder Id "+ MapedFolderId);
			for(var i=0;i<folderjson.length;i++)
			{
				// console.log(folderjson[i].id);
				if(folderjson[i].id==MapedFolderId)
				{	console.log(folderjson[i].id);
					$('#'+folderjson[i].id).parent().parent().show();
					$('#'+folderjson[i].id).parent().parent().parent().children('span.Collapsable').find('span').find('label').addClass('fa-minus-square-o').removeClass('fa-plus-square-o');
					viewMappedTree(folderjson[i].folderId,folderjson);
				}	
			}	
		}
		
		function buildNestedList(treeNodes, rootId) 
		{	
			var nodesByParent = {};

			$.each(treeNodes, function(i, node) 
			{
				if (!(node.folderId in nodesByParent)) nodesByParent[node.folderId] = [];
				nodesByParent[node.folderId].push(node);
			});
			var childFlag = true;
			function buildTree(children) 
			{	
				var $container = $("<ul>");
				
				if (!children) 
				{	
					childFlag=false;
					return;
				}
				
				$.each(children, function(i, child) 
				{
					
					if(MapedFolderId>0 && MapedFolderId ==child.id )
					{
						if(child.parent==true)
						{
							$("<li><span class=\"Collapsable\"> <span class=\"fa fa-plus-square-o\"></span>   </span><img width =\"15\" src=\"/assetnamingassistant/assets/image/check.png\" class=\"checkimg\" id=\""+child.id+"\"/> " + "<span class=\"longFolderName\" id=\"childName\" style=\"text-align:left\">" + child.name + "</span>" + "</span>")
						
						.appendTo($container)
						.append( buildTree(nodesByParent[child.id]) );
						}
						else
						{
							$("<li><span class=\"Collapsable\"> <span class=\"fa fa-minus-square-o\"></span>   </span><img width =\"15\" src=\"/assetnamingassistant/assets/image/check.png\" class=\"checkimg\" id=\""+child.id+"\"/> " +"<span class=\"longFolderName\" id=\"childName\" style=\"text-align:left\">" + child.name + "</span>"  + "</span>")
						
						.appendTo($container)
						.append( buildTree(nodesByParent[child.id]) );
						}
						
					}
					else
					{
						if(child.parent==true)
						{
							$("<li><span class=\"Collapsable\"> <span class=\"fa fa-plus-square-o\" id=\"folderSign\"></span>   </span><img width =\"15\" src=\"/assetnamingassistant/assets/image/uncheck.png\" class=\"uncheckimg\" id=\""+child.id+"\"/> " + "<span class=\"longFolderName\" id=\"childName\" style=\"text-align:left\">" + child.name + "</span>" + "</span>")
							
							.appendTo($container)
							.append( buildTree(nodesByParent[child.id]) );
						}
						else
						{
							$("<li><span class=\"Collapsable\"> <span class=\"fa fa-minus-square-o\" id=\"folderSign\"></span>   </span><img width =\"15\" src=\"/assetnamingassistant/assets/image/uncheck.png\" class=\"uncheckimg\" id=\""+child.id+"\"/> " + "<span class=\"longFolderName\" id=\"childName\" style=\"text-align:left\">" + child.name + "</span>" + "</span>")
							
							.appendTo($container)
							.append( buildTree(nodesByParent[child.id]) );
						}
					}
				});
			
				return $container;
			}
		  return buildTree(nodesByParent[rootId]);
		}
		
	function setParent(folderjson)
	{
		for(var i = 0; i<folderjson.length;i++)
		{
			folderjson[i]['parent'] = false;
			for(var j = 0; j<folderjson.length;j++)
			{
				//if(typeof folderjson[i].description == 'undefined')
				{
					if(folderjson[i].id==folderjson[j].folderId)
					{
						folderjson[i]['parent'] = true;
					}
				}
			}
		}
		//console.log(folderjson);
		return folderjson;
	}
	
	$(".Folder_structure").on("click",".Collapsable",function () 
	{
		var a = $(this).children('span');
		if(a.attr("class")=="fa fa-minus-square-o")
		{
			a.removeClass("fa fa-minus-square-o");
			a.addClass("fa fa-plus-square-o");
			
		}
		else{
			a.removeClass("fa fa-plus-square-o");
			a.addClass("fa fa-minus-square-o");
		}
		
		$(this).parent().children().toggle();
		$(this).toggle();

	});
	
	function collapseAllFolders()
	{
		$(".Folder_structure .Collapsable").each(function()
		{
			$(this).parent().children().toggle();
			$(this).toggle();
		});
	}
	
	$(".Folder_structure").on("click","li img",function () 
	{
		if($(this).attr("class")=="uncheckimg")
		{
			$(this).attr("class","checkimg");
			$("#createRename").attr('disabled', false);// inserted this line
			$(this).attr("src","/assetnamingassistant/assets/image/check.png");
			//console.log($(this).attr("id"));
			var id = $(this).attr("id");
			$("#folderId").val(id);
			uncheckImg(id);
		}
		
		else if($(this).attr("class")=="checkimg")
		{
			$(this).attr("class","uncheckimg");
			$(this).attr("src","/assetnamingassistantOAuth/assets/image/uncheck.png");
			$("#folderId").val(null);
		}

	});
	
	function uncheckImg(id)
	{
		$("#folder_container img[class='checkimg']").each(function()
		{
			if($(this).attr("id")!=id)
			{
				$(this).attr("class","uncheckimg")
				$(this).attr("src","/assetnamingassistant/assets/image/uncheck.png")

			}
		});
		
	}


	$('.ValidatePackage').click(function()
	{
		var package_id = $('.package_id_hidden').val();
		var datavalue = {packageId : package_id};
		//var result = checkForTokenAvailability('Validate');
		$.ajax(
		{
			type: 'post',
			url: '<?php echo base_url();?>package_info/defaultMicrositeAssignedStatus',
			data: datavalue,
			dataType:'json',
			beforeSend: function()
			{				
			},
			success: function(data1)
			{
				if(data1==1){
					$('.ValidatePackage span').remove();
					$('.DeployPackage span').remove();
					$('.Default_Microsite').show();
					$('.Default_Microsite_Message').html("Default microsite is not assigned. Open Validation Report and click on Assign Microsite.");
				}
				else{
					var result = checkForTokenAvailability('Validate');
				}
			},
			error: function(data2) 
			{
				
				//alert('error');
			}
		});			
	});
	
	function checkForTokenAvailability(action)
	{
		var package_id = $('.package_id_hidden').val();
		var datavalue = {PackageId : package_id};
		$.ajax(
		{
			type: 'post',
			url: '<?php echo base_url();?>deploy/checkForTokenAvailability',
			data: datavalue,
			dataType:'json',
			beforeSend: function()
			{
				if(action == 'Validate')
				{
					$('.ValidatePackage span').remove();
					$('.ValidatePackage').append(' <span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span>');
				}
				else if(action == 'Deploy')
				{
					$('.DeployPackage span').remove();
					$('.DeployPackage').append(' <span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span>');
				}
				//pageReload();
			},
			success: function(data1)
			{
				//console.log("hello "+data1.message);
				$('.ValidatePackage span').remove();
				$('.DeployPackage span').remove();
				//$('.footer_deploy_button').after('<p style="margin-bottom: 2px;">This Package Validation is in queue.</p>');
				var finalResult;
				if(data1.message == "sourceTokenMissing")
				{
					finalResult =  "Tokens for source system are corrupted, Please reinstall Transporter App in the source system.";
				}
				else if(data1.message == "targetTokenMissing")
				{
					finalResult =  "Tokens for target system are corrupted, Please reinstall Transporter App in the target system.";
				}
				else if(data1.message == "bothTokensMissing")
				{
					finalResult =  "Tokens for source and target systems are corrupted, Please reinstall Transporter App in both the systems.";
				}
				else if(data1.message == "success")
				{
					finalResult =  data1.message;
				}
				else
				{
					finalResult =  "Unable to reach the server. Try after some time.";
				}
				
				if(data1.message != "success")
				{
					if(action == 'Validate' || action == 'Deploy')
					{
						$('.Token_verification').show();
						$('.Token_Error_Message').html(finalResult);
					}
					else if(action == 'missingTokens')
					{
						$('.missingTokens').show();
						$('.missingTokens').html(finalResult);
					}
					
				}
				else
				{
					if(action == 'Validate')
					{
						$('.validate_package').show();
						$('.deleteBtn').attr("disabled",true);
						$('.viewChildBtn').attr("disabled",true);
					}
					else if(action == 'Deploy')
					{ 
				        
						if($('.status_cursor').text() == "Validate Completed")
						{
							$('.deploy_package #myCheck').show();
						}
						else
						{
							$('.deploy_package #myCheck').hide();
						}
							
						$('.deploy_package').show();
					}		
				}
			},
			error: function(data2) 
			{
				$('.ValidatePackage span').remove();
				$('.DeployPackage span').remove();
				$('.Token_verification').show();
				$('.Token_Error_Message').html("Unable to reach the server. Try after some time.");
			}	
		});
	}
	
	$('.ValidatePackage_Auto').click(function ()
	{
		var package_id = $('.package_id_hidden').val();
		var datavalue = {PackageId : package_id, Auto : 1};
		
		$.ajax(
		{
			type: 'post',
			// url: 'package_info/validatepackage',
			url: '<?php echo base_url();?>deploy/validationinqueue',
			data: datavalue,
			dataType:'json',
			beforeSend: function()
			{
				
				$('.ValidatePackage span').remove();
				$('.ValidatePackage').append(' <span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span>');
				$('.ValidatePackage').prop("disabled",'true');
				$('.ValidatePackage').off('click');
				$('.validate_package').hide();
				$('#loaderOnStatusBar').show();
				getTotalValidateAssets();
			},
			success: function(data1)
			{
				$('.ValidatePackage span').remove();
				//$('.footer_deploy_button').after('<p style="margin-bottom: 2px;">This Package Validation is in queue.</p>');
				$('#loaderOnStatusBar').hide();
				//location.reload(1);
			},
			error: function() 
			{
				$('.ValidatePackage span').remove();
			}						
		});
			
	});
	
	$('.ValidatePackage_Manual').click(function ()
	{
		var package_id = $('.package_id_hidden').val();
		var datavalue = {PackageId : package_id, Auto : 0};
		
		$.ajax(
		{
			type: 'post',
			// url: 'package_info/validatepackage',
			url: '<?php echo base_url();?>deploy/validationinqueue',
			data: datavalue,
			dataType:'json',
			beforeSend: function()
			{
				
				$('.ValidatePackage span').remove();
				$('.ValidatePackage').append(' <span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span>');
				$('.ValidatePackage').prop("disabled",'true');
				$('.ValidatePackage').off('click');
				$('.validate_package').hide();
				$('#loaderOnStatusBar').show();
				
				getTotalValidateAssets();
			},
			success: function(data1)
			{
				$('.ValidatePackage span').remove();
				//$('.footer_deploy_button').after('<p style="margin-bottom: 2px;">This Package Validation is in queue.</p>');
				$('#loaderOnStatusBar').hide();
				//location.reload(1);
		    },
			error: function() 
			{
			   $('.ValidatePackage span').remove();
			}
		});
	});
   
    function getStatusColor()
	{
		var package_id = $('.package_id_hidden').val();
		$.ajax(
		{
			type: 'post',
			// url: 'package_info/validatepackage',
			url: '<?php echo base_url();?>package_info/statusColor',
			data: {PackageId : package_id},
			dataType:'json',
			success: function(data1)
			{
				var color = data1.color;
				var backgroundColor = data1.backgroundcolor;
				$('#validateAssets').css('color' ,color );
				$('#validateAssets').css('background-color' ,backgroundColor );
			},
			error: function(data) 
			{
				//alert("error");
				console.log('error',data);
				//location.reload(1);
			}
		});
	}
	
	var status ;
	var flagForReload = 0;
	function getTotalValidateAssets()
	{
		if(status == 'Validate In Queue' || status == 'Deployment In Queue')
		 {
				   getStatusColor();
		 }
		
		var package_id = $('.package_id_hidden').val();
		$.ajax(
		{
			type: 'post',
			// url: 'package_info/validatepackage',
			url: '<?php echo base_url();?>package_info/getTotalValidateAssets',
			data: {PackageId : package_id},
			dataType:'json',
			success: function(data1)
			{
				
				//alert(data1.message);
				$('#validateAssetsCount').html(data1.message);
				$('#validateAssets').show();
				status  = data1.packageStatus[0].Status;
				var duplicateCount = data1.DuplicateCount;
				var newAssetCount = data1.newAssetCount;
				$('.status_cursor ').html(status);
				$('.status_cursor ').addClass(status);
				getPackageinfo();
				//buttonEnableDisable(status);
				//alert((flagForReload == 1) && (status != 'Validate In Queue' || status != 'Deployment In Queue'));
				if(flagForReload == 1 && status !== 'Validate In Queue')
				 {
					 console.log("in side loop",flagForReload);
					 location.reload(1);
				 }
				else if(flagForReload == 2 && status !== 'Deployment In Queue')
				 {
					 console.log("in side loop",flagForReload);
					 location.reload(1);
				 }
				else if(status == "Deploy Completed" || status == "Errored" || status == "Action Required")
				{
					$('.ValidatePackage').attr('disabled' , 'disabled');
					$('.DeployPackage').attr('disabled' , 'disabled');
					$('.ValidationReport').removeAttr('disabled');
				}
				else if(status == 'Validate In Queue' || status == 'Deployment In Queue')
				{
					if(status == 'Validate In Queue')
					{
					   flagForReload = 1;
					}
					else
					{
						flagForReload = 2;
					}
					$('.status_cursor ').removeClass('Errored').removeClass('Unsupported').removeClass('Missing Assets').removeClass('Duplicate Found').removeClass('Duplicates Found').removeClass('Missing Tokens').removeClass('Invalid').removeClass('Completed').removeClass('New');
					$('.ValidatePackage').attr('disabled' , 'disabled');
					$('.DeployPackage').attr('disabled' , 'disabled');
					$('.ValidationReport').attr('disabled' , 'disabled');
				}
				else if(status == 'New')
				{
					$('.ValidatePackage').removeAttr('disabled');
					$('.DeployPackage').attr('disabled' , 'disabled');
					$('.ValidationReport').removeAttr('disabled');
				}
				else if( status == "Validate Completed")
				{
					$('.ValidatePackage').removeAttr('disabled');
					$('.DeployPackage').removeAttr('disabled');;
					$('.ValidationReport').removeAttr('disabled');
				}
			  else if(status == "Duplicates Found" && duplicateCount == 0)
				{
					$('.DeployPackage').removeAttr('disabled' , 'disabled');
					$('.ValidationReport').removeAttr('disabled');
					$('.ValidatePackage').removeAttr('disabled');
				}
				else
                {
					$('.DeployPackage').attr('disabled' , 'disabled');
					$('.ValidationReport').removeAttr('disabled');
					$('.ValidatePackage').removeAttr('disabled');
				}					
				
				if(newAssetCount > 0)
				{
					$('.DeployPackage').attr('disabled' , 'disabled');
			    }
				if(status == "New" || status == "Validate Completed" || status == "Invalid" || status == "Deploy Completed" || status == "Errored" 
				|| status == "Unsupported" ||  status == 'Duplicates Found' || status  == 'Missing Assets' || status == "Action Required")
				{
					$('#loaderOnStatusBar').hide();
				}
				else
				{
					$('#loaderOnStatusBar').show();
				}	
				
				if(status != "Deploy Completed")
				{
				  setTimeout(getTotalValidateAssets , 10000);
				}
			},
			error: function() 
			{
				//alert("error");
				location.reload(1);
			}
		});
	}
	
</script>

<script>
	
	$(document).ready(function ()
	{
		function reload()
		{
			var status = $('.status_cursor').text();
			//console.log(status);
			if(status == 'Validate In Queue' || status == 'Deployment In Queue')
			{				
				setTimeout(function () 
				{ 
					//location.reload(1); 
					//getTotalValidateAssets();
					// $('.status_cursor_div').load('.status_cursor_div');
				}, 1000);
			}
		}
		 reload();
		 
		 loadScript("assets/plugins/" + "jqgrid/js/i18n/grid.locale-en.js", function()
		{
			loadScript("assets/plugins/" + "jqgrid/js/jquery.jqGrid.js", function()
			{
				loadScript("https://transporter-dev.portqii.com/assets/plugins/" + "jqgrid/js/jquery.fmatter.js", function() 
				{
					loadScript("assets/plugins/" + "bootstrap.datepicker/js/bootstrap-datepicker.min.js", function()
					{
						getPackageinfo();
					});
				});
			});
		});
	});	
	
	
	</script>
<!-- PAGE LEVEL SCRIPTS  jqGrid-->
	<script type="text/javascript">
			
	function get_jqgrid(jqgrid_data)
	{		
		jQuery("#jqgrid").jqGrid({
			data : jqgrid_data,
			datatype : "local",
			// height : '196',
			height : "auto",
			// caption : "Assets Included in Deployment Package",
			caption : "ASSETS INCLUDED IN DEPLOYMENT PACKAGE",
			colNames : ['Asset Id', 'Asset Type', 'Asset Name','Folder','Status','Actions'],
			colModel : 
			[
				{ name : 'Asset_Id' ,width : 60,key: true}, 
				{ name : 'Asset_Type' ,width : 160, editable : false}, 
				{ name : 'Asset_Name' ,width : 200, editable : false }, 
				{ name : 'Folder' ,width : 100, formatter: folderHandler, align: 'center',editable : false }, 
				{ name : 'Status',width : 80, sortable : false, editable : false, align: 'center', formatter: statusformatter},
				{ name : 'act',width : 155,sortable:false,formatter: actformatter,title : false }, 
			],
			rowNum : 7,
			//rowList : [10, 20, 30 ,40 ,50],
			pager : '#pager_jqgrid',
			sortname : 'id',
			toolbarfilter: false,
			viewrecords : true,
			sortorder : "asc",
			gridComplete: function()
			{
				var recs = parseInt($("#jqgrid").getGridParam("records"),10);
				if (isNaN(recs) || recs == 0) {
					$("#gridWrapper").hide();
					$(".footer_deploy_button").hide();
				}
				else {
					$('#gridWrapper').show();
					$(".footer_deploy_button").show();
					eventList();
					//alert('records > 0');
				}				
			},
			multiselect : false,
			autowidth : false,
		});			
		jQuery("#jqgrid").jqGrid('navGrid', "#pager_jqgrid", {
			edit : false,
			add : false,
			del : false,
			search: false,
			refresh: false,
		});
		jQuery("a.get_selected_ids").bind("click", function() {
			s = jQuery("#jqgrid").jqGrid('getGridParam', 'selarrrow');
			// alert(s);
		});

		// Select/Unselect specific Row by id
		jQuery("a.select_unselect_row").bind("click", function() {
			jQuery("#jqgrid").jqGrid('setSelection', "13");
		});
		
		function folderHandler(cellvalue, options, rowObject)
		{
			return act = '<button id="'+rowObject.Deployment_Package_Item_Id+'"type="button" class="btn btn-default btn-sm setFolder" >'+
          '<span class="glyphicon glyphicon-folder-open"></span> Folder</button>';
		}
		
		function actformatter(cellvalue, options, rowObject)
		{
			var statusDupFound = $('.status_cursor').text();
			//if(statusDupFound=='Validate In Queue')
			// alert(rowObject.Status)
			if(rowObject.Status == "Errored" || statusDupFound=='Validate In Queue'
			|| statusDupFound=='Deployment In Queue' || statusDupFound=="Errored")
			{
				// alert("Deploy Completed");
				var act = '<input type="hidden" name="component_id" value="'+rowObject.Deployment_Package_Item_Id+'"/>'+
				'<input type="hidden" name="changeset_id" value="'+rowObject.Deployment_Package_Id+'"/>'+
				'<button role="button" title="Delete" style="margin-right: 5px;" ondblclick="document.getElementById(\'form'+this.Deployment_Package_Item_Id+'\').submit();" type="submit" class="btn btn-danger btn-xs deleteBtn delete_package" id ="delBtn" data-title="Delete" data-toggle="modal" data-target="#delete" disabled >Delete</button>'+
				'<input type="hidden" name="Asset_Type" value="'+rowObject.Asset_Type+'"/>'+
				'<input type="hidden" name="Asset_Id" value="'+rowObject.Asset_Id+'"/>'+
				'<button class="btn btn-sm btn-info viewChildBtn ViewChild" title="View Child" disabled>View Child </button>';
				return act;
			}
			else if(statusDupFound == 'Deploy Completed' || statusDupFound == 'Action Required')
			{
				var act = '<input type="hidden" name="component_id" value="'+rowObject.Deployment_Package_Item_Id+'"/>'+
				'<input type="hidden" name="changeset_id" value="'+rowObject.Deployment_Package_Id+'"/>'+
				'<button role="button" title="Delete" style="margin-right: 5px;" ondblclick="document.getElementById(\'form'+this.Deployment_Package_Item_Id+'\').submit();" type="submit" class="btn btn-danger btn-xs deleteBtn delete_package" id ="delBtn" data-title="Delete" data-toggle="modal" data-target="#delete" disabled >Delete</button>'+
				'<input type="hidden" name="Asset_Type" value="'+rowObject.Asset_Type+'"/>'+
				'<input type="hidden" name="Asset_Id" value="'+rowObject.Asset_Id+'"/>'+
				'<button class="btn btn-sm btn-info viewChildBtn ViewChild" title="View Child">View Child </button>';
				return act;
			}
			else
			{
				// alert("not Deploy Completed");
				var act = '<form id="form'+rowObject.Deployment_Package_Item_Id+'" role="form" action="package_info/deletecomponent" method="post" onSubmit="if(!confirm(\'Are you sure you want to delete this from Packages?\')){return false;}" style="float: left;">'+		
					'<input type="hidden" name="component_id" value="'+rowObject.Deployment_Package_Item_Id+'"/>'+
					'<input type="hidden" name="changeset_id" value="'+rowObject.Deployment_Package_Id+'"/>'+
					'<button role="button" title="Delete" style="margin-right: 5px;" ondblclick="document.getElementById(\'form'+this.Deployment_Package_Item_Id+'\').submit();" type="submit" class="btn btn-danger btn-xs deleteBtn delete_package" id ="delBtn" data-title="Delete" data-toggle="modal" data-target="#delete" >Delete</button>'+
					'<input type="hidden" name="Asset_Type" value="'+rowObject.Asset_Type+'"/>'+
					'<input type="hidden" name="Asset_Id" value="'+rowObject.Asset_Id+'"/>'+
					'</form>'+
					'<button class="btn btn-sm btn-info viewChildBtn ViewChild " title="View Child">View Child </button>';
				return act;
			}
		}		
		function statusformatter(cellvalue, options, rowObject)
		{
			if (cellvalue == "Valid")
				return "<a class='btn btn-xs btn-warning validBtn btn-quick' style='cursor:default;pointer-events: none;'>Valid</a>";			
			else if(cellvalue == "Deploy Completed")
				return "<a class='btn btn-xs btn-success btn-quick' >"+cellvalue+"</a>";
			else if(cellvalue == "Action Required")
				return "<a class='btn btn-xs btn-warning btn-quick actionRequired' style='width: 85px !important;' value='"+rowObject.ActionRequired_Asset_Message+"'>"+cellvalue+"</a>";
			else if(cellvalue == "Completed")
				return "<a class='btn btn-xs btn-success unsprtCpltd btn-quick' style='cursor:default;pointer-events: none;'>"+cellvalue+"</a>";
			else if(cellvalue == "Errored")
				return "<a class='btn btn-xs btn-danger unsprtErr btn-quick' style='cursor:default;pointer-events: none;'>"+cellvalue+"</a>";
			else if(cellvalue == "Unsupported")
				return "<a class='btn btn-xs btn-danger unsprtBtn btn-quick Unsupported' value='"+rowObject.Unsupported_Asset_Message+"'>"+cellvalue+"</a>";
			else if(cellvalue == "Duplicate" || cellvalue == "Duplicate Found")
				return "<a class='btn btn-xs btn-danger unsprtdup btn-quick Duplicate' style='padding-left:5px!important;cursor:default;pointer-events: none;'>"+cellvalue+"</a>";	
			else if(cellvalue == "Invalid")
				return "<a class='btn btn-xs btn-danger invalidBtn btn-quick Invalid' value='"+rowObject.Invalid_Asset_Message+"'>"+cellvalue+"</a>";				
			else 
				return "<a class='btn btn-xs btn-info newBtn btn-quick' style='cursor:default;pointer-events: none;'>"+cellvalue+"</a>";
		}
		
		// On Resize
		jQuery(window).resize(function() {

			if(window.afterResize) {
				clearTimeout(window.afterResize);
			}
			window.afterResize = setTimeout(function() {

				/**
					After Resize Code
					.................
				**/

				jQuery("#jqgrid").jqGrid('setGridWidth', jQuery("#middle").width() - 32);

			}, 500);

		});
		/**
			@STYLING
		**/
		jQuery(".ui-jqgrid").removeClass("ui-widget ui-widget-content");
		jQuery(".ui-jqgrid-view").children().removeClass("ui-widget-header ui-state-default");
		jQuery(".ui-jqgrid-labels, .ui-search-toolbar").children().removeClass("ui-state-default ui-th-column ui-th-ltr");
		jQuery(".ui-jqgrid-pager").removeClass("ui-state-default");
		jQuery(".ui-jqgrid").removeClass("ui-widget-content");

		jQuery(".ui-jqgrid-htable").addClass("table table-bordered table-hover");
		jQuery(".ui-pg-div").removeClass().addClass("btn btn-sm btn-primary");
		
		jQuery( ".ui-icon.ui-icon-seek-prev" ).wrap( "<div class='btn btn-sm btn-default'></div>" );
		jQuery(".ui-icon.ui-icon-seek-prev").removeClass().addClass("fa fa-backward");

		jQuery( ".ui-icon.ui-icon-seek-first" ).wrap( "<div class='btn btn-sm btn-default'></div>" );
		jQuery(".ui-icon.ui-icon-seek-first").removeClass().addClass("fa fa-fast-backward");		  	

		jQuery( ".ui-icon.ui-icon-seek-next" ).wrap( "<div class='btn btn-sm btn-default'></div>" );
		jQuery(".ui-icon.ui-icon-seek-next").removeClass().addClass("fa fa-forward");

		jQuery( ".ui-icon.ui-icon-seek-end" ).wrap( "<div class='btn btn-sm btn-default'></div>" );
		jQuery(".ui-icon.ui-icon-seek-end").removeClass().addClass("fa fa-fast-forward");
		jQuery(".ui-jqgrid-caption").css("position","static");
	}	
	
	function getPackageinfo()
	{
		var package_id = $('.package_id_hidden').val();
		// alert(package_id);
		$.ajax(
		{
			type: 'post',
			url: '<?php echo base_url();?>package_info/getpackageinfo',
			data: {packageId: package_id},
			dataType:'json',
			beforeSend: function()
			{
				// $('.jqgrid_div').html('<span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span>')
			},
			success: function(data)
			{
				get_jqgrid(data);
				var $mygrid =  $("#jqgrid");
				allGridParams = $mygrid.jqGrid("getGridParam");
				//allGridParams = $("#jqgrid").jqGrid("getGridParam", "data")
				allGridParams.data = data;
				$mygrid.trigger("reloadGrid", [{current: true}]);
				
				if(data != ""){
					$('.selectSys , .selectSys *').attr("disabled", true);
					$('.selectSys2 , .selectSys2 *').attr("disabled", true);
				} 
				
			},
			error: function() 
			{
			
			}
		});
	}
	
	function showPopUp()
	{
		console.log('clicked');
		$('.parentChildPopUp').show();
	}
</script>