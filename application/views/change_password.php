<style>
	#page-wrapper{
		margin-top: 20px ;
	}
	.saveButton{
		padding: 9px;
		text-align: center;
	}
	.form-control {
		font-size: 12px !important;
		height: 32px !important;
	}
</style>
<section>
				<div class="container">
					<div class="panel-heading " id="page-wrapper">
						<div class="col-lg-6 col-md-6 col-md-offset-3 col-lg-offset-3">
							<h3 class="Lead" style="color: #00bbff;"><?php //print_r($user_info[0]->first_name); ?></h3>
						</div>
						
						<div class="panel-body">
							<div class="row">
								<form action="<?php echo base_url();?>settings/password" method="post" enctype="multipart/form-data">									
									<div style="" class="col-lg-6 col-md-6 col-md-offset-3 col-lg-offset-3 login-box">
										<div class="form-group col-lg-12">										
											<?php $result2 = validation_errors(); ?>
												<?php if ((isset($result) && $result != '') || $result2!='')
												{ ?>
												<div class="alert alert-danger alert-dismissable">
													<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
													<?php echo $result2; ?>
													<?php echo isset($result) ?$result: '' ; ?>
												</div>
											<?php }?>
										</div>
										<div class="form-group">
											<div class="input-group">
												<div class="input-group-addon">
												<span class="glyphicon glyphicon-lock"></span>
												</div> 
												<input class="form-control" name="old_password" type="password" placeholder="Current Password">
											</div>
										</div>
										<div class="form-group">
											<div class="input-group">
												<div class="input-group-addon">
												<span class="glyphicon glyphicon-log-in"></span></div> 
												<input class="form-control" name="new_password" type="password" placeholder="New Password">
											</div>
										</div>
										<div class="form-group">
											<div class="input-group">
												<div class="input-group-addon">
												<span class="glyphicon glyphicon-log-in"></span></div> 
												<input class="form-control" name="confirm_new_password" type="password" placeholder="Confirm New Password">
											</div>
										</div>
										<div class="form-group">
											<div class="input-group pull-right">
												<button type="submit" class="btn btn-sm btn-info pull-right">Change Password</button>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div><!-- /FEATURED BOXES 3 -->	
					</div>

				</div>
			</section>
		
		