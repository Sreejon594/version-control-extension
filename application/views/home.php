	<style>	
		.removed {
			background-color: #fd7f7f;
		}
		.added {
			background-color: #8bff7f;
		}
		.changed {
			background-color: #fcff7f;
		}
		#diffinfo{
			display: none;
			border: solid #337ab7;	
			position: fixed; 
			bottom: 0px;
			top:50px;
			background-color: #333;
			color: #fff;
			height: auto; 
			overflow: auto;
		}
		#diffinfo .panel-heading
		{
			padding-bottom: 25px;
			padding-top: 0px;
			border-bottom: 2px solid;
		}
		#diffinfo .info_body
		{
			overflow: auto;
			margin-top: 5px;
		}
		#diffinfo .panel-title
		{
			float: left;
		}
		#diffinfo .close
		{
			float: right;
			opacity: 1;
			color: #ffffff;
		}
		#page-wrapper{
			margin-top: -2px !important;
			font-size: 12px !important;
		}
		.packageButton{
			padding: 0.6em 1.5em 0.5em !important;
			font-size: 94% !important;
		}
		.lead{
			font-size: 12px !important;
		}
			
		td 
		{		
			padding-top: 4px !important;
			padding-bottom: 4px !important;
		}
		.deploymentButtonDiv{
			margin-top: -19px !important;
			margin-bottom: 10px !important;
		}
		.ui-jqgrid .ui-jqgrid-bdiv { overflow-y: scroll !important; }
		

		.checkbox label:after, 
		.radio label:after {
			content: '';
			display: table;
			clear: both;
		}

		.checkbox .cr,
		.radio .cr {
			position: relative;
			display: inline-block;
			border: 1px solid #a9a9a9;
			border-radius: .25em;
			width: 1.3em;
			height: 1.3em;
			float: left;
			margin-right: .5em;
		}

		.radio .cr {
			border-radius: 50%;
		}	
		.checkbox .cr .cr-icon,
		.radio .cr .cr-icon {
			position: absolute;
			font-size: .8em;
			line-height: 0;
			top: 50%;
			left: 20%;
		}

		.radio .cr .cr-icon {
			margin-left: 0.04em;
		}

		.checkbox label input[type="checkbox"],
		.radio label input[type="radio"] {
			display: none;
		}

		.checkbox label input[type="checkbox"] + .cr > .cr-icon,
		.radio label input[type="radio"] + .cr > .cr-icon {
			transform: scale(3) rotateZ(-20deg);
			opacity: 0;
			transition: all .3s ease-in;
		}

		.checkbox label input[type="checkbox"]:checked + .cr > .cr-icon,
		.radio label input[type="radio"]:checked + .cr > .cr-icon {
			transform: scale(1) rotateZ(0deg);
			opacity: 1;
		}

		.checkbox label input[type="checkbox"]:disabled + .cr,
		.radio label input[type="radio"]:disabled + .cr {
			opacity: .5;
		}
		.checkbox, .radio {
			margin-top: 0px;
			margin-bottom: 0px;
		}
		.checkbox label, .radio label {
			min-height: 20px;
			padding-left: 5px;
		}
		.ui-jqgrid .ui-jqgrid-view, .ui-jqgrid .ui-paging-info, .ui-jqgrid .ui-pg-selbox, .ui-jqgrid .ui-pg-table {
			font-size: 11px !important;
		}

		.ui-jqgrid-labels th div{
			white-space: nowrap;
			overflow: hidden !important;
			height: 30px !important;
		}
		.btn{
			font-size: 94% !important;
		}
		.info{
			color: white; 
			background-color: grey; 
			padding: 5px; border: 1px solid black;  
			border-radius: 50%; 
			margin-left: 10px;"
		}
		.jqgfirstrow{
			visibility: hidden;
		}
		.info_circle{
			margin-left: 20px;
			border: 1px solid black;
			padding: 0px 6px 0px 6px;
			border-radius: 50%;
			background-color: white;
		}
		.ui-jqgrid-sortable{
			padding-bottom: 0px;
			padding-top: 9px !important;	
		}
		
		.ui-jqgrid-hbox
		{
			//padding-right: 0px !important;
		}
		.blue
		{
			background-color: #337ab7 !important;
			color: #ffffff !important;
		}
		
		.actnReq
		{
			background-color: #ff9999 !important;
			color: #ffffff !important;
		}
		.expansiva {
			font-family: "expansiva", sans-serif;
		}
		.ui-jqgrid-btable{
			
			margin-top: -9px !important;
		}
		.btn-warning,.btn-success,.btn-danger,.blue,.newBtn,.actnReq
		{
			width:110px !important;
		}
		
		#jqgh_jqgrid_Status,#jqgh_jqgrid_act
		{
			pointer-events: none !important;
		}
		.s-ico{
			display: none !important;
		}
		div.alert {
			border-left-color: #faebcc !important;
			border-width: 1px;
			border-left-width: 1px !important;
		}	
		.highlight{
			background-color:red;
		}
		/*
		.actnReq
		{
			background-color:#ff0066 !important;
			color:white;
			border-color:#ff0066 !important;
		}*/
	</style> 
	
	<head>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-3.1.1.min.js">
		
	</script>
	
	<link href="https://transporter-dev.portqii.com/assets/plugins/jqgrid/css/ui.jqgrid.css" rel="stylesheet" type="text/css" />
	<link href="https://transporter-dev.portqii.com/assets/css/layout-jqgrid.css" rel="stylesheet" type="text/css" />
	<script>
	$(document).ready(function()
	{
		//jQuery("#jqgrid").clearGridData();
		//getPackageList();
		loadScript("https://transporter-dev.portqii.com/assets/plugins/" + "jqgrid/js/i18n/grid.locale-en.js", function()
		{
			loadScript("https://transporter-dev.portqii.com/assets/plugins/" + "jqgrid/js/minified/jquery.jqGrid.min.js", function() 
			{
				loadScript("https://transporter-dev.portqii.com/assets/plugins/" + "jqgrid/js/jquery.fmatter.js", function() 
				{
					loadScript("https://transporter-dev.portqii.com/assets/plugins/" + "bootstrap.datepicker/js/bootstrap-datepicker.min.js", function()
					{
						
						getPackageList(); 
					});
				});
			}); 
		}); 
	});
	
	//getPackageList();
	
	
	
	function get_jqgrid(jqgrid_data)
	{		
		
		jQuery("#jqgrid").jqGrid({
			data : jqgrid_data,
			datatype : "local",
			height : '250',
			colNames : ['ID',
					'<a data-toggle="tooltip" data-placement="right" title="Package Name" class="">Package Name <a>',
					'<a data-toggle="tooltip" data-placement="right" title="Source Instance" class="">Source Instance <a>',
					'<a data-toggle="tooltip" data-placement="right" title="Target Instance" class="">Target Instance <a>',
					'<a data-toggle="tooltip" data-placement="right" title="Created By" class="">Created By <a>',
					'<a data-toggle="tooltip" data-placement="right" title="Created On" class="">Created On <a>',
					'Status', 'Actions'],
			colModel : 
			[
				{ name : 'Deployment_Package_Id' ,width : 15, formatter:linkformatter,key: true}, 
				{ name : 'Package_Name',width : 50,  formatter:linkformatter}, 
				{ name : 'Source_Site_Name',width : 50,  editable : false , formatter:linkformatter}, 
				{ name : 'Target_Site_Name',width : 49,  editable : false , formatter:linkformatter}, 
				{ name : 'User_Name',width : 26,   editable : false , formatter:linkformatter}, 
				{ name : 'created_at',width : 35,  editable : false , formatter:linkformatter}, 
				{ name : 'Status',width : 40,  sortable : false, editable : false, align: 'center', formatter: statusformatter},
				{ name : 'act',width : 25, sortable:false,formatter: actformatter }, 
			],
			rowNum : 10,
			//rowList : [10, 20, 30],
			pager : '#pager_jqgrid',
			sortname : 'id',
			multiSort : true,
			toolbarfilter: false,
			viewrecords : true,
			sortorder : 'desc',
			gridComplete: function()
			{
				var recs = parseInt($("#jqgrid").getGridParam("records"),10);
				if (isNaN(recs) || recs == 0) {
					$("#gridWrapper").hide();
				}
				else {
					$('#gridWrapper').show();
					eventList();
					//alert('records > 0');
				}				
			},
			multiselect : false,
			autowidth : true,
			gridview: true,
			autoencode: true
		});	
		
		jQuery("#jqgrid").jqGrid('navGrid', "#pager_jqgrid", {
			edit : false,
			add : false,
			del : false,
			search: false,
			refresh: false,
		});
		jQuery("a.get_selected_ids").bind("click", function() {
			s = jQuery("#jqgrid").jqGrid('getGridParam', 'selarrrow');
			alert(s);
		});

		// Select/Unselect specific Row by id
		jQuery("a.select_unselect_row").bind("click", function() {
			jQuery("#jqgrid").jqGrid('setSelection', "13");
		});

		
		function actformatter(cellvalue, options, rowObject)
		{
			se = "<button class='btn btn-xs btn-default btn-quick copy_Deployment_Package' onclick='' title='Copy Package' ><i class='fa fa-copy'></i></button>";
			ca = "<button class='btn btn-xs btn-default btn-quick delete_Deployment_Package' title='Delete' ><i class='fa fa-times'></i></button>";  
			return se+ca;
		}		
		function linkformatter(cellvalue, options, rowObject)
		{
			return"<a href='https://transporter-dev.portqii.com/package_info?id="+rowObject.Deployment_Package_Id+"'>"+cellvalue+"</a>";
		}		
		function statusformatter(cellvalue, options, rowObject)
		{
			if (cellvalue == "Deployment In Queue")
				return "<a class='btn btn-xs btn-warning btn-quick' style='pointer-events: none;'>Deployment In Queue</a>";
			else if (cellvalue == "Deployment In Progress")
				return "<a class='btn btn-xs btn-warning btn-quick' style='pointer-events: none;'>Deployment In Progress</a>";
			else if (cellvalue == "Deploy Completed")
				return "<a class='btn btn-xs btn-success btn-quick' style='pointer-events: none;'>Deploy Completed</a>";
			else if (cellvalue == "Errored")
				return "<a class='btn btn-xs btn-danger btn-quick' style='pointer-events: none;'>Errored</a>";
			else if (cellvalue == "Unsupported")
				return "<a class='btn btn-xs btn-danger btn-quick' style='pointer-events: none;'>Unsupported</a>";
			else if (cellvalue == "Validate Completed")
				return "<a class='btn btn-xs blue btn-quick' style='pointer-events: none;'>Validate Completed</a>";
			else if (cellvalue == "Validate In Queue")
				return "<a class='btn btn-xs btn-warning btn-quick' style='pointer-events: none;'>Validate In Queue</a>";
			else if (cellvalue == "Missing Assets")
				return "<a class='btn btn-xs btn-danger btn-quick' style='pointer-events: none;'>Missing Assets</a>";
			else if (cellvalue == "Duplicates Found")
				return "<a class='btn btn-xs btn-danger btn-quick' style='pointer-events: none;'>Duplicates Found</a>";
			else if (cellvalue == "Action Required")
				return "<a class='btn btn-xs actnReq btn-quick' style='pointer-events: none; '>Action Required</a>";
			else if (cellvalue == "Invalid")
				return "<a class='btn btn-xs btn-danger btn-quick' style='pointer-events: none;'>Invalid</a>";
			else if (cellvalue == "" || cellvalue == "undefined" || cellvalue == "New")
				return "<a class='btn btn-xs btn-info newBtn btn-quick' style='pointer-events: none;'>New</a>";
		}
		
		// On Resize
		jQuery(window).resize(function() {

			if(window.afterResize) {
				clearTimeout(window.afterResize);
			}
			window.afterResize = setTimeout(function() {

				/**
					After Resize Code
					.................
				**/

				jQuery("#jqgrid").jqGrid('setGridWidth', jQuery("#middle").width() - 32);

			}, 500);

		});
		/**
			@STYLING
		**/
		jQuery(".ui-jqgrid").removeClass("ui-widget ui-widget-content");
		jQuery(".ui-jqgrid-view").children().removeClass("ui-widget-header ui-state-default");
		jQuery(".ui-jqgrid-labels, .ui-search-toolbar").children().removeClass("ui-state-default ui-th-column ui-th-ltr");
		jQuery(".ui-jqgrid-pager").removeClass("ui-state-default");
		jQuery(".ui-jqgrid").removeClass("ui-widget-content");

		jQuery(".ui-jqgrid-htable").addClass("table table-bordered table-hover");
		jQuery(".ui-pg-div").removeClass().addClass("btn btn-sm btn-primary");
		
		jQuery( ".ui-icon.ui-icon-seek-prev" ).wrap( "<div class='btn btn-sm btn-default'></div>" );
		jQuery(".ui-icon.ui-icon-seek-prev").removeClass().addClass("fa fa-backward");

		jQuery( ".ui-icon.ui-icon-seek-first" ).wrap( "<div class='btn btn-sm btn-default'></div>" );
		jQuery(".ui-icon.ui-icon-seek-first").removeClass().addClass("fa fa-fast-backward");		  	

		jQuery( ".ui-icon.ui-icon-seek-next" ).wrap( "<div class='btn btn-sm btn-default'></div>" );
		jQuery(".ui-icon.ui-icon-seek-next").removeClass().addClass("fa fa-forward");

		jQuery( ".ui-icon.ui-icon-seek-end" ).wrap( "<div class='btn btn-sm btn-default'></div>" );
		jQuery(".ui-icon.ui-icon-seek-end").removeClass().addClass("fa fa-fast-forward");
	
	}
	
	function getPackageList()
	{
		var text = $('.search').val();
		$.ajax(
		{			
			type: 'post',
			url: 'https://transporter-dev.portqii.com/transport/getPackageList',
			data: {'text': text},
			dataType:'json',
			beforeSend: function()
			{
				
			},
			success: function(data)
			{	
				if(data=='' || data.length==0)
				{
					$('.assetNotFound').show();
				}
				else if(data=='Licence Expired')
				{	
					$('.packageButton').attr('disabled',true);	
					$('.search_button').attr('disabled',true);	
					$('.reset').attr('disabled',true);	
					$('.search').attr('disabled',true);	
					$('#gbox_jqgrid').css('display','none');
					$('.licenceExpired').show();
					return;	
				}
				else if(data=='Instance Needed')
				{	
					$('.packageButton').attr('disabled',true);	
					$('.search_button').attr('disabled',true);	
					$('.reset').attr('disabled',true);	
					$('.search').attr('disabled',true);	
					$('#gbox_jqgrid').css('display','none');
					$('.minimum2Instance').show();	
					return;
				}
				else
				{
					$('.assetNotFound').hide();					
				}
				get_jqgrid(data);
				var $mygrid =  $("#jqgrid");
				allGridParams = $mygrid.jqGrid("getGridParam");
				allGridParams.data = data;
				$mygrid.trigger("reloadGrid", [{current: true}]);	
			},
			error: function() 
			{
				//alert('HI');
			}
		});
	}
	
	function eventList()
	{
		$('.copy_Deployment_Package').click(function()
		{
			var id = $(this).parent().parent().find('td[aria-describedby="jqgrid_Deployment_Package_Id"] a').html();
			$.ajax(
			{
				type: 'post',
				url: 'https://transporter-dev.portqii.com/transport/copy_Deployment_Package',
				data: {'Deployment_Package_Id' : id},
				beforeSend: function()
				{
					
				},
				success: function(data)
				{
					getPackageList();
				},
				error: function() 
				{
				
				}
			});        
		});
			
		$('.delete_Deployment_Package').click(function()
		{
			var id = $(this).parent().parent().find('td[aria-describedby="jqgrid_Deployment_Package_Id"] a').html();
			$.ajax(
			{
				type: 'post',
				url: 'https://transporter-dev.portqii.com/transport/delete_Deployment_Package',
				data: {'Deployment_Package_Id' : id},
				beforeSend: function()
				{
					
				},
				success: function(data)
				{
					getPackageList();
				},
				error: function() 
				{
				
				}
			});        
		});
		
		$('.search_button').on('click', function(){
			getPackageList();
		});
	
		$('.reset').on('click', function(){
			$('.search').val('');
			getPackageList();
			$('.assetNotFound').hide();			
		});
		
		$('.search').keyup(function(e)
		{
			if(e.keyCode == 13)
			{
				getPackageList();
			}
		});		
	
	}
	
	var _arr 	= {};
	function loadScript(scriptName, callback) {

		if (!_arr[scriptName]) {
			_arr[scriptName] = true;

			var body 		= document.getElementsByTagName('body')[0];
			var script 		= document.createElement('script');
			script.type 	= 'text/javascript';
			script.src 		= scriptName;

			// then bind the event to the callback function
			// there are several events for cross browser compatibility
			// script.onreadystatechange = callback;
			script.onload = callback;

			// fire the loading
			body.appendChild(script);

		} else if (callback) {

			callback();

		}

	};
	</script>
	</head>
	<div id="page-wrapper" style="margin-top:40px;">
		<div class="container-fluid" style="font-family: Helvetica Neue,Helvetica Arial,sans-serif !important;">                
			<div class="row profileDiv" style="/border:1px solid pink/;">
				<div class="col-md-2" style="/border:1px solid orange/;"></div>
				<div class="col-sm-12 col-xs-12 col-md-8 col-lg-8 mainContainer" style="height:auto;padding:0px 0px 0px 0px;">
						<div class="col-lg-12 col-md-12 pull-left" style="margin-top:0px;padding:0px 0px 0px 0px;">
							<p class="lead" style="margin-bottom: 9px;"><b> DEPLOYMENT PACKAGE </b></p>
							<p class=" lead_description" style="text-align: justify;margin-bottom: 14px;">A deployment package contains customizations/assets that you want to send from one instance to another instance of Oracle Eloqua. These customizations can include new components or modifications to existing components, such as emails, landing pages, forms etc. An outbound deployment can't be used to delete or rename components in another instance.
							</p>
						</div>
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 package_and_search" style="padding:0px 0px 0px 0px;/border:1px solid black/;">
							<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12pull-left newPackage_button" style="padding:0px 0px 0px 0px;">
								<a href="<?php echo base_url();?>package_info" role="button" class="btn btn-sm btn-info packageButton pull-left" target="_self" >New Package</a>
							</div>
							<div class="col-lg-4 pull-center" style="/*border:1px solid green;*/">
							</div>
							<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 pull-right newPackage_button" style="padding:0px 0px 0px 0px; display: -webkit-inline-box;/border:1px solid red/;">
								<input type="text" class="form-control search" placeholder="Search" style="height:31px;width: 75%;">
								<a role="button" class="btn btn-sm btn-info search_button" style="margin-left: 4px;padding-top: 8px;"><span class="fa fa-search"></span></a>
								<a role="button" class="btn btn-sm btn-info reset" style="margin-left: 3px;    padding-top: 8px;"><span class="fa fa-times"></span></a>
							</div>
						</div>
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 gridDiv" style="/*border:1px solid green;*/">
							<!-- JQGRID TABLE -->
							<div class="" id="gridWrapper" style="margin:10px -14px 0px -15px;">
								<table id="jqgrid"></table>
								<div id="pager_jqgrid"></div>
							</div>
						</div>
						
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 assetNotFound" style="display:none;margin-top: 5px;padding:0px;">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 assetNotFoundHeader" style="padding:0px;">
								<div class="alert alert-warning" style="text-align: center;    font-size: 12px;font-style: italic;"><strong>No packages found</strong>	
								</div>
							</div>
						</div>
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 licenceExpired" style="display:none;margin-top: 5px;padding:0px;">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 assetNotFoundHeader" style="padding:0px;">
								<div class="alert alert-warning" style="text-align: center;    font-size: 13px;font-style: italic;"><strong>Your licence has expired. Contact your system administrator to get access.</strong>	
								</div>
							</div>
						</div>
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 minimum2Instance" style="display:none;margin-top: 5px;padding:0px;">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 assetNotFoundHeader" style="padding:0px;">
								<div class="alert alert-warning" style="text-align: center;    font-size: 13px;font-style: italic;"><strong>Please ensure to install the app in  atleast two instances.</strong>	
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-2 col-md-2"></div>
				</div>
			</div>
		</div>
	</div>
	<!-- PAGE LEVEL SCRIPTS -->
