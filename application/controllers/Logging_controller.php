<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Logging_Controller extends CI_Controller {
	
	public function __construct()
	{
        parent::__construct();
        $this -> load -> helper('url');
        $this -> load -> database();
		$this -> load -> model('Logging_model','',TRUE);
		$this -> load -> model('Settings_model','',TRUE);
		$this -> load -> library('session');
		$this->load->helper('file');
	}
	// function test()
	// {
		// $this->log('Logging_Controller/Log','Enter');
	// }
	function log($param1,$param2)
	{	
		if($this -> session -> userdata('logged_in'))
	    {	
			$session_data = $this -> session -> userdata('logged_in');
			foreach ($session_data as $key => $value)
			{ 
				if($key=='logEnabled')
				{ 	
					if($value)	
					{	
						$data1 		= date('Y-m-d H:i:s')." | ". $param1." | ".$param2 ;
						$file_path 	= $_SERVER['DOCUMENT_ROOT']."/application/assets/log/Log_file.txt";
						if (file_exists($file_path)) 
						{
							write_file($file_path, $data1. "\r\n", 'a');
							$this->load->view('Logging_view');
						} 
						else 
						{
							echo "The file " .$file_path. " does not exist";
						}
					}	
				}
				else
				{	
					$enableStatus = $this -> Logging_model -> logProperties();
					if($enableStatus !='')
					{	
						$enableStatus = explode('=', $enableStatus );
						$session_data['logEnabled'] = $enableStatus[1];
						$this->session->set_userdata("logged_in", $session_data);
					}
					foreach ($session_data as $key => $value)
					{
						if($key=='logEnabled')
						{ 	
							if($value)	
							{	
								$data1 		= date('Y-m-d H:i:s')." | ". $param1." | ".$param2 ;
								$file_path 	= $_SERVER['DOCUMENT_ROOT']."/application/assets/log/Log_file.txt";
								if (file_exists($file_path)) 
								{
									write_file($file_path, $data1. "\r\n", 'a');
									$this->load->view('Logging_view');
								} 
								else 
								{
									echo "The file " .$file_path. " does not exist";
								}
							}	
						}
					}
				}	
			}
		}
		else
		{
			echo 'Login Again';
		}	
	}
}
?>