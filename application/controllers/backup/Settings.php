<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->database();
		$this->load->library('session');
		$this->load->library('form_validation');		  
		$this->load->model('settings_model','',TRUE);
		$this->load->helper('form');
		$this->load->model('user_model','',TRUE);
		$this->load->model('eloqua','',TRUE);
		  
	}
	public function index()
	{
		if($this->session->userdata('logged_in'))
	    {	$session_data = $this->session->userdata('logged_in');
							
			$data['page_title']= 'Eloqua Transporter';
			$data['user_info']= $this->user_model->get_user_info($session_data['id']);
						
			$this->load->view('common/header',$data);
			$data['page_data']['acountlist'] = $this->settings_model->get_OrgList($session_data['id']);				
			$this->load->view('profile',$data);
			$this->load->view('common/footer');
			
	   }
	   else
	   {
		 //If no session, redirect to login page
		 redirect($this->config->item('parent_url').'login', 'refresh');
	   }
	}
	
	public function dashboard()
	{
		if($this->session->userdata('logged_in'))
	    {	$session_data = $this->session->userdata('logged_in');
							
			$data['page_title']= 'Dashboard';
			$data['user_info']= $this->user_model->get_user_info($session_data['id']);
		
			$this->load->view('common/dashboard_header',$data);
			//$this->load->view('common/nav',$data);
			//$data['page_data']['acountlist'] = $this->settings_model->get_OrgList($session_data['id']);				
			//$data['page_data']['eloqua'] = $this->eloqua->getUserinfo();				
			$this->load->view('dashboard',$data);
			$this->load->view('common/footer');
			
	   }
	   else
	   {
		 //If no session, redirect to login page
		 redirect($this->config->item('parent_url').'login', 'refresh');
	   }
	}
	
	public function editprofile()
	{
		if($this->session->userdata('logged_in'))
	    {	$session_data = $this->session->userdata('logged_in');
			$this->form_validation->set_rules('baseurl', 'BaseURL', 'trim|required');
			$this->form_validation->set_rules('username', 'New Password', 'trim|required');
			$this->form_validation->set_rules('password', 'Password', 'trim|required');   
			if($this->form_validation->run() == FALSE)
			{
				$data['page_title']= 'Eloqua Transporter';
				$data['user_info']= $this->user_model->get_user_info($session_data['id']);
				
				$this->load->view('common/header',$data);
				$data['page_data']['acountlist'] = $this->settings_model->get_OrgList($session_data['id']);				
				$this->load->view('editProfile',$data);
				$this->load->view('common/footer');
			}
			else
			{		
				
			}
	   }
	   else
	   {
		 //If no session, redirect to login page
		 redirect($this->config->item('parent_url').'login', 'refresh');
	   }
	}
	
	public function saveToken()
	{
		if($this->session->userdata('logged_in'))
	    {	
			$session_data = $this->session->userdata('logged_in');
			$getdata = $this->input->get();
			$result = $this->settings_model->saveToken($session_data['id'],$getdata);
			if($result['success'] == true)
			{
				redirect('/settings/orginfo', 'refresh');
			}
			else{
				print_r($result);
			}
		}		
	}
	
	public function orginfo()
	{
		if($this->session->userdata('logged_in'))
	    {	$session_data = $this->session->userdata('logged_in');
			$this->form_validation->set_rules('OrgName', 'Org Name', 'trim|required');
			if($this->form_validation->run() == FALSE)
			{
				$data['page_title']= 'Eloqua Transporter';
				$data['user_info']= $this->user_model->get_user_info($session_data['id']);
			
				
				$this->load->view('common/header',$data);
				//$this->load->view('common/nav',$data);
				$data['page_data']['acountlist'] = $this->settings_model->get_OrgList($session_data['id']);				
				$this->load->view('eloqua_setting',$data);
				$this->load->view('common/footer');
			}
			else
			{		
				$data['OrgName'] = $this->input->post('OrgName');				
				$result = $this->settings_model->new_Org($session_data['id'],$data);
				
				if($result['success'] == true)
				{
					redirect('/settings/orginfo', 'refresh');
				}
				else{
					//print_r($result);
				}
			}			
	   }
	   else
	   {
		 //If no session, redirect to login page
		 redirect($this->config->item('parent_url').'login', 'refresh');
	   }      
	}
	function orgdelete()
	{	$session_data = $this->session->userdata('logged_in');
	   if($this->session->userdata('logged_in'))
	    {
			$this->form_validation->set_rules('orgid', 'Org id', 'trim|required');
			if($this->form_validation->run() == true)
			{
				$data['id'] = $this->input->post('orgid');
				$result = $this->settings_model->delete_Org($session_data['id'],$data);
				redirect('/settings/orginfo', 'refresh');
			}
			else
			{
				//Go to private area
				//redirect('home', 'refresh');
			}			
			
	   }
	   else
	   {
		 //If no session, redirect to login page
		 redirect($this->config->item('parent_url').'login', 'refresh');
	   }	   
	}
	
	function manageMeta()
	{
	   if($this->session->userdata('logged_in'))
	    {	$data['page_title']= 'Eloqua Transporter';
			$session_data = $this->session->userdata('logged_in');
			$data['user_info']= $this->user_model->get_user_info($session_data['id']);
			//$this->form_validation->set_rules('metaPreference[]', 'Meta Preference', 'trim|required');
			if($this->input->post() == NULL)
			{
				$this->load->view('common/header',$data);
				$data['page_data']['PreferenceList'] = $this->changeset->getAssets($data['user_info'][0]->Company_Id);
				$this->load->view('metaPreference',$data);
				$this->load->view('common/footer');
			}
			else
			{
				$PreferenceList = $this->input->post('PreferenceList'); 
				$return = $this->settings_model->update_Preference($data['user_info'][0]->Company_Id,$PreferenceList);
				// print_r($return);
				redirect('/settings/manageMeta', 'refresh');
			}			
	   }
	   else
	   {
		 //If no session, redirect to login page
		redirect($this->config->item('parent_url').'login', 'refresh');
	   }	   
	}
	function password()
	{
	   if($this->session->userdata('logged_in'))
	    {
			$this->form_validation->set_rules('old_password', 'Old Password', 'trim|required');
			$this->form_validation->set_rules('new_password', 'New Password', 'trim|required|matches[confirm_new_password]');
			$this->form_validation->set_rules('confirm_new_password', 'Confirm New Password', 'trim|required|callback_check_database');   
			if($this->form_validation->run() == FALSE)
			{
				//Field validation failed.  User redirected to login page
				$session_data = $this->session->userdata('logged_in');			
				$data['page_title']= 'Eloqua Transporter';
				$data['user_info']= $this->user_model->get_user_info($session_data['id']);
			
				$this->load->view('common/header',$data);
				//$data['scenario_data']['active_scenario'] = $this->scenario->get_list();
				//$data['scenario_data']['inactive_scenario'] = $this->scenario->get_inactive_list();
				$this->load->view('change_password',$data);
				$this->load->view('common/footer');
			}
			else
			{
				//Go to private area
				redirect('home', 'refresh');
			}			
			
	   }
	   else
	   {
		 //If no session, redirect to login page
		redirect($this->config->item('parent_url').'login', 'refresh');
	   }	   
	}
	
	function check_database($confirm_new_password)
	{
	   //Field validation succeeded.  Validate against database
		$session_data = $this->session->userdata('logged_in');
		$username = $session_data['id'];
		$old_password = $this->input->post('old_password');
		$new_password = $this->input->post('new_password');
		//query the database
		
		$result = $this->user_model->change_password($username, $old_password,$new_password,$confirm_new_password);
		if($result)
		{
			return TRUE;
		}
		else
		{
			$this->form_validation->set_message('check_database', 'Invalid Password');
			return false;
		}
	}
}


