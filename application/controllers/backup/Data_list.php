<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data_list extends CI_Controller {

	public function __construct()
     {
          parent::__construct();
          $this->load->helper(array('form', 'url'));
		  $this->load->library('form_validation');
          $this->load->database();
		  $this->load->library('session');		  
		  $this->load->model('get_data_list','',TRUE);
		  $this->load->model('user_model','',TRUE);		  		 
     }
	public function index()
	{
		if($this->session->userdata('logged_in'))
	    {
			$session_data = $this->session->userdata('logged_in');	  
			
	   }
	   else
	   {
		 //If no session, redirect to login page
		 redirect($this->config->item('parent_url').'login', 'refresh');
	   }	   
	}	
	
}



