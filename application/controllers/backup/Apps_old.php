<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Apps extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->database();
		$this->load->library('session');
		$this->load->library('form_validation');		  
		$this->load->helper('form');
		$this->load->model('user_model','',TRUE);		  
	}
	public function index()
	{
		if($this->session->userdata('logged_in'))
	    {	
			$session_data = $this->session->userdata('logged_in');
			$data['page_title']= 'Dashboard';
			$data['user_info']= $this->user_model->get_user_info($session_data['id']);		
			$this->load->view('common/dashboard_header',$data);
			$this->load->view('apps',$data);
			$this->load->view('common/footer');			
	   }
	   else
	   {
		 //If no session, redirect to login page
		 redirect('login', 'refresh');
	   }
	}
}


