<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Compare extends CI_Controller {

	public function __construct()
     {
          parent::__construct();
          $this->load->helper('url');
          $this->load->database();
		  $this->load->library('session');	
		  $this->load->library('form_validation');
		  $this->load->model('eloqua','',TRUE);
		  $this->load->model('settings_model','',TRUE);
		  $this->load->model('user_model','',TRUE);		  
		  $this->load->model('changeset','',TRUE);		  
     }
	public function index()
	{
		if($this->session->userdata('logged_in'))
	    {
			$session_data = $this->session->userdata('logged_in');	  
			
			$data['page_title']= 'Eloqua Transporter';
			$data['user_info']= $this->user_model->get_user_info($session_data['id']);
			
			$data['left_nav'][1] = array("lable"=>'Home',"href"=>'/',"icon_class"=>'fa fa-fw fa-list-alt',"active"=>false);
			$data['left_nav'][2] = array("lable"=>'Compare',"href"=>'/compare',"icon_class"=>'fa fa-fw fa-list-alt',"active"=>true);
				
			$this->load->view('common/header',$data);
			//$this->load->view('common/nav',$data);
			if(($this->input->post('orgID1') !=NULL && $this->input->post('orgID2') !=NULL) || $this->input->post('changeset_id') !=NULL)
			{
				$this->form_validation->set_rules('orgID1', 'orgID1', 'trim|required');
				$this->form_validation->set_rules('orgID2', 'orgID2', 'trim|required');   
				if($this->form_validation->run() == true || $this->input->post('changeset_id') !=NULL )
				{
					if($this->input->post('changeset_id') !=NULL)
					{
						$changeset_id = $this->input->post('changeset_id');
						$orginfo = $this->changeset->getChangesetToOrg($session_data['id'],$changeset_id);
					}
					$temporg = ($this->input->post('changeset_id') != NULL)?$orginfo->org_id:$this->input->post('orgID1');
					$org1= $this->settings_model->get_OrgInfo($session_data['id'],$temporg);
					$org2= $this->settings_model->get_OrgInfo($session_data['id'],$this->input->post('orgID2'));
					//print_r ($org1);
					$PreferenceList = $this->settings_model->get_Preference($session_data['id']);
					if($PreferenceList[0]->Contact ==1)
						$data['page_data']['CompareList']['ContactFields'] = $this->eloqua->get_ContactFields($org1,$org2);
					if($PreferenceList[0]->Account ==1)
						$data['page_data']['CompareList']['AccountFields'] = $this->eloqua->get_AccountFields($org1,$org2);
					if($PreferenceList[0]->CDO ==1)
						$data['page_data']['CompareList']['CustomObjects'] = $this->eloqua->get_CustomObjectsFields($org1,$org2);
					if($PreferenceList[0]->Segments ==1)
						$data['page_data']['CompareList']['SegmentList'] = $this->eloqua->get_SegmentList($org1,$org2);
					if($PreferenceList[0]->Filters ==1)
						$data['page_data']['CompareList']['FilterList'] = $this->eloqua->get_FilterList($org1,$org2);
					if($PreferenceList[0]->Emails ==1)
						$data['page_data']['CompareList']['EmailList'] = $this->eloqua->get_EmailList($org1,$org2);			
					if($PreferenceList[0]->Forms ==1)
						$data['page_data']['CompareList']['FormsList'] = $this->eloqua->get_FormsList($org1,$org2);
					if($PreferenceList[0]->Landing_Pages ==1)
						$data['page_data']['CompareList']['LandingPagesList'] = $this->eloqua->get_LandingPagesList($org1,$org2);
					if($PreferenceList[0]->Campaigns ==1)
						$data['page_data']['CompareList']['CampaignsList'] = $this->eloqua->get_CampaignsList($org1,$org2);
					if($PreferenceList[0]->Programs ==1)
						$data['page_data']['CompareList']['ProgramList'] = $this->eloqua->get_ProgramList($org1,$org2);
					if($PreferenceList[0]->Shared_Lists ==1)
						$data['page_data']['CompareList']['SharedList'] = $this->eloqua->get_SharedList($org1,$org2);
					if($PreferenceList[0]->Email_Groups ==1)
						$data['page_data']['CompareList']['EmailGroups'] = $this->eloqua->get_EmailGroups($org1,$org2);
					if($PreferenceList[0]->Option_Lists ==1)
						$data['page_data']['CompareList']['OptionLists'] = $this->eloqua->get_OptionLists($org1,$org2);
					if($PreferenceList[0]->Events ==1)
						$data['page_data']['CompareList']['EventList'] = $this->eloqua->get_EventList($org1,$org2);
					if($PreferenceList[0]->Contact_Views ==1)
						$data['page_data']['CompareList']['ContactViews'] = $this->eloqua->get_ContactViews($org1,$org2);
					if($PreferenceList[0]->Account_Views ==1)
						$data['page_data']['CompareList']['Account_Views'] = $this->eloqua->get_Account_Views($org1,$org2);
				}
				$data['page_data']['org1'] = $org1[0];
				$data['page_data']['org2'] = isset($org2[0])?$org2[0]:'';
				$data['page_data']['changeset_id'] = $this->input->post('changeset_id');
			}
			$data['page_data']['orglist'] = $this->settings_model->get_OrgList($session_data['id']);
			$data['page_data']['changeset_list'] = $this->changeset->getChangesetList($session_data['id']);
				
			$this->load->view('compare',$data);
			$this->load->view('common/footer');
	   }
	   else
	   {
		 //If no session, redirect to login page
		 redirect($this->config->item('parent_url').'login', 'refresh');
	   }	   
	}
	
	public function deploylist()
	{
		if($this->session->userdata('logged_in'))
	    {
			$session_data = $this->session->userdata('logged_in');	  
			$data = $this->input->post();
			if(isset($data['changeset_id']) && $data['changeset_id'] !=NULL)
			{
				
			}else{
				$data['changeset_id'] = $data['changesetId'];
			}
			//changesetId
			$result = $this->changeset->postChangesetComponent($session_data['id'],$data);
			if($result['success']==false)
			{
				//echo $data['changeset_id'];
				
			}else{
				//echo $data['changeset_id'];
				redirect('/changesetinfo?id='.$data['changeset_id'], 'refresh');
			}
	   }
	   else
	   {
		 redirect($this->config->item('parent_url').'login', 'refresh');
	   }	   
	}
	
}