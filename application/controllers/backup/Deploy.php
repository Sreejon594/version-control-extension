<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Deploy extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->database();
		$this->load->library('session');
		$this->load->helper('form');		  
		$this->load->library('form_validation');
		$this->load->model('changeset','',TRUE);
		$this->load->model('deploy_model','',TRUE);
		$this->load->model('eloqua','',TRUE);
		$this->load->model('settings_model','',TRUE);
		$this->load->model('user_model','',TRUE);		  
	}
	public function index()
	{
		if($this->session->userdata('logged_in'))
	    {
				$session_data = $this->session->userdata('logged_in');			
				$data['page_title']= 'Eloqua Transporter';
				$data['user_info']= $this->user_model->get_user_info($session_data['id']);			
				$data['left_nav'][1] = array("lable"=>'Home',"href"=>'/',"icon_class"=>'fa fa-fw fa-list-alt',"active"=>true);
				$data['left_nav'][2] = array("lable"=>'Compare',"href"=>'/compare',"icon_class"=>'fa fa-fw fa-list-alt',"active"=>false);
				$this->load->view('common/header',$data);
				//$this->load->view('common/nav',$data);
				$data['page_data']['changeset_id'] = $this->input->post('changeset_id');	
				$data['page_data']['component_list'] = $this->changeset->getComponentList($session_data['id'],$data['page_data']['changeset_id']);	
				$data['page_data']['org1'] = $this->settings_model->get_OrgInfo($session_data['id'],$data['page_data']['changeset_id']);	
				$data['page_data']['meta'] = array();
				foreach($data['page_data']['component_list'] as $key=>$val)
				{	$temporg1 = $data['page_data']['org1'];
					$objId = $val->object_id;
					if($val->meta_type == 'AccountFields')
						$data['page_data']['meta'][] = $this->eloqua->get_AccountFieldsById($temporg1,$objId);
					if($val->meta_type == 'ContactFields')
						$data['page_data']['meta'][] = $this->eloqua->get_ContactFieldsById($temporg1,$objId);
					if($val->meta_type == 'CustomObjects')
						$data['page_data']['meta'][] = $this->eloqua->get_CustomObjectsFieldsById($temporg1,$objId);
					if($val->meta_type == 'SegmentList')
						$data['page_data']['meta'][] = $this->eloqua->get_SegmentListById($temporg1,$objId);
					if($val->meta_type == 'FilterList')
						$data['page_data']['meta'][] = $this->eloqua->get_FilterListById($temporg1,$objId);
					if($val->meta_type == 'EmailList')
						$data['page_data']['meta'][] = $this->eloqua->get_EmailListById($temporg1,$objId);			
					if($val->meta_type == 'FormsList')
						$data['page_data']['meta'][] = $this->eloqua->get_FormsListById($temporg1,$objId);
					if($val->meta_type == 'LandingPagesList')
						$data['page_data']['meta'][] = $this->eloqua->get_LandingPagesListById($temporg1,$objId);
					if($val->meta_type == 'CampaignsList')
						$data['page_data']['meta'][] = $this->eloqua->get_CampaignsListById($temporg1,$objId);
					if($val->meta_type == 'ProgramList')
						$data['page_data']['meta'][] = $this->eloqua->get_ProgramListById($temporg1,$objId);
					if($val->meta_type == 'SharedList')
						$data['page_data']['meta'][] = $this->eloqua->get_SharedListById($temporg1,$objId);
					if($val->meta_type == 'EmailGroups')
						$data['page_data']['meta'][] = $this->eloqua->get_EmailGroupsById($temporg1,$objId);
					if($val->meta_type == 'OptionLists')
						$data['page_data']['meta'][] = $this->eloqua->get_OptionListsById($temporg1,$objId);
					if($val->meta_type == 'EventList')
						$data['page_data']['meta'][] = $this->eloqua->get_EventListById($temporg1,$objId);
					if($val->meta_type == 'ContactViews')
						$data['page_data']['meta'][] = $this->eloqua->get_ContactViewsById($temporg1,$objId);
					if($val->meta_type == 'Account_Views')
						$data['page_data']['meta'][] = $this->eloqua->get_Account_ViewsById($temporg1,$objId);
				}
				$data['page_data']['orglist'] = $this->settings_model->get_OrgList($session_data['id']);
				$this->load->view('deploy',$data);
				$this->load->view('common/footer');
			
	   }
	   else
	   {
		 //If no session, redirect to login page
		 redirect($this->config->item('parent_url').'login', 'refresh');
	   }	   
	}

	public function send_deploy()
	{
		if($this->session->userdata('logged_in'))
	    {
			$this->form_validation->set_rules('org_id', 'Source Org', 'trim|required');
			$this->form_validation->set_rules('orgID2', 'Destination Org', 'trim|required');
			$this->form_validation->set_rules('changeset_id', 'Password', 'trim|required');   
			if($this->form_validation->run() == FALSE)
			{
				redirect('/changesetinfo?id='.$this->input->post('changeset_id'), 'refresh');
			}else
			{
				$session_data = $this->session->userdata('logged_in');			
				$data['page_title']= 'Eloqua Transporter';
				$data['user_info']= $this->user_model->get_user_info($session_data['id']);			
				$data['left_nav'][1] = array("lable"=>'Home',"href"=>'/',"icon_class"=>'fa fa-fw fa-list-alt',"active"=>true);
				$data['left_nav'][2] = array("lable"=>'Compare',"href"=>'/compare',"icon_class"=>'fa fa-fw fa-list-alt',"active"=>false);
				//$this->load->view('common/header',$data);
				//$this->load->view('common/nav',$data);
				$data['page_data']['changeset_id'] = $this->input->post('changeset_id');	
				$data['page_data']['component_list'] = $this->changeset->getComponentList($session_data['id'],$data['page_data']['changeset_id']);	
				$data['page_data']['org1'] = $this->settings_model->get_OrgInfo($session_data['id'],$this->input->post('org_id'));	
				$data['page_data']['org2'] = $this->settings_model->get_OrgInfo($session_data['id'],$this->input->post('orgID2'));	
				$data['page_data']['meta'] = array();
				foreach($data['page_data']['component_list'] as $key=>$val)
				{	$temporg1 = $data['page_data']['org1'];
					$objId = $val->object_id;
					if($val->meta_type == 'AccountFields')
						$data['page_data']['meta'][] = $this->eloqua->get_AccountFieldsById($temporg1,$objId);
					if($val->meta_type == 'ContactFields')
						$data['page_data']['meta'][] = $this->eloqua->get_ContactFieldsById($temporg1,$objId);
					if($val->meta_type == 'CustomObjects')
						$data['page_data']['meta'][] = $this->eloqua->get_CustomObjectsFieldsById($temporg1,$objId);
					if($val->meta_type == 'SegmentList')
						$data['page_data']['meta'][] = $this->eloqua->get_SegmentListById($temporg1,$objId);
					if($val->meta_type == 'FilterList')
						$data['page_data']['meta'][] = $this->eloqua->get_FilterListById($temporg1,$objId);
					if($val->meta_type == 'EmailList')
						$data['page_data']['meta'][] = $this->eloqua->get_EmailListById($temporg1,$objId);			
					if($val->meta_type == 'FormsList')
						$data['page_data']['meta'][] = $this->eloqua->get_FormsListById($temporg1,$objId);
					if($val->meta_type == 'LandingPagesList')
						$data['page_data']['meta'][] = $this->eloqua->get_LandingPagesListById($temporg1,$objId);
					if($val->meta_type == 'CampaignsList')
						$data['page_data']['meta'][] = $this->eloqua->get_CampaignsListById($temporg1,$objId);
					if($val->meta_type == 'ProgramList')
						$data['page_data']['meta'][] = $this->eloqua->get_ProgramListById($temporg1,$objId);
					if($val->meta_type == 'SharedList')
						$data['page_data']['meta'][] = $this->eloqua->get_SharedListById($temporg1,$objId);
					if($val->meta_type == 'EmailGroups')
						$data['page_data']['meta'][] = $this->eloqua->get_EmailGroupsById($temporg1,$objId);
					if($val->meta_type == 'OptionLists')
						$data['page_data']['meta'][] = $this->eloqua->get_OptionListsById($temporg1,$objId);
					if($val->meta_type == 'EventList')
						$data['page_data']['meta'][] = $this->eloqua->get_EventListById($temporg1,$objId);
					if($val->meta_type == 'ContactViews')
						$data['page_data']['meta'][] = $this->eloqua->get_ContactViewsById($temporg1,$objId);
					if($val->meta_type == 'Account_Views')
						$data['page_data']['meta'][] = $this->eloqua->get_Account_ViewsById($temporg1,$objId);
				}
				$data['page_data']['orglist'] = $this->settings_model->get_OrgList($session_data['id']);
				$result = $this->eloqua->postDeployLog($session_data['id'],$data['page_data']);
				redirect('/deploy/result?changeset_id='.$data['page_data']['changeset_id'], 'refresh');
				//echo '<pre>';
				//print_r($data);
				//$this->load->view('deploy',$data);
				//$this->load->view('common/footer');
			}
			
	   }
	   else
	   {
		 //If no session, redirect to login page
		 redirect($this->config->item('parent_url').'login', 'refresh');
	   }	   
	}	
	public function result()
	{
		if($this->session->userdata('logged_in'))
	    {
			
			$session_data = $this->session->userdata('logged_in');			
			$data['page_title']= 'Eloqua Transporter';
			$data['user_info']= $this->user_model->get_user_info($session_data['id']);			
			$data['left_nav'][1] = array("lable"=>'Home',"href"=>'/',"icon_class"=>'fa fa-fw fa-list-alt',"active"=>true);
			$data['left_nav'][2] = array("lable"=>'Compare',"href"=>'/compare',"icon_class"=>'fa fa-fw fa-list-alt',"active"=>false);
			$this->load->view('common/header',$data);
			//$this->load->view('common/nav',$data);
			$data['page_data']['changeset_id'] = $this->input->get('changeset_id');	
			$data['page_data']['DeployHistory'] = $this->changeset->getDeployHistory($session_data['id'],$data['page_data']['changeset_id']);	
			//echo '<pre>';
			//print_r($data);
			$this->load->view('deploy_result',$data);
			$this->load->view('common/footer');
			
			
	   }
	   else
	   {
		 //If no session, redirect to login page
		 redirect($this->config->item('parent_url').'login', 'refresh');
	   }	   
	}	
}