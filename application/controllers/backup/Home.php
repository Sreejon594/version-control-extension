<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct()
     {
          parent::__construct();
          $this->load->helper('url');
          $this->load->database();
		  $this->load->library('session');	
		  $this->load->library('form_validation');
		  $this->load->model('changeset','',TRUE);
		  $this->load->model('eloqua','',TRUE);
		  $this->load->model('settings_model','',TRUE);
		  $this->load->model('user_model','',TRUE);	
		  $this->load->helper('cookie');
		 $this->load->library('Curl');   
     }

	public function index()
	{
		// echo '<pre>';
		// $this->session->set_userdata('logged_in', $sess_array);
		//$session_data = $this->session->userdata('appcloud.portqii');	
		
		// $cookie = $this->input->cookie();
		// echo 'cookie()-<br/>';
		// print_r($cookie);
		// echo 'logged_in-<br/>';
		// print_r($this->session->userdata('logged_in'));
		// if(isset($cookie['ci_session'])){
		// $dbcookie = $this->user_model->getCookie($cookie['ci_session']);
		
		// echo 'dbcookie-<br/>';
		// print_r($dbcookie);
		// }
// exit;
// echo '</pre>';
		if($this->session->userdata('logged_in'))
	    {
			$this->form_validation->set_rules('orgID', 'org', 'trim|required');
			$this->form_validation->set_rules('list_name', 'list_name', 'trim|required'); 
			
			$session_data = $this->session->userdata('logged_in');			
			$data['page_title']= 'Eloqua Transporter';
			$data['user_info']= $this->user_model->get_user_info($session_data['id']);			
			$data['left_nav'][1] = array("lable"=>'Home',"href"=>'/',"icon_class"=>'fa fa-fw fa-list-alt',"active"=>true);
			$data['left_nav'][2] = array("lable"=>'Compare',"href"=>'/compare',"icon_class"=>'fa fa-fw fa-list-alt',"active"=>false);
			$this->load->view('common/header',$data);
			//$this->load->view('common/nav',$data);
			$data['page_data']['changesetList'] = $this->changeset->getChangesetList($session_data['id']);	
			$this->load->view('home',$data);
			$this->load->view('common/footer');
			
	   }
	   else
	   {
		 redirect($this->config->item('parent_url').'login', 'refresh');
	   }
	}
	
	public function newlist()
	{
		if($this->session->userdata('logged_in'))
	    {
			$session_data = $this->session->userdata('logged_in');				
			$data['page_title']= 'Eloqua Transporter';
			$data['user_info']= $this->user_model->get_user_info($session_data['id']);
			$data['page_data']['orglist'] = $this->settings_model->get_OrgList($session_data['id']);
			$data['left_nav'][1] = array("lable"=>'Home',"href"=>'/',"icon_class"=>'fa fa-fw fa-list-alt',"active"=>false);
			$data['left_nav'][2] = array("lable"=>'Compare',"href"=>'/compare',"icon_class"=>'fa fa-fw fa-list-alt',"active"=>false);
				
			$this->form_validation->set_rules('orgID', 'org', 'trim|required');
			$this->form_validation->set_rules('list_name', 'list_name', 'trim|required'); 
			if($this->form_validation->run() == FALSE)
			{				
				$this->load->view('common/header',$data);
				//$this->load->view('common/nav',$data);
				$this->load->view('newchangesetlist',$data);
				$this->load->view('common/footer');
			}else
			{
				$tempdata['list_name'] = $this->input->post('list_name');
				$tempdata['org_id'] = $this->input->post('orgID');
				$result = $this->changeset->postChangesetList($session_data['id'],$tempdata);
				if($result['success']==false)
				{
					$data['page_errors'] =$result['msg'];
					$this->load->view('common/header',$data);
					//$this->load->view('common/nav',$data);
					$this->load->view('newchangesetlist',$data);
					$this->load->view('common/footer');
				}
				if($result['success']==true)
				{
					redirect('changesetinfo?id='.$result['id'], 'refresh');
				}
			}
	   }
	   else
	   {
		 //If no session, redirect to login page
		 redirect($this->config->item('parent_url').'login', 'refresh');
	   }	   
	}
	function logout()
	 {
	   $this->session->unset_userdata('logged_in');
	   session_destroy();
	   redirect('home', 'refresh');
	 }
}