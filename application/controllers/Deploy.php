<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require(APPPATH.'/third_party/json_path/vendor/autoload.php');
use Peekmo\JsonPath\JsonStore;
use Flow\JSONPath\JSONPath;
use Flow\JSONPath\JSONPathLexer;
use \Peekmo\JsonPath\JsonPath as PeekmoJsonPath;

class Deploy extends CI_Controller 
{
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->database();
		$this->load->library('session');
		$this->load->helper('form');		  
		$this->load->library('form_validation');
		$this->load->model('changeset','',TRUE);
		// $this->load->model('deploy_model','',TRUE);
		$this->load->model('deploy_model','',TRUE);
		$this->load->model('eloqua','',TRUE);
		$this->load->model('deploy_helper_model','',TRUE);
		$this->load->model('settings_model','',TRUE);
		$this->load->model('user_model','',TRUE);
		$this->load->model('Logging_model','',TRUE);
	}
	
	public function index()
	{
		if($this->session->userdata('logged_in'))
	    {
			//echo 'hi';
		}
		else
		{
		 //If no session, redirect to login page
		 //redirect($this->config->item('parent_url').'login', 'refresh');
		}	   
	}

	public function result()
	{
		if($this->session->userdata('logged_in'))
	    {
			$session_data = $this->session->userdata('logged_in');			
			$data['page_title']= 'Oracle Eloqua Transporter';
			$data['user_info']= $this->user_model->get_user_info($session_data['id']);
			$data['left_nav'][1] = array("lable"=>'Home',"href"=>'/',"icon_class"=>'fa fa-fw fa-list-alt',"active"=>true);
			$data['left_nav'][2] = array("lable"=>'Compare',"href"=>'/compare',"icon_class"=>'fa fa-fw fa-list-alt',"active"=>false);
			$this->load->view('common/header',$data);
			//$this->load->view('common/nav',$data);
			$data['page_data']['changeset_id'] = $this->input->get('changeset_id');	
			$data['page_data']['DeployHistory'] = $this->changeset->getDeployHistory($session_data['id'],$data['page_data']['changeset_id']);	
			$this->load->view('deploy_result',$data);
			$this->load->view('common/footer');
		}
		else
		{
			//If no session, redirect to login page
			redirect($this->config->item('parent_url').'login', 'refresh');
		}	   
	}

	public function deploymentinqueue()
	{
		if($this->session->userdata('logged_in'))
	    {
			$skipValidationFlag=$this->input->post('skipValidationFlag');
			$data['deploy_info']= $this->deploy_model->deploy_inqueue($this->input->get('pId'),$skipValidationFlag);
		}
		else
		{
			//If no session, redirect to login page
			redirect($this->config->item('parent_url').'login', 'refresh');
		}
	}
	
	public function deployment()
	{
		$data['deploy_info']= $this->deploy_model->deploy_queue();		
	}
	
	public function deployment_recursive()
	{
		/*Endpoint for Deploy*/
		
		//$data['deploy_info']= $this->deploy_model->updateMissingElements(2877);
		
		// $data['deploy_info']= $this->deploy_model->validatepackage_recursive(2901);
		
		$data['deploy_info']= $this->deploy_model->pre_deploy_package_item(2916,3383);
		
		
		/* Endpoint for Validate - Change packageId with ur package id*/
		
		// $data['deploy_info']= $this->deploy_model->validatepackage_recursive(2870);
		//$data['deploy_info']= $this->deploy_helper_model->getPackageCampaign(2588);
		
		// $data['deploy_info']= $this->deploy_helper_model->checkForUserDefinedMappingForAsset(2528);
		
		//Other Endpoints
		//echo 'hello';
		
		// $data['deploy_info']= $this->eloqua->getAssetChild(2865,39842,426,'Form','Read Single');
		
		//echo 'hello';
		// $data['deploy_info']= $this->deploy_model->updateMissingAssetJsonAndStatus(2477);
		// $result = $this->deploy_helper_model->checkForTokenAvailability(1888);
		// $data['deploy_info']= $this->deploy_model->handleCircularDependencyafterDeploy(1714);	
		// $data['deploy_info']= $this->deploy_model->deploy_inqueue(1697);
		// $data['deploy_info']= $this->deploy_model->deploy_queue();
		// print_r('<pre>');
		//$dataOrg1 = $this->eloqua->getAssetChild(1806,18437,561,'Segment');
		// print_r($dataOrg1);
		//$data['deploy_info']= $this->deploy_model->identifyCircular_initiate_updated(1890);
		//$result = $this->deploy_helper_model->getFolderStructure(25845);
		//print_r('<pre>');
		//$result = $this->callTest();
		//print_r($result);
		
	
		
	}
	
	public function validationinqueue()
	{
		if($this->session->userdata('logged_in'))
	    {
			$data['deploy_info']= $this->deploy_model->validate_inqueue($this->input->post('PackageId'),$this->input->post('Auto'));
		}
		else
		{
			//If no session, redirect to login page
			//redirect($this->config->item('parent_url').'login', 'refresh');
		}
	}
	
	public function validate()
	{
		$data['validate_info']= $this->deploy_model->validate_queue_recursive();		
	}
	
	
	function injSON($jsonObj,$node,$key,$type)
	{
		if(is_array($jsonObj->$node))
		{
			foreach($jsonObj->$node  as $key1=>$val1)
			{
				$pi2 = explode(",",$key);
				foreach($pi2 as $k1=>$v1)
				{
					unset($val1->$v1);
				}
			}
		}
		if(is_object($jsonObj->$node))
		{
			//$this->injSON($jsonObj,$node,$key,"object");
		}
		return $jsonObj;
	}
	
	function valueinjSON($jsonObj,$key1,$JSON_Node)
	{		
		if(is_object($jsonObj) || is_array($jsonObj))
		{
			foreach ($jsonObj as $key=>$val) 
			{			
				if (is_object($val) || is_array($val)) 
				{
					$this->valueinjSON((array)$val,$key1,$JSON_Node);
				} 
				else if ($key1 == $key) 
				{
					//echo '<pre>';
						////print_r($jsonObj);
					//echo '</pre>';
		
				}
			}
		}
		return $jsonObj;
	}
	
	function checkForTokenAvailability()
	{
		$packageId=2784;
		$packageId = $this->input->post('PackageId');
		// print_r($result);
		//exit;
		$result = $this->deploy_helper_model->checkForTokenAvailability($packageId);
		$data['message'] =$result; 
		
		$this->output->set_output(json_encode($data));
		// return $result;
	}
	
	function skipValidationOnDeploy()
	{
		$packageId = $this->input->post('packageId');
		$skipValidationFlag = $this->input->post('skipValidation'); 
		$this->changeset->skipValidationOnDeploy($packageId,$skipValidationFlag);
	}
	
	function callDependencyEndpoint()
	{
		//$result = $this->deploy_model->callDependencyEnd();
		print_r('hello');
	}
	
	function callTransporterJaveEnd()
	{
		$result = $this->deploy_model->callTransporterJaveEnd();
		return $result;
	}
	
	function callTransporterJaveEndNew()
	{
		$result = $this->deploy_model->callTransporterJaveEndNew();
	}	
			
	function callValidationEndpoint()
	{
		$this->deploy_helper_model->invalidateDuplicateCampaignField($packageId=2757);
	}
	
	function callTest()
	{
		
		$JSON_Submitted=json_decode($this->getSJ(2576));
		// $packageValidationList=$this->getPackageValidationList($packageId=2434);
		
		// echo '<pre>';
			// print_r($JSON_Submitted);
		// echo '</pre>';
		
		if($JSON_Submitted->type=='ContactSegment' && strpos(json_encode($JSON_Submitted), 'ContactListSegmentElement') !== false)
		{			
			foreach($JSON_Submitted->elements as $clKey=>$clVal){
				if(isset($clVal->list))
				{
					$message='' .$clVal->list->name;
					$this->deploy_helper_model->updateActionRequiredToDeploymentPackageItem($Package_Item->Deployment_Package_Item_Id,$message);
					if($this->setSkeletonFlag==0)
					{
						$this->setSkeletonFlag=1;
					}
				}
			}
		}
		//$result=$this->deploy_helper_model->replaceDocumentDiscriptionInEmail($submittedJSON,json_encode($packageValidationList));
		//$result=$this->deploy_helper_model->handleHTMLChanges($submittedJSON,json_encode($packageValidationList));
		
		//print_r($result);
		
	}
	
	function getSJ($packageId)
	{
		$this -> db -> select('JSON_Asset');
		$this -> db -> from('deployment_package_item');
		$this -> db -> where('Deployment_Package_Id', $packageId);
		$this -> db -> where('Asset_Type', 'Landing Page');
		$this -> db -> where('Asset_Id', 1834);
		$query = $this -> db -> get();		
		if($query->num_rows()>0)
		{
			$defaultMSResults = $query->result()[0];
			$submittedJSON=$defaultMSResults->JSON_Asset;				
		}
		return $submittedJSON;
	}
	
	function getPackageValidationList($packageId)
	{
		$this -> db -> select('*');
		$this -> db -> from('deployment_package_validation_list');
		$this -> db -> where('Deployment_Package_Id', $packageId);
		$query = $this -> db -> get();
		if($query->num_rows()>0)
		{
			$packageValidationList = $query->result();			
		}
		return $packageValidationList;
	}
	
	function parseSubject()
	{
		// $subject = "Hi<eloqua entity=\"dynamiccontent\" dynamiccontentid=\"62\">TRX_A_DC_Hyperlink_FS</eloqua><eloqua entity=\"dynamiccontent\" dynamiccontentid=\"93\">TRX_D_DC_Hyperlinks_All</eloqua><eloqua entity=\"dynamiccontent\" dynamiccontentid=\"25\">TRX_DC_WithRule 180517 3</eloqua>";
		$subject = "Hi <eloqua type=\"emailfield\" syntax=\"DEP_N_Field_Merge_140917_11\">DEP_N_Field_Merge_140917_11</eloqua>₵<eloqua entity=\"dynamiccontent\" dynamiccontentid=\"109\">DEP_N_Dynamic Content 211217 1</eloqua>";
		$assetArray = array();
		$new = htmlspecialchars($subject, ENT_NOQUOTES);
		$html = str_get_html($new);
		//$dynamiccontentId= explode('dynamiccontentid=',$subject);
		$palinText_temp=explode("/eloqua",$new);
		foreach($palinText_temp as $ptKey=>$ptVal)
		{
			$assetObj = array();
			$splitText=explode("dynamiccontentid=\"",$ptVal);
			$endStr = end($splitText);
			$id = strstr($endStr, '"', true);			
			
			// echo '<br>';
				// print_r($endStr);
			// echo '<br>';
				// print_r($id);
			// echo '<br>';
				
			$si = strrpos($endStr, '&gt;')+4;
			$ei = strrpos($endStr, '&lt;', -1);
			$len = $ei - $si;
			$assetName = substr($endStr, $si, $len);
			
			// echo '<br>startIndex<br>';
				// print_r($si);
			// echo '<br>endindex<br>';
				// print_r($ei);
			// echo '<br>name<br>';
				// print_r($assetName);
			
			if($id != '' && $assetName != '')
			{
				array_push($assetObj,$id,$assetName);
				array_push($assetArray,$assetObj);
			}
			
		}
		$palinText_temp=explode(" ",$palinText_temp[1]);
		$signaturelayout_asset_id=str_replace('"','',$palinText_temp[0]);
		// echo '<br><pre>';
		// print_r($assetArray);
	}
	
	function searchAsset()
	{
		// $assetName = 'TRX_SNM_Seg_Test 160518 1:Copy';
		$assetName = 'Trx_Snm_A:B[]C&D*E()F?G>H<I,J.K/L;M\'N:O"P[]Q\R{}S|T~U!V@W#X$Y%Z^A&B*C()D_E+F-G=H`I';
		$posFlag = 0;
		$pattern = '/[\':;`!^£$%&*()}{@#~?><>,|=+¬-]/';
		
		 // $pos = strpos($assetName,'/[^A-Za-z0-9\-]/');
		 // echo '<br>pos<br><pre>';
			// print_r($pos);
		// echo '</pre><br>';
		
		// if($assetName{$pos-1}!=' ' && $assetName{$pos+1}!=' ')
		// {
			// $posFlag = 1;
		// $plainText_temp=explode(":",$assetName);
		// $tempAssetName = trim($plainText_temp[0]);
		
		// echo '<br>pos<br><pre>';
			// print_r($pos);
		// echo '</pre><br>';
		// echo '<br>array<br><pre>';
			// print_r($plainText_temp);
		// echo '</pre><br>';
		// echo '<br>new name <br><pre>';
			// print_r($tempAssetName);
		// echo '</pre><br>';
		// echo '<br>posFlag<br><pre>';
			// print_r($posFlag);
		// echo '</pre><br>';
		// }
		// else
		
		if(preg_match($pattern,  $assetName))
		{
			echo 'in replace';
			$tempAssetName = str_replace('-', '_', $assetName);
			$tempAssetName = preg_replace('/[^A-Za-z0-9\-]/','*',$tempAssetName);
		}
		else
			$tempAssetName=$assetName;
		
		echo '<br>assetName<br><pre>';
			print_r($assetName);
		echo '</pre><br>';
		
		$returnedList = $this->deploy_model->getSourceAssetJSONById('Email',$tempAssetName,2534,$posFlag);
		// $returnedList = $this->deploy_model->getSourceAssetJSONById('Email',$tempAssetName,2525,$posFlag);
		
		echo 'returned assets<br><pre>';
			print_r ($returnedList);
		echo '</pre><br>';
		$assetArray = array();
		foreach($returnedList->elements as $rKey=>$rVal)
		{
			if($rVal->name==$assetName)
				array_push($assetArray,$rVal);
				
		}
		echo 'matching Assets<br><pre>';
			print_r ($assetArray);
		echo '</pre><br>';
	}
	
	function pickListInvalid(){
		echo 'here';
		$this->deploy_helper_model->packagesForDestination('TechnologyPartnerportQii','CSAjaySikri',2778);
	}
	
	function FMInLP(){
		
		$json=$this->getSJ(2707);
		$this->deploy_helper_model->removeFMOfTypeEventFromLandingPage($json,2707);
	}
	
	//Version Control Sreejon 080818
	// function vcRollback($parentAssetDetails)
	// {
		// $this->logging_model->logT(9999,999991,'coming from VC','return result from ',$parentAssetDetails);
		// $this->logging_model->logT(9999,999991,'coming from VC','return result from ',json_encode($parentAssetDetails));
		// $parentAssetDetails = json_encode($parentAssetDetails);
		// $this->eloqua->create_rollback_package($parentAssetDetails->siteName,$parentAssetDetails->siteId,$parentAssetDetails->userName,$parentAssetDetails->assetType,$parentAssetDetails->assetName,$parentAssetDetails->assetId);
	// }
	
	// function vcRollback()
	// {
		// $siteId = $this->input->get('siteId');
		
		// $this->logging_model->logT(9999,999992,'coming from VC','return result from ',$siteId);
		// $this->logging_model->logT(9999,999991,'coming from VC','return result from ',json_encode($parentAssetDetails));
		// $parentAssetDetails = json_encode($parentAssetDetails);
		// $this->eloqua->create_rollback_package($parentAssetDetails->siteName,$parentAssetDetails->siteId,$parentAssetDetails->userName,$parentAssetDetails->assetType,$parentAssetDetails->assetName,$parentAssetDetails->assetId);
	// }
	
	function vcValidate()
	{
		// $this->logging_model->logT(9999,999991,'coming from VC','return result from ',json_decode($_POST['parentData']));
		
		// $parentAssetDetails = json_encode(json_decode($_POST['parentData']));
		$parentAssetDetails=(array)json_decode($_POST['parentData'],true);
		$this->logging_model->logT(9999,999995,'coming from VC for versionControlId '.$parentAssetDetails['versionControlId'],'return result from ',json_encode(json_decode($_POST['parentData'])));
		$this->logging_model->logT(9999,999996,'coming from VC for versionControlId '.$parentAssetDetails['versionControlId'],'siteId',$parentAssetDetails['siteId']);
		$packageId = $this->eloqua->create_validate_package($parentAssetDetails['siteName'],$parentAssetDetails['siteId'],$parentAssetDetails['userName'],$parentAssetDetails['assetType'],$parentAssetDetails['assetName'],$parentAssetDetails['assetId'],$parentAssetDetails['versionControlId']);
		
		$arr = array('versionControlId' => $parentAssetDetails['versionControlId'], 
					'assetId' => $parentAssetDetails['assetId'], 
					'assetName' => $parentAssetDetails['assetName'], 
					'assetType' => $parentAssetDetails['assetType'], 
					'packageId' => $packageId);
					
		echo json_encode($arr);
	}
	
	function vcRollback()
	{
		
		$parentAssetDetails=(array)json_decode($_POST['parentData'],true);
		$valPackId=$parentAssetDetails['validationPackageId'];
		
		$this->logging_model->logT(9999,999998,'coming from VC for Deployment_Package_Id '.$valPackId,'return result from ',$parentAssetDetails);
		
		$this->deploy_model->pre_deploy_package_item($valPackId,0);
		$packageValidationItem = "empty";
		$this -> db -> select('*');
		$this -> db -> from('deployment_package_item');
		$this -> db -> where('Deployment_Package_Id', $valPackId);
		$this -> db -> where('Asset_Type', $parentAssetDetails['assetType']);
		$this -> db -> where('Asset_Id', $parentAssetDetails['assetId']);
		$query = $this -> db -> get();
		if($query->num_rows()>0)
		{
			$packageValidationItem = $query->result()[0];			
		}
		$this->logging_model->logT(9999,999999,'coming from VC for Deployment_Package_Id '.$valPackId,'return result from ',$packageValidationItem);
	}
	
}