<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->database();
		$this->load->model('user_model','',TRUE);
		$this->load->library('form_validation');
		  
		$this->load->helper('form');
		$this->load->library('session');
	}

	function index()
	{
		// print_r("coming");exit;
		$data['page_title']= 'Oracle Eloqua Transporter';
		//This method will have the credentials validation 
		$this->form_validation->set_rules('username', 'Username', 'trim|required');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|callback_check_database');   
		if($this->form_validation->run() == FALSE)
		{
			// print_r("validation failed");exit;
			//Field validation failed.  User redirected to login page
			$this->load->view('login',$data);
		}
		else
		{
			//Go to private area
			// print_r("coming to transporter");exit;
			redirect('home', 'refresh');
		}
	}
	
	function check_database($password)
	{
		//Field validation succeeded.  Validate against database
		$username = $this->input->post('username');

		//query the database
		$result = $this->user_model->login($username, $password);
		if($result)
		{
			$sess_array = array();
			foreach($result as $row)
			{
			$sess_array = array(
			'id' => $row->id,
			'email' => $row->email,
			'first_name' => $row->first_name,
			'last_name' => $row->last_name,
			);
			$this->session->set_userdata('logged_in', $sess_array);
			}
			return TRUE;
		}
		else
		{
			$this->form_validation->set_message('check_database', 'Invalid username or password');
			return false;
		}
	}
	
	function status()
	{
	}
}
