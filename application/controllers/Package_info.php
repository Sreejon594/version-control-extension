<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Package_info extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
		$this->load->database();
		$this->load->library('session');	
		$this->load->library('form_validation');
		$this->load->model('changeset','',TRUE);
		$this->load->model('license_model','',TRUE);
		$this->load->model('deploy_helper_model','',TRUE);
	}
	public function index()
	{	
		if($this->session->userdata('logged_in'))
	    {
			$session_data = $this->session->userdata('logged_in');
			//print_r($session_data);	
			if(isset($session_data['siteId']) && isset($session_data['siteName']))
			{	
				$data['page_title']= 'Oracle Eloqua Transporter';
				$data['page_data']['sites_list']= $this->license_model->associatedSitesForSiteName($session_data['siteName']);
				$this->load->view('common/header',$data);
				$data['page_data']['changeset_id'] = $this->input->get('id');	
				$data['page_data']['changeset_info'] = $this->changeset->getChangesetToOrg($session_data['siteId'],$this->input->get('id'));
				
				$data['page_data']['AssetType'] = $this->changeset->getAssetType($session_data['siteId']);	
				$data['page_data']['component_list'] = $this->changeset->getComponentList($session_data['siteId'],$this->input->get('id'));	
				$data['page_data']['externalInstance'] = $this->settings_model->get_externalInstances($session_data['siteId']);
				
			    $data['page_data']['ValidatedAsset'] = $this->changeset->getValidatedAssetCount($this->input->get('id'));
				$data['page_data']['ShellParentChildConfigList'] = $this->changeset->getShellParentChildConfig();
				// $data['page_data']['defaultMicrositeAssignedStatus'] = $this->changeset->defaultMicrositeAssignedStatus($Package_Id);
				
				if(isset($session_data) && isset($session_data['appId']))
				{
					$data['page_data']['homeURL']=$this->changeset->getHomeURL($session_data);
					if(isset($data['page_data']['homeURL']))
					{
						$packageId = $this->input->get('id');
						if(isset($packageId) && (!empty($packageId)))
						{
							$this->eloqua->validateTokens($packageId);
						}
						$this->load->view('newpackage_copy',$data);
						
					}	
					else
					{
						$data['page_title']= 'Oracle Eloqua Transporter';
						$data['sessionExpireStatus']=true;
						$this->load->view('user_authentication',$data);						
					}
				}
				else
				{
					$data['page_title']= 'Oracle Eloqua Transporter';
					$data['sessionExpireStatus']=true;
					$this->load->view('user_authentication',$data);					
				}
				$this->load->view('common/footer');
			}
			else
			{	
				$data['page_title']= 'Oracle Eloqua Transporter';
				$data['sessionExpireStatus']=true;
				$this->load->view('common/header',$data);
				$this->load->view('user_authentication',$data);
			}		
		}
		else
		{
			$data['page_title']= 'Oracle Eloqua Transporter';
			$data['sessionExpireStatus']=true;
			$this->load->view('common/header',$data);
			$this->load->view('user_authentication',$data);
		}	   
	}
	
	public function create_new_Package()
	{
		if($this->session->userdata('logged_in'))
	    {
			$session_data = $this->session->userdata('logged_in');
			$package_Name = $this->input->post('packageName');
			$package_description = $this->input->post('packageDescription');
			$source_siteName = $this->input->post('sourceName');
			$target_siteName = $this->input->post('targetName');
			$package_id_hidden = $this->input->post('package_id');
			$jsonShell = $this->input->post('jsonShell');
			if($this->license_model->haveCommonLicenses($source_siteName,$target_siteName))
			{
				$package_Id = $this->changeset->create_new_package($package_id_hidden,$package_Name,$package_description,$source_siteName,$target_siteName,$session_data['siteId'],$session_data['userName'],$jsonShell);
				$this->output->set_output(json_encode($package_Id));
			}
			else
			{
				$this->output->set_output('Source and Target site have different licenece');
			}	
		}
		else
		{
			$data['page_title']= 'Oracle Eloqua Transporter';
			$data['sessionExpireStatus']=true;
			$this->load->view('common/header',$data);
			$this->load->view('user_authentication',$data);
		}
	}
	
	public function addtopackage()
	{
		if($this->session->userdata('logged_in'))
	    {
			$session_data = $this->session->userdata('logged_in');
			$data = $this->input->post();
			print_r($data);
			$result = $this->changeset->addtopackage($session_data['siteId'],$data);
			
			if($result == true)
			{
				redirect('/', 'refresh');
			}
			if($result == false)
			{
				$this->load->view('error',$data);
			}	
		}
		else
		{
			$data['page_title']= 'Oracle Eloqua Transporter';
			$data['sessionExpireStatus']=true;
			$this->load->view('common/header',$data);
			$this->load->view('user_authentication',$data);
		}	
	}

	public function delete()
	{
		if($this->session->userdata('logged_in'))
	    {
			$session_data = $this->session->userdata('logged_in');			
			$data['page_title']= 'Oracle Eloqua Transporter';
			// $data['user_info']= $this->user_model->get_user_info($session_data['id']);			
			$this->load->view('common/header',$data);
			//$this->load->view('common/nav',$data);	
			$result = $this->changeset->deleteChangeset($session_data['siteId'],$this->input->post('changeset_id'));
			if($result == true)
			{
				redirect('/', 'refresh');
			}
			if($result == false)
			{
				$this->load->view('error',$data);
			}				
			$this->load->view('common/footer');
		}
		else
		{
			$data['page_title']= 'Oracle Eloqua Transporter';
			$data['sessionExpireStatus']=true;
			$this->load->view('common/header',$data);
			$this->load->view('user_authentication',$data);
		}	   
	}
	
	public function deleteComponent()
	{
		if($this->session->userdata('logged_in'))
	    {
			$session_data = $this->session->userdata('logged_in');			
			$data['page_title']= 'Oracle Eloqua Transporter';
			$this->load->view('common/header',$data);
			$this->load->view('common/nav',$data);
			$tempdata['changeset_id']=$this->input->post('changeset_id');
			$tempdata['component_id']=$this->input->post('component_id');
			
			$result = $this->changeset->deleteComponent($session_data['siteId'],$tempdata);
			if($result == true)
			{
				redirect('/package_info?id='.$tempdata['changeset_id'], 'refresh');
			}
			if($result == false)
			{
				$this->load->view('error',$data);
			}				
			$this->load->view('common/footer');
		}
		else
		{
			$data['page_title']= 'Oracle Eloqua Transporter';
			$data['sessionExpireStatus']=true;
			$this->load->view('common/header',$data);
			$this->load->view('user_authentication',$data);
		}	
	}
	
	public function getAssetType()
	{
		$session_data = $this->session->userdata('logged_in');
		$result = $this->changeset->getAssetType($session_data['id']);	
	}
	
	public function getAssetData()
	{
		$session_data = $this->session->userdata('logged_in');
		$child_site_name = $this->input->post('OrgId');
		$meta = $this->changeset->getAssetNameById($this->input->post('Asset_Type_Id'));
		$page = $this->input->post('page');	
		$org= $this->settings_model->get_child_site_assets($child_site_name);
		$dataOrg1 = $this->eloqua->getAsset($org[0] , $meta , $page);
		echo $dataOrg1;
	}
	
	public function searchAsset()
	{
		$session_data = $this->session->userdata('logged_in');
		$child_site_name = $this->input->post('OrgId');
		$AssetName = $this->input->post('AssetName');
		$meta = $this->changeset->getAssetNameById($this->input->post('Asset_Type_Id'));
		$page = $this->input->post('page');	
		$org= $this->settings_model->get_child_site_assets($child_site_name);
		$dataOrg1 = $this->eloqua->getSearchAsset($org[0] , $meta , $page, $AssetName);	
		print_r($dataOrg1);
	}
	
	public function rename_package_name()
	{
		if($this->session->userdata('logged_in'))
	    {
			$session_data = $this->session->userdata('logged_in');
			$package_name = $this -> input -> post('package_name');
			$package_description = $this -> input -> post('package_description');
			$package_id = $this -> input -> post('package_id');
			$result = $this -> changeset -> rename_package_name($session_data['id'], $package_name, $package_id, $package_description);
			print_r($result);
			return $result;
			//$this->output->set_output(json_encode($result));
		}
		else
		{
			$data['sessionExpireStatus']=true;
			$this->load->view('user_authentication',$data);
		}	
	}
	
	//Get the details of package info
	public function getPackageinfo()
	{
		if($this->session->userdata('logged_in'))
	    {
			$session_data = $this->session->userdata('logged_in');
			$package_id = $this->input->post('packageId');
			$data['page_data']['component_list'] = $this->changeset->getComponentList($session_data['siteId'],$package_id);	
			$this->output->set_output(json_encode($data['page_data']['component_list']));
		}   
	}
	
	public function verify_existing()
	{
		if($this->session->userdata('logged_in'))
	    {
			$session_data = $this->session->userdata('logged_in');
			$package_id = $this->input->post('packageId');
			$verify_existing_value = $this->input->post('verify_existing_value');
			$data['page_data']['verify_missing'] = $this->changeset->verify_existing($session_data['siteId'],$package_id, $verify_existing_value);
			$this->output->set_output(json_encode($data['page_data']['verify_missing']));
		}   
	}
	
	public function getchildinfo()
	{
		if($this->session->userdata('logged_in'))
	    {
			$session_data = $this->session->userdata('logged_in');
			$Asset_Id = $this->input->post('Asset_Id');	
			$Asset_Type = $this->input->post('Asset_Type');	
			$Package_Id = $this->input->post('Package_Id');	
			$Package_Item_Id = $this->input->post('Package_Item_Id');	
			$dataOrg1 = $this->eloqua->getAssetChild($Package_Id,$Package_Item_Id,$Asset_Id,$Asset_Type );
			$this->output->set_output(json_encode($dataOrg1));
			
		}
		else
		{
			$data['page_title']= 'Oracle Eloqua Transporter';
			$data['sessionExpireStatus']=true;
			$this->load->view('common/header',$data);
			$this->load->view('user_authentication',$data);
		}	
	}
	
	public function getchildinfo_new()
	{
		
		//$session_data = $this->session->userdata('logged_in');
		$Asset_Id = $this->input->get('Asset_Id');	
		$Asset_Type = $this->input->get('Asset_Type');	
		$Package_Id = $this->input->get('Package_Id');	
		$Package_Item_Id = $this->input->get('Package_Item_Id');	
		$temp['packageId'] = $Package_Id;
		$dataOrg1 = $this->eloqua->getAssetChild($Package_Id,$Package_Item_Id,$Asset_Id,$Asset_Type );
		$this->output->set_output(json_encode($dataOrg1));
			
		
		
	}
	
	public function getAssetReport()
	{
		if($this->session->userdata('logged_in'))
	    {
			
			$Package_Id = $this->input->post('Package_Id');	
			$dataOrg1 = $this->eloqua->getPackageItems($Package_Id);			
			$this->output->set_output(json_encode($dataOrg1));
			
		}
		else
		{
			$data['page_title']= 'Oracle Eloqua Transporter';
			$data['sessionExpireStatus']=true;
			$this->load->view('common/header',$data);
			$this->load->view('user_authentication',$data);
		}	
	}
	
	public function validatepackage()
	{
		if($this->session->userdata('logged_in'))
	    {
			$session_data = $this->session->userdata('logged_in');
			$Package_Id = $this->input->post('PackageId');	
			$data = $this->eloqua->validatepackage($Package_Id );
			$this->output->set_output(json_encode($data));
		}   
	}
	
	public function update_source_site_name()
	{
		if($this->session->userdata('logged_in'))
	    {
			$session_data = $this->session->userdata('logged_in');
			$Package_Id = $this->input->post('packageId');	
			$Source_Site_Name = $this->input->post('Source_Site_Name');	
			$package_description = $this->input->post('package_description');	
			$package_name = $this->input->post('package_name');	
			$data = $this->changeset->update_source_site_name($session_data['siteId'],$Package_Id ,$Source_Site_Name, $package_description, $package_name);
			$this->output->set_output(json_encode($data));
		}   
	}
	
	public function update_target_site_name()
	{
		if($this->session->userdata('logged_in'))
	    {
			$session_data = $this->session->userdata('logged_in');
			$Package_Id = $this->input->post('packageId');	
			$Target_Site_Name = $this->input->post('Target_Site_Name');	
			$data = $this->changeset->update_target_site_name($session_data['siteId'], $Package_Id, $Target_Site_Name );
			$this->output->set_output(json_encode($data));
		}   
	}
	
	public function delete_deployment_package_item()
	{
		if($this->session->userdata('logged_in'))
		{
			$Package_Id = $this->input->post('packageId');	
			$data = $this->changeset->delete_deployment_package_item($Package_Id);
		}
	}
	
	public function getAllLandingPages()
	{
		if($this->session->userdata('logged_in'))
	    {
			$session_data = $this->session->userdata('logged_in');
			$Package_Id = $this->input->post('packageId');	
			//$dataOrg1['LPwithTargetMS'] = $this->changeset->getAllLandingPages($Package_Id);
			//$dataOrg1['defaultTargetMicrosite']= $this->changeset->getDefaultTargetMicrosite($Package_Id);
			$dataOrg1=$this->changeset->getAllLandingPages($Package_Id);
			$this->output->set_output(json_encode($dataOrg1));
		} 
	}
	
	public function Save_MicrositeJSON_LP()
	{
		if($this->session->userdata('logged_in'))
	    {
			$Package_Id = $this->input->post('packageId');
			$JSONSave = $this->input->post('JSONFormed');
			$micrositesList = $this->input->post('micrositesList');
			// print_r($JSONSave);
			// echo '<br>';
			// print_r($micrositesList);
			foreach($JSONSave as $key=>$val)
			{
				foreach($micrositesList as $mkey=>$mval)
				{
					if($mval['id']==$val['MSID'])
					{
						$returnResult=$this->changeset->Save_MicrositeJSON_LP($val['LPname'],$val['LPID'],$mval,$Package_Id);
					}	
				}
			}
			$this->output->set_output($returnResult);
		}
	}
	
	public function targetDuplicatesAssets()
	{
		if($this->session->userdata('logged_in'))
	    {
			$session_data = $this->session->userdata('logged_in');
			$Package_Id = $this->input->post('packageId');	
			$dataOrg1 = $this->changeset->targetDuplicatesAssets($Package_Id);
			$this->output->set_output(json_encode($dataOrg1));
		} 
	}
	
	public function Save_SelectedTargetAssetId()
	{
		if($this->session->userdata('logged_in'))
	    {
			$Package_Id = $this->input->post('packageId');
			$JSONSave = $this->input->post('JSONFormed');
			foreach($JSONSave as $key=>$val)
			{
				$returnResult=$this->changeset->Save_SelectedTargetAssetId($val['AssetId'],$val['AssetType'],$val['AssetName'],$val['TargetAssetId'],$Package_Id,$val['DPI_Id']);
			}
			$this->output->set_output($returnResult);
		}
	}
	
	public function getFolderStructure()
	{
		if($this->session->userdata('logged_in'))
		{
			$package_item_id = $this->input->get('package_item_id');
			$result = $this->deploy_helper_model->getFolderStructure($package_item_id);
			$resultJson = json_encode($result->elements);
			$this->output->set_output($resultJson);
			
		}
	}
	
	public function updateFolderForPackageItem()
	{
		if($this->session->userdata('logged_in'))
		{
			$package_item_id = $this->input->get('package_item_id');
			$folder_id = $this->input->get('folderId');
			$result = $this->deploy_helper_model->updateFolderIdForPackageItem($package_item_id,$folder_id);			
		}
	}
	
	public function testTiming()
	{
		$packageId = $this->input->get('packageId');
		$this->eloqua->getAllChildAssets($packageId);
	}
	
	
	public function statusColor()
	{
		$packageId = $this->input->post('PackageId');
		$packageStatus = $this->changeset->getPackageStatus($packageId);
		if(strcmp($packageStatus[0]->Status  , "Deployment In Queue" ) == 0 )
		{
			$result = $this->changeset->getDeployedAssetCount($packageId);
		}
		else if(strcmp($packageStatus[0]->Status  , "Validate In Queue" ) == 0 )
		{
			$result = $this->changeset->getValidatedAssetCount($packageId);
		}
			if($result <= 20)
			{
				$data['backgroundcolor'] = "#1fc600";
				$data['color'] = "white";
			}
			else if($result <= 40)
			{
				$data['backgroundcolor'] = "#089000";
				$data['color'] = "white";
			}
			else if($result <=50)
			{
				$data['backgroundcolor'] = "#228a16";
			}
			else if($result <=70)
			{
				$data['backgroundcolor'] = "#FFFF99";
				$data['color'] = "black";
			}
			else if($result <=80)
			{
				$data['backgroundcolor'] = "#ffe200";
				$data['color'] = "black";
			}
			else if($result <=100)
			{
				$data['backgroundcolor'] = "#FFFF66";
				$data['color'] = "black";
			}
			else if($result <=120)
			{
				$data['backgroundcolor'] = "#f26161";
				$data['color'] = "white";
			}
			else if($result <=140)
			{
				$data['backgroundcolor'] = "#f03939";
				$data['color'] = "white";
			}
			else 
			{
				$data['backgroundcolor'] = "#FF0000";
				$data['color'] = "white";
			}
			
	   $this->output->set_output(json_encode($data));
	}
	
	public function getTotalValidateAssets()
	{	
	    $packageId = $this->input->post('PackageId');
		
		$packageStatus = $this->changeset->getPackageStatus($packageId);
	//	print_r($packageStatus);
		
		
		if(strcmp($packageStatus[0]->Status  , "New" ) == 0)
		{
			$result = $this->changeset->getIncludedAssetCount($packageId);
		    $data['message'] = '<b>' .$result .'</b> assets included '; 
		    $data['packageStatus'] = $packageStatus;
			
		}
		else if(strcmp($packageStatus[0]->Status  , "Deployment In Queue" ) == 0 || strcmp($packageStatus[0]->Status  , "Errored" ) == 0)
        {
			$totalAsset = $this->changeset->getVariFiedAssetCount($packageId);
			$result = $this->changeset->getDeployedAssetCount($packageId);
		    $data['message'] = '<b>' .$result .'</b> of <b>' .$totalAsset . '</b> assets deployed'; 
		    $data['packageStatus'] = $packageStatus;
			
		}
		else if(strcmp($packageStatus[0]->Status,"Deploy Completed") == 0 || strcmp($packageStatus[0]->Status,"Action Required") == 0)
		{
			$result = $this->changeset->getDeployedAssetCount($packageId);
		    $data['message'] = '<b>' .$result .'</b> assets deployed successfully'; 
		    $data['packageStatus'] = $packageStatus;
			
		}
		else if(strcmp($packageStatus[0]->Status,"Duplicates Found") == 0)
		{
			$result = $this->changeset->getValidatedAssetCount($packageId);
		    $data['message'] = '<b>' .$result .'</b> assets validated'; 
			$data['DuplicateCount'] = $this->changeset->getDuplicateAssetCount($packageId);
		    $data['packageStatus'] = $packageStatus;
		}
		else
	    {
			$result = $this->changeset->getValidatedAssetCount($packageId);
		    $data['message'] = '<b>' .$result .'</b> assets validated'; 
		    $data['packageStatus'] = $packageStatus;
		}
		$newAssetCount = $this->changeset->getNewAssetCount($packageId);
		$data['newAssetCount'] = $newAssetCount;
		$this->output->set_output(json_encode($data));
	} 
	
	public function getTotalValidateCompletedAssets()
	{
		$packageId = $this->input->post('PackageId');
		$data = $this->changeset->getValidatedCompletedAssetCount($packageId);
	}
	
	function saveDefaultMicrosite()
	{
		//$default_targetMicrositeId = $this->input->post('micrositeId');
		$Package_Id = $this->input->post('packageId');
		$DMSID = $this->input->post('DMSID');
		//$micrositesList = $this->input->post('micrositesList');
		$micrositesList = $this->changeset->getAllTargetMicrosites($Package_Id);
		// print_r($Package_Id);
		// print_r($DMSID);
		//print_r($micrositesList);
		$returnResult=null;
		foreach($micrositesList as $mkey=>$mval)
		{
			if($mval['id']==$DMSID)
			{
				$returnResult=$this->changeset->saveDefaultMicrosite($Package_Id,$mval);
			}	
		}
		$this->output->set_output($returnResult);
		//$data=$this->changeset->saveDefaultMicrosite($default_targetMicrositeId);
	}
	
	function defaultMicrositeAssignedStatus()
	{
		$Package_Id = $this->input->post('packageId');
		$defaultMicrosite=$this->changeset->defaultMicrositeAssignedStatus($Package_Id);
		//print_r($defaultMicrosite);
		$this->output->set_output($defaultMicrosite);
	}
	
	
	public function getChildAsset()
	{
	    $Asset_Id = $this->input->get('Asset_Id');	
		$Asset_Type = $this->input->get('Asset_Type');	
		$Source_SiteId = $this->input->get('Source_SiteId');	
		$Target_SiteId = $this->input->get('Target_SiteId');	
		$dataOrg = $this->eloqua->getAssetChild_new($Source_SiteId,$Target_SiteId,$Asset_Id,$Asset_Type );
		$this->output->set_output(json_encode($dataOrg));		
	}
	
}