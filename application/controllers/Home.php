<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct()
     {
          parent::__construct();
          $this->load->helper('url');
          $this->load->database();
		  $this->load->library('session');	
		  $this->load->library('form_validation');
		  $this->load->model('changeset','',TRUE);
		  $this->load->model('eloqua','',TRUE);
		  $this->load->model('settings_model','',TRUE);
		  $this->load->model('user_model','',TRUE);	
		  $this->load->helper('cookie');
		  $this->load->library('Curl');
		  $this->legacy_db = $this->load->database('parent', true);
     }

	public function index()
	{
		
		if($this->session->userdata('logged_in') && (isset($session_data['email'])))
	    {
			$this->form_validation->set_rules('orgID', 'org', 'trim|required');
			$this->form_validation->set_rules('list_name', 'list_name', 'trim|required'); 
			
			$session_data = $this->session->userdata('logged_in');
			$licence_id = $this->user_model->get_licence_id($session_data['email']);
			if($licence_id)
			{			
				$session_data['licence_id'] = $licence_id[0]->License_Id;
				$this->session->set_userdata("logged_in", $session_data);
				//print_r($licence_id[0]->License_Id);exit;
				$data['page_title'] = 'Oracle Eloqua Transporter';
				$data['user_info'] = $this->user_model->get_user_info($session_data['id']);
				
				$this->load->view('common/header',$data);
				$this->load->view('home',$data);
				$this->load->view('common/footer');
			}
			else
			{
				$this->session->set_userdata("logged_in", $session_data);
				//print_r($licence_id[0]->License_Id);exit;
				$data['page_title'] = 'Oracle Eloqua Transporter';
				$data['user_info'] = $this->user_model->get_user_info($session_data['id']);
				
				$this->load->view('common/header',$data);
				$this->load->view('home',$data);
				$this->load->view('common/footer');
				//redirect($this->config->item('parent_url'), 'refresh');
			}
		}
		else
		{
			redirect($this->config->item('parent_url').'login', 'refresh');
		}
	}
	
	public function new_package()
	{
		if($this->session->userdata('logged_in'))
	    {
			$session_data = $this->session->userdata('logged_in');				
			$data['page_title']= 'Oracle Eloqua Transporter';
			$data['user_info']= $this->user_model->get_user_info($session_data['id']);
			$data['page_data']['orglist'] = $this->settings_model->get_OrgList($session_data['licence_id']);
			//print_r($data['page_data']['orglist']); exit;
			$this->form_validation->set_rules('orgID', 'org', 'trim|required');
			$this->form_validation->set_rules('Package_Name', 'Package Name', 'trim|required'); 
			if($this->form_validation->run() == FALSE)
			{				
				$this->load->view('common/header',$data);
				//$this->load->view('common/nav',$data);
				$this->load->view('new_package',$data);
				$this->load->view('common/footer');
			}
			else
			{				
				$tempdata['Package_Name'] = $this->input->post('Package_Name');
				$tempdata['Source_Site_Name'] = $this->input->post('orgID');
				$result = $this->changeset->postChangesetList($session_data['id'],$tempdata);
				if($result['success']==false)
				{
					$data['page_errors'] =$result['msg'];
					// print_r($data['page_errors']);
					$this->load->view('common/header',$data);
					$this->load->view('new_package',$data);
					$this->load->view('common/footer');
				}
				if($result['success']==true)
				{
					redirect('package_info?id='.$result['id'], 'refresh');
				}
			}
	   }
	   else
	   {
		 //If no session, redirect to login page
		 redirect($this->config->item('parent_url').'login', 'refresh');
	   }	   
	}
	function getPackageList()
	{
		if($this->session->userdata('logged_in'))
	    {
			$session_data = $this->session->userdata('logged_in');
			$licence_id = $this->user_model->get_licence_id($session_data['email']);
			$data['page_title']= 'Oracle Eloqua Transporter';
			$data['user_info']= $this->user_model->get_user_info($session_data['id']);			
			
			if($this->input->post('text'))
			{
				$data['page_data']['changesetList'] = $this->changeset->getChangesetList_search($session_data['id'], $this->input->post('text'));
				// print_r(json_encode($data['page_data']['changesetList']));
				// exit;
			}
			else
			{
				$data['page_data']['changesetList'] = $this->changeset->getChangesetList($session_data['id']);	
			}
			// echo json_encode($data['page_data']['changesetList']);
			$this->output->set_output(json_encode($data['page_data']['changesetList']));
		}
	   else
	   {
		 // redirect($this->config->item('parent_url').'login', 'refresh');
	   }
	}
	
	function copy_Deployment_Package()
	{
		$deployment_Package_Id = $this->input->post('Deployment_Package_Id');
		$data['deployment_Package']= $this->changeset->copy_Deployment_Package($deployment_Package_Id);		
		// print_r($data['deployment_Package']);
		return $data['deployment_Package'];
	}
	
	function delete_Deployment_Package()
	{
		$deployment_Package_Id = $this->input->post('Deployment_Package_Id');
		$data['deployment_Package']= $this->changeset->delete_Deployment_Package($deployment_Package_Id);
		// print_r($data['deployment_Package']);
		return $data['deployment_Package'];
	}
	
	function logout()
	{
	   $this->session->unset_userdata('logged_in');
	   session_destroy();
	   redirect('home', 'refresh');
	}
}