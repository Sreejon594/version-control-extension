<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Changesetinfo extends CI_Controller {

	public function __construct()
    {
          parent::__construct();
          $this->load->helper('url');
          $this->load->database();
		  $this->load->library('session');	
		  $this->load->library('form_validation');
		  // $this->load->model('changeset','',TRUE);
		  // $this->load->model('eloqua','',TRUE);
		  // $this->load->model('settings_model','',TRUE);
		  // $this->load->model('user_model','',TRUE);		  
    }
	
	public function index()
	{
		if($this->session->userdata('logged_in'))
	    {
			$session_data = $this->session->userdata('logged_in');			
			$data['page_title']= 'Eloqua Transporter';
			$data['user_info']= $this->user_model->get_user_info($session_data['id']);			
			$this->load->view('common/header',$data);
			$data['page_data']['changeset_id'] = $this->input->get('id');	
			$data['page_data']['changeset_info'] = $this->changeset->getChangesetToOrg($session_data['id'],$this->input->get('id'));
			$data['page_data']['AssetType'] = $this->changeset->getAssetType($data['user_info'][0]->Company_Id);	
			//print_r($data['page_data']['changeset_info']); exit;
			$data['page_data']['component_list'] = $this->changeset->getComponentList($session_data['id'],$this->input->get('id'));	
			$data['page_data']['orglist'] = $this->settings_model->get_OrgList($session_data['id']);
			//print_r($data['page_data']['orglist']); exit;
			$this->load->view('changesetinfo',$data);
			$this->load->view('common/footer');
	   }
		else
		{
			redirect($this->config->item('parent_url').'login', 'refresh');
		}	   
	}
	
	public function delete()
	{
		if($this->session->userdata('logged_in'))
	    {
			$session_data = $this->session->userdata('logged_in');			
			$data['page_title']= 'Eloqua Transporter';
			$data['user_info']= $this->user_model->get_user_info($session_data['id']);			
			$this->load->view('common/header',$data);
			//$this->load->view('common/nav',$data);	
			$result = $this->changeset->deleteChangeset($session_data['id'],$this->input->post('changeset_id'));
			if($result == true)
			{
				redirect('/', 'refresh');
			}
			if($result == false)
			{
				$this->load->view('error',$data);
			}				
			$this->load->view('common/footer');
		}
		else
		{
			redirect($this->config->item('parent_url').'login', 'refresh');
		}	   
	}
	
	public function deleteComponent()
	{
		if($this->session->userdata('logged_in'))
	    {
			$session_data = $this->session->userdata('logged_in');			
			$data['page_title']= 'Eloqua Transporter';
			$data['user_info']= $this->user_model->get_user_info($session_data['id']);			
			$this->load->view('common/header',$data);
			$this->load->view('common/nav',$data);
			$tempdata['changeset_id']=$this->input->post('changeset_id');
			$tempdata['component_id']=$this->input->post('component_id');
			$result = $this->changeset->deleteComponent($session_data['id'],$tempdata);
			if($result == true)
			{
				redirect('/changesetinfo?id='.$tempdata['changeset_id'], 'refresh');
			}
			if($result == false)
			{
				$this->load->view('error',$data);
			}				
			$this->load->view('common/footer');
		}
		else
		{
			//If no session, redirect to login page
			redirect($this->config->item('parent_url').'login', 'refresh');
		}	   
	}
	
	public function getAssetType()
	{
		$session_data = $this->session->userdata('logged_in');
		$result = $this->changeset->getAssetType($session_data['id']);	
		// print_r($result);
	}
	
	public function getAssetData()
	{
		$session_data = $this->session->userdata('logged_in');
		$orgid = $this->input->post('OrgId');
		$meta = $this->changeset->getAssetNameById($this->input->post('Asset_Type_Id'));
		$page = $this->input->post('page');		
		$org= $this->settings_model->get_OrgInfo($session_data['id'], $orgid);
		$dataOrg1 = $this->eloqua->getAsset($org[0] , $meta , $page);	
		echo $dataOrg1;
	}
	
	public function searchAsset()
	{
		$session_data = $this->session->userdata('logged_in');
		$orgid = $this->input->post('OrgId');
		$AssetName = $this->input->post('AssetName');
		$meta = $this->changeset->getAssetNameById($this->input->post('Asset_Type_Id'));
		$page = $this->input->post('page');		
		$org= $this->settings_model->get_OrgInfo($session_data['id'],$orgid);
		$dataOrg1 = $this->eloqua->getSearchAsset($org[0] , $meta , $page,$AssetName ,'search');	
		echo $dataOrg1;
	}
	
}