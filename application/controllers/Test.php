<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Test extends CI_Controller {

	public function __construct()
     {
          parent::__construct();
          $this->load->helper('url');
          $this->load->database();
		  $this->load->library('session');	
		  $this->load->library('form_validation');
		  $this->load->model('changeset','',TRUE);
		  $this->load->model('eloqua','',TRUE);
		  $this->load->model('settings_model','',TRUE);
		  $this->load->model('user_model','',TRUE);	
		  $this->load->helper('cookie');
		 $this->load->library('Curl');   
     }

	public function index()
	{
			// $this->load->view('common/header');
			// $this->load->view('common/nav');
			
			// $data['page_data']['changesetList'] = $this->changeset->getChangesetList($session_data['id']);	
			$this->load->view('jqgrid');
			// $this->load->view('common/footer');
			
	 
	}
	
	public function merge()
	{
		$this->eloqua->merge();
	}
	public function QuickLogs()
	{
		$data = $this->input->post();
		$temp['QuickLogText'] = $data;
		$this->db->insert('QuickLogs',json_encode($temp));
	}
	

}