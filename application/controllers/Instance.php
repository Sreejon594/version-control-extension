<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Instance extends CI_Controller{

	public function __construct()
     {
        parent::__construct();
        // $this->load->helper('url');
        $this->load->database();
		// $this->load->model('User_model','',TRUE);
		// $this->load->model('Register_model','',TRUE);
		// $this->load->model('Catagory_model','',TRUE);
		// $this->load->model('Settings_model','',TRUE);
		$this->load->model('Instance_info_model','',TRUE);
		// $this->load->library('form_validation');
		// $this->load->helper('form');
		// $this->load->library('session');

		$this->legacy_db = $this->load->database('parent', true);

    }


	function enable()
	{
		$data = $this->input->get();
		$result = $this->Instance_info_model->updateOnInstall($data);
		if($result=='success')
		{
			$callBackUrl = $data['CallbackUrl'];
		$info = $callBackUrl.'|'.$data['siteName'].'|'.$data['siteId'].'|'.$data['installId'];
			$client_id = $this->config->item('App_Id');
			$redirect_uri = $this->config->item('redirect_uri');
			redirect($uri = 'https://login.eloqua.com/auth/oauth2/authorize?response_type=code&client_id='.$client_id.'&redirect_uri='.$redirect_uri.'&scope=full&state='.$info , $method = 'auto', $code = 302 );
		}
		else if($result=='NoLicense')
		{
			redirect($uri = 'installFailed' , $method = 'auto', $code = 500 );
		}
		else if($result=='NoSite')
		{
			redirect($uri = 'installFailed' , $method = 'auto', $code = 500 );
		}
	}

	function installFailed()
	{
		$pageData['message'] = 'You do not have a valid license. Please contact your administrator.';
		$this->load->view('warning_with_message',$pageData);
	}

	function uninstall()
	{
		$data = $this->input->get();
		// print_r($data); exit;
		$result = $this->Instance_info_model->updateOnUninstalled($data);
	}


	function contactMe(){
		$val['siteid'] = $_GET['siteId'];
		$val['siteName'] = $_GET['siteName'];
		$val['userName'] = $_GET['userName'];

		$valid = $this->Instance_info_model->trial($val);
		if($valid){
			if($valid[0]->Valid_Till < date('Y-m-d'))
			{

				$this->load->library('email');
				$config['mailtype'] = 'html';
				$this->email->initialize($config);
				$this->email->from('no-reply@portqii.com', 'portQii');
				$this->email->to('info@portqii.com');
				$this->email->subject('Eloqua Asset Naming Assistant - In App Interest');
				$this->email->message($val['userName'].' is showing interest to buy a license for application - Eloqua Asset Naming Assistant');
				$this->email->send();
				$val['showMsgFor'] ='mailSent';
				$this->load->view('trail',$val);
			}

		}

	}

	//This will be used for configuration page
	function viewconfig()
	{
		$getdata = $this->input->get();
		// echo '<pre>';print_r($getdata);
		$sess_array = array(
					'siteId'=>$getdata['siteId'],
					 'siteName'=>$getdata['siteName'],
					 'userId'=>$getdata['UserId'],
					 'userName'=>$getdata['userName']
				   );
		// print_r($sess_array );exit;
		$this->session->set_userdata('logged_in', $sess_array);

		redirect('settings/orginfo');
	}
}
