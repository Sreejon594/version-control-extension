<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Validate extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->database();
		$this->load->library('session');
		$this->load->helper('form');		  
		$this->load->library('form_validation');
		$this->load->model('changeset','',TRUE);
		$this->load->model('validate_model','',TRUE);
		$this->load->model('eloqua','',TRUE);
		$this->load->model('settings_model','',TRUE);
		$this->load->model('user_model','',TRUE);
		$this->load->model('Deploy_model_copy','',TRUE);
		$this->load->helper('download');
		// $this->load->library('zip');
	}
	public function index()
	{
		/*if($this->session->userdata('logged_in'))
	    {
				$session_data = $this->session->userdata('logged_in');			
				$data['page_title']= 'Oracle Eloqua Transporter';
				$data['user_info']= $this->user_model->get_user_info($session_data['id']);			
				$data['left_nav'][1] = array("lable"=>'Home',"href"=>'/',"icon_class"=>'fa fa-fw fa-list-alt',"active"=>true);
				$data['left_nav'][2] = array("lable"=>'Compare',"href"=>'/compare',"icon_class"=>'fa fa-fw fa-list-alt',"active"=>false);
				$this->load->view('common/header',$data);
				//$this->load->view('common/nav',$data);
				$data['page_data']['changeset_id'] = $this->input->post('changeset_id');	
				$data['page_data']['component_list'] = $this->changeset->getComponentList($session_data['id'],$data['page_data']['changeset_id']);	
				$data['page_data']['org1'] = $this->settings_model->get_OrgInfo($session_data['id'],$data['page_data']['changeset_id']);	
				$data['page_data']['meta'] = array();
				foreach($data['page_data']['component_list'] as $key=>$val)
				{	$temporg1 = $data['page_data']['org1'];
					$objId = $val->object_id;
					if($val->meta_type == 'AccountFields')
						$data['page_data']['meta'][] = $this->eloqua->get_AccountFieldsById($temporg1,$objId);
					if($val->meta_type == 'ContactFields')
						$data['page_data']['meta'][] = $this->eloqua->get_ContactFieldsById($temporg1,$objId);
					if($val->meta_type == 'CustomObjects')
						$data['page_data']['meta'][] = $this->eloqua->get_CustomObjectsFieldsById($temporg1,$objId);
					if($val->meta_type == 'SegmentList')
						$data['page_data']['meta'][] = $this->eloqua->get_SegmentListById($temporg1,$objId);
					if($val->meta_type == 'FilterList')
						$data['page_data']['meta'][] = $this->eloqua->get_FilterListById($temporg1,$objId);
					if($val->meta_type == 'EmailList')
						$data['page_data']['meta'][] = $this->eloqua->get_EmailListById($temporg1,$objId);			
					if($val->meta_type == 'FormsList')
						$data['page_data']['meta'][] = $this->eloqua->get_FormsListById($temporg1,$objId);
					if($val->meta_type == 'LandingPagesList')
						$data['page_data']['meta'][] = $this->eloqua->get_LandingPagesListById($temporg1,$objId);
					if($val->meta_type == 'CampaignsList')
						$data['page_data']['meta'][] = $this->eloqua->get_CampaignsListById($temporg1,$objId);
					if($val->meta_type == 'ProgramList')
						$data['page_data']['meta'][] = $this->eloqua->get_ProgramListById($temporg1,$objId);
					if($val->meta_type == 'SharedList')
						$data['page_data']['meta'][] = $this->eloqua->get_SharedListById($temporg1,$objId);
					if($val->meta_type == 'EmailGroups')
						$data['page_data']['meta'][] = $this->eloqua->get_EmailGroupsById($temporg1,$objId);
					if($val->meta_type == 'OptionLists')
						$data['page_data']['meta'][] = $this->eloqua->get_OptionListsById($temporg1,$objId);
					if($val->meta_type == 'EventList')
						$data['page_data']['meta'][] = $this->eloqua->get_EventListById($temporg1,$objId);
					if($val->meta_type == 'ContactViews')
						$data['page_data']['meta'][] = $this->eloqua->get_ContactViewsById($temporg1,$objId);
					if($val->meta_type == 'Account_Views')
						$data['page_data']['meta'][] = $this->eloqua->get_Account_ViewsById($temporg1,$objId);
				}
				$data['page_data']['orglist'] = $this->settings_model->get_OrgList($session_data['id']);
				$this->load->view('deploy',$data);
				$this->load->view('common/footer');
			
	   }
	   else
	   {
		 //If no session, redirect to login page
		 redirect($this->config->item('parent_url').'login', 'refresh');
	   }*/	   
	}

	
	
	
	public function pre_deploy_package_item()
	{
		if($this->session->userdata('logged_in'))
	    {
			$data['deploy_info']= $this->Deploy_model_copy->pre_deploy_package_item('723', 'queue');
		}   
	}
	public function validateinqueue()
	{
		if($this->session->userdata('logged_in'))
	    {
			
			$session_data = $this->session->userdata('logged_in');			
			$data['page_title']= 'Oracle Eloqua Transporter';
			//$data['user_info']= $this->user_model->get_user_info($session_data['id']);
			$data['deploy_info']= $this->validate_model->validate_inqueue($this->input->get('pId'));
			echo $data['deploy_info'];
	   }
	   else
	   {
		 //If no session, redirect to login page
		 redirect($this->config->item('parent_url').'login', 'refresh');
	   }	   
	}
	
	public function validate()
	{
		$data['deploy_info']= $this->validate_model->validate_queue();
		
	}
	public function get_image()
	{
		//to find out source detail
		$this -> db -> select('Base_Url, Token, Site_Name, Token_Expiry_Time, Refresh_Token');
		$this -> db -> from('instance_ inst');				
		$this -> db -> join('deployment_package dp','dp.Source_Site_Name = inst.Site_Name', 'LEFT OUTER');
		$this -> db -> join('deployment_package_item dpi','dpi.Deployment_Package_Id= dp.Deployment_Package_Id', 'LEFT OUTER');
		$this -> db -> where('dpi.Deployment_Package_Id', 755);
		$this -> db -> limit(1);
		
		$query 		= $this -> db -> get();
		$token 		= $query->result()[0]->Token;
		$Base_Url 	= $query->result()[0]->Base_Url;		
		$sourceorg	= $query->result()[0];
		
		
		if(time()+1800 > $query->result()[0]->Token_Expiry_Time)
		{
		   $result2 = $this -> eloqua -> refreshToken($sourceorg);
		   $query->result()[0]->Token_Expiry_Time = $result2['Token']['access_token'];
		}
		
		$id 			= 155;		
		$url 			= $Base_Url.'/API/REST/2.0/assets/image/'.$id;		
		$Authorization 	= 'Authorization: Bearer' .$token;
		$headers 		= array('Authorization: Bearer '.$token);
		
		$ch 			= curl_init(); 
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HTTPGET, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);			
		$httpCode 		= curl_getinfo($ch, CURLINFO_HTTP_CODE);
		$header_size 	= curl_getinfo($ch, CURLINFO_HEADER_SIZE);
		$data 			= curl_exec($ch);			
		$responseInfo 	= curl_getinfo($ch);
		
		$json_data 		= json_decode($data);
		$img_url 		= $Base_Url.$json_data->fullImageUrl;
		
		
		//to find out target detail
		$this -> db -> select('Base_Url, Token, Site_Name, Token_Expiry_Time, Refresh_Token');
		$this -> db -> from('instance_ inst');				
		$this -> db -> join('deployment_package dp','dp.Target_Site_Name = inst.Site_Name', 'LEFT OUTER');
		$this -> db -> join('deployment_package_item dpi','dpi.Deployment_Package_Id= dp.Deployment_Package_Id', 'LEFT OUTER');
		$this -> db -> where('dpi.Deployment_Package_Id', 755);
		$this -> db -> limit(1);
		
		$target_query 		= $this -> db -> get();
		$target_token 		= $target_query->result()[0]->Token;
		$target_Base_Url 	= $target_query->result()[0]->Base_Url;		
		$target_sourceorg	= $target_query->result()[0];
		
		if(time()+1800 > $target_query->result()[0]->Token_Expiry_Time)
		{
			// print_r("coming inside if");
			// exit;
			$result2 = $this -> eloqua -> refreshToken($target_sourceorg);
			$target_query->result()[0]->Token_Expiry_Time = $result2['Token']['access_token'];
		}
		
		$target_url = $target_Base_Url.'/API/REST/2.0/assets/image/content';
		$img_info 	= getimagesize($img_url);
		$img_real 	= realpath($img_url);
		$data		= "--ELOQUA_BOUNDARY\r\n".
						"Content-Disposition: form-data; name=\"Filedata\"; filename=\"".$json_data->name."\"\r\n".
						"Content-Type: ".$img_url['mime']."\r\n".	
						"\r\n".
						file_get_contents($img_url)."\r\n".
						"--ELOQUA_BOUNDARY--";
						
		$headers 		= array('Authorization: Bearer '.$target_token, "Content-Length: ".strlen($data),"Content-Type: multipart/form-data; boundary=ELOQUA_BOUNDARY");
		
		$Authorization = $this->config->item('App_Id').':'.$this->config->item('Client_Secret');
		$ch=curl_init();
		curl_setopt($ch, CURLOPT_URL, $target_url); 
		curl_setopt($ch, CURLOPT_USERPWD, $Authorization);  
		curl_setopt($ch, CURLOPT_POST, true); 
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_HEADER, false);		
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		$result 		= curl_exec($ch);		
		$httpCode 		= curl_getinfo($ch, CURLINFO_HTTP_CODE);
		$responseInfo 	= curl_getinfo($ch);
		$header_size 	= curl_getinfo($ch, CURLINFO_HEADER_SIZE);
		$body 			= substr($result, $header_size); 
		curl_close($ch);
		$result2 		= json_decode($result);
		$image_id		= $result2->id;
		
		$data_update['Target_Asset_Id'] = $image_id;
		$this -> db -> where('Deployment_Package_Id',$Package_Item->Deployment_Package_Id);
		$this -> db -> where('Asset_Id',$Package_Item->Asset_Id);
		$this -> db -> update('deployment_package_item',$data_update);
			
			
		$New_JSON_data['responseInfo'] 	= $responseInfo;
		$New_JSON_data['Output_result'] = $result;
		$New_JSON_data['body']			= $result;
		$New_JSON_data['httpCode'] 		= $httpCode;
		
		return $New_JSON_data;
	}
}