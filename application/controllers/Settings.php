<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require(APPPATH.'/src/Spout/Autoloader/autoload.php');
class Settings extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->database();
		$this->load->library('session');
		$this->load->library('form_validation');		  
		$this->load->model('settings_model','',TRUE);
		$this->load->helper('form');
		$this->load->model('user_model','',TRUE);
		$this->load->model('eloqua','',TRUE);
		$this->legacy_db = $this->load->database('parent', true);  
	}
	
	public function index()
	{
		if($this->session->userdata('logged_in'))
	    {	$session_data = $this->session->userdata('logged_in');
			$data['page_title']= 'Oracle Eloqua Transporter';
			
			$data['user_info']= $this->user_model->get_user_info($session_data['id']);
			
			$this->load->view('common/header',$data);
			$data['page_data']['acountlist'] = $this->settings_model->get_OrgList($session_data['id']);				
			$this->load->view('profile',$data);
			$this->load->view('common/footer');
			
	   }
	   else
	   {
		 //If no session, redirect to login page
		 redirect($this->config->item('parent_url').'login', 'refresh');
	   }
	}
	
	public function saveusers()
	{
		$postedData = $this->input->post();
		$this->settings_model->saveUsers($postedData);
		
		//return 'Hi';
	}
	
	public function dashboard()
	
	{
		if($this->session->userdata('logged_in'))
	    {	$session_data = $this->session->userdata('logged_in');
			echo'hi from dashbaord function of setting';
			$data['page_title']= 'Dashboard';
			$data['user_info']= $this->user_model->get_user_info($session_data['id']);
		
			$this->load->view('common/dashboard_header',$data);
			//$this->load->view('common/nav',$data);
			//$data['page_data']['acountlist'] = $this->settings_model->get_OrgList($session_data['id']);				
			//$data['page_data']['eloqua'] = $this->eloqua->getUserinfo();				
			$this->load->view('dashboard',$data);
			$this->load->view('common/footer');
			
	   }
	   else
	   {
		 //If no session, redirect to login page
		 redirect($this->config->item('parent_url').'login', 'refresh');
	   }
	}
	
	public function editProfile()
	{
		if($this->session->userdata('logged_in'))
	    {	
			$session_data = $this->session->userdata('logged_in');
			$data['page_title']= 'portQii Appcloud transporter-Edit Profile';
			$data['section_title']= 'Edit Profile';
			$this->form_validation->set_rules('first_name', 	'First Name', 	'trim|required');
			$this->form_validation->set_rules('last_name', 		'Last Name', 	'trim|required');
			//echo "hi from controller"; exit;
			
			if($this->form_validation->run() != FALSE)
			{
				$config['upload_path'] 		= dirname($_SERVER["SCRIPT_FILENAME"]).'/uploads/profilepics/'.$session_data["id"].'/';
				$config['upload_url']    	= './uploads/profilepics/'.$session_data["id"].'/';
				$config['allowed_types'] 	= 'gif|jpg|png|jpeg';
				$config['overwrite']       	= TRUE;
				$config['max_size']      	= "1000KB";
				$config['max_height']      	= "768";
				$config['max_width']       	= "1024"; 
				$config['file_name'] 		= 'dp_'.$session_data["id"];
				
				$this->load->library('upload', $config);
				//echo "hey ";
				
				if (!is_dir(dirname($_SERVER["SCRIPT_FILENAME"])."/uploads/profilepics/".$session_data["id"].'/'))
				{
					mkdir(dirname($_SERVER["SCRIPT_FILENAME"])."/uploads/profilepics/".$session_data["id"].'/', 0777, TRUE);
					echo "creating folder";
				}
				
				$result = $this->upload->do_upload('image');
				//print_r($this->upload->display_errors());exit;
				if($result)
				{
					$ext = '.'.pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION);					
					$profileimg = $config['upload_url'].'dp_'.$session_data["id"].$ext;
					// print_r($profileimg);
					// print_r($ext);
					// exit;
					$profileimg = substr( $profileimg , 1);$this->user_model->changeProfilePic($session_data['id'], $profileimg);
				}
				
				
				$data1 = array(
					'first_name'	=> $this->input->post('first_name'),
					'last_name' 	=> $this->input->post('last_name')
				);
				$data['edit_data']= $this->user_model->editProfile($session_data['id'], $data1);
				$data['result']= 'Profile Updated Successfully';
			}
			
			$data['user_info']= $this->user_model->get_user_info($session_data['id']);
			
			$this->load->view('common/header',$data);
			//$data['page_data']['acountlist'] = $this->settings_model->get_OrgList($session_data['id']);				
			$this->load->view('editProfile',$data);
			$this->load->view('common/footer');
		}
		else
		{
			redirect('login', 'refresh');
		}
	}	
	
	public function saveToken()
	{
		$getdata = $this->input->get();
		// print_r($getdata);
		$result = $this->instance_info_model->saveToken($getdata);
		if($result['success'] == true)
		{
			redirect($result['callback'], 'refresh');
		}
		else
		{
			//A view should be displayed for unsuccessful installation.
			//print_r('coming to else');
			print_r($result);
		}		
	}
	
	public function orginfo()
	{
		if($this->session->userdata('logged_in'))
	    {	
			$session_data = $this->session->userdata('logged_in');
			$this->form_validation->set_rules('OrgName', 'Org Name', 'trim|required');
			if($this->form_validation->run() == FALSE)
			{	
				$data['page_title']= 'Oracle Eloqua Transporter';
				$data['page_data']['all_users'] =$this->settings_model->get_all_users($session_data['siteId']);
				$data['page_data']['permitted_users']=$this->settings_model->get_User_Permissions_for_this_site($session_data['siteId']);
				$data['page_data']['siteId'] = $session_data['siteId'];
				$data['page_data']['siteName'] = $session_data['siteName'];
				$this->load->view('common/header',$data);
				$associatedSites = $this->License_model->associatedSitesForSiteName($session_data['siteName']);				
				$data['page_data']['acountlist'] = $this->settings_model->get_OrgList($associatedSites,$session_data['siteId']);
				$data['page_data']['instanceList'] = $associatedSites;
				$data['page_data']['assetMapping'] = $this->settings_model->get_Mapping($associatedSites);	
				$data['page_data']['imageBaseURL'] = $this->settings_model->getImageURL($session_data['siteId']);
				$this->load->view('eloqua_setting',$data);
				$this->load->view('common/footer');
			}
			else
			{	
				$data['OrgName'] = $this->input->post('OrgName');				
				$result = $this->settings_model->new_Org($session_data['id'],$data);
				
				if($result['success'] == true)
				{
					redirect('/settings/orginfo', 'refresh');
				}
				else{
					//////////////print_r($result);
				}
			}			
		}
		else
		{			
			$data['page_title']= 'Oracle Eloqua Transporter';
			$data['sessionExpireStatus']=true;
			$this->load->view('common/header',$data);
			$this->load->view('user_authentication',$data);
		}      
	}
	
	function orgdelete()
	{	
		$session_data = $this->session->userdata('logged_in');
		// print_r($session_data);
		if($this->session->userdata('logged_in'))
	    {
			$this->form_validation->set_rules('orgid', 'Org id', 'trim|required');
			$this->form_validation->set_rules('siteid', 'Site id', 'trim|required');
			$this->form_validation->set_rules('sitename', 'Site name', 'trim|required');
			if($this->form_validation->run() == true)
			{
				$data['id'] = $this->input->post('orgid');
				$data['site_id'] = $this->input->post('siteid');
				$data['site_name'] = $this->input->post('sitename');
				$result = $this->settings_model->delete_Org($session_data['siteId'],$data);
				redirect('/settings/orginfo', 'refresh');
			}
			else
			{
				//Go to private area
				//redirect('home', 'refresh');
			}			
			
	   }
	   else
	   {
		 //If no session, redirect to login page
		 redirect($this->config->item('parent_url').'login', 'refresh');
	   }	   
	}
	
	function manageMeta()
	{
		if($this->session->userdata('logged_in'))
	    {
			$data['page_title']= 'Oracle Eloqua Transporter';
			$session_data = $this->session->userdata('logged_in');
			$data['user_info']= $this->user_model->get_user_info($session_data['id']);
			//$this->form_validation->set_rules('metaPreference[]', 'Meta Preference', 'trim|required');
			if($this->input->post() == NULL)
			{
				$this->load->view('common/header',$data);
				$data['page_data']['PreferenceList'] = $this->changeset->getAssets($data['user_info'][0]->Company_Id);
				$this->load->view('metaPreference',$data);
				$this->load->view('common/footer');
			}
			else
			{
				$PreferenceList = $this->input->post('PreferenceList'); 
				$return = $this->settings_model->update_Preference($data['user_info'][0]->Company_Id,$PreferenceList);
				// print_r($return);
				redirect('/settings/manageMeta', 'refresh');
			}			
		}
		else
		{
		 //If no session, redirect to login page
		redirect($this->config->item('parent_url').'login', 'refresh');
		}	   
	}
	function password()
	{
		if($this->session->userdata('logged_in'))
	    {	
			$session_data = $this->session->userdata('logged_in');
			$data['page_title']= 'portQii Appcloud transporter-Edit Profile';
			$data['section_title']= 'Edit Profile';
			
			$this->form_validation->set_rules('old_password', 'Old Password', 'trim|required');
			$this->form_validation->set_rules('new_password', 'New Password', 'trim|required|matches[confirm_new_password]');
			$this->form_validation->set_rules('confirm_new_password', 'Confirm New Password', 'trim|required|callback_check_database'); 
			if($this->form_validation->run() != FALSE)
			{
				//$data1 = array(
				//	'first_name'	=> $this->input->post('first_name'),
				//	'last_name' 	=> $this->input->post('last_name')
				//);
				//$data['edit_data']= $this->user_model->editProfile($session_data['id'], $data1);
				$data['result']= 'Profile Updated Successfully';
			}
			
			$data['user_info']= $this->user_model->get_user_info($session_data['id']);
			
			$this->load->view('common/header',$data);
			$this->load->view('change_password',$data);
			$this->load->view('common/footer');
		}
		else
		{
			redirect($this->config->item('parent_url').'login', 'refresh');
		} 
	}
	
	function check_database($confirm_new_password)
	{
		$session_data = $this->session->userdata('logged_in');
		$username = $session_data['id'];
		
		$old_password = $this->input->post('old_password');
		$new_password = $this->input->post('new_password');
		//query the database
		
		$result = $this->user_model->change_password($username, $old_password, $new_password, $confirm_new_password);
		if($result)
		{
			$this->form_validation->set_message('check_database', 'Password Changed Successfully');
			return TRUE;
		}
		else
		{
			$this->form_validation->set_message('check_database', 'You Have Entered Wrong Password');
			return false;
		}
	}
	
	public function populateAssets()
	{
		$postedData = $this->input->post();	
		$AssetList['Source'] = $this->settings_model->getAssetListByAssetTypeAndInstanceName($postedData['assetType'],$postedData['sourceSiteName'],"source");
		$AssetList['Target'] = $this->settings_model->getAssetListByAssetTypeAndInstanceName($postedData['assetType'],$postedData['targetSiteName'],"target");
		$this->output->set_output(json_encode($AssetList));
	}
	
	public function saveAssetMapping()
	{
		if($this->session->userdata('logged_in'))
	    {	
			$session_data = $this->session->userdata('logged_in');			
			$assetMappingData = $this->input->post();
			$dbUpdateResult['returnResult'] = 	$this->settings_model->saveAssetMapping($assetMappingData);			
			$associatedSites = $this->License_model->associatedSitesForSiteName($session_data['siteName']);
			$dbUpdateResult['assetMapping'] = $this->settings_model->get_Mapping($associatedSites);
			$this->output->set_output(json_encode($dbUpdateResult));
		}	
	}
	
	public function deleteMapping()
	{
		if($this->session->userdata('logged_in'))
	    {	
			$session_data = $this->session->userdata('logged_in');	
			$assetMappingData = $this->input->post();
			$dbUpdateResult['returnResult'] = 	$this->settings_model->deleteMapping($assetMappingData);
			$associatedSites = $this->License_model->associatedSitesForSiteName($session_data['siteName']);
			$dbUpdateResult['assetMapping'] = $this->settings_model->get_Mapping($associatedSites);
			$this->output->set_output(json_encode($dbUpdateResult));
		}	
	}
	
	function getEloquaUserBySearchText()
	{			
		if($this->session->userdata('logged_in'))
	    {		
			$session_data = $this->session->userdata('logged_in');
			$searchText = $this->input->post();
			$userSearchResult['userDetails']=$this->settings_model->getUserOnSearch($searchText['searchText'],$session_data['siteName']);
			$this->output->set_output(json_encode($userSearchResult));
		}			
	}	
	
	function saveUserDetails()
	{
		if($this->session->userdata('logged_in'))
	    {		
			$session_data = $this->session->userdata('logged_in');
			$postDate = $this->input->post();
			$userSearchResult['returnResult']=$this->settings_model->saveUserDetails($postDate,$session_data['siteId']);
			$userSearchResult['savedUserDetails']=$this->settings_model->get_User_Permissions_for_this_site($session_data['siteId']);
			$this->output->set_output(json_encode($userSearchResult));
		}
	}
	
	public function deleteUser()
	{
		if($this->session->userdata('logged_in'))
	    {	
			$session_data = $this->session->userdata('logged_in');	
			$postData = $this->input->post();
			$userSearchResult['returnResult'] = $this->settings_model->deleteUser($postData);
			$userSearchResult['savedUserDetails']=$this->settings_model->get_User_Permissions_for_this_site($session_data['siteId']);
			$this->output->set_output(json_encode($userSearchResult));
		}	
	}
	
	public function clientActivity()
	{
		$data['page_title']= 'Oracle Eloqua Transporter';
		$this->load->view('clientActivity',$data);
	}
	
	public function getPackageListBySiteName()
	{	
		$postData = $this->input->post();
		$packaegeList=$this->settings_model->getPackageListBySiteName($postData['searchText']);
		$this->output->set_output(json_encode($packaegeList));
	}
	
	function saveImageBaseURL()
	{
		$postData=$this->input->post();
		$result = $this->settings_model->saveImageBaseURL($postData['siteId'],$postData['imgBaseURL']);
		$this->output->set_output(json_encode($result));
	}
	
	function readXlsxFile()
	{
		
		$this->settings_model->readXlsxFile();
	}
	
	function test()
	{
		print_r("gsjgfjs");
	}
}