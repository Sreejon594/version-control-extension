<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transport extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
		$this->load->helper('url');
		$this->load->database();
		$this->load->library('session');	
		$this->load->library('form_validation');
		$this->load->model('changeset','',TRUE);
		$this->load->model('eloqua','',TRUE);
		$this->load->model('settings_model','',TRUE);
		$this->load->model('user_model','',TRUE);	
		$this->load->helper('cookie');
		$this->load->library('Curl');
	}
	
	public function doTransport()
	{
		$getdata = $this->input->get();
	
		// echo '<pre>';print_r($getdata);
		$sess_array = array(
						'appId'=>$getdata['appId'],
						'installId'=>$getdata['installId'],
						'userName'=>$getdata['userName'],
						'siteName'=>$getdata['siteName'],
						'siteId'=>$getdata['siteId'],
						'userId'=>$getdata['UserId'],
						'UserCulture'=>$getdata['UserCulture'],
						'oauth_consumer_key'=>$getdata['oauth_consumer_key'],
						'oauth_nonce'=>$getdata['oauth_nonce'],
						'oauth_signature_method'=>$getdata['oauth_signature_method'],
						'oauth_timestamp'=>$getdata['oauth_timestamp'],
						'oauth_version'=>$getdata['oauth_version'],
						'oauth_signature'=>$getdata['oauth_signature']
					);
		$this->session->set_userdata('logged_in', $sess_array);
		if($this->session->userdata('logged_in'))
		{	
			$session_data = $this->session->userdata('logged_in');
			
			$oAuthsignFlag = $this->oAuthSigning($session_data);
			//print_r($oAuthsignFlag);
			//exit;
			
			$permittedUser = $this->settings_model->userPermission($session_data['userId'],$session_data['siteId']);
			if($permittedUser)
			{	
				if($session_data['siteId'])
				{	
					$this->session->set_userdata("logged_in", $session_data);
					$data['page_title'] = 'Oracle Eloqua Transporter';
					$this->load->view('common/header',$data);
					$this->load->view('home',$data);
					// $this->load->view('common/header1',$data);
					// $this->load->view('homedependency',$data);
					$this->load->view('common/footer');
				}
				else
				{	
					$this->session->set_userdata("logged_in", $session_data);
					$data['page_title'] = 'Oracle Eloqua Transporter';
					$this->load->view('common/header',$data);
					$this->load->view('home',$data);
					// $this->load->view('common/header1',$data);
					// $this->load->view('homedependency',$data);
					$this->load->view('common/footer');
				}
			}
			else
			{	
				$data['page_title'] = 'Oracle Eloqua Transporter';
				$data['sessionExpireStatus']=false;
				$data['userPermissionStatus']=false;
				$this->load->view('common/header',$data);
				$this->load->view('user_authentication');
				$this->load->view('common/footer');
			}	
		}
		else
		{	
			$data['page_title']= 'Oracle Eloqua Transporter';
			$data['sessionExpireStatus']=true;
			$this->load->view('common/header',$data);
			$this->load->view('user_authentication',$data);
		}
	}

	public function new_package()
	{
		if($this->session->userdata('logged_in'))
	    {
			$session_data = $this->session->userdata('logged_in');				
			$data['page_title']= 'Oracle Eloqua Transporter';
			$data['user_info']= $this->user_model->get_user_info($session_data['id']);
			$data['page_data']['orglist'] = $this->settings_model->get_OrgList($session_data['licence_id']);
			$this->form_validation->set_rules('orgID', 'org', 'trim|required');
			$this->form_validation->set_rules('Package_Name', 'Package Name', 'trim|required'); 
			if($this->form_validation->run() == FALSE)
			{				
				$this->load->view('common/header',$data);
				$this->load->view('new_package',$data);
				$this->load->view('common/footer');
			}
			else
			{				
				$tempdata['Package_Name'] = $this->input->post('Package_Name');
				$tempdata['Source_Site_Name'] = $this->input->post('orgID');
				$result = $this->changeset->postChangesetList($session_data['id'],$tempdata);
				if($result['success']==false)
				{
					$data['page_errors'] =$result['msg'];
					$this->load->view('common/header',$data);
					$this->load->view('new_package',$data);
					$this->load->view('common/footer');
				}
				if($result['success']==true)
				{
					redirect('package_info?id='.$result['id'], 'refresh');
				}
			}
		}
	   	else
		{
			$data['sessionExpireStatus']=true;
			$this->load->view('user_authentication',$data);
		}   
	}
	function getPackageList()
	{
		header("Access-Control-Allow-Origin: *");

		if($this->session->userdata('logged_in'))
	    {
			$session_data = $this->session->userdata('logged_in');
			$data['page_title']= 'Oracle Eloqua Transporter';			
			if($this->input->post('text'))
			{
				$data['page_data']['changesetList'] = $this->changeset->getChangesetList_search($session_data["siteId"], $this->input->post('text'),$session_data["siteName"]);
			}
			else
			{
				$data['page_data']['changesetList'] = $this->changeset->getChangesetList($session_data["siteId"],$session_data["siteName"]);	
			}
			$this->output->set_output(json_encode($data['page_data']['changesetList']));
		}
		else
		{
			$data['sessionExpireStatus']=true;
			$this->load->view('common/header',$data);
			$this->load->view('user_authentication',$data);
		}
	}
	
	function copy_Deployment_Package()
	{
		if($this->session->userdata('logged_in'))
	    {
			$session_data = $this->session->userdata('logged_in');
			$deployment_Package_Id = $this->input->post('Deployment_Package_Id');
			$data['deployment_Package']= $this->changeset->copy_Deployment_Package($deployment_Package_Id,$session_data['userName']);		
			return $data['deployment_Package'];		
		}
		else
		{
			$data['sessionExpireStatus']=true;
			$this->load->view('common/header',$data);
			$this->load->view('user_authentication',$data);
		}	
	}
	
	function delete_Deployment_Package()
	{
		if($this->session->userdata('logged_in'))
	    {
			$session_data = $this->session->userdata('logged_in');
			$deployment_Package_Id = $this->input->post('Deployment_Package_Id');
			$data['deployment_Package']= $this->changeset->delete_Deployment_Package($deployment_Package_Id);
			return $data['deployment_Package'];
		}
		else
		{
			$data['sessionExpireStatus']=true;
			$this->load->view('common/header',$data);
			$this->load->view('user_authentication',$data);
		}		
	}
	
	function oAuthSigning($sessionData)
	{		
		if(isset($sessionData))
		{
			$queryParamDecoded = null;
			
			if(isset($sessionData['appId']))
			{
				$queryParamDecoded = "appId=".$sessionData['appId'];
			}
			if(isset($sessionData['installId']))
			{
				$queryParamDecoded = $queryParamDecoded . "&installId=".$sessionData['installId'];
			}
			if(isset($sessionData['oauth_consumer_key']))
			{
				$queryParamDecoded = $queryParamDecoded . "&oauth_consumer_key=".$sessionData['oauth_consumer_key'];
			}	
			if(isset($sessionData['oauth_nonce']))
			{
				$queryParamDecoded = $queryParamDecoded."&oauth_nonce=".$sessionData['oauth_nonce'];
			}	
			if(isset($sessionData['oauth_signature_method']))
			{
				$queryParamDecoded = $queryParamDecoded."&oauth_signature_method=".$sessionData['oauth_signature_method'];
			}	
			if(isset($sessionData['oauth_timestamp']))
			{
				$queryParamDecoded = $queryParamDecoded."&oauth_timestamp=".$sessionData['oauth_timestamp'];
			}
			if(isset($sessionData['oauth_version']))
			{
				$queryParamDecoded = $queryParamDecoded."&oauth_version=".$sessionData['oauth_version'];
			}	
			if(isset($sessionData['siteId']))
			{
				$queryParamDecoded = $queryParamDecoded."&siteId=".$sessionData['siteId'];
			}
			if(isset($sessionData['siteName']))
			{
				$queryParamDecoded = $queryParamDecoded."&siteName=".$sessionData['siteName'];
			}				
			if(isset($sessionData['UserCulture']))
			{
				$queryParamDecoded = $queryParamDecoded."&UserCulture=".$sessionData['UserCulture'];
			}
			if(isset($sessionData['userId']))
			{
				$queryParamDecoded = $queryParamDecoded."&UserId=".$sessionData['userId'];
			}
			if(isset($sessionData['userName']))
			{
				$queryParamDecoded = $queryParamDecoded."&userName=".$sessionData['userName'];
			}			
			
			$urlString = "https://appcloud-dev.portqii.com/transporter2.0/transport/doTransport";
			
			$clientId = $this->config->item('App_Id');
			$hash_key =$this->config->item('Client_Secret')."&";
			$hash_message ="GET&". urlencode(utf8_encode($urlString))."&" .urlencode(utf8_encode($queryParamDecoded));
			
			if((string)$sessionData['oauth_consumer_key']==(string)$clientId)
			{	
				$currentTime = time();
				$timeGap = $currentTime - $sessionData['oauth_timestamp'];
				//echo $timeGap;
				if($timeGap <=300)
				{					
					$signatureCalculated = $this->hmacSha1($hash_message,$hash_key);
					if((string)$signatureCalculated == (string)$sessionData['oauth_signature'])
					{
						return true;
					}  
					else
					{
						return false;
					}
				}	
			}	
			else
			{
				return false;
			}						
		}	
	}
	
	function hmacSha1($value,$key)
	{
		$hmac = hash_hmac("sha1", $value, $key, TRUE);		
		$signature = base64_encode($hmac);
		return $signature;
	}
}