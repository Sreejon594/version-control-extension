function eventList()
	{
		$('.ViewChild').click(function (){
			$('.childInfo').show();
			var Type = $(this).parent().find('input[name="Asset_Type"]').val();
			var Id = $(this).parent().find('input[name="Asset_Id"]').val();
			var package_id = $(this).parent().find('input[name="changeset_id"]').val();		
			var package_item_id = $(this).parent().find('input[name="component_id"]').val();
			var datavalue = {Package_Id:package_id,Package_Item_Id:package_item_id, Asset_Id:Id , Asset_Type:Type};
			getChildInfo(datavalue);		
		});
		$('.viewInvalidAsset').click(function (){
			$('.invalidAssets').show();
			var package_id = $(this).parent().find('input[name="changeset_id"]').val();
			var package_item_id = $(this).parent().find('input[name="component_id"]').val();	
			var datavalue = {Package_Id:package_id,Package_Item_Id:package_item_id};
			getInvalidAsset(datavalue);		
		});
		// $('.viewErrorMessage').click(function (){
			// $('.errorMessage').show();
			// var package_id = $(this).parent().find('input[name="changeset_id"]').val();
			// var package_item_id = $(this).parent().find('input[name="component_id"]').val();
			// var datavalue = {Package_Id:package_id,Package_Item_Id:package_item_id};
			// getErrorMeassge(datavalue);		
		// });
		
	}
	
	function deploymentInQueue()
	{
		var package_id = $('.package_id').val();
		//alert(package_id);
		$.ajax({			
			type: 'post',
			url: '<?php echo base_url();?>deploy/deploymentinqueue?pId='+package_id,
			//data: {},
			beforeSend: function()
			{
				$('.DeployPackage span').remove();
				$('.DeployPackage').append(' <span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span>');
				$('.DeployPackage').attr("disabled",'true');
				$('.DeployPackage').off('click');
			},
			success: function(data)
			{
				$('.deploy_package').hide();
				$('.deploy_package').find('.modal-body').text('You want to deploy it again');
				$('.DeployPackage span').remove();
				$('.footer_deploy_button').after('<p style="margin-bottom: 2px;">This Package Deployment is in queue.</p>');
				// getPackageinfo();
				
			},
			error: function() 
			{
				$('.DeployPackage span').remove();
			}			
			
		});
	}
	
	$(document).ready(function()
	{
		var package_id = $('.package_id').val();
		var package_description = $('#package_description').val();
		var package_name = $('#package_value').val();
		var selectSys = $('.selectSys :selected').val();
		$('.selectSys').parent().find('.OrgId').val(selectSys);
		$(".Asset_Type").focus(function(){
			$(this).css("border-color", "#ddd");
		});
		
		$('.DeployPackage').click(function (){
			$('.deploy_package').show();
		});
		
		$('.DeployPackage_ok').click(function (){
			$(this).parent().parent().find('.modal-body').text('');
			$(this).parent().parent().find('.modal-body').append(' <span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span>');
			deploymentInQueue();
		});
		
		$('.close').click(function(){
			$(this).parent().parent().parent().parent().hide();
			$(this).parent().parent().parent().find('.asset_list').html('').removeClass('in').attr('aria-expanded','false');
			$(this).parent().parent().parent().find('.asset_type span').addClass('fa-plus-square-o').removeClass('fa-minus-square-o');
		});
		
		
		var source_value = $('.selectSys :selected').val();
		var destination_value = $('.selectSys2 :selected').val();
		$('.source_value').val(source_value);
		$('.destination_value').val(destination_value);
		
		$(".Asset_Type").blur(function(){
			var Asset_Type = $('.Asset_Type :selected').val();
			if(Asset_Type == "Asset Type")
			{
				$(this).css("border-color", "red");
			}
			else
			{
				$(this).css("border-color", "#ddd");
			}
		});
		
		$(".Target_Asset_Type").blur(function(){
			var Target_Asset_Type = $('.Target_Asset_Type :selected').val();
			if(Target_Asset_Type == "Asset Type")
			{
				$(this).css("border-color", "red");
			}
			else
			{
				$(this).css("border-color", "#ddd");
			}
		});
		
		$('.Asset_Type').on('change',function(){
			$(this).parent().parent().parent().find('.asset_list').hide();
		    $(this).parent().parent().parent().find('#source_asset_type'+$(this).val().replace(" ","_")).show();    
			$(this).parent().parent().parent().find('#asset_type_'+$(this).val().replace(" ","_")).show();    
			$(this).parent().parent().parent().find('.asset_type').hide();
			$(this).parent().parent().parent().find('#source_asset_type'+$(this).val().replace(" ","_")).parent().find('.asset_type').show();
			$(this).parent().parent().parent().find('#asset_type_'+$(this).val().replace(" ","_")).parent().find('.asset_type').show();
        });

		$('.reset_asset').click(function(){
			$(this).parent().parent().find('.Asset_Type option').removeAttr('selected , disabled');
			$(this).parent().parent().find('.Asset_Type option:first').attr('selected','selected');
			$(this).parent().parent().find('.Asset_Type option:first').attr('disabled', true);
			$(this).parent().parent().find('.Asset_Name').val('');
			$(this).parent().parent().find('.Asset_Type').css("border-color", "#ddd");
			$(this).parent().parent().parent().find('.asset_list').html('').removeClass('in').attr('aria-expanded','false');
			$(this).parent().parent().parent().find('.asset_type span').addClass('fa-plus-square-o').removeClass('fa-minus-square-o');
			$(this).parent().parent().parent().find('.asset_type').attr('aria-expanded','false');
			$(this).parent().parent().parent().find('.asset_type').show();
		});
	
		if(package_name == "")
		{
			$('.edit').hide();
			$('.editNew').hide();
			$('.mix_source').hide();
			// $('.selectSys , .selectSys *').attr("readonly", true);
			// $('.selectSys2 , .selectSys2 *').attr("readonly", true);
			
			$('.selectSys , .selectSys *').attr("disabled", true);
			$('.selectSys2 , .selectSys2 *').attr("disabled", true);
			
			$('.Asset_Type , .Asset_Type *').attr("disabled", true);
			$('.viewAssetTree').attr("disabled", false); 
			$('.Asset_Name').attr("disabled", true); 
			$('.searchAsset').attr("disabled", true);
			$('.reset_asset').attr("disabled", true);
			
		}
		
		if(package_id == "")
		{
			$('.edit').hide();
			$('#package_name_id').show();
			$('#package_description').show();
			$('.mix_source').hide();
			$(".footer_deploy_button").hide();
			
		}
		
		if(package_description == ""){
			$('#package_description').show();
		}
		else{
			$('#package_description_label').show();
			$('.editNew').show();
		}
		
		//checking source value null or not
		var selectSys = $('.selectSys :selected').val();		
		if(selectSys == "" || selectSys == 'NULL')
		{
			$('.mix_source').hide();
		}
		else{
			$('.mix_source').show();
		}
		
		$('.selectSys').change(function()
		{
			var selectSys = $('.selectSys :selected').val();
			$('.source_value').val(selectSys);
			if(selectSys == "" || selectSys == 'NULL')
			{
				$('.mix_source').hide();
				$(this).parent().find('.Asset_Type , .Asset_Type *').attr("disabled", true);
				$(this).parent().find('.Asset_Name').attr("disabled", true); 
				$(this).parent().find('.searchAsset').attr("disabled", true);
				$(this).parent().find('.reset_asset').attr("disabled", true);
				getPackageinfo();
			}
			else
			{
				$('.mix_source').show();
				$('.selectSys2 , .selectSys2 *').removeAttr("readonly");
				$('.selectSys2 , .selectSys2 *').removeAttr("disabled");
				$(this).parent().find('.Asset_Type , .Asset_Type *').removeAttr("disabled");
				$(this).parent().find('.Asset_Name').removeAttr("disabled"); 
				$(this).parent().find('.searchAsset').removeAttr("disabled");
				$(this).parent().find('.reset_asset').removeAttr("disabled");					
				var name = $('.selectSys :selected').text();
				$('#systemName').html(name);
				updatesource(name);
				getPackageinfo();
				// $(this).parent().parent().find('.source_value').val(name);
			}
		});
		
		//checking target value null or not
		var selectSys2 = $('.selectSys2 :selected').val();		
		if(selectSys2 == "" || selectSys2 == 'NULL')
		{
			$('.destination').hide();					
		}
		else
		{
			$('.destination').show();
		}
		
		$('.selectSys2').change(function()
		{	
			var name = $('.selectSys2 :selected').val();
			$('.destination_value').val(name);
			$('#systemName').html(name);
			
			if(name == "" || name == 'NULL')
			{
				$('.destination').hide();
				$('.searchButton').show();
				$(this).parent().find('.Asset_Type , .Asset_Type *').attr("disabled" ,true);
				$(this).parent().find('.Asset_Name').attr("disabled" ,true); 
				$(this).parent().find('.searchAsset').attr("disabled" ,true);
				$(this).parent().find('.reset_asset').attr("disabled" ,true);				
			}
			else
			{				
				$('.destination').show();
				$(this).parent().find('.Asset_Type , .Asset_Type *').removeAttr("disabled");
				$(this).parent().find('.Asset_Name').removeAttr("disabled"); 
				$(this).parent().find('.searchAsset').removeAttr("disabled");
				$(this).parent().find('.reset_asset').removeAttr("disabled");
			}
			
			var package_id = $('.package_id').val();
			var targetName = $('.selectSys2 :selected').text();
			$.ajax(
			{
				type: 'post',
				url: 'Package_info/update_target_site_name',
				data: {packageId: package_id, Target_Site_Name: targetName},
				dataType:'json',
				beforeSend: function()
				{				
				},
				success: function(data)
				{
					
					
				},
				error: function() 
				{
				
				}
			}); 
		});
		
		$('.search_popup .close').click(function(){
			$(this).parent().parent().parent().parent().hide();
		});
		
		$('.deploy_package #close1').click(function(){
			$(this).parent().parent().parent().parent().hide();
		});
	});



	<!-- JAVASCRIPT FILES -->
	<script type="text/javascript">var plugin_path = 'assets/plugins/';</script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/jquery/jquery-2.1.4.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/app.js"></script>
	<script type="text/javascript">
		loadScript(plugin_path + "nestable/jquery.nestable.js", function(){
			if(jQuery().nestable) {
				var updateOutput = function (e) {
					var list = e.length ? e : $(e.target),
						output = list.data('output');
					if (window.JSON) {
						output.val(window.JSON.stringify(list.nestable('serialize'))); //, null, 2));
					} else {
						output.val('JSON browser support required for this demo.');
					}
				};

				// Nestable list 1
				jQuery('#nestable_list_1').nestable({
					group: 1
				}).on('change', updateOutput);
				// Nestable list 1
				jQuery('#nestable_list_2').nestable({
					group: 1
				}).on('change', updateOutput);


				// output initial serialised data
				updateOutput(jQuery('#nestable_list_1').data('output', jQuery('#nestable_list_1_output')));
				updateOutput(jQuery('#nestable_list_2').data('output', jQuery('#nestable_list_2_output')));
				
				// Expand All
				jQuery("button[data-action=expand-all]").bind("click", function() {
					jQuery('.dd').nestable('expandAll');
				});

				// Collapse All
				jQuery("button[data-action=collapse-all]").bind("click", function() {
					jQuery('.dd').nestable('collapseAll');
				});
			}
		});
		
		
		
	function addtopackage(package_id,Asset_Type,Asset_Id,Asset_Name)
	{	
		var tempdata= {
				Deployment_Package_Id: package_id,
				Asset_Type: Asset_Type,
				Asset_Id: Asset_Id,
				Asset_Name: Asset_Name,
		};
		var destination = $(".selectSys2 :selected").text();
		if(destination != "" && destination != "NULL")
		{
			$.ajax(
			{
				type: 'post',
				url: '<?php echo base_url();?>package_info/addtopackage',
				data: tempdata,
				beforeSend: function()
				{
					
				},
				success: function(data)
				{   
					getPackageinfo();
					// $('#orgID1').prop('readonly', true);
					// $('#orgID2').prop('readonly', true);
					$('#orgID1').prop('disabled', true);
					$('#orgID2').prop('disabled', true);
				},
				error: function()
				{
					result = false;
				}
			});
		}
		else
		{
			alert("Please select Target System first");
			var val = Asset_Id;
			$('input:checkbox[value="' + val + '"]').attr('checked', false);
		}
	}
	
	$(document).ready(function() { 
		var flag = true;
		function getAssetList(Asset,assetlist,tempdata)
		{
			
			if(flag == true)
			{
				$.ajax(
				{
					type: 'post',
					url: '<?php echo base_url();?>package_info/getAssetData',
					data: tempdata,
					beforeSend: function()
					{
						$(Asset).append('<div class="loader">'+
							'<span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span>'+
						'</div></a>');
						//$('.searchAsset').html('<span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span>');
						flag = false;
					},
					success: function(data)
					{
						$(Asset).find('.loader').hide();
						//$('.search_popup').hide();
						$('.searchAsset').html('<span class="fa fa-search"> </span>');
						var list = JSON.parse( data );
						$(list.elements).each(function(){
							var out = printObject(this);
							$(assetlist).append( '<div class="list-group-item "><input class="additem"  type="checkbox" value="'+this.id+'"/><a href="#id_'+tempdata.OrgId+'_'+ this.id +'" class="asset_item" data-toggle="collapse" >'+this.name +'</a></div>' );
							$('.destination').find('.additem').remove();
							// $(assetlist).append( '<div id="id_'+tempdata.OrgId+'_'+ this.id +'" class="list-group-item asset_struct collapse">'+out+'</div>' );
							
						});	
						
						if(list.page * list.pageSize < list.total ){
							var pageno = list.page +1;
							$(assetlist).append( '<div class="load_more_asset"><input class="page" type="hidden" value="'+pageno+'" /> <a class="btn btn-xs btn-info">Load More ('+list.page * list.pageSize +'/'+list.total+') </a></div>' );
						}					
						$('.load_more_asset').click(function(){
						var tempdata = {
									"Asset_Type_Id":$(this).parent().parent().find('input[name="RSys_Asset_Type_Id"]').val(),
									"OrgId": $(this).parent().parent().parent().find('.OrgId_hidden').val(),
									"page": $(this).find('.page').val(),
								};
						var assetlist = $(this).parent().parent().find('.asset_list');
							
							getAssetList(this,assetlist,tempdata);
							$(this).remove();
						});
						disabled_checkbox();
						flag = true;
						additem();
					},
					error: function()
					{
						result = false;
						flag =true;
					}
				});
			}
		}
		
		$('.asset_type').click(function(){
			if($(this).attr('aria-expanded') == "false"){
				$(this).find(".fa-plus-square-o").addClass("fa-minus-square-o").removeClass("fa-plus-square-o");
			}else{
				$(this).find(".fa-minus-square-o").addClass("fa-plus-square-o").removeClass("fa-minus-square-o");
			}
			var tempdata = {
						"Asset_Type_Id":$(this).parent().find('input[name="RSys_Asset_Type_Id"]').val(),
						"OrgId": $(this).parent().parent().find('.OrgId_hidden').val(),
					};
			var assetlist = $(this).parent().find('.asset_list');
			if($.trim($(this).parent().find('.asset_list').html()) =='')
				getAssetList(this,assetlist,tempdata);
				disabled_checkbox();
			
		});
		
		function getSearchAsset(Asset,assetlist,tempdata)
		{
			$.ajax(
			{
				type: 'post',
				url: '<?php echo base_url();?>package_info/searchAsset',
				data: tempdata,
				beforeSend: function()
				{
					$(Asset).html('<span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span>');
				},
				success: function(data)
				{  
					$(assetlist).empty();
					$(Asset).html('<span class="fa fa-search"></span>');
					$(Asset).find('.loader').hide();
					$('.search_popup').hide();
					var list = JSON.parse( data );
					$(list.elements).each(function(){
						var out = printObject(this);
					$(assetlist).append( '<div class="list-group-item "><input class="additem" type="checkbox" value="'+this.id+'"/><a href="#id_'+tempdata.OrgId+'_'+ this.id +'" class="asset_item" data-toggle="collapse" >'+this.name +'</a></div>' );
					$('.destination').find('.additem').remove();
				// $(assetlist).append( '<div id="id_'+tempdata.OrgId+'_'+ this.id +'" class="list-group-item asset_struct collapse">'+out+'</div>' );
					});	
					if($('.asset_type').hasClass('collapsed'))
					{
						$('.asset_type').removeClass('collapsed');
						$(this).attr('aria-expanded') = "true";
						//$('.asset_list').addClass('in');
						$('.asset_list').attr('aria-expanded') = "true";
					}
					
					if(list.page * list.pageSize < list.total ){
						var pageno = list.page +1;
						$(assetlist).append( '<div class="load_more_asset"><input class="page" type="hidden" value="'+pageno+'" /> <a class="btn btn-xs btn-info">Load More ('+list.page * list.pageSize +'/'+list.total+') </a></div>' );
					}
					if(list.total<=0 ){
						$(assetlist).append( '<div class="no_asset">No asset found.</div>' );
					}					
					$('.load_more_asset').click(function(){
					var tempdata = {
								"Asset_Type_Id":$(this).parent().parent().find('input[name="Asset_Type_Id"]').val(),
								"OrgId": $(this).parent().parent().parent().parent().parent().parent().parent().find('.source_value').val(),
								"page": $(this).find('.page').val(),
							};
					var assetlist = $(this).parent().parent().find('.asset_list');						
						getAssetList(this,assetlist,tempdata);						
						$(this).remove();
					});
					disabled_checkbox();
					additem();
				},
				error: function()
				{
					result = false;
				}
			});
		}
		
		$('.searchAsset').click(function()
		{
			var Asset_Type = $(this).parent().parent().find('.Asset_Type :selected').val();
			// checking for asset type is empty or not
			if(Asset_Type == "Asset Type")
			{
				$(this).parent().parent().find('.Asset_Type').focus();
				$(this).parent().parent().find('.Asset_Type').css("border-color","red");
				return false;
			}
			var tempdata = {
				"Asset_Type_Id" : $(this).parent().parent().find('select[name="Asset_Type_Id"]').val(),
				"OrgId" : $(this).parent().parent().parent().find('.pack_value').val(),
				"AssetName" : $(this).parent().parent().find('input[name="AssetName"]').val(),
				"page" : 1,
			};
			var AssetId=$(this).parent().parent().find('select[name="Asset_Type_Id"]').val();
			// var OrgId=$(this).parent().parent().parent().find('.pack_value').val();		
			if($(this).parent().parent().parent().find('.list_type').val() == 'source' ){
				$('#source_asset_type'+AssetId).parent().find('.asset_list').html('');		
				var assetlist = $('#source_asset_type'+AssetId.replace(" ", "_")).parent().find('.asset_list');
			}
			else{
				$('#asset_type_2_'+AssetId).parent().find('.asset_list').html('');		
				var assetlist = $('#asset_type_'+AssetId.replace(" ", "_")).parent().find('.asset_list');
			}
			if($.trim($(this).parent().find('.asset_list').html()) =='')
				getSearchAsset(this,assetlist,tempdata);
				disabled_checkbox();
			
		});		
		
		$('.Asset_Name').keyup(function(e){
			if(e.keyCode == 13)
			{
				var Asset_Type = $(this).parent().parent().find('.Asset_Type :selected').val();
				if(Asset_Type == "Asset Type")
				{
					$(this).parent().parent().find('.Asset_Type').focus();
					$(this).parent().parent().find('.Asset_Type').css("border-color","red");
					return false;
				}
				var tempdata = {
					"Asset_Type_Id" : $(this).parent().parent().find('select[name="Asset_Type_Id"]').val(),
					"OrgId" : $(this).parent().parent().parent().find('.pack_value').val(),
					"AssetName" : $(this).parent().parent().find('input[name="AssetName"]').val(),
					"page" : 1,
				};
				var AssetId=$(this).parent().parent().find('select[name="Asset_Type_Id"]').val();
				// var OrgId=$(this).parent().parent().find('.source_value').val();		
				
				if($(this).parent().parent().parent().find('.list_type').val() == 'source' ){
					$('#source_asset_type'+AssetId).parent().find('.asset_list').html('');		
					var assetlist = $('#source_asset_type'+AssetId.replace(" ", "_")).parent().find('.asset_list');
				}
				else{
					$('#asset_type_2_'+AssetId).parent().find('.asset_list').html('');		
					var assetlist = $('#asset_type_'+AssetId.replace(" ", "_")).parent().find('.asset_list');
				}
				if($.trim($(this).parent().find('.asset_list').html()) =='')
					var search_button = $(this).parent().parent().find('.searchAsset');
					getSearchAsset(search_button, assetlist, tempdata);
				
			}
		});

		$(".package_name").on('blur keyup',function(e) {
			if (e.type == 'blur' || e.keyCode == '13'){
				var package_id = $(".package_id").val();
				var package_name = $('.package_value').val();
				var package_description = $('.package_description').val();
				if(package_name == "" )
				{
					$('.edit').hide();
					$('.editNew').hide();
					$('.mix_source').hide();
					// $('.selectSys , .selectSys *').attr("readonly", true);
					// $('.selectSys2 , .selectSys2 *').attr("readonly", true);
					$('.selectSys , .selectSys *').attr("disabled", true);
					$('.selectSys2 , .selectSys2 *').attr("disabled", true);
					$('.Asset_Type , .Asset_Type *').attr("disabled", true);
					$('.Asset_Name').attr("disabled", true); 
					$('.searchAsset').attr("disabled", true);
					$('.reset_asset').attr("disabled", true);
				}
				else
				{					
					$('.selectSys , .selectSys *').removeAttr("readonly");
					$('.selectSys , .selectSys *').removeAttr("disabled");
					//$('.selectSys2 , .selectSys2 *').removeAttr("disabled");
					//$('.Asset_Type , .Asset_Type *').removeAttr("disabled");
					//$('.Asset_Name').removeAttr("disabled"); 
					//$('.searchAsset').removeAttr("disabled");
					//$('.reset_asset').removeAttr("disabled");					
				}
				if(package_id != "" && package_name != "" )
				{
					$.ajax(
					{
						type: 'post',
						url: 'package_info/rename_package_name',
						//data: { 'package_name' : package_name, "package_id" : package_id},
						data: { 'package_name' : package_name, "package_id" : package_id, "package_description": package_description},
						beforeSend: function()
						{					
						},
						success: function(data)
						{
							$('.package_id').val = "";
							$('.edit').show();				
							$('#Package_name_label').text(package_name);
							$('#Package_name_label').show();							
							$('#package_name_id').hide();							
							$('.editNew').show();
							$('#package_description_label').text(package_description);
							$('#package_description_label').show();							
							$('#package_description').hide();
							//$('#package_description').hide();
							
							
							$('.mix_source').show();
							getPackageinfo();
						},
						error: function()
						{
							result = false;
						}
					});
				}				
			} 
		 });		
	});
	
	$('.edit').click(function(){
		$(this).hide();
		$('#Package_name_label').hide();
		$('#package_name_id').show();
		//$('#package_description').show();
	});
	
	$('.editNew').click(function(){
		$(this).hide();
		$('#package_description_label').hide();
		$('#package_description').show();
	});
	
	function printObject(o) {
		var out = '';
		for (var p in o) {
			if(typeof o[p] === 'object')
			{
				o[p] = printObject(o[p]);
				o[p] = '{ <br/> '+o[p] + '} <br/>';
			}
			out += p + ': ' + o[p] + '<br/>';
		}
		return out;
	}
	

	function additem(){
		$('.additem').off('click');
		$('.additem').click(function(){
			if (this.checked) {
				var package_id = $('.package_id').val();
				var Asset_Type = $(this).parent().parent().parent().find('input[name="RSys_Asset_Type_Id"]').val();
					if(Asset_Type=='' || Asset_Type == null || Asset_Type == 'undefined')
					{
						Asset_Type = $(this).parent().find('input[name="Asset_Type"]').val();
					}
				var Asset_Id = $(this).val();
				var Asset_Name = $(this).parent().find('.asset_item').html();
				addtopackage(package_id, Asset_Type, Asset_Id, Asset_Name);
				$('.ValidatePackage').removeAttr("disabled");
				$('.DeployPackage').removeAttr("disabled");
				$('#gridWrapper').show();
			}
			
		});
	}
	
	function disabled_checkbox()
	{
		var status = $('.status_cursor').text();
		if(status == 'Completed' || status == 'Deployment In Queue')
		{
			$("input.additem").attr("disabled", true);
			$(".ValidatePackage").removeClass('.ValidatePackage');
			$(".DeployPackage").removeClass('.DeployPackage');
			// $('.ValidatePackage').off('click');
			// $('.DeployPackage').off('click');
			// $('.DeployPackage, .ValidatePackage').click(function(e) {
			// console.log("enable");
			// e.preventDefault();
			// do other stuff when a click happens
		// });
		}
	}
	
	var status = $('.status_cursor').text();
	if(status == 'Completed' || status == 'Deployment In Queue')
	{
		$(".ValidatePackage").removeClass('.ValidatePackage');
		$(".DeployPackage").removeClass('.DeployPackage');
		// $('.ValidatePackage').attr("disabled", true);
		// $('.DeployPackage').attr("disabled", true);
		// $('.ValidatePackage').off('click');
		// $('.DeployPackage').off('click');
		// $('.DeployPackage, .ValidatePackage').click(function(e) {
			// e.preventDefault();
		// });
	}
	
	$('.dependent_child').click(function ()
	{
		if($(this).attr('aria-expanded') == "false"){
			$(this).find(".fa-plus-square-o").addClass("fa-minus-square-o").removeClass("fa-plus-square-o");
		}else{
			$(this).find(".fa-minus-square-o").addClass("fa-plus-square-o").removeClass("fa-minus-square-o");
		}
	});

	function getInvalidAsset(datavalue){
		var package_id = $('.package_id').val();
		var assetList= $('.invalid_child_assets');
		$.ajax(
		{
			type: 'post',
			url: 'package_info/getinvalidAssets',
			data: datavalue,
			dataType:'json',
			beforeSend: function()
			{
				assetList.html('<div class="list-group-item " style="text-align: center;" ><div class="dependent_child_refresh glyphicon glyphicon-refresh glyphicon-refresh-animate"></div></div>');
			},
			success: function(data)
			{
				assetList.html('');
				$('.dependent_child_refresh').remove();
				if(data.length == "" || data.length < 0 || data == false)
				{
					assetList.html('No Invalid assets found');
				}
				else
				{
					var childIdList = [];					
					assetList.append("Assets not found in target System or in the Deployment package");
					$(data).each(function(){	
						var flag=true;
						var out='';
						var dataAsset_Id=this.Asset_Id;
						var dataAsset_Type=this.Asset_Type;
						$(childIdList).each(function(){
							if(dataAsset_Id == this.Asset_Id && dataAsset_Type == this.Asset_Type ){
								flag = false;
							}
						});
						
						if(flag){							
							// out += '<div class="list-group-item "><label class="invalidAsset" style="color:#337ab7;"><!--span class="glyphicon glyphicon-ok"></span> '+this.Asset_Name+' </label></div>';
							out += '<div class="list-group-item "><label class="invalidAsset" style="color:#337ab7;">'+this.Asset_Name+' </label></div>';
							assetList.append(out);
						}
					});
				}
				$(".invalidAsset").attr('title', 'Add these assets in Deployment Package');
			},
			error: function() 
			{
				assetList.html('Connection Timeout Error');
			}
		});
	
	}
	// function getErrorMeassge(datavalue){
		
		// var package_id = $('.package_id').val();
		// var assetList= $('.error_message');
		// $.ajax(
		// {
			// type: 'post',
			// url: 'package_info/getErrorMeassge',
			// data: datavalue,
			// dataType:'json',
			// beforeSend: function()
			// {
				// assetList.html('<div class="list-group-item " style="text-align: center;" ><div class="dependent_child_refresh glyphicon glyphicon-refresh glyphicon-refresh-animate"></div></div>');
			// },
			// success: function(data)
			// {
				// alert('came to success');
				// assetList.html('');
				// $('.dependent_child_refresh').remove();
				// if(data.length == "" || data.length < 0 || data == false)
				// {
					// assetList.html('No Data found');
				// }
				// else
				// {
					// assetList.html(data);
				// }
			// },
			// error: function() 
			// {
				// assetList.html('HTTP/1.1 100 Continue HTTP/1.1 500 InternalServerError');
			// }
		// });
	
	// }
	
	function getChildInfo(datavalue){
		var package_id = $('.package_id').val();
		var packageList= $('.dependent_child_header');
		$.ajax(
		{
			type: 'post',
			url: 'package_info/getchildinfo',
			data: datavalue,
			dataType:'json',
			beforeSend: function()
			{
				//alert(this);
				$('.childList').html('');
				$('.dependent_childInfo .childValue').hide();				
				var child_asset_type = $('.dependent_child').parent().find(this).show();packageList.html('<div class="list-group-item " style="text-align: center;" ><div class="dependent_child_refresh glyphicon glyphicon-refresh glyphicon-refresh-animate"></div></div>');
			},
			success: function(data)
			{
				packageList.html('');
				$('.ValidatePackage span').remove();
				$('.dependent_child_refresh').remove();
				$('.childList').html('');
				if(data.length == "" || data.length < 0 || data == false)
				{
					packageList.html('No child assets found');
				}
				else
				{
					var childIdList = [];					
					$(data).each(function(){	
						var flag=true;
						var out='';
						var dataAsset_Id=this.Asset_Id;
						var dataAsset_Type=this.Asset_Type;
						$(childIdList).each(function(){
							if(dataAsset_Id == this.Asset_Id && dataAsset_Type == this.Asset_Type ){
								flag = false;
							}
						});
						if(flag){							
							childIdList.push({Asset_Id:this.Asset_Id,Asset_Type:this.Asset_Type});
							var child_asset_type = $('.dependent_child').parent().find('#child_asset_type'+this.Asset_Type.replace(" ","_"));
							
							$('.dependent_childInfo .childValue'+this.Asset_Type.replace(" ","_")).show();
							out += 	'<div class="list-group-item "><input name="Asset_Type"  type="hidden" value="'+this.Asset_Type+'"/><input class="additem"  type="checkbox" name="chkItem" value="'+this.Asset_Id+'"/><a href="#id_'+ this.Asset_Id +'" class="asset_item" data-toggle="collapse" >'+this.Display_Name +'</a></div>';
							
							child_asset_type.append(out);
							//$(child_asset_type).attr('aria-expanded') == "true";
							//child_asset_type.show();
						}
					});
					additem();
				}	
				
			},
			error: function() 
			{
				//packageList.html('');	
				packageList.html('Connection Timeout Error');
			}
		}); 
	}
	
	
	//after clicking on source update Package name (only when when new package )
	function updatesource(sourcename)
	{
		var package_id = $('.package_id').val();
		var package_description = $('#package_description').val();
		var package_name = $('#package_name_id').val();
		$.ajax(
		{
			type: 'post',
			url: 'Package_info/update_source_site_name',
			data: {packageId: package_id, Source_Site_Name: sourcename, package_description: package_description, package_name: package_name},
			dataType:'json',
			beforeSend: function()
			{				
			},
			success: function(data)
			{
				$('.package_id').val(data);
				$('.edit').show();
				$('.editNew').show();
				$('#Package_name_label').text(package_name);
				$('#Package_name_label').show();
				$('#package_description_label').text(package_description);
				$('#package_description_label').show();
				$('#package_name_id').hide();
				$('#package_description').hide();
				//$(".footer_deploy_button").hide();
			},
			error: function() 
			{
			
			}
		}); 
	}
	
	//to differentiate Source and Target
	$(document).ready(function() {		
		//code can be better than that
		$('.selectSys option').each(function(){
			var val_one = $('.selectSys').val();
			$('.selectSys2 option').each(function(){
				if($(this).val() == val_one)
					$(this).hide();
				else
					$(this).show();
			});
		});
		$('.selectSys').change(function(){
			$('.selectSys option').each(function(){
				var val_one = $('.selectSys').val();
				$('.selectSys2 option').each(function(){
					if($(this).val() == val_one)
						$(this).hide();
					else
						$(this).show();
				});
			});
		});		
		$('.selectSys2').change(function(){
			$('.selectSys2 option').each(function(){				
				var val_one = $('.selectSys2').val();
				$('.selectSys option').each(function(){					
					if($(this).val() == val_one)
						$(this).hide();
					else
						$(this).show();
				});
			});
		});
		
		if($('.package_id').val() == '' || $('.package_id').val() == 'null'){
			$(".build_section *").attr("disabled", "disabled");
		}	
		
		$('.ValidatePackage').click(function (){
			var package_id = $('.package_id').val();
			
			var datavalue = {PackageId:package_id};
			$.ajax(
			{
				type: 'post',
				// url: 'package_info/validatepackage',
				url: 'deploy/validationinqueue',
				data: datavalue,
				dataType:'json',
				beforeSend: function()
				{
					
					$('.ValidatePackage span').remove();
					$('.ValidatePackage').append(' <span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span>');
					$('.ValidatePackage').attr("disabled",'true');
					$('.ValidatePackage').off('click');
				},
				success: function(data1)
				{
					$('.ValidatePackage span').remove();
					$('.footer_deploy_button').after('<p style="margin-bottom: 2px;">This Package Validation is in queue.</p>');
					
					// getPackageinfo();
					
				},
				error: function() 
				{
					// alert('fvdf error');
					$('.ValidatePackage span').remove();
				}
			});
		});
	});


//Code for select all
	// $('.select_all').on('click',function () { 
		// alert("coming here");
		// $('input:checkbox').prop('checked', this.checked).each(function {
			// additem();
		// });
	// });	
	// $('#select_all').change(function () {    
		// $('.additem').trigger('click');	
			
	// });
	
	// $(document).ready(function(){
		// $( "#select_all" ).on( "click", function() {
			// $('input:checkbox').prop('checked', this.checked).each(function(){
				// $('.additem').trigger('click');
		// });
		// });
	// });
	// function checking(){
		// $('.additem').trigger('click');
		// additem();
	// }
	

</script>