-- phpMyAdmin SQL Dump
-- version 4.4.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 20, 2016 at 10:45 AM
-- Server version: 5.6.25
-- PHP Version: 5.6.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `eloqua`
--
CREATE DATABASE IF NOT EXISTS `eloqua` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `eloqua`;

-- --------------------------------------------------------

--
-- Table structure for table `changeset`
--

CREATE TABLE IF NOT EXISTS `changeset` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `org_id` int(10) NOT NULL,
  `list_name` varchar(32) NOT NULL,
  `status` int(2) DEFAULT NULL,
  `msg` longtext,
  `created_at` date DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `changeset`
--

INSERT INTO `changeset` (`id`, `user_id`, `org_id`, `list_name`, `status`, `msg`, `created_at`) VALUES
(1, 1, 1, 'test list', 1, '', '2015-12-17'),
(2, 1, 1, 'test2', 1, NULL, NULL),
(4, 1, 2, 'my list', 1, NULL, NULL),
(5, 1, 2, 'tarun test', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `changeset_components_log`
--

CREATE TABLE IF NOT EXISTS `changeset_components_log` (
  `id` int(11) NOT NULL,
  `changeset_log_id` int(11) NOT NULL,
  `source_id` int(11) NOT NULL,
  `destination_id` int(11) NOT NULL,
  `meta_type` varchar(32) NOT NULL,
  `meta` longtext NOT NULL,
  `status` int(3) NOT NULL,
  `msg` varchar(400) NOT NULL,
  `Timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `changeset_components_log`
--

INSERT INTO `changeset_components_log` (`id`, `changeset_log_id`, `source_id`, `destination_id`, `meta_type`, `meta`, `status`, `msg`, `Timestamp`) VALUES
(1, 67, 1, 1, 'ContactField', '{"type":"ContactField","id":"100001","createdAt":"-2208970800","depth":"complete","name":"Email Address","updatedAt":"-2208970800","dataType":"text","displayType":"text","internalName":"C_EmailAddress","isCaseSensitive":"false","isReadOnly":"false","isRequired":"false","isStandard":"true","isAccountLinkageField":"false","isPopulatedInOutlookPlugin":"false","isProtected":"false","updateType":"always"}', 409, '"[{\\"type\\":\\"ObjectValidationError\\",\\"container\\":{\\"type\\":\\"ObjectKey\\",\\"objectType\\":\\"ContactField\\"},\\"property\\":\\"name\\",\\"requirement\\":{\\"type\\":\\"UniquenessRequirement\\",\\"uniquenessScope\\":\\"global\\"},\\"value\\":\\"Email Address\\"}]"', '2016-05-24 10:25:30'),
(2, 67, 1, 1, 'ContactField', '{"type":"ContactField","id":"100002","createdAt":"-2208970800","depth":"complete","name":"First Name","updatedAt":"1323196613","dataType":"text","displayType":"text","internalName":"C_FirstName","isCaseSensitive":"false","isReadOnly":"false","isRequired":"false","isStandard":"true","isAccountLinkageField":"false","isPopulatedInOutlookPlugin":"false","isProtected":"false","updateType":"newNotBlank"}', 409, '"[{\\"type\\":\\"ObjectValidationError\\",\\"container\\":{\\"type\\":\\"ObjectKey\\",\\"objectType\\":\\"ContactField\\"},\\"property\\":\\"name\\",\\"requirement\\":{\\"type\\":\\"UniquenessRequirement\\",\\"uniquenessScope\\":\\"global\\"},\\"value\\":\\"First Name\\"}]"', '2016-05-24 10:25:31'),
(3, 67, 1, 1, 'ContactField', '{"type":"ContactField","id":"100003","createdAt":"-2208970800","depth":"complete","name":"Last Name","updatedAt":"1323196613","dataType":"text","displayType":"text","internalName":"C_LastName","isCaseSensitive":"false","isReadOnly":"false","isRequired":"false","isStandard":"true","isAccountLinkageField":"false","isPopulatedInOutlookPlugin":"false","isProtected":"false","updateType":"newNotBlank"}', 409, '"[{\\"type\\":\\"ObjectValidationError\\",\\"container\\":{\\"type\\":\\"ObjectKey\\",\\"objectType\\":\\"ContactField\\"},\\"property\\":\\"name\\",\\"requirement\\":{\\"type\\":\\"UniquenessRequirement\\",\\"uniquenessScope\\":\\"global\\"},\\"value\\":\\"Last Name\\"}]"', '2016-05-24 10:25:33'),
(4, 67, 1, 1, 'ContactField', '{"type":"ContactField","id":"100188","createdAt":"1323785280","createdBy":"12","depth":"complete","name":"Website","updatedAt":"1323785280","updatedBy":"12","dataType":"text","displayType":"text","internalName":"C_Website1","isCaseSensitive":"false","isReadOnly":"false","isRequired":"false","isStandard":"false","isAccountLinkageField":"false","isPopulatedInOutlookPlugin":"false","isProtected":"false","updateType":"always"}', 409, '"[{\\"type\\":\\"ObjectValidationError\\",\\"container\\":{\\"type\\":\\"ObjectKey\\",\\"objectType\\":\\"ContactField\\"},\\"property\\":\\"name\\",\\"requirement\\":{\\"type\\":\\"UniquenessRequirement\\",\\"uniquenessScope\\":\\"global\\"},\\"value\\":\\"Website\\"}]"', '2016-05-24 10:25:34'),
(5, 67, 1, 1, 'ContactField', '{"type":"ContactField","id":"100190","createdAt":"1324395660","createdBy":"12","depth":"complete","name":"Last SFDC Campaign Name1","updatedAt":"1324395660","updatedBy":"12","dataType":"text","displayType":"text","internalName":"C_SFDCLastCampaignName","isCaseSensitive":"false","isReadOnly":"false","isRequired":"false","isStandard":"true","isAccountLinkageField":"false","isPopulatedInOutlookPlugin":"false","isProtected":"false"}', 400, '"[{\\"type\\":\\"ObjectValidationError\\",\\"container\\":{\\"type\\":\\"ObjectKey\\",\\"objectId\\":\\"100190\\",\\"objectType\\":\\"ContactField\\"},\\"property\\":\\"updateType\\",\\"requirement\\":{\\"type\\":\\"EnumRequirement\\",\\"enumValues\\":[\\"always\\",\\"newNotBlank\\",\\"newIsEmailAddress\\",\\"existingBlank\\",\\"useFieldRule\\"]},\\"value\\":\\"<null>\\"}]"', '2016-05-24 10:25:35'),
(6, 68, 1, 1, 'ContactField', '{"type":"ContactField","id":"100186","createdAt":"1323785100","createdBy":"12","depth":"complete","name":"Industry - Normalized","updatedAt":"1323785100","updatedBy":"12","dataType":"text","displayType":"text","internalName":"C_Industry___Normalized1","isCaseSensitive":"false","isReadOnly":"false","isRequired":"false","isStandard":"false","isAccountLinkageField":"false","isPopulatedInOutlookPlugin":"false","isProtected":"false","updateType":"always"}', 401, '""', '2016-05-27 11:24:31'),
(7, 68, 1, 1, 'ContactField', '{"type":"ContactField","id":"100187","createdAt":"1323785100","createdBy":"12","depth":"complete","name":"Industry Description - Normalized","updatedAt":"1323785100","updatedBy":"12","dataType":"text","displayType":"text","internalName":"C_Industry_Description___Normalized1","isCaseSensitive":"false","isReadOnly":"false","isRequired":"false","isStandard":"false","isAccountLinkageField":"false","isPopulatedInOutlookPlugin":"false","isProtected":"false","updateType":"always"}', 401, '""', '2016-05-27 11:24:32'),
(8, 68, 1, 1, 'ContactField', '{"type":"ContactField","id":"100188","createdAt":"1323785280","createdBy":"12","depth":"complete","name":"Website","updatedAt":"1323785280","updatedBy":"12","dataType":"text","displayType":"text","internalName":"C_Website1","isCaseSensitive":"false","isReadOnly":"false","isRequired":"false","isStandard":"false","isAccountLinkageField":"false","isPopulatedInOutlookPlugin":"false","isProtected":"false","updateType":"always"}', 401, '""', '2016-05-27 11:24:34'),
(9, 68, 1, 1, 'ContactField', '{"type":"ContactField","id":"100189","createdAt":"1323785280","createdBy":"12","depth":"complete","name":"Title - Normalized","updatedAt":"1323785280","updatedBy":"12","dataType":"text","displayType":"text","internalName":"C_Title___Normalized1","isCaseSensitive":"false","isReadOnly":"false","isRequired":"false","isStandard":"false","isAccountLinkageField":"false","isPopulatedInOutlookPlugin":"false","isProtected":"false","updateType":"always"}', 401, '""', '2016-05-27 11:24:35'),
(10, 68, 1, 1, 'ContactField', '{"type":"ContactField","id":"100190","createdAt":"1324395660","createdBy":"12","depth":"complete","name":"Last SFDC Campaign Name1","updatedAt":"1324395660","updatedBy":"12","dataType":"text","displayType":"text","internalName":"C_SFDCLastCampaignName","isCaseSensitive":"false","isReadOnly":"false","isRequired":"false","isStandard":"true","isAccountLinkageField":"false","isPopulatedInOutlookPlugin":"false","isProtected":"false"}', 401, '""', '2016-05-27 11:24:36'),
(11, 69, 1, 1, 'ContactField', '{"type":"ContactField","id":"100186","createdAt":"1323785100","createdBy":"12","depth":"complete","name":"Industry - Normalized","updatedAt":"1323785100","updatedBy":"12","dataType":"text","displayType":"text","internalName":"C_Industry___Normalized1","isCaseSensitive":"false","isReadOnly":"false","isRequired":"false","isStandard":"false","isAccountLinkageField":"false","isPopulatedInOutlookPlugin":"false","isProtected":"false","updateType":"always"}', 409, '"[{\\"type\\":\\"ObjectValidationError\\",\\"container\\":{\\"type\\":\\"ObjectKey\\",\\"objectType\\":\\"ContactField\\"},\\"property\\":\\"name\\",\\"requirement\\":{\\"type\\":\\"UniquenessRequirement\\",\\"uniquenessScope\\":\\"global\\"},\\"value\\":\\"Industry - Normalized\\"}]"', '2016-05-27 11:25:25'),
(12, 69, 1, 1, 'ContactField', '{"type":"ContactField","id":"100187","createdAt":"1323785100","createdBy":"12","depth":"complete","name":"Industry Description - Normalized","updatedAt":"1323785100","updatedBy":"12","dataType":"text","displayType":"text","internalName":"C_Industry_Description___Normalized1","isCaseSensitive":"false","isReadOnly":"false","isRequired":"false","isStandard":"false","isAccountLinkageField":"false","isPopulatedInOutlookPlugin":"false","isProtected":"false","updateType":"always"}', 409, '"[{\\"type\\":\\"ObjectValidationError\\",\\"container\\":{\\"type\\":\\"ObjectKey\\",\\"objectType\\":\\"ContactField\\"},\\"property\\":\\"name\\",\\"requirement\\":{\\"type\\":\\"UniquenessRequirement\\",\\"uniquenessScope\\":\\"global\\"},\\"value\\":\\"Industry Description - Normalized\\"}]"', '2016-05-27 11:25:26'),
(13, 69, 1, 1, 'ContactField', '{"type":"ContactField","id":"100188","createdAt":"1323785280","createdBy":"12","depth":"complete","name":"Website","updatedAt":"1323785280","updatedBy":"12","dataType":"text","displayType":"text","internalName":"C_Website1","isCaseSensitive":"false","isReadOnly":"false","isRequired":"false","isStandard":"false","isAccountLinkageField":"false","isPopulatedInOutlookPlugin":"false","isProtected":"false","updateType":"always"}', 409, '"[{\\"type\\":\\"ObjectValidationError\\",\\"container\\":{\\"type\\":\\"ObjectKey\\",\\"objectType\\":\\"ContactField\\"},\\"property\\":\\"name\\",\\"requirement\\":{\\"type\\":\\"UniquenessRequirement\\",\\"uniquenessScope\\":\\"global\\"},\\"value\\":\\"Website\\"}]"', '2016-05-27 11:25:28'),
(14, 69, 1, 1, 'ContactField', '{"type":"ContactField","id":"100189","createdAt":"1323785280","createdBy":"12","depth":"complete","name":"Title - Normalized","updatedAt":"1323785280","updatedBy":"12","dataType":"text","displayType":"text","internalName":"C_Title___Normalized1","isCaseSensitive":"false","isReadOnly":"false","isRequired":"false","isStandard":"false","isAccountLinkageField":"false","isPopulatedInOutlookPlugin":"false","isProtected":"false","updateType":"always"}', 409, '"[{\\"type\\":\\"ObjectValidationError\\",\\"container\\":{\\"type\\":\\"ObjectKey\\",\\"objectType\\":\\"ContactField\\"},\\"property\\":\\"name\\",\\"requirement\\":{\\"type\\":\\"UniquenessRequirement\\",\\"uniquenessScope\\":\\"global\\"},\\"value\\":\\"Title - Normalized\\"}]"', '2016-05-27 11:25:29'),
(15, 69, 1, 1, 'ContactField', '{"type":"ContactField","id":"100190","createdAt":"1324395660","createdBy":"12","depth":"complete","name":"Last SFDC Campaign Name1","updatedAt":"1324395660","updatedBy":"12","dataType":"text","displayType":"text","internalName":"C_SFDCLastCampaignName","isCaseSensitive":"false","isReadOnly":"false","isRequired":"false","isStandard":"true","isAccountLinkageField":"false","isPopulatedInOutlookPlugin":"false","isProtected":"false"}', 400, '"[{\\"type\\":\\"ObjectValidationError\\",\\"container\\":{\\"type\\":\\"ObjectKey\\",\\"objectId\\":\\"100190\\",\\"objectType\\":\\"ContactField\\"},\\"property\\":\\"updateType\\",\\"requirement\\":{\\"type\\":\\"EnumRequirement\\",\\"enumValues\\":[\\"always\\",\\"newNotBlank\\",\\"newIsEmailAddress\\",\\"existingBlank\\",\\"useFieldRule\\"]},\\"value\\":\\"<null>\\"}]"', '2016-05-27 11:25:31'),
(16, 70, 1, 1, 'ContactField', '{"type":"ContactField","id":"100186","createdAt":"1323785100","createdBy":"12","depth":"complete","name":"Industry - Normalized","updatedAt":"1323785100","updatedBy":"12","dataType":"text","displayType":"text","internalName":"C_Industry___Normalized1","isCaseSensitive":"false","isReadOnly":"false","isRequired":"false","isStandard":"false","isAccountLinkageField":"false","isPopulatedInOutlookPlugin":"false","isProtected":"false","updateType":"always"}', 409, '"[{\\"type\\":\\"ObjectValidationError\\",\\"container\\":{\\"type\\":\\"ObjectKey\\",\\"objectType\\":\\"ContactField\\"},\\"property\\":\\"name\\",\\"requirement\\":{\\"type\\":\\"UniquenessRequirement\\",\\"uniquenessScope\\":\\"global\\"},\\"value\\":\\"Industry - Normalized\\"}]"', '2016-05-27 11:27:13'),
(17, 70, 1, 1, 'ContactField', '{"type":"ContactField","id":"100187","createdAt":"1323785100","createdBy":"12","depth":"complete","name":"Industry Description - Normalized","updatedAt":"1323785100","updatedBy":"12","dataType":"text","displayType":"text","internalName":"C_Industry_Description___Normalized1","isCaseSensitive":"false","isReadOnly":"false","isRequired":"false","isStandard":"false","isAccountLinkageField":"false","isPopulatedInOutlookPlugin":"false","isProtected":"false","updateType":"always"}', 409, '"[{\\"type\\":\\"ObjectValidationError\\",\\"container\\":{\\"type\\":\\"ObjectKey\\",\\"objectType\\":\\"ContactField\\"},\\"property\\":\\"name\\",\\"requirement\\":{\\"type\\":\\"UniquenessRequirement\\",\\"uniquenessScope\\":\\"global\\"},\\"value\\":\\"Industry Description - Normalized\\"}]"', '2016-05-27 11:27:14'),
(18, 70, 1, 1, 'ContactField', '{"type":"ContactField","id":"100188","createdAt":"1323785280","createdBy":"12","depth":"complete","name":"Website","updatedAt":"1323785280","updatedBy":"12","dataType":"text","displayType":"text","internalName":"C_Website1","isCaseSensitive":"false","isReadOnly":"false","isRequired":"false","isStandard":"false","isAccountLinkageField":"false","isPopulatedInOutlookPlugin":"false","isProtected":"false","updateType":"always"}', 409, '"[{\\"type\\":\\"ObjectValidationError\\",\\"container\\":{\\"type\\":\\"ObjectKey\\",\\"objectType\\":\\"ContactField\\"},\\"property\\":\\"name\\",\\"requirement\\":{\\"type\\":\\"UniquenessRequirement\\",\\"uniquenessScope\\":\\"global\\"},\\"value\\":\\"Website\\"}]"', '2016-05-27 11:27:15'),
(19, 70, 1, 1, 'ContactField', '{"type":"ContactField","id":"100189","createdAt":"1323785280","createdBy":"12","depth":"complete","name":"Title - Normalized","updatedAt":"1323785280","updatedBy":"12","dataType":"text","displayType":"text","internalName":"C_Title___Normalized1","isCaseSensitive":"false","isReadOnly":"false","isRequired":"false","isStandard":"false","isAccountLinkageField":"false","isPopulatedInOutlookPlugin":"false","isProtected":"false","updateType":"always"}', 409, '"[{\\"type\\":\\"ObjectValidationError\\",\\"container\\":{\\"type\\":\\"ObjectKey\\",\\"objectType\\":\\"ContactField\\"},\\"property\\":\\"name\\",\\"requirement\\":{\\"type\\":\\"UniquenessRequirement\\",\\"uniquenessScope\\":\\"global\\"},\\"value\\":\\"Title - Normalized\\"}]"', '2016-05-27 11:27:17'),
(20, 70, 1, 1, 'ContactField', '{"type":"ContactField","id":"100190","createdAt":"1324395660","createdBy":"12","depth":"complete","name":"Last SFDC Campaign Name1","updatedAt":"1324395660","updatedBy":"12","dataType":"text","displayType":"text","internalName":"C_SFDCLastCampaignName","isCaseSensitive":"false","isReadOnly":"false","isRequired":"false","isStandard":"true","isAccountLinkageField":"false","isPopulatedInOutlookPlugin":"false","isProtected":"false"}', 400, '"[{\\"type\\":\\"ObjectValidationError\\",\\"container\\":{\\"type\\":\\"ObjectKey\\",\\"objectId\\":\\"100190\\",\\"objectType\\":\\"ContactField\\"},\\"property\\":\\"updateType\\",\\"requirement\\":{\\"type\\":\\"EnumRequirement\\",\\"enumValues\\":[\\"always\\",\\"newNotBlank\\",\\"newIsEmailAddress\\",\\"existingBlank\\",\\"useFieldRule\\"]},\\"value\\":\\"<null>\\"}]"', '2016-05-27 11:27:18');

-- --------------------------------------------------------

--
-- Table structure for table `changeset_log`
--

CREATE TABLE IF NOT EXISTS `changeset_log` (
  `id` int(11) NOT NULL,
  `changeset_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `destination` int(11) NOT NULL,
  `status` int(2) NOT NULL,
  `msg` longtext NOT NULL,
  `Timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=71 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `changeset_log`
--

INSERT INTO `changeset_log` (`id`, `changeset_id`, `user_id`, `destination`, `status`, `msg`, `Timestamp`) VALUES
(1, 2, 1, 1, 1, '', '2016-05-27 06:46:14'),
(2, 2, 1, 1, 1, '', '2016-05-27 06:46:14'),
(3, 2, 1, 1, 1, '', '2016-05-27 06:46:14'),
(4, 2, 1, 1, 1, '', '2016-05-27 06:46:14'),
(5, 2, 1, 1, 1, '', '2016-05-27 06:46:14'),
(6, 2, 1, 1, 1, '', '2016-05-27 06:46:14'),
(7, 2, 1, 1, 1, '', '2016-05-27 06:46:14'),
(8, 2, 1, 1, 1, '', '2016-05-27 06:46:14'),
(9, 2, 1, 1, 1, '', '2016-05-27 06:46:14'),
(10, 2, 1, 1, 1, '', '2016-05-27 06:46:14'),
(11, 2, 1, 1, 1, '', '2016-05-27 06:46:14'),
(12, 2, 1, 1, 1, '', '2016-05-27 06:46:14'),
(13, 1, 1, 2, 1, '', '2016-05-27 06:46:14'),
(14, 1, 1, 2, 1, '', '2016-05-27 06:46:14'),
(15, 1, 1, 2, 1, '', '2016-05-27 06:46:14'),
(16, 1, 1, 2, 1, '', '2016-05-27 06:46:14'),
(17, 1, 1, 2, 1, '', '2016-05-27 06:46:14'),
(18, 1, 1, 2, 1, '', '2016-05-27 06:46:14'),
(19, 1, 1, 2, 1, '', '2016-05-27 06:46:14'),
(20, 1, 1, 2, 1, '', '2016-05-27 06:46:14'),
(21, 1, 1, 2, 1, '', '2016-05-27 06:46:14'),
(22, 1, 1, 2, 1, '', '2016-05-27 06:46:14'),
(23, 1, 1, 2, 1, '', '2016-05-27 06:46:14'),
(24, 1, 1, 2, 1, '', '2016-05-27 06:46:14'),
(25, 3, 1, 1, 1, '', '2016-05-27 06:46:14'),
(26, 3, 1, 1, 1, '', '2016-05-27 06:46:14'),
(27, 3, 1, 1, 1, '', '2016-05-27 06:46:14'),
(28, 3, 1, 1, 1, '', '2016-05-27 06:46:14'),
(29, 3, 1, 1, 1, '', '2016-05-27 06:46:14'),
(30, 3, 1, 1, 1, '', '2016-05-27 06:46:14'),
(31, 3, 1, 1, 1, '', '2016-05-27 06:46:14'),
(32, 3, 1, 1, 1, '', '2016-05-27 06:46:14'),
(33, 3, 1, 1, 1, '', '2016-05-27 06:46:14'),
(34, 3, 1, 1, 1, '', '2016-05-27 06:46:14'),
(35, 3, 1, 1, 1, '', '2016-05-27 06:46:14'),
(36, 3, 1, 1, 1, '', '2016-05-27 06:46:14'),
(37, 3, 1, 1, 1, '', '2016-05-27 06:46:14'),
(38, 3, 1, 1, 1, '', '2016-05-27 06:46:14'),
(39, 3, 1, 1, 1, '', '2016-05-27 06:46:14'),
(40, 3, 1, 1, 1, '', '2016-05-27 06:46:14'),
(41, 3, 1, 1, 1, '', '2016-05-27 06:46:14'),
(42, 3, 1, 1, 1, '', '2016-05-27 06:46:14'),
(43, 3, 1, 1, 1, '', '2016-05-27 06:46:14'),
(44, 3, 1, 1, 1, '', '2016-05-27 06:46:14'),
(45, 1, 1, 2, 1, '', '2016-05-27 06:46:14'),
(46, 1, 1, 2, 1, '', '2016-05-27 06:46:14'),
(47, 1, 1, 2, 1, '', '2016-05-27 06:46:14'),
(48, 1, 1, 2, 1, '', '2016-05-27 06:46:14'),
(49, 1, 1, 2, 1, '', '2016-05-27 06:46:14'),
(50, 1, 1, 2, 1, '', '2016-05-27 06:46:14'),
(51, 1, 1, 2, 1, '', '2016-05-27 06:46:14'),
(52, 1, 1, 2, 1, '', '2016-05-27 06:46:14'),
(53, 1, 1, 2, 1, '', '2016-05-27 06:46:14'),
(54, 1, 1, 2, 1, '', '2016-05-27 06:46:14'),
(55, 1, 1, 2, 1, '', '2016-05-27 06:46:14'),
(56, 1, 1, 2, 1, '', '2016-05-27 06:46:14'),
(57, 1, 1, 2, 1, '', '2016-05-27 06:46:14'),
(58, 1, 1, 2, 1, '', '2016-05-27 06:46:14'),
(59, 1, 1, 2, 1, '', '2016-05-27 06:46:14'),
(60, 1, 1, 2, 1, '', '2016-05-27 06:46:14'),
(61, 1, 1, 2, 1, '', '2016-05-27 06:46:14'),
(62, 1, 1, 2, 1, '', '2016-05-27 06:46:14'),
(63, 1, 1, 2, 1, '', '2016-05-27 06:46:14'),
(64, 1, 1, 2, 1, '', '2016-05-27 06:46:14'),
(65, 1, 1, 2, 1, '', '2016-05-27 06:46:14'),
(66, 5, 1, 1, 1, '', '2016-05-27 06:46:14'),
(67, 5, 1, 1, 1, '', '2016-05-27 06:46:14'),
(68, 4, 1, 1, 1, '', '2016-05-27 11:24:30'),
(69, 4, 1, 1, 1, '', '2016-05-27 11:25:24'),
(70, 4, 1, 1, 1, '', '2016-05-27 11:27:12');

-- --------------------------------------------------------

--
-- Table structure for table `component_list`
--

CREATE TABLE IF NOT EXISTS `component_list` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `changeset_id` int(11) NOT NULL,
  `meta_type` varchar(20) NOT NULL,
  `object_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `component_list`
--

INSERT INTO `component_list` (`id`, `user_id`, `changeset_id`, `meta_type`, `object_id`) VALUES
(26, 1, 1, 'ContactFields', 100001),
(27, 1, 1, 'ContactFields', 100002),
(28, 1, 1, 'ContactFields', 100003),
(29, 1, 1, 'ContactFields', 100004),
(40, 1, 2, 'ContactFields', 100002),
(41, 1, 2, 'ContactFields', 100003),
(42, 1, 2, 'ContactFields', 100004),
(43, 1, 2, 'ContactFields', 100005),
(44, 1, 2, 'ContactFields', 100006),
(45, 1, 2, 'AccountFields', 100085),
(46, 1, 2, 'AccountFields', 100088),
(47, 1, 2, 'AccountFields', 100089),
(48, 1, 2, 'AccountFields', 100090),
(49, 1, 1, 'AccountFields', 100192),
(50, 1, 1, 'AccountFields', 100203),
(51, 1, 1, 'AccountFields', 100204),
(52, 1, 1, 'AccountFields', 100205),
(55, 1, 4, 'ContactFields', 100186),
(56, 1, 4, 'ContactFields', 100187),
(57, 1, 4, 'ContactFields', 100188),
(58, 1, 4, 'ContactFields', 100189),
(59, 1, 4, 'ContactFields', 100190),
(60, 1, 5, 'ContactFields', 100001),
(61, 1, 5, 'ContactFields', 100002),
(62, 1, 5, 'ContactFields', 100003),
(63, 1, 5, 'ContactFields', 100188),
(64, 1, 5, 'ContactFields', 100190);

-- --------------------------------------------------------

--
-- Table structure for table `eloqua_login`
--

CREATE TABLE IF NOT EXISTS `eloqua_login` (
  `id` int(10) NOT NULL,
  `users_id` int(10) NOT NULL,
  `OrgName` varchar(50) DEFAULT NULL,
  `base_url` varchar(100) NOT NULL,
  `userid` varchar(32) NOT NULL,
  `token` varchar(250) DEFAULT NULL,
  `token_type` varchar(20) DEFAULT NULL,
  `refresh_token` varchar(250) DEFAULT NULL,
  `ExpiresAt` int(11) NOT NULL DEFAULT '0',
  `enable` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `eloqua_login`
--

INSERT INTO `eloqua_login` (`id`, `users_id`, `OrgName`, `base_url`, `userid`, `token`, `token_type`, `refresh_token`, `ExpiresAt`, `enable`) VALUES
(1, 1, 'CSMichaelTorio\\Ajay.Sikri', 'https://secure.p01.eloqua.com/', 'CSMichaelTorio/Ajay.Sikri', 'MzExNToxYzBhN1FGMEw1MHFsWVZ+R2VTWG9GZFhRc284cUNXNkFFajdHU0YxRDFaOHJKfnFwQkVsdTFXbnRRd1dIfmtQV2R3UEZxS2NOOWI4VnB2OWVPNkhkdmZFdXVCamhLOWNpbTdS', 'bearer', 'MzExNToxb1AtUnMtVFFUa1d1aWc5SlpnRUtjYm5FUzJhZDNqYzFCWVBZRzRsbm04SH5LZHVUNjdEZGVQMFdzSjFISnpKdWRtdkd3Mml1RU9yMS1qV1REY1VudUpYdWpuRjZXbjctaWwz', 1464377069, 1),
(2, 1, 'CSAjaySikri\\Ajay.Sikri', 'https://secure.p03.eloqua.com/', 'CSAjaySikri/Ajay.Sikri', 'MTg5NjE4Mzc2NzoxYm1TdGZkNmF+MDJTQUhaQjhMVkdxNnFjOWZ3fn5MZmN6NjkzdVhyejBXU1RXZVhsWWFqQVBWdXV4M0ZrSG5vTmltSFlWQ252TmdOfnZkcnF+QlBPSjd3N1ROR2dROVMxZ0V+', 'bearer', 'MTg5NjE4Mzc2Nzoxb1J2MHpxTHhRMDJzZnVMVGtzbkIxTUVmUFhWYTh1YzdmcXFydWJOTFliTGtmTzhsazdvZzhwb0l1eWFic0ItbWlPNTJpSHdJUTAtWGxqSVFYY1JXbHRGd1Zwb0dFTk93NnNT', 1464375569, 1),
(3, 1, 'tango', 'https://secure.p03.eloqua.com/', 'CSAjaySikri/Ajay.Sikri', 'MTg5NjE4Mzc2NzoxN0hPd1JrdDdiVXlyVFR+eEFtNEQ2VVpsU1FDZTVYOFhGMDVWSDRiVDQ1NmlWT3dZYTVxSlN4elZUczF0UC1+Q1VJNk9Vcn5mMmpvdFJEYU92ZXRzUDl4RGx5YlpzdS1hc2Zm', 'bearer', 'MTg5NjE4Mzc2NzoxY0hFOFVTSC1kMGVhSmxnUFFRWXlhZjA4WkNIaFB4THkzUkFlVmRxQkQwUjNYWlFSWHN+UnhqSVlPT05zUjJab2lBaXBISlR4c35Mbng2R3d5MWxONkpLdmMzT1JnaWIza1dH', 1464094842, 1),
(4, 1, 'Leo', 'https://secure.p03.eloqua.com/', 'CSAjaySikri/Ajay.Sikri', 'MTg5NjE4Mzc2NzoxdDdJZlMxVkY1a2F2SjNlekhaVUh+Qko5aHRsRUp6ZnI4RFdUeTdrMTJLc3NmSjVOT1NrZVJ1eGwwWEpVcklZalVkV2hOcVppa3VhaTBiWjNUQ0JFZHIweX56R1N+T3UyM0pS', 'bearer', 'MTg5NjE4Mzc2NzoxdTJGallmcjM1VW1RN3Y2WlJ6OVhtVmpjOHVnd0lsMUtOOXpNVm4waExaWVEweHRRem9+dlU3VH5RQ1pWaGJk', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `preference`
--

CREATE TABLE IF NOT EXISTS `preference` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `Contact` int(2) NOT NULL DEFAULT '0',
  `Account` int(2) NOT NULL DEFAULT '0',
  `CDO` int(2) NOT NULL DEFAULT '0',
  `Segments` int(2) NOT NULL DEFAULT '0',
  `Filters` int(2) NOT NULL DEFAULT '0',
  `Emails` int(2) NOT NULL DEFAULT '0',
  `Forms` int(2) NOT NULL DEFAULT '0',
  `Landing_Pages` int(2) NOT NULL DEFAULT '0',
  `Campaigns` int(2) NOT NULL DEFAULT '0',
  `Programs` int(2) NOT NULL DEFAULT '0',
  `Shared_Lists` int(2) NOT NULL DEFAULT '0',
  `Email_Groups` int(2) NOT NULL DEFAULT '0',
  `Option_Lists` int(2) NOT NULL DEFAULT '0',
  `Events` int(2) NOT NULL DEFAULT '0',
  `Contact_Views` int(2) NOT NULL DEFAULT '0',
  `Account_Views` int(2) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `preference`
--

INSERT INTO `preference` (`id`, `user_id`, `Contact`, `Account`, `CDO`, `Segments`, `Filters`, `Emails`, `Forms`, `Landing_Pages`, `Campaigns`, `Programs`, `Shared_Lists`, `Email_Groups`, `Option_Lists`, `Events`, `Contact_Views`, `Account_Views`) VALUES
(1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `role_id` bigint(20) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0=inactive;1=active',
  `last_login` timestamp NULL DEFAULT NULL,
  `login_ip` varchar(15) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '0.0.0.0',
  `PasswordResetCode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` bigint(20) NOT NULL DEFAULT '-1',
  `modified_by` bigint(20) NOT NULL DEFAULT '-1',
  `admin_access` int(2) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `first_name`, `last_name`, `mobile`, `role_id`, `status`, `last_login`, `login_ip`, `PasswordResetCode`, `created_at`, `updated_at`, `created_by`, `modified_by`, `admin_access`) VALUES
(1, 'tarun.singh@crmsci.com', 'e35cf7b66449df565f93c607d5a81d09', 'Tarun Kumar', 'Singh', '8884388123', 2, 1, NULL, '0.0.0.0', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', -1, -1, 1),
(2, 'contactus@portqii.com', 'e10adc3949ba59abbe56e057f20f883e', 'test', 'test', '9756131356', 2, 1, NULL, '0.0.0.0', NULL, '2016-05-19 06:25:55', '0000-00-00 00:00:00', -1, -1, 0),
(3, 'mr.tksingh@gmail.com', '4297f44b13955235245b2497399d7a93', 'Tarun', 'Singh', '9473690542', 2, 1, NULL, '0.0.0.0', NULL, '2016-06-01 06:52:41', '0000-00-00 00:00:00', -1, -1, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `changeset`
--
ALTER TABLE `changeset`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `changeset_components_log`
--
ALTER TABLE `changeset_components_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `changeset_log`
--
ALTER TABLE `changeset_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `component_list`
--
ALTER TABLE `component_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `eloqua_login`
--
ALTER TABLE `eloqua_login`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `preference`
--
ALTER TABLE `preference`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `changeset`
--
ALTER TABLE `changeset`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `changeset_components_log`
--
ALTER TABLE `changeset_components_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `changeset_log`
--
ALTER TABLE `changeset_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=71;
--
-- AUTO_INCREMENT for table `component_list`
--
ALTER TABLE `component_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=65;
--
-- AUTO_INCREMENT for table `preference`
--
ALTER TABLE `preference`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
